[SPADE](https://gitlab.com/nest.lbl.gov/spade) contains the interfaces and classes used to create a SPADE server.

This can either build as a standalone vanilla server or a JAR file to be included in a customized version. For details on how to build this project see the [developer notes](developer_notes.md).
