# Scanning Drop-boxes #

The scanning of drop-boxes are required to meet a number of policy constraints.

1. Do not exceed the limit for the total number of tickets.

   (Previously the scan has been allowed to "overflow" this number when the limit is crossed during the sweep. This is, now, a poor policy as it means that high priority registrations have to wait for the ticket number to drop below the limit before they can even be detected. By not allowing it to exceed the limit, space under the limit should open up by the time of the next scan, allowing at least some high priority registrations to be ingested for every scan.)

1. Do not exceed the limit for each registration.

1. Ensure files matching high priority registrations are ingested before ones with a lower priority.

1. Search the tree of directories below any drop-box directory, in order to allow semaphore files to be spread over an arbitrary number of sub-directories.

   (The cases where two registrations share the same drop-box, or one registration's drop-box is a sub-directory of another registration's drop-box need to be addressed. Provided the semaphore patterns of the two registrations do not both match a file in any common directory there is no issue. For the moment, the policy will be that if the the semaphore patterns of the two registrations both match a file in any common directory then the resulting registration assignment is undefined.)

1. Allow for ordering for the selection of which files to ingest, e.g. oldest first, youngest first, etc., depending on the configuration setting.


As well as the above policy contraints, there are some implementation constraints.

1. Minimize the number of times a host is accessed, as this can be very expensive.

   (Currently the `localhost` is the only host accessed but that should not be taken as granted as the facility for alternate hosts is there.)

1. Minimize the number of times a directory listing is made, as this can be expensive.

1. Minimize the creation of `File` objects for files that do not match a registration.


##  Minimizing the Creation of `File` Objects ##

The clearest way to ensure the "Minimize the creation of `File` objects" constraint is to use the `File#list(FilenameFilter)` method. This avoid the creation and any unnecessary `File` objects, while at the same time providing a callback to select what filesname match any of the registrations appropriate to this directory. Another option would be the `Files#newDirectoryStream(Path, , DirectoryStream.Filter)`method, but that would create `Path` objects, and cause the same challenge as the creation of `File` objects.


## Minimizing File System access ##

The solution to "Minimize the number of times a host is accessed" and "Minimize the number of times a directory listing is made" is to pre-sort the known registrations into a dictionary, indexed by host and directory, whose values are the set of regisrations that apply to the indicated directory (This is implemented in the `LoadedRegistrations` class). Note that while the drop-box directories are know in advance, the sub-directory trees are not so these need to be handle dynamically when the scan is made so new sub-directories can be picked up. When a drop-box directory is large, this can be time consuming, but it is not clear it can be avoided. However, inbound drop-boxes should not need to support sub-directories, so scans of those directories should be able to skip that step. Tis also means that an option should be introduced to the registration/drop-box allowing it to skip sub-directory searches, thus speeding up its scans.



