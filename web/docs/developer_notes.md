# Developer Notes for `spade` #

These notes are to help developers develop, maintain and debug the `spade` Java library.


# Index of Notes #

This document is a list of notes. They are in no particular order (as defining one and maintaining it does not seem the best use of time).

The current topic covered are:

*   [Building `spade`](#building-spade)
*   [Deploying `spade`](#deploying-spade)


# Building `spade` #

The following assumes [gradle](https://gradle.org/) is already installed and shows how to build and install a released version of `spade` locally.

    SPADE_VERSION=5.0.0
    git clone git@gitlab.com:nest.lbl.gov/spade.git
    cd spade
    git checkout ${SPADE_VERSION}
    ./gradlew clean build publishMavenPublicationToMavenLocal

Clearly to build the `master` the `SPADE_VERSION` should be set to that value.

The `publishMavenPublicationToMavenLocal` option of the `gradle` command means that the resulting JAR file is put in the local Maven repository so that you can build against this version without have to do a full deployment.

---
*Note:* This step assumes all of the JAR files on which `spade` depends are available from their maven repositories, if not the following links show how to build them.

*   [`nest-common`](https://gitlab.com/nest.lbl.gov/nest-common/-/blob/master/developer_notes.md#building-nest-common)
*   [`nest-jee`](https://gitlab.com/nest.lbl.gov/nest-jee/-/blob/master/developer_notes.md#building-nest-jee)
*   [`tally`](https://gitlab.com/nest.lbl.gov/tally/-/blob/master/developer_notes.md#building-tally)
*   [`lazarus`](https://gitlab.com/nest.lbl.gov/lazarus/-/blob/master/developer_notes.md#building-lazarus)
*   [`spade-api`](https://gitlab.com/nest.lbl.gov/spade-api/-/blob/master/developer_notes.md#building-spade-api)
*   [`spade-ajax`](https://gitlab.com/nest.lbl.gov/spade-ajax/-/blob/master/developer_notes.md#building-spade-ajax)

---


# Deploying `spade` #

As noted in the [Building `spade`](#building-spade) section, you can create a local copy of the JAR file in order to build against it. When you are ready to deploy a released version of the project all you need to do is tag the code and push that tag to the [GitLab respoistory](https://gitlab.com/nest.lbl.gov/spade). Provided the tag is of the form `XX.YY.ZZ.aaa`, where `XX`, `YY` and `ZZ` are numerical and `aaa` should be something along the lines of `devA`, the [CI/CD](https://gitlab.com/nest.lbl.gov/spade/-/pipelines) will be triggered an, if successful, will deploy the resulting artifact to the appropriate [repository](https://gitlab.com/groups/nest.lbl.gov/-/packages).

---
*Note:* Access to the package repository on GitLab require a deploy-token. Rather than needing every use to acquire one, one has already been set of the web server at `http://nest.lbl.gov/`, and the following URL can be used to replace the one provide by the GitLab repository.

*   [https://nest.lbl.gov/maven2/repository](https://nest.lbl.gov/maven2/repository)

---
