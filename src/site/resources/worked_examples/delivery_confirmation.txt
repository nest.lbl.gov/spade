# The following should be pasted into a terminal (not run as a script)
#   as the commands actually are run in various sessions and a script
#   will not transfer commands that change a session into that session.

if [ "X" = "X${SPADE_VERSION} ] ; then
    echo "Please set the environmental variables before proceeding."
else
    echo "It is okay to proceed"
fi

    <neighborhood>
        <neighbor>
            <name>spade.zero@localhost</name>
            <urls>
		        <confirmables>http://localhost:8080/spade/local/report/timestamps/confirmable</confirmables>
            </urls>
        </neighbor>
    </neighborhood>


docker exec -i -t -u jboss tour_spade \
    cp spade_apps/spade-${SPADE_VERSION}.war \
    wildfly/standalone/deployments/spade.war

docker exec -i -t -u jboss tour_spade \
    spade_apps/spade-cli delivery_scan

docker exec -i -t -u jboss tour_spade \
    spade_apps/spade-cli unverified

docker exec -i -t -u jboss tour_spade \
    spade_apps/spade-cli verified

