# Site Map for SPADE #

_Note:_`>` means a GET method done via the `report` URL, which `<` means a POST method done via the `command` URL)

    ---+----  "" : getApplication
       |
       +---+ activity
       |   |
       |   +---+ name
       |       |
       |       +---> resume : resumeActivity
       |       |
       |       +---< status : getActivityStatus
       |       |
       |       +---> suspend : suspendActivity
       |
       +---+ application : getApplicationStatus
       |   |
       |   +---> resume : resumeApplication
       |   |
       |   +---< status : getActivityStatus
       |   |
       |   +---> suspend : suspendApplication
       |
       +---+ bundle
       |   |
       |   +---+ {bundle}
       |       |
       |       +---< placement : getPlacement
       |       |
       |       +---< timestamps : getTimeStampsByBundle *
       |
       +---+ bundles
       |   |
       |   +---> ingest : ingest
       |
       +---+ delivery
       |   |
       |   +---> {neighbor}
       |
       +---+ scan
       |   |
       |   +---> delivery : deliveryScan
       |   |
       |   +---> inbound : inboundScan
       |   |
       |   +---> local : localScan
       |   |
       |   +---> redispatch : redispatchScan
       |
       +---+ ticket
       |   |
       |   +---+ {ticket}
       |       |
       |       +---< timestamps : getTimeStampsByTicket
       |
       +---+ tickets
       |   |
       |   +---> redispatch : redispatch
       |   |
       |   +---+ reship
       |       |
       |       +---> {outbound} : reship
       |
       |
       +---+ timestampers
       |   |
       |   +---< {stamper} : getTimeStampsByStamper
       |
