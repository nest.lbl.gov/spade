# Problem Files #


## Overview ##

When a Trask encounters a problem such that is can not continue, it terminated the current instance of the workflow and then the `failed` method of the `IngestTermination` class is invoked. One of the tasks of this method is to move all files associated with the problem out of the main cache and into  new, unique, area thus maintaining only _acvtive_ files in the the main cache.


