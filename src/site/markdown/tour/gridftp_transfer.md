# Setting up SPADE to use `GridFTP` for its transfers #


## Overview ##

This scenario show how to set up both shipping and receiving SPADE instances so that they can use `globus-url-copy`, a.k.a `GridFTP`, to transfer their file.


## Preparing a SPADE to Ship Files Using `GridFTP` ##

The current implementation of GridFTP transfers uses the `ssh` control channel mechanism. Therefore much of the preparation needed for GridFTP follows that of [`scp`](scp_transfer.html) but will be repeated in detail here in order to avoid confusion where there are differences. For example, by convention GridFTP uses a different `ssh` key than the `scp` transfer as each key is bound at the receiving end to a particular command that is valid only for that protocol.

The following commands create a suitable `ssh` key pair for GridFTP. (*Note* This pair is set up without a passphrase as executing up GridFTP transfers using a passphrase is beyond the scope of this tour.)

    docker exec -i -t -u jboss tour_spade /bin/bash
    ssh-keygen -b 4096 -N '' -C "Grid Transfers from ${HOSTNAME}" \
        -f ${HOME}/spade_svc/.ssh/${HOSTNAME}_gridftp.transfer
    cat >> ${HOME}/spade_svc/.ssh/config << EOF
    
    Host spade.zero.gridftp
        IdentityFile ${HOME}/spade_svc/.ssh/${HOSTNAME}_gridftp.transfer
        HostName ${HOST_FQDN}
        HostKeyAlias spade.zero
        User ${HOST_USER}
        UserKnownHostsFile ${HOME}/spade_svc/.ssh/known_hosts
    EOF
    chmod 644 ${HOME}/spade_svc/.ssh/config
    cat ${HOME}/spade_svc/.ssh/${HOSTNAME}_gridftp.transfer.pub
    exit


## `.ssh/config` Limitation ##

Currently `GridFTP` does not allow you to specified the `config` file that `ssh` will use. Therefore in order for `GridFTP` to work you will need to copy the `spade_svc/.ssh` directory and its contents to the standard location using the following commands.

    docker exec -i -t -u jboss tour_spade /bin/bash
    rm -fr ${HOME}/.ssh
    cp -r ${HOME}/spade_svc/.ssh ${HOME}/.ssh
    exit


## Preparing a SPADE to Receive Files Using `GridFTP` ##

On the receiving node you now need to install the shipping node's public key, along with the necessary configuration details to make sure that the `ssh` channel SPADE is using can only be used for transfers into the expected receiving area. The following commands do all of this. (*Note* The result of the last line above is the text you will need to paste into the file below.)

    cat > ${HOME}/tmp.pub << EOF
    <paste in the results of the "cat ${HOME}/.ssh/${HOSTNAME}_gridftp.transfer" command
        run on the shipping node>
    EOF

    read PUBLIC_KEY < ${HOME}/tmp.pub
    mkdir -p ${HOME}/.ssh
    chmod 700 ${HOME}/.ssh
    echo "no-agent-forwarding,no-port-forwarding,no-pty,no-user-rc,no-X11-forwarding \
    ${PUBLIC_KEY}" >> ${HOME}/.ssh/authorized_keys
    chmod 644 authorized_keys
    unset PUBLIC_KEY
    rm ${HOME}/tmp.pub


## Testing the `GridFTP` connection ##

Before SPADE uses the `ssh` connection for transfers it is advisable to run it once by hand. Not only does this check that the credentials have been set up correctly, but also it provides an opportunity to store the remote host's key. This can be done by running the following command on the shipping SPADE's node.

    docker exec -i -t -u jboss tour_spade \
        /usr/bin/globus-url-copy -v -vb -p 5 \
        file:/opt/jboss/spade_svc/.ssh/config \
        sshftp://pdsf.nersc.gov/spade.zero/receiving/loopback/junk

If that works successfully then the following will clean up the now useless file.

    rm ${HOME}/spade.zero/receiving/loopback/junk 

We are now ready to configure SPADE to use this transfer channel.


## Updating the SPADE configuration ##

In order to get an outbound transfer to use the `scp` channel you need to specifiy the `SCPTransfer` class as the class for the transfer. Therefore the following element should be added to `spade.xml` between the existing `outboundTransfer` element and the `inboundTransfer` one.

    <outbound_transfer>
        <name>Loopback using SCP</name>
        <neighbor>spade.zero@localhost</neighbor>
        <location>spade.zero:spade.zero/receiving/loopback</location>
        <class>gov.lbl.nest.spade.services.impl.SCPTransfer</class>
    </outbound_transfer>

*Note* All `outbound_transfer` declarations for a single neighbor may deliver to that same location as the receiving SPADE does not care which transfer protocol was used. However it does mean that for any given registration _only one_ `outbound_transfer` per neighbor must be declared.


## Adding a Registration ##

In order to use this new transfer protocol we need to have a regisration that uses it. Rather than editing an existing one, which is perfectly acceptable, for the purposes of this tour we'll create a new one. The following commands create that new regisration.

    mkdir -p ${HOME}/spade.zero/spade/registrations/local
    cat > ${HOME}/spade.zero/spade/registrations/local/scp_transfer.3.xml << EOF
    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <registration>
        <local_id>scp.transfer.3</local_id>
        <drop_box>
            <location>
                <directory>~/dropbox</directory>
            </location>
            <pattern>.*.scp</pattern>
            <mapping>gov.lbl.nest.spade.tour.SCPTransferLocator</mapping>
        </drop_box>
        <warehouse>false</warehouse>
        <outbound_transfer>Loopback using SCP</outbound_transfer>
    </registration>
    EOF

Once the configuration has been updated and the registration exists you will need to redeploy SPADE in order for it to pick up both of these changes. This is done with the same command are before.

    docker exec -i -t -u jboss tour_spade \
        cp spade_apps/spade-${SPADE_VERSION}.war \
        wildfly/standalone/deployments/spade.war


## Transferring a File using SCP ##

You are now ready to use the `scp` transfer channel. As before this is a matter of creating data and semaphore files and telling SPADE to handle this bundle. The following commands do just that.

    cat > ${HOME}/spade.zero/dropbox/loopback/tour.3.data << EOF
    This data file should be shipped via the "Loopback using SCP" outbound
    transfer.
    EOF
    touch ${HOME}/spade.zero/dropbox/loopback/tour.3.scp
    docker exec -i -t -u jboss tour_spade \
        spade_apps/spade-cli local_scan
    docker exec -i -t -u jboss tour_spade \
        spade_apps/spade-cli inbound_scan
    docker exec -i -t -u jboss tour_spade \
        spade_apps/spade-cli delivery_scan
