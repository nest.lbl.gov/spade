# Customizing Metadata #


## Overview ##

This scenario explains how to configure a SPADE deployment customize the metadata the is associated with a bundle.


## Updating the SPADE configuration ##

By default SPADE assumes there is no metadata associated with a bundle. While this is clearly the most universal case, normally one wants some metadata associated with the file in order to customize its handling when it is delivered. Currently the configuration of which Metadata is used is _not_ specified in the SPADE configuration, but rather its is handled by the JEE Enterprise Bean mechanism. (It is hoped to be able to put these into the configuration in a later release.)


## Implementing Metadata Customization ##

Customization of metadata is done by providing two new classes when building the SPADE `WAR` file.

- one class implements the `Metadata` interface to provide a XML read/writable properties that make up the metadata.
- the other class extends the `MetadataImplManager` class in order to declare the new `Metadata` implementation to the application.

In the following the `Metadata` implementation is broken into two parts, the `PathMetadata` interface that declares the interface to the customized metadata properties and the `PathMetadataImpl` class that implements it. (While this could be done in a single class this way property access in kept separate from the XML annotations.) Finally the `PathMetadataManager` class declares the `PathMetadataImpl` to the application.

All three files are bundled into the default SPADE distribution.

### Custom Metadata Interface and Classes ###

A customized metadata interface simply declares the properties that that metadata will container. In the case of `PathMetadata`, it declares a single method:

    public interface PathMetadata extends
                                  Metadata {

        public List<ExternalFile> getPaths();
    
    }

The `PathMetadataImpl` class implements this as an XML enables class:

    @XmlRootElement(name = "path_metadata")
    public class PathMetadataImpl implements
                                  PathMetadata {

        private List<ExternalFile> paths;

        @Override
        @XmlElement(name = "path")
        public List<ExternalFile> getPaths() {
            return paths;
        }

        public void setPaths(final List<ExternalFile> paths) {
            this.paths = paths;
        }
    }

Finally the `PathMetadataManager` class simply specializes it superclass by calling its constructor:

    @Alternative
    public class PathMetadataManager extends
                                     MetadataImplManager {

        public PathMetadataManager() {
            super(PathMetadataImpl.class);
        }
    }


## Deployment for Custom Metadata ##

To configure SPADE to use the alternate classes you need to run the following commands.

    docker exec -it tour_spade bash
    cd ~/wars
    mkdir -p WEB-INF
    cat > WEB-INF/beans.xml << EOF
    <?xml version="1.0" encoding="UTF-8"?>
    <beans xmlns="http://xmlns.jcp.org/xml/ns/javaee"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/beans_1_1.xsd"
           bean-discovery-mode="all">
        <alternatives>
            <class>gov.lbl.nest.spade.metadata.impl.PathMetadataManager</class> 
        </alternatives>
    </beans>
    EOF
    jar -uf ~/wars/spade-${SPADE_VERSION}.war WEB-INF/beans.xml
    exit

These will change the `WAR` file so that the application will now use the `PathMetadataManager` class as is `MetadataManager`.

Of course SPADE must be redeployed to pick up this change>

    docker exec -it tour_spade \
        cp wars/spade-${SPADE_VERSION}.war \
        /opt/wildfly/standalone/deployments/spade.war


## Creating data and semaphore files ##

You can now run SPADE using the custom metadata. In this case the metadata is provided in the semaphore file.

*Note:* The semaphore file is created under a temporary name so SPADE does not see it until it is complete.

    cat > ${HOME}/spade.zero/dropbox/loopback/tour.4.data << EOF
    This data file should use custom metadata
    EOF

    cat > ${HOME}/spade.zero/dropbox/loopback/tour.4.tmp << EOF
    <path_metadata>
        <path>/opt/jboss/data/spade/dropbox/loopback/tour.4.data</path>
    </path_metadata>
    EOF
    
    mv ${HOME}/spade.zero/dropbox/loopback/tour.4.tmp ${HOME}/spade.zero/dropbox/loopback/tour.4.sem

    docker exec -it tour_spade bash -l -c "spade-cli local_scan"


## Viewing the new metadata ##

The following command will show the contents of the metadata file:

    xmllint -format $(find ${HOME}/spade.zero/warehouse -name "tour.4.meta.xml")

As you can see, this now includes the `path` associated with the bundle.

[NEXT STEP](customizing_placement.html)
