# Confirming the Successful Delivery of a Transfer #


## Overview ##

This scenario show how the delivery of from one SPADE to another is confirmed.


## Updating the SPADE configuration ##

Now that SPADE has sent a transfer to another SPADE (or itself) it must find out whether that transfer succeeded or not. It does this by interrogating the receiving SPADE to see if the transfer has been accepted, and it also compares checksums to make sure the transfer was not corrupted in transit. The interrogation is done by a RESTful call so the appropriate URI for this must be specified to the sending SPADE. This is done by adding a `neighborhood` element, containing one or more `neighbor` elements, to the `spade.xml` configuration file. Using your favorite editor, you will need to add the following element to that file between the `inbound_transfer` and `warehouse` elements.

    <neighborhood>
        <neighbor>
            <name>SPADE@${TOUR_CONTAINER_HOSTNAME}</name>
            <urls>
                <api>http://localhost:8080/spade/local/report/</api>
                <confirmables>timestamps/confirmable?max=8</confirmables>
            </urls>
        </neighbor>
    </neighborhood>


This element declares how this SPADE can talk to any neighboring SPADEs. Each `neighbor` contains elements as follows:

-    `name` : this provides a local name by which the other SPADE be referred. This should match any `inbound_transfer/neighbor` element referring to this neighbor, i.e. by `SPADE@<docker container name>` and not `spade.zero@localhost`.

-   `urls` : this declares a collection of RESTful URLs that can be use to communicate with the neighbor. In the example here only the `confirmables` element is declared and this URL will return a list of transfers that can be confirmed.

Again, to pick up the new configuration SPADE needs to be redeployed.

    docker exec -i -t -u jboss tour_spade \
        cp spade_apps/spade-${SPADE_VERSION}.war \
        wildfly/standalone/deployments/spade.war


## Telling SPADE to Confirm Transfers ##

The shipping SPADE can check which transfers have been successfully delivered by using the following command.

    docker exec -it tour_spade bash -l -c "spade-cli delivery_scan"

After that you can check that the transfer has been verified by running the following command again and seeing that the bundle is no longer listed.

    docker exec -it tour_spade bash -l -c "spade-cli unverified"

While the following command will show it has been successfully verified.

    docker exec -it tour_spade bash -l -c "spade-cli verified"

[NEXT STEP](analyzing_data.html)
