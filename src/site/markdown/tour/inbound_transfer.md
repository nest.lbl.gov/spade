# Receiving a File to another SPADE instance #


## Overview ##

This scenario show how a file from another SPADE instance is handled by the receiving SPADE which will then place it in the local warehouse.


## Updating the SPADE configuration ##

In order for a SPADE instance to receive inbound transfers, the locations of where those files will appear must be specified. This is done by adding one or more `inbound_transfer` elements to the `spade.xml` configuration file. Again using your favorite editor, you will need to add the following element to that file between the `outbound_transfer` and `warehouse` elements.

    <inbound_transfer>
        <name>Loopback from Self</name>
        <neighbor>SPADE@${TOUR_CONTAINER_HOSTNAME}</neighbor>
        <location>~/data/external/spade.zero/receiving/loopback</location>
    </inbound_transfer>

This element declares that another spade (or for the sake of this tour, the same spade) will be delivering files to the specified directory. The child elements of this element are as follows:

-    `name` : this is the name by which this transfer will be referred to in registrations.

-   `neighbor` : this provides a local name by which the other SPADE be referred. By convention this value takes the form of `<instance>@<host>`. (If there are both inbound and outbound transfers to the same SPADE this name should be the same in both the `inbound_transfer` and `outbound_transfer` elements.) As with the [outbound transfer](outbound_transfer.html), this should be set to the same value as the `assembly/name` element. This will be the docker container name, *not* `localhost` as should here.

-   `location` : this declares a location where this SPADE will look for files delivered via this transfer. This is normally a path on the local file system.

As in previous scenarios, a change of configuration need a redeployment in order for it to be picked up. This is done with the same command are before.

    docker exec -it tour_spade \
        cp wars/spade-${SPADE_VERSION}.war \
        /opt/wildfly/standalone/deployments/spade.war


## Telling SPADE to Handle an Inbound Transfer ##

In the case of inbound transfers, SPADE picks up the incomning bundles using the `inbound_scan` command, as shown here.

    docker exec -it tour_spade bash -l -c "spade-cli inbound_scan"

The other terminal showing the `tour_spade` container's output wiil display the bundle's progress through the receiving workflow, once again ending with the _finished_ task.

You can check the placement of the inbound bundle using the `placed` command use in the eariler, *local warehouse* scenario.

    docker exec -it tour_spade bash -l -c "spade-cli placed"

Also, as shown in that scenario, you and see that the file has been place in the receiving warehouse with the following command.

    find ${HOME}/spade.zero/warehouse -name "*tour.2.*"

Along with the metadata file that you can inspect this file using the following command.

    xmllint -format $(find ${HOME}/spade.zero/warehouse -name "tour.2.meta.xml")

Now the receiving SPADE has handled the inbound bundle, you can check back to the shipping SPADE to see if the delivery has been verified with the following.

    docker exec -it tour_spade bash -l -c "spade-cli unverified"

As you will see, it is still unverified, and the next scenario show how that verification may be done.

[NEXT STEP](delivery_confirmation.html)
