# SPADE configuration #

The main settings for SPADE are declared in the file that is usually found at the following location:

    ~/spade/spade.xml

When SPADE is deployed this file is read and acted on accordingly. Whenever the file is modified the application needs to be redeployed in order to use the new configuration.

If SPADE is deployed without a configuration file, then a default one is created and sort in the file location. The default, i.e. minimal, file looks like the following.

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <spade_config>
        <assembly>
            <name>SPADE@${HOSTNAME}</name>
        </assembly>
        <warehouse>
            <root_path>${HOME}/warehouse</root_path>
        </warehouse>
        <cache>
            <root>${HOME}/spade/cache</root>
            <minimum units="GB">1</minimum>
        </cache>
    </spade_config>

The elements of this file, long with all of the other valid elements are discussed in the rest of this document.


## The `spade_config` Document ##

The root element of a spade configuration is the `spade_config` element. This may contain any of the following elements:

    * `assembly` - information about the SPADE workflows.
    * `registration_paths` - where to find _local_ and _inbound_ registrations.
    * `outbound_transfer` (multiple) - routes over which file can be transfer to other SPADE instances.
    * `inbound_transfer`  (multiple) - places where inbound transfer will be found from other SPADE instances.
    * `neighborhood` - information about adjacent SPADE instances.
    * `duplication`  (multiple) - local places, outside SPADE's management, to place duplicates of files.
    * `warehouse` - information about the local warehouse.
    * `archive` - information about the local tertiary storage.
    * `cache` - information about the local cache.


## The `assembly` Element ##

The `assembly` element declares information about workflows that are use within SPADE. It may be composed of any of the following elements:

    * `name` - the name that will be used when displaying information about this instance.
    * `ticket_limit` - the maximum number of workflow instances that can exist.
    * `thread_limit` (multiple) - the maximum number of threads allocated to a workflow.
    * `activity` (multiple) - configurations for task in the SPADE workflow.


### The `name` Element ###

The `name` element declares the name that will be used when displaying information about this instance. The convention is that this is also the name used in `neighbor` elements elsewhere when declaring inbound and outbound transfers, though this is not a requirement.


### The `ticket_limit` Element ###

The `ticket_limit` element limits the number of tickets, i.e. workflow instances, that can exist in the application at any point in time. This limit is used to avoid overuse of resources such as cache spade, whose exhaustion would cause the application to stall. If this element is missing or set to "0" or less then there is no effective limit.


### The `thread_limit` Element ###

The `ticket_limit` element limits the number of threads a workflow can use at any point in time. This limit is used to avoid overuse of resources. The `workflow` attribute of this element limits the specified workflow to the specified number of thread. If the `workflow` attribute is missing then the limit is applied to the `INGEST` workflow.  If this element is missing or set to "0" or less then the effective limit is one.


### The Multiple `activity` Elements ###

Each `activity` element allows configuration of an individual task that appear in the SPADE workflows. In default configuration file above all tasks use their default settings as no `activity` elements appear in the `assembly` one. Each `activity` element may be composed of some of the following elements:

    * `name` (required) - the name of the task to be configured.
    * `threads` - the maximum number of threads that can execute the task, the default is 1.
    * `storage_factor` - not currently used.
    * `init-param` (multiple) - parameters values to used by the task. (The list of value parameters for each task is detailed in that task's reference.)
    * `policy` - the Java class that is used in implment some part of the task's behavor (The list of know policies for each task is detailed in that task's reference.)


#### The multiple  `init-param` elements

Each `init-param` element contains a key-value pair used to define the value of the parameter to used. Each element contains exactly one each of the following elements.

    * `param-name` - the name of the parameter whose value is being set.
    * `param-value` - the value to be set for that parameter.


#### The `policy` Element ####

The `policy` element is used to declare which Java class should be used in implement some part of the task. The elemetn may be composed of some pf the following elements:

    * `class` (required) - name of the class that implements a polciy appropriate for the task.
    * `init-param` (multiple) - parameters values to used by the policy

The `init-param` elements are the type as those used by the `activity` element (see above.)


## The `registration_paths` Element ##



## The multiple `outbound_transfer` Elements ##

The SPADE configuration file can contain zero, one or many `outbound_transfer` element that define a destination and transport protocol that can be used to transfer a bundle to another SPADE instance. Here is an example `outbound_transfer` element that is used in the simple SPADE tour.

    <outbound_transfer>
        <name>Loopback to Self</name>
        <description>Send bundles back to itself use the cp command</description>
        <neighbor>spade.zero@localhost</neighbor>
        <location>localhost:~/receiving/loopback</location>
        <class>gov.lbl.nest.spade.services.impl.LocalhostTransfer</class>
    </outbound_transfer>

The `name` element is used to refer to this transfer configuration within the context of a registration.

The `description` element is optional and it purely there to provide details about the  transfer so users can understand what it does.

The `neighbor` element is use as a unique label to one of the neighboring SPADE instances. If more that one `outbound_transfer` references the same neighboring instance then they should have the same value for this element. This value may also be referenced in the `neighborhood` element discussed below.

The `location` element specifies where, on the remote SPADE, to put the files being transferred. The syntax of this element needs to match the syntax required by the class implementing the transfer protocol, which is the next element.

The `class` element specifies the class implementing the transfer protocol. The class specified here must be an implementation of the `FileTransfer` interface. In the example above the `LocalhostTransfer` class simply moves files around the local files systems in as an efficient manner as possible. Other build in implementations are discussed elsewhere.


## The Multiple `inbound_transfer` Elements ##

If a SPADE instance is expecting files to be delivered from remote SPADEs those inbound transfers need to be declared in and `inbound_transfer` element in the configuration file. Again, using the SPADE tour, here is and example declaration.

    <inbound_transfer>
        <name>Loopback from Self</name>
        <description>Bundles sent using the loopback interface</description>
        <neighbor>spade.zero@localhost</neighbor>
        <location>~/receiving/loopback</location>
    </inbound_transfer>

The first three elements are identical to in use to the first three elements of an `outbound_transfer` element. The final `location` element is the local directory where the inbound transfer have been put by one of more `outbound_transfer` running on the remote SPADE. Normally, if there is more than one `outbound_transfer` to the same SPADE but using different protocols, they deliver their bundles to the same location. This means normally there is only one `inbound_transfer` for each neighboring SPADE. However if the remote SPADE delivers bundle to more than one location, because it has multiple `outbound_transfers` that have different `locations`, there needs to be an `inbound_transfer` for each location to which bundles are being delivered.


## The `neighborhood` Element ##

The SPADE configuration file may container a `neighborhood` element. This element allow for features of each neighbor to be declared in one place, while the neighbor's names may be used in many places, such as inbound and outbound transfers.

    <neighborhood>
        <neighbor>
            <name>spade.zero@localhost</name>
            <urls>
            <confirmables>http://localhost:8080/spade/local/report/timestamps/confirmable</confirmables>
            </urls>
        </neighbor>
    </neighborhood>

## The Multiple `duplication` Elements ##


## The `warehouse` Element ##

The `warehouse` element contains just the `root_path` element that declares the path to a directory under which all of the warehouse entries will be stored.


## The `archive` Element ##


## The `cache` Element ##

