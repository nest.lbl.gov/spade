# Registrations #

When SPADE is managing a bundle, i.e. one or more files that are considered an atomic element, its handling of that bundle is defined by the *Registration* with which it is associated. Two types of registrations exist:

*   "local" - This type of Registration handles local files that have not yet been handled by a SPADE instance.
*   "inbound -  - This type of Registration handles bundles that have been delivered to this instance of SPADE.


## Local Registrations ##

All local Registrations are normally found in or below the following directory:

    ~/spade/registrations/local

and take the form of a XML file. In the next few sections the main features of the XML file will be reviewed.


### Dropbox ###

The existence of a file, known as a semaphore file, is used to signal SPADE that a bundle is ready to be managed. Each semaphore file is associated with one registration and that association is deduced by the location and name of the semaphore file. To associate different types of file with different registrations one or both of those quantities must be different.

Here is an example of the simplest registration.

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <registration>
        <local_id>local_warehouse.1</local_id>
        <drop_box>
            <location>
                <directory>dropbox/local_warehouse</directory>
            </location>
            <pattern>.*.sem</pattern>
        </drop_box>
    </registration>

The `local_id` element is used to refer to this registration within the context of its SPADE instance. It should be descriptive of the purpose of the registration and must be *unique* among both local and inbound registrations.

The `dropbox` element defines a mapping between the registration and its associated semaphore files. Details about the semaphore file are dealt with elsewhere. In this example the Registrations is associated with any semaphote file in the `dropbox/local_warehouse` directory provided is has a suffix `.sem`.

The example registration above will put data files in the warehouse provided they are:

*   in the same directory as the semaphore file,
*   they have the same root name but their suffix is `.data`.

This behavior can be simply changed by provideing the class name of an implementation of the `DataLocator` interface as the value of a `mapping` sub-element of the `dropbox` element. The details of how to create such an implementation are discussed elsewhere.


### Warehousing ###

By default all files are placed in the local warehouse. If a bundle should not be placed there, normally because it is simply being transferred to another SPADE, then a `warehouse` element must be included in that bundle's Registration with a value of `false`.


### Ownership ###

SPADE supports two types of ownership pattern. If a Registration has no `owner` element or that element is `true` then SPADE will copy the file(s) that make up the bundle into its cache after which it will delete the original files. On the other hand if there is an `owner` element whose value is `false` then SPADE will *not* delete the original files and it will be up to some external mechanism to delete them.


### Outbound Transfers ###

Apart from putting a bundle in the local warehouse, SPADE can transfer a bundle to one or more other SPADE instances so that is can be placed in their warehouses too. The transfer of a bundle is specified by one or more `outbound_transfer` elements in the appropriate Registration. The value of those elements should be the names of the `outbound_transfer` elements declared in the SPADE configuration file.

Putting together all of the above, a more complex registration can be developed.

    <?xml version="1.0" encoding="UTF-8" standalone="yes"?>
    <registration>
        <local_id>remote_warehouse.5</local_id>
        <drop_box>
            <location>
                <directory>dropbox/remote_warehouse</directory>
            </location>
            <pattern>.*.tem</pattern>
            <mapping>gov.lbl.nest.spade.locators.LemToTgzLocator</mapping>
        </drop_box>
        <owner>false</owner>
        <warehouse>false</warehouse>
        <outbound_transfer>Ship Elsewhere</outbound_transfer>
    </registration>

This registration ships files that end with `.tgz` in the `dropbox/remote_warehouse` to another SPADE as specified by the `Ship Elsewhere` outbound transfer defined for the SPADE instance. The original files will not be deleted by SPADE and will not be placed in the local warehouse.