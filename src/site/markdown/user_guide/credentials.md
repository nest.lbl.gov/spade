# Credentials for SPADE transfers #

The credentials needs to complete transfers depend on the class used for the transfer. This section will go though all of the known transfer class and explain their credential when running directly on a system ("natively"), and when running in a container ("containerized secrets").


## The `SCPTransfer` class ##

The `SCPTransfer` class use the `scp` to effect the file transfers and therefore needed the necessary `ssh` infrastructure that, at a bare minimum is a `ssh` key file but more usually a key file plus a `ssh_config` file.

The default ssh-key used by `scp` is normally contained in the file `~user/.ssh/id_rsa`, however this can be customized as descibed below. Similarly the default `ssh_config` file is `~user/.ssh/config` but again this can be or is customized as descibed below.


### Native customization ###

As data transfer is very particular `ssh` stream, it is recommended that the `SCPTransfer` class use its own ssh key. This allows for additional security to be applied at the receiving end (this is documented elsewhere).

The ssh key is customized using the `ssh_config` file that the class will be using. (How to select that file is covered later in this section.) To use non-default key, `scp_transfer_key` is the conventional replacement name,

...


### Containerized Secrets ###

When SPADE is run in a containerized environment, it set up to use the typical "secrets" infrastructure to customize the `ssh` credentials. In this case the `ssh_config` file is normally set in the image  to be `/opt/jboss/.ssh/config` by writing the appropriate `scp_transfer_ssh_config` file into the `spade.war` WAR file when image is created.

That file, `/opt/jboss/.ssh/config`, will contain the following

    Host *
        User spade
        IdentityFile /opt/jboss/services/run/secrets/scp_transfer_key

although this content can be overwritten by a later `Dockerfile`. (The most common reason for this is to change the `User` field.)

The idea here is that the `scptransfer_key` is specified in the secrets system that deployed the image and this secret's value is then mapped to `/opt/jboss/services/run/secrets/scptransfer_key` inside the container at deployment.

