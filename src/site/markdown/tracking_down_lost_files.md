# Tracking Down Lost File #

## What File is Lost ##

Look up the files missing from an incomplete run.

    RUN=5587
    curl http://localhost:8080/sentry/local/report/lz/run/${RUN}/missing/Missing%20USDC%20Placed%20Files | xmllint -format -

## Is it stuck in Receiving? ##

    ls -lrt /global/cfs/cdirs/lz/spade/receiving/surf | grep ${LOST_BUNDLE}
    
    source /opt/wildfly/extras/local.env
    gfal-ls -l gsiftp://gfe02.grid.hep.ph.ic.ac.uk/pnfs/hep.ph.ic.ac.uk/data/lz/spade/dropbox/usdc | grep ${LOST_BUNDLE}


### Is it being blocked? ###

    spade-cli bundles blocked | grep ${LOST_BUNDLE}

### Is so, Unblock it and rescan to pick it up ###

    spade-cli bundles -b ${LOST_BUNDLE} unblock   
    spade-cli application inbound_scan


## Was is Dispatched from Upstream? ##

    grep "${LOST_BUNDLE}" /lzdata/test/s-export1-lzd/lz-jee/app/wildfly/standalone/log/server.log

    grep "${LOST_BUNDLE}" /global/cfs/cdirs/lz/spin/prod/data/shared/lz-data/app/wildfly/standalone/log/server.log


### Is it waiting to be reshipped? ###

    ls -l /global/cfs/cdirs/lz/spin/prod/data/private/lz-data/app/spade/cache/reship/*/payload/*${LOST_BUNDLE}*
    spade-cli tickets -t 13010950 reship


## Was is a Problem File ##

    cd /global/cfs/cdirs/lz/spin/prod/data/private/lz-data/app/spade/cache/problems/
    ls -lrt */files/payload/*${LOST_BUNDLE}*


## Was it 

