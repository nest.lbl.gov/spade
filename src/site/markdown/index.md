# Overview of SPADE #

SPADE is a data management application. Its purpose is to manage data file both in a local warehouse and distribute them to remote warehouses.

This version (4.0) of SPADE is still in development. At the moment the following elements of this web site are available:

-   A [short tour](tour/introduction.html) of what SPADE can do and how it can do it.
-   A [User Guide](user_guide.html), which is still being fleshed out.
