# User Guide #

This section of the web site collects together the set of web pages that constitute a User's Guide on how you can used the various parts of SPADE to achieve your goals.

As this version (4.5) of SPADE is still in development, it is currently limited to the following sections:

-   A description of what goes into a [Registration](user_guide/registrations.html).
