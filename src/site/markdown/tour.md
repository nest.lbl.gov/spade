# A Tour of SPADE #

This section of the web site is a short tour of what SPADE can do and how it can do it.

As this version (4.0) of SPADE is still in development, it is currently limited to the following sections:

-    An [Introduction](tour/introduction.html) to the tour.
-    An example of [storing data in the local warehouse](tour/local_warehouse.html).
-    An example of [sending out data to a SPADE instance](tour/outbound_transfer.html).
-    An example of [receiving data from a SPADE instance](tour/inbound_transfer.html).
-    An example of [confirming the delivery of data to a SPADE instance](tour/delivery_confirmation.html).
-    An example of [analyzing data as it enters the local warehouse](tour/analyzing_data.html).
-    An example of [customizing the metadata associated with data](tour/customizing_metadata.html).
-    An example of [customizing the placement of data within the local warehouse](tour/customizing_placement.html).
-    An example of [finding data that is not placed in the "dropbox"](tour/non-dropbox_data.html).
-    An example of [doing data transfers using `scp`](tour/scp_transfer.html).
