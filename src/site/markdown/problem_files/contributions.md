# What Contributes Which Problem Files #


## Overview ##

When a Task encounters a "problem" all files associated with the problem are moved into a new, unique, directory. This document detail how the file being stored in that directory are located.


## Ingest Ticket ##

The primary source for locating which files are to be moved if the _Ingest Ticket_ handed to a Task. Any upstream Task will have added the appropriate file path to the ticket. Thus first and foremost the `failed` method of the `IngestTermination` class moves all those files into the appropriate directory. The following is a list of Tasks that contribute to the ticket and what paths they contibute.

-   __examiner__ : 
    *   internal semaphore file
    *   metadata file
    *   data delivery directory
-   __packer__
    *    packed file
-   __compressor__ : 
    *   compressed file

Therefore, any of these path that are non-null will have their files moved to their own problem area.


## Task Exceptions ##

If any of the tasks shown above encounter a terminal problem then their contribution to the Ingect Ticket will not yet have been made. Therefore each of those Tasks throws a specialized `Exception` that may contain their missing contributions to the ticket. The following are the specialized `Exception` classes.

-   `ExaminerException`
-   `PackerException`
-   `CompressorException`

