POSTGRES_PASSWORD=******** # the password of the 'postgres' account in the Postgres DB
POSTGRES_FORMAT=10.1
SPADE_VERSION=4.1.5       # the version number of the SPADE being used in this tour
SPADE_PASSWORD=********
SPADE_USER=remote

SPADE_ONE_SECRETS=${HOME}/spade.one/run/secrets
mkdir -p ${HOME}/spade.one/run/secrets
cat > ${SPADE_ONE_SECRETS}/postgres_password << EOF
${POSTGRES_PASSWORD}
EOF
cat > ${SPADE_ONE_SECRETS}/spade_password << EOF
${SPADE_PASSWORD}
EOF

mkdir -p ${HOME}/docker/env/
cat > ${HOME}/docker/env/remote_postgres.env << EOF
POSTGRES_PASSWORD_FILE=/run/secrets/postgres_password
POSTGRES_FORMAT=${POSTGRES_FORMAT}
SPADE_PASSWORD_FILE=/run/secrets/spade_password
SPADE_VERSION=${SPADE_VERSION}
SPADE_USER=${SPADE_USER}
EOF
chmod 600 ${HOME}/docker/env/remote_postgres.env

docker run \
    --name remote_postgres \
    --network=tour_network \
    --volume ${HOME}/spade.one/run:/run \
    --env-file ${HOME}/docker/env/remote_postgres.env \
    -d postgres
docker exec -i -t remote_postgres /bin/bash
apt-get -y update && apt-get -y upgrade
apt-get -y install curl wget
dpkg-reconfigure tzdata # Set this to be you local timezone

cat > ~/.pgpass << EOF
:::${SPADE_USER}:$(< ${SPADE_PASSWORD_FILE})
EOF
chmod 600 ~/.pgpass 
exit

docker exec -i -t remote_postgres /bin/bash
wget -O spade_initialize_psql.sh \
http://nest.lbl.gov/projects/spade/resources/${SPADE_VERSION}/bash/initialize_psql.sh
. spade_initialize_psql.sh
rm spade_initialize_psql.sh
exit

export HOST_UID=$(id -u ${USER})
export HOST_GID=$(id -g ${USER})
export HOST_FQDN=$(hostname -f)
cat > ${HOME}/docker/env/remote_spade.env << EOF
POSTGRES_HOST=remote_postgres
SPADE_PASSWORD_FILE=/opt/jboss/services/run/secrets/spade_password
SPADE_VERSION=${SPADE_VERSION}
SPADE_USER=${SPADE_USER}
HOST_UID=${HOST_UID}
HOST_GID=${HOST_GID}
HOST_FQDN=${HOST_FQDN}
HOST_USER=${USER}
EOF
chmod 600 ${HOME}/docker/env/remote_spade.env

SPADE_OPTIONS="-v ${HOME}/spade.one/wildfly/extras:/opt/wildfly/workdir/extras \
    -v ${HOME}/spade.one/wildfly/standalone:/opt/wildfly/workdir/standalone \
    -v ${HOME}/spade.one/spade:/opt/jboss/services/spade \
    -v ${HOME}/spade.one/cache/spade:/opt/jboss/cache/spade \
    -v ${HOME}/spade.one/dropbox:/opt/jboss/data/spade/dropbox \
    -v ${HOME}/spade.one/warehouse:/opt/jboss/data/spade/warehouse \
    -v ${HOME}/spade/shared/spade.one/receiving:/opt/jboss/data/spade/receiving \
    -v ${HOME}/spade/shared:/opt/jboss/data/external \
    --env-file ${HOME}/docker/env/remote_spade.env"

# The `cache` soft-link is only required to map SPADE's default locations
#     to container's volumes. The `spade.xml` can be updated to use the
#     container's volume directly.
mkdir -p ${HOME}/spade.one
(cd ${HOME}/spade.one ; \
    mkdir -p wildfly/extras wildfly/standalone spade cache/spade dropbox/spade.zero warehouse ; \
    ln -s /opt/jboss/cache/spade spade/cache )
mkdir -p ${HOME}/spade/shared/spade.one/receiving


docker run \
    --name remote_spade \
    --network=tour_network  \
    --publish 7080:8080 \
    --volume ${HOME}/spade.one/run:/opt/jboss/services/run \
    ${SPADE_OPTIONS} \
    -d lblnest/spade:${SPADE_VERSION}

docker exec -it remote_spade bash
cd ~/wars
mkdir -p WEB-INF
cat > WEB-INF/beans.xml << EOF
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://xmlns.jcp.org/xml/ns/javaee"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/beans_1_1.xsd"
       bean-discovery-mode="all">
    <alternatives>
        <class>gov.lbl.nest.spade.metadata.impl.PathMetadataManager</class> 
    </alternatives>
</beans>
EOF
jar -uf ~/wars/spade-${SPADE_VERSION}.war WEB-INF/beans.xml
exit

cat > ${HOME}/spade.one/spade/mount-point.properties << EOF
# Maps original mount points to locations in the warehouse
/opt/jboss/data/external/spade.zero/tour=alternate/placement
EOF


# The following resets the area as much as possible.
#   There are postgres files in ${HOME}/spade.one/run that need higher
#   privileges to delete.

docker container rm -f remote_spade
docker image rm lblnest/spade:${SPADE_VERSION}
rm -fr ${HOME}/spade
rm -fr ${HOME}/spade.one
rm -f ${HOME}/docker/env/remote_spade.env
docker container rm -f remote_postgres
docker image rm postgres
rm -f ${HOME}/docker/env/remote_postgres.env
docker network rm tour_network
