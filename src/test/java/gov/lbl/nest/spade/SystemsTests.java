package gov.lbl.nest.spade;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.jee.testing.XmlTestUtility;
import gov.lbl.nest.spade.config.Assembly;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.ejb.KnownLocalRegistration;
import gov.lbl.nest.spade.ejb.MockEntityManager;
import gov.lbl.nest.spade.lazarus.JavaInstanceFactoryImpl;
import gov.lbl.nest.spade.lazarus.JavaInstanceFactoryImpl.ExecutionEnvironment;
import gov.lbl.nest.spade.lazarus.MockWorkflowsManager;
import gov.lbl.nest.spade.metadata.impl.MetadataImplManager;
import gov.lbl.nest.spade.registry.LocalRegistration;
import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.rs.Bundles;
import gov.lbl.nest.spade.services.Spade;
import gov.lbl.nest.spade.services.TicketManager;
import gov.lbl.nest.spade.services.impl.CacheManagerImpl;
import gov.lbl.nest.spade.services.impl.LoadedRegistrationsUtilities;
import gov.lbl.nest.spade.services.impl.MockBookkeeping;
import gov.lbl.nest.spade.services.impl.MockNeighborhoodManager;
import gov.lbl.nest.spade.services.impl.MockRegistrationManager;
import gov.lbl.nest.spade.services.impl.MockRetryManager;
import gov.lbl.nest.spade.services.impl.MockTicketManager;
import gov.lbl.nest.spade.services.impl.MockVerificationManager;
import gov.lbl.nest.spade.services.impl.RetryManagerImpl;
import gov.lbl.nest.spade.services.impl.SpadeImpl;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import jakarta.xml.bind.JAXBException;

/**
 * This class used Junit 5 to test the SPADE system behaves as expected.
 *
 * @author patton
 */
public class SystemsTests {

    @Nested
    class Loopback {

        @Test
        @DisplayName("Inbound from Self")
        void inboundFromSelf() {

        }

        @Test
        @DisplayName("Outbound to Self")
        void outboundToSelf() throws Exception {

            final String OUTBOUND_INGEST_BUNDLES = "loopback/outbound_ingest_bundles.xml";

            final String OUTBOUND_LOOPBACK_REGISTRATION = "registrations/local/loopback.2.xml";

            final String OUTBOUND_TRANSFER_BPMN = "lazarus/outbound_transfer.bpmn";

            final String OUTBOUND_TRANSFER_NAME = "Loopback to Self";

            final OutboundTransfer OUTBOUND_TRANSFER_OBJECT = new OutboundTransfer(OUTBOUND_TRANSFER_NAME,
                                                                                   "files/receiving/loopback",
                                                                                   SELF_NAME);

            /*
             * Prepare SPADE JEE Elements.
             */
            @SuppressWarnings("serial")
            final List<String> resources = new ArrayList<>() {
                {
                    add(OUTBOUND_LOOPBACK_REGISTRATION);
                }
            };
            @SuppressWarnings("serial")
            final Map<String, OutboundTransfer> outboundTransfers = new HashMap<>() {
                {
                    put(OUTBOUND_TRANSFER_NAME,
                        OUTBOUND_TRANSFER_OBJECT);
                }
            };
            final ExecutionEnvironment executionEnvironment = getMockExecutionEnvironment(resources,
                                                                                          outboundTransfers);
            executionEnvironment.retryManager = new RetryManagerImpl(executionEnvironment.cacheManager,
                                                                     ((executionEnvironment.configuration).getCacheDefinition()).getCacheMap());
            JavaInstanceFactoryImpl.setManualExecutionEnvironment(executionEnvironment);

            prepareOutboundFiles();

            /*
             * Create SPADE instance
             */
            final TicketManager ticketManager = new MockTicketManager();
            final Workflow workflow = Workflow.INGEST;
            MockWorkflowsManager workflowsManager;
            try (InputStream is = UnitTestUtility.getResourceAsStream(getClass(),
                                                                      OUTBOUND_TRANSFER_BPMN)) {
                workflowsManager = new MockWorkflowsManager(workflow,
                                                            is);
            }
            final Spade spade = new SpadeImpl((executionEnvironment.configuration).getAssembly(),
                                              executionEnvironment.registrationManager,
                                              ticketManager,
                                              workflowsManager);

            /*
             * Execution SPADE
             */
            try (InputStream is = UnitTestUtility.getResourceAsStream(getClass(),
                                                                      OUTBOUND_INGEST_BUNDLES)) {
                final Bundles bundles = XmlTestUtility.read(is,
                                                            Bundles.class);
                spade.ingest(bundles);
            }
            final AtomicInteger ingestCount = spade.getCount(workflow);
            synchronized (ingestCount) {
                while (0 != ingestCount.get()) {
                    ingestCount.wait();
                }
            }

            /*
             * Confirm expected results.
             */
            final File reshipDir = (((executionEnvironment.configuration).getCacheDefinition()).getCacheMap()).get(CacheDefinition.RESHIP);
            final List<File> reshipTickets = Arrays.asList(reshipDir.listFiles());
            assertEquals(1,
                         reshipTickets.size(),
                         "Incorrect number of tickets in the reship directory");
            final List<String> retries = Arrays.asList((reshipTickets.get(0)).list());
            assertEquals(3,
                         retries.size(),
                         "Incorrect number of files in the reship/ticket directory");
        }

        private void prepareOutboundFiles() throws IOException {
            final File DROPBOX_DIR = new File(DEFAULT_FILES_DIR,
                                              "dropbox/tour.2");
            final File RECEIVING_DIR = new File(DEFAULT_FILES_DIR,
                                                "receiving/loopback");
            DROPBOX_DIR.mkdirs();
            final File OUTBOUND_DATA = new File(DROPBOX_DIR,
                                                "loop.1.data");
            UnitTestUtility.createFileFromResource(getClass(),
                                                   "loopback/" + OUTBOUND_DATA.getName(),
                                                   OUTBOUND_DATA);
            final File OUTBOUND_SEMAPHORE = new File(DROPBOX_DIR,
                                                     "loop.1.sem");
            UnitTestUtility.createFileFromResource(getClass(),
                                                   "loopback/" + OUTBOUND_SEMAPHORE.getName(),
                                                   OUTBOUND_SEMAPHORE);
            RECEIVING_DIR.mkdirs();
        }

    }

    @Nested
    class Redispatch {

        final static private int ZERO_COUNT = 0;

        final static private int ONE_COUNT = 1;
        final static private int MANY_COUNT = 2;
        final static private int LIMIT_COUNT = 2;
        final String RESHIP_TRANSFER_BPMN = "lazarus/reship.bpmn";

        private Spade spade;

        private Collection<String> redispatchScan(Map<String, File> cacheMap,
                                                  int count) throws Exception {

            final String OUTBOUND_TRANSFER_NAME = "Loopback to Self";

            final OutboundTransfer OUTBOUND_TRANSFER_OBJECT = new OutboundTransfer(OUTBOUND_TRANSFER_NAME,
                                                                                   "files/receiving/loopback",
                                                                                   SELF_NAME);
            prepareReshipFiles(cacheMap,
                               count);
            preparePostponedFiles(cacheMap,
                                  count);

            final File RECEIVING_DIR = new File(DEFAULT_FILES_DIR,
                                                "receiving/loopback");
            RECEIVING_DIR.mkdirs();

            final List<String> resources = new ArrayList<>();
            @SuppressWarnings("serial")
            final Map<String, OutboundTransfer> outboundTransfers = new HashMap<>() {
                {
                    put(OUTBOUND_TRANSFER_NAME,
                        OUTBOUND_TRANSFER_OBJECT);
                }
            };
            final ExecutionEnvironment executionEnvironment = getMockExecutionEnvironment(resources,
                                                                                          outboundTransfers);
            ((executionEnvironment.configuration).getAssembly()).setTicketLimit(4);
            executionEnvironment.retryManager = new RetryManagerImpl(executionEnvironment.cacheManager,
                                                                     ((executionEnvironment.configuration).getCacheDefinition()).getCacheMap());
            JavaInstanceFactoryImpl.setManualExecutionEnvironment(executionEnvironment);

            final TicketManager ticketManager = new MockTicketManager();
            MockWorkflowsManager workflowsManager;
            try (InputStream is = UnitTestUtility.getResourceAsStream(getClass(),
                                                                      RESHIP_TRANSFER_BPMN)) {
                workflowsManager = new MockWorkflowsManager(Workflow.RESHIP,
                                                            is);
            }
            spade = new SpadeImpl((executionEnvironment.configuration).getAssembly(),
                                  executionEnvironment.retryManager,
                                  ticketManager,
                                  workflowsManager);
            final long cushion = 0;
            final TimeUnit unit = TimeUnit.MILLISECONDS;
            final Collection<String> result = spade.redispatchScan(cushion,
                                                                   unit,
                                                                   null);
            return result;
        }

        @Test
        @DisplayName("Resdispatch Scan Limits")
        void redispatchScanLimit() throws Exception {
            final CacheDefinition cacheDefinition = new CacheDefinition(null);
            final Collection<String> result = redispatchScan(cacheDefinition.getCacheMap(),
                                                             4);
            assertEquals(LIMIT_COUNT,
                         result.size());
        }

        @Test
        @DisplayName("Resdispatch Scan Many")
        void redispatchScanMany() throws Exception {
            final CacheDefinition cacheDefinition = new CacheDefinition(null);
            final Collection<String> result = redispatchScan(cacheDefinition.getCacheMap(),
                                                             2);
            assertEquals(MANY_COUNT,
                         result.size());
        }

        @Test
        @DisplayName("Resdispatch Scan One")
        void redispatchScanOne() throws Exception {
            final CacheDefinition cacheDefinition = new CacheDefinition(null);
            final Collection<String> result = redispatchScan(cacheDefinition.getCacheMap(),
                                                             1);

            final AtomicInteger ingestCount = spade.getCount(Workflow.RESHIP);
            synchronized (ingestCount) {
                while (0 != ingestCount.get()) {
                    ingestCount.wait();
                }
            }
            assertEquals(ONE_COUNT,
                         result.size());
        }

        @Test
        @DisplayName("Resdispatch Scan Zero")
        void redispatchScanZero() throws Exception {
            final CacheDefinition cacheDefinition = new CacheDefinition(null);
            final Collection<String> result = redispatchScan(cacheDefinition.getCacheMap(),
                                                             0);
            assertEquals(ZERO_COUNT,
                         result.size());
        }
    }

    private final File DEFAULT_FILES_DIR = new File("files");

    private final String SELF_NAME = "SpadeTest";

    final MockEntityManager entityManager = new MockEntityManager();

    private Map<String, Map<String, Collection<Registration>>> createKnownRegistrations(List<String> resources) throws FileNotFoundException,
                                                                                                                JAXBException,
                                                                                                                IOException {
        final Map<String, Map<String, Collection<Registration>>> result = new HashMap<>();
        LoadedRegistrationsUtilities.extendRegistrationMappings(prepareRegistrations(resources),
                                                                result);
        return result;
    }

    private ExecutionEnvironment getMockExecutionEnvironment(List<String> resources,
                                                             Map<String, OutboundTransfer> outboundTransfers) throws FileNotFoundException,
                                                                                                              JAXBException,
                                                                                                              IOException {
        final ExecutionEnvironment executionEnvironment = new ExecutionEnvironment();
        executionEnvironment.bookkeeping = new MockBookkeeping(entityManager);
        executionEnvironment.cacheManager = new CacheManagerImpl();
        executionEnvironment.configuration = new Configuration(new File(Configuration.DEFAULT_APPLICATION_DIR,
                                                                        "configuration"),
                                                               new Assembly(SELF_NAME,
                                                                            null),
                                                               new CacheDefinition(null),
                                                               null,
                                                               null,
                                                               null,
                                                               null);
        executionEnvironment.metadataManager = new MetadataImplManager();
        executionEnvironment.neighborhoodManager = new MockNeighborhoodManager(outboundTransfers);
        executionEnvironment.registrationManager = new MockRegistrationManager(createKnownRegistrations(resources));
        executionEnvironment.retryManager = new MockRetryManager();
        executionEnvironment.verificationManager = new MockVerificationManager(outboundTransfers);
        executionEnvironment.warehouseManager = null;
        return executionEnvironment;
    }

    /**
     * To be used in testing action
     *
     * @param cacheMap
     *            the map of directories that make up the cache for this
     *            application.
     * @param count
     *            the number of instances to create in the postponed directory.
     *
     * @throws IOException
     *             when there is a problem with I/O.
     */
    public void preparePostponedFiles(Map<String, File> cacheMap,
                                      int count) throws IOException {
        final File postponed = cacheMap.get(CacheDefinition.POSTPONED);
        final String ticketTemplate = UnitTestUtility.getResourceAsString(getClass(),
                                                                          "scanning/reship/ticket.xml.tmpl");
        for (int index = 0;
             count != index;
             ++index) {
            final String number = Integer.toString(index + 1);
            final File ticket = new File(postponed,
                                         Integer.toString(index));
            Files.writeString(ticket.toPath(),
                              MessageFormat.format(ticketTemplate,
                                                   index,
                                                   number),
                              StandardOpenOption.CREATE);
        }
    }

    private List<Registration> prepareRegistrations(List<String> resources) throws JAXBException,
                                                                            IOException,
                                                                            FileNotFoundException {
        final List<Registration> registrations = new ArrayList<>();
        for (String resource : resources) {
            try (InputStream is = UnitTestUtility.getResourceAsStream(getClass(),
                                                                      resource)) {
                final LocalRegistration registration = XmlTestUtility.read(is,
                                                                           LocalRegistration.class);
                registrations.add(registration);
                final KnownLocalRegistration knownLocalRegistration = new KnownLocalRegistration(registration);
                entityManager.persist(knownLocalRegistration);
            }
        }
        return registrations;
    }

    /**
     * To be used in testing action
     *
     * @param cacheMap
     *            the map of directories that make up the cache for this
     *            application.
     * @param count
     *            the number of instances to create in the reship directory.
     *
     * @throws IOException
     *             when there is a problem with I/O.
     */
    public void prepareReshipFiles(Map<String, File> cacheMap,
                                   int count) throws IOException {
        final File reshipDir = cacheMap.get(CacheDefinition.RESHIP);
        final String ticketTemplate = UnitTestUtility.getResourceAsString(getClass(),
                                                                          "scanning/reship/ticket.xml.tmpl");
        for (int index = 0;
             count != index;
             ++index) {
            final String number = Integer.toString(index + 1);
            final File ticketDir = new File(reshipDir,
                                            Integer.toString(index));
            ticketDir.mkdirs();
            final File metadata = new File(ticketDir,
                                           "loop." + number
                                                      + ".meta.xml");
            UnitTestUtility.createFileFromResource(getClass(),
                                                   "scanning/reship/loop.x.meta.xml",
                                                   metadata);
            final File payloadDir = new File(ticketDir,
                                             "payload");
            payloadDir.mkdir();
            final File payload = new File(payloadDir,
                                          "loop." + number
                                                      + ".data");
            UnitTestUtility.createFileFromResource(getClass(),
                                                   "scanning/reship/loop.x.data",
                                                   payload);
            final File ticket = new File(ticketDir,
                                         "ticket.xml");
            Files.writeString(ticket.toPath(),
                              MessageFormat.format(ticketTemplate,
                                                   index,
                                                   number),
                              StandardOpenOption.CREATE);
        }
    }

    @BeforeEach
    void setUp() {
        System.setProperty("gov.lbl.nest.spade.disk.failure.limit",
                           "1");
        UnitTestUtility.deleteTree(CacheDefinition.DEFAULT_CACHE_DIR);
        UnitTestUtility.deleteTree(DEFAULT_FILES_DIR);
    }

    @AfterEach
    void tearDown() {
        UnitTestUtility.deleteTree(CacheDefinition.DEFAULT_CACHE_DIR);
        UnitTestUtility.deleteTree(DEFAULT_FILES_DIR);
    }
}
