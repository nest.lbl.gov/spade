package gov.lbl.nest.spade.ejb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jakarta.persistence.FlushModeType;
import jakarta.persistence.LockModeType;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.Parameter;
import jakarta.persistence.TemporalType;
import jakarta.persistence.TypedQuery;

/**
 * The {@link TypedQuery} implementation used in unit tests.
 *
 * @author patton
 *
 * @param <X>
 *            the class to which the query is typed.
 */
public class MockTypedQuery<X> implements
                           TypedQuery<X> {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The NamedQuery to determine whether a ticket is active.
     */
    private static final String ACTIVE_TICKET = "activeTicket";

    /**
     * The NamedQuery to find confirmable TicketedFiles between a pair of specified
     * dates.
     */
    private static final String CONFIRMABLE_BETWEEN = "confirmableBetween";

    /**
     * The NamedQuery to find confirmable TicketedFiles from a specified date.
     */
    private static final String CONFIRMABLE_SINCE = "confirmableSince";

    /**
     * The NamedQuery to find confirmable TicketedFiles between a pair of specified
     * dates.
     */
    private static final String CONFIRMABLE_BY_REGISTRATION_BETWEEN = "confirmableByRegistrationBetween";

    /**
     * The NamedQuery to find confirmable TicketedFiles from a specified date.
     */
    private static final String CONFIRMABLE_BY_REGISTRATION_AFTER = "confirmableByRegistrationAfter";

    /**
     * The NamedQuery to find akk KnownInboundRegistrations.
     */
    private static final String GET_INBOUND_REGISTRATONS = "getInboundRegistrations";

    /**
     * The NamedQuery to find the collection of {@link ShippedFile} instance that
     * are unverified for a given Bundle.
     */
    private static final String GET_CONFIRMABLES_BY_BUNDLE = "getConfirmablesByBundle";

    /**
     * The NamedQuery to find an {@link Confirmation} instance using its ticketed
     * file and its destination.
     */
    private static final String GET_CONFIRMATION_BY_TICKET_AND_DESTINATION = "getConfirmationByTicketAndDestination";

    /**
     * The NamedQuery to find an {@link Confirmation} instance using its ticketed
     * file.
     */
    private static final String GET_CONFIRMATIONS_BY_TICKETED_FILE = "getConfirmationsByTicketedFile";

    /**
     * The NamedQuery to find an {@link Entry} instance using its identity.
     */
    private static final String GET_ENTRY_BY_IDENTITY = "getEntryByIdentity";

    /**
     * The NamedQuery to find an {@link KnownInboundRegistration} instance using its
     * local id.
     */
    private static final String GET_INBOUND_REGISTRATIONS_BY_LOCAL_ID = "getInboundRegistrationsByLocalId";

    /**
     * The NamedQuery to find an {@link KnownInboundRegistration} instance using its
     * local id.
     */
    private static final String GET_KNOWN_REGISTRATIONS_BY_LOCAL_ID = "getKnownRegistrationsByLocalIds";

    /**
     * The NamedQuery to find an {@link KnownLocalRegistration} instance using its
     * local id.
     */
    private static final String GET_LOCAL_REGISTRATIONS_BY_LOCAL_ID = "getLocalRegistrationsByLocalId";

    /**
     * The NamedQuery to find an {@link HierarchicalPath} instance for the metadata
     * of an {@link Entry} instance using the Entry's identity.
     */
    private static final String GET_META_PATH_BY_IDENTITY = "getMetaPathByIdentity";

    /**
     * The NamedQuery to find an {@link KnownNeighbor} instance using its name.
     */
    private static final String GET_NEIGHBOR_BY_NAME = "getNeighborByName";

    /**
     * The NamedQuery to find an {@link KnownRegistration} instance using its local
     * id.
     */
    private static final String GET_REGISTRATION_BY_LOCAL_ID = "getKnownRegistrationsByLocalId";

    /**
     * The NamedQuery to find the number of unconfirmed {@link Confirmation}
     * instance for a ticket.
     */
    private static final String GET_UNCONFIRMED_COUNT_BY_SHIPPED_FILE = "getUnconfirmedCountByShippedFile";

    /**
     * The NamedQuery to find the collection of {@link TicketedFile} instance that
     * are unverified for a given Bundle.
     */
    private static final String GET_UNVERIFIEDS_BY_BUNDLE = "getUnverifiedsByBundle";

    /**
     * The NamedQuery to find the collection of neighbors to whom this ticket has
     * not successfully been delivered.
     */
    private static final String GET_DELIVERED_NEIGHBOR_BY_TICKET = "getUndeliveredNeighborsByTicket";

    /**
     * The NamedQuery to find an {@link HierarchicalPath} instance using its name.
     */
    private static final String GET_PATHS_BY_NAME = "getPathsByName";

    /**
     * The NamedQuery to find verified TicketedFiles between a pair of specified
     * dates.
     */
    private static final String VERIFIED_BETWEEN = "verifiedBetween";

    /**
     * The NamedQuery to find verified TicketedFiles from a specified date for a
     * collection of registrations.
     */
    private static final String VERIFIED_BY_REGISTRATION_AFTER = "verifiedByRegistrationAfter";

    /**
     * The NamedQuery to find verified TicketedFiles from a specified date.
     */
    private static final String VERIFIED_SINCE = "verifiedSince";

    /**
     * The {@link Comparator} used to order verifiable queries.
     */
    private static final Comparator<ShippedFile> VERIFIABLE_COMPARATOR = new Comparator<>() {

        @Override
        public int compare(ShippedFile o1,
                           ShippedFile o2) {
            final Date lhs = o1.getWhenConfirmable();
            final Date rhs = o1.getWhenConfirmable();
            if (lhs.before(rhs)) {
                return -1;
            }
            if (lhs.after(rhs)) {
                return 1;
            }
            return 0;
        }
    };

    /**
     * The {@link Comparator} used to order verified queries.
     */
    private static final Comparator<ShippedFile> VERIFIED_COMPARATOR = new Comparator<>() {

        @Override
        public int compare(ShippedFile o1,
                           ShippedFile o2) {
            final Date lhs = o1.getWhenVerificationCompleted();
            final Date rhs = o1.getWhenVerificationCompleted();
            if (lhs.before(rhs)) {
                return -1;
            }
            if (lhs.after(rhs)) {
                return 1;
            }
            return 0;
        }
    };

    // private static member data

    // private instance member data

    /**
     * The value of the after parameter if set.
     */
    private Date after;

    /**
     * The value of the before parameter if set.
     */
    private Date before;

    /**
     * The value of the bundle parameter if set.
     */
    private String bundle;

    /**
     * The value of the destination parameter if set.
     */
    private String destination;

    /**
     * The value of the identity parameter if set.
     */
    private String identity;

    /**
     * The value of the localId parameter if set.
     */
    private String localId;

    /**
     * The value of the localIds parameter if set.
     */
    private Collection<String> localIds;

    /**
     * The value of the name parameter if set.
     */
    private String name;

    /**
     * The value of the registration parameter if set,
     */
    private List<KnownRegistration> registrations;

    /**
     * The value of the file parameter if set.
     */
    private ShippedFile shippedFile;

    /**
     * The value of the ticket parameter if set.
     */
    private Integer ticket;

    /**
     * The {@link MockEntityManager} instance used by this object.
     */
    private MockEntityManager entityManager;

    /**
     * The NamedQuery with which this object was created.
     */
    private String namedQuery;

    // constructors

    /**
     * Constructs an instance of this class.
     *
     * @param query
     *            the NamedQuery defining this query.
     */
    MockTypedQuery(String query,
                   MockEntityManager manager) {
        entityManager = manager;
        namedQuery = query;
    }

    // instance member method (alphabetic)

    @Override
    public int executeUpdate() {
        // Not used.
        return 0;
    }

    @Override
    public int getFirstResult() {
        // Not used.
        return 0;
    }

    @Override
    public FlushModeType getFlushMode() {
        // Not used.
        return null;
    }

    @Override
    public Map<String, Object> getHints() {
        // Not used.
        return null;
    }

    @Override
    public LockModeType getLockMode() {
        // Not used.
        return null;
    }

    @Override
    public int getMaxResults() {
        // Not used.
        return 0;
    }

    @Override
    public Parameter<?> getParameter(int position) {
        // Not used.
        return null;
    }

    @Override
    public <T> Parameter<T> getParameter(int position,
                                         Class<T> type) {
        // Not used.
        return null;
    }

    @Override
    public Parameter<?> getParameter(String name) {
        // Not used.
        return null;
    }

    @Override
    public <T> Parameter<T> getParameter(String name,
                                         Class<T> type) {
        // Not used.
        return null;
    }

    @Override
    public Set<Parameter<?>> getParameters() {
        // Not used.
        return null;
    }

    @Override
    public Object getParameterValue(int position) {
        // Not used.
        return null;
    }

    @Override
    public <T> T getParameterValue(Parameter<T> param) {
        // Not used.
        return null;
    }

    @Override
    public Object getParameterValue(String name) {
        // Not used.
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<X> getResultList() {
        return (List<X>) resolveResultsList(namedQuery);
    }

    @SuppressWarnings("unchecked")
    @Override
    public X getSingleResult() {
        return (X) resolveSingleResult(namedQuery);
    }

    @Override
    public boolean isBound(Parameter<?> param) {
        // Not used.
        return false;
    }

    /**
     * Resolves {@link #getResultList()} is an untyped manner.
     *
     * @param query
     *            the NamedQuery to resolve.
     *
     * @return the {@link Object} that is the resolution of this query.
     */
    private Object resolveResultsList(String query) {
        if (CONFIRMABLE_BETWEEN.equals(query)) {
            final List<ShippedFile> result = new ArrayList<>();
            synchronized (entityManager.SHIPPED_FILES) {
                for (ShippedFile entry : entityManager.SHIPPED_FILES) {
                    final Date whenVerifiable = entry.getWhenConfirmable();
                    if (null != whenVerifiable && (!after.after(whenVerifiable))
                        && before.after(whenVerifiable)) {
                        result.add(entry);
                    }
                }
            }
            Collections.sort(result,
                             VERIFIABLE_COMPARATOR);
            return result;
        }
        if (CONFIRMABLE_SINCE.equals(query)) {
            final List<ShippedFile> result = new ArrayList<>();
            synchronized (entityManager.SHIPPED_FILES) {
                for (ShippedFile entry : entityManager.SHIPPED_FILES) {
                    final Date whenVerifiable = entry.getWhenConfirmable();
                    if (null != whenVerifiable && (!after.after(whenVerifiable))) {
                        result.add(entry);
                    }
                }
            }
            Collections.sort(result,
                             VERIFIABLE_COMPARATOR);
            return result;
        }
        if (CONFIRMABLE_BY_REGISTRATION_BETWEEN.equals(query)) {
            final List<ShippedFile> result = new ArrayList<>();
            synchronized (entityManager.SHIPPED_FILES) {
                for (ShippedFile entry : entityManager.SHIPPED_FILES) {
                    final Date whenVerifiable = entry.getWhenConfirmable();
                    if (null != whenVerifiable && (!after.after(whenVerifiable))
                        && before.after(whenVerifiable)) {
                        final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                                             entry.getTicketedFileKey());
                        if (null != registrations && registrations.contains(ticketedFile.getLocalRegistration())) {
                            result.add(entry);
                        }
                    }
                }
            }
            Collections.sort(result,
                             VERIFIABLE_COMPARATOR);
            return result;
        }
        if (CONFIRMABLE_BY_REGISTRATION_AFTER.equals(query)) {
            final List<ShippedFile> result = new ArrayList<>();
            synchronized (entityManager.SHIPPED_FILES) {
                for (ShippedFile entry : entityManager.SHIPPED_FILES) {
                    final Date whenVerifiable = entry.getWhenConfirmable();
                    if (null != whenVerifiable && (!after.after(whenVerifiable))) {
                        final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                                             entry.getTicketedFileKey());
                        if (null != registrations && registrations.contains(ticketedFile.getLocalRegistration())) {
                            result.add(entry);
                        }
                    }
                }
            }
            Collections.sort(result,
                             VERIFIABLE_COMPARATOR);
            return result;
        }
        if (GET_INBOUND_REGISTRATONS.equals(query)) {
            final List<KnownRegistration> result = new ArrayList<>();
            synchronized (entityManager.REGISTRATIONS) {
                for (KnownRegistration entry : entityManager.REGISTRATIONS) {
                    if (entry instanceof KnownInboundRegistration) {
                        result.add(entry);
                    }
                }
            }
            return result;
        }
        if (GET_CONFIRMABLES_BY_BUNDLE.equals(query)) {
            final List<ShippedFile> result = new ArrayList<>();
            synchronized (entityManager.SHIPPED_FILES) {
                for (ShippedFile entry : entityManager.SHIPPED_FILES) {
                    final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                                         entry.getTicketedFileKey());
                    if (bundle.equals(ticketedFile.getBundle()) && null != entry.getWhenConfirmable()) {
                        result.add(entry);
                    }
                }
            }
            return result;
        }
        if (GET_CONFIRMATIONS_BY_TICKETED_FILE.equals(query)) {
            final List<Confirmation> result = new ArrayList<>();
            synchronized (entityManager.CONFIRMATIONS) {
                for (Confirmation entry : entityManager.CONFIRMATIONS) {
                    if (shippedFile.equals(entry.getShippedFile())) {
                        result.add(entry);
                    }
                }
            }
            return result;
        }
        if (GET_DELIVERED_NEIGHBOR_BY_TICKET.equals(query)) {
            final List<String> result = new ArrayList<>();
            synchronized (entityManager.CONFIRMATIONS) {
                for (Confirmation entry : entityManager.CONFIRMATIONS) {
                    if (ticket.equals((entry.getShippedFile()).getTicketedFileKey()) && null == entry.getWhenDelivered()) {
                        result.add((entry.getDestination()).getName());
                    }
                }
            }
            return result;
        }
        if (GET_KNOWN_REGISTRATIONS_BY_LOCAL_ID.equals(query)) {
            final List<KnownRegistration> result = new ArrayList<>();
            synchronized (entityManager.REGISTRATIONS) {
                for (KnownRegistration entry : entityManager.REGISTRATIONS) {
                    if (localIds.contains(entry.localId)) {
                        result.add(entry);
                    }
                }
            }
            return result;
        }
        if (GET_PATHS_BY_NAME.equals(query)) {
            final List<HierarchicalPath> result = new ArrayList<>();
            synchronized (entityManager.HIERARCHICAL_PATHS) {
                for (HierarchicalPath entry : entityManager.HIERARCHICAL_PATHS) {
                    if (name.equals(entry.getName())) {
                        result.add(entry);
                    }
                }
            }
            return result;
        }
        if (GET_UNVERIFIEDS_BY_BUNDLE.equals(query)) {
            final List<ShippedFile> result = new ArrayList<>();
            synchronized (entityManager.SHIPPED_FILES) {
                for (ShippedFile entry : entityManager.SHIPPED_FILES) {
                    final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                                         entry.getTicketedFileKey());
                    if (bundle.equals(ticketedFile.getBundle()) && null != entry.getWhenVerificationStarted()
                        && null == entry.getWhenVerificationCompleted()
                        && null == entry.getWhenAbandoned()) {
                        result.add(entry);
                    }
                }
            }
            return result;
        }
        if (VERIFIED_BETWEEN.equals(query)) {
            final List<ShippedFile> result = new ArrayList<>();
            synchronized (entityManager.SHIPPED_FILES) {
                for (ShippedFile entry : entityManager.SHIPPED_FILES) {
                    final Date whenVerificationCompleted = entry.getWhenVerificationCompleted();
                    if (null != whenVerificationCompleted && (!after.after(whenVerificationCompleted))
                        && before.after(whenVerificationCompleted)) {
                        result.add(entry);
                    }
                }
            }
            Collections.sort(result,
                             VERIFIABLE_COMPARATOR);
            return result;
        }
        if (VERIFIED_BY_REGISTRATION_AFTER.equals(query)) {
            final List<ShippedFile> result = new ArrayList<>();
            synchronized (entityManager.SHIPPED_FILES) {
                for (ShippedFile entry : entityManager.SHIPPED_FILES) {
                    final Date whenVerificationCompleted = entry.getWhenVerificationCompleted();
                    if (null != whenVerificationCompleted && (!after.after(whenVerificationCompleted))) {
                        final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                                             entry.getTicketedFileKey());
                        if (null != registrations && registrations.contains(ticketedFile.getLocalRegistration())) {
                            result.add(entry);
                        }
                    }
                }
            }
            Collections.sort(result,
                             VERIFIED_COMPARATOR);
            return result;
        }
        if (VERIFIED_SINCE.equals(query)) {
            final List<ShippedFile> result = new ArrayList<>();
            synchronized (entityManager.SHIPPED_FILES) {
                for (ShippedFile entry : entityManager.SHIPPED_FILES) {
                    final Date whenVerificationCompleted = entry.getWhenVerificationCompleted();
                    if (null != whenVerificationCompleted && (!after.after(whenVerificationCompleted))) {
                        result.add(entry);
                    }
                }
            }
            Collections.sort(result,
                             VERIFIABLE_COMPARATOR);
            return result;
        }
        return null;
    }

    /**
     * Resolves {@link #getSingleResult()} is an untyped manner.
     *
     * @param query
     *            the NamedQuery with which this object was created.
     * @param definitionToSet
     *            the value of the process definition parameter if set.
     * @param stateToSet
     *            the value of the State parameter to set, if any.
     * @param stringsToSet
     *            the value of the String parameters to set, if any.
     *
     * @return the {@link Object} that is the resolution of this query.
     */
    private Object resolveSingleResult(final String query) {
        if (ACTIVE_TICKET.equals(query)) {
            Boolean result = Boolean.FALSE;
            int count = 0;
            for (TicketedFile ticketedFile : entityManager.TICKETED_FILES) {
                if (identity.equals(ticketedFile.getTicketIdentity())) {
                    ++count;
                    result = (null == ticketedFile.getWhenAbandoned());
                }
            }
            if (1 != count) {
                return Boolean.TRUE;
            }
            return result;
        }
        if (GET_CONFIRMATION_BY_TICKET_AND_DESTINATION.equals(query)) {
            Confirmation result = null;
            for (Confirmation entry : entityManager.CONFIRMATIONS) {
                if (ticket.equals((entry.getShippedFile()).getTicketedFileKey()) && destination.equals((entry.getDestination()).getName())) {
                    if (null == result) {
                        result = entry;
                    } else {
                        throw new NonUniqueResultException();
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        if (GET_UNCONFIRMED_COUNT_BY_SHIPPED_FILE.equals(query)) {
            int result = 0;
            for (Confirmation entry : entityManager.CONFIRMATIONS) {
                if ((shippedFile.getTicketedFileKey()) == (entry.getShippedFile()).getTicketedFileKey() && null == entry.getWhenConfirmed()) {
                    result += 1;
                }
            }
            return Long.valueOf(result);
        }
        if (GET_ENTRY_BY_IDENTITY.equals(query)) {
            Entry result = null;
            for (Entry entry : entityManager.ENTRIES) {
                if (identity.equals(entry.getIdentity())) {
                    if (null == result) {
                        result = entry;
                    } else {
                        throw new NonUniqueResultException();
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        if (GET_INBOUND_REGISTRATIONS_BY_LOCAL_ID.equals(query)) {
            KnownRegistration result = null;
            for (KnownRegistration entry : entityManager.REGISTRATIONS) {
                if (localId.equals(entry.getLocalId()) && entry instanceof KnownInboundRegistration) {
                    if (null == result) {
                        result = entry;
                    } else {
                        throw new NonUniqueResultException();
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        if (GET_LOCAL_REGISTRATIONS_BY_LOCAL_ID.equals(query)) {
            KnownRegistration result = null;
            for (KnownRegistration entry : entityManager.REGISTRATIONS) {
                if (localId.equals(entry.getLocalId()) && entry instanceof KnownLocalRegistration) {
                    if (null == result) {
                        result = entry;
                    } else {
                        throw new NonUniqueResultException();
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        if (GET_META_PATH_BY_IDENTITY.equals(query)) {
            HierarchicalPath result = null;
            for (Entry entry : entityManager.ENTRIES) {
                if (identity.equals(entry.getIdentity()) && null != entry.getMetaPath()) {
                    if (null == result) {
                        result = entry.getMetaPath();
                    } else {
                        throw new NonUniqueResultException();
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        if (GET_NEIGHBOR_BY_NAME.equals(query)) {
            KnownNeighbor result = null;
            for (KnownNeighbor entry : entityManager.NEIGHBOR) {
                if (name.equals(entry.getName())) {
                    if (null == result) {
                        result = entry;
                    } else {
                        throw new NonUniqueResultException();
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        if (GET_REGISTRATION_BY_LOCAL_ID.equals(query)) {
            KnownRegistration result = null;
            for (KnownRegistration entry : entityManager.REGISTRATIONS) {
                if (localId.equals(entry.getLocalId())) {
                    if (null == result) {
                        result = entry;
                    } else {
                        throw new NonUniqueResultException();
                    }
                }
            }
            if (null == result) {
                throw new NoResultException();
            }
            return result;
        }
        throw new NoResultException();
    }

    @Override
    public TypedQuery<X> setFirstResult(int startPosition) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setFlushMode(FlushModeType flushMode) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setHint(String hintName,
                                 Object value) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setLockMode(LockModeType lockMode) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setMaxResults(int maxResult) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(int position,
                                      Calendar value,
                                      TemporalType temporalType) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(int position,
                                      Date value,
                                      TemporalType temporalType) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(int position,
                                      Object value) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(Parameter<Calendar> param,
                                      Calendar value,
                                      TemporalType temporalType) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(Parameter<Date> param,
                                      Date value,
                                      TemporalType temporalType) {
        // Not used.
        return null;
    }

    @Override
    public <T> TypedQuery<X> setParameter(Parameter<T> param,
                                          T value) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(String name,
                                      Calendar value,
                                      TemporalType temporalType) {
        // Not used.
        return null;
    }

    @Override
    public TypedQuery<X> setParameter(String name,
                                      Date value,
                                      TemporalType temporalType) {
        // Not used.
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public TypedQuery<X> setParameter(String parameterName,
                                      Object value) {
        if ("after".equals(parameterName)) {
            after = (Date) value;
            return this;
        }
        if ("before".equals(parameterName)) {
            before = (Date) value;
            return this;
        }
        if ("bundle".equals(parameterName)) {
            bundle = (String) value;
            return this;
        }
        if ("destination".equals(parameterName)) {
            destination = (String) value;
            return this;
        }
        if ("identity".equals(parameterName)) {
            identity = (String) value;
            return this;
        }
        if ("localId".equals(parameterName)) {
            localId = (String) value;
            return this;
        }
        if ("localIds".equals(parameterName)) {
            localIds = (Collection<String>) value;
            return this;
        }
        if ("name".equals(parameterName)) {
            name = (String) value;
            return this;
        }
        if ("registrations".equals(parameterName)) {
            registrations = (List<KnownRegistration>) value;
            return this;
        }
        if ("shippedFile".equals(parameterName)) {
            shippedFile = (ShippedFile) value;
            return this;
        }
        if ("ticket".equals(parameterName)) {
            ticket = (Integer) value;
            return this;
        }
        throw new IllegalArgumentException();
    }

    @Override
    public <T> T unwrap(Class<T> cls) {
        // Not used.
        return null;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
