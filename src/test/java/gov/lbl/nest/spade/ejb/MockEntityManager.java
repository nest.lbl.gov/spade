package gov.lbl.nest.spade.ejb;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.spade.ImplementationString;
import jakarta.persistence.EntityGraph;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.FlushModeType;
import jakarta.persistence.LockModeType;
import jakarta.persistence.Query;
import jakarta.persistence.StoredProcedureQuery;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.CriteriaUpdate;
import jakarta.persistence.metamodel.Metamodel;

/**
 * The class {@link ImplementationString} the {@link EntityManager} interface of
 * Testing purposes.
 *
 * @author patton
 *
 */
public class MockEntityManager implements
                               EntityManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The array holding persistent {@link Entry} instances.
     */
    final List<Entry> ENTRIES = new ArrayList<>();

    /**
     * The array holding persistent {@link Entry} instances.
     */
    final List<Confirmation> CONFIRMATIONS = new ArrayList<>();

    /**
     * The array holding persistent {@link HIERARCHICAL_PATHS} instances.
     */
    final List<HierarchicalPath> HIERARCHICAL_PATHS = new ArrayList<>();

    /**
     * The array holding persistent {@link KnownNeighbor} instances.
     */
    final List<KnownNeighbor> NEIGHBOR = new ArrayList<>();

    /**
     * The array holding persistent {@link KnownLocalRegistration} instances.
     */
    final List<KnownRegistration> REGISTRATIONS = new ArrayList<>();

    /**
     * The array holding persistent {@link ShippedFile} instances.
     */
    final List<ShippedFile> SHIPPED_FILES = new ArrayList<>();

    /**
     * The array holding persistent {@link TicketedFile} instances.
     */
    final List<TicketedFile> TICKETED_FILES = new ArrayList<>();

    // constructors

    // instance member method (alphabetic)

    @Override
    public void clear() {
        // Not used.
    }

    @Override
    public void close() {
        // Not used.
    }

    @Override
    public boolean contains(Object arg0) {
        // Not used.
        return false;
    }

    @Override
    public <T> EntityGraph<T> createEntityGraph(Class<T> arg0) {
        // Not used.
        return null;
    }

    @Override
    public EntityGraph<?> createEntityGraph(String arg0) {
        // Not used.
        return null;
    }

    @Override
    public Query createNamedQuery(String arg0) {
        // Not used.
        return null;
    }

    @Override
    public <T> TypedQuery<T> createNamedQuery(String arg0,
                                              Class<T> arg1) {
        return new MockTypedQuery<>(arg0,
                                    this);
    }

    @Override
    public StoredProcedureQuery createNamedStoredProcedureQuery(String arg0) {
        // Not used.
        return null;
    }

    @Override
    public Query createNativeQuery(String arg0) {
        // Not used.
        return null;
    }

    @Override
    public Query createNativeQuery(String arg0,
                                   @SuppressWarnings("rawtypes") Class arg1) {
        // Not used.
        return null;
    }

    @Override
    public Query createNativeQuery(String arg0,
                                   String arg1) {
        // Not used.
        return null;
    }

    @Override
    public Query createQuery(@SuppressWarnings("rawtypes") CriteriaDelete arg0) {
        // Not used.
        return null;
    }

    @Override
    public <T> TypedQuery<T> createQuery(CriteriaQuery<T> arg0) {
        // Not used.
        return null;
    }

    @Override
    public Query createQuery(@SuppressWarnings("rawtypes") CriteriaUpdate arg0) {
        // Not used.
        return null;
    }

    @Override
    public Query createQuery(String arg0) {
        // Not used.
        return null;
    }

    @Override
    public <T> TypedQuery<T> createQuery(String arg0,
                                         Class<T> arg1) {
        // Not used.
        return null;
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String arg0) {
        // Not used.
        return null;
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String arg0,
                                                           @SuppressWarnings("rawtypes") Class... arg1) {
        // Not used.
        return null;
    }

    @Override
    public StoredProcedureQuery createStoredProcedureQuery(String arg0,
                                                           String... arg1) {
        // Not used.
        return null;
    }

    @Override
    public void detach(Object arg0) {
        // Not used.
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T find(Class<T> arg0,
                      Object arg1) {
        if (arg0 == ShippedFile.class) {
            final int key = (Integer) arg1;
            for (ShippedFile entry : SHIPPED_FILES) {
                if (key == entry.getTicketedFileKey()) {
                    return (T) entry;
                }
            }
        }
        if (arg0 == TicketedFile.class) {
            final int key = (Integer) arg1;
            for (TicketedFile entry : TICKETED_FILES) {
                if (key == entry.getTicketedFileKey()) {
                    return (T) entry;
                }
            }
        }
        return null;
    }

    @Override
    public <T> T find(Class<T> arg0,
                      Object arg1,
                      LockModeType arg2) {
        return find(arg0,
                    arg1);
    }

    @Override
    public <T> T find(Class<T> arg0,
                      Object arg1,
                      LockModeType arg2,
                      Map<String, Object> arg3) {
        // Not used.
        return null;
    }

    @Override
    public <T> T find(Class<T> arg0,
                      Object arg1,
                      Map<String, Object> arg2) {
        // Not used.
        return null;
    }

    @Override
    public void flush() {
        // Not used.
    }

    @Override
    public CriteriaBuilder getCriteriaBuilder() {
        // Not used.
        return null;
    }

    @Override
    public Object getDelegate() {
        // Not used.
        return null;
    }

    @Override
    public EntityGraph<?> getEntityGraph(String arg0) {
        // Not used.
        return null;
    }

    @Override
    public <T> List<EntityGraph<? super T>> getEntityGraphs(Class<T> arg0) {
        // Not used.
        return null;
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        // Not used.
        return null;
    }

    @Override
    public FlushModeType getFlushMode() {
        // Not used.
        return null;
    }

    @Override
    public LockModeType getLockMode(Object arg0) {
        // Not used.
        return null;
    }

    @Override
    public Metamodel getMetamodel() {
        // Not used.
        return null;
    }

    @Override
    public Map<String, Object> getProperties() {
        // Not used.
        return null;
    }

    @Override
    public <T> T getReference(Class<T> arg0,
                              Object arg1) {
        // Not used.
        return null;
    }

    @Override
    public EntityTransaction getTransaction() {
        // Not used.
        return null;
    }

    @Override
    public boolean isJoinedToTransaction() {
        // Not used.
        return false;
    }

    @Override
    public boolean isOpen() {
        // Not used.
        return false;
    }

    @Override
    public void joinTransaction() {
        // Not used.
    }

    @Override
    public void lock(Object arg0,
                     LockModeType arg1) {
        // Not used.
    }

    @Override
    public void lock(Object arg0,
                     LockModeType arg1,
                     Map<String, Object> arg2) {
        // Not used.
    }

    @Override
    public <T> T merge(T arg0) {
        return arg0;
    }

    @Override
    public void persist(Object arg0) {
        if (arg0 instanceof Entry) {
            final int key = ENTRIES.size();
            final Entry entry = (Entry) arg0;
            entry.setEntryKey(key);
            ENTRIES.add(entry);
            return;
        }
        if (arg0 instanceof Confirmation) {
            final int key = CONFIRMATIONS.size();
            final Confirmation entry = (Confirmation) arg0;
            entry.setConfirmationKey(key);
            CONFIRMATIONS.add(entry);
            return;
        }
        if (arg0 instanceof HierarchicalPath) {
            synchronized (HIERARCHICAL_PATHS) {
                final int key = HIERARCHICAL_PATHS.size();
                final HierarchicalPath entry = (HierarchicalPath) arg0;
                entry.setPathKey(key);
                HIERARCHICAL_PATHS.add(entry);
            }
            return;
        }
        if (arg0 instanceof KnownNeighbor) {
            final int key = NEIGHBOR.size();
            final KnownNeighbor entry = (KnownNeighbor) arg0;
            entry.setNeighborKey(key);
            NEIGHBOR.add(entry);
            return;
        }
        if (arg0 instanceof KnownInboundRegistration) {
            final int key = REGISTRATIONS.size();
            final KnownInboundRegistration entry = (KnownInboundRegistration) arg0;
            entry.setRegistrationKey(key);
            REGISTRATIONS.add(entry);
            return;
        }
        if (arg0 instanceof KnownLocalRegistration) {
            final int key = REGISTRATIONS.size();
            final KnownLocalRegistration entry = (KnownLocalRegistration) arg0;
            entry.setRegistrationKey(key);
            REGISTRATIONS.add(entry);
            return;
        }
        if (arg0 instanceof ShippedFile) {
            final ShippedFile entry = (ShippedFile) arg0;
            SHIPPED_FILES.add(entry);
            return;
        }
        if (arg0 instanceof TicketedFile) {
            final int key = TICKETED_FILES.size();
            final TicketedFile entry = (TicketedFile) arg0;
            entry.setTicketedFileKey(key);
            TICKETED_FILES.add(entry);
            return;
        }
    }

    @Override
    public void refresh(Object arg0) {
        // Not used.
    }

    @Override
    public void refresh(Object arg0,
                        LockModeType arg1) {
        // Not used.
    }

    @Override
    public void refresh(Object arg0,
                        LockModeType arg1,
                        Map<String, Object> arg2) {
        // Not used.
    }

    @Override
    public void refresh(Object arg0,
                        Map<String, Object> arg1) {
        // Not used.
    }

    @Override
    public void remove(Object arg0) {
        // Not used.
    }

    @Override
    public void setFlushMode(FlushModeType arg0) {
        // Not used.
    }

    @Override
    public void setProperty(String arg0,
                            Object arg1) {
        // Not used.
    }

    @Override
    public <T> T unwrap(Class<T> arg0) {
        // Not used.
        return null;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
