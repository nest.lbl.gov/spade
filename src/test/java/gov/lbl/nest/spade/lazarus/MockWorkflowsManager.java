package gov.lbl.nest.spade.lazarus;

import java.io.File;
import java.io.InputStream;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.lazarus.bpmn.XMLDefinitions;
import gov.lbl.nest.lazarus.control.ControlOnlyExecutableProcess;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.execution.ProcessDefinition;
import gov.lbl.nest.lazarus.structure.DataObject;
import gov.lbl.nest.lazarus.structure.Process;
import gov.lbl.nest.spade.workflow.ActivityManager;
import gov.lbl.nest.spade.workflow.ActivityMonitor;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowDirector;
import gov.lbl.nest.spade.workflow.WorkflowTermination;
import gov.lbl.nest.spade.workflow.WorkflowsManager;
import gov.lbl.nest.spade.workflow.WorkflowsMonitor;
import gov.lbl.nest.spade.xpath.FnNamespaceContextImpl;
import gov.lbl.nest.spade.xpath.FnXPathFunctionResolverImpl;

/**
 * This class implements the {@link WorkflowsManager} interface for testing a
 * single workflow.
 *
 * @author patton
 *
 */
public class MockWorkflowsManager implements
                                  WorkflowsManager {

    /**
     * The directory in which to store Lazarus work files.
     */
    public static final File LAZARUS_DIR = new File("lazarus");

    /**
     * The name of the {@link DataObject} instance containing the ingest ticket.
     */
    private final static String INGESTION_TICKET = LoadedLazarusWorkflows.INGESTION_TICKET;

    /**
     * Create the {@link Process} instance that will be used by this Object.
     *
     * @param workflow
     *            the {@link Workflow} instance this Object is managing.
     * @param inputStream
     *            the {@link InputStream} containing the BPMN diagram of the
     *            workflow.
     *
     * @return the {@link Process} instance that will be used by this Object.
     *
     */
    private static Process createProcess(Workflow workflow,
                                         InputStream inputStream) throws Exception {
        final XMLDefinitions xmlDefintions = new XMLDefinitions(new FnNamespaceContextImpl(null),
                                                                new FnXPathFunctionResolverImpl(null),
                                                                null,
                                                                false);

        final Map<String, ? extends ProcessDefinition> processes = xmlDefintions.parseDefinitions(inputStream);
        if (1 != processes.size()) {
            throw new UnsupportedOperationException("Only a single Process per BPMN file can be handled.");
        }
        return processes.get(workflow.toString());
    }

    /**
     * The {@link Process} instance used by this Object.
     */
    private final Process process;

    /**
     * The {@link Workflow} instance this Object is managing.
     */
    private final Workflow workflow;

    /**
     * Creates an instance of this class.
     *
     * @throws Exception
     *             when there is a problem with execution.
     */
    public MockWorkflowsManager() throws Exception {
        this(null,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param workflow
     *            the {@link Workflow} instance this Object is managing. * @param
     *            inputStream the {@link InputStream} containing the BPMN diagram of
     *            the workflow.
     * @param inputStream
     *            the {@link InputStream} instance from which to build the
     *            {@link Process} instance.
     *
     * @throws Exception
     *             when there is a problem with execution.
     */
    public MockWorkflowsManager(Workflow workflow,
                                InputStream inputStream) throws Exception {
        if (null == inputStream) {
            process = null;
        } else {
            process = createProcess(workflow,
                                    inputStream);
        }
        this.workflow = workflow;
    }

    @Override
    public void drain(Workflow workflow) throws InitializingException {
        // TODO Auto-generated method stub

    }

    @Override
    public ActivityManager getActivityManager(Workflow workflow,
                                              String activity) throws InitializingException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ActivityMonitor getActivityMonitor(Workflow workflow,
                                              String activity) throws InitializingException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<? extends String> getActivityNames(Workflow workflow) throws InitializingException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AtomicInteger getCount(Workflow workflow) throws InitializingException {
        if (this.workflow != workflow) {
            throw new IllegalArgumentException();
        }
        return process.getCount();
    }

    @Override
    public WorkflowDirector getWorkflowDirector(Workflow workflow) throws InitializingException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public WorkflowsMonitor getWorkflowsMonitor() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isSuspended(Workflow workflow) throws InitializingException {
        return false;
    }

    @Override
    public void recommence(URI uri,
                           Object message) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<String> rescueWorkflows(Workflow workflow) throws InitializingException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean setSuspended(Workflow workflow,
                                boolean suspended) throws InitializingException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setTermination(Workflow workflow,
                               WorkflowTermination termination) throws InitializingException {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean startWorkflow(Workflow workflow,
                                 String name,
                                 Integer priority,
                                 Object dataObject) throws InitializingException {
        if (this.workflow != workflow) {
            throw new IllegalArgumentException();
        }
        try {
            final Map<String, Object> values = new HashMap<>();
            values.put(INGESTION_TICKET,
                       dataObject);

            final ProcessDefinition processDefinition = (ProcessDefinition) process;
            final ExecutableProcess executableProcess = processDefinition.createExecutableProcess(name,
                                                                                                  priority,
                                                                                                  values);
            ((ControlOnlyExecutableProcess) executableProcess).execute();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
