package gov.lbl.nest.spade.lazarus;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Supplier;

import jakarta.enterprise.concurrent.ContextService;
import jakarta.enterprise.concurrent.ManagedExecutorService;

/**
 * This class is uses to test preparation of workflows.
 *
 * @author patton
 */
public class MockManagedExecutorService implements
                                        ManagedExecutorService {

    @Override
    public boolean awaitTermination(long timeout,
                                    TimeUnit unit) throws InterruptedException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public <U> CompletableFuture<U> completedFuture(U value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <U> CompletionStage<U> completedStage(U value) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> CompletableFuture<T> copy(CompletableFuture<T> stage) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> CompletionStage<T> copy(CompletionStage<T> stage) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void execute(Runnable command) {
        // TODO Auto-generated method stub

    }

    @Override
    public <U> CompletableFuture<U> failedFuture(Throwable ex) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <U> CompletionStage<U> failedStage(Throwable ex) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ContextService getContextService() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks,
                                         long timeout,
                                         TimeUnit unit) throws InterruptedException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException,
                                                                    ExecutionException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks,
                           long timeout,
                           TimeUnit unit) throws InterruptedException,
                                          ExecutionException,
                                          TimeoutException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isShutdown() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isTerminated() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public <U> CompletableFuture<U> newIncompleteFuture() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CompletableFuture<Void> runAsync(Runnable runnable) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void shutdown() {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Runnable> shutdownNow() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Future<?> submit(Runnable task) {
        task.run();
//        final Thread thread = new Thread(task);
//        thread.start();
        return new Future<Boolean>() {

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public Boolean get() throws InterruptedException,
                                 ExecutionException {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public Boolean get(long timeout,
                               TimeUnit unit) throws InterruptedException,
                                              ExecutionException,
                                              TimeoutException {
//                thread.join(TimeUnit.MILLISECONDS.convert(timeout,
//                                                          unit));
                return null;
            }

            @Override
            public boolean isCancelled() {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean isDone() {
                // TODO Auto-generated method stub
                return false;
            }
        };
    }

    @Override
    public <T> Future<T> submit(Runnable task,
                                T result) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <U> CompletableFuture<U> supplyAsync(Supplier<U> supplier) {
        // TODO Auto-generated method stub
        return null;
    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
