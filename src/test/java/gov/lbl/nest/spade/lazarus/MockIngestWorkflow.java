package gov.lbl.nest.spade.lazarus;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.tasks.Consumer;
import gov.lbl.nest.lazarus.bpmn.XMLDefinitions;
import gov.lbl.nest.lazarus.execution.ProcessDefinition;
import gov.lbl.nest.spade.config.ApplicationSuspension;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.TicketManager;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowDirector;
import gov.lbl.nest.spade.xpath.FnNamespaceContextImpl;
import gov.lbl.nest.spade.xpath.FnXPathFunctionResolverImpl;
import jakarta.xml.bind.JAXBException;

/**
 * This class is uses to test the local warehouse flow only.
 *
 * @author patton
 */
public class MockIngestWorkflow extends
                                LoadedLazarusWorkflows {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} used by this object.
     */
//    private final Configuration configuration;

    /**
     * The name of the resource containing the BPMN for the ingest process.
     */
    private final String ingestBpmn;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     * @param configuration
     *            the {@link Configuration} instance of this application.
     * @param ticketManager
     *            the {@link TicketManager} used by this object.
     * @param ingestBpmn
     *            the name of the resource containing the BPMN for the ingest
     *            process.
     *
     * @throws InitializingException
     *             when the application is still initializing.
     */
    public MockIngestWorkflow(CacheManager cacheManager,
                              final Configuration configuration,
                              TicketManager ticketManager,
                              final String ingestBpmn) throws InitializingException {
        super(configuration,
              new ApplicationSuspension(configuration,
                                        false),
              new MockManagedExecutorService());
        this.ingestBpmn = ingestBpmn;
    }

    // instance member method (alphabetic)

    @Override
    protected void prepareWorkflows(List<Consumer> consumers,
                                    Path suspentionPath) throws NoSuchMethodException {
        final XMLDefinitions xmlDefintions = new XMLDefinitions(new FnNamespaceContextImpl(null),
                                                                new FnXPathFunctionResolverImpl(null),
                                                                null,
                                                                false);
        final Workflow workflow = Workflow.INGEST;

        try (InputStream inputStream = getClass().getResourceAsStream(ingestBpmn)) {
            final Map<String, ? extends ProcessDefinition> processes = xmlDefintions.parseDefinitions(inputStream);
            if (1 != processes.size()) {
                throw new UnsupportedOperationException("Only a single Process per BPMN file can be handled.");
            }
            final ProcessDefinition process = ((processes.values()).iterator()).next();
            if (!(process instanceof ProcessDefinition)) {
                throw new UnsupportedOperationException("The class " + (process.getClass()).getName()
                                                        + " is not currently supported");
            }
            final WorkflowDirector director = new LazarusWorkflowDirector(process.getProcessManager());
            addWorkflowDirector(workflow,
                                director);
            addWorkflow(workflow,
                        processes.get("INGEST"));
        } catch (ClassNotFoundException
                 | JAXBException
                 | IOException
                 | NoSuchElementException
                 | NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
