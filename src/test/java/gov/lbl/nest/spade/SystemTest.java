package gov.lbl.nest.spade;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import gov.lbl.nest.common.configure.InitParam;
import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendedException;
import gov.lbl.nest.common.testing.UnitTestUtility;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.activities.Analyzer;
import gov.lbl.nest.spade.activities.Examiner;
import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.Assembly;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.config.DiskSpace;
import gov.lbl.nest.spade.config.InboundTransfer;
import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.config.RegistrationPaths;
import gov.lbl.nest.spade.config.RootedPathDefinition;
import gov.lbl.nest.spade.ejb.Confirmation;
import gov.lbl.nest.spade.ejb.KnownInboundRegistration;
import gov.lbl.nest.spade.ejb.KnownLocalRegistration;
import gov.lbl.nest.spade.ejb.KnownNeighbor;
import gov.lbl.nest.spade.ejb.MockEntityManager;
import gov.lbl.nest.spade.ejb.ShippedFile;
import gov.lbl.nest.spade.ejb.TicketedFile;
import gov.lbl.nest.spade.interfaces.metadata.ChecksumFactory;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.lazarus.JavaInstanceFactoryImpl;
import gov.lbl.nest.spade.lazarus.JavaInstanceFactoryImpl.ExecutionEnvironment;
import gov.lbl.nest.spade.lazarus.LoadedLazarusWorkflows;
import gov.lbl.nest.spade.lazarus.MockIngestWorkflow;
import gov.lbl.nest.spade.metadata.impl.MetadataImplManager;
import gov.lbl.nest.spade.registry.LocalRegistration;
import gov.lbl.nest.spade.registry.internal.InboundRegistration;
import gov.lbl.nest.spade.rs.Bundles;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.InProgressException;
import gov.lbl.nest.spade.services.MockNullWorkflow;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import gov.lbl.nest.spade.services.RegistrationManager;
import gov.lbl.nest.spade.services.RetryManager;
import gov.lbl.nest.spade.services.Spade;
import gov.lbl.nest.spade.services.TicketManager;
import gov.lbl.nest.spade.services.TooManyTicketsException;
import gov.lbl.nest.spade.services.impl.CacheManagerImpl;
import gov.lbl.nest.spade.services.impl.LoadedNeighborhood;
import gov.lbl.nest.spade.services.impl.LoadedRegistrations;
import gov.lbl.nest.spade.services.impl.LocalhostTransfer;
import gov.lbl.nest.spade.services.impl.MockBookkeeping;
import gov.lbl.nest.spade.services.impl.MockIngestTermination;
import gov.lbl.nest.spade.services.impl.MockOffloadedTransfer;
import gov.lbl.nest.spade.services.impl.NeighborhoodManagerImpl;
import gov.lbl.nest.spade.services.impl.RegistrationManagerImpl;
import gov.lbl.nest.spade.services.impl.RetryManagerImpl;
import gov.lbl.nest.spade.services.impl.SpadeImpl;
import gov.lbl.nest.spade.services.impl.TicketManagerImpl;
import gov.lbl.nest.spade.services.impl.VerificationManagerImpl;
import gov.lbl.nest.spade.services.impl.WarehouseManagerImpl;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowsManager;
import gov.lbl.nest.spade.workflow.impl.WorkflowsManagerImpl;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;

/**
 * This class test the SPADE system behaves as expected.
 *
 * @author patton
 */
@DisplayName("System Tests")
public class SystemTest {

    private static class Definition {

        final File cacheDirectory;

        final File configuration;

        final List<InboundTransfer> inboundTransfers;

        final List<OutboundTransfer> outboundTransfers;

        final RegistrationPaths registrationPaths;

        File warehouse;

        Definition(final File cacheDirectory,
                   final File configuration,
                   final List<InboundTransfer> inboundTransfers,
                   final List<OutboundTransfer> outboundTransfers,
                   final RegistrationPaths registrationPaths,
                   final File warehouse) {
            this.cacheDirectory = cacheDirectory;
            this.configuration = configuration;
            this.inboundTransfers = inboundTransfers;
            this.outboundTransfers = outboundTransfers;
            this.registrationPaths = registrationPaths;
            this.warehouse = warehouse;
        }
    }

    private class MockDeployment {

        /**
         * The {@link Bookkeeping} used by this deployment.
         */
        Bookkeeping bookkeeping;

        /**
         * The {@link CacheDefinition} used by this deployment.
         */
        CacheDefinition cacheDefinition;

        /**
         * The {@link CacheManager} used by this deployment.
         */
        CacheManager cacheManager;

        /**
         * The {@link Configuration} used by this deployment.
         */
        Configuration configuration;

        /**
         * The {@link LoadedRegistrations} used by this deployment.
         */
        LoadedRegistrations loadedRegistrations;

        /**
         * The {@link MockEntityManager} used by this deployment.
         */
        MockEntityManager entityManager;

        /**
         * The {@link MetadataManager} used by this deployment.
         */
        MetadataManager metadataManager;

        /**
         * The {@link NeighborhoodManager} used by this deployment.
         */
        NeighborhoodManager neighborhoodManager;

        /**
         * The {@link RegistrationManager} used by this deployment.
         */
        RegistrationManager registrationManager;

        /**
         * The {@link RegistrationManager} used by this deployment.
         */
        RetryManager retryManager;

        /**
         * The {@link MockIngestTermination} used by this deployment.
         */
        MockIngestTermination terminationListener;

        /**
         * The {@link Spade} instance being deployed.
         */
        Spade spade;

        /**
         * The {@link TicketManager} used by this deployment.
         */
        TicketManager ticketManager;

        MockDeployment(final String name,
                       final Definition definition) {
            final List<InitParam> initParams = new ArrayList<>();
            initParams.add(new InitParam("command",
                                         ANALYSIS_COMMAND));
            initParams.add(new InitParam("output",
                                         ANALYSIS_OUTPUT_DIR.getPath()));
            initParams.add(new InitParam("fatal",
                                         ANALYSIS_FAILURE_IS_FATAL));
            final List<Activity> activities = new ArrayList<>();
            activities.add(new Activity(Activity.ANALYZER,
                                        initParams));
            final Assembly assembly = new Assembly(name,
                                                   activities);
            cacheDefinition = new CacheDefinition((definition.cacheDirectory).getPath(),
                                                  new DiskSpace(DiskSpace.GB,
                                                                1));
            cacheManager = new CacheManagerImpl();
            final RegistrationPaths registrationPaths = definition.registrationPaths;
            final File warehouseRoot = new File(".",
                                                (definition.warehouse).getPath());
            final RootedPathDefinition warehouseDefinition = new RootedPathDefinition(warehouseRoot);
            configuration = new Configuration(definition.configuration,
                                              assembly,
                                              cacheDefinition,
                                              definition.inboundTransfers,
                                              definition.outboundTransfers,
                                              registrationPaths,
                                              warehouseDefinition);
            entityManager = new MockEntityManager();
            final LoadedNeighborhood loadedNeighborhood = new LoadedNeighborhood(configuration,
                                                                                 entityManager);
            loadedRegistrations = new LoadedRegistrations(configuration,
                                                          entityManager,
                                                          loadedNeighborhood);
            neighborhoodManager = new NeighborhoodManagerImpl(configuration,
                                                              loadedNeighborhood,
                                                              null);
            registrationManager = new RegistrationManagerImpl(loadedRegistrations);
            ticketManager = new TicketManagerImpl(entityManager);
            terminationListener = new MockIngestTermination(ticketManager);
            retryManager = new RetryManagerImpl(cacheManager,
                                                configuration.getCacheMap());
            bookkeeping = new MockBookkeeping(entityManager);
            metadataManager = new MetadataImplManager();
        }
    }

    // public static final member data

    // protected static final member data

    // static final member data

    /**
     * The command to run when testing the analysis activity.
     */
    static final String ANALYSIS_COMMAND = "src/test/bash/analyze.sh {0} {1} {2}";

    /**
     * The directory into which to write the output files when testing the analysis
     * activity.
     */
    static final File ANALYSIS_OUTPUT_DIR = new File("analysis");

    /**
     * True is the {@link Analyzer} activity should fail its workflow if it does not
     * execute successfully.
     */
    static final String ANALYSIS_FAILURE_IS_FATAL = (Boolean.TRUE).toString();

    /**
     * The name of the default configuration directory.
     */
    static final File SPADE_DIRECTORY = new File("spade");

    /**
     * The name of the directory used as the 'cache' for testing.
     */
    static final File CACHE_DIRECTORY = new File("cache");

    /**
     * The name of the directory used as the 'lazarus' cache for testing.
     */
    static final String LAZARUS_ROOT = "lazarus";

    /**
     * The {@link RegistrationPaths} used by the test deployment.
     */
    private final static RegistrationPaths REGISTRATION_PATHS = new RegistrationPaths("../src/test/files/registrations/local",
                                                                                      "../src/test/files/registrations/inbound");
    /**
     * The name of the directory used as the 'warehouse' for testing.
     */
    static final File WAREHOUSE = new File("warehouse");

    /**
     * The name of the directory used as the 'receiving' area for testing.
     */
    private static final File RECEIVING = new File("receiving");

    /**
     * The name of the directory used as to hold files whose transfer may be
     * retried.
     */
    private static final File RESHIP = new File(CACHE_DIRECTORY,
                                                "reship");

    /**
     * The receiving area for the 'one' neighbor.
     */
    private static final File RECEIVING_ONE = new File(new File(RECEIVING,
                                                                "example.com"),
                                                       "one");

    /**
     * The receiving area for the 'two' neighbor.
     */
    private static final File RECEIVING_TWO = new File(new File(RECEIVING,
                                                                "example.com"),
                                                       "two");

    /**
     * The receiving area for the 'three' neighbor.
     */
    private static final File RECEIVING_THREE = new File(new File(RECEIVING,
                                                                  "example.com"),
                                                         "three");

    /**
     * The receiving area for the 'four' neighbor.
     */
    private static final File RECEIVING_FOUR = new File(new File(RECEIVING,
                                                                 "example.com"),
                                                        "four");

    /**
     * The receiving area for the 'five' neighbor.
     */
    private static final File RECEIVING_FIVE = new File(new File(RECEIVING,
                                                                 "example.com"),
                                                        "five");

    /**
     * The receiving area for the 'six' neighbor.
     */
    private static final File RECEIVING_SIX = new File(new File(RECEIVING,
                                                                "example.com"),
                                                       "six");

    /**
     * The receiving area for the 'seven' neighbor.
     */
    private static final File RECEIVING_SEVEN = new File(new File(RECEIVING,
                                                                  "example.com"),
                                                         "seven");

    /**
     * The receiving area for the 'eight' neighbor.
     */
    private static final File RECEIVING_EIGHT = new File(new File(RECEIVING,
                                                                  "example.com"),
                                                         "eight");

    private static final List<String> DESTINATIONS_1 = Arrays.asList(new String[] { "localhost:" + RECEIVING_ONE.toString() });
    private static final List<String> DESTINATIONS_2 = Arrays.asList(new String[] { "localhost:" + RECEIVING_TWO.toString() });
    private static final List<String> DESTINATIONS_4 = Arrays.asList(new String[] { "localhost:" + RECEIVING_FOUR.toString() });
    private static final List<String> DESTINATIONS_6 = Arrays.asList(new String[] { "localhost:" + RECEIVING_SIX.toString() });
    private static final List<String> DESTINATIONS_8 = Arrays.asList(new String[] { "localhost:" + RECEIVING_EIGHT.toString() });
    static final List<OutboundTransfer> OUTBOUND_TRANSFERS = Arrays.asList(new OutboundTransfer[] { new OutboundTransfer("one",
                                                                                                                         null,
                                                                                                                         "one@example.com",
                                                                                                                         DESTINATIONS_1,
                                                                                                                         new LocalhostTransfer()),
                                                                                                    new OutboundTransfer("two",
                                                                                                                         null,
                                                                                                                         "two@example.com",
                                                                                                                         DESTINATIONS_2,
                                                                                                                         new LocalhostTransfer()),
                                                                                                    new OutboundTransfer("four",
                                                                                                                         null,
                                                                                                                         "four@example.com",
                                                                                                                         DESTINATIONS_4,
                                                                                                                         new LocalhostTransfer()),
                                                                                                    new OutboundTransfer("six",
                                                                                                                         null,
                                                                                                                         "six@example.com",
                                                                                                                         DESTINATIONS_6,
                                                                                                                         new LocalhostTransfer()),
                                                                                                    new OutboundTransfer("eight",
                                                                                                                         null,
                                                                                                                         "eight@example.com",
                                                                                                                         DESTINATIONS_8,
                                                                                                                         new MockOffloadedTransfer()) });

    static final List<InboundTransfer> INBOUND_TRANSFERS = Arrays.asList(new InboundTransfer[] { new InboundTransfer("three",
                                                                                                                     null,
                                                                                                                     "three@example.com",
                                                                                                                     "localhost:" + RECEIVING_THREE.toString()),
                                                                                                 new InboundTransfer("five",
                                                                                                                     null,
                                                                                                                     "five@example.com",
                                                                                                                     "localhost:" + RECEIVING_FIVE.toString()),
                                                                                                 new InboundTransfer("seven",
                                                                                                                     null,
                                                                                                                     "seven@example.com",
                                                                                                                     "localhost:" + RECEIVING_SEVEN.toString()) });

    static final List<InboundTransfer> INBOUND_TRANSFERS_2 = Arrays.asList(new InboundTransfer[] { new InboundTransfer("alpha",
                                                                                                                       null,
                                                                                                                       "zero@example.com",
                                                                                                                       "localhost:" + RECEIVING_ONE.toString()) });

    static final List<InboundTransfer> INBOUND_TRANSFERS_3 = Arrays.asList(new InboundTransfer[] { new InboundTransfer("omega",
                                                                                                                       null,
                                                                                                                       "zero@example.com",
                                                                                                                       "localhost:" + RECEIVING_TWO.toString()) });

    // private static final member data

    /**
     * The suffix used to mark the data directory for a ticket. (Must match the
     * declaration in {@link Examiner} with the leading separator)
     */
    private static final String TICKET_PAYLOAD_DIRECTORY_SUFFIX = "payload";

    /**
     * The name of the resource containing the BPMN for the simple local warehouse
     * process.
     */
    private final static String LOCAL_WAREHOUSE_BPMN = "local_warehouse.bpmn";

    /**
     * The name of the resource containing the BPMN for the simple local warehouse
     * process whether the warehousing is optional.
     */
    private final static String LOCAL_WAREHOUSE_OPTIONAL_BPMN = "local_warehouse_optional.bpmn";

    /**
     * The name of the resource containing the BPMN for the simple local warehouse
     * process.
     */
    private final static String LOCAL_WAREHOUSE_ANALYZE_BPMN = "local_warehouse_analyze.bpmn";

    /**
     * The name of the resource containing the BPMN for the simple outbound process.
     */
    private final static String OUTBOUND_TRANSFER_BPMN = "outbound_transfer.bpmn";

    /**
     * The name of the resource containing the BPMN for the simple outbound process.
     */
    private final static String OUTBOUND_TRANSFER_PACK_BPMN = "outbound_transfer_pack.bpmn";

    /**
     * The name of the resource containing the BPMN for the simple outbound process.
     */
    private final static String OUTBOUND_TRANSFER_COMPRESS_BPMN = "outbound_transfer_compress.bpmn";

    /**
     * The name of the resource containing the BPMN for the simple inbound process.
     */
    private final static String INBOUND_TRANSFER_BPMN = "inbound_transfer.bpmn";

    /**
     * The name of the resource containing the BPMN for the inbound process that
     * unpacks a packed bundle.
     */
    private final static String INBOUND_TRANSFER_UNPACK_BPMN = "inbound_transfer_unpack.bpmn";

    /**
     * The name of the resource containing the BPMN for the inbound process that
     * unpacks a packed bundle.
     */
    private final static String INBOUND_TRANSFER_EXPAND_BPMN = "inbound_transfer_expand.bpmn";

    /**
     * The name of the directory used as the 'dropbox' for testing.
     */
    private static final File DROPBOX = new File("dropbox");

    /**
     * The name of the data file associated with file.1.
     */
    private static final String FILE_1_DATA = "file.1.data";

    /**
     * The name of the metadata file associated with file.1.
     */
    private static final String FILE_1_META = "file.1.meta.xml";

    /**
     * The name of the semaphore file associated with file.1.
     */
    private static final String FILE_1_SEMA = "file.1.sem";

    /**
     * The directory, in the warehouse, where the "file.1" files should have been
     * placed.
     */
    private static final File FILE_1_WAREHOUSE = new File(WAREHOUSE,
                                                          "67/A6322E04F9A89147341153A75792D3280E0B14");

    /**
     * The name of the data file associated with file.2.
     */
    private static final String FILE_2_DATA = "file.2.tgz";

    /**
     * The name of the metadata file associated with file.2.
     */
    private static final String FILE_2_META = "file.2.meta.xml";

    /**
     * The name of the semaphore file associated with file.2.
     */
    private static final String FILE_2_SEMA = "file.2.lem";

    /**
     * The directory, in the warehouse, where the "file.2" files should have been
     * placed.
     */
    private static final File FILE_2_WAREHOUSE = new File(WAREHOUSE,
                                                          "19/0E1D99FEF4462069A60F04FA38571CCB56B816");

    /**
     * The name of the data file associated with file.5.
     */
    private static final String FILE_5_DATA = "file.5.gif";

    /**
     * The name of the metadata file associated with file.5.
     */
    private static final String FILE_5_META = "file.5.meta.xml";

    /**
     * The name of the semaphore file associated with file.5.
     */
    private static final String FILE_5_SEMA = "file.5.xem";

    /**
     * The name of the semaphore file associated with the delivered file.5.
     */
    private static final String DELIVERED_FILE_5_DONE = "v4_0_DONE_outbound.transfer.5_file.5";

    /**
     * The name of the payload file associated with the delivered file.5.
     */
    private static final String DELIVERED_FILE_5_PAYLOAD = "v4_0_DATA_file.5.gif";

    /**
     * The name of the metadata file associated with the delivered file.5.
     */
    private static final String DELIVERED_FILE_5_META = "v4_0_METADATA_file.5.meta.xml";

    /**
     * The name of the inbound payload file associated with file.6.
     */
    private static final String DELIVERED_FILE_6_PAYLOAD = "v4_0_DATA_file.6.tgz";

    /**
     * The name of the metadata file associated with file.5.
     */
    private static final String DELIVERED_FILE_6_META = "v4_0_METADATA_file.6.meta.xml";

    /**
     * The name of the semaphore file associated with file.6.
     */
    private static final String DELIVERED_FILE_6_SEMA = "v4_0_DONE_outbound.transfer.6_file.6";

    /**
     * The name of the bundle associated with file.7.
     */
    private static final String FILE_7_BUNDLE = "file.7";

    /**
     * The name of the data file associated with file.5.
     */
    private static final String FILE_7_DATA = "file.7.tgz";

    /**
     * The name of the metadata file associated with file.5.
     */
    private static final String FILE_7_META = "file.7.meta.xml";

    /**
     * The algorithm used to calculate the checksum of the transfer file associated
     * with file.7.
     */
    private static final String FILE_7_ALGORITHM = ChecksumFactory.CRC32_TYPE;

    /**
     * The checksum of the transfer file associated with file.7.
     */
    private static final Long FILE_7_CHECKSUM = 9876543210L;

    /**
     * The local id associated with file.7 on the local SPADE instance.
     */
    private static final String FILE_7_INBOUND_REGISTRATION = "inbound.transfer.7";

    /**
     * The local id associated with file.7 on the local SPADE instance.
     */
    private static final String FILE_7_OUTBOUND_REGISTRATION = "outbound.transfer.7";

    /**
     * The name of the data file associated with file.8.
     */
    private static final String FILE_8_DATA = "file.8.asc";

    /**
     * The name of the metadata file associated with file.8.
     */
    private static final String FILE_8_META = "file.8.meta.xml";

    /**
     * The name of the semaphore file associated with file.8.
     */
    private static final String FILE_8_SEMA = "file.8.pem";

    /**
     * The name of the semaphore file associated with the delivered file.8.
     */
    private static final String DELIVERED_FILE_8_DONE = "v4_0_DONE_outbound.transfer.packed.8_file.8";

    /**
     * The name of the payload file associated with the delivered file.8.
     */
    private static final String DELIVERED_FILE_8_PAYLOAD = "v4_0_PACKED_file.8.tar";

    /**
     * The name of the metadata file associated with the delivered file.8.
     */
    private static final String DELIVERED_FILE_8_META = "v4_0_METADATA_file.8.meta.xml";

    /**
     * The name of the payloas file associated with file.9.
     */
    private static final String DELIVERED_FILE_9_PAYLOAD = "v4_0_PACKED_file.9.tar";

    /**
     * The name of the metadata file associated with file.9.
     */
    private static final String DELIVERED_FILE_9_META = "v4_0_METADATA_file.9.meta.xml";

    /**
     * The name of the semaphore file associated with file.9.
     */
    private static final String DELIVERED_FILE_9_SEMA = "v4_0_DONE_outbound.transfer.packed.9_file.9";

    /**
     * The directory, in the warehouse, where the "file.1" files should have been
     * placed.
     */
    private static final File FILE_9_WAREHOUSE = new File(WAREHOUSE,
                                                          "62/EEADDA71728CD236D7E5D3368DBAF7F6A0B7CE");

    /**
     * The bundle name associated with file.9.
     */
    private static final String FILE_9_BUNDLE = "file.9";

    /**
     * The name of the first data file associated with file.9.
     */
    private static final String FILE_9_1_DATA = "file.9.1.asc";

    /**
     * The name of the second data file associated with file.9.
     */
    private static final String FILE_9_2_DATA = "file.9.2.asc";

    /**
     * The name of the metadata file associated with file.9.
     */
    private static final String FILE_9_META = "file.9.meta.xml";

    /**
     * The name of the data file associated with file.10.
     */
    private static final String FILE_10_DATA = "file.10.tif";

    /**
     * The name of the metadata file associated with file.10.
     */
    private static final String FILE_10_META = "file.10.meta.xml";

    /**
     * The name of the semaphore file associated with file.10.
     */
    private static final String FILE_10_SEMA = "file.10.zem";

    /**
     * The name of the semaphore file associated with the delivered file.10.
     */
    private static final String DELIVERED_FILE_10_DONE = "v4_0_DONE_outbound.transfer.compress.10_file.10";

    /**
     * The name of the payload file associated with the delivered file.10.
     */
    private static final String DELIVERED_FILE_10_PAYLOAD = "v4_0_COMPRESSED_file.10.tgz";

    /**
     * The name of the metadata file associated with the delivered file.10.
     */
    private static final String DELIVERED_FILE_10_META = "v4_0_METADATA_file.10.meta.xml";

    /**
     * The name of the payloas file associated with file.11.
     */
    private static final String DELIVERED_FILE_11_PAYLOAD = "v4_0_COMPRESSED_file.11.tgz";

    /**
     * The name of the metadata file associated with file.11.
     */
    private static final String DELIVERED_FILE_11_META = "v4_0_METADATA_file.11.meta.xml";

    /**
     * The name of the semaphore file associated with file.11.
     */
    private static final String DELIVERED_FILE_11_SEMA = "v4_0_DONE_outbound.transfer.compress.11_file.11";

    /**
     * The directory, in the warehouse, where the "file.1" files should have been
     * placed.
     */
    private static final File FILE_11_WAREHOUSE = new File(WAREHOUSE,
                                                           "1F/755DBD7B448E004778987DC143FFC09450879F");

    /**
     * The bundle name associated with file.11.
     */
    private static final String FILE_11_BUNDLE = "file.11";

    /**
     * The name of the first data file associated with file.11.
     */
    private static final String FILE_11_1_DATA = FILE_11_BUNDLE + ".1.asc";

    /**
     * The name of the second data file associated with file.11.
     */
    private static final String FILE_11_2_DATA = FILE_11_BUNDLE + ".2.asc";

    /**
     * The name of the metadata file associated with file.11.
     */
    private static final String FILE_11_META = FILE_11_BUNDLE + ".meta.xml";

    /**
     * The bundle name associated with file.12.
     */
    private static final String FILE_12_BUNDLE = "file.12";

    /**
     * The name of the data file associated with file.1.
     */
    private static final String FILE_12_DATA = FILE_12_BUNDLE + ".csv";

    /**
     * The name of the metadata file associated with file.1.
     */
    private static final String FILE_12_META = FILE_12_BUNDLE + ".meta.xml";

    /**
     * The name of the semaphore file associated with file.1.
     */
    private static final String FILE_12_SEMA = FILE_12_BUNDLE + ".aem";

    /**
     * The directory, in the warehouse, where the "file.12" files should have been
     * placed.
     */
    private static final File FILE_12_WAREHOUSE = new File(WAREHOUSE,
                                                           "73/13B6917C24461E1A14CCBB56766A6B436E6C87");

    /**
     * The name of the data file associated with file.13.
     */
    private static final String FILE_13_DATA = "file.13.png";

    /**
     * The name of the metadata file associated with file.13.
     */
    private static final String FILE_13_META = "file.13.meta.xml";

    /**
     * The name of the semaphore file associated with file.13.
     */
    private static final String FILE_13_SEMA = "file.13.oem";

    /**
     * The name of the semaphore file associated with the delivered file.5.
     */
    private static final String DELIVERED_FILE_13_DONE = "v4_0_DONE_outbound.transfer.13_file.13";

    /**
     * The name of the payload file associated with the delivered file.5.
     */
    private static final String DELIVERED_FILE_13_PAYLOAD = "v4_0_DATA_file.13.png";

    /**
     * The name of the metadata file associated with the delivered file.5.
     */
    private static final String DELIVERED_FILE_13_META = "v4_0_METADATA_file.13.meta.xml";

    /**
     * The drop box for the 'local warehouse' tests.
     */
    private static final File LOCAL_WAREHOUSE_DROPBOX = new File(DROPBOX,
                                                                 "local_warehouse");

    /**
     * The drop box for the 'outbound transfer' tests.
     */
    private static final File OUTBOUND_TRANSFER_DROPBOX = new File(DROPBOX,
                                                                   "outbound_transfer");
    /**
     * The definition of the test SPADE deployment.
     */
    private static final Definition DEFINITION = new Definition(CACHE_DIRECTORY,
                                                                new File("spade.xml"),
                                                                INBOUND_TRANSFERS,
                                                                OUTBOUND_TRANSFERS,
                                                                REGISTRATION_PATHS,
                                                                WAREHOUSE);
    /*
     * Declare the first destination SPADE.
     */

    /**
     * The name of the directory used as the 'cache' for testing.
     */
    static final File CACHE_DIRECTORY_2 = new File("cache.2");

    /**
     * The definition, if required, of the "remote" SPADE deployment.
     */
    private final static RegistrationPaths REGISTRATION_PATHS_2 = new RegistrationPaths("../src/test/files/registrations.2/local",
                                                                                        "../src/test/files/registrations.2/inbound");

    /**
     * The name of the directory used as the 'warehouse' for testing.
     */
    static final File WAREHOUSE_2 = new File("warehouse.2");

    private static final Definition DEFINITION_2 = new Definition(CACHE_DIRECTORY_2,
                                                                  new File("spade.2.xml"),
                                                                  INBOUND_TRANSFERS_2,
                                                                  null,
                                                                  REGISTRATION_PATHS_2,
                                                                  WAREHOUSE_2);

    /*
     * Declare the second destination SPADE.
     */

    /**
     * The name of the directory used as the 'cache' for testing.
     */
    static final File CACHE_DIRECTORY_3 = new File("cache.3");

    /**
     * The definition, if required, of the "remote" SPADE deployment.
     */
    private final static RegistrationPaths REGISTRATION_PATHS_3 = new RegistrationPaths("../src/test/files/registrations.3/local",
                                                                                        "../src/test/files/registrations.3/inbound");

    /**
     * The name of the directory used as the 'warehouse' for testing.
     */
    static final File WAREHOUSE_3 = new File("warehouse.3");

    private static final Definition DEFINITION_3 = new Definition(CACHE_DIRECTORY_3,
                                                                  new File("spade.3.xml"),
                                                                  INBOUND_TRANSFERS_3,
                                                                  null,
                                                                  REGISTRATION_PATHS_3,
                                                                  WAREHOUSE_3);

    /*
     * Declare the second destination SPADE.
     */

    /**
     * The name of the directory used as the 'cache' for testing.
     */
    static final File CACHE_DIRECTORY_4 = new File("cache.4");

    /*
     * Declare the second destination SPADE.
     */

    /**
     * The name of the directory used as the 'cache' for testing.
     */
    static final File CACHE_DIRECTORY_5 = new File("cache.5");

    /*
     * Declare the second destination SPADE.
     */

    /**
     * The name of the directory used as the 'cache' for testing.
     */
    static final File CACHE_DIRECTORY_6 = new File("cache.6");

    /*
     * Declare the second destination SPADE.
     */

    /**
     * The name of the directory used as the 'cache' for testing.
     */
    static final File CACHE_DIRECTORY_7 = new File("cache.7");

    // private static member data

    // private instance member data

    private static void cleanUpTrees() {
        UnitTestUtility.deleteTree(SPADE_DIRECTORY);

        UnitTestUtility.deleteTree(CACHE_DIRECTORY);
        UnitTestUtility.deleteTree(DROPBOX);
        UnitTestUtility.deleteTree(RECEIVING);
        UnitTestUtility.deleteTree(WAREHOUSE);

        UnitTestUtility.deleteTree(CACHE_DIRECTORY_2);

        UnitTestUtility.deleteTree(CACHE_DIRECTORY_3);

        UnitTestUtility.deleteTree(CACHE_DIRECTORY_4);

        UnitTestUtility.deleteTree(CACHE_DIRECTORY_5);

        UnitTestUtility.deleteTree(CACHE_DIRECTORY_6);

        UnitTestUtility.deleteTree(CACHE_DIRECTORY_7);

        UnitTestUtility.deleteTree(ANALYSIS_OUTPUT_DIR);
    }

    // constructors

    // instance member method (alphabetic)

    /**
     * Create a {@link Spade} instance with which to run a test.
     *
     * @param deployment
     *            the {@link MockDeployment} for which the SPADE should be created.
     * @param workflowManager
     *            the {@link WorkflowsManager} implementation with which to build
     *            the {@link Spade} instance.
     *
     * @throws InitializingException
     */
    private static void createSpade(MockDeployment deployment,
                                    WorkflowsManager workflowManager) throws InitializingException {
        if (null != workflowManager) {
            workflowManager.setTermination(Workflow.INGEST,
                                           deployment.terminationListener);
        }
        deployment.spade = new SpadeImpl(deployment.configuration,
                                         deployment.bookkeeping,
                                         deployment.loadedRegistrations,
                                         deployment.neighborhoodManager,
                                         deployment.registrationManager,
                                         null,
                                         deployment.ticketManager,
                                         new VerificationManagerImpl(deployment.cacheManager,
                                                                     deployment.loadedRegistrations,
                                                                     deployment.metadataManager,
                                                                     deployment.neighborhoodManager,
                                                                     deployment.retryManager,
                                                                     deployment.entityManager),
                                         workflowManager);
    }

    /**
     * Returns a {@link Bundles} instance read from an XML representation of it
     * provided by the specified {@link InputStream}.
     *
     * @param stream
     *            the {@link InputStream} containing the XML representation.
     *
     * @return the instance of this class read from the {@link InputStream}.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     */
    public static Bundles readBundles(final InputStream stream) throws JAXBException {
        JAXBContext content = JAXBContext.newInstance(Bundles.class);
        Unmarshaller unmarshaller = content.createUnmarshaller();
        return (Bundles) unmarshaller.unmarshal(stream);
    }

    /**
     * The deployment of SPADE bing tested.
     */
    private MockDeployment testObject;

    private String beginOffloadedTransfer(final String inputBundles,
                                          final String expectedBundles,
                                          final LoadedLazarusWorkflows loadedWorkflows) throws IOException,
                                                                                        InitializingException,
                                                                                        JAXBException,
                                                                                        FileNotFoundException,
                                                                                        InterruptedException,
                                                                                        SuspendedException,
                                                                                        TooManyTicketsException,
                                                                                        InProgressException {
        JavaInstanceFactoryImpl.setManualExecutionEnvironment(getMockExecutionEnvironment());
        final WorkflowsManagerImpl workflowManager = new WorkflowsManagerImpl(loadedWorkflows);
        createSpade(testObject,
                    workflowManager);
        final Bundles bundles = readBundles(new FileInputStream(inputBundles));
        final AtomicReference<String> filePath = MockOffloadedTransfer.filePath;

        final Bundles results = (testObject.spade).ingest(bundles);
        synchronized (filePath) {
            while (null == filePath.get()) {
                filePath.wait(1);
            }
        }

        final Bundles actual = new Bundles(null,
                                           results);
        final Bundles expected = readBundles(new FileInputStream(expectedBundles));
        assertEquals(expected.toString(),
                     actual.toString(),
                     "Incorrect result returned from \"ingest\" invocation");
        assertEquals(0,
                     (testObject.terminationListener).getFailedCount(),
                     "At least one workflow failed");
        return filePath.get();
    }

    private void completeOffloadedTransfer(final String uriFile) throws FileNotFoundException,
                                                                 URISyntaxException,
                                                                 InterruptedException,
                                                                 InitializingException {
        final String urlAsString;
        final File source = new File(uriFile);
        try (Scanner scanner = new Scanner(source)) {
            Scanner s = scanner.useDelimiter("\\Z");
            urlAsString = s.next();
        }
        source.delete();
        testObject.spade.recommence(new URI(urlAsString),
                                    null);
        final AtomicInteger ingestCount = (testObject.spade).getCount(Workflow.INGEST);
        synchronized (ingestCount) {
            while (0 != ingestCount.get()) {
                ingestCount.wait();
            }
        }
    }

    private void confirmFromDestination(final Definition definition,
                                        final String neighbor) throws InitializingException {
        final MockDeployment target = new MockDeployment("remote",
                                                         definition);
        final InboundRegistration registration = (InboundRegistration) target.registrationManager.getRegistration(FILE_7_INBOUND_REGISTRATION);
        final KnownNeighbor knownNeighbor = new KnownNeighbor("zero@example.com");
        (target.entityManager).persist(knownNeighbor);
        final KnownInboundRegistration knownInboundRegistration = new KnownInboundRegistration(registration,
                                                                                               knownNeighbor);
        (target.entityManager).persist(knownInboundRegistration);

        final TicketedFile ticketedFile = new TicketedFile(FILE_7_BUNDLE,
                                                           null,
                                                           knownInboundRegistration);
        (target.entityManager).persist(ticketedFile);
        final ShippedFile shippedFile = new ShippedFile(ticketedFile,
                                                        "0",
                                                        FILE_7_OUTBOUND_REGISTRATION);
        shippedFile.setAlgorithm(FILE_7_ALGORITHM);
        shippedFile.setChecksum(FILE_7_CHECKSUM);
        shippedFile.setWhenConfirmable(new Date());
        (target.entityManager).persist(shippedFile);
        createSpade(target,
                    null);
        final List<DigestChange> confirmable = (target.spade).getConfirmableByTime(null,
                                                                                   null,
                                                                                   false,
                                                                                   -1,
                                                                                   null,
                                                                                   null);

        final List<? extends DigestChange> confirmed = (testObject.spade).delivery(neighbor,
                                                                                   confirmable);
        assertNotNull(confirmed,
                      "No confirmed object was returned");
        assertEquals(1,
                     confirmed.size(),
                     "Wrong number of confirmed bundles was returned");
    }

    /**
     * Creates the standard sets of data files to test inbound transfers.
     *
     * @throws IOException
     */
    private void createInboundTransferDataFiles() throws IOException {
        RECEIVING_THREE.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(RECEIVING_THREE,
                                                        DELIVERED_FILE_6_META));
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(RECEIVING_THREE,
                                                        DELIVERED_FILE_6_PAYLOAD));
    }

    /**
     * Creates the standard sets of data files to test inbound transfers.
     *
     * @throws IOException
     */
    private void createInboundTransferDataSemaphores() throws IOException {
        RECEIVING_THREE.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(RECEIVING_THREE,
                                                        DELIVERED_FILE_6_SEMA));
    }

    /**
     * Creates the standard sets of data files to test inbound transfers.
     *
     * @throws IOException
     */
    private void createInboundTransferExpandFiles() throws IOException {
        RECEIVING_SEVEN.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(RECEIVING_SEVEN,
                                                        DELIVERED_FILE_11_META));
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(RECEIVING_SEVEN,
                                                        DELIVERED_FILE_11_PAYLOAD));
    }

    /**
     * Creates the standard sets of data files to test inbound transfers.
     *
     * @throws IOException
     */
    private void createInboundTransferExpandSemaphores() throws IOException {
        RECEIVING_SEVEN.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(RECEIVING_SEVEN,
                                                        DELIVERED_FILE_11_SEMA));
    }

    /**
     * Creates the standard sets of data files to test inbound transfers.
     *
     * @throws IOException
     */
    private void createInboundTransferUnpackFiles() throws IOException {
        RECEIVING_FIVE.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(RECEIVING_FIVE,
                                                        DELIVERED_FILE_9_META));
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(RECEIVING_FIVE,
                                                        DELIVERED_FILE_9_PAYLOAD));
    }

    /**
     * Creates the standard sets of data files to test inbound transfers.
     *
     * @throws IOException
     */
    private void createInboundTransferUnpackSemaphores() throws IOException {
        RECEIVING_FIVE.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(RECEIVING_FIVE,
                                                        DELIVERED_FILE_9_SEMA));
    }

    /**
     * Creates the standard sets of data files in each bundle.
     *
     * @throws IOException
     */
    private void createLocalWarehouseAnalyzeFiles() throws IOException {
        LOCAL_WAREHOUSE_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(LOCAL_WAREHOUSE_DROPBOX,
                                                        FILE_12_DATA));
    }

    /**
     * Creates the standard pair of semaphore files in a dropbox.
     *
     * @throws IOException
     */
    private void createLocalWarehouseAnalyzeSemaphores() throws IOException {
        LOCAL_WAREHOUSE_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(LOCAL_WAREHOUSE_DROPBOX,
                                                        FILE_12_SEMA));
    }

    /**
     * Creates the standard sets of data files in each bundle.
     *
     * @throws IOException
     */
    private void createLocalWarehouseDataFiles() throws IOException {
        LOCAL_WAREHOUSE_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(LOCAL_WAREHOUSE_DROPBOX,
                                                        FILE_1_DATA));
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(LOCAL_WAREHOUSE_DROPBOX,
                                                        FILE_2_DATA));
    }

    /**
     * Creates the standard pair of semaphore files in a dropbox.
     *
     * @throws IOException
     */
    private void createLocalWarehouseDataSemaphores() throws IOException {
        LOCAL_WAREHOUSE_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(LOCAL_WAREHOUSE_DROPBOX,
                                                        FILE_1_SEMA));
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(LOCAL_WAREHOUSE_DROPBOX,
                                                        FILE_2_SEMA));
    }

    /**
     * Creates the standard sets of data files to test outbound transfers.
     *
     * @throws IOException
     */
    private void createOffloadedTransferDataFiles() throws IOException {
        OUTBOUND_TRANSFER_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(OUTBOUND_TRANSFER_DROPBOX,
                                                        FILE_13_DATA));
    }

    /**
     * Creates the standard pair of semaphore files to test outbound transfers.
     *
     * @throws IOException
     */
    private void createOffloadedTransferDataSemaphores() throws IOException {
        OUTBOUND_TRANSFER_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(OUTBOUND_TRANSFER_DROPBOX,
                                                        FILE_13_SEMA));
    }

    /**
     * Creates the standard sets of data files to test outbound transfers.
     *
     * @throws IOException
     */
    private void createOutboundTransferCompressFiles() throws IOException {
        OUTBOUND_TRANSFER_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(OUTBOUND_TRANSFER_DROPBOX,
                                                        FILE_10_DATA));
    }

    /**
     * Creates the standard pair of semaphore files to test outbound transfers.
     *
     * @throws IOException
     */
    private void createOutboundTransferCompressSemaphores() throws IOException {
        OUTBOUND_TRANSFER_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(OUTBOUND_TRANSFER_DROPBOX,
                                                        FILE_10_SEMA));
    }

    /**
     * Creates the standard sets of data files to test outbound transfers.
     *
     * @throws IOException
     */
    private void createOutboundTransferDataFiles() throws IOException {
        OUTBOUND_TRANSFER_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(OUTBOUND_TRANSFER_DROPBOX,
                                                        FILE_5_DATA));
    }

    /**
     * Creates the standard pair of semaphore files to test outbound transfers.
     *
     * @throws IOException
     */
    private void createOutboundTransferDataSemaphores() throws IOException {
        OUTBOUND_TRANSFER_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(OUTBOUND_TRANSFER_DROPBOX,
                                                        FILE_5_SEMA));
    }

    /**
     * Creates the standard sets of data files to test outbound transfers.
     *
     * @throws IOException
     */
    private void createOutboundTransferPackFiles() throws IOException {
        OUTBOUND_TRANSFER_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(OUTBOUND_TRANSFER_DROPBOX,
                                                        FILE_8_DATA));
    }

    /**
     * Creates the standard pair of semaphore files to test outbound transfers.
     *
     * @throws IOException
     */
    private void createOutboundTransferPackSemaphores() throws IOException {
        OUTBOUND_TRANSFER_DROPBOX.mkdirs();
        UnitTestUtility.createFileFromResource(this.getClass(),
                                               new File(OUTBOUND_TRANSFER_DROPBOX,
                                                        FILE_8_SEMA));
    }

    private ExecutionEnvironment getMockExecutionEnvironment() {
        final ExecutionEnvironment executionEnvironment = new ExecutionEnvironment();
        executionEnvironment.bookkeeping = testObject.bookkeeping;
        executionEnvironment.cacheManager = testObject.cacheManager;
        executionEnvironment.configuration = testObject.configuration;
        executionEnvironment.metadataManager = testObject.metadataManager;
        executionEnvironment.neighborhoodManager = testObject.neighborhoodManager;
        executionEnvironment.registrationManager = testObject.registrationManager;
        executionEnvironment.retryManager = testObject.retryManager;
        executionEnvironment.verificationManager = new VerificationManagerImpl(testObject.cacheManager,
                                                                               testObject.loadedRegistrations,
                                                                               testObject.metadataManager,
                                                                               testObject.neighborhoodManager,
                                                                               testObject.retryManager,
                                                                               testObject.entityManager);
        executionEnvironment.warehouseManager = new WarehouseManagerImpl(testObject.configuration,
                                                                         testObject.entityManager);
        return executionEnvironment;
    }

    /**
     * Returns true if the specified directory does not contain and files.
     *
     * @return true if the specified directory does not contain and files.
     */
    private boolean isTreeEmpty(final File directory) {
        if (LAZARUS_ROOT.equals(directory.getName())) {
            return true;
        }
        final File[] contents = directory.listFiles();
        if (null != contents) {
            for (File content : contents) {
                if (!content.isDirectory() || !isTreeEmpty(content)) {
                    return false;
                }
            }
        }
        return true;
    }

    private void runInboundTransfer(final String inputBundles,
                                    final String expectedBundles,
                                    final LoadedLazarusWorkflows loadedWorkflows) throws IOException,
                                                                                  InitializingException,
                                                                                  JAXBException,
                                                                                  FileNotFoundException,
                                                                                  InterruptedException,
                                                                                  SuspendedException,
                                                                                  TooManyTicketsException,
                                                                                  InProgressException {
        JavaInstanceFactoryImpl.setManualExecutionEnvironment(getMockExecutionEnvironment());
        final WorkflowsManagerImpl workflowManager = new WorkflowsManagerImpl(loadedWorkflows);
        createSpade(testObject,
                    workflowManager);
        final Bundles bundles = readBundles(new FileInputStream(inputBundles));
        final Bundles results = (testObject.spade).ingest(bundles);
        AtomicInteger ingestCount = (testObject.spade).getCount(Workflow.INGEST);
        synchronized (ingestCount) {
            while (0 != ingestCount.get()) {
                ingestCount.wait();
            }
        }
        final Bundles actual = new Bundles(null,
                                           results);
        final Bundles expected = readBundles(new FileInputStream(expectedBundles));
        assertEquals(expected.toString(),
                     actual.toString(),
                     "Incorrect result returned from \"ingest\" invocation");
        assertEquals(0,
                     (testObject.terminationListener).getFailedCount(),
                     "At least one workflow failed");
    }

    private void runLocalWarehouse(String inputBundles,
                                   String expectedBundles,
                                   final LoadedLazarusWorkflows loadedWorkflows) throws IOException,
                                                                                 InitializingException,
                                                                                 JAXBException,
                                                                                 FileNotFoundException,
                                                                                 InterruptedException,
                                                                                 SuspendedException,
                                                                                 TooManyTicketsException,
                                                                                 InProgressException {
        JavaInstanceFactoryImpl.setManualExecutionEnvironment(getMockExecutionEnvironment());
        final WorkflowsManagerImpl workflowManager = new WorkflowsManagerImpl(loadedWorkflows);
        createSpade(testObject,
                    workflowManager);
        final Bundles bundles = readBundles(new FileInputStream(inputBundles));
        final Bundles results = (testObject.spade).ingest(bundles);
        AtomicInteger ingestCount = (testObject.spade).getCount(Workflow.INGEST);
        synchronized (ingestCount) {
            while (0 != ingestCount.get()) {
                ingestCount.wait();
            }
        }
        final Bundles actual = new Bundles(null,
                                           results);
        final Bundles expected = readBundles(new FileInputStream(expectedBundles));
        assertEquals(expected.toString(),
                     actual.toString(),
                     "Incorrect result returned from \"ingest\" invocation");
        assertEquals(0,
                     (testObject.terminationListener).getFailedCount(),
                     "At least one workflow failed");
    }

    private void runOutboundTransfer(final String inputBundles,
                                     final String expectedBundles,
                                     final LoadedLazarusWorkflows loadedWorkflows) throws IOException,
                                                                                   InitializingException,
                                                                                   JAXBException,
                                                                                   FileNotFoundException,
                                                                                   InterruptedException,
                                                                                   SuspendedException,
                                                                                   TooManyTicketsException,
                                                                                   InProgressException {
        JavaInstanceFactoryImpl.setManualExecutionEnvironment(getMockExecutionEnvironment());
        final WorkflowsManagerImpl workflowManager = new WorkflowsManagerImpl(loadedWorkflows);
        createSpade(testObject,
                    workflowManager);
        final Bundles bundles = readBundles(new FileInputStream(inputBundles));
        final Bundles results = (testObject.spade).ingest(bundles);
        final AtomicInteger ingestCount = (testObject.spade).getCount(Workflow.INGEST);
        synchronized (ingestCount) {
            while (0 != ingestCount.get()) {
                ingestCount.wait();
            }
        }
        final Bundles actual = new Bundles(null,
                                           results);
        final Bundles expected = readBundles(new FileInputStream(expectedBundles));
        assertEquals(expected.toString(),
                     actual.toString(),
                     "Incorrect result returned from \"ingest\" invocation");
        assertEquals(0,
                     (testObject.terminationListener).getFailedCount(),
                     "At least one workflow failed");
    }

    @BeforeEach
    protected void setUp() throws Exception {
        cleanUpTrees();

        SPADE_DIRECTORY.mkdirs();
        testObject = new MockDeployment("Test",
                                        DEFINITION);
    }

    @AfterEach
    protected void tearDown() throws Exception {
        cleanUpTrees();
    }

    /**
     * Tests that SPADE can ingest a Bundle.
     *
     * @throws Exception
     *             when a problem has occured.
     */
    @Test
    @DisplayName("Bundle Ingest")
    public void testBundleIngest() throws Exception {
        createSpade(testObject,
                    new MockNullWorkflow(true));
        final Bundles bundles = readBundles(new FileInputStream("src/test/files/bundles/local_warehouse.xml"));
        final Bundles results = (testObject.spade).ingest(bundles);
        final Bundles actual = new Bundles(null,
                                           results);
        final Bundles expected = readBundles(new FileInputStream("src/test/files/bundles/local_warehouse_result.xml"));
        assertEquals(expected.toString(),
                     actual.toString(),
                     "Incorrect result returned from \"ingest\" invocation");
        assertEquals(0,
                     (testObject.terminationListener).getFailedCount(),
                     "At least one workflow failed");
    }

    /**
     * Tests that SPADE can receive a Bundle to a remote instance.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
    @Test
    @DisplayName("Inbound Transfer")
    public void testInboundTransfer() throws IOException,
                                      InitializingException,
                                      JAXBException,
                                      InterruptedException,
                                      SuspendedException,
                                      TooManyTicketsException,
                                      InProgressException {
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              INBOUND_TRANSFER_BPMN);
        createInboundTransferDataSemaphores();
        createInboundTransferDataFiles();
        runInboundTransfer("src/test/files/bundles/inbound_transfer.xml",
                           "src/test/files/bundles/inbound_transfer_result.xml",
                           loadedWorkflows);
    }

    /**
     * Tests that SPADE can receive a Bundle to a remote instance.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
    @Test
    @DisplayName("Inbonud Transfer Expand")
    public void testInboundTransferExpand() throws IOException,
                                            InitializingException,
                                            JAXBException,
                                            InterruptedException,
                                            SuspendedException,
                                            TooManyTicketsException,
                                            InProgressException {
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              INBOUND_TRANSFER_EXPAND_BPMN);
        createInboundTransferExpandSemaphores();
        createInboundTransferExpandFiles();
        runInboundTransfer("src/test/files/bundles/inbound_transfer_expand.xml",
                           "src/test/files/bundles/inbound_transfer_expand_result.xml",
                           loadedWorkflows);
        final File dataFile1 = new File(new File(FILE_11_WAREHOUSE,
                                                 FILE_11_BUNDLE),
                                        FILE_11_1_DATA);
        assertTrue(dataFile1.exists(),
                   "First data file missing from warehouse");
        final File dataFile2 = new File(new File(FILE_11_WAREHOUSE,
                                                 FILE_11_BUNDLE),
                                        FILE_11_2_DATA);
        assertTrue(dataFile2.exists(),
                   "Second data file missing from warehouse");
        final File metaFile1 = new File(FILE_11_WAREHOUSE,
                                        FILE_11_META);
        assertTrue(metaFile1.exists(),
                   "Metadata file missing from warehouse");
    }

    /**
     * Tests that SPADE can receive a Bundle to a remote instance.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
    @Test
    @DisplayName("Inbound Transfer Unpack")
    public void testInboundTransferUnpack() throws IOException,
                                            InitializingException,
                                            JAXBException,
                                            InterruptedException,
                                            SuspendedException,
                                            TooManyTicketsException,
                                            InProgressException {
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              INBOUND_TRANSFER_UNPACK_BPMN);
        createInboundTransferUnpackSemaphores();
        createInboundTransferUnpackFiles();
        runInboundTransfer("src/test/files/bundles/inbound_transfer_unpack.xml",
                           "src/test/files/bundles/inbound_transfer_unpack_result.xml",
                           loadedWorkflows);
        final File dataFile1 = new File(new File(FILE_9_WAREHOUSE,
                                                 FILE_9_BUNDLE),
                                        FILE_9_1_DATA);
        assertTrue(dataFile1.exists(),
                   "First data file missing from warehouse");
        final File dataFile2 = new File(new File(FILE_9_WAREHOUSE,
                                                 FILE_9_BUNDLE),
                                        FILE_9_2_DATA);
        assertTrue(dataFile2.exists(),
                   "Second data file missing from warehouse");
        final File metaFile1 = new File(FILE_9_WAREHOUSE,
                                        FILE_9_META);
        assertTrue(metaFile1.exists(),
                   "Metadata file missing from warehouse");
    }

    /**
     * Tests that SPADE can place a Bundle in its warehouse.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
    @Test
    @DisplayName("Local Warehouse")
    public void testLocalWarehouse() throws JAXBException,
                                     InitializingException,
                                     IOException,
                                     InterruptedException,
                                     SuspendedException,
                                     TooManyTicketsException,
                                     InProgressException {
        createLocalWarehouseDataSemaphores();
        createLocalWarehouseDataFiles();
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              LOCAL_WAREHOUSE_BPMN);
        runLocalWarehouse("src/test/files/bundles/local_warehouse.xml",
                          "src/test/files/bundles/local_warehouse_result.xml",
                          loadedWorkflows);
        final File externData1 = new File(LOCAL_WAREHOUSE_DROPBOX,
                                          FILE_1_DATA);
        assertFalse(externData1.exists(),
                    "External data file still exists");
        final File semaData1 = new File(LOCAL_WAREHOUSE_DROPBOX,
                                        FILE_1_SEMA);
        assertFalse(semaData1.exists(),
                    "Semaphore file still exists");
        final File externData2 = new File(LOCAL_WAREHOUSE_DROPBOX,
                                          FILE_2_DATA);
        assertTrue(externData2.exists(),
                   "External data file does not exists");
        final File semaData2 = new File(LOCAL_WAREHOUSE_DROPBOX,
                                        FILE_2_SEMA);
        assertFalse(semaData2.exists(),
                    "Semaphore file still exists");
        assertTrue(isTreeEmpty(CACHE_DIRECTORY),
                   "The Cache is not empty");
        final File dataFile1 = new File(FILE_1_WAREHOUSE,
                                        FILE_1_DATA);
        assertTrue(dataFile1.exists(),
                   "Data file missing from warehouse");
        final File metaFile1 = new File(FILE_1_WAREHOUSE,
                                        FILE_1_META);
        assertTrue(metaFile1.exists(),
                   "Metadata file missing from warehouse");
        final File dataFile2 = new File(FILE_2_WAREHOUSE,
                                        FILE_2_DATA);
        assertTrue(dataFile2.exists(),
                   "Data file missing from warehouse");
        final File metaFile2 = new File(FILE_2_WAREHOUSE,
                                        FILE_2_META);
        assertTrue(metaFile2.exists(),
                   "Metadata file missing from warehouse");
    }

    /**
     * Tests that SPADE can place a Bundle in its warehouse.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
    @Test
    @DisplayName("Local Warehouse Analyze")
    public void testLocalWarehouseAnalyze() throws JAXBException,
                                            InitializingException,
                                            IOException,
                                            InterruptedException,
                                            SuspendedException,
                                            TooManyTicketsException,
                                            InProgressException {
        createLocalWarehouseAnalyzeSemaphores();
        createLocalWarehouseAnalyzeFiles();
        ANALYSIS_OUTPUT_DIR.mkdirs();
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              LOCAL_WAREHOUSE_ANALYZE_BPMN);
        runLocalWarehouse("src/test/files/bundles/local_warehouse_analyze.xml",
                          "src/test/files/bundles/local_warehouse_analyze_result.xml",
                          loadedWorkflows);
        final File[] year = ANALYSIS_OUTPUT_DIR.listFiles();
        assertEquals(1,
                     year.length,
                     "Incorrect output directory for year");
        final File[] month = year[0].listFiles();
        assertEquals(1,
                     month.length,
                     "Incorrect output directory for month");
        final File[] day = month[0].listFiles();
        assertEquals(1,
                     day.length,
                     "Incorrect output directory for day");
        final File[] results = day[0].listFiles();
        assertEquals(2,
                     results.length,
                     "Incorrect output directory with day");
        final File logDir;
        final File errDir;
        if (Analyzer.LOG_DIR.equals(results[0].getName())) {
            logDir = results[0];
            errDir = results[1];
            assertEquals(Analyzer.ERR_DIR,
                         errDir.getName(),
                         "Incorrect output directory for errors");
        } else {
            logDir = results[1];
            assertEquals(Analyzer.LOG_DIR,
                         logDir.getName(),
                         "Incorrect log directory for errors");
            errDir = results[0];
            assertEquals(Analyzer.ERR_DIR,
                         errDir.getName(),
                         "Incorrect output directory for errors");
        }
        final File logFile = new File(logDir,
                                      FILE_12_BUNDLE + Analyzer.LOG_SUFFIX);
        assertTrue(logFile.exists(),
                   "Log file does not exist");
        final File errFile = new File(errDir,
                                      FILE_12_BUNDLE + Analyzer.ERR_SUFFIX);
        final Properties contents = new Properties();
        contents.load(new FileInputStream(logFile));
        assertEquals(FILE_12_BUNDLE,
                     contents.getProperty("bundle"),
                     "Not the correct bundle");
        assertEquals(new File(FILE_12_WAREHOUSE,
                              FILE_12_META).getCanonicalPath(),
                     new File((contents.getProperty("metaPath"))).getCanonicalPath(),
                     "Not the correct metadata file");
        assertEquals(new File(FILE_12_WAREHOUSE,
                              FILE_12_DATA).getCanonicalPath(),
                     new File((contents.getProperty("dataPath"))).getCanonicalPath(),
                     "Not the correct data file");
        assertTrue(errFile.exists(),
                   "Error file does not exist");
        assertEquals(0,
                     errFile.length(),
                     "Error file is not empty");
    }

    /**
     * Tests that local warehouse workflow passes the {@link IngestTicket} instance
     * correctly.
     *
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws InterruptedException
     *             when the test is interrupted.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
    @Test
    @DisplayName("NOP Local Warehouse")
    public void testNopLocalWarehouse() throws JAXBException,
                                        InitializingException,
                                        SuspendedException,
                                        TooManyTicketsException,
                                        IOException,
                                        InProgressException,
                                        InterruptedException {
        createLocalWarehouseDataSemaphores();
        createLocalWarehouseDataFiles();
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              LOCAL_WAREHOUSE_BPMN);
        final WorkflowsManagerImpl workflowManager = new WorkflowsManagerImpl(loadedWorkflows);
        createSpade(testObject,
                    workflowManager);
        final Bundles bundles = readBundles(new FileInputStream("src/test/files/bundles/local_warehouse.xml"));
        final Bundles results = (testObject.spade).ingest(bundles);
        final Bundles actual = new Bundles(null,
                                           results);
        final Bundles expected = readBundles(new FileInputStream("src/test/files/bundles/local_warehouse_result.xml"));
        assertEquals(expected.toString(),
                     actual.toString(),
                     "Incorrect result returned from \"ingest\" invocation");
        assertEquals(0,
                     (testObject.terminationListener).getFailedCount(),
                     "At least one workflow failed");
    }

    /**
     * Tests that SPADE can place a Bundle in its warehouse.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
//    @Test
//    @DisplayName("Optional Local Warehouse")
    public void testOptionalLocalWarehouse() throws JAXBException,
                                             InitializingException,
                                             IOException,
                                             InterruptedException,
                                             SuspendedException,
                                             TooManyTicketsException,
                                             InProgressException {
        createLocalWarehouseDataSemaphores();
        createLocalWarehouseDataFiles();
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              LOCAL_WAREHOUSE_OPTIONAL_BPMN);
        runLocalWarehouse("src/test/files/bundles/local_warehouse.xml",
                          "src/test/files/bundles/local_warehouse_result.xml",
                          loadedWorkflows);
        final File dataFile1 = new File(FILE_1_WAREHOUSE,
                                        FILE_1_DATA);
        assertTrue(dataFile1.exists(),
                   "Data file missing from warehouse");
        final File metaFile1 = new File(FILE_1_WAREHOUSE,
                                        FILE_1_META);
        assertTrue(metaFile1.exists(),
                   "Metadata file missing from warehouse");
        final File dataFile2 = new File(FILE_2_WAREHOUSE,
                                        FILE_2_DATA);
        assertFalse(dataFile2.exists(),
                    "Data file exists in warehouse");
        final File metaFile2 = new File(FILE_2_WAREHOUSE,
                                        FILE_2_META);
        assertFalse(metaFile2.exists(),
                    "Data file exists in warehouse");
    }

    // static member methods (alphabetic)

    /**
     * Tests that SPADE can deliver a Bundle to a remote instance.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
    @Test
    @DisplayName("Outbound Transfer")
    public void testOutboundTransfer() throws IOException,
                                       InitializingException,
                                       JAXBException,
                                       InterruptedException,
                                       SuspendedException,
                                       TooManyTicketsException,
                                       InProgressException {
        RECEIVING_ONE.mkdirs();
        RECEIVING_TWO.mkdirs();

        createOutboundTransferDataSemaphores();
        createOutboundTransferDataFiles();
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              OUTBOUND_TRANSFER_BPMN);
        runOutboundTransfer("src/test/files/bundles/outbound_transfer.xml",
                            "src/test/files/bundles/outbound_transfer_result.xml",
                            loadedWorkflows);
        final File deliveredOneMeta5 = new File(RECEIVING_ONE,
                                                DELIVERED_FILE_5_META);
        assertTrue(deliveredOneMeta5.exists(),
                   "Delivered metadata file does not exist in \"one\"");
        final File deliveredOneLoad5 = new File(RECEIVING_ONE,
                                                DELIVERED_FILE_5_PAYLOAD);
        assertTrue(deliveredOneLoad5.exists(),
                   "Delivered payload file does not exist in \"one\"");
        final File deliveredOneSema5 = new File(RECEIVING_ONE,
                                                DELIVERED_FILE_5_DONE);
        assertTrue(deliveredOneSema5.exists(),
                   "Delivered done file does not exist in \"one\"");
        final File deliveredTwoMeta5 = new File(RECEIVING_TWO,
                                                DELIVERED_FILE_5_META);
        assertTrue(deliveredTwoMeta5.exists(),
                   "Delivered metadata file does not exist in \"two\"");
        final File deliveredTwoLoad5 = new File(RECEIVING_TWO,
                                                DELIVERED_FILE_5_PAYLOAD);
        assertTrue(deliveredTwoLoad5.exists(),
                   "Delivered payload file does not exist in \"two\"");
        final File deliveredTwoSema5 = new File(RECEIVING_TWO,
                                                DELIVERED_FILE_5_DONE);
        assertTrue(deliveredTwoSema5.exists(),
                   "Delivered done file does not exist in \"two\"");
        final File directory = new File(RESHIP,
                                        "0");
        final List<String> retries = Arrays.asList(directory.list());
        assertEquals(3,
                     retries.size(),
                     "Incorrect number of file in the retry directory");
        assertTrue(retries.contains(FILE_5_META),
                   "Metadata not stored for retries");
        assertTrue(retries.contains(TICKET_PAYLOAD_DIRECTORY_SUFFIX),
                   "Transfer retry payload directory does not exist");
        assertTrue(new File(new File(directory,
                                     TICKET_PAYLOAD_DIRECTORY_SUFFIX),
                            FILE_5_DATA).exists(),
                   "Transfer file not stored for retries");
    }

    /**
     * Tests that SPADE can deliver a Bundle to a remote instance.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
//    @Test
//    @DisplayName("Outbouhnd Transfer Compress")
    public void testOutboundTransferCompress() throws IOException,
                                               InitializingException,
                                               JAXBException,
                                               InterruptedException,
                                               SuspendedException,
                                               TooManyTicketsException,
                                               InProgressException {
        RECEIVING_SIX.mkdirs();

        createOutboundTransferCompressSemaphores();
        createOutboundTransferCompressFiles();
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              OUTBOUND_TRANSFER_COMPRESS_BPMN);
        runOutboundTransfer("src/test/files/bundles/outbound_transfer_compress.xml",
                            "src/test/files/bundles/outbound_transfer_compress_result.xml",
                            loadedWorkflows);
        final File deliveredOneMeta8 = new File(RECEIVING_SIX,
                                                DELIVERED_FILE_10_META);
        assertTrue(deliveredOneMeta8.exists(),
                   "Delivered metadata file does not exist in \"six\"");
        final File deliveredOneLoad8 = new File(RECEIVING_SIX,
                                                DELIVERED_FILE_10_PAYLOAD);
        assertTrue(deliveredOneLoad8.exists(),
                   "Delivered payload file does not exist in \"six\"");
        final File deliveredOneSema8 = new File(RECEIVING_SIX,
                                                DELIVERED_FILE_10_DONE);
        assertTrue(deliveredOneSema8.exists(),
                   "Delivered done file does not exist in \"six\"");
        final List<String> retries = Arrays.asList(new File(RESHIP,
                                                            "0").list());
        assertEquals(3,
                     retries.size(),
                     "Incorrect number of file in the retry directory");
        assertTrue(retries.contains(FILE_10_META),
                   "Metadata not stored for retries");
        // assertTrue("Transfer retry payload directory does not exist",
        // retries.contains(prefix + TICKET_PAYLOAD_DIRECTORY_SUFFIX));
        // assertTrue("Transfer file not stored for retries",
        // new File(new File(RESHIP,
        // prefix + TICKET_PAYLOAD_DIRECTORY_SUFFIX),
        // FILE_8_DATA).exists());
    }

    /**
     * Tests that SPADE can deliver a Bundle to a remote instance.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
    @Test
    @DisplayName("Outbound Transfer Pack")
    public void testOutboundTransferPack() throws IOException,
                                           InitializingException,
                                           JAXBException,
                                           InterruptedException,
                                           SuspendedException,
                                           TooManyTicketsException,
                                           InProgressException {
        RECEIVING_FOUR.mkdirs();

        createOutboundTransferPackSemaphores();
        createOutboundTransferPackFiles();
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              OUTBOUND_TRANSFER_PACK_BPMN);
        runOutboundTransfer("src/test/files/bundles/outbound_transfer_pack.xml",
                            "src/test/files/bundles/outbound_transfer_pack_result.xml",
                            loadedWorkflows);
        final File deliveredOneMeta8 = new File(RECEIVING_FOUR,
                                                DELIVERED_FILE_8_META);
        assertTrue(deliveredOneMeta8.exists(),
                   "Delivered metadata file does not exist in \"four\"");
        final File deliveredOneLoad8 = new File(RECEIVING_FOUR,
                                                DELIVERED_FILE_8_PAYLOAD);
        assertTrue(deliveredOneLoad8.exists(),
                   "Delivered payload file does not exist in \"four\"");
        final File deliveredOneSema8 = new File(RECEIVING_FOUR,
                                                DELIVERED_FILE_8_DONE);
        assertTrue(deliveredOneSema8.exists(),
                   "Delivered done file does not exist in \"four\"");
        final List<String> retries = Arrays.asList(new File(RESHIP,
                                                            "0").list());
        assertEquals(3,
                     retries.size(),
                     "Incorrect number of file in the retry directory");
        assertTrue(retries.contains(FILE_8_META),
                   "Metadata not stored for retries");
        // assertTrue("Transfer retry payload directory does not exist",
        // retries.contains(prefix + TICKET_PAYLOAD_DIRECTORY_SUFFIX));
        // assertTrue("Transfer file not stored for retries",
        // new File(new File(RESHIP,
        // prefix + TICKET_PAYLOAD_DIRECTORY_SUFFIX),
        // FILE_8_DATA).exists());
    }

    /**
     * Tests that SPADE can verify a Bundles to a remote instance.
     *
     * @throws IOException
     *             when a file can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     */
    @Test
    @DisplayName("Verification")
    public void testVerification() throws IOException,
                                   InitializingException {
        final LocalRegistration registration = new LocalRegistration(FILE_7_OUTBOUND_REGISTRATION);
        final KnownLocalRegistration knownRegistration = new KnownLocalRegistration(registration);
        (testObject.entityManager).persist(knownRegistration);
        final TicketedFile ticketedFile = new TicketedFile(FILE_7_BUNDLE,
                                                           null,
                                                           knownRegistration);
        (testObject.entityManager).persist(ticketedFile);
        final ShippedFile shippedFile = new ShippedFile(ticketedFile);
        shippedFile.setAlgorithm(FILE_7_ALGORITHM);
        shippedFile.setChecksum(FILE_7_CHECKSUM);
        shippedFile.setWhenVerificationStarted(new Date());
        (testObject.entityManager).persist(shippedFile);

        for (OutboundTransfer outboundTransfer : OUTBOUND_TRANSFERS.subList(0,
                                                                            2)) {
            final KnownNeighbor knownNeighbor = new KnownNeighbor(outboundTransfer.getNeighbor());
            (testObject.entityManager).persist(knownNeighbor);
            final Confirmation confirmation = new Confirmation(shippedFile,
                                                               knownNeighbor);
            (testObject.entityManager).persist(confirmation);
        }

        final File directory = new File(RESHIP,
                                        shippedFile.getTicketIdentity());
        directory.mkdirs();
        final File retryMetadata = new File(directory,
                                            FILE_7_META);
        retryMetadata.createNewFile();
        final File retryTransfer = new File(directory,
                                            FILE_7_DATA);
        retryTransfer.createNewFile();
        final File retryTicket = new File(directory,
                                          "ticket.xml");
        retryTicket.createNewFile();

        createSpade(testObject,
                    null);
        // Make sure files exist so we can confirm they are removed.
        final String[] retries = new File(RESHIP,
                                          "0").list();
        assertEquals(3,
                     retries.length,
                     "Incorrect number of file in the retry directory");

        final Definition definition_2 = DEFINITION_2;
        final String neighbor_2 = (OUTBOUND_TRANSFERS.get(0)).getNeighbor();
        confirmFromDestination(definition_2,
                               neighbor_2);

        final List<DigestChange> verified_2 = (testObject.spade).getVerifiedByTime(null,
                                                                                   null,
                                                                                   false,
                                                                                   -1,
                                                                                   null);

        assertNotNull(verified_2,
                      "No verified object was returned");
        assertEquals(0,
                     verified_2.size(),
                     "Wrong number of confirmed bundles was returned");

        final Definition definition_3 = DEFINITION_3;
        final String neighbor_3 = (OUTBOUND_TRANSFERS.get(1)).getNeighbor();
        confirmFromDestination(definition_3,
                               neighbor_3);

        final List<DigestChange> verified_3 = (testObject.spade).getVerifiedByTime(null,
                                                                                   null,
                                                                                   false,
                                                                                   -1,
                                                                                   null);

        assertNotNull(verified_3,
                      "No verified object was returned");
        assertEquals(1,
                     verified_3.size(),
                     "Wrong number of confirmed bundles was returned");
        assertFalse(new File(RESHIP,
                             "0").exists(),
                    "Retry directory for this transfer still exists");
    }

    /**
     * Tests that SPADE can deliver a Bundle to a remote instance.
     *
     * @throws JAXBException
     *             when the {@link Bundles} instance can not be read.
     * @throws IOException
     *             when semaphores can not be created.
     * @throws InitializingException
     *             when the application is still initializing.
     * @throws InterruptedException
     *             when interrupted while wait for workflow to complete.
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws URISyntaxException
     *             when the URI is not correct.
     * @throws InProgressException
     *             when a scan it already in progress.
     */
    public void xtestOffloadedTransfer() throws IOException,
                                         InitializingException,
                                         JAXBException,
                                         InterruptedException,
                                         SuspendedException,
                                         TooManyTicketsException,
                                         URISyntaxException,
                                         InProgressException {
        RECEIVING_EIGHT.mkdirs();

        createOffloadedTransferDataSemaphores();
        createOffloadedTransferDataFiles();
        final LoadedLazarusWorkflows loadedWorkflows = new MockIngestWorkflow(testObject.cacheManager,
                                                                              testObject.configuration,
                                                                              testObject.ticketManager,
                                                                              OUTBOUND_TRANSFER_BPMN);
        final String uriFile = beginOffloadedTransfer("src/test/files/bundles/offloaded_transfer.xml",
                                                      "src/test/files/bundles/offloaded_transfer_result.xml",
                                                      loadedWorkflows);
        completeOffloadedTransfer(uriFile);
        final File deliveredEightMeta13 = new File(RECEIVING_EIGHT,
                                                   DELIVERED_FILE_13_META);
        assertTrue(deliveredEightMeta13.exists(),
                   "Delivered metadata file does not exist in \"eight\"");
        final File deliveredEightLoad13 = new File(RECEIVING_EIGHT,
                                                   DELIVERED_FILE_13_PAYLOAD);
        assertTrue(deliveredEightLoad13.exists(),
                   "Delivered payload file does not exist in \"eight\"");
        final File deliveredEightSema13 = new File(RECEIVING_EIGHT,
                                                   DELIVERED_FILE_13_DONE);
        assertTrue(deliveredEightSema13.exists(),
                   "Delivered data file does not exist in \"eight\"");
        final File directory = new File(RESHIP,
                                        "0");
        final List<String> retries = Arrays.asList(directory.list());
        assertEquals(3,
                     retries.size(),
                     "Incorrect number of file in the retry directory");
        assertTrue(retries.contains(FILE_13_META),
                   "Metadata not stored for retries");
        assertTrue(retries.contains(TICKET_PAYLOAD_DIRECTORY_SUFFIX),
                   "Transfer retry payload directory does not exist");
        assertTrue(new File(new File(directory,
                                     TICKET_PAYLOAD_DIRECTORY_SUFFIX),
                            FILE_13_DATA).exists(),
                   "Transfer file not stored for retries");
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
