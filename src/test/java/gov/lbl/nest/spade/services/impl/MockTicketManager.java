package gov.lbl.nest.spade.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.rs.Bundles;
import gov.lbl.nest.spade.services.TicketManager;
import gov.lbl.nest.spade.services.TooManyTicketsException;

/**
 * This class implements the {@link TicketManager} interface for use in testing.
 * 
 * @author patton
 */
public class MockTicketManager implements
                               TicketManager {

    private final List<IngestTicket> issued = new ArrayList<>();

    private final AtomicInteger next = new AtomicInteger();

    @Override
    public <T> boolean block(T ticket,
                             String cause) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public <T> boolean dispose(T ticket) {
        synchronized (issued) {
            return issued.remove(ticket);
        }
    }

    @Override
    public Bundles getIssuedBundles() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getIssuedCount() {
        return issued.size();
    }

    @Override
    public String getPlacementId(String bundle) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getPlacementIds(List<String> bundle) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Bundles getRetainedBundles() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getTickets(String bundle) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IngestTicket issue(Bundle bundle) throws TooManyTicketsException {
        final IngestTicket result = new IngestTicket(Integer.toString(next.getAndIncrement()),
                                                     bundle,
                                                     null);
        synchronized (issued) {
            issued.add(result);
        }
        return result;
    }

    @Override
    public <T> boolean reissue(T ticket) {
        if (ticket instanceof IngestTicket) {
            final IngestTicket ingestTicket = (IngestTicket) ticket;
            synchronized (issued) {
                return issued.add(ingestTicket);
            }
        }
        return false;
    }

    @Override
    public void setLatestScan(Date dateTime) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean unblock(Bundle bundle) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public <T> boolean unblock(T ticket) {
        // TODO Auto-generated method stub
        return false;
    }

}
