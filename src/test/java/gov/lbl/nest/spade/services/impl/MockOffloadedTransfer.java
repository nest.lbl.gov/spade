package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.util.concurrent.atomic.AtomicReference;

import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.services.FileTransfer;

/**
 * This class implements the {@link FileTransfer} for files are directly
 * accessible on the local file system.
 *
 * @author patton
 */
public class MockOffloadedTransfer implements
                                   FileTransfer {

    // public static final member data

    /**
     * The callabck URI set when this class first executed.
     */
    public static AtomicReference<String> filePath = new AtomicReference<>();

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The name of the file in which the {@link URI} is stored to mock up a
     * callback.
     */
    private final static String URI_FILENAME = "transfer_uri.txt";

    // private static member data

    // private instance member data

    private final LocalhostTransfer delegate = new LocalhostTransfer();

    // constructors

    // instance member method (alphabetic)

    @Override
    public boolean send(final ExternalLocation targetLocation,
                        final File metadataFile,
                        final String targetMetadataName,
                        final File transferFile,
                        final String targetTransferName,
                        final File semaphoreFile,
                        final String targetSemaphoreName,
                        final URI callback) throws IOException,
                                            InterruptedException {
        delegate.send(targetLocation,
                      metadataFile,
                      targetMetadataName,
                      transferFile,
                      targetTransferName,
                      semaphoreFile,
                      targetSemaphoreName,
                      callback);
        final File uriFile = new File(URI_FILENAME);
        try (PrintWriter out = new PrintWriter(uriFile)) {
            out.println(callback.toString());
        }
        synchronized (filePath) {
            filePath.set(uriFile.getAbsolutePath());
            filePath.notifyAll();
        }
        return false;
    }

    @Override
    public void setEnvironment(File configurationDir) {
        // Do nothing
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
