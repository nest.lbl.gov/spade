package gov.lbl.nest.spade.services.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.services.RegistrationManager;

/**
 * This class implements the {@link RegistrationManager} interface for use in
 * testing.
 * 
 * @author patton
 */
public class MockRegistrationManager implements
                                     RegistrationManager {

    /**
     * The {@link Map} of {@link Registration} instances index by local identity
     */
    private Map<String, Registration> registrationsByLocalId;

    /**
     * The {@link Map} of {@link Registration} instances index by host and
     * directory.
     */
    private Map<String, Map<String, Collection<Registration>>> knownRegistrations;

    /**
     * Creates an instance of this class.
     * 
     * @param knownRegistrations
     *            the {@link Map} of {@link Registration} instances index by host
     *            and directory.
     */
    public MockRegistrationManager(Map<String, Map<String, Collection<Registration>>> knownRegistrations) {
        this.knownRegistrations = knownRegistrations;
    }

    @Override
    public Bundle assignRegistration(Bundle bundle) {
        final ExternalFile semaphore = bundle.getSemaphore();
        for (Registration registration : getRegistrationsByLocalId().values()) {
            final Predicate<String> predicate = registration.getPredicate();
            final String name = semaphore.getName();
            if (predicate.test(name)) {
                bundle.setLocalId(registration.getLocalId());
                if (null == bundle.getName()) {
                    bundle.setName((registration.getDataLocator()).getBundleName(name));
                }
                final String neighbor = registration.getNeighbor();
                bundle.setNeighbor(neighbor);
                return bundle;
            }
        }
        return null;
    }

    @Override
    public Registration getRegistration(String localId) {

        return registrationsByLocalId.get(localId);
    }

    private Map<String, Registration> getRegistrationsByLocalId() {
        if (null == registrationsByLocalId) {
            registrationsByLocalId = new HashMap<>();
            for (String host : knownRegistrations.keySet()) {
                final Map<String, Collection<Registration>> directories = knownRegistrations.get(host);
                for (String directory : directories.keySet()) {
                    for (Registration registration : directories.get(directory)) {
                        registrationsByLocalId.put(registration.getLocalId(),
                                                   registration);
                    }
                }
            }
        }
        return registrationsByLocalId;
    }

}
