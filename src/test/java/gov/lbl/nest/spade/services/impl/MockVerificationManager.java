package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import gov.lbl.nest.spade.services.VerificationManager;

/**
 * This class implements the {@link VerificationManager} interface for use in
 * testing.
 * 
 * @author patton
 */
public class MockVerificationManager implements
                                     VerificationManager {

    /**
     * List of tickets being delivered.
     */
    private final Map<String, Collection<String>> delivering = new HashMap<>();

    /**
     * The mapping of {@link OutboundTransfer} instances to their destinations.
     */
    private final Map<String, String> destinations = new HashMap<>();

    /**
     * Creates an instance of this class.
     * 
     * @param outboundTransfers
     *            the mapping of {@link OutboundTransfer} instances to their names.
     */
    public MockVerificationManager(Map<String, OutboundTransfer> outboundTransfers) {
        for (OutboundTransfer outboundTransfer : outboundTransfers.values()) {
            destinations.put(outboundTransfer.getName(),
                             outboundTransfer.getNeighbor());
        }
    }

    @Override
    public List<? extends DigestChange> confirm(String neighbor,
                                                List<? extends DigestChange> confirmables) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void confirmable(String ticket) {
        // TODO Auto-generated method stub

    }

    @Override
    public void confirmable(String ticket,
                            File metadataFile,
                            File transferFile) throws IOException,
                                               MetadataParseException {
        // TODO Auto-generated method stub

    }

    @Override
    public void delivered(String ticket,
                          String neighbor) {
        final Collection<String> transfers = delivering.get(ticket);
        if (null != transfers) {
            transfers.remove(neighbor);
        }
    }

    @Override
    public List<DigestChange> getByBundle(Stamped stamped,
                                          List<String> bundles) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DigestChange> getByTime(Stamped stamped,
                                        Date after,
                                        Date before,
                                        boolean reversed,
                                        int max) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DigestChange> getByTime(Stamped stamped,
                                        Date after,
                                        Date before,
                                        boolean reversed,
                                        int max,
                                        String neighbor,
                                        List<String> registrations) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<String> getUndeliveredTransfers(String ticket) {
        return delivering.get(ticket);
    }

    @Override
    public void prepare(String ticket,
                        String bundle,
                        File metadataFile,
                        File transferFile,
                        Collection<String> transfers) throws IOException,
                                                      MetadataParseException {
        final List<String> neighbors = new ArrayList<>();
        for (String transfer : transfers) {
            neighbors.add(destinations.get(transfer));
        }
        delivering.put(ticket,
                       neighbors);
    }

    @Override
    public void start(String ticket) {
        // TODO Auto-generated method stub

    }

}
