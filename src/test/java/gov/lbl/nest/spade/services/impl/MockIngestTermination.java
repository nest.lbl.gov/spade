package gov.lbl.nest.spade.services.impl;

import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.spade.activities.IngestTermination;
import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.services.TicketManager;

/**
 * This class is an implementation of the {@link IngestTermination} interface
 * use in tests.
 *
 * @author patton
 *
 */
public class MockIngestTermination implements
                                   IngestTermination {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The number of failed workflows.
     */
    private AtomicInteger failedCount = new AtomicInteger(0);

    /**
     * The {@link TicketManager} instance used by this object.
     */
    private TicketManager ticketManager;

    // constructors

    /**
     * Create an instance of this class.
     *
     * @param ticketManager
     *            the {@link TicketManager} instance used by this object.
     */
    public MockIngestTermination(final TicketManager ticketManager) {
        this.ticketManager = ticketManager;
    }

    // instance member method (alphabetic)

    @Override
    public boolean failed(IngestTicket ticket,
                          String name,
                          Object rescue,
                          Throwable t) {
        ticketManager.dispose(ticket);
        failedCount.incrementAndGet();
        return false;
    }

    @Override
    public boolean failed(Object ticket,
                          String name,
                          Object rescue,
                          Throwable t) {
        return failed((IngestTicket) ticket,
                      name,
                      rescue,
                      t);
    }

    /**
     * Returns the number of failed workflows.
     *
     * @return the number of failed workflows.
     */
    public int getFailedCount() {
        return failedCount.get();
    }

    @Override
    public void succeeded(IngestTicket ticket) {
        ticketManager.dispose(ticket);
    }

    @Override
    public void succeeded(Object ticket) {
        succeeded((IngestTicket) ticket);

    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
