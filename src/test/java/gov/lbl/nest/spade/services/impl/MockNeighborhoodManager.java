package gov.lbl.nest.spade.services.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.config.Neighbor;
import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.services.NeighborhoodManager;

/**
 * This class implements the {@link NeighborhoodManager} interface for use in
 * testing.
 * 
 * @author patton
 */
public class MockNeighborhoodManager implements
                                     NeighborhoodManager {

    /**
     * The mapping of {@link OutboundTransfer} instances to their names.
     */
    private final Map<String, OutboundTransfer> outboundTransfers;

    /**
     * Creates an instance of this class.
     * 
     * @param outboundTransfers
     *            the mapping of {@link OutboundTransfer} instances to their names.
     */
    public MockNeighborhoodManager(Map<String, OutboundTransfer> outboundTransfers) {
        this.outboundTransfers = outboundTransfers;
    }

    @Override
    public List<? extends DigestChange> getConfirmables(Neighbor neighbor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getDirectory(String neighbor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<Neighbor> getNeighbourhood() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public OutboundTransfer getOutboundTransfer(String name) {
        return outboundTransfers.get(name);
    }

    @Override
    public Neighbor getSelf() {
        // TODO Auto-generated method stub
        return null;
    }

}
