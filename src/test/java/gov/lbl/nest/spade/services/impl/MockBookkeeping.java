package gov.lbl.nest.spade.services.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.ejb.TicketedFile;
import gov.lbl.nest.spade.rs.Slice;
import gov.lbl.nest.spade.services.Bookkeeping;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;

/**
 * This implementation of the {@link Bookkeeping} interface is used in tests.
 *
 * @author patton
 */
public class MockBookkeeping implements
                             Bookkeeping {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link EntityManager} instance used by this object.
     */
    private EntityManager entityManager;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param entityManager
     *            the {@link EntityManager} instance used by this object.
     */
    public MockBookkeeping(final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // instance member method (alphabetic)

    @Override
    public boolean abandon(String ticket) {
        return true;
    }

    @Override
    public boolean abandon(String ticket,
                           String destination) {
        return true;
    }

    @Override
    public List<DigestChange> getByTime(Stamped stamped,
                                        Date after,
                                        Date before,
                                        boolean reversed,
                                        int max,
                                        String neighbor,
                                        List<String> registrations) {
        final String sinceNamedQuery;
        final String betweenNamedQuery;
        if (Bookkeeping.Stamped.PLACED == stamped) {
            sinceNamedQuery = "placedSince";
            betweenNamedQuery = "placedBetween";
        } else {
            return null;
        }
        final List<TicketedFile> ticketedFiles = getTicketedFilesSince(sinceNamedQuery,
                                                                       betweenNamedQuery,
                                                                       after,
                                                                       before,
                                                                       max);
        if (null == ticketedFiles) {
            return null;
        }
        final List<DigestChange> result = new ArrayList<>(ticketedFiles.size());
        for (TicketedFile ticketedFile : ticketedFiles) {
            final Date when;
            if (Bookkeeping.Stamped.PLACED == stamped) {
                when = ticketedFile.getWhenPlaced();
            } else {
                // Should never get here is all enumerations as dealt with.o
                throw new IllegalArgumentException();
            }
            result.add(new DigestChange(ticketedFile.getBundle(),
                                        when));
        }
        return result;
    }

    @Override
    public Digest getHistoryByTicket(String ticket) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Slice> getPlacementSlices(boolean fineBins,
                                          Integer span,
                                          Date after) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Slice> getShippingSlices(boolean fineBins,
                                         Integer span,
                                         Date after) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Returns the sequence to {@link TicketedFile} instance that match the
     * specified criteria.
     *
     * @param sinceNamedQuery
     *            the name of the query to use when no "before" is specified.
     * @param betweenNamedQuery
     *            the name of the query to use when "before" is specified.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     *
     * @return the sequence to {@link TicketedFile} instance that match the
     *         specified criteria.
     */
    private List<TicketedFile> getTicketedFilesSince(final String sinceNamedQuery,
                                                     final String betweenNamedQuery,
                                                     Date after,
                                                     Date before,
                                                     int max) {
        final TypedQuery<TicketedFile> query;
        if (null == before) {
            query = entityManager.createNamedQuery(sinceNamedQuery,
                                                   TicketedFile.class);
        } else {
            query = entityManager.createNamedQuery(betweenNamedQuery,
                                                   TicketedFile.class);
            query.setParameter("before",
                               before);
        }
        if (null == after) {
            query.setParameter("after",
                               new Date(0));
        } else {
            query.setParameter("after",
                               after);
        }
        if (max > 0) {
            query.setMaxResults(max);
        }
        final List<TicketedFile> ticketedFiles = query.getResultList();
        return ticketedFiles;
    }

    @Override
    public void sending(String ticket,
                        String destination) {
        // TODO Auto-generated method stub

    }

    @Override
    public void sent(String ticket,
                     String destination) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setArchived(String ticket,
                            String suffix) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setBinarySize(String ticket,
                              long size) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setCompressedSize(String ticket,
                                  long size) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setFlushed(String ticket) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setMetadataSize(String ticket,
                                long size) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setPackedSize(String ticket,
                              long size) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setPlacement(String ticket,
                             Object warehouseId,
                             long size) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setRestored(String ticket) {
        // TODO Auto-generated method stub

    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
