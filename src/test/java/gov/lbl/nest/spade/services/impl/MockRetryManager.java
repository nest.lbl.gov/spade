package gov.lbl.nest.spade.services.impl;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.services.RetryManager;
import jakarta.xml.bind.JAXBException;

/**
 * This class implements the {@link RetryManager} interface for use in testing.
 * 
 * @author patton
 */
public class MockRetryManager implements
                              RetryManager {

    @Override
    public boolean cancel(String ticket) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IngestTicket getAwaitingConfirmationTicket(String ticket) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IngestTicket getAwaitingVerificationTicket(String ticket) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IngestTicket getDispatchFailedTicket(String ticket) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getPostponed(Long cushion,
                                     TimeUnit unit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void postponed(IngestTicket ticket,
                          Collection<String> outboundTransfers) throws JAXBException {
        // TODO Auto-generated method stub

    }

    @Override
    public IngestTicket prepare(IngestTicket ticket) throws IOException,
                                                     JAXBException {
        // TODO Auto-generated method stub
        return null;
    }

}
