package gov.lbl.nest.spade.services;

import java.net.URI;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.spade.workflow.ActivityManager;
import gov.lbl.nest.spade.workflow.ActivityMonitor;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowDirector;
import gov.lbl.nest.spade.workflow.WorkflowTermination;
import gov.lbl.nest.spade.workflow.WorkflowsManager;
import gov.lbl.nest.spade.workflow.WorkflowsMonitor;

/**
 * This class implements the {@link WorkflowsManager} so that all workflows do
 * nothing, but report success or failure as set.
 *
 * @author patton
 */
public class MockNullWorkflow implements
                              WorkflowsManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link AtomicInteger} that contains the number of active ingest
     * instances.
     */
    private final AtomicInteger ingestCount = new AtomicInteger(0);

    /**
     * The result to return from starting a workflow.
     */
    private boolean result;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param result
     *            the result to return from starting a workflow.
     */
    public MockNullWorkflow(boolean result) {
        setResult(result);
    }

    // instance member method (alphabetic)

    @Override
    public void drain(Workflow workflow) throws InitializingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ActivityManager getActivityManager(Workflow workflow,
                                              String activity) throws InitializingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public ActivityMonitor getActivityMonitor(Workflow workflow,
                                              String activity) throws InitializingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<? extends String> getActivityNames(Workflow workflow) throws InitializingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public AtomicInteger getCount(Workflow workflow) throws InitializingException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public WorkflowDirector getWorkflowDirector(Workflow workflow) {
        throw new UnsupportedOperationException();
    }

    @Override
    public WorkflowsMonitor getWorkflowsMonitor() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isSuspended(Workflow workflow) throws InitializingException {
        return false;
    }

    @Override
    public void recommence(URI uri,
                           Object message) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> rescueWorkflows(Workflow workflow) throws InitializingException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Sets the result to return from starting a workflow.
     *
     * @param result
     *            the result to return from starting a workflow.
     */
    public void setResult(boolean result) {
        this.result = result;
    }

    @Override
    public boolean setSuspended(Workflow workflow,
                                boolean suspended) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setTermination(Workflow workflow,
                               WorkflowTermination termination) throws InitializingException {
    }

    @Override
    public boolean startWorkflow(Workflow worflow,
                                 String name,
                                 Integer priority,
                                 Object dataObject) throws InitializingException {
        ingestCount.incrementAndGet();
        return result;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
