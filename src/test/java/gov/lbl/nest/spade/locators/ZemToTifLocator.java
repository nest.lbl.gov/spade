package gov.lbl.nest.spade.locators;

import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.SuffixSubstitution;

/**
 * This implements the {@link DataLocator} interface to generate the data file
 * by substituting the ".zem" semaphore suffix with the ".tif" data one.
 *
 * @author patton
 *
 */
public class ZemToTifLocator extends
                             SuffixSubstitution {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     */
    public ZemToTifLocator() {
        super(".zem",
              ".tif");
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
