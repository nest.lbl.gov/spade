package gov.lbl.nest.spade.locators;

import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.SuffixSubstitution;

/**
 * This implements the {@link DataLocator} interface to generate the data file
 * by substituting the ".aem" semaphore suffix with the ".csv" data one.
 *
 * @author patton
 *
 */
public class AemToCsvLocator extends
                             SuffixSubstitution {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     */
    public AemToCsvLocator() {
        super(".aem",
              ".csv");
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
