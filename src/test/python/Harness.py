class FatalError(Exception):
    def __init__(self, message, errorCode, response):
        self.code = errorCode
        self.message = message
        self.response = response

import requests
import sys
import xml.dom.minidom
import xml.etree.ElementTree as ET


class Harness(object):

    DEBUG_SEPARATOR = '--------'
    HEADERS = {'Content-Type': 'application/xml',
               'Accept': 'application/xml'}


    def eprint(self,
                *args,
                **kwargs):
        """Prints to standard error"""
        print(*args, file=sys.stderr, **kwargs)


    def _debug_separator(self):
        """Prints separator use in Debug output"""
        self.eprint(self.DEBUG_SEPARATOR)



    def __init__(self, xml = False):
        self.debug=xml
        if self.debug:
            self._debug_separator()
        self.session=requests.Session()


    def _check_status(self,
                      url,
                      r,
                      expected):
        """Checks the return status of a request to a URL
    
        Keyword arguments:
        url      -- the URL to which the request was made
        r        -- the response to the request
        expected -- the expected response code
        """
        if expected == r.status_code:
            return
        raise FatalError('Unexpected status (' + str(r.status_code) + ') returned from "' + url  + '"',
                         r.status_code,
                         r.text)


    def _pretty_print(self,
                      url,
                      s,
                      response = True):
        """Prints out a formatted version of the supplied XML
    
        :param str url: the URL to which the request was made.
        :param str s: the XML to print.
        :param bool response: True is the XML is the reponse to a request.
        """
        if self.debug:
            if None != url:
                if response:
                    self.eprint('URL : Response : ' + url)
                else:
                    self.eprint('URL : Request :  ' + url)
            self.eprint(xml.dom.minidom.parseString(s).toprettyxml())
            self._debug_separator()


    def get_URL(self,
                 url):
        """Returns an ElementTree containing the contents of the supplied URL
    
        :param str url: the URL whose contents should be returned.
        """
        r = self.session.get(url)
        if 200 != r.status_code:
            return None, r.status_code
        contents = ET.fromstring(r.text)
        self._pretty_print(url, ET.tostring(contents))
        return contents, r.status_code

    def post_URL(self,
                 url,
                 document = None):
        """Returns an ElementTree containing the contents of the supplied URL
    
        :param str url: the URL whose contents should be returned.
        """
        if None == document:
            r = self.session.post(url, headers=self.HEADERS)
        else:
            r = self.session.post(url, data=ET.tostring(document), headers=HEADERS)
        self._check_status(url, r, 200)
        contents = ET.fromstring(r.text)
        self._pretty_print(url, ET.tostring(contents))
        return contents, r.status_code
