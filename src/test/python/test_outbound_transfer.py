#
# Module: test_template
#
# Description: Template for python tests for SPADE.
#
from __future__ import print_function

from Harness import Harness, FatalError

import argparse


def create_parser():
    """Create argparser to be used by this module"""
    parser = argparse.ArgumentParser(
        description="Test that placement into a local warehouse."
    )
    parser.add_argument(
        "-d",
        "--debug",
        dest="DEBUG",
        help="print out RESTful documents.",
        action="store_true",
        default=False,
    )
    return parser


import fnmatch
import os
import socket
import subprocess
import sys
from pathlib import Path

WILDFLY_HOME = Path(os.environ.get("WILDFLY_HOME", Path.home() / "wildfly"))
SPADE_HOMES = Path(os.environ.get("SPADE_HOMES", Path.home() / "spade"))


def create_spade_xml():

    SPADE_ZERO = SPADE_HOMES / "spade.zero"
    if not SPADE_ZERO.exists():
        os.makedirs(SPADE_ZERO)
    SPADE_NAME = "SPADE@" + socket.gethostname()
    with open(SPADE_ZERO / "spade.xml", "w") as f:
        f.write(
            """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<spade_config>
    <assembly>
        <name>"""
            + SPADE_NAME
            + """</name>
    </assembly>
    <warehouse>
        <root_path>"""
            + str(SPADE_ZERO / "warehouse")
            + """</root_path>
    </warehouse>
    <cache>
        <root_path>"""
            + str(SPADE_ZERO / "cache")
            + """</root_path>
        <minimum units="GB">1</minimum>
    </cache>
</spade_config>"""
        )


def create_registration():
    local_registrations = SPADE_HOMES / "spade.zero" / "registrations" / "local"
    if not local_registrations.exists():
        os.makedirs(local_registrations)
    dropbox = SPADE_HOMES / "data" / "zero" / "dropbox" / "loopback"
    if not dropbox.exists():
        os.makedirs(dropbox)
    with open(local_registrations / "local_warehouse.2.xml", "w") as f:
        f.write(
            """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<registration>
    <local_id>loopback.2</local_id>
    <drop_box>
        <location>
            <directory>"""
            + str(dropbox)
            + """</directory>
        </location>
        <pattern>.*.lem</pattern>
        <mapping>gov.lbl.nest.spade.tour.LoopbackLocator</mapping>
    </drop_box>
    <warehouse>false</warehouse>
    <outbound_transfer>Loopback to Self</outbound_transfer>
</registration>
"""
        )


def patch_spade_xml():
    SPADE_ZERO = SPADE_HOMES / "spade.zero"
    if not SPADE_ZERO.exists():
        os.makedirs(SPADE_ZERO)
    SPADE_NAME = "SPADE@" + socket.gethostname()

    receiving = SPADE_HOMES / "data" / "zero" / "receiving" / "loopback"
    if not receiving.exists():
        os.makedirs(receiving)

    patch_file = Path("patch.spade_xml")
    with open(patch_file, "w") as f:
        f.write(
            """*** /Users/patton/spade/spade.zero/spade.xml	1969-12-31 16:00:00.000000000 -0800
--- /Users/patton/spade/spade.zero/spade.xml	1969-12-31 16:00:00.000000000 -0800
***************
*** 3,8 ****
--- 3,15 ----
      <assembly>
          <name>"""
            + SPADE_NAME
            + """</name>
      </assembly>
+     <outbound_transfer>
+         <name>Loopback to Self</name>
+         <description>Send bundles back to itself use the cp command</description>
+         <neighbor>"""
            + SPADE_NAME
            + """</neighbor>
+         <location>localhost:"""
            + str(receiving)
            + """</location>
+         <class>gov.lbl.nest.spade.services.impl.LocalhostTransfer</class>
+     </outbound_transfer>
      <warehouse>
          <root_path>"""
            + str(SPADE_ZERO / "warehouse")
            + """</root_path>
      </warehouse>
"""
        )
    SPADE_XML = SPADE_ZERO / "spade.xml"
    subprocess.call(["patch", "-N", str(SPADE_XML), str(patch_file)])
    patch_file.unlink()


def queue_transfer():
    dropbox = SPADE_HOMES / "data" / "zero" / "dropbox" / "loopback"
    with open(dropbox / "tour.2.data", "w") as f:
        f.write(
            """This data file should be shipped via the "Loopback to Self" outbound
transfer and then, upon reception at the other end, placed in the
warehouse.
"""
        )
    (dropbox / "tour.2.lem").touch()


def start_spade():
    SPADE_ZERO = SPADE_HOMES / "spade.zero"
    WEBINF_PATH = (
        Path("WEB-INF") / "classes" / "gov" / "lbl" / "nest" / "spade" / "config"
    )
    SPADE_WEB_CONFIG_DIR = SPADE_ZERO / WEBINF_PATH
    if not SPADE_WEB_CONFIG_DIR.exists():
        os.makedirs(SPADE_WEB_CONFIG_DIR)
    SPADE_WEB_CONFIG = SPADE_WEB_CONFIG_DIR / "spade_config"
    with open(SPADE_WEB_CONFIG, "w") as f:
        f.write(
            """# This file may contain the path to an alternate configuration file for SPADE.
"""
            + str(SPADE_ZERO / "spade.xml")
            + """
"""
        )
    WILDFLY_DEPLOYMENTS = WILDFLY_HOME / "standalone" / "deployments"
    DEPLOYED_SPADE = WILDFLY_DEPLOYMENTS / "spade.war"
    subprocess.call(["jar", "uf", str(DEPLOYED_SPADE), "-C", SPADE_ZERO, WEBINF_PATH])


def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result


def check_delivery():
    receiving = SPADE_HOMES / "data" / "zero" / "receiving" / "loopback"
    found = find("*tour.2.*", receiving)
    if 2 == len(found):
        print("outbound transfer was successful")


def main():
    """Main routine for this module"""
    parser = create_parser()
    options, ignore = parser.parse_known_args()
    if options.DEBUG:
        print(options, ignore)
    harness = Harness(options.DEBUG)

    url = os.getenv("SPADE_APPLICATION", "http://localhost:8080/spade/local/report/")
    try:
#        create_registration()
        patch_spade_xml()
#        start_spade()
#        queue_transfer()
#        check_delivery()
    except FatalError as e:
        harness.eprint(e.message)
        sys.exit(e.code)


if __name__ == "__main__":
    main()
