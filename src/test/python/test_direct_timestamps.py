#
# Module: test_template
#
# Description: Tests that the direct URL Timestamp requests work and are consistent.
#
from __future__ import print_function

from Harness import Harness, FatalError

import argparse


def create_parser():
    """Create argparser to be used by this module"""
    parser = argparse.ArgumentParser(
        description="Tests that the direct URL Timestamp requests work and are consistent."
    )
    parser.add_argument(
        "-d",
        "--debug",
        dest="DEBUG",
        help="print out RESTful documents.",
        action="store_true",
        default=False,
    )
    return parser


import os
import sys


def main():
    """Main routine for this module"""
    parser = create_parser()
    options, ignore = parser.parse_known_args()
    if options.DEBUG:
        print(options, ignore)
    harness = Harness(options.DEBUG)

    url = os.getenv("SPADE_APPLICATION", "http://localhost:8080/spade/local/report/")
    try:
        stamps = [
            "placed",
            "ticketed",
            "dispatched",
            "verified",
            "confirmable",
            "confirmed",
        ]
        for stamp in stamps:
            timestampUrl = url + "timestamps/" + stamp + "?max=4"
            harness.get_URL(timestampUrl)
            timestampUrl = timestampUrl + "&reversed=true"
            harness.get_URL(timestampUrl)
            print(stamp + " requests have been successful")
    except FatalError as e:
        harness.eprint(e.message)
        sys.exit(e.code)


if __name__ == "__main__":
    main()
