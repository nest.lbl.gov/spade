#
# Module: test_template
#
# Description: Template for python tests for SPADE.
#
from __future__ import print_function

from Harness import Harness, FatalError

import argparse


def create_parser():
    """Create argparser to be used by this module"""
    parser = argparse.ArgumentParser(
        description="Test that placement into a local warehouse."
    )
    parser.add_argument(
        "-d",
        "--debug",
        dest="DEBUG",
        help="print out RESTful documents.",
        action="store_true",
        default=False,
    )
    return parser

import fnmatch
import os
from pathlib import Path
import sys
import subprocess
import time

WILDFLY_HOME = Path(os.environ.get("WILDFLY_HOME", Path.home() / "wildfly"))
SPADE_HOMES = Path(os.environ.get("SPADE_HOMES", Path.home() / "spade"))


def create_spade_xml():
    import socket

    SPADE_ZERO = SPADE_HOMES / "spade.zero"
    if not SPADE_ZERO.exists():
        os.makedirs(SPADE_ZERO)
    SPADE_NAME = "SPADE@" + socket.gethostname()
    with open(SPADE_ZERO / "spade.xml", "w") as f:
        f.write(
            """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<spade_config>
    <assembly>
        <name>"""
            + SPADE_NAME
            + """</name>
    </assembly>
    <warehouse>
        <root_path>"""
            + str(SPADE_ZERO / "warehouse")
            + """</root_path>
    </warehouse>
    <cache>
        <root_path>"""
            + str(SPADE_ZERO / "cache")
            + """</root_path>
        <minimum units="GB">1</minimum>
    </cache>
</spade_config>"""
        )


def create_registration():
    local_registrations = SPADE_HOMES / "spade.zero" / "registrations" / "local"
    if not local_registrations.exists():
        os.makedirs(local_registrations)
    dropbox = SPADE_HOMES / "data" / "zero" / "dropbox" / "loopback"
    if not dropbox.exists():
        os.makedirs(dropbox)
    with open(local_registrations / "local_warehouse.1.xml", "w") as f:
        f.write(
            """<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<registration>
    <local_id>local.warehouse.1</local_id>
    <drop_box>
        <location>
            <directory>"""
            + str(dropbox)
            + """</directory>
        </location>
        <pattern>.*.sem</pattern>
    </drop_box>
</registration>
"""
        )


def queue_transfer():
    dropbox = SPADE_HOMES / "data" / "zero" / "dropbox" / "loopback"
    with open(dropbox / "tour.1.data", "w") as f:
        f.write(
            """This data file should be placed in the local warehouse
"""
        )
    (dropbox / "tour.1.sem").touch()


def trigger_local_scan(harness, url):
    application, status = harness.get_URL(url)
    action = application.find('commands/[name = "application"]/action/[name = "local_scan"]')
    local_scan = action.find('uri').text
    harness.post_URL(local_scan)


def start_spade(harness, url):
    SPADE_ZERO = SPADE_HOMES / "spade.zero"
    WEBINF_PATH = (
        Path("WEB-INF") / "classes" / "gov" / "lbl" / "nest" / "spade" / "config"
    )
    SPADE_WEB_CONFIG_DIR = SPADE_ZERO / WEBINF_PATH
    if not SPADE_WEB_CONFIG_DIR.exists():
        os.makedirs(SPADE_WEB_CONFIG_DIR)
    SPADE_WEB_CONFIG = SPADE_WEB_CONFIG_DIR / "spade_config"
    with open(SPADE_WEB_CONFIG, "w") as f:
        f.write(
            """# This file may contain the path to an alternate configuration file for SPADE.
"""
            + str(SPADE_ZERO / "spade.xml")
            + """
"""
        )
    WILDFLY_DEPLOYMENTS = WILDFLY_HOME / "standalone" / "deployments"
    DEPLOYED_SPADE = WILDFLY_DEPLOYMENTS / "spade.war"
    subprocess.call(["jar", "uf", str(DEPLOYED_SPADE), "-C", SPADE_ZERO, WEBINF_PATH])
    time.sleep(10)
    r, status = harness.get_URL(url)
    while 200 != status:
        time.sleep(2)
        r, status = harness.get_URL(url)


def find(pattern, path):
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

def check_warehouse():
    SPADE_ZERO = SPADE_HOMES / "spade.zero"
    SPADE_WAREHOUSE = SPADE_ZERO / "warehouse"
    found = find("*tour.1.*", SPADE_WAREHOUSE)
    if 2 == len(found):
        print("Local warehousing was successful")


def main():
    """Main routine for this module"""
    parser = create_parser()
    options, ignore = parser.parse_known_args()
    if options.DEBUG:
        print(options, ignore)
    harness = Harness(options.DEBUG)

    url = os.getenv("SPADE_APPLICATION", "http://localhost:8080/spade/local/report/")
    try:
        create_spade_xml()
        create_registration()
        start_spade(harness, url)
        queue_transfer()
        trigger_local_scan(harness, url)
        time.sleep(2)
        check_warehouse()
    except FatalError as e:
        harness.eprint(e.message)
        sys.exit(e.code)


if __name__ == "__main__":
    main()
