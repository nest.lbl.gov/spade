--
-- PostgreSQL database dump
--

-- Dumped from database version 14.1
-- Dumped by pg_dump version 14.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: confirmation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.confirmation (
    confirmationkey integer NOT NULL,
    whenabandoned timestamp with time zone,
    whenconfirmed timestamp with time zone,
    whendelivered timestamp with time zone,
    destination_neighborkey integer NOT NULL,
    shippedfile_ticketedfilekey integer NOT NULL
);


--
-- Name: dispatch; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dispatch (
    dispatchkey integer NOT NULL,
    courseepochcompleted bigint,
    fineepochcompleted bigint,
    whenabandoned timestamp with time zone,
    whencompleted timestamp with time zone,
    whenstarted timestamp with time zone,
    destination_neighborkey integer NOT NULL,
    ticketedfile_ticketedfilekey integer NOT NULL
);


--
-- Name: entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.entry (
    entrykey integer NOT NULL,
    identity character varying(255),
    whenlastmodified timestamp with time zone,
    compressedpath_pathkey integer,
    datapath_pathkey integer,
    metapath_pathkey integer,
    wrappedpath_pathkey integer
);


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hierarchicalpath; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hierarchicalpath (
    pathkey integer NOT NULL,
    cachedpath character varying(255),
    name character varying(255),
    parent_pathkey integer NOT NULL
);


--
-- Name: inboundregistration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.inboundregistration (
    registrationkey integer NOT NULL,
    knownneighbor_neighborkey integer NOT NULL
);


--
-- Name: knownregistration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.knownregistration (
    registrationkey integer NOT NULL,
    localid character varying(255),
    whensuperseded timestamp with time zone
);


--
-- Name: lastwatched; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lastwatched (
    lastwatchedkey integer NOT NULL,
    lastdatetime timestamp with time zone NOT NULL,
    role character varying(255) NOT NULL
);


--
-- Name: localregistration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.localregistration (
    registrationkey integer NOT NULL
);


--
-- Name: neighbor; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.neighbor (
    neighborkey integer NOT NULL,
    name character varying(255)
);


--
-- Name: placedfile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.placedfile (
    courseepochplaced bigint,
    fineepochplaced bigint,
    placedsize bigint,
    placementid character varying(255),
    whenplaced timestamp with time zone,
    ticketedfilekey integer NOT NULL
);


--
-- Name: poi; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poi (
    pointofinterestkey integer NOT NULL,
    level integer,
    status integer,
    whenoccurred timestamp with time zone,
    guid_poiguidkey integer,
    type_poitypekey integer
);


--
-- Name: poiattribute; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiattribute (
    poiattributekey integer NOT NULL,
    value character varying(255),
    name_poiattributenamekey integer,
    pointofinterest_pointofinterestkey integer
);


--
-- Name: poiattributename; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiattributename (
    poiattributenamekey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poiguid; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiguid (
    poiguidkey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poitype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poitype (
    poitypekey integer NOT NULL,
    value character varying(255)
);


--
-- Name: seenitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seenitem (
    seenitemkey integer NOT NULL,
    item character varying(256) NOT NULL,
    lastwatched_lastwatchedkey integer NOT NULL
);


--
-- Name: shippedfile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.shippedfile (
    ticketedfilekey integer NOT NULL,
    algorithm character varying(255),
    checksum bigint,
    remoteregistration character varying(255),
    remoteticket character varying(255),
    whenabandoned timestamp with time zone,
    whenconfirmable timestamp with time zone,
    whenverificationcompleted timestamp with time zone,
    whenverificationstarted timestamp with time zone
);


--
-- Name: ticketedfile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ticketedfile (
    ticketedfilekey integer NOT NULL,
    archivesuffix character varying(255),
    binarysize bigint,
    bundle character varying(255) NOT NULL,
    compressedsize bigint,
    metadatasize bigint,
    packedsize bigint,
    whenabandoned timestamp with time zone,
    whenarchived timestamp with time zone,
    whenbinaryset timestamp with time zone,
    whencompressedset timestamp with time zone,
    whenflushed timestamp with time zone,
    whenmetadataset timestamp with time zone,
    whenpackedset timestamp with time zone,
    whenrestored timestamp with time zone,
    whenticketed timestamp with time zone,
    localregistration_registrationkey integer NOT NULL
);


--
-- Name: confirmation confirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.confirmation
    ADD CONSTRAINT confirmation_pkey PRIMARY KEY (confirmationkey);


--
-- Name: dispatch dispatch_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispatch
    ADD CONSTRAINT dispatch_pkey PRIMARY KEY (dispatchkey);


--
-- Name: entry entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT entry_pkey PRIMARY KEY (entrykey);


--
-- Name: hierarchicalpath hierarchicalpath_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hierarchicalpath
    ADD CONSTRAINT hierarchicalpath_pkey PRIMARY KEY (pathkey);


--
-- Name: inboundregistration inboundregistration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inboundregistration
    ADD CONSTRAINT inboundregistration_pkey PRIMARY KEY (registrationkey);


--
-- Name: knownregistration knownregistration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.knownregistration
    ADD CONSTRAINT knownregistration_pkey PRIMARY KEY (registrationkey);


--
-- Name: lastwatched lastwatched_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lastwatched
    ADD CONSTRAINT lastwatched_pkey PRIMARY KEY (lastwatchedkey);


--
-- Name: localregistration localregistration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localregistration
    ADD CONSTRAINT localregistration_pkey PRIMARY KEY (registrationkey);


--
-- Name: neighbor neighbor_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.neighbor
    ADD CONSTRAINT neighbor_pkey PRIMARY KEY (neighborkey);


--
-- Name: placedfile placedfile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.placedfile
    ADD CONSTRAINT placedfile_pkey PRIMARY KEY (ticketedfilekey);


--
-- Name: poi poi_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT poi_pkey PRIMARY KEY (pointofinterestkey);


--
-- Name: poiattribute poiattribute_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT poiattribute_pkey PRIMARY KEY (poiattributekey);


--
-- Name: poiattributename poiattributename_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattributename
    ADD CONSTRAINT poiattributename_pkey PRIMARY KEY (poiattributenamekey);


--
-- Name: poiguid poiguid_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiguid
    ADD CONSTRAINT poiguid_pkey PRIMARY KEY (poiguidkey);


--
-- Name: poitype poitype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poitype
    ADD CONSTRAINT poitype_pkey PRIMARY KEY (poitypekey);


--
-- Name: seenitem seenitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seenitem
    ADD CONSTRAINT seenitem_pkey PRIMARY KEY (seenitemkey);


--
-- Name: shippedfile shippedfile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.shippedfile
    ADD CONSTRAINT shippedfile_pkey PRIMARY KEY (ticketedfilekey);


--
-- Name: ticketedfile ticketedfile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ticketedfile
    ADD CONSTRAINT ticketedfile_pkey PRIMARY KEY (ticketedfilekey);


--
-- Name: confirmation_destination_neighborkey_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX confirmation_destination_neighborkey_idx ON public.confirmation USING btree (destination_neighborkey);


--
-- Name: confirmation_shippedfile_ticketedfilekey_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX confirmation_shippedfile_ticketedfilekey_idx ON public.confirmation USING btree (shippedfile_ticketedfilekey);


--
-- Name: confirmation_whenconfirmed_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX confirmation_whenconfirmed_idx ON public.confirmation USING btree (whenconfirmed);


--
-- Name: confirmation_whendelivered_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX confirmation_whendelivered_idx ON public.confirmation USING btree (whendelivered);


--
-- Name: dispatch_courseepochcompleted_fineepochcompleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX dispatch_courseepochcompleted_fineepochcompleted_idx ON public.dispatch USING btree (courseepochcompleted, fineepochcompleted);


--
-- Name: dispatch_destination_neighborkey_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX dispatch_destination_neighborkey_idx ON public.dispatch USING btree (destination_neighborkey);


--
-- Name: dispatch_ticketedfile_ticketedfilekey_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX dispatch_ticketedfile_ticketedfilekey_idx ON public.dispatch USING btree (ticketedfile_ticketedfilekey);


--
-- Name: dispatch_whenabandoned_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX dispatch_whenabandoned_idx ON public.dispatch USING btree (whenabandoned);


--
-- Name: dispatch_whencompleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX dispatch_whencompleted_idx ON public.dispatch USING btree (whencompleted);


--
-- Name: dispatch_whenstarted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX dispatch_whenstarted_idx ON public.dispatch USING btree (whenstarted);


--
-- Name: knownregistration_localid_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX knownregistration_localid_idx ON public.knownregistration USING btree (localid);


--
-- Name: knownregistration_whensuperseded_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX knownregistration_whensuperseded_idx ON public.knownregistration USING btree (whensuperseded);


--
-- Name: shippedfile_ticketedfilekey_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX shippedfile_ticketedfilekey_idx ON public.shippedfile USING btree (ticketedfilekey);


--
-- Name: shippedfile_whenabandoned_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX shippedfile_whenabandoned_idx ON public.shippedfile USING btree (whenabandoned);


--
-- Name: shippedfile_whenconfirmable_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX shippedfile_whenconfirmable_idx ON public.shippedfile USING btree (whenconfirmable);


--
-- Name: shippedfile_whenverificationcompleted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX shippedfile_whenverificationcompleted_idx ON public.shippedfile USING btree (whenverificationcompleted);


--
-- Name: shippedfile_whenverificationstarted_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX shippedfile_whenverificationstarted_idx ON public.shippedfile USING btree (whenverificationstarted);


--
-- Name: ticketedfile_localregistration_registrationkey_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ticketedfile_localregistration_registrationkey_idx ON public.ticketedfile USING btree (localregistration_registrationkey);


--
-- Name: ticketedfile_whenabandoned_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ticketedfile_whenabandoned_idx ON public.ticketedfile USING btree (whenabandoned);


--
-- Name: ticketedfile_whenticketed_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX ticketedfile_whenticketed_idx ON public.ticketedfile USING btree (whenticketed);


--
-- Name: entry fk3yy4xwxid2ccwi8ds5203fgn6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT fk3yy4xwxid2ccwi8ds5203fgn6 FOREIGN KEY (datapath_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: dispatch fk9etp5fova6pvqsamlhorldbqh; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispatch
    ADD CONSTRAINT fk9etp5fova6pvqsamlhorldbqh FOREIGN KEY (destination_neighborkey) REFERENCES public.neighbor(neighborkey);


--
-- Name: dispatch fk9t5cpdk3v7c46enkpckewulfi; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispatch
    ADD CONSTRAINT fk9t5cpdk3v7c46enkpckewulfi FOREIGN KEY (ticketedfile_ticketedfilekey) REFERENCES public.ticketedfile(ticketedfilekey);


--
-- Name: localregistration fkb4xjhbcespini9tnpya47101b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localregistration
    ADD CONSTRAINT fkb4xjhbcespini9tnpya47101b FOREIGN KEY (registrationkey) REFERENCES public.knownregistration(registrationkey);


--
-- Name: entry fkbxsckvwqm4gqql5laq94qkivu; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT fkbxsckvwqm4gqql5laq94qkivu FOREIGN KEY (wrappedpath_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: hierarchicalpath fkdf9gsahm0hll28n19gosrydvh; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hierarchicalpath
    ADD CONSTRAINT fkdf9gsahm0hll28n19gosrydvh FOREIGN KEY (parent_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: confirmation fkeeu6nbkv0vtwmjy4l6dv5fyvd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.confirmation
    ADD CONSTRAINT fkeeu6nbkv0vtwmjy4l6dv5fyvd FOREIGN KEY (destination_neighborkey) REFERENCES public.neighbor(neighborkey);


--
-- Name: poi fkf2erw9j5mh3342utu16on4egp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT fkf2erw9j5mh3342utu16on4egp FOREIGN KEY (guid_poiguidkey) REFERENCES public.poiguid(poiguidkey);


--
-- Name: entry fkfnfybxs1y4awwom2wq4ftrdh2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT fkfnfybxs1y4awwom2wq4ftrdh2 FOREIGN KEY (compressedpath_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: entry fkfw3e5chhtrqdklp8nv7bdbixg; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT fkfw3e5chhtrqdklp8nv7bdbixg FOREIGN KEY (metapath_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: poi fkgkqlhivye5d9x3ca1jwr6h08v; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT fkgkqlhivye5d9x3ca1jwr6h08v FOREIGN KEY (type_poitypekey) REFERENCES public.poitype(poitypekey);


--
-- Name: poiattribute fkhbmf9108sv87mr5eswkso49br; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT fkhbmf9108sv87mr5eswkso49br FOREIGN KEY (pointofinterest_pointofinterestkey) REFERENCES public.poi(pointofinterestkey);


--
-- Name: seenitem fkle7rsa4d2lrpkjajtuvc6qmyj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seenitem
    ADD CONSTRAINT fkle7rsa4d2lrpkjajtuvc6qmyj FOREIGN KEY (lastwatched_lastwatchedkey) REFERENCES public.lastwatched(lastwatchedkey);


--
-- Name: placedfile fklnrvk1kpkp5kbmge8sjm9aany; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.placedfile
    ADD CONSTRAINT fklnrvk1kpkp5kbmge8sjm9aany FOREIGN KEY (ticketedfilekey) REFERENCES public.ticketedfile(ticketedfilekey);


--
-- Name: confirmation fknd5410dfqq62r7w1fdbism5h8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.confirmation
    ADD CONSTRAINT fknd5410dfqq62r7w1fdbism5h8 FOREIGN KEY (shippedfile_ticketedfilekey) REFERENCES public.shippedfile(ticketedfilekey);


--
-- Name: inboundregistration fkndx4iur7prax0yuynfh3d1ign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inboundregistration
    ADD CONSTRAINT fkndx4iur7prax0yuynfh3d1ign FOREIGN KEY (knownneighbor_neighborkey) REFERENCES public.neighbor(neighborkey);


--
-- Name: inboundregistration fksf5fsv6kk089eel2208tok3ny; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inboundregistration
    ADD CONSTRAINT fksf5fsv6kk089eel2208tok3ny FOREIGN KEY (registrationkey) REFERENCES public.knownregistration(registrationkey);


--
-- Name: poiattribute fkt89qj0m0jxapdi0mypkw02xgl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT fkt89qj0m0jxapdi0mypkw02xgl FOREIGN KEY (name_poiattributenamekey) REFERENCES public.poiattributename(poiattributenamekey);


--
-- Name: ticketedfile fkt8tl27xkg7v18toaximry5vc9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ticketedfile
    ADD CONSTRAINT fkt8tl27xkg7v18toaximry5vc9 FOREIGN KEY (localregistration_registrationkey) REFERENCES public.knownregistration(registrationkey);


--
-- PostgreSQL database dump complete
--

