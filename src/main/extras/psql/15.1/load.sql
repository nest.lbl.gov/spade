--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1
-- Dumped by pg_dump version 15.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: confirmation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.confirmation (
    confirmationkey integer NOT NULL,
    destination_neighborkey integer NOT NULL,
    shippedfile_ticketedfilekey integer NOT NULL,
    whenabandoned timestamp(6) without time zone,
    whenconfirmed timestamp(6) without time zone,
    whendelivered timestamp(6) without time zone
);


--
-- Name: confirmation_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.confirmation_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: dispatch; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.dispatch (
    destination_neighborkey integer NOT NULL,
    dispatchkey integer NOT NULL,
    ticketedfile_ticketedfilekey integer NOT NULL,
    courseepochcompleted bigint,
    fineepochcompleted bigint,
    whenabandoned timestamp(6) without time zone,
    whencompleted timestamp(6) without time zone,
    whenstarted timestamp(6) without time zone
);


--
-- Name: dispatch_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.dispatch_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: entry; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.entry (
    compressedpath_pathkey integer,
    datapath_pathkey integer,
    entrykey integer NOT NULL,
    metapath_pathkey integer,
    wrappedpath_pathkey integer,
    whenlastmodified timestamp(6) without time zone,
    identity character varying(255)
);


--
-- Name: entry_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.entry_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: hierarchicalpath; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.hierarchicalpath (
    parent_pathkey integer NOT NULL,
    pathkey integer NOT NULL,
    cachedpath character varying(255),
    name character varying(255)
);


--
-- Name: hierarchicalpath_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.hierarchicalpath_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: inboundregistration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.inboundregistration (
    knownneighbor_neighborkey integer NOT NULL,
    registrationkey integer NOT NULL
);


--
-- Name: knownregistration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.knownregistration (
    registrationkey integer NOT NULL,
    whensuperseded timestamp(6) without time zone,
    localid character varying(255)
);


--
-- Name: knownregistration_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.knownregistration_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: lastwatched; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.lastwatched (
    lastwatchedkey integer NOT NULL,
    lastdatetime timestamp(6) without time zone NOT NULL,
    role character varying(255) NOT NULL
);


--
-- Name: lastwatched_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.lastwatched_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: localregistration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.localregistration (
    registrationkey integer NOT NULL
);


--
-- Name: neighbor; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.neighbor (
    neighborkey integer NOT NULL,
    name character varying(255)
);


--
-- Name: neighbor_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.neighbor_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: placedfile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.placedfile (
    ticketedfilekey integer NOT NULL,
    courseepochplaced bigint,
    fineepochplaced bigint,
    placedsize bigint,
    whenplaced timestamp(6) without time zone,
    placementid character varying(255)
);


--
-- Name: poi; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poi (
    guid_poiguidkey integer,
    level smallint,
    pointofinterestkey integer NOT NULL,
    status integer,
    type_poitypekey integer,
    whenoccurred timestamp(6) without time zone,
    CONSTRAINT poi_level_check CHECK (((level >= 0) AND (level <= 8)))
);


--
-- Name: poi_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poi_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poiattribute; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiattribute (
    name_poiattributenamekey integer,
    poiattributekey integer NOT NULL,
    pointofinterest_pointofinterestkey integer,
    value character varying(255)
);


--
-- Name: poiattribute_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poiattribute_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poiattributename; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiattributename (
    poiattributenamekey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poiattributename_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poiattributename_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poiguid; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poiguid (
    poiguidkey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poiguid_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poiguid_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: poitype; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.poitype (
    poitypekey integer NOT NULL,
    value character varying(255)
);


--
-- Name: poitype_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.poitype_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: seenitem; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.seenitem (
    lastwatched_lastwatchedkey integer NOT NULL,
    seenitemkey integer NOT NULL,
    item character varying(256) NOT NULL
);


--
-- Name: seenitem_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.seenitem_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: shippedfile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.shippedfile (
    ticketedfilekey integer NOT NULL,
    checksum bigint,
    whenabandoned timestamp(6) without time zone,
    whenconfirmable timestamp(6) without time zone,
    whenverificationcompleted timestamp(6) without time zone,
    whenverificationstarted timestamp(6) without time zone,
    algorithm character varying(255),
    remoteregistration character varying(255),
    remoteticket character varying(255)
);


--
-- Name: ticketedfile; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.ticketedfile (
    localregistration_registrationkey integer NOT NULL,
    ticketedfilekey integer NOT NULL,
    binarysize bigint,
    compressedsize bigint,
    metadatasize bigint,
    packedsize bigint,
    whenabandoned timestamp(6) without time zone,
    whenarchived timestamp(6) without time zone,
    whenbinaryset timestamp(6) without time zone,
    whencompressedset timestamp(6) without time zone,
    whenflushed timestamp(6) without time zone,
    whenmetadataset timestamp(6) without time zone,
    whenpackedset timestamp(6) without time zone,
    whenrestored timestamp(6) without time zone,
    whenticketed timestamp(6) without time zone,
    archivesuffix character varying(255),
    bundle character varying(255) NOT NULL
);


--
-- Name: ticketedfile_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.ticketedfile_seq
    START WITH 1
    INCREMENT BY 50
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: confirmation confirmation_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.confirmation
    ADD CONSTRAINT confirmation_pkey PRIMARY KEY (confirmationkey);


--
-- Name: dispatch dispatch_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispatch
    ADD CONSTRAINT dispatch_pkey PRIMARY KEY (dispatchkey);


--
-- Name: entry entry_compressedpath_pathkey_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT entry_compressedpath_pathkey_key UNIQUE (compressedpath_pathkey);


--
-- Name: entry entry_datapath_pathkey_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT entry_datapath_pathkey_key UNIQUE (datapath_pathkey);


--
-- Name: entry entry_metapath_pathkey_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT entry_metapath_pathkey_key UNIQUE (metapath_pathkey);


--
-- Name: entry entry_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT entry_pkey PRIMARY KEY (entrykey);


--
-- Name: entry entry_wrappedpath_pathkey_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT entry_wrappedpath_pathkey_key UNIQUE (wrappedpath_pathkey);


--
-- Name: hierarchicalpath hierarchicalpath_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hierarchicalpath
    ADD CONSTRAINT hierarchicalpath_pkey PRIMARY KEY (pathkey);


--
-- Name: inboundregistration inboundregistration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inboundregistration
    ADD CONSTRAINT inboundregistration_pkey PRIMARY KEY (registrationkey);


--
-- Name: knownregistration knownregistration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.knownregistration
    ADD CONSTRAINT knownregistration_pkey PRIMARY KEY (registrationkey);


--
-- Name: lastwatched lastwatched_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.lastwatched
    ADD CONSTRAINT lastwatched_pkey PRIMARY KEY (lastwatchedkey);


--
-- Name: localregistration localregistration_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localregistration
    ADD CONSTRAINT localregistration_pkey PRIMARY KEY (registrationkey);


--
-- Name: neighbor neighbor_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.neighbor
    ADD CONSTRAINT neighbor_pkey PRIMARY KEY (neighborkey);


--
-- Name: placedfile placedfile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.placedfile
    ADD CONSTRAINT placedfile_pkey PRIMARY KEY (ticketedfilekey);


--
-- Name: poi poi_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT poi_pkey PRIMARY KEY (pointofinterestkey);


--
-- Name: poiattribute poiattribute_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT poiattribute_pkey PRIMARY KEY (poiattributekey);


--
-- Name: poiattributename poiattributename_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattributename
    ADD CONSTRAINT poiattributename_pkey PRIMARY KEY (poiattributenamekey);


--
-- Name: poiguid poiguid_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiguid
    ADD CONSTRAINT poiguid_pkey PRIMARY KEY (poiguidkey);


--
-- Name: poitype poitype_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poitype
    ADD CONSTRAINT poitype_pkey PRIMARY KEY (poitypekey);


--
-- Name: seenitem seenitem_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seenitem
    ADD CONSTRAINT seenitem_pkey PRIMARY KEY (seenitemkey);


--
-- Name: shippedfile shippedfile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.shippedfile
    ADD CONSTRAINT shippedfile_pkey PRIMARY KEY (ticketedfilekey);


--
-- Name: ticketedfile ticketedfile_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ticketedfile
    ADD CONSTRAINT ticketedfile_pkey PRIMARY KEY (ticketedfilekey);


--
-- Name: entry fk3yy4xwxid2ccwi8ds5203fgn6; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT fk3yy4xwxid2ccwi8ds5203fgn6 FOREIGN KEY (datapath_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: dispatch fk9etp5fova6pvqsamlhorldbqh; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispatch
    ADD CONSTRAINT fk9etp5fova6pvqsamlhorldbqh FOREIGN KEY (destination_neighborkey) REFERENCES public.neighbor(neighborkey);


--
-- Name: dispatch fk9t5cpdk3v7c46enkpckewulfi; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.dispatch
    ADD CONSTRAINT fk9t5cpdk3v7c46enkpckewulfi FOREIGN KEY (ticketedfile_ticketedfilekey) REFERENCES public.ticketedfile(ticketedfilekey);


--
-- Name: localregistration fkb4xjhbcespini9tnpya47101b; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.localregistration
    ADD CONSTRAINT fkb4xjhbcespini9tnpya47101b FOREIGN KEY (registrationkey) REFERENCES public.knownregistration(registrationkey);


--
-- Name: entry fkbxsckvwqm4gqql5laq94qkivu; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT fkbxsckvwqm4gqql5laq94qkivu FOREIGN KEY (wrappedpath_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: hierarchicalpath fkdf9gsahm0hll28n19gosrydvh; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.hierarchicalpath
    ADD CONSTRAINT fkdf9gsahm0hll28n19gosrydvh FOREIGN KEY (parent_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: confirmation fkeeu6nbkv0vtwmjy4l6dv5fyvd; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.confirmation
    ADD CONSTRAINT fkeeu6nbkv0vtwmjy4l6dv5fyvd FOREIGN KEY (destination_neighborkey) REFERENCES public.neighbor(neighborkey);


--
-- Name: poi fkf2erw9j5mh3342utu16on4egp; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT fkf2erw9j5mh3342utu16on4egp FOREIGN KEY (guid_poiguidkey) REFERENCES public.poiguid(poiguidkey);


--
-- Name: entry fkfnfybxs1y4awwom2wq4ftrdh2; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT fkfnfybxs1y4awwom2wq4ftrdh2 FOREIGN KEY (compressedpath_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: entry fkfw3e5chhtrqdklp8nv7bdbixg; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.entry
    ADD CONSTRAINT fkfw3e5chhtrqdklp8nv7bdbixg FOREIGN KEY (metapath_pathkey) REFERENCES public.hierarchicalpath(pathkey);


--
-- Name: poi fkgkqlhivye5d9x3ca1jwr6h08v; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poi
    ADD CONSTRAINT fkgkqlhivye5d9x3ca1jwr6h08v FOREIGN KEY (type_poitypekey) REFERENCES public.poitype(poitypekey);


--
-- Name: poiattribute fkhbmf9108sv87mr5eswkso49br; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT fkhbmf9108sv87mr5eswkso49br FOREIGN KEY (pointofinterest_pointofinterestkey) REFERENCES public.poi(pointofinterestkey);


--
-- Name: seenitem fkle7rsa4d2lrpkjajtuvc6qmyj; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.seenitem
    ADD CONSTRAINT fkle7rsa4d2lrpkjajtuvc6qmyj FOREIGN KEY (lastwatched_lastwatchedkey) REFERENCES public.lastwatched(lastwatchedkey);


--
-- Name: placedfile fklnrvk1kpkp5kbmge8sjm9aany; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.placedfile
    ADD CONSTRAINT fklnrvk1kpkp5kbmge8sjm9aany FOREIGN KEY (ticketedfilekey) REFERENCES public.ticketedfile(ticketedfilekey);


--
-- Name: confirmation fknd5410dfqq62r7w1fdbism5h8; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.confirmation
    ADD CONSTRAINT fknd5410dfqq62r7w1fdbism5h8 FOREIGN KEY (shippedfile_ticketedfilekey) REFERENCES public.shippedfile(ticketedfilekey);


--
-- Name: inboundregistration fkndx4iur7prax0yuynfh3d1ign; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inboundregistration
    ADD CONSTRAINT fkndx4iur7prax0yuynfh3d1ign FOREIGN KEY (knownneighbor_neighborkey) REFERENCES public.neighbor(neighborkey);


--
-- Name: inboundregistration fksf5fsv6kk089eel2208tok3ny; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.inboundregistration
    ADD CONSTRAINT fksf5fsv6kk089eel2208tok3ny FOREIGN KEY (registrationkey) REFERENCES public.knownregistration(registrationkey);


--
-- Name: poiattribute fkt89qj0m0jxapdi0mypkw02xgl; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.poiattribute
    ADD CONSTRAINT fkt89qj0m0jxapdi0mypkw02xgl FOREIGN KEY (name_poiattributenamekey) REFERENCES public.poiattributename(poiattributenamekey);


--
-- Name: ticketedfile fkt8tl27xkg7v18toaximry5vc9; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.ticketedfile
    ADD CONSTRAINT fkt8tl27xkg7v18toaximry5vc9 FOREIGN KEY (localregistration_registrationkey) REFERENCES public.knownregistration(registrationkey);


--
-- PostgreSQL database dump complete
--

