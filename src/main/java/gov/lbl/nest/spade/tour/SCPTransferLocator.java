package gov.lbl.nest.spade.tour;

import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.SuffixSubstitution;

/**
 * This implements the {@link DataLocator} interface to generate the data file
 * by substituting the ".scp" semaphore suffix with the ".data" data one.
 *
 * @author patton
 */
public class SCPTransferLocator extends
                                SuffixSubstitution {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * Constructs an instance of this class.
     */
    public SCPTransferLocator() {
        super(".scp",
              ".data");
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
