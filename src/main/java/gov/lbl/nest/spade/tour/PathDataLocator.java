package gov.lbl.nest.spade.tour;

import java.io.File;
import java.io.IOException;
import java.util.List;

import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import gov.lbl.nest.spade.metadata.impl.PathMetadata;
import gov.lbl.nest.spade.metadata.impl.PathMetadataImpl;
import gov.lbl.nest.spade.metadata.impl.PathMetadataManager;
import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.SuffixSubstitution;

/**
 * This class implements the {@link DataLocator} interface to generate the data
 * file by reading the path from a {@link PathMetadataImpl} instance. The
 * semaphore file name will be the name of the bundle.
 *
 * @author patton
 */
public class PathDataLocator extends
                             SuffixSubstitution {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link PathMetadataManager} used to read the semaphore file.
     */
    private PathMetadataManager metadataManager = new PathMetadataManager();

    // constructors

    /**
     * Constructs an instance of this class.
     */
    public PathDataLocator() {
        super(".ndd",
              null);
    }

    // instance member method (alphabetic)

    @Override
    public List<ExternalFile> locateData(final ExternalFile externalSemaphore,
                                         final File internalSemaphore,
                                         final String identity) {
        PathMetadata metadata;
        try {
            metadata = (PathMetadata) metadataManager.createMetadata(internalSemaphore);
        } catch (MetadataParseException
                 | IOException e) {
            e.printStackTrace();
            return null;
        }
        return metadata.getPaths();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
