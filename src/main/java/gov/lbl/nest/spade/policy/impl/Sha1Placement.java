package gov.lbl.nest.spade.policy.impl;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.policy.PlacingPolicy;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedException;

/**
 * This class implements the {@link PlacingPolicy} interface using the SHA1
 * digest of the metadata filename. The first two digits of the digest are used
 * as the main directory, with the remains of the digest used as a
 * sub-directory. The purpose of this is to (hopefully) have a smooth
 * distribution of directories.
 *
 * @author patton
 */
public class Sha1Placement implements
                           PlacingPolicy {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    private static String byteToHex(final byte[] bytes) {
        final StringBuffer sb = new StringBuffer();
        for (byte b : bytes) {
            sb.append(String.format("%02X",
                                    b));
        }
        return sb.toString();
    }

    /**
     * Returns the directory indicated by the {@link Metadata1Impl} instance.
     *
     * @param metadataFile
     *            the cached metadata file
     *
     * @return the directory indicated by the {@link Metadata1Impl} instance.
     */
    private static File path(final File metadataFile) throws PolicyFailedException {
        final String name = metadataFile.getName();
        try {
            final MessageDigest messageDigest = MessageDigest.getInstance("SHA");
            messageDigest.update(name.getBytes());
            final byte[] digest = messageDigest.digest();

            return new File(byteToHex(Arrays.copyOfRange(digest,
                                                         0,
                                                         1)),
                            byteToHex(Arrays.copyOfRange(digest,
                                                         1,
                                                         digest.length)));

        } catch (NoSuchAlgorithmException e) {
            throw new PolicyFailedException("Should never have reach here!",
                                            e);
        }
    }

    @Override
    public File getArchivePlacement(String fileName,
                                    File metadataFile) throws PolicyFailedException {
        return new File(path(metadataFile),
                        fileName);
    }

    @Override
    public File getCompressedPlacement(final String fileName,
                                       final File metadataFile) {
        return null;
    }

    @Override
    public File getDataPlacement(final File deliveryDirectory,
                                 final String bundle,
                                 final File metadataFile) throws PolicyFailedException {
        return new File(path(metadataFile),
                        bundle);
    }

    @Override
    public File getDataPlacement(String fileName,
                                 File metadataFile) throws PolicyFailedException {
        return new File(path(metadataFile),
                        fileName);
    }

    @Override
    public File getMetadataPlacement(String fileName,
                                     final File metadataFile) throws PolicyFailedException {
        return new File(path(metadataFile),
                        fileName);
    }

    // static member methods (alphabetic)

    @Override
    public File getWrappedPlacement(final String fileName,
                                    final File metadataFile) {
        return null;
    }

    @Override
    public void setEnvironment(final File configurationDir,
                               final MetadataManager manager) {
        // Does not use these objects.
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
