package gov.lbl.nest.spade.policy.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import gov.lbl.nest.spade.interfaces.policy.PlacingPolicy;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedException;
import gov.lbl.nest.spade.metadata.impl.UtcMetadata;

/**
 * This class implements the {@link PlacingPolicy} interface so that files are
 * placed in a hierarchy base on the date that they were flagged.
 *
 * @author patton
 */
public class UtcPlacement implements
                          PlacingPolicy {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The UTC {@link TimeZone}, used to build log file hierarchy.
     */
    private static final TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("Etc/UTC");

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * The {@link MetadataManager} to be used by this object.
     */
    private MetadataManager metadataManager;

    // constructors

    // instance member method (alphabetic)

    @Override
    public File getArchivePlacement(String fileName,
                                    File metadataFile) {
        throw new UnsupportedOperationException();
    }

    @Override
    public File getCompressedPlacement(final String fileName,
                                       final File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    @Override
    public File getDataPlacement(final File deliveredDirectory,
                                 String bundle,
                                 final File metadataFile) throws PolicyFailedException {
        throw new PolicyFailedException("Policy only supports single data files");
    }

    @Override
    public File getDataPlacement(final String fileName,
                                 final File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    @Override
    public File getMetadataPlacement(String fileName,
                                     File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    @Override
    public File getWrappedPlacement(String fileName,
                                    File metadataFile) {
        return new File(path(metadataFile),
                        fileName);
    }

    /**
     * Returns the directory indicated by the {@link Metadata1Impl} instance.
     *
     * @param metadataFile
     *            the cached metadata file
     *
     * @return the directory indicated by the {@link Metadata1Impl} instance.
     */
    private File path(final File metadataFile) {
        UtcMetadata utcPlacement;
        try {
            final Metadata metadata = metadataManager.createMetadata(metadataFile);
            utcPlacement = (UtcMetadata) metadata;
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Metadata file can not be found");
        } catch (MetadataParseException e) {
            throw new IllegalArgumentException("Can not parse metadata file",
                                               e);
        } catch (IOException e) {
            throw new IllegalArgumentException("Metadata file can not be read");
        }
        final Calendar calendar = new GregorianCalendar(UTC_TIMEZONE);
        calendar.setTime(utcPlacement.getUtcDateTime());

        final File year = new File(String.format("%04d",
                                                 Integer.valueOf(calendar.get(Calendar.YEAR))));
        final File month = new File(year,
                                    String.format("%02d",
                                                  Integer.valueOf(calendar.get(Calendar.MONTH) + 1)));
        final File day = new File(month,
                                  String.format("%02d",
                                                Integer.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
        return day;
    }

    @Override
    public void setEnvironment(final File configurationDir,
                               final MetadataManager manager) {
        metadataManager = manager;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
