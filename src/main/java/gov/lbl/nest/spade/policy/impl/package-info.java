/**
 * The package contains the built-in implementations of the SPADE policy
 * interfaces.
 *
 * @author patton
 */
package gov.lbl.nest.spade.policy.impl;