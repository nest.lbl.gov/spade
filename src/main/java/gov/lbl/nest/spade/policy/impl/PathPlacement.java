package gov.lbl.nest.spade.policy.impl;

import java.io.File;
import java.io.IOException;
import java.util.List;

import gov.lbl.nest.common.configure.MountPointsMapping;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import gov.lbl.nest.spade.interfaces.policy.PlacingPolicy;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedException;
import gov.lbl.nest.spade.metadata.impl.MetadataImpl;
import gov.lbl.nest.spade.metadata.impl.PathMetadata;
import gov.lbl.nest.spade.metadata.impl.PlacementMismatchException;
import gov.lbl.nest.spade.registry.ExternalFile;

/**
 * This class implements the {@link PlacingPolicy} interface using the original
 * path name as carried in the metadata.
 *
 * @author patton
 */
public class PathPlacement implements
                           PlacingPolicy {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The name of the default properties file contains the mount points mapping.
     */
    private static final String MOUNT_POINT_PROPERTIES = "mount-point.properties";

    /**
     * The name of the resource contains the path to the properties file contains
     * the mount points mapping.
     */
    private static final String MOUNT_POINT_RESOURCE = "path_placement.mounts";

    // private static member data

    // private instance member data

    /**
     * The {@link MetadataManager} used by this object.
     */
    private MetadataManager metadataManager;

    /**
     * The {@link MountPointsMapping} instance that maps the mount points of the
     * original files to sub-directories in the warehouse.
     */
    private MountPointsMapping mountPointsMapping;

    /**
     * The {@link Sha1Placement} instance to use when the {@link Metadata} instance
     * is a {@link MetadataImpl} object.
     */
    private Sha1Placement sha1Placement = new Sha1Placement();

    // constructors

    // instance member method (alphabetic)

    @Override
    public File getArchivePlacement(String fileName,
                                    File metadataFile) {
        throw new UnsupportedOperationException();
    }

    @Override
    public File getCompressedPlacement(final String fileName,
                                       final File metadataFile) {
        return null;
    }

    @Override
    public File getDataPlacement(final File deliveredDirectory,
                                 String bundle,
                                 final File metadataFile) throws PolicyFailedException {
        throw new PolicyFailedException("Policy only supports single data files");
    }

    @Override
    public File getDataPlacement(final String fileName,
                                 final File metadataFile) throws PolicyFailedException {
        try {
            return new File(path(metadataFile),
                            fileName);
        } catch (PlacementMismatchException e) {
            return sha1Placement.getDataPlacement(fileName,
                                                  metadataFile);
        }
    }

    @Override
    public File getMetadataPlacement(String fileName,
                                     final File metadataFile) throws PolicyFailedException {
        try {
            return new File(path(metadataFile),
                            fileName);
        } catch (PlacementMismatchException e) {
            return sha1Placement.getMetadataPlacement(fileName,
                                                      metadataFile);
        }
    }

    /**
     * Returns the directory within the warehouse as indicated by original remote
     * directory of the data file.
     *
     * @param originalDir
     *            the original remote directory of the data file.
     *
     * @return the directory within the warehouse as indicated by original remote
     *         directory of the data file.
     *
     * @throws IOException
     *             if the mount points information can not be loaded.
     */
    public File getWarehouseSubdir(final String originalDir) throws IOException {
        return mountPointsMapping.getMappedFile(originalDir);
    }

    @Override
    public File getWrappedPlacement(String fileName,
                                    File metadataFile) {
        return null;
    }

    /**
     * Returns the directory indicated by the {@link Metadata1Impl} instance.
     *
     * @param metadataFile
     *            the cached metadata file
     *
     * @return the directory indicated by the {@link Metadata1Impl} instance.
     *
     * @throws PlacementMismatchException
     *             when the {@link Metadata} instance can not be placed by this
     *             method.
     */
    private File path(final File metadataFile) throws PolicyFailedException,
                                               PlacementMismatchException {
        try {
            Metadata metadata = metadataManager.createMetadata(metadataFile);
            if (!(metadata instanceof PathMetadata)) {
                throw new PlacementMismatchException();
            }
            final PathMetadata pathMetadata = (PathMetadata) metadata;
            final List<ExternalFile> paths = pathMetadata.getPaths();
            if (null == paths || paths.isEmpty()) {
                throw new PolicyFailedException("The metadata file \"" + metadataFile.getName()
                                                + "\" contains no paths");
            }
            if (paths.size() > 1) {
                throw new PolicyFailedException("Policy only supports single data files");
            }
            final ExternalFile originalFile = (paths.iterator()).next();
            return getWarehouseSubdir(originalFile.getDirectory());
        } catch (MetadataParseException
                 | IOException e) {
            throw new PolicyFailedException(e);
        }
    }

    @Override
    public void setEnvironment(final File configurationDir,
                               final MetadataManager manager) {
        sha1Placement.setEnvironment(configurationDir,
                                     manager);
        metadataManager = manager;
        mountPointsMapping = new MountPointsMapping(Configuration.class,
                                                    MOUNT_POINT_RESOURCE,
                                                    new File(configurationDir,
                                                             MOUNT_POINT_PROPERTIES));
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
