package gov.lbl.nest.spade.policy.impl;

import java.io.File;
import java.io.IOException;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.interfaces.policy.PackingPolicy;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedException;

/**
 * This class implements the {@link PackingPolicy} instance for the UNIX tar
 * command.
 *
 * @author patton
 */
public class TarPacking implements
                        PackingPolicy {

    // public static final member data

    /**
     * The suffix for compressed files.
     */
    public static final String WRAPPED_SUFFIX = ".tar";

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public String getSuffix() {
        return WRAPPED_SUFFIX;
    }

    @Override
    public void pack(File dataFile,
                     File metadataFile,
                     boolean isSingle,
                     File packedFile) throws PolicyFailedException,
                                      IOException,
                                      InterruptedException {
        final File dataFileToUse;
        if (isSingle) {
            final File[] files = dataFile.listFiles();
            dataFileToUse = files[0];
        } else {
            dataFileToUse = dataFile;
        }
        final File[] files = new File[] { dataFileToUse,
                                          metadataFile };
        final String[] leader = new String[] { "tar",
                                               "cf",
                                               packedFile.getAbsolutePath() };
        final String[] cmdArray = new String[leader.length + (3 * files.length)];
        System.arraycopy(leader,
                         0,
                         cmdArray,
                         0,
                         leader.length);
        int index = leader.length - 1;
        for (File file : files) {
            cmdArray[++index] = "-C";
            final File directory = file.getParentFile();
            cmdArray[++index] = directory.getAbsolutePath();
            cmdArray[++index] = file.getName();
        }
        final Command command = new Command(cmdArray);
        command.execute();
        final ExecutionFailedException cmdException = command.getCmdException();
        if (null != cmdException) {
            throw new PolicyFailedException(cmdException);
        }
    }

    @Override
    public void unpack(final File packedFile,
                       final File directory) throws PolicyFailedException,
                                             IOException,
                                             InterruptedException {
        final String[] cmdArray = new String[] { "tar",
                                                 "xf",
                                                 packedFile.getAbsolutePath(),
                                                 "-C",
                                                 directory.getAbsolutePath() };
        final Command command = new Command(cmdArray);
        command.execute();
        final ExecutionFailedException cmdException = command.getCmdException();
        if (null != cmdException) {
            throw new PolicyFailedException(cmdException);
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
