package gov.lbl.nest.spade.policy.impl;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileVisitOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.interfaces.policy.FetchingPolicy;
import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.services.impl.LocalhostTransfer;
import jakarta.resource.spi.IllegalStateException;

/**
 * This class implements the {@link FetchingPolicy} interface to fetch files
 * from the localhost.
 *
 * @author patton
 */
public class LocalhostFetching implements
                               FetchingPolicy {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(LocalhostFetching.class);

    /**
     * The {@link Function} instance to convert a {@link Path} instance into a
     * {@link String} instance.
     */
    private final static Function<Path, String> PATH_TO_STRING = p -> p.toString();

    /**
     * Value to use when executing a Files.find call.
     */
    private static final int MAX_DEPTH = 127;

    /**
     * The {@link BiPredicate} to use to select directories.
     */
    private static final BiPredicate<Path, BasicFileAttributes> SELECT_DIRECTORIES = new BiPredicate<>() {

        @Override
        public boolean test(Path t,
                            BasicFileAttributes u) {
            return u.isDirectory();
        }
    };

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Locates the single file in the specified directory that matches the supplied
     * regular expression.
     *
     * @param directory
     *            the directory to eb searched.
     * @param regex
     *            the regular expression to be matched.
     *
     * @return the name of the file that matched the regular expression.
     *
     * @throws IllegalStateException
     */
    private static String locateFile(final File directory,
                                     final String regex) throws IllegalStateException {
        final Pattern pattern = Pattern.compile(regex);
        final String[] matches = directory.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir,
                                  String name) {
                return (pattern.matcher(name)).matches();
            }
        });
        if (0 == matches.length) {
            throw new IllegalStateException("There is no match to the requested search \"" + regex
                                            + "\"");
        } else if (1 != matches.length) {
            throw new IllegalStateException("There are " + matches.length
                                            + " matches to the requested search \""
                                            + regex
                                            + "\"");
        }
        return matches[0];
    }

    @Override
    public boolean delete(final Collection<ExternalFile> externalFiles) {
        boolean result = true;
        for (ExternalFile externalFile : externalFiles) {
            final boolean removed = delete(externalFile);
            if (result && !removed) {
                result = false;
            }
        }
        return result;
    }

    @Override
    public boolean delete(final ExternalFile external) {
        final ExternalLocation location = external.getLocation();
        final String host = location.getHost();
        if (null == host || LocalhostTransfer.LOCALHOST.equals(host)) {
            final File from = new File(location.getResolvedDirectory(),
                                       external.getName());
            return LocalhostTransfer.delete(from);
        }
        throw new UnsupportedOperationException();
    }

    @Override
    public long getLastModified(final ExternalFile externalFile) {
        final ExternalLocation location = externalFile.getLocation();
        final String host = location.getHost();
        if (null != host && !LocalhostTransfer.LOCALHOST.equals(host)) {
            throw new UnsupportedOperationException();
        }
        final File file = new File(location.getResolvedDirectory(),
                                   externalFile.getName());
        return file.lastModified();
    }

    @Override
    public List<String> getSubdirs(ExternalLocation location) {
        final String host = location.getHost();
        if (null != host && !LocalhostTransfer.LOCALHOST.equals(host)) {
            throw new UnsupportedOperationException();
        }
        final File directory = new File(location.getResolvedDirectory());
        if (!directory.exists()) {
            LOG.error("Directory \"" + directory.toPath()
                      + "\" does not exist and will be ignored");
            return null;
        }
        try (Stream<Path> directories = Files.find(directory.toPath(),
                                                   MAX_DEPTH,
                                                   SELECT_DIRECTORIES,
                                                   FileVisitOption.FOLLOW_LINKS)) {
            final Stream<String> names = directories.map(PATH_TO_STRING);
            return names.collect(Collectors.toList());
        } catch (IOException e) {
            LOG.warn("Failed to complete listing of sub-directories.");
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<String> list(ExternalLocation location,
                             FilenameFilter filter) {
        final String host = location.getHost();
        if (null != host && !LocalhostTransfer.LOCALHOST.equals(host)) {
            throw new UnsupportedOperationException("Only localhost is supported, not \"" + host
                                                    + "\"");
        }
        final List<String> result = new ArrayList<>();
        final File directory = new File(location.getResolvedDirectory());
        if (!directory.exists()) {
            LOG.error("Directory \"" + directory.toPath()
                      + "\" does not exist and will be ignored");
            return result;
        }
        return Arrays.asList(directory.list(filter));
    }

    @Override
    public ExternalFile receive(final ExternalFile external,
                                final File internal) throws IOException,
                                                     InterruptedException {
        return receive(external,
                       internal,
                       true,
                       true);
    }

    @Override
    public ExternalFile receive(final ExternalFile external,
                                final File internal,
                                final boolean softlink) throws IOException,
                                                        InterruptedException {
        return receive(external,
                       internal,
                       softlink,
                       true);
    }

    // static member methods (alphabetic)

    @Override
    public ExternalFile receive(final ExternalFile external,
                                final File internal,
                                final boolean softlink,
                                final boolean hardlink) throws IOException,
                                                        InterruptedException {
        final ExternalLocation location = external.getLocation();
        final String host = location.getHost();
        if (null != host && !LocalhostTransfer.LOCALHOST.equals(host)) {
            throw new UnsupportedOperationException();
        }
        final String name;
        final ExternalFile result;
        if (null == external.getReplacement()) {
            name = external.getName();
            result = new ExternalFile(location,
                                      name,
                                      new File(internal.getParentFile(),
                                               (external.getDeliveryLocation()).getName()));
        } else {
            final String regex = external.getName();
            try {
                name = locateFile(new File(location.getResolvedDirectory()),
                                  regex);
            } catch (IllegalStateException e) {
                throw new IOException(e);
            }
            final Pattern pattern = Pattern.compile(regex);
            final Matcher matcher = pattern.matcher(name);
            String foundName = matcher.replaceAll(external.getReplacement());
            result = new ExternalFile(location,
                                      name,
                                      new File(internal.getParentFile(),
                                               foundName));
        }
        final File from = new File(location.getResolvedDirectory(),
                                   name);
        if (softlink) {
            LocalhostTransfer.softlink(from,
                                       internal);
        } else if (hardlink) {
            LocalhostTransfer.copy(from,
                                   internal);
        } else {
            LocalhostTransfer.copy(from,
                                   internal,
                                   false);
        }
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
