package gov.lbl.nest.spade.policy.impl;

import java.io.File;
import java.io.IOException;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.interfaces.policy.CompressingPolicy;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedException;

/**
 * The class implements the {@link CompressingPolicy} interface using the 'gzip'
 * algorithm.
 *
 * @author patton
 */
public class GzipCompression implements
                             CompressingPolicy {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The suffix used to label Gzipped files.
     */
    public static final String SUFFIX = ".gz";

    /**
     * The suffix used to label Gzipped files.
     */
    public static final String TAR_SUFFIX = ".tar";

    /**
     * The suffix used to label Gzipped files.
     */
    public static final String TGZ_SUFFIX = ".tgz";

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public void compress(final File packedFile,
                         final File compressedFile) throws ExecutionFailedException,
                                                    InterruptedException,
                                                    IOException {
        final String[] cmdArray = new String[] { "gzip",
                                                 "-c",
                                                 packedFile.getAbsolutePath() };
        final Command command = new Command(cmdArray);
        command.execute(compressedFile);
        final ExecutionFailedException cmdException = command.getCmdException();
        if (null == cmdException) {
            return;
        }
        throw cmdException;
    }

    @Override
    public void expand(final File compressedFile,
                       final File packedFile) throws PolicyFailedException,
                                              InterruptedException,
                                              IOException {
        final String[] cmdArray = new String[] { "gunzip",
                                                 "-c",
                                                 compressedFile.getAbsolutePath() };
        final Command command = new Command(cmdArray);
        command.execute(packedFile);
        final ExecutionFailedException cmdException = command.getCmdException();
        if (null != cmdException) {
            throw new PolicyFailedException(cmdException);
        }
    }

    @Override
    public String getCompressedSuffixFromPackedFile(String fileName) {
        if (fileName.endsWith(TAR_SUFFIX)) {
            return TGZ_SUFFIX;
        }
        final int cut = fileName.lastIndexOf(".");
        return fileName.substring(cut) + SUFFIX;
    }

    @Override
    public String getPackedSuffixFromCompressedFile(String fileName) {
        if (fileName.endsWith(SUFFIX)) {
            final String noSuffix = fileName.substring(0,
                                                       fileName.length() - SUFFIX.length());
            final int cut = noSuffix.lastIndexOf(".");
            return noSuffix.substring(0,
                                      cut);
        } else if (fileName.endsWith(TGZ_SUFFIX)) {
            return TAR_SUFFIX;
        }
        return SUFFIX;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
