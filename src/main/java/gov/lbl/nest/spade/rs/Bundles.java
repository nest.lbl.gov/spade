package gov.lbl.nest.spade.rs;

import java.io.StringWriter;
import java.net.URI;
import java.util.Collection;
import java.util.Date;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, a group of
 * zero, one or more {@link Bundle} instances.
 *
 * @author patton
 */
@XmlRootElement(name = "bundles")
@XmlType(propOrder = { "uri",
                       "neighbor",
                       "dateTime",
                       "bundles" })
public class Bundles {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection of the {@link Bundle} instances constitute this object.
     */
    private Collection<Bundle> bundles;

    /**
     * The date and time that applies to this collection of {@link Bundle}
     * instances.
     */
    private Date dateTime;

    /**
     * The identity of the neighbor, if any, who delivered the content of this
     * object.
     */
    private String neighbor;

    /**
     * The URI, if any, that is related to the content of this object.
     */
    private URI uri;

    // constructors

    // instance member method (alphabetic)

    /**
     * Create an instance of this class.
     */
    protected Bundles() {
    }

    /**
     * Creates an instance of this class.
     *
     * The new instance will contain the same {@link Collection} as the original (so
     * a change in one will be a change in the other), but the new instance has a
     * different URL.
     *
     * @param url
     *            the URI, if any, that is related to the content of this object.
     * @param rhs
     *            the {@link Bundles} instance whose collection will be shared.
     */
    public Bundles(URI url,
                   Bundles rhs) {
        this(url,
             rhs.getBundles(),
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param url
     *            the URI, if any, that is related to the content of this object.
     * @param bundles
     *            the collection of the {@link Bundle} instances constitute this
     *            object.
     */
    public Bundles(URI url,
                   Collection<Bundle> bundles) {
        this(url,
             bundles,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param url
     *            the URI, if any, that is related to the content of this object.
     * @param bundles
     *            the collection of the {@link Bundle} instances constitute this
     *            object.
     * @param dateTime
     *            the date and time that applies to this collection of
     *            {@link Bundle} instances.
     */
    public Bundles(URI url,
                   Collection<Bundle> bundles,
                   Date dateTime) {
        setBundles(bundles);
        setDateTime(dateTime);
        setUri(url);
    }

    /**
     * Returns the collection of the {@link Bundle} instances constitute this
     * object.
     *
     * @return the collection of the {@link Bundle} instances constitute this
     *         object.
     */
    @XmlElement(name = "bundle")
    public Collection<Bundle> getBundles() {
        if (null != neighbor) {
            for (Bundle bundle : bundles) {
                bundle.setNeighbor(neighbor);
            }
        }
        return bundles;
    }

    /**
     * Returns the date and time that applies to this collection of {@link Bundle}
     * instances.
     *
     * @return the date and time that applies to this collection of {@link Bundle}
     *         instances.
     */
    @XmlElement(name = "date_time")
    public Date getDateTime() {
        return dateTime;
    }

    /**
     * Returns the identity of the neighbor, if any, who delivered the content of
     * this object.
     *
     * @return the identity of the neighbor, if any, who delivered the content of
     *         this object.
     */
    @XmlElement
    public String getNeighbor() {
        return neighbor;
    }

    /**
     * Returns the URI, if any, that is related to the content of this object.
     *
     * @return the URI, if any, that is related to the content of this object.
     */
    @XmlElement
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the collection of the {@link Bundle} instances constitute this object.
     *
     * @param bundles
     *            the collection of the {@link Bundle} instances constitute this
     *            object.
     */
    protected void setBundles(Collection<Bundle> bundles) {
        this.bundles = bundles;
    }

    /**
     * Sets the date and time that applies to this collection of {@link Bundle}
     * instances.
     *
     * @param dateTime
     *            the date and time that applies to this collection of
     *            {@link Bundle} instances.
     */
    protected void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    /**
     * Sets the identity of the neighbor, if any, who delivered the content of this
     * object.
     *
     * @param neighbor
     *            the identity of the neighbor, if any, who delivered the content of
     *            this object.
     */
    protected void setNeighbor(String neighbor) {
        this.neighbor = neighbor;
    }

    /**
     * Sets the URI, if any, that is related to the content of this object.
     *
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Returns the number of {@link Bundle} instances that constitute this object.
     *
     * @return the number of {@link Bundle} instances that constitute this object.
     */
    public int size() {
        if (null == bundles) {
            return 0;
        }
        return bundles.size();
    }

    // static member methods (alphabetic)

    // Description of this object.
    @Override
    public String toString() {
        JAXBContext content;
        try {
            content = JAXBContext.newInstance(Bundles.class);
            final Marshaller marshaller = content.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                                   true);
            final StringWriter writer = new StringWriter();
            marshaller.marshal(this,
                               writer);
            return writer.toString();
        } catch (JAXBException e) {
            return super.toString();
        }
    }

    // public static void main(String args[]) {}
}
