package gov.lbl.nest.spade.rs;

import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.rs.NestType;
import gov.lbl.nest.common.suspension.SuspendedException;
import gov.lbl.nest.common.watching.DateUtilities;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.common.watching.Digests;
import gov.lbl.nest.spade.ImplementationString;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.interfaces.storage.Location;
import gov.lbl.nest.spade.interfaces.storage.Locations;
import gov.lbl.nest.spade.interfaces.storage.WarehouseManager;
import gov.lbl.nest.spade.registry.LocalRegistration;
import gov.lbl.nest.spade.registry.Registry;
import gov.lbl.nest.spade.registry.internal.InboundRegistration;
import gov.lbl.nest.spade.rs.ExecutionStatus.Condition;
import gov.lbl.nest.spade.services.Spade;
import gov.lbl.nest.spade.workflow.ActivityMonitor;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowsMonitor;
import gov.lbl.nest.spade.workflow.WorkflowsMonitor.AggregateExecution;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.Response.Status.Family;
import jakarta.ws.rs.core.Response.StatusType;
import jakarta.ws.rs.core.UriInfo;

/**
 * The class provides the read-only RESTful interface to access SPADE
 * application status.
 *
 * @author patton
 *
 * @see CommandSpade
 */
@Path("report")
@Stateless
public class ReportSpade {

    /**
     * This interface is used to retrieve a set of time stamps associated with an
     * action.
     *
     * @author patton
     */
    private interface TimeStamper {
        String getMessage();

        List<DigestChange> getTimeStamps(Date after,
                                         Date before,
                                         boolean reversed,
                                         int max,
                                         String neighbor,
                                         List<String> registrations,
                                         Boolean inbound);
    }

    private static final String TRANSFERS_FLOW = "transfers";

    private static final String PLACEMENTS_FLOW = "placements";

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link StatusType} to use when a URI is currently not implemented.
     */
    public static final StatusType NOT_IMPLEMENTED = new Response.StatusType() {

        @Override
        public Family getFamily() {
            return Family.SERVER_ERROR;
        }

        @Override
        public String getReasonPhrase() {
            return "The server does not support the functionality required to fulfill the request.";
        }

        @Override
        public int getStatusCode() {
            return 501;
        }
    };

    // private static member data

    /**
     * Creates the XML representation of the status of the named activity.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param spade
     *            The {@link Spade} instance in which the activity is executing.
     * @param workflow
     *            the workflow that contains the named activity.
     * @param activity
     *            the name of the activity whose status should be returned.
     * @param detailed
     *            <code>true</code> is the status should be returned, otherwise a
     *            summary will be returned.
     *
     * @return the XML representation of the status of the named activity.
     *
     * @throws InitializingException
     *             when the application is still initializing
     */
    private static ActivityStatus createActivityStatus(final UriInfo uriInfo,
                                                       final Spade spade,
                                                       final Workflow workflow,
                                                       final String activity,
                                                       final boolean detailed) throws InitializingException {
        final URI baseUri = uriInfo.getBaseUri();
        final URI reportUri = baseUri.resolve("report/");

        final ActivityMonitor monitor = spade.getActivityMonitor(workflow,
                                                                 activity);
        if (null == monitor) {
            return null;
        }
        final ExecutionStatus.Condition execution;
        if (monitor.isSuspended()) {
            execution = Condition.SUSPENDED;
        } else {
            execution = Condition.RUNNING;
        }
        if (detailed) {
            return new ActivityStatus(reportUri.resolve("activity/" + percentEncode(activity)
                                                        + "/status"),
                                      monitor.getName(),
                                      execution,
                                      monitor.getMaximumThreadCount(),
                                      monitor.getActivityTaskStatuses());
        }
        return new ActivityStatus(reportUri.resolve("activity/" + percentEncode(activity)
                                                    + "/status"),
                                  monitor.getName(),
                                  execution,
                                  new StatusCounts(monitor.getExecutingCount(),
                                                   monitor.getPendingCount(),
                                                   monitor.getMaximumThreadCount()));
    }

    // private instance member data

    static Response createActivityStatusResponse(UriInfo uriInfo,
                                                 Spade spade,
                                                 Workflow workflow,
                                                 String activity) {
        ResponseBuilder builder;
        try {
            final ActivityStatus status = createActivityStatus(uriInfo,
                                                               spade,
                                                               workflow,
                                                               activity,
                                                               true);
            if (null == status) {
                builder = Response.status(Response.Status.NOT_FOUND);
            } else {
                builder = Response.status(Response.Status.OK);
            }
            builder = builder.entity(status);
        } catch (InitializingException e) {
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        }
        return builder.build();
    }

    /**
     * Creates a new {@link ApplicationStatus} instance to reflect the current state
     * of the application.
     *
     * @param uriInfo
     * @param spade
     *            The {@link Spade} instance whose {@link ApplicationStatus}
     *            instance should be created.
     * @param summary
     *            true if only a summary status should be created.
     *
     * @return the new {@link ApplicationStatus} instance to reflect the current
     *         state of the application.
     *
     * @throws InitializingException
     *             TODO
     */
    static ApplicationStatus createApplicationStatus(UriInfo uriInfo,
                                                     Spade spade,
                                                     boolean summary) throws InitializingException {
        final URI baseUri = uriInfo.getBaseUri();
        final URI reportUri = baseUri.resolve("report/");

        if (summary) {
            return new ApplicationStatus(reportUri.resolve("application/status/"),
                                         spade.getName(),
                                         getApplicationExecution(spade));
        }
        final List<ActivityStatus> activities = new ArrayList<>();
        final List<? extends String> names = spade.getActivityNames(null);
        for (String name : names) {
            final ActivityStatus activity = createActivityStatus(uriInfo,
                                                                 spade,
                                                                 null,
                                                                 name,
                                                                 false);
            activities.add(activity);
        }
        final WorkflowsMonitor workflowsMonitor = spade.getWorkflowsMonitor();
        final ApplicationStatus status = new ApplicationStatus(reportUri.resolve("application/status"),
                                                               spade.getName(),
                                                               getApplicationExecution(spade),
                                                               activities,
                                                               workflowsMonitor.getThreadLimit(),
                                                               workflowsMonitor.getExecutingCount(),
                                                               workflowsMonitor.getPendingCount());
        return status;
    }

    static Response createApplicationStatusReponse(UriInfo uriInfo,
                                                   Spade spade,
                                                   boolean summary) {
        ResponseBuilder builder;
        try {
            final ApplicationStatus status = createApplicationStatus(uriInfo,
                                                                     spade,
                                                                     summary);
            builder = Response.status(Response.Status.OK);
            builder = builder.entity(status);
        } catch (InitializingException e) {
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        }
        return builder.build();
    }

    // constructors

    /**
     * Creates a new {@link BundleStatus} instance to reflect the current state of a
     * bundle.
     *
     * @param uriInfo
     * @param bundle
     *            The name of the bundle whose status is to be created.
     * @param spade
     *            The {@link Spade} instance from which the whose
     *            {@link BundleStatus} instance should be created.
     * @param summary
     *            true if only a summary status should be created.
     *
     * @return the new {@link BundleStatus} instance to reflect the current state of
     *         the bundle.
     *
     * @throws InitializingException
     *             TODO
     */
    static BundleStatus createBundleStatus(final UriInfo uriInfo,
                                           final String bundle,
                                           final Spade spade,
                                           final boolean summary) throws InitializingException {
        final URI baseUri = uriInfo.getBaseUri();
        final URI reportUri = baseUri.resolve("report/");

        final List<String> tickets = spade.getTicketsByBundle(bundle);
        if (null == tickets || tickets.isEmpty()) {
            return null;
        }

        if (summary) {
            return new BundleStatus(reportUri.resolve("application/" + bundle
                                                      + "status/"),
                                    bundle,
                                    createTicketHistory(uriInfo,
                                                        tickets.get(0),
                                                        spade));
        }
        final List<Digest> histories = createTicketHistories(uriInfo,
                                                             tickets,
                                                             spade);
        final BundleStatus status = new BundleStatus(reportUri.resolve("application/" + bundle
                                                                       + "status/"),
                                                     bundle,
                                                     histories);
        return status;
    }

    static Response createBundleStatusReponse(final UriInfo uriInfo,
                                              final String bundle,
                                              final Spade spade,
                                              boolean summary) {
        ResponseBuilder builder;
        try {
            final BundleStatus status = createBundleStatus(uriInfo,
                                                           bundle,
                                                           spade,
                                                           summary);
            builder = Response.status(Response.Status.OK);
            builder = builder.entity(status);
        } catch (InitializingException e) {
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        }
        return builder.build();
    }

    // instance member method (alphabetic)

    static List<Digest> createTicketHistories(UriInfo uriInfo,
                                              Collection<String> identities,
                                              Spade spade) {
        if (null == identities || identities.isEmpty()) {
            return null;
        }
        List<Digest> collection = new ArrayList<>();
        for (String ticket : identities) {
            final Digest history = createTicketHistory(uriInfo,
                                                       ticket,
                                                       spade);
            if (null != history) {
                collection.add(history);
            }
        }
        return collection;
    }

    /**
     * Creates a new {@link Digest} instance to reflect the history of a ticket.
     *
     * @param uriInfo
     * @param ticket
     *            The name of the ticket whose history is to be created.
     * @param spade
     *            The {@link Spade} instance from which the whose {@link Digest}
     *            instance should be created.
     *
     * @return the new {@link Digest} instance to reflect the history of the ticket.
     *
     * @throws InitializingException
     *             TODO
     */
    static Digest createTicketHistory(UriInfo uriInfo,
                                      String ticket,
                                      Spade spade) {
        return spade.getHistoryByTicket(ticket);
    }

    static Response createTicketHistoryResponse(UriInfo uriInfo,
                                                String ticket,
                                                Spade spade) {
        final Digest history = createTicketHistory(uriInfo,
                                                   ticket,
                                                   spade);
        ResponseBuilder builder;
        if (null == history) {
            builder = Response.status(Response.Status.NOT_FOUND);
        } else {
            history.setUri((uriInfo.getBaseUri()).resolve("report/ticket/" + ticket
                                                          + "/history"));
            builder = Response.status(Response.Status.OK);
            builder = builder.entity(history);
        }
        return builder.build();
    }

    static Response createTicketsHistoriesResponse(UriInfo uriInfo,
                                                   final Collection<String> identities,
                                                   Spade spade) {
        final List<Digest> collection = createTicketHistories(uriInfo,
                                                              identities,
                                                              spade);
        final Digests histories = new Digests("Tickets histories for " + spade.getName(),
                                              (uriInfo.getBaseUri()).resolve("report/history"),
                                              collection);
        ResponseBuilder builder;
        builder = Response.status(Response.Status.OK);
        builder = builder.entity(histories);
        return builder.build();
    }

    /**
     * Returns the {@link Condition} instance appropriate for the current state of
     * the application.
     *
     * @param spade
     *            The {@link Spade} instance whose {@link Condition} instance should
     *            be returned.
     *
     * @return the {@link Condition} instance appropriate for the current stat of
     *         the application.
     *
     * @throws InitializingException
     *             when the application is still initializing
     */
    private static Condition getApplicationExecution(final Spade spade) throws InitializingException {
        final WorkflowsMonitor workflowsMonitor = spade.getWorkflowsMonitor();
        final AggregateExecution aggregateExecution = workflowsMonitor.getAggregateExecution();
        if (AggregateExecution.RUNNING == aggregateExecution) {
            return Condition.RUNNING;
        }
        if (AggregateExecution.PARTIALLY_RUNNING == aggregateExecution) {
            return Condition.PARTIALLY_RUNNING;
        }
        if (AggregateExecution.PARTIALLY_SUSPENDED == aggregateExecution) {
            return Condition.PARTIALLY_SUSPENDED;
        }
        return Condition.SUSPENDED;
    }

    /**
     * Taken from
     * https://stackoverflow.com/questions/724043/http-url-address-encoding-in-java
     *
     * Percent-encodes a string so it's suitable for use in a URL Path (not a query
     * string / form encode, which uses + for spaces, etc)
     *
     * @param encodeMe
     *            the {@link String} to encode
     *
     * @return the encoded {@link String}
     */
    private static String percentEncode(String encodeMe) {
        if (encodeMe == null) {
            return "";
        }
        String encoded = encodeMe.replace("%",
                                          "%25");
        encoded = encoded.replace(" ",
                                  "%20");
        encoded = encoded.replace("!",
                                  "%21");
        encoded = encoded.replace("#",
                                  "%23");
        encoded = encoded.replace("$",
                                  "%24");
        encoded = encoded.replace("&",
                                  "%26");
        encoded = encoded.replace("'",
                                  "%27");
        encoded = encoded.replace("(",
                                  "%28");
        encoded = encoded.replace(")",
                                  "%29");
        encoded = encoded.replace("*",
                                  "%2A");
        encoded = encoded.replace("+",
                                  "%2B");
        encoded = encoded.replace(",",
                                  "%2C");
        encoded = encoded.replace("/",
                                  "%2F");
        encoded = encoded.replace(":",
                                  "%3A");
        encoded = encoded.replace(";",
                                  "%3B");
        encoded = encoded.replace("=",
                                  "%3D");
        encoded = encoded.replace("?",
                                  "%3F");
        encoded = encoded.replace("@",
                                  "%40");
        encoded = encoded.replace("[",
                                  "%5B");
        encoded = encoded.replace("]",
                                  "%5D");
        return encoded;
    }

    /**
     * The objects used to retrieve a set of time stamps associated with an action.
     */
    private final Map<String, TimeStamper> TIME_STAMPERS = new HashMap<>();

    /**
     * The implementation version.
     */
    @Inject
    @ImplementationString
    private String implementation;

    /**
     * The {@link Spade} instance used by this object.
     */
    @Inject
    private Spade spade;

    /**
     * The {@link Warehouse} instance used by this object.
     */
    @Inject
    private WarehouseManager warehouseManager;

    /**
     * Create an instance of this class.
     */
    public ReportSpade() {
        TIME_STAMPERS.put("archived",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Bundles that have been archived";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getArchivedByTime(after,
                                                                 before,
                                                                 reversed,
                                                                 max,
                                                                 neighbor,
                                                                 registrations);
                              }
                          });
        TIME_STAMPERS.put("confirmable",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Bundles ready to be confirmed by one or all shippers";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getConfirmableByTime(after,
                                                                    before,
                                                                    reversed,
                                                                    max,
                                                                    neighbor,
                                                                    registrations);
                              }
                          });
        TIME_STAMPERS.put("confirmed",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Bundles whose delivery to a single destintation has been verified";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getConfirmedByTime(after,
                                                                  before,
                                                                  reversed,
                                                                  max,
                                                                  neighbor,
                                                                  registrations);
                              }
                          });
        TIME_STAMPERS.put("dispatched",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Successful dispatches of Bundles to one or more their destinations";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getDispatchedByTime(after,
                                                                   before,
                                                                   reversed,
                                                                   max,
                                                                   neighbor,
                                                                   registrations);
                              }
                          });
        TIME_STAMPERS.put("placed",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Bundles placed in the local warehouse";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getPlacedByTime(after,
                                                               before,
                                                               reversed,
                                                               max,
                                                               neighbor,
                                                               registrations);
                              }
                          });
        TIME_STAMPERS.put("ticketed",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Bundles ticketed by this instance";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getTicketedByTime(after,
                                                                 before,
                                                                 reversed,
                                                                 max,
                                                                 neighbor,
                                                                 registrations,
                                                                 inbound);
                              }
                          });
        TIME_STAMPERS.put("ticketed_inbound",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Bundles ticketed by this instance";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getTicketedByTime(after,
                                                                 before,
                                                                 reversed,
                                                                 max,
                                                                 neighbor,
                                                                 registrations,
                                                                 null);
                              }
                          });
        TIME_STAMPERS.put("ticketed_outbound",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Bundles ticketed by this instance";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getTicketedByTime(after,
                                                                 before,
                                                                 reversed,
                                                                 max,
                                                                 neighbor,
                                                                 registrations,
                                                                 null);
                              }
                          });
        TIME_STAMPERS.put("unverified",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Bundles whose delivery to one or all destintation is unverified";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getUnverifiedByTime(after,
                                                                   before,
                                                                   reversed,
                                                                   max);
                              }
                          });
        TIME_STAMPERS.put("verified",
                          new TimeStamper() {

                              @Override
                              public String getMessage() {
                                  return "Bundles whose delivery to one or all destintation has been verified";
                              }

                              @Override
                              public List<DigestChange> getTimeStamps(Date after,
                                                                      Date before,
                                                                      boolean reversed,
                                                                      int max,
                                                                      String neighbor,
                                                                      List<String> registrations,
                                                                      Boolean inbound) {
                                  return spade.getVerifiedByTime(after,
                                                                 before,
                                                                 reversed,
                                                                 max,
                                                                 registrations);
                              }
                          });
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param spade
     *            the {@link Spade} instance used by this object.
     * @param warehouseManager
     *            the {@link WarehouseManager} instance used by this object.
     */
    protected ReportSpade(final Spade spade,
                          WarehouseManager warehouseManager) {
        this.spade = spade;
        this.warehouseManager = warehouseManager;
    }

    /**
     * Creates a {@link Flow} instance based on the supplied information,
     *
     * @param reportUri
     *            the base URI to use for the {@link Flow} URI.
     * @param name
     *            the name of the flow to return.
     * @param fineBins
     *            true if fine "epoch" bins should be used, otherwise course will be
     *            used.
     * @param span
     *            the limit, if any, of number of slices to include.
     * @param after
     *            the date and time from which to start the sequence. If
     *            <code>null</code> then this is set to "now" minus the time covered
     *            by the "span".
     * @param neighbor
     *            the neighboring SPADE instance, in any, for which to restrict the
     *            result.
     *
     * @return the {@link Flow} instance based on the supplied information,
     */
    private Flow createFlow(URI reportUri,
                            String name,
                            boolean fineBins,
                            Integer span,
                            Date after,
                            String neighbor) {
        final String subject;
        final List<Slice> slices;
        if (null == neighbor) {
            if (PLACEMENTS_FLOW.equals(name)) {
                subject = "Placements for " + spade.getName();
                slices = spade.getPlacementSlices(fineBins,
                                                  span,
                                                  after);
            } else if (TRANSFERS_FLOW.equals(name)) {
                subject = "Transfers from " + spade.getName();
                slices = spade.getShippingSlices(fineBins,
                                                 span,
                                                 after);
            } else {
                return null;
            }
        } else {
            if (TRANSFERS_FLOW.equals(name)) {
                subject = "Transfers to " + neighbor
                          + " from "
                          + spade.getName();
                slices = spade.getShippingSlices(neighbor,
                                                 fineBins,
                                                 span,
                                                 after);
            } else {
                return null;
            }
        }
        String queryString = "";
        String conjunction = "?";
        if (fineBins) {
            queryString = queryString + conjunction
                          + "fine=true";
            conjunction = "&";
        }
        if (null != span) {
            queryString = queryString + conjunction
                          + "span="
                          + Integer.toString(span);
        }
        return new Flow(reportUri.resolve("flow/" + name
                                          + queryString),
                        subject,
                        slices);
    }

    /**
     * Returns the XML representation of all the bundles that currently active in
     * the application (waiting for an action is considered active).
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request containing a
     *         {@link Bundles} instance that contains of all {@link Bundle}
     *         instances that currently active in the application.
     */
    @GET
    @Path("bundles/active")
    @Produces({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getActiveBundles(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            if (spade.isSuspended()) {
                builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
                return builder.build();
            }
            final Bundles result = new Bundles(uriInfo.getRequestUri(),
                                               spade.getActiveBundles());
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | SuspendedException e) {
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of the detailed status of the named .
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param activity
     *            the name of the whose detailed status should be returned.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the named .
     */
    @GET
    @Path("activity/{name}/status")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getActivityStatus(@Context UriInfo uriInfo,
                                      @PathParam("name") String activity) {
        return createActivityStatusResponse(uriInfo,
                                            spade,
                                            null,
                                            activity);
    }

    /**
     * Returns the XML representation of the detailed status of the named .
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param workflow
     *            the name of the workflow that contains the named activity.
     * @param activity
     *            the name of the whose detailed status should be returned.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the named .
     */
    @GET
    @Path("activity/{workflow}/{activity}/status")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getActivityStatus(@Context UriInfo uriInfo,
                                      @PathParam("workflow") String workflow,
                                      @PathParam("activity") String activity) {
        return createActivityStatusResponse(uriInfo,
                                            spade,
                                            Workflow.valueOf(workflow),
                                            activity);
    }

    /**
     * Returns the representation of this application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the representation of this application.
     */
    @GET
    @Path("")
    @Produces({ SpadeType.APPLICATION_XML,
                SpadeType.APPLICATION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getApplication(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            final URI baseUri = uriInfo.getBaseUri();
            final URI reportUri = baseUri.resolve("report/");

            final URI timestampersUri = reportUri.resolve("timestamps/");
            final List<Action> timestampers = new ArrayList<>();
            for (String name : TIME_STAMPERS.keySet()) {
                final TimeStamper stamper = TIME_STAMPERS.get(name);
                if (null != stamper) {
                    timestampers.add(new Action(timestampersUri.resolve(name),
                                                name,
                                                stamper.getMessage()));
                }
            }
            final StamperGroup stamperGroup = new StamperGroup(null,
                                                               null,
                                                               timestampers);

            final List<ActionsGroup> reportActions = new ArrayList<>();

            final List<Action> applicationReports = new ArrayList<>();
            applicationReports.add(new Action(reportUri.resolve("application/configuration"),
                                              "configuration",
                                              "Shows the configuration currently in used by the application"));
            applicationReports.add(new Action(reportUri.resolve("application/registrations"),
                                              "registrations",
                                              "Shows all of the registrations in uses by the application"));
            applicationReports.add(new Action(reportUri.resolve(""),
                                              "summary",
                                              "Shows which application is connected, and whether it is running"));
            applicationReports.add(new Action(reportUri.resolve("application/status"),
                                              "status",
                                              "Shows details about the executing application"));
            reportActions.add(new ActionsGroup("application",
                                               null,
                                               applicationReports));

            final List<Action> bundlesReports = new ArrayList<>();
            bundlesReports.add(new Action(reportUri.resolve("bundles/active"),
                                          "active",
                                          "Shows the list of all active bundles"));
            bundlesReports.add(new Action(reportUri.resolve("bundles/blocked"),
                                          "blocked",
                                          "Shows the list of blocked bundles"));
            bundlesReports.add(new Action(reportUri.resolve("bundles/history"),
                                          "history",
                                          "Shows the history of the supplied bundles",
                                          SpadeType.BUNDLES_XML.toString()));
            bundlesReports.add(new Action(reportUri.resolve("bundles/placement"),
                                          "placement",
                                          "Shows the placememnt of the data files of supplied bundles",
                                          SpadeType.LOCATIONS_XML.toString()));
            reportActions.add(new ActionsGroup("bundles",
                                               null,
                                               bundlesReports));

            final List<Action> ticketsReports = new ArrayList<>();
            ticketsReports.add(new Action(reportUri.resolve("tickets/history"),
                                          "history",
                                          "Shows the history of the supplied tickets",
                                          SpadeType.TICKETS_XML.toString()));
            ticketsReports.add(new Action(reportUri.resolve("tickets/postponed"),
                                          "postponed",
                                          "Shows the set of ticket currently postponed, i.e. at least one dispatch has not been successful"));
            reportActions.add(new ActionsGroup("tickets",
                                               null,
                                               ticketsReports));

            final URI timingUri = reportUri.resolve("timestamps/");
            final List<Action> timingReports = new ArrayList<>();
            for (String name : TIME_STAMPERS.keySet()) {
                final TimeStamper stamper = TIME_STAMPERS.get(name);
                if (null != stamper) {
                    timingReports.add(new Action(timingUri.resolve(name),
                                                 name,
                                                 stamper.getMessage()));
                }
            }
            reportActions.add(new ActionsGroup("timing",
                                               null,
                                               timingReports));

            final URI commandUri = baseUri.resolve("command/");
            final List<ActionsGroup> commandActions = new ArrayList<>();

            final List<Action> applicationCommands = new ArrayList<>();
            applicationCommands.add(new Action(commandUri.resolve("application/scan/delivery"),
                                               "delivery_scan",
                                               "Scans the neighbors for completed deliveries"));
            applicationCommands.add(new Action(commandUri.resolve("application/drain"),
                                               "drain",
                                               "Suspends the workflows, but allows their action to drain existing tickets"));
            applicationCommands.add(new Action(commandUri.resolve("application/scan/inbound"),
                                               "inbound_scan",
                                               "Scan for new inbound bundles"));
            applicationCommands.add(new Action(commandUri.resolve("application/scan/local"),
                                               "local_scan",
                                               "Scan for new local bundles"));
            applicationCommands.add(new Action(commandUri.resolve("application/scan/redispatch"),
                                               "redispatch_scan",
                                               "Scan for bundles that need to be disptached again"));
            applicationCommands.add(new Action(commandUri.resolve("application/resume"),
                                               "resume",
                                               "Resumes the entire application"));
            applicationCommands.add(new Action(commandUri.resolve("application/suspend"),
                                               "suspend",
                                               "Suspends the entire application"));
            final ActionsGroup applicationGroup = new ActionsGroup("application",
                                                                   null,
                                                                   applicationCommands);
            commandActions.add(applicationGroup);

            final List<Action> bundlesCommands = new ArrayList<>();
            bundlesCommands.add(new Action(commandUri.resolve("bundles/ingest"),
                                           "ingest",
                                           "Ingests the supplied bundles",
                                           SpadeType.BUNDLES_XML.toString()));
            bundlesCommands.add(new Action(commandUri.resolve("bundles/unblock"),
                                           "unblock",
                                           "Unblocks the supplied blocked bundles",
                                           SpadeType.BUNDLES_XML.toString()));
            final ActionsGroup bundlesGroup = new ActionsGroup("bundles",
                                                               null,
                                                               bundlesCommands);
            commandActions.add(bundlesGroup);

            final List<Action> ticketsCommands = new ArrayList<>();
            ticketsCommands.add(new Action(commandUri.resolve("tickets/abandon"),
                                           "abandon",
                                           "Abandons any futher dispatches of the supplied tickets",
                                           SpadeType.TICKETS_XML.toString()));
            ticketsCommands.add(new Action(commandUri.resolve("tickets/rearchive"),
                                           "rearchive",
                                           "Re-archives the supplied tickets",
                                           SpadeType.TICKETS_XML.toString()));
            ticketsCommands.add(new Action(commandUri.resolve("tickets/redispatch"),
                                           "redispatch",
                                           "Re-dispatches the supplied tickets",
                                           SpadeType.TICKETS_XML.toString()));
            ticketsCommands.add(new Action(commandUri.resolve("tickets/reship"),
                                           "reship",
                                           "Re-ships the supplied tickets",
                                           SpadeType.TICKETS_XML.toString()));
            final ActionsGroup ticketGroup = new ActionsGroup("tickets",
                                                              null,
                                                              ticketsCommands);
            commandActions.add(ticketGroup);

            builder = Response.status(Response.Status.OK);
            builder.entity(new Application(reportUri,
                                           createApplicationStatus(uriInfo,
                                                                   spade,
                                                                   true),
                                           stamperGroup,
                                           reportActions,
                                           commandActions,
                                           Spade.SPECIFICATION,
                                           implementation));
        } catch (InitializingException e) {
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of the detailed status of the application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the application.
     */
    @GET
    @Path("application/status")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getApplicationStatus(@Context UriInfo uriInfo) {
        return createApplicationStatusReponse(uriInfo,
                                              spade,
                                              false);
    }

    /**
     * Returns the XML representation of all the bundles that currently being
     * "blocked" by the application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request containing a
     *         {@link Bundles} instance that contains of all {@link Bundle}
     *         instances that currently active in the application.
     */
    @GET
    @Path("bundles/blocked")
    @Produces({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getBlockedBundles(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            if (spade.isSuspended()) {
                builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
                return builder.build();
            }
            final Bundles result = new Bundles(uriInfo.getRequestUri(),
                                               spade.getBlockedBundles());
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | SuspendedException e) {
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of the set of history of a bundle in the form
     * of a {@link Digest} instances for the specified bundle.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param bundle
     *            the bundle whose time-stamp {@link Digest} should be returned.
     *
     * @return the XML representation of the set of time-stamp {@link Digest}
     *         instances for the specified bundle.
     */
    @GET
    @Path("bundle/{bundle}/history")
    @Produces({ NestType.DIGESTS_XML,
                NestType.DIGESTS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getBundleHistory(@Context UriInfo uriInfo,
                                     @PathParam("bundle") String bundle) {
        final List<String> identities = spade.getTicketsByBundle(bundle);
        return createTicketsHistoriesResponse(uriInfo,
                                              identities,
                                              spade);
    }

    /**
     * Returns the XML representation of the set of history of a set of bundles in
     * the form of a {@link Digest} instances for the specified bundles.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param bundles
     *            the bundles whose time-stamp {@link Digest} should be returned.
     *
     * @return the XML representation of the set of time-stamp {@link Digest}
     *         instances for the specified bundles.
     */
    @GET
    @POST
    @Path("bundles/history")
    @Consumes({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ NestType.DIGESTS_XML,
                NestType.DIGESTS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getBundlesHistories(@Context UriInfo uriInfo,
                                        Bundles bundles) {
        final Collection<String> identities;
        if (null == bundles) {
            identities = null;
        } else {
            identities = new ArrayList<>();
            for (Bundle bundle : bundles.getBundles()) {
                identities.addAll(spade.getTicketsByBundle(bundle.getName()));
            }
        }
        return createTicketsHistoriesResponse(uriInfo,
                                              identities,
                                              spade);
    }

    /**
     * Returns the XML representation of the configuration file used by the
     * application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the application.
     */
    @GET
    @Path("application/configuration")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getConfiguration(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            final Configuration configuration = spade.getConfiguration();
            builder = Response.status(Response.Status.OK);
            builder = builder.entity(configuration);
        } catch (Exception e) {
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of the flow of data for the specified part of
     * the application in the specified slices.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param name
     *            the name of the flow to return.
     * @param fineBins
     *            true if fine "epoch" bins should be used, otherwise course will be
     *            used.
     * @param span
     *            the limit, if any, of number of slices to include.
     * @param after
     *            the date and time from which to start the sequence. If
     *            <code>null</code> then this is set to "now" minus the time covered
     *            by the "span".
     *
     *
     * @return the XML representation of the flow of data for the specified part of
     *         the application in the specified slices.
     */
//    @GET
//    @Path("flow/{name}")
//    @Produces({ SpadeType.FLOW_XML,
//                MediaType.APPLICATION_XML })
    public Response getFlow(@Context UriInfo uriInfo,
                            @PathParam("name") String name,
                            @QueryParam("fine") boolean fineBins,
                            @QueryParam("span") Integer span,
                            @QueryParam("after") String after) {
        ResponseBuilder builder;
        try {
            final Date afterDate = DateUtilities.parseQueryDate(after);
            final URI reportUri = (uriInfo.getBaseUri()).resolve("report/");
            final Flow flow = createFlow(reportUri,
                                         name,
                                         fineBins,
                                         span,
                                         afterDate,
                                         null);
            if (null == flow) {
                builder = Response.status(Response.Status.NOT_FOUND);
            } else {
                builder = Response.status(Response.Status.OK);
                builder = builder.entity(flow);
            }

        } catch (ParseException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        } catch (NumberFormatException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        }
        return builder.build();
    }

    // static member methods (alphabetic)

    /**
     * Returns the JSON representation of the flow of data for the specified part of
     * the application in the specified slices.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param name
     *            the name of the flow to return.
     * @param fineBins
     *            true if fine "epoch" bins should be used, otherwise course will be
     *            used.
     * @param span
     *            the limit, if any, of number of slices to include.
     * @param after
     *            the date and time from which to start the sequence. If
     *            <code>null</code> then this is set to "now" minus the time covered
     *            by the "span".
     *
     * @return the JSON representation of the flow of data for the specified part of
     *         the application in the specified slices.
     */
    @GET
    @Path("flow/{name}")
    @Produces({ SpadeType.FLOW_JSON,
                MediaType.APPLICATION_JSON })
    public Response getFlow_JSON(@Context UriInfo uriInfo,
                                 @PathParam("name") String name,
                                 @QueryParam("fine") boolean fineBins,
                                 @QueryParam("span") Integer span,
                                 @QueryParam("after") String after) {
        ResponseBuilder builder;
        try {
            final Date afterDate = DateUtilities.parseQueryDate(after);
            final URI reportUri = (uriInfo.getBaseUri()).resolve("report/");
            final Flow flow = createFlow(reportUri,
                                         name,
                                         fineBins,
                                         span,
                                         afterDate,
                                         null);
            if (null == flow) {
                builder = Response.status(Response.Status.NOT_FOUND);
            } else {
                builder = Response.status(Response.Status.OK);
                builder = builder.entity(flow.toJSON(fineBins));
            }
        } catch (ParseException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        } catch (NumberFormatException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of the placement of the specified bundle
     * within the warehouse.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param bundle
     *            the number of hours to include in the result.
     * @param filters
     *            the list, if any, of field to include in the returned
     *            {@link Location} instance. A <code>null</code> or empty list means
     *            all field should be included.
     *
     * @return the XML representation of latest placement counts for this
     *         application.
     */
    @GET
    @Path("bundle/{bundle}/placement")
    @Produces({ SpadeType.LOCATION_XML,
                SpadeType.LOCATION_XML,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getPlacement(@Context UriInfo uriInfo,
                                 @PathParam("bundle") String bundle,
                                 @QueryParam("filter") List<String> filters) {
        final String placementId = spade.getPlacementId(bundle);
        final Location location;
        if (null == placementId) {
            location = null;
        } else {
            location = warehouseManager.getLocation(placementId,
                                                    filters);
        }
        ResponseBuilder builder;
        if (null == location) {
            builder = Response.status(Response.Status.NOT_FOUND);
        } else {
            builder = Response.status(Response.Status.OK);
            builder = builder.entity(location);
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of the placements of the specified bundles
     * within the warehouse.
     *
     * N.B. This is also a POST method for the technical reasons that a
     * HttpURLConnection can not send data to a GET method.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param bundles
     *            the collection of bundle identities whose bundles should be
     *            included in the result.
     * @param filters
     *            the list, if any, of field to include in the returned
     *            {@link Location} instance. A <code>null</code> or empty list means
     *            all field should be included.
     *
     * @return the XML representation of latest placement counts for this
     *         application.
     */
    @GET
    @POST
    @Path("bundles/placement")
    @Consumes({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ SpadeType.LOCATIONS_XML,
                SpadeType.LOCATIONS_XML,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getPlacements(@Context UriInfo uriInfo,
                                  Bundles bundles,
                                  @QueryParam("filter") List<String> filters) {
        final Locations placements;
        if (null == bundles) {
            placements = null;
        } else {
            final List<Location> content = new ArrayList<>();
            placements = new Locations(content);
            for (Bundle bundle : bundles.getBundles()) {
                final String placementId = spade.getPlacementId(bundle.getName());
                final Location location;
                if (null == placementId) {
                    content.add(new Location(placementId,
                                             null));
                } else {
                    location = warehouseManager.getLocation(placementId,
                                                            filters);
                    if (null == location) {
                        content.add(new Location(placementId,
                                                 null));
                    } else {
                        content.add(location);
                    }
                }
            }
        }
        ResponseBuilder builder;
        builder = Response.status(Response.Status.OK);
        builder = builder.entity(placements);
        return builder.build();
    }

    /**
     * Returns the XML representation of all the tickets that currently being
     * "postponed" by the application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param cushion
     *            the time in minutes that must have elapsed before a bundle is
     *            included in the result.
     *
     * @return the {@link Response} generated for this request containing a
     *         {@link Bundles} instance that contains of all {@link Bundle}
     *         instances that currently active in the application.
     */
    @Path("tickets/postponed")
    @GET
    @Produces({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getPostponed(@Context UriInfo uriInfo,
                                 @QueryParam("cushion") long cushion) {
        ResponseBuilder builder;
        try {
            if (spade.isSuspended()) {
                builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
                return builder.build();
            }
            final Tickets result = new Tickets(uriInfo.getRequestUri(),
                                               spade.getPostponed(cushion,
                                                                  TimeUnit.MINUTES));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (Exception e) {
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of a known registrations.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param registration
     *            the registration to include in the returned {@link Response}
     *            instance.
     *
     * @return the XML representation of the known registrations in this
     *         application.
     */
    @GET
    @Path("registration/{registration}/xml")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getRegistration(@Context UriInfo uriInfo,
                                    @PathParam("registration") String registration) {
        final List<String> registrations = new ArrayList<>();
        registrations.add(registration);
        final Registry registry = spade.getRegistrations(registrations);
        ResponseBuilder builder;
        if (null == registry) {
            builder = Response.status(Response.Status.NOT_FOUND);
        } else {
            String localId = null;
            if ((registry.getLocal()).isEmpty()) {
                if ((registry.getLocal()).isEmpty()) {
                    builder = Response.status(Response.Status.NOT_FOUND);
                } else {
                    final InboundRegistration inbound = (registry.getInbound()).get(0);
                    localId = inbound.getLocalId();
                    builder = Response.status(Response.Status.OK);
                    builder = builder.entity(inbound);
                }
            } else {
                final LocalRegistration local = (registry.getLocal()).get(0);
                localId = local.getLocalId();
                builder = Response.status(Response.Status.OK);
                builder = builder.entity(local);
            }
            if (null == localId) {
                builder = Response.status(Response.Status.NOT_FOUND);
            } else {
                registry.setUri((uriInfo.getBaseUri()).resolve("report/registration/" + localId
                                                               + "/xml"));
            }
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of the known registrations.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param registrations
     *            the collections of registrations to include in the returned
     *            {@link Response} instance. A <code>null</code> or empty list means
     *            all registrations should be included.
     *
     * @return the XML representation of the known registrations in this
     *         application.
     */
    @GET
    @Path("application/registrations")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getRegistrations(@Context UriInfo uriInfo,
                                     @QueryParam("registration") List<String> registrations) {
        final Registry registry = spade.getRegistrations(registrations);
        ResponseBuilder builder;
        if (null == registry) {
            builder = Response.status(Response.Status.NOT_FOUND);
        } else {
            registry.setUri((uriInfo.getBaseUri()).resolve("report/registrations"));
            builder = Response.status(Response.Status.OK);
            builder = builder.entity(registry);
        }
        return builder.build();
    }

    /**
     * Returns the {@link Spade} instance used by this object.
     *
     * @return the {@link Spade} instance used by this object.
     */
    Spade getSpade() {
        return spade;
    }

    /**
     * Returns the XML representation of the placement of the specified bundle
     * within the warehouse.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param bundle
     *            the number of hours to include in the result.
     * @param summary
     *            <code>true</code> if only a summary of the bundle should be
     *            returned.
     *
     * @return the XML representation of latest placement counts for this
     *         application.
     */
    @GET
    @Path("bundle/{bundle}/status")
    @Produces({ SpadeType.LOCATION_XML,
                SpadeType.LOCATION_XML,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getStatusByBundle(@Context UriInfo uriInfo,
                                      @PathParam("bundle") String bundle,
                                      @QueryParam("summary") Boolean summary) {
        return createBundleStatusReponse(uriInfo,
                                         bundle,
                                         spade,
                                         !(null == summary || summary.booleanValue()));
    }

    /**
     * Returns the XML representation of the set of history of a ticket in the form
     * of a {@link Digest} instances for the specified ticket.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param ticket
     *            the ticket whose time-stamp {@link Digest} should be returned.
     *
     * @return the XML representation of the set of time-stamp {@link Digest}
     *         instances for the specified ticket.
     */
    @GET
    @Path("ticket/{ticket}/history")
    @Produces({ NestType.DIGESTS_XML,
                NestType.DIGESTS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getTicketHistory(@Context UriInfo uriInfo,
                                     @PathParam("ticket") String ticket) {
        return createTicketHistoryResponse(uriInfo,
                                           ticket,
                                           spade);
    }

    /**
     * Returns the XML representation of the set of history of a ticket in the form
     * of a {@link Digest} instances for the specified ticket.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param tickets
     *            the tickets whose time-stamp {@link Digest} should be returned.
     *
     * @return the XML representation of the set of time-stamp {@link Digest}
     *         instances for the specified ticket.
     */
    @GET
    @POST
    @Path("tickets/history")
    @Consumes({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ NestType.DIGESTS_XML,
                NestType.DIGESTS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getTicketsHistories(@Context UriInfo uriInfo,
                                        Tickets tickets) {
        final Collection<String> identities;
        if (null == tickets) {
            identities = null;
        } else {
            identities = tickets.getTickets();
        }
        return createTicketsHistoriesResponse(uriInfo,
                                              identities,
                                              spade);
    }

    /**
     * Returns the XML representation of the list of bundle identities and
     * associated times that have been acted between specified dates.
     *
     * @param stamper
     *            the stamper for which to return the list of time stamps.
     * @param neighbor
     *            the neighbor, if any, for which to return the list of time stamps.
     *            <code>null</code> implies all neighbors should be considered.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param reversed
     *            TODO
     * @param max
     *            the maximum number of bundles to return, 0 or less means
     *            unlimited.
     * @param registrations
     *            the collections of registrations for which to return in the list
     *            of time stamps. A <code>null</code> or empty list means all
     *            registrations should be included.
     *
     *            <b>Note:</b> This is different from the other
     *            <code>getTimeStampsByStamper</code> method only by the order of
     *            the parameters. The reason there are two version of this method is
     *            that they have different @Path values.
     * @param direction
     *            "inbound/outbound" to filter the returned list.
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @see #getTimeStampsByStamper(UriInfo, String, String, String, String,
     *      Boolean, Integer, List, String)
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the list of bundle identities and associated times
     *         that have been acted between the specified dates.
     */
    @GET
    @Path("timestamps/{stamper}/{neighbor}")
    @Produces({ NestType.DIGESTS_XML,
                NestType.DIGESTS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getTimeStampsByStamper(@PathParam("stamper") String stamper,
                                           @PathParam("neighbor") String neighbor,
                                           @QueryParam("after") String after,
                                           @QueryParam("before") String before,
                                           @QueryParam("reversed") Boolean reversed,
                                           @QueryParam("max") Integer max,
                                           @QueryParam("registration") List<String> registrations,
                                           @QueryParam("direction") String direction,
                                           @Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            final Date afterDate = DateUtilities.parseQueryDate(after);
            final Date beforeDate = DateUtilities.parseQueryDate(before);
            final boolean reverseOrder;
            if (null == reversed || !reversed.booleanValue()) {
                reverseOrder = false;
            } else {
                reverseOrder = true;
            }
            final int maxCount;
            if (null == max) {
                maxCount = 0;
            } else {
                maxCount = max.intValue();
            }
            if (!TIME_STAMPERS.containsKey(stamper)) {
                builder = Response.status(Response.Status.NOT_FOUND);
            } else {
                final TimeStamper stamperToUse = TIME_STAMPERS.get(stamper);
                if (null == stamperToUse) {
                    builder = Response.status(NOT_IMPLEMENTED);
                } else {
                    final Boolean inbound;
                    if (null == direction) {
                        inbound = null;
                    } else if ("inbound".equals(direction)) {
                        inbound = Boolean.TRUE;
                    } else if ("outbound".equals(direction)) {
                        inbound = Boolean.FALSE;
                    } else {
                        inbound = null;
                    }
                    final List<DigestChange> items = stamperToUse.getTimeStamps(afterDate,
                                                                                beforeDate,
                                                                                reverseOrder,
                                                                                maxCount,
                                                                                neighbor,
                                                                                registrations,
                                                                                inbound);
                    final Date afterToUse;
                    final Date beforeToUse;
                    if (reverseOrder) {
                        if (null == items || items.isEmpty()) {
                            afterToUse = null;
                        } else {
                            final DigestChange lastChange = items.get(items.size() - 1);
                            afterToUse = lastChange.getWhenItemChanged();
                        }
                        beforeToUse = beforeDate;
                    } else {
                        afterToUse = afterDate;
                        if (null == items || items.isEmpty()) {
                            beforeToUse = null;
                        } else {
                            final DigestChange lastChange = items.get(items.size() - 1);
                            beforeToUse = lastChange.getWhenItemChanged();
                        }
                    }

                    builder = Response.status(Response.Status.OK);
                    builder = builder.entity(new Digest((uriInfo.getBaseUri()).resolve("report/timestamps/" + stamper),
                                                        stamperToUse.getMessage() + " for "
                                                                                                                        + spade.getName(),
                                                        afterToUse,
                                                        beforeToUse,
                                                        items));
                }
            }
        } catch (ParseException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        } catch (NumberFormatException e) {
            builder = Response.status(Response.Status.BAD_REQUEST);
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of the list of bundle identities and
     * associated times that have been acted between specified dates.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param stamper
     *            the stamper for which to return the list of time stamps.
     * @param neighbor
     *            the neighbor, if any, for which to return the list of time stamps.
     *            <code>null</code> implies all neighbors should be considered.
     *
     *            <b>Note:</b> This is different from the other
     *            <code>getTimeStampsByStamper</code> method only by the order of
     *            the parameters. The reason there are two version of this method is
     *            that they have different @Path values.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param reversed
     *            TODO
     * @param max
     *            the maximum number of bundles to return, 0 or less means
     *            unlimited.
     * @param registrations
     *            the collections of registrations for which to return in the list
     *            of time stamps. A <code>null</code> or empty list means all
     *            registrations should be included.
     * @param direction
     *            "inbound/outbound" to filter the returned list.
     *
     * @see #getTimeStampsByStamper(UriInfo, String, String, String, String,
     *      Boolean, Integer, List, String)
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the list of bundle identities and associated times
     *         that have been acted between the specified dates.
     */
    @GET
    @Path("timestamps/{stamper}")
    @Produces({ NestType.DIGESTS_XML,
                NestType.DIGESTS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getTimeStampsByStamper(@Context UriInfo uriInfo,
                                           @PathParam("stamper") String stamper,
                                           @QueryParam("neighbor") String neighbor,
                                           @QueryParam("after") String after,
                                           @QueryParam("before") String before,
                                           @QueryParam("reversed") Boolean reversed,
                                           @QueryParam("max") Integer max,
                                           @QueryParam("registration") List<String> registrations,
                                           @QueryParam("direction") String direction) {
        return getTimeStampsByStamper(stamper,
                                      neighbor,
                                      after,
                                      before,
                                      reversed,
                                      max,
                                      registrations,
                                      direction,
                                      uriInfo);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
