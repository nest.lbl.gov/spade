package gov.lbl.nest.spade.rs;

import java.net.URI;

import gov.lbl.nest.common.rs.NamedResource;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class describes an action that can be executed by the application.
 * 
 * @author patton
 */
@XmlType
public class Action extends
                    NamedResource {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The media type, if any, of the attachment the action needs.
     */
    private String attachment;

    /**
     * True string if the action is deprecated.
     */
    private String deprecated;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI with which to access the resource.
     * @param name
     *            the name by which the resource should be references.
     * @param description
     *            the short description of the resource.
     */
    public Action(URI uri,
                  String name,
                  String description) {
        this(uri,
             name,
             description,
             null,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI with which to access the resource.
     * @param name
     *            the name by which the resource should be references.
     * @param description
     *            the short description of the resource.
     * @param attachment
     *            the media type, if any, of the attachment the action needs.
     */
    public Action(URI uri,
                  String name,
                  String description,
                  String attachment) {
        this(uri,
             name,
             description,
             attachment,
             null);
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI with which to access the resource.
     * @param name
     *            the name by which the resource should be references.
     * @param description
     *            the short description of the resource.
     * @param attachment
     *            The media type, if any, of the attachment the action needs.
     * @param deprecated
     *            {@link Boolean} instance of TRUE if the action is deprecated.
     */
    public Action(URI uri,
                  String name,
                  String description,
                  String attachment,
                  Boolean deprecated) {
        super(uri,
              name,
              description);
        this.attachment = attachment;
        if (null == deprecated) {
            this.deprecated = null;
        } else {
            this.deprecated = deprecated.toString();
        }
    }

    /**
     * Returns the media type, if any, of the attachment the action needs.
     *
     * @return the media type, if any, of the attachment the action needs.
     */
    @XmlElement
    public String getAttachment() {
        return attachment;
    }

    /**
     * Returns true string if the action is deprecated.
     *
     * @return true string if the action is deprecated.
     */
    @XmlAttribute
    public String getDeprecated() {
        return deprecated;
    }

    /**
     * Sets the media type, if any, of the attachment the action needs.
     *
     * @param attachment
     *            the media type, if any, of the attachment the action needs.
     */
    protected void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    /**
     * Sets whether the action is deprecated or not
     *
     * @param deprecated
     *            true string if the action is deprecated.
     */
    protected void setDeprecated(String deprecated) {
        this.deprecated = deprecated;
    }

}
