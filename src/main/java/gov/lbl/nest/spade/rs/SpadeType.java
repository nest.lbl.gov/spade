package gov.lbl.nest.spade.rs;

import gov.lbl.nest.common.watching.Digest;

/**
 * The class contains the media types used by the RESTful interface.
 *
 * @author patton
 *
 */
public class SpadeType {

    /**
     * The media type for the XML representation of an {@link Application} instance.
     */
    public static final String APPLICATION_XML = "application/gov.lbl.nest.spade.rs.Application+xml";

    /**
     * The media type for the JSON representation of an {@link Application}
     * instance.
     */
    public static final String APPLICATION_JSON = "application/gov.lbl.nest.spade.rs.Application+json";

    /**
     * The media type for the XML representation of an {@link Bundles} instance.
     */
    public static final String BUNDLES_XML = "application/gov.lbl.nest.spade.rs.Bundles+xml";

    /**
     * The media type for the JSON representation of an {@link Bundles} instance.
     */
    public static final String BUNDLES_JSON = "application/gov.lbl.nest.spade.rs.Bundles+json";

    /**
     * The media type for the XML representation of an {@link Digest} instance.
     */
    public static final String DIGEST_XML = "application/gov.lbl.nest.common.watching.Digest+xml";

    /**
     * The media type for the JSON representation of an {@link Digest} instance.
     */
    public static final String DIGEST_JSON = "application/gov.lbl.nest.common.watching.Digest+json";

    /**
     * The media type for the XML representation of an {@link Digest} instance.
     */
    public static final String FLOW_XML = "application/gov.lbl.nest.spade.rs.Flow+xml";

    /**
     * The media type for the JSON representation of an {@link Digest} instance.
     */
    public static final String FLOW_JSON = "application/gov.lbl.nest.spade.rs.Flow+json";

    /**
     * The media type for the XML representation of an {@link Bundles} instance.
     */
    public static final String LOCATION_XML = "application/gov.lbl.nest.spade.rs.Location+xml";

    /**
     * The media type for the JSON representation of an {@link Bundles} instance.
     */
    public static final String LOCATION_JSON = "application/gov.lbl.nest.spade.rs.Location+json";

    /**
     * The media type for the XML representation of an {@link Bundles} instance.
     */
    public static final String LOCATIONS_XML = "application/gov.lbl.nest.spade.rs.Locations+xml";

    /**
     * The media type for the JSON representation of an {@link Bundles} instance.
     */
    public static final String LOCATIONS_JSON = "application/gov.lbl.nest.spade.rs.Locations+json";

    /**
     * The media type for the XML representation of an {@link Tickets} instance.
     */
    public static final String TICKETS_XML = "application/gov.lbl.nest.spade.rs.Tickets+xml";

    /**
     * The media type for the JSON representation of an {@link Bundles} instance.
     */
    public static final String TICKETS_JSON = "application/gov.lbl.nest.spade.rs.Tickets+json";

}
