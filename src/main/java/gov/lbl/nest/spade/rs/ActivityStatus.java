package gov.lbl.nest.spade.rs;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.spade.workflow.ActivityTaskStatus;
import gov.lbl.nest.spade.workflow.ActivityTaskStatus.STATE;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the current
 * status of the the application.
 *
 * @author patton
 */
@XmlType(propOrder = { "counts",
                       "complete",
                       "executing",
                       "pending" })
@XmlRootElement(name = "activity_status")
public class ActivityStatus extends
                            ExecutionStatus {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    private static StatusCounts createStatusCounts(final Integer threads,
                                                   final Integer target,
                                                   final List<String> executing,
                                                   final List<String> pending) {
        final Integer executingSize;
        if (null == executing) {
            executingSize = null;
        } else {
            executingSize = executing.size();
        }
        final Integer pendingSize;
        if (null == pending) {
            pendingSize = null;
        } else {
            pendingSize = pending.size();
        }
        return new StatusCounts(executingSize,
                                pendingSize,
                                target);
    }

    /**
     * The list of labels for Instances that are complete.
     */
    private List<String> complete;

    /**
     * The {@link StatusCounts} for this activity.
     */
    private StatusCounts counts;

    /**
     * The list of labels for Instances that are executing.
     */
    private List<String> executing;

    // constructors

    /**
     * The list of labels for Instances that are pending.
     */
    private List<String> pending;

    /**
     * Creates an instance of this class.
     */
    protected ActivityStatus() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the activity.
     * @param condition
     *            the current execution state of the activity.
     * @param counts
     *            the counts for this activity.
     */
    public ActivityStatus(URI uri,
                          final String name,
                          final Condition condition,
                          final StatusCounts counts) {
        super(uri,
              name,
              condition,
              true);
        this.counts = counts;
    }

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the activity.
     * @param condition
     *            the current execution state of the activity.
     * @param target
     *            the target number of threads.
     * @param activityTaskStatuses
     *            the collection of {@link ActivityTaskStatus} instance associated
     *            with this Object.
     */
    public ActivityStatus(URI uri,
                          final String name,
                          final ExecutionStatus.Condition condition,
                          final Integer target,
                          List<ActivityTaskStatus> activityTaskStatuses) {
        super(uri,
              name,
              condition,
              null);
        if (null != activityTaskStatuses) {
            for (ActivityTaskStatus activityTaskStatus : activityTaskStatuses) {
                if (STATE.EXECUTING == activityTaskStatus.getState()) {
                    if (null == executing) {
                        executing = new ArrayList<>();
                    }
                    executing.add(activityTaskStatus.getBundle());
                } else if (STATE.PENDING == activityTaskStatus.getState()) {
                    if (null == pending) {
                        pending = new ArrayList<>();
                    }
                    pending.add(activityTaskStatus.getBundle());
                } else {
                    if (null == complete) {
                        complete = new ArrayList<>();
                    }
                    complete.add(activityTaskStatus.getBundle());
                }
            }
            counts = createStatusCounts(null,
                                        target,
                                        executing,
                                        pending);
        }
    }

    /**
     * Returns the list of labels for Instances that are complete.
     *
     * @return the list of labels for Instances that are complete.
     */
    @XmlElement
    protected List<String> getComplete() {
        return complete;
    }

    /**
     * Returns the {@link StatusCounts} for this activity.
     *
     * @return the {@link StatusCounts} for this activity.
     */
    @XmlElement
    protected StatusCounts getCounts() {
        return counts;
    }

    /**
     * Returns the list of labels for Instances that are executing.
     *
     * @return the list of labels for Instances that are executing.
     */
    @XmlElement
    protected List<String> getExecuting() {
        return executing;
    }

    /**
     * Returns the list of labels for Instances that are pending.
     *
     * @return the list of labels for Instances that are pending.
     */
    @XmlElement
    protected List<String> getPending() {
        return pending;
    }

    /**
     * Sets the {@link StatusCounts} for this activity.
     *
     * @param counts
     *            the {@link StatusCounts} for this activity.
     */
    protected void setCounts(StatusCounts counts) {
        this.counts = counts;
    }

    /**
     * Sets the list of labels for Instances that are executing.
     *
     * @param executing
     *            the list of labels for Instances that are executing.
     */
    protected void setExecuting(List<String> executing) {
        this.executing = executing;
    }

    // static member methods (alphabetic)

    /**
     * Sets the list of labels for Instances that are pending.
     *
     * @param pending
     *            the list of labels for Instances that are pending.
     */
    protected void setPending(List<String> pending) {
        this.pending = pending;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
