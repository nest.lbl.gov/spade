package gov.lbl.nest.spade.rs;

import java.util.List;

import gov.lbl.nest.common.rs.NamedResource;
import gov.lbl.nest.common.rs.NamedResourcesGroup;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, a group of
 * time-stampers that are {@link NamedResource} instances.
 *
 * @author patton
 */
@XmlType(propOrder = { "actions" })
public class StamperGroup extends
                          NamedResourcesGroup {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected StamperGroup() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name by which the group should be references.
     * @param description
     *            the short description of the group.
     * @param resources
     *            the {@link NamedResource} instances that are in this group.
     */
    public StamperGroup(String name,
                        String description,
                        List<? extends NamedResource> resources) {
        super(name,
              description,
              resources);
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link NamedResource} instances that are the actions in this
     * group.
     *
     * @return the {@link NamedResource} instances that are the actions in this
     *         group.
     */
    @XmlElement(name = "stamper")
    protected List<? extends NamedResource> getActions() {
        return getResources();
    }

    /**
     * Sets the {@link NamedResource} instances that are the actions in this group.
     *
     * @param actions
     *            the {@link NamedResource} instances that are the actions in this
     *            group.
     */
    protected void setActions(List<NamedResource> actions) {
        setResources(actions);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
