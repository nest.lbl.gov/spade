package gov.lbl.nest.spade.rs;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.watching.Digest;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the current
 * status of a Bundle.
 *
 * @author patton
 */
@XmlRootElement(name = "bundle_status")
@XmlType(propOrder = { "histories" })
public class BundleStatus extends
                          Status {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The histories of the most recent tickets associated with this bundle.
     */
    private List<Digest> histories;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected BundleStatus() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the component of the component of the application
     * @param history
     *            the history of the most recent ticket associated with this bundle.
     */
    public BundleStatus(final URI uri,
                        final String name,
                        final Digest history) {
        super(uri,
              name,
              true);
        histories = new ArrayList<>();
        histories.add(history);
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the component of the component of the application
     * @param histories
     *            the histories of the most recent tickets associated with this
     *            bundle.
     */
    public BundleStatus(final URI uri,
                        final String name,
                        final List<Digest> histories) {
        super(uri,
              name,
              null);
        this.histories = histories;
    }

    // instance member method (alphabetic)

    /**
     * Returns the histories of the most recent tickets associated with this bundle.
     *
     * @return the histories of the most recent tickets associated with this bundle.
     */
    @XmlElement(name = "history")
    protected List<Digest> getHistories() {
        return histories;
    }

    /**
     * Sets the histories of the most recent tickets associated with this bundle.
     *
     * @param histories
     *            he histories of the most recent tickets associated with this
     *            bundle.
     */
    protected void setHistories(List<Digest> histories) {
        this.histories = histories;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
