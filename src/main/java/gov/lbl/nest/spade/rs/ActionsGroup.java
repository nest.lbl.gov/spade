package gov.lbl.nest.spade.rs;

import java.util.List;

import gov.lbl.nest.common.rs.NamedResource;
import gov.lbl.nest.common.rs.NamedResourcesGroup;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, a group of
 * actions that are {@link NamedResource} instances.
 *
 * @author patton
 */
@XmlType(propOrder = { "actions" })
public class ActionsGroup extends
                          NamedResourcesGroup {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ActionsGroup() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name by which the group should be references.
     * @param description
     *            the short description of the group.
     * @param resources
     *            the {@link NamedResource} instances that are in this group.
     */
    public ActionsGroup(String name,
                        String description,
                        List<Action> resources) {
        super(name,
              description,
              resources);
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link NamedResource} instances that are the actions in this
     * group.
     *
     * @return the {@link NamedResource} instances that are the actions in this
     *         group.
     */
    @SuppressWarnings("unchecked")
    @XmlElement(name = "action")
    protected List<Action> getActions() {
        return (List<Action>) getResources();
    }

    /**
     * Sets the {@link NamedResource} instances that are the actions in this group.
     *
     * @param actions
     *            the {@link NamedResource} instances that are the actions in this
     *            group.
     */
    protected void setActions(List<Action> actions) {
        setResources(actions);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
