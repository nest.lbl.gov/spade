package gov.lbl.nest.spade.rs;

import java.net.URI;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the current
 * status of the component of the application.
 *
 * @author patton
 */
@XmlType(propOrder = { "condition" })
public class ExecutionStatus extends
                             Status {

    /**
     * This enumerates the possible execution states of a component of the
     * application.
     *
     * @author patton
     */
    @XmlEnum(String.class)
    public enum Condition {
                           /**
                            * In this state the component is running and none of its constituent components
                            * have been suspended.
                            */
                           RUNNING,

                           /**
                            * In this state the component is running and one of more of its constituent
                            * components have been suspended.
                            */
                           PARTIALLY_RUNNING,

                           /**
                            * In this state the component has been suspended but one of more of its
                            * constituent components are running.
                            */
                           PARTIALLY_SUSPENDED,

                           /**
                            * In this state the component has been suspended and none of its constituent
                            * components are running.
                            */
                           SUSPENDED
    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The current execution state of the component of the application.
     */
    private Condition condition;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ExecutionStatus() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the component of the component of the application
     * @param execution
     *            the current execution state of the component of the application.
     * @param summary
     *            <code>true</code> if this status is not the complete one.
     */
    public ExecutionStatus(final URI uri,
                           final String name,
                           final Condition execution,
                           final Boolean summary) {
        super(uri,
              name,
              summary);
        setCondition(execution);
    }

    // instance member method (alphabetic)

    /**
     * Returns the current execution state of the component of the application.
     *
     * @return the current execution state of the component of the application.
     */
    @XmlElement(name = "execution")
    protected Condition getCondition() {
        return condition;
    }

    /**
     * Sets the current execution state of the component of the application.
     *
     * @param execution
     *            the current execution state of the component of the application.
     */
    public void setCondition(final Condition execution) {
        this.condition = execution;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
