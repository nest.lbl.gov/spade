package gov.lbl.nest.spade.rs;

import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.Registration;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, a group of one
 * or more files the SPADE will treat as a single unit.
 *
 * @author patton
 */
@XmlType(propOrder = { "name",
                       "semaphore",
                       "localId",
                       "neighbor",
                       "note" })
public class Bundle {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The local {@link Registration} identity, if any, associated with this object.
     */
    private String localId;

    /**
     * The name of this bundle.
     */
    private String name;

    /**
     * The identity of the remote SPADE instance, if any, who delivered the file.
     */
    private String neighbor;

    /**
     * The note, if any, attached to this bundle.
     */
    private String note;

    /**
     * /** The external location of the semaphore file.
     */
    private ExternalFile semaphore;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Bundle() {
    }

    /**
     * Create a copy of this class.
     *
     * @param rhs
     *            the {@link Bundle} instance to be copied.
     */
    public Bundle(final Bundle rhs) {
        this(rhs.name,
             new ExternalFile(rhs.semaphore),
             rhs.localId,
             rhs.neighbor);
    }

    /**
     * Creates an instance of this class
     *
     * @param name
     *            the name of this bundle.
     */
    public Bundle(String name) {
        setName(name);
    }

    /**
     * Creates an instance of this class
     *
     * @param name
     *            the name of this bundle.
     * @param externalFile
     *            the external location of the semaphore file.
     * @param localId
     *            the local {@link Registration} identity, if any, associated with
     *            this object.
     * @param neighbor
     *            the identity of the remote SPADE instance, if any, who delivered
     *            the file.
     */
    public Bundle(final String name,
                  final ExternalFile externalFile,
                  final String localId,
                  final String neighbor) {
        setLocalId(localId);
        setName(name);
        setNeighbor(neighbor);
        setSemaphore(externalFile);
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this bundle.
     * @param note
     *            the note, if any, attached to this bundle.
     */
    public Bundle(final String name,
                  final String note) {
        setName(name);
        setNote(note);
    }

    // instance member method (alphabetic)

    /**
     * Returns the local {@link Registration} identity, if any, associated with this
     * object.
     *
     * @return the local {@link Registration} identity, if any, associated with this
     *         object.
     */
    @XmlElement(name = "local_id")
    public String getLocalId() {
        return localId;
    }

    /**
     * Returns the name of this bundle.
     *
     * @return The name of this bundle.
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * Returns the identity of the remote SPADE instance, if any, who delivered the
     * file.
     *
     * @return the identity of the remote SPADE instance, if any, who delivered the
     *         file.
     */
    @XmlElement
    public String getNeighbor() {
        return neighbor;
    }

    /**
     * Returns the note, if any, attached to this bundle.
     *
     * @return the note, if any, attached to this bundle.
     */
    @XmlElement
    public String getNote() {
        return note;
    }

    /**
     * Returns the external location of the semaphore file.
     *
     * @return the external location of the semaphore file.
     */
    @XmlElement
    public ExternalFile getSemaphore() {
        return semaphore;
    }

    /**
     * Returns true if this bundle is inbound.
     *
     * @return true if this bundle is inbound.
     */
    @XmlTransient
    public boolean isInbound() {
        return null != neighbor;
    }

    /**
     * Sets the local {@link Registration} identity, if any, associated with this
     * object.
     *
     * @param localId
     *            the local {@link Registration} identity, if any, associated with
     *            this object.
     */
    public void setLocalId(String localId) {
        this.localId = localId;
    }

    /**
     * Sets the name of this bundle.
     *
     * @param name
     *            the name of this bundle.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the identity of the remote SPADE instance, if any, who delivered the
     * file.
     *
     * @param neighbor
     *            the identity of the remote SPADE instance, if any, who delivered
     *            the file.
     */
    public void setNeighbor(String neighbor) {
        this.neighbor = neighbor;
    }

    /**
     * Sets the note, if any, attached to this bundle.
     *
     * @param note
     *            the note, if any, attached to this bundle.
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * Sets the external location of the semaphore file.
     *
     * @param semaphore
     *            the external location of the semaphore file.
     */
    protected void setSemaphore(ExternalFile semaphore) {
        this.semaphore = semaphore;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
