package gov.lbl.nest.spade.rs;

import java.util.Date;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the flow of
 * data through a specific part of the application for a given slice of time.
 *
 * @author patton
 */
@XmlType(propOrder = { "whenTaken",
                       "bytes",
                       "count" })
public class Slice {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The total number of bytes that flowed during this object's slice of time.
     */
    private long bytes;

    /**
     * The number of times that the flow was updated during this object's slice of
     * time.
     */
    private int count;

    /**
     * The date and time at the end this object's slice of time.
     */
    private Date whenTaken;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Slice() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param dateTime
     *            the date and time at the end this object's slice of time.
     * @param bytes
     *            the total number of bytes that flowed during this object's slice
     *            of time.
     * @param count
     *            the number of times that the flow was updated during this object's
     *            slice of time.
     */
    public Slice(Date dateTime,
                 long bytes,
                 int count) {
        this.bytes = bytes;
        this.count = count;
        whenTaken = dateTime;
    }

    // instance member method (alphabetic)

    /**
     * Returns the total number of bytes that flowed during this object's slice of
     * time.
     *
     * @return the total number of bytes that flowed during this object's slice of
     *         time.
     */
    @XmlElement
    public long getBytes() {
        return bytes;
    }

    /**
     * Returns the number of times that the flow was updated during this object's
     * slice of time.
     *
     * @return the number of times that the flow was updated during this object's
     *         slice of time.
     */
    @XmlElement
    public int getCount() {
        return count;
    }

    /**
     * Returns the date and time at the end this object's slice of time.
     *
     * @return the date and time at the end this object's slice of time.
     */
    @XmlElement(name = "when_taken")
    public Date getWhenTaken() {
        return whenTaken;
    }

    /**
     * Sets the total number of bytes that flowed during this object's slice of
     * time.
     *
     * @param bytes
     *            the total number of bytes that flowed during this object's slice
     *            of time.
     */
    public void setBytes(long bytes) {
        this.bytes = bytes;
    }

    /**
     * Sets the number of times that the flow was updated during this object's slice
     * of time.
     *
     * @param count
     *            the number of times that the flow was updated during this object's
     *            slice of time.
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * Sets the date and time at the end this object's slice of time.
     *
     * @param dateTime
     *            the date and time at the end this object's slice of time.
     */
    public void setWhenTaken(Date dateTime) {
        whenTaken = dateTime;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
