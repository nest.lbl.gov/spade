/**
 * This package contains the classes needed to suport the RESTful interface of
 * this application.
 *
 * @author patton
 */
package gov.lbl.nest.spade.rs;