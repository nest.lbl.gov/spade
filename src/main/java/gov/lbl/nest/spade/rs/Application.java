package gov.lbl.nest.spade.rs;

import java.net.URI;
import java.util.List;

import gov.lbl.nest.common.rs.NamedResource;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class represents a user's entry point to all accessible resources in the
 * application.
 *
 * @author patton
 */
@XmlRootElement(name = "application")
@XmlType(propOrder = { "uri",
                       "status",
                       "stampers",
                       "reports",
                       "commands",
                       "specification",
                       "implementation" })
public class Application {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection of {@link ActionsGroup} instances that can be used to initiate
     * a command, via a POST.
     */
    private List<ActionsGroup> commands;

    /**
     * The implementation version of this application.
     */
    private String implementation;

    /**
     * The collection of {@link ActionsGroup} instances that can be used to return a
     * report, via a GET.
     */
    private List<ActionsGroup> reports;

    /**
     * The specification version this application implements.
     */
    private String specification;

    /**
     * The group of {@link NamedResource} instance that are the time-stampers.
     */
    private StamperGroup stampers;

    /**
     * The current status of the application.
     */
    private ExecutionStatus status;

    /**
     * The URI of this object.
     */
    private URI uri;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Application() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI of the created object.
     * @param status
     *            the current status of the application.
     * @param stampers
     *            the group of {@link NamedResource} instances that are the
     *            time-stampers.
     * @param reports
     *            the collection of {@link ActionsGroup} instances that can be used
     *            to return a report, via a GET.
     * @param commands
     *            the collection of {@link ActionsGroup} instances that can be used
     *            to initiate a command, via a POST.
     * @param specification
     *            the specification version this application implements.
     * @param implementation
     *            the implementation version of this application.
     */
    public Application(final URI uri,
                       ExecutionStatus status,
                       StamperGroup stampers,
                       List<ActionsGroup> reports,
                       List<ActionsGroup> commands,
                       String specification,
                       String implementation) {
        setCommands(commands);
        setImplementation(implementation);
        setReports(reports);
        setSpecification(specification);
        setStampers(stampers);
        setStatus(status);
        setUri(uri);
    }

    // instance member method (alphabetic)

    /**
     * Returns the collection of {@link ActionsGroup} instances that can be used to
     * initiate a command, via a POST.
     *
     * @return the collection of {@link ActionsGroup} instances that can be used to
     *         initiate a command, via a POST.
     */
    @XmlElement
    protected List<ActionsGroup> getCommands() {
        return commands;
    }

    /**
     * Returns the implementation version of this application.
     *
     * @return the implementation version of this application.
     */
    @XmlElement
    protected String getImplementation() {
        return implementation;
    }

    /**
     * Returns the collection of {@link ActionsGroup} instances that can be used to
     * return a report, via a GET.
     *
     * @return the collection of {@link ActionsGroup} instances that can be used to
     *         return a report, via a GET.
     */
    @XmlElement
    protected List<ActionsGroup> getReports() {
        return reports;
    }

    /**
     * Returns the specification version this application implements.
     *
     * @return the specification version this application implements.
     */
    @XmlElement
    protected String getSpecification() {
        return specification;
    }

    /**
     * Returns the group of {@link NamedResource} instance that are the
     * time-stampers.
     *
     * @return the group of {@link NamedResource} instance that are the
     *         time-stampers.
     */
    @XmlElement(name = "time_stampers")
    protected StamperGroup getStampers() {
        return stampers;
    }

    /**
     * Returns the current status of the application.
     *
     * @return the current status of the application.
     */
    @XmlElement
    protected ExecutionStatus getStatus() {
        return status;
    }

    /**
     * Returns the URI of this object.
     *
     * @return the URI of this object.
     */
    @XmlElement(name = "uri")
    protected URI getUri() {
        return uri;
    }

    /**
     * Sets the collection of {@link ActionsGroup} instances that can be used to
     * initiate a command, via a POST.
     *
     * @param commands
     *            the collection of {@link ActionsGroup} instances that can be used
     *            to initiate a command, via a POST.
     */
    protected void setCommands(List<ActionsGroup> commands) {
        this.commands = commands;
    }

    /**
     * Sets the implementation version of this application.
     *
     * @param implementation
     *            the implementation version of this application.
     */
    protected void setImplementation(String implementation) {
        this.implementation = implementation;
    }

    /**
     * Sets the collection of {@link ActionsGroup} instances that can be used to
     * return a report, via a GET.
     *
     * @param reports
     *            the collection of {@link ActionsGroup} instances that can be used
     *            to return a report, via a GET.
     */
    protected void setReports(List<ActionsGroup> reports) {
        this.reports = reports;
    }

    /**
     * Sets the specification version this application implements.
     *
     * @param specification
     *            the specification version this application implements.
     */
    protected void setSpecification(String specification) {
        this.specification = specification;
    }

    /**
     * Sets the group of {@link NamedResource} instance that are the time-stampers.
     *
     * @param stampers
     *            the group of {@link NamedResource} instance that are the
     *            time-stampers.
     */
    protected void setStampers(StamperGroup stampers) {
        this.stampers = stampers;
    }

    /**
     * Sets the current status of the application.
     *
     * @param status
     *            the current status of the application.
     */
    protected void setStatus(ExecutionStatus status) {
        this.status = status;
    }

    /**
     * Sets the URI of this object.
     *
     * @param uri
     *            the URI of this object.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
