package gov.lbl.nest.spade.rs;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is a container for the counts of a component.
 */
@XmlType(propOrder = { "target",
                       "executing",
                       "pending" })
public class StatusCounts {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The number of executing instances.
     */
    @XmlElement
    Integer executing;

    /**
     * The number of pending instances.
     */
    @XmlElement
    Integer pending;

    /**
     * The target number of threads available for execution.
     */
    @XmlElement
    Integer target;

    // constructors

    /**
     * Creates a instance of this class.
     *
     * @param executing
     *            the number of executing instances.
     * @param pending
     *            the number of pending instances.
     * @param target
     *            the target number of threads available for execution.
     */
    protected StatusCounts(final Integer executing,
                           final Integer pending,
                           final Integer target) {
        if (null == pending || 0 == pending.intValue()) {
            this.pending = null;
        } else {
            this.pending = pending;
        }
        if (null == target || 0 == target.intValue()) {
            this.target = null;
        } else {
            this.target = target;
        }
        if (null == executing) {
            if (null == pending && null == target) {
                // Always have at least one quantity with a value.
                this.executing = 0;
                return;
            }
        }
        this.executing = executing;
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
