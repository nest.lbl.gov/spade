package gov.lbl.nest.spade.rs;

import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendedException;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.services.InProgressException;
import gov.lbl.nest.spade.services.Spade;
import gov.lbl.nest.spade.services.TooManyTicketsException;
import gov.lbl.nest.spade.workflow.ActivityManager;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowDirector;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

/**
 * The class provides the write RESTful interface to access SPADE application
 * status.
 *
 * @author patton
 *
 * @see ReportSpade
 */
@Path("command")
@Stateless
public class CommandSpade {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CommandSpade.class);

    // private static member data

    // private instance member data

    /**
     * The {@link Spade} instance used by this object
     */
    @Inject
    private Spade spade;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected CommandSpade() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param reportSpade
     *            the {@link ReportSpade} instance used by this object.
     *
     */
    public CommandSpade(final ReportSpade reportSpade) {
        spade = reportSpade.getSpade();
    }

    // instance member method (alphabetic)

    /**
     * Requests that the any open dispatches for the supplied ticket names be
     * abandoned by this instance of the application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param tickets
     *            The set of ticket names to be abandoned.
     *
     * @return the {@link Response} generated for this request containing a
     *         {@link Tickets} instance that contains of all ticket names that where
     *         successfully abandoned.
     */
    @POST
    @Path("tickets/abandon")
    @Consumes({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response abandon(@Context UriInfo uriInfo,
                            Tickets tickets) {
        ResponseBuilder builder;
        try {
            if (spade.isSuspended()) {
                builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
                return builder.build();
            }
            final Tickets result = new Tickets(uriInfo.getRequestUri(),
                                               spade.abandon(tickets));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | SuspendedException e) {
            LOG.warn("Unblock request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to release a collection of Ticket Ingest Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to add a new thread to the specified workflow.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param workflow
     *            the name of the workflow from which to add the thread.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the application.
     */
    @POST
    @Path("workflow/{workflow}/threads/add")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response addWorkflowThread(@Context UriInfo uriInfo,
                                      @PathParam("workflow") String workflow) {
        ResponseBuilder builder;
        try {
            final WorkflowDirector workflowDirector = spade.getWorkflowDirector(Workflow.valueOf(workflow));
            workflowDirector.addThread();
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Thread addition request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to add a Thread from \"" + workflow
                      + "\" workflow");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Returns the XML representation of the detailed status of the application.
     * This call can be used to see if the client has access to the targets in
     * 'command' tree.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the XML representation of the detailed status of the application.
     */
    @POST
    @Path("authorized")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response authorized(@Context final UriInfo uriInfo) {
        return ReportSpade.createApplicationStatusReponse(uriInfo,
                                                          spade,
                                                          false);
    }

    /**
     * Attempts to confirm the delivery of the bundles from the specified neighbor.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param neighbor
     *            the neighbor from which the confirmation has been received.
     * @param digest
     *            the confirmation information from the neighbor.
     *
     * @return to be decided.
     */
    @POST
    @Path("delivery/{neighbor}")
    @Consumes({ SpadeType.DIGEST_XML,
                SpadeType.DIGEST_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ SpadeType.DIGEST_XML,
                SpadeType.DIGEST_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response delivery(@Context final UriInfo uriInfo,
                             @PathParam("neighbor") final String neighbor,
                             final Digest digest) {
        ResponseBuilder builder;
        try {
            final List<? extends DigestChange> delivered = spade.delivery(neighbor,
                                                                          digest.getIssued());
            final List<? extends DigestChange> deliveredToUse;
            if (delivered.isEmpty()) {
                deliveredToUse = null;
            } else {
                deliveredToUse = delivered;
            }
            final Digest result = new Digest(null,
                                             "",
                                             digest.getBegin(),
                                             digest.getEnd(),
                                             deliveredToUse);
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (Exception e) {
            LOG.error("Failed when attempting to confirm deliveries");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to confirm the delivery of the bundles from the specified neighbor.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return to be decided.
     */
    @POST
    @Path("application/scan/delivery")
    @Produces({ SpadeType.DIGEST_XML,
                SpadeType.DIGEST_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response deliveryScan(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            final Date now = new Date();
            final List<DigestChange> confirmed = spade.deliveryScan();
            final List<DigestChange> confirmedToUse;
            if (confirmed.isEmpty()) {
                confirmedToUse = null;
            } else {
                confirmedToUse = confirmed;
            }
            final Digest result = new Digest((uriInfo.getBaseUri()).resolve("report/timestamps/confirmed"),
                                             "",
                                             now,
                                             (Date) null,
                                             confirmedToUse);
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | InProgressException
                 | SuspendedException e) {
            LOG.warn("Delivery Scan request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to confirm deliveries");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to suspend the execution of all activities of the application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the application.
     */
    @POST
    @Path("application/drain")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response drainApplication(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            spade.drain();
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Suspend request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to suspend the execution of the specified workflow.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param workflow
     *            the name of the workflow that contains the named activity.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the named activity.
     */
    @POST
    @Path("workflow/{workflow}/drain")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response drainWorkflow(@Context UriInfo uriInfo,
                                  @PathParam("workflow") String workflow) {
        ResponseBuilder builder;
        try {
            final WorkflowDirector workflowDirector = spade.getWorkflowDirector(Workflow.valueOf(workflow));
            workflowDirector.drain();
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Suspend Activity request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Scans for new files in the local dropboxes.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("application/scan/inbound")
    @POST
    @Produces({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response inboundScan(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            final Bundles result = new Bundles(uriInfo.getRequestUri(),
                                               spade.inboundScan(uriInfo.getBaseUri()));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | InProgressException
                 | SuspendedException e) {
            LOG.warn("Inbound Scan request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to do a inbound scan");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Requests that the supplied {@link Bundle} instances be ingested by this
     * instance of the application. The application will determine the appropriate
     * registration for each Bundle. If not registration is found for a Bundle it
     * will not be included in the returned list of Bundles.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param bundles
     *            The set of {@link Bundle} instances to be processed.
     *
     * @return the {@link Response} generated for this request containing a
     *         {@link Bundles} instance that contains of all {@link Bundle}
     *         instances that matched to a registration.
     */
    @POST
    @Path("bundles/ingest")
    @Consumes({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response ingest(@Context UriInfo uriInfo,
                           Bundles bundles) {
        ResponseBuilder builder;
        try {
            if (spade.isSuspended()) {
                builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
                return builder.build();
            }
            final Bundles result = new Bundles(uriInfo.getRequestUri(),
                                               spade.ingest(bundles,
                                                            uriInfo.getBaseUri()));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | SuspendedException
                 | TooManyTicketsException e) {
            LOG.warn("Ingest request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to ingest a collection of Bundles");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Scans for new files in the local dropboxes.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("application/scan/local")
    @POST
    @Produces({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response localScan(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            final Bundles result = new Bundles(uriInfo.getRequestUri(),
                                               spade.localScan(uriInfo.getBaseUri()));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | InProgressException
                 | SuspendedException
                 | TooManyTicketsException e) {
            LOG.warn("Local Scan request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to do a local scan");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Requests that the application re-archive the specified tickets that have
     * failed to be archived before.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param tickets
     *            The collection of ticket identities to be re-shipped.
     *
     * @return the collection of ticket identities for which re-archiving is being
     *         attempted.
     */
    @POST
    @Path("tickets/rearchive")
    @Consumes({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response rearchive(@Context UriInfo uriInfo,
                              final Tickets tickets) {
        ResponseBuilder builder;
        try {
            if (spade.isSuspended()) {
                builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
                return builder.build();
            }
            final Tickets result = new Tickets(uriInfo.getRequestUri(),
                                               spade.rearchive(tickets.getTickets(),
                                                               uriInfo.getBaseUri()));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | SuspendedException e) {
            LOG.warn("Rearchive request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-archive a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Requests that the application attempt to re-dispatch the specified tickets to
     * any destination to which it has not been successfully dispatched.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param tickets
     *            The collection of ticket identities to be re-dispatched.
     *
     * @return the collection of ticket identities for which re-dispatching is being
     *         attempted.
     */
    @POST
    @Path("tickets/redispatch")
    @Consumes({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response redispatch(@Context UriInfo uriInfo,
                               final Tickets tickets) {
        ResponseBuilder builder;
        try {
            if (spade.isSuspended()) {
                builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
                return builder.build();
            }
            final Tickets result = new Tickets(uriInfo.getRequestUri(),
                                               spade.redispatch(tickets.getTickets(),
                                                                uriInfo.getBaseUri()));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | SuspendedException e) {
            LOG.warn("Redispatch request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Scans for bundles that have one or more dispatch failure and re-dispatch
     * them.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("application/scan/redispatch")
    @POST
    @Produces({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response redispatchScan(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            final URI requestUri = uriInfo.getRequestUri();
            final Tickets result = new Tickets(requestUri,
                                               spade.redispatchScan(6,
                                                                    TimeUnit.MINUTES,
                                                                    null));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | InProgressException
                 | SuspendedException e) {
            LOG.warn("Redispatch Scan request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to do a local scan");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to remove a new thread to the specified workflow.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param workflow
     *            the name of the workflow from which to removed the thread.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the application.
     */
    @POST
    @Path("workflow/{workflow}/threads/remove")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response removeWorkflowThread(@Context UriInfo uriInfo,
                                         @PathParam("workflow") String workflow) {
        ResponseBuilder builder;
        try {
            final WorkflowDirector workflowDirector = spade.getWorkflowDirector(Workflow.valueOf(workflow));
            workflowDirector.removeThread();
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Thread removal request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to remove a Thread from \"" + workflow
                      + "\" workflow");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Scans for rescue files.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param workflow
     *            the name of the workflow for which to rescue workflows.
     *
     * @return the {@link Response} generated for this request.
     */
    @Path("application/scan/rescue/{workflow}")
    @POST
    @Produces({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response rescueScan(@Context UriInfo uriInfo,
                               @PathParam("workflow") String workflow) {
        ResponseBuilder builder;
        try {
            final Bundles result = new Bundles(uriInfo.getRequestUri(),
                                               spade.rescueScan(Workflow.valueOf(workflow)));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | InProgressException
                 | SuspendedException e) {
            LOG.warn("Rescue Scan request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to do a rescue scan");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Requests that the application re-ship the specified tickets to any
     * destination to which it has not been successfully received.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param tickets
     *            The collection of ticket identities to be re-shipped.
     * @param all
     *            true if all outbound transfers should be re-dispatched, otherwise
     *            only the unconfirmed ones will be.
     *
     * @return the collection of ticket identities for which re-shipping is being
     *         attempted.
     */
    @POST
    @Path("tickets/reship")
    @Consumes({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ SpadeType.TICKETS_XML,
                SpadeType.TICKETS_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response reship(@Context UriInfo uriInfo,
                           final Tickets tickets,
                           @QueryParam("all") Boolean all) {
        ResponseBuilder builder;
        try {
            if (spade.isSuspended()) {
                builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
                return builder.build();
            }
            final Tickets result = new Tickets(uriInfo.getRequestUri(),
                                               spade.reship(tickets.getTickets(),
                                                            all,
                                                            uriInfo.getBaseUri()));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | SuspendedException e) {
            LOG.warn("Reship request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to resume the execution of the named activity.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param name
     *            the name of the activity whose should be resumed.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the named activity.
     */
    @POST
    @Path("activity/{activity}/resume")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response resumeActivity(@Context UriInfo uriInfo,
                                   @PathParam("activity") String name) {
        ResponseBuilder builder;
        try {
            final ActivityManager activityManager = spade.getActivityManager(null,
                                                                             name);
            activityManager.setSuspended(false);
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Resume Activity request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to resume the execution of the specified activity.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param workflow
     *            the name of the workflow that contains the named activity.
     * @param activity
     *            the name of the activity whose should be resumed.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the named activity.
     */
    @POST
    @Path("activity/{workflow}/{activity}/resume")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response resumeActivity(@Context UriInfo uriInfo,
                                   @PathParam("workflow") String workflow,
                                   @PathParam("activity") String activity) {
        ResponseBuilder builder;
        try {
            final ActivityManager activityManager = spade.getActivityManager(Workflow.valueOf(workflow),
                                                                             activity);
            activityManager.setSuspended(false);
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Resume Activity request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to resume the execution of all activities of the application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the application.
     */
    @POST
    @Path("application/resume")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response resumeApplication(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            spade.setSuspended(false);
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Resume request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to suspend the execution of the specified workflow.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param workflow
     *            the name of the workflow that contains the named activity.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the named activity.
     */
    @POST
    @Path("workflow/{workflow}/resume")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response resumeWorkflow(@Context UriInfo uriInfo,
                                   @PathParam("workflow") String workflow) {
        ResponseBuilder builder;
        try {
            final WorkflowDirector workflowDirector = spade.getWorkflowDirector(Workflow.valueOf(workflow));
            workflowDirector.setSuspended(false);
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Suspend Activity request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to suspend the execution of the named activity.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param activity
     *            the name of the activity whose should be suspended.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the named activity.
     */
    @POST
    @Path("activity/{activity}/suspend")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response suspendActivity(@Context UriInfo uriInfo,
                                    @PathParam("activity") String activity) {
        ResponseBuilder builder;
        try {
            final ActivityManager activityManager = spade.getActivityManager(null,
                                                                             activity);
            activityManager.setSuspended(true);
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Suspend Activity request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to suspend the execution of the specified activity.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param workflow
     *            the name of the workflow that contains the named activity.
     * @param activity
     *            the name of the activity whose should be suspended.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the named activity.
     */
    @POST
    @Path("activity/{workflow}/{activity}/suspend")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response suspendActivity(@Context UriInfo uriInfo,
                                    @PathParam("workflow") String workflow,
                                    @PathParam("activity") String activity) {
        ResponseBuilder builder;
        try {
            final ActivityManager activityManager = spade.getActivityManager(Workflow.valueOf(workflow),
                                                                             activity);
            activityManager.setSuspended(true);
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Suspend Activity request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to suspend the execution of all activities of the application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the application.
     */
    @POST
    @Path("application/suspend")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response suspendApplication(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            spade.setSuspended(true);
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Suspend request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Attempts to suspend the execution of the specified workflow.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param workflow
     *            the name of the workflow that contains the named activity.
     *
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the named activity.
     */
    @POST
    @Path("workflow/{workflow}/suspend")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response suspendWorkflow(@Context UriInfo uriInfo,
                                    @PathParam("workflow") String workflow) {
        ResponseBuilder builder;
        try {
            final WorkflowDirector workflowDirector = spade.getWorkflowDirector(Workflow.valueOf(workflow));
            workflowDirector.setSuspended(true);
            builder = Response.status(Response.Status.OK);
            builder.entity(ReportSpade.createApplicationStatus(uriInfo,
                                                               spade,
                                                               false));
        } catch (InitializingException e) {
            LOG.warn("Suspend Activity request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to re-dispatch a collection of Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    /**
     * Requests that the tickets for the supplied {@link Bundle} instances be
     * unblocked by this instance of the application.
     *
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param bundles
     *            The set of {@link Bundle} instances to be unblocked.
     *
     * @return the {@link Response} generated for this request containing a
     *         {@link Bundles} instance that contains of all {@link Bundle}
     *         instances that where unblocked.
     */
    @POST
    @Path("bundles/unblock")
    @Consumes({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    @Produces({ SpadeType.BUNDLES_XML,
                SpadeType.BUNDLES_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response unblock(@Context UriInfo uriInfo,
                            Bundles bundles) {
        ResponseBuilder builder;
        try {
            if (spade.isSuspended()) {
                builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
                return builder.build();
            }
            final Bundles result = new Bundles(uriInfo.getRequestUri(),
                                               spade.unblock(bundles));
            builder = Response.status(Response.Status.OK);
            builder.entity(result);
        } catch (InitializingException
                 | SuspendedException e) {
            LOG.warn("Unblock request was ignored because:" + e.getMessage());
            builder = Response.status(Response.Status.SERVICE_UNAVAILABLE);
        } catch (Exception e) {
            LOG.error("Failed when attempting to release a collection of Bundle Ingest Tickets");
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
