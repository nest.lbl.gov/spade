package gov.lbl.nest.spade.rs;

import java.net.URI;
import java.util.Date;
import java.util.List;

import gov.lbl.nest.common.watching.DigestChange;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the time
 * evolution of the flow of data through a specific part of the application.
 *
 * @author patton
 */
@XmlRootElement(name = "flow")
@XmlType(propOrder = { "uri",
                       "timestamp",
                       "subject",
                       "slices" })
public class Flow {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The sequence of {@link Slice} instance that collectively make up this object.
     */
    private List<? extends Slice> slices;

    /**
     * The part of the application to which this object refers.
     */
    private String subject;

    /**
     * The date and time this object was created.
     */
    private Date timestamp;

    /**
     * The URI that will refresh the client representation of this this object.
     */
    private URI uri;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected Flow() {
    }

    /**
     * Create an instance of this class.
     *
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     * @param slices
     *            the sequence of {@link Slice} instance that collectively make up
     *            this object.
     */
    public Flow(URI uri,
                String subject,
                List<? extends Slice> slices) {
        setSlices(slices);
        setSubject(subject);
        setTimestamp(new Date());
        setUri(uri);
    }

    // instance member method (alphabetic)

    /**
     * Returns the sequence of {@link Slice} instance that collectively make up this
     * object.
     *
     * @return the sequence of {@link Slice} instance that collectively make up this
     *         object.
     */
    @XmlElement(name = "slice")
    @XmlElementWrapper(name = "slices")
    public List<? extends Slice> getSlices() {
        return slices;
    }

    /**
     * Returns the subject to which the {@link DigestChange} instances refer.
     *
     * @return the subject to which the {@link DigestChange} instances refer.
     */
    @XmlElement
    public String getSubject() {
        return subject;
    }

    /**
     * Returns the date and time this object was created.
     *
     * @return the date and time this object was created.
     */
    @XmlElement
    protected Date getTimestamp() {
        return timestamp;
    }

    /**
     * Returns the URI that will refresh the client representation of this this
     * object.
     *
     * @return the URI that will refresh the client representation of this this
     *         object.
     */
    @XmlElement
    protected URI getUri() {
        return uri;
    }

    /**
     * Sets the sequence of {@link Slice} instance that collectively make up this
     * object.
     *
     * @param slices
     *            the sequence of {@link Slice} instance that collectively make up
     *            this object.
     */
    protected void setSlices(List<? extends Slice> slices) {
        this.slices = slices;
    }

    /**
     * Sets the subject to which the {@link DigestChange} instances refer.
     *
     * @param subject
     *            the subject to which the {@link DigestChange} instances refer.
     */
    protected void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Sets the date and time this object was created.
     *
     * @param dateTime
     *            the date and time this object was created.
     */
    protected void setTimestamp(Date dateTime) {
        this.timestamp = dateTime;
    }

    /**
     * Sets the URI that will refresh the client representation of this this object.
     *
     * @param uri
     *            the URI that will refresh the client representation of this this
     *            object.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    /**
     * Returns the JSON representation of this object.
     *
     * @param columwise
     *            true if the results should be presented for a column series chart.
     *
     * @return the JSON representation of this object.
     */
    public String toJSON(boolean columwise) {
        final StringBuilder sb = new StringBuilder("[");
        sb.append("[");
        sb.append("\"" + uri
                  + "\",");
        sb.append(Long.toString(timestamp.getTime()) + ",");
        sb.append("\"" + subject
                  + "\"");
        sb.append("],");

        if (columwise) {
            final StringBuilder whenTaken = new StringBuilder("[null");
            final StringBuilder size = new StringBuilder("[\"size\"");
            final StringBuilder files = new StringBuilder("[\"files\"");
            for (Slice slice : slices) {
                whenTaken.append("," + Long.toString(((slice.getWhenTaken()).getTime())));
                size.append("," + Long.toString(slice.getBytes()));
                files.append("," + Integer.toString(slice.getCount()));
            }
            whenTaken.append("],");
            sb.append(whenTaken);
            size.append("],");
            sb.append(size);
            files.append("]");
            sb.append(files);
        } else {
            String conjunction = "";
            for (Slice slice : slices) {
                sb.append(conjunction + "[");
                sb.append(Long.toString(((slice.getWhenTaken()).getTime())));
                sb.append("," + (Long.toString(slice.getBytes())));
                sb.append("," + (Integer.toString(slice.getCount())));
                sb.append("]");
                conjunction = ",";
            }
        }
        sb.append("]");
        return sb.toString();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
