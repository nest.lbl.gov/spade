package gov.lbl.nest.spade.rs;

import java.net.URI;
import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the current
 * status of the application.
 *
 * @author patton
 */
@XmlRootElement(name = "application_status")
@XmlType(propOrder = { "counts",
                       "activities" })
public class ApplicationStatus extends
                               ExecutionStatus {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    private static StatusCounts createApplicationCounts(Integer threadLimit,
                                                        Integer threadCount,
                                                        Integer pendingCount,
                                                        List<ActivityStatus> activities) {
        int target = 0;
        int executing = 0;
        int pending = 0;
        for (ActivityStatus activity : activities) {
            final StatusCounts counts = activity.getCounts();
            if (null == threadLimit) {
                final Integer countsTarget = counts.target;
                if (null != countsTarget) {
                    target = countsTarget;
                }
            }
            if (null == threadCount) {
                final Integer countsExecuting = counts.executing;
                if (null != countsExecuting) {
                    executing += countsExecuting;
                }
            }
            if (null == pendingCount) {
                final Integer countsPending = counts.pending;
                if (null != countsPending) {
                    pending += countsPending;
                }
            }
        }

        int targetToUse;
        if (null == threadLimit) {

            targetToUse = target;
        } else {
            targetToUse = threadLimit.intValue();
        }
        int executingToUse;
        if (null == threadCount) {
            executingToUse = executing;
        } else {
            executingToUse = threadCount.intValue();
        }
        int pendingingToUse;
        if (null == pendingCount) {
            pendingingToUse = pending;
        } else {
            pendingingToUse = pendingCount.intValue();
        }

        return new StatusCounts(executingToUse,
                                pendingingToUse,
                                targetToUse);
    }

    /**
     * The statuses of all of the activities that make up the application.
     */
    private List<ActivityStatus> activities;

    // constructors

    /**
     * The {@link StatusCounts} for this activity.
     */
    private StatusCounts counts;

    /**
     * Creates an instance of this class.
     */
    protected ApplicationStatus() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the he application.
     * @param condition
     *            the current execution state of the application.
     */
    public ApplicationStatus(URI uri,
                             String name,
                             Condition condition) {
        super(uri,
              name,
              condition,
              true);
    }

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the he application.
     * @param condition
     *            the current execution state of the application.
     * @param activities
     *            the statuses of all of the activities that make up the
     *            application.
     * @param threadLimit
     *            the maximum number of threads for the application, if known. If
     *            <code>null</code>, this value will be derived from the sum of
     *            individual activities.
     * @param executingCount
     *            the number of active threads for the application, if known. If
     *            <code>null</code>, this value will be derived from the sum of
     *            individual activities.
     * @param pendingCount
     *            the number of instances waiting for an active thread, it known. If
     *            <code>null</code>, this value will be derived from the sum of
     *            individual activities.
     */
    public ApplicationStatus(URI uri,
                             String name,
                             Condition condition,
                             List<ActivityStatus> activities,
                             Integer threadLimit,
                             Integer executingCount,
                             Integer pendingCount) {
        super(uri,
              name,
              condition,
              null);
        this.activities = activities;
        counts = createApplicationCounts(threadLimit,
                                         executingCount,
                                         pendingCount,
                                         activities);
    }

    /**
     * Returns the statuses of all of the activities that make up the application.
     *
     * @return the statuses of all of the activities that make up the application.
     */
    @XmlElement(name = "activity")
    public List<ActivityStatus> getActivities() {
        return activities;
    }

    /**
     * Returns the {@link StatusCounts} for this activity.
     *
     * @return the {@link StatusCounts} for this activity.
     */
    @XmlElement
    protected StatusCounts getCounts() {
        return counts;
    }

    /**
     * Sets the statuses of all of the activities that make up the application.
     *
     * @param activities
     *            the statuses of all of the activities that make up the
     *            application.
     */
    protected void setActivities(List<ActivityStatus> activities) {
        this.activities = activities;
    }

    // static member methods (alphabetic)

    /**
     * Sets the {@link StatusCounts} for this activity.
     *
     * @param counts
     *            the {@link StatusCounts} for this activity.
     */
    protected void setCounts(StatusCounts counts) {
        this.counts = counts;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
