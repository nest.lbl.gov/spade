package gov.lbl.nest.spade.rs;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

/**
 * The RESTful access to the SPADE application.
 *
 * @author patton
 */
@ApplicationPath("local")
public class SpadeApplication extends
                              Application {
    // No content
}
