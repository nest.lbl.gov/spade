package gov.lbl.nest.spade.rs;

import java.io.StringWriter;
import java.net.URI;
import java.util.Collection;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, a group zero,
 * one or more ticket identities.
 *
 * @author patton
 */
@XmlRootElement(name = "tickets")
@XmlType(propOrder = { "uri",
                       "tickets" })
public class Tickets {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The collection of the ticket identities constitute this object.
     */
    private Collection<String> tickets;

    /**
     * The URI, if any, that is related to the content of this object.
     */
    private URI uri;

    // constructors

    // instance member method (alphabetic)

    /**
     * Create an instance of this class.
     */
    protected Tickets() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param url
     *            the URI, if any, that is related to the content of this object.
     * @param tickets
     *            the collection of the ticket identities constitute this object.
     */
    public Tickets(URI url,
                   Collection<String> tickets) {
        setTickets(tickets);
        setUri(url);
    }

    /**
     * Creates an instance of this class.
     *
     * The new instance will contain the same {@link Collection} as the original (so
     * a change in one will be a change in the other), but the new instance has a
     * different URL.
     *
     * @param url
     *            the URI, if any, that is related to the content of this object.
     * @param rhs
     *            the {@link Tickets} instance whose collection will be shared.
     */
    public Tickets(URI url,
                   Tickets rhs) {
        this(url,
             rhs.getTickets());
    }

    /**
     * Returns the collection of the ticket identities constitute this object.
     *
     * @return the collection of the ticket identities constitute this object.
     */
    @XmlElement(name = "ticket")
    public Collection<String> getTickets() {
        return tickets;
    }

    /**
     * Returns the URI, if any, that is related to the content of this object.
     *
     * @return the URI, if any, that is related to the content of this object.
     */
    @XmlElement
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the collection of the ticket identities constitute this object.
     *
     * @param tickets
     *            the collection of the ticket identities constitute this object.
     */
    protected void setTickets(Collection<String> tickets) {
        this.tickets = tickets;
    }

    /**
     * Sets the URI, if any, that is related to the content of this object.
     *
     * @param uri
     *            the URI, if any, that is related to the content of this object.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    @Override
    public String toString() {
        JAXBContext content;
        try {
            content = JAXBContext.newInstance(Tickets.class);
            final Marshaller marshaller = content.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT,
                                   true);
            final StringWriter writer = new StringWriter();
            marshaller.marshal(this,
                               writer);
            return writer.toString();
        } catch (JAXBException e) {
            return super.toString();
        }
    }

    // public static void main(String args[]) {}
}
