package gov.lbl.nest.spade.workflow.impl;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.CollectableSuspendable;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.spade.workflow.WorkflowCollection;
import gov.lbl.nest.spade.workflow.WorkflowDirector;

/**
 * This class manages the collected {@link WorkflowDirector} instances for the
 * application.
 *
 * @author patton
 *
 */
public class WorkflowCollectionImpl extends
                                    WorkflowCollection {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(WorkflowCollectionImpl.class);

    // private static member data

    // private instance member data

    /**
     * The mapping of {@link WorkflowDirector} instances to {@link Workflow}
     * enumeration.
     */
    private Map<Workflow, WorkflowDirector> workflowDirectors = new HashMap<>();

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param suspendable
     *            the {@link Suspendable} instance this created instance should use
     *            as the collection of {@link Suspendable} instances's
     *            {@link Suspendable}.
     * @param file
     *            the file, if any, containing the set of names of suspended
     *            {@link CollectableSuspendable} instances.
     */
    public WorkflowCollectionImpl(final Suspendable suspendable,
                                  final File file) {
        super(suspendable,
              file);
    }

    // instance member method (alphabetic)

    @Override
    public void addWorkflowDirector(Workflow workflow,
                                    WorkflowDirector workflowDirector) throws InitializingException {
        final WorkflowDirector director = workflowDirector;
        director.setCollection(this);
        if (workflowDirectors.containsKey(workflow)) {
            throw new IllegalArgumentException("There has already been an WorkflowDirector created for \"" + workflow.toString()
                                               + "\"");
        }
        workflowDirectors.put(workflow,
                              director);
    }

    @Override
    public Collection<? extends CollectableSuspendable> getCollectableSuspendables() {
        return workflowDirectors.values();
    }

    @Override
    public WorkflowDirector getWorkflowDirector(Workflow workflow) {
        return workflowDirectors.get(workflow);
    }

    @Override
    public boolean isWorkflowSuspended(final Workflow workflow) throws InitializingException {
        return isSuspended(workflow.toString());
    }

    @Override
    public void stop() {
        for (Workflow workflow : workflowDirectors.keySet()) {
            final WorkflowDirector director = getWorkflowDirector(workflow);
            director.pause();
            LOG.info("Stopping " + workflow);
        }
        for (Workflow workflow : workflowDirectors.keySet()) {
            final WorkflowDirector director = getWorkflowDirector(workflow);
            final AtomicInteger executionCount = director.getExecutingCount();
            while (0 != executionCount.get()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            LOG.info(workflow + " stopped");
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
