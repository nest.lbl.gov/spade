/**
 * The package contains the classes generic implementation of interfaces from
 * the "workflow" package.
 *
 * @author patton
 */
package gov.lbl.nest.spade.workflow.impl;