package gov.lbl.nest.spade.workflow;

import java.io.File;
import java.util.Collection;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.CollectableSuspendable;
import gov.lbl.nest.common.suspension.FileSuspendedWhileRunning;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.suspension.SuspendableCollection;
import gov.lbl.nest.common.suspension.SuspendableCollectionImpl;

/**
 * This class manages the collected {@link WorkflowDirector} instances for the
 * application.
 *
 * @author patton
 *
 */
public abstract class WorkflowCollection implements
                                         SuspendableCollection {

    /**
     * This enumerates the known workflows that can be loaded.
     *
     * @author patton
     *
     */
    public enum Workflow {

                          /**
                           * The workflow that ingests Bundles.
                           */
                          INGEST,

                          /**
                           * The workflow that re-ships bundles.
                           */
                          RESHIP,

                          /**
                           * The workflow that re-archives bundles.
                           */
                          REARCHIVE
    }

    // public static final member data

    private class WorkflowDirectorCollection extends
                                             SuspendableCollectionImpl {

        WorkflowDirectorCollection(final Suspendable suspendable,
                                   final File file) {
            super(suspendable,
                  new FileSuspendedWhileRunning(file));
        }

        @Override
        public Collection<? extends CollectableSuspendable> getCollectableSuspendables() {
            return WorkflowCollection.this.getCollectableSuspendables();
        }
    }

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link SuspendableCollection} instance to which this Object's
     * implementation is delegated.
     */
    private final SuspendableCollection delegateeCollection;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param suspendable
     *            the {@link Suspendable} instance this created instance should use
     *            as the collection of {@link Suspendable} instances's
     *            {@link Suspendable}.
     * @param file
     *            the file, if any, containing the set of names of suspended
     *            {@link CollectableSuspendable} instances.
     */
    protected WorkflowCollection(Suspendable suspendable,
                                 File file) {
        delegateeCollection = new WorkflowDirectorCollection(suspendable,
                                                             file);
    }

    // instance member method (alphabetic)

    /**
     * Adds a new {@link WorkflowDirector} instance to this Object.
     *
     * @param workflow
     *            The {@link Workflow} instance whose {@link WorkflowDirector} is
     *            being added.
     * @param workflowDirector
     *            the {@link WorkflowDirector} instance to add.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    public abstract void addWorkflowDirector(Workflow workflow,
                                             WorkflowDirector workflowDirector) throws InitializingException;

    @Override
    public String getName() {
        return delegateeCollection.getName();
    }

    /**
     * Returns the {@link WorkflowDirector} used by the specified workflow.
     *
     * @param workflow
     *            the {@link Workflow} whose {@link WorkflowDirector} should be
     *            returned.
     *
     * @return the {@link WorkflowDirector} used by the specified workflow.
     */
    public abstract WorkflowDirector getWorkflowDirector(Workflow workflow);

    @Override
    public boolean isSuspended() throws InitializingException {
        return delegateeCollection.isSuspended();
    }

    @Override
    public boolean isSuspended(String name) throws InitializingException {
        return delegateeCollection.isSuspended(name);
    }

    /**
     * Returns <code>true</code> if the supplied {@link Workflow} instance is
     * suspended.
     *
     * @param workflow
     *            the {@link Workflow} instance to test.
     *
     * @return <code>true</code> if the supplied {@link Workflow} instance is
     *         suspended.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    public abstract boolean isWorkflowSuspended(final Workflow workflow) throws InitializingException;

    @Override
    public void setCollection(SuspendableCollection collection) {
        delegateeCollection.setCollection(collection);
    }

    @Override
    public boolean setSuspended(boolean suspend) throws InitializingException {
        return delegateeCollection.setSuspended(suspend);
    }

    /**
     * Stops the execution of all workflows. Those tasks already running will
     * complete, but no new ones will be able to start.
     */
    protected abstract void stop();

    @Override
    public void update(CollectableSuspendable suspendable) throws InitializingException {
        delegateeCollection.update(suspendable);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
