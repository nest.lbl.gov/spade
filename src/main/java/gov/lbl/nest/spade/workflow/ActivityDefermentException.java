package gov.lbl.nest.spade.workflow;

import java.net.URI;

/**
 * The exception is thrown when an Activity wants to defer its execution until a
 * specified condition is met.
 *
 * @author patton
 */
public class ActivityDefermentException extends
                                        RuntimeException {

    /**
     * Required by Serializable interface.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The {@link ActivityDefermentKey} to be used to match the message signaling
     * that deferred execution can proceed.
     */
    private ActivityDefermentKey key;

    /**
     * The {@link URI}, if any, that will recommence the execution of a flow node
     * that is correlated with the supplied {@link ActivityDefermentException}.
     *
     */
    private final URI uri;

    /**
     * Creates an instance of this class.
     *
     * @param uri
     *            the {@link URI}, if any, that will recommence the execution of a
     *            flow node that is correlated with the supplied
     *            {@link ActivityDefermentException}.
     */
    public ActivityDefermentException(final URI uri) {
        this.uri = uri;
    }

    /**
     * Returns the {@link ActivityDefermentKey} to be used to match the message
     * signaling that deferred execution can proceed.
     *
     * @return the {@link ActivityDefermentKey} to be used to match the message
     *         signaling that deferred execution can proceed.
     */
    public ActivityDefermentKey getKey() {
        return key;
    }

    /**
     * Returns the {@link URI}, if any, that will recommence the execution of a flow
     * node that is correlated with the supplied {@link ActivityDefermentException}.
     *
     * @return the {@link URI}, if any, that will recommence the execution of a flow
     *         node that is correlated with the supplied
     *         {@link ActivityDefermentException}.
     */
    public URI getUri() {
        return uri;
    }

    /**
     * Sets the {@link ActivityDefermentKey} to be used to match the message
     * signaling that deferred execution can proceed.
     *
     * @param key
     *            the {@link ActivityDefermentKey} to be used to match the message
     *            signaling that deferred execution can proceed.
     */
    public void setKey(ActivityDefermentKey key) {
        this.key = key;
    }
}
