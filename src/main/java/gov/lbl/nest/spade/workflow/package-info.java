/**
 * The package contains the interface and classes that define the API for
 * installing a workflow system into this application.
 *
 * @author patton
 */
package gov.lbl.nest.spade.workflow;