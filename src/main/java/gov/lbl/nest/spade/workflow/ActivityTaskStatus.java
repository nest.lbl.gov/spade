package gov.lbl.nest.spade.workflow;

import jakarta.xml.bind.annotation.XmlEnum;

/**
 * This interface is used to access information about a single task of an
 * Activity.
 *
 * @author patton
 */
public interface ActivityTaskStatus {

    /**
     * This enumerates the possible conditions of the bundle this Object is
     * representing within the context of the associated activity.
     *
     * @author patton
     */
    @XmlEnum(String.class)
    public enum STATE {
                       /**
                        * This state means that the associated activity is currently processing the
                        * bundle this Object is representing.
                        */
                       EXECUTING,

                       /**
                        * This state means that the associated activity is currently waiting to process
                        * the bundle this Object is representing.
                        */
                       PENDING,

                       /**
                        * This state means that the associated activity has processed the bundle this
                        * Object is representing.
                        */
                       COMPLETE,
    }

    /**
     * Returns the name of the bundle this Object is representing.
     *
     * @return the name of the bundle this Object is representing.
     */
    String getBundle();

    /**
     * Returns the current condition of the bundle this Object is representing
     * within the context of the associated activity.
     *
     * @return the current condition of the bundle this Object is representing
     *         within the context of the associated activity.
     */
    STATE getState();
}
