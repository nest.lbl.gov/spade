package gov.lbl.nest.spade.workflow;

import java.net.URI;
import java.util.List;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;

/**
 * This interface is used by the default {@link WorkflowsManager} class to
 * access the loaded workflows.
 *
 * @author patton
 */
public interface LoadedWorkflows {

    /**
     * Adds the supplied {@link WorkflowTermination} to the specified workflow.
     *
     * @param workflow
     *            the workflow to which the {@link WorkflowTermination} should be
     *            added.
     * @param termination
     *            the {@link WorkflowTermination} to be added.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    void addWorkflowTermination(Workflow workflow,
                                WorkflowTermination termination) throws InitializingException;

    /**
     * Allow all workflows to "drain", by suspending the work themselves, so they
     * will reject any more attempts to start a new instance, but resuming all
     * activities in the workflows.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    void drain() throws InitializingException;

    /**
     * Returns the names of all activities in the specified workflow.
     *
     * @param workflow
     *            the workflow of all activities in the specified workflow.
     *
     * @return the names of all known tasks.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    List<? extends String> getActivityNames(Workflow workflow) throws InitializingException;

    /**
     * Returns the {@link WorkflowDirector} used by the specified workflow.
     *
     * @param workflow
     *            the {@link Workflow} whose {@link WorkflowDirector} should be
     *            returned.
     *
     * @return the {@link WorkflowDirector} used by the specified workflow.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    WorkflowDirector getWorkflowDirector(Workflow workflow) throws InitializingException;

    /**
     * Returns true when this object is suspended.
     *
     * @return true when this object is suspended.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    boolean isSuspended() throws InitializingException;

    /**
     * Recommences the execution of the deferred activity instance that correlates
     * to the supplied {@link URI}.
     *
     * @param uri
     *            the {@link URI} which refers to the deferred activity instance.
     * @param message
     *            the message to be used by the deferred activity instance when it
     *            recommences.
     */
    void recommence(URI uri,
                    Object message);

    /**
     * Recovers and restarted any workflows that failed earlier but have been
     * rescued and so can continue.
     *
     * @param workflow
     *            the {@link Workflow} instance for which to rescue workflows.
     *
     * @return the collection of {@link String} instances containing the labels of
     *         the rescued workflows.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    List<String> rescueWorkflow(Workflow workflow) throws InitializingException;

    /**
     * Sets whether execution of this object should be suspended or not.
     *
     * @param suspended
     *            True when execution of this object should be suspended.
     *
     * @return true if the state of this object has changed.
     */
    boolean setSuspended(boolean suspended);

    /**
     * Starts the execution of a new workflow.
     *
     * @param workflow
     *            the {@link Workflow} for which a new execution to start.
     * @param name
     *            the name to give the new execution instance.
     * @param priority
     *            the priority to give the new execution instance.
     * @param dataObject
     *            the value of the Data Object that is the ingest ticket for the new
     *            execution instance.
     *
     * @return <code>true</code> if the new instance successfully starts.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    boolean startWorkflow(Workflow workflow,
                          String name,
                          Integer priority,
                          Object dataObject) throws InitializingException;

}
