package gov.lbl.nest.spade.workflow.impl;

import java.net.URI;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.spade.lazarus.LoadedLazarusWorkflows;
import gov.lbl.nest.spade.workflow.ActivityManager;
import gov.lbl.nest.spade.workflow.ActivityMonitor;
import gov.lbl.nest.spade.workflow.LoadedWorkflows;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowDirector;
import gov.lbl.nest.spade.workflow.WorkflowTermination;
import gov.lbl.nest.spade.workflow.WorkflowsManager;
import gov.lbl.nest.spade.workflow.WorkflowsMonitor;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

/**
 * This class implements the {@link WorkflowsManager} interface using the
 * {@link LoadedWorkflows}.
 *
 * @author patton
 */
@Stateless
public class WorkflowsManagerImpl implements
                                  WorkflowsManager,
                                  WorkflowsMonitor {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link LoadedLazarusWorkflows} instance used by this object.
     */
    @Inject
    private LoadedWorkflows loadedWorkflows;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected WorkflowsManagerImpl() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param loadedWorkflows
     *            the {@link LoadedWorkflows} instance used by this object.
     */
    public WorkflowsManagerImpl(final LoadedWorkflows loadedWorkflows) {
        this.loadedWorkflows = loadedWorkflows;
    }

    // instance member method (alphabetic)

    @Override
    public void drain(final Workflow workflow) throws InitializingException {
        if (null == workflow) {
            loadedWorkflows.drain();
        }
        WorkflowDirector director = loadedWorkflows.getWorkflowDirector(workflow);
        if (null == director) {
            return;
        }
        director.drain();
    }

    @Override
    public ActivityManager getActivityManager(final Workflow workflow,
                                              final String activity) throws InitializingException {
        if (null == workflow) {
            for (Workflow flow : Workflow.values()) {
                final WorkflowDirector director = loadedWorkflows.getWorkflowDirector(flow);
                if (null != director) {
                    final ActivityManager result = director.getActivityManager(activity);
                    if (null != result) {
                        return result;
                    }
                }
            }
            return null;
        }
        final WorkflowDirector director = loadedWorkflows.getWorkflowDirector(workflow);
        if (null == director) {
            return null;
        }
        return director.getActivityManager(activity);
    }

    private ActivityMonitor getActivityMonitor(final String activity) throws InitializingException {
        for (Workflow flow : Workflow.values()) {
            final WorkflowDirector director = loadedWorkflows.getWorkflowDirector(flow);
            if (null != director) {
                final ActivityMonitor result = director.getActivityMonitor(activity);
                if (null != result) {
                    return result;
                }
            }
        }
        return null;
    }

    @Override
    public ActivityMonitor getActivityMonitor(final Workflow workflow,
                                              final String activity) throws InitializingException {
        if (null == workflow) {
            return getActivityMonitor(activity);
        }
        final WorkflowDirector director = loadedWorkflows.getWorkflowDirector(workflow);
        if (null == director) {
            return null;
        }
        return director.getActivityMonitor(activity);
    }

    private List<String> getActivityNames() throws InitializingException {
        final List<String> result = new ArrayList<>();
        for (Workflow flow : Workflow.values()) {
            result.addAll(loadedWorkflows.getActivityNames(flow));
        }
        return result;
    }

    @Override
    public List<? extends String> getActivityNames(Workflow workflow) throws InitializingException {
        if (null == workflow) {
            return getActivityNames();
        }
        return loadedWorkflows.getActivityNames(workflow);
    }

    @Override
    public AggregateExecution getAggregateExecution() throws InitializingException {
        final boolean applicationSuspended = loadedWorkflows.isSuspended();
        final Iterator<String> iterator = getActivityNames().iterator();
        while (iterator.hasNext()) {
            final ActivityMonitor activityManager = getActivityMonitor(iterator.next());
            if (applicationSuspended != activityManager.isSuspended()) {
                if (applicationSuspended) {
                    return AggregateExecution.PARTIALLY_SUSPENDED;
                }
                return AggregateExecution.PARTIALLY_RUNNING;
            }
        }
        if (applicationSuspended) {
            return AggregateExecution.SUSPENDED;
        }
        return AggregateExecution.RUNNING;
    }

    @Override
    public AtomicInteger getCount(final Workflow workflow) throws InitializingException {
        if (null == workflow) {
            throw new NullPointerException("Used the getCount method with no arguments");
        }
        final WorkflowDirector director = getWorkflowDirector(workflow);
        return director.getCount();
    }

    @Override
    public Integer getExecutingCount() throws InitializingException {
        int result = 0;
        for (Workflow flow : Workflow.values()) {
            final AtomicInteger executionCount = (loadedWorkflows.getWorkflowDirector(flow)).getExecutingCount();
            if (null != executionCount) {
                result += executionCount.get();
            }
        }
        return result;
    }

    @Override
    public Integer getPendingCount() throws InitializingException {
        int result = 0;
        for (Workflow flow : Workflow.values()) {
            final Integer pendingCount = (loadedWorkflows.getWorkflowDirector(flow)).getPendingCount();
            if (null != pendingCount) {
                result += pendingCount;
            }
        }
        return result;
    }

    @Override
    public Integer getThreadLimit() throws InitializingException {
        int result = 0;
        for (Workflow flow : Workflow.values()) {
            final Integer threadLimit = (loadedWorkflows.getWorkflowDirector(flow)).getThreadLimit();
            if (null != threadLimit) {
                result += threadLimit;
            }
        }
        return result;
    }

    @Override
    public WorkflowDirector getWorkflowDirector(Workflow workflow) throws InitializingException {
        return loadedWorkflows.getWorkflowDirector(workflow);
    }

    @Override
    public WorkflowsMonitor getWorkflowsMonitor() {
        return this;
    }

    @Override
    public boolean isSuspended(final Workflow workflow) throws InitializingException {
        if (null == workflow) {
            return loadedWorkflows.isSuspended();
        }
        return (loadedWorkflows.getWorkflowDirector(workflow)).isSuspended();
    }

    @Override
    public void recommence(URI uri,
                           Object message) {
        loadedWorkflows.recommence(uri,
                                   message);
    }

    @Override
    public List<String> rescueWorkflows(final Workflow workflow) throws InitializingException {
        return loadedWorkflows.rescueWorkflow(workflow);
    }

    @Override
    public boolean setSuspended(final Workflow workflow,
                                final boolean suspended) throws InitializingException {
        if (null == workflow) {
            return loadedWorkflows.setSuspended(suspended);
        }
        WorkflowDirector director;
        try {
            director = loadedWorkflows.getWorkflowDirector(workflow);
        } catch (InitializingException e) {
            return true;
        }
        return director.setSuspended(suspended);
    }

    @Override
    public void setTermination(final Workflow workflow,
                               final WorkflowTermination termination) throws InitializingException {
        loadedWorkflows.addWorkflowTermination(workflow,
                                               termination);
    }

    @Override
    public boolean startWorkflow(final Workflow workflow,
                                 String name,
                                 Integer priority,
                                 final Object dataObject) throws InitializingException {
        return loadedWorkflows.startWorkflow(workflow,
                                             name,
                                             priority,
                                             dataObject);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
