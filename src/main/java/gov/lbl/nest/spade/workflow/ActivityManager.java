package gov.lbl.nest.spade.workflow;

import gov.lbl.nest.common.suspension.Suspendable;

/**
 * This interface is used to monitor an individual activity within the SPADE
 * workflows.
 *
 * (This interface separates SPADE from the underlying workflow implmentation.)
 *
 * @author patton
 */
public interface ActivityManager extends
                                 ActivityMonitor,
                                 Suspendable {
    // Simply combines two interfaces into one.
}
