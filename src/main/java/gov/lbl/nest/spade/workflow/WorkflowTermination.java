package gov.lbl.nest.spade.workflow;

/**
 * This interface is used to execute a termination method when a workflow
 * finishes.
 *
 * @author patton
 */
public interface WorkflowTermination {

    /**
     * This method is invoked when the workflow does not ends successfully.
     *
     * @param ticket
     *            the resulting ticket of the workflow.
     * @param name
     *            the name of the task that failed.
     * @param rescue
     *            the Object that can be used to rescue a failed workflow once its
     *            issue has been resolved.
     * @param t
     *            the {@link Throwable} instance that cause the workflow to fail.
     *
     * @return true if the internal structures of the instance should be preserved
     *         so it can be rescued.
     *
     */
    boolean failed(final Object ticket,
                   final String name,
                   final Object rescue,
                   final Throwable t);

    /**
     * This method is invoked when the workflow ends successfully.
     *
     * @param ticket
     *            the resulting ticket of the workflow.
     */
    void succeeded(Object ticket);

}
