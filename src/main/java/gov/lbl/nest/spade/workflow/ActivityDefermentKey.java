package gov.lbl.nest.spade.workflow;

/**
 * This interface is used to correlate messages with deferred executions.
 *
 * @author patton
 */
public interface ActivityDefermentKey extends
                                      Comparable<ActivityDefermentKey> {

    /**
     * Returns a string representing this object that can be placed in a URI.
     *
     * @return a string representing this object that can be placed in a URI.
     */
    String forURI();
}
