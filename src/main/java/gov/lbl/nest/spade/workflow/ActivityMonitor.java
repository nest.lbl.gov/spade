package gov.lbl.nest.spade.workflow;

import java.util.List;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * This interface is used to monitor an individual activity within the SPADE
 * workflows.
 *
 * (This interface separates SPADE from the underlying workflow implementation.)
 *
 * @author patton
 */
public interface ActivityMonitor {

    /**
     * Returns the collection of {@link ActivityTaskStatus} instances currently
     * associated with the activity instance this Object is monitoring.
     *
     * @return the collection of {@link ActivityTaskStatus} instances currently
     *         associated with the activity instance this Object is monitoring.
     */
    List<ActivityTaskStatus> getActivityTaskStatuses();

    /**
     * Returns the number of executing activities that this object controls.
     *
     * @return the number of executing activities that this object controls.
     */
    int getExecutingCount();

    /**
     * Returns the target number of {@link Thread} instances that can execute
     * activities that this object controls, or <code>null</code> if this object can
     * not control that.
     *
     * @return the target number of {@link Thread} instances that can execute
     *         activities that this object controls.
     */
    Integer getMaximumThreadCount();

    /**
     * Returns the name of the activity this Object is monitoring.
     *
     * @return the name of the activity this Object is monitoring.
     */
    String getName();

    /**
     * Returns the number of pending activities that this object controls.
     *
     * @return the number of pending activities that this object controls.
     */
    int getPendingCount();

    /**
     * Returns true when this object is suspended.
     *
     * @return true when this object is suspended.
     *
     * @throws InitializingException
     *             when this object is still initializing.
     */
    boolean isSuspended() throws InitializingException;
}