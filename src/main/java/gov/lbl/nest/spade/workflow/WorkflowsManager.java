package gov.lbl.nest.spade.workflow;

import java.net.URI;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;

/**
 * This interface is used to manage the execution of internal SPADE workflows.
 *
 * @author patton
 */
public interface WorkflowsManager {

    /**
     * Allow the specified workflow to "drain", by suspending the workflow itself,
     * so it will reject any more attempts to start a new instance, but resuming all
     * activities in the workflow.
     *
     * @param workflow
     *            the workflow that should be drained, <code>null</code> means all
     *            workflows should be drained.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    void drain(final Workflow workflow) throws InitializingException;

    /**
     * Returns the {@link ActivityManager} for the named task, or <code>null</code>
     * if the task does not exist.
     *
     * @param workflow
     *            the workflow that contains the named activity, <code>null</code>
     *            returns the first activity that matches the specified name for any
     *            workflow.
     * @param activity
     *            the name of the task whose {@link ActivityManager} should be
     *            returned.
     *
     * @return the {@link ActivityManager} for the named task, or <code>null</code>
     *         if the task does not exist.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    ActivityManager getActivityManager(Workflow workflow,
                                       String activity) throws InitializingException;

    /**
     * Returns the {@link ActivityMonitor} for the named task, or <code>null</code>
     * if the task does not exist.
     *
     * @param workflow
     *            the workflow that contains the named activity, <code>null</code>
     *            returns the first activity that matches the specified name for any
     *            workflow.
     * @param activity
     *            the name of the task whose {@link ActivityMonitor} should be
     *            returned.
     *
     * @return the {@link ActivityMonitor} for the named task, or <code>null</code>
     *         if the task does not exist.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    ActivityMonitor getActivityMonitor(Workflow workflow,
                                       String activity) throws InitializingException;

    /**
     * Returns the names of all activities in the specified workflow.
     *
     * @param workflow
     *            the workflow of all activities in the specified workflow,
     *            <code>null</code> returns all the activities from all workflows.
     *
     * @return the names of all known tasks.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    List<? extends String> getActivityNames(Workflow workflow) throws InitializingException;

    /**
     * Returns an {@link AtomicInteger} containing the number of current active
     * instances of the specified workflow.
     *
     * The returned {@link AtomicInteger} instance can be queried at any time. Also
     * it is notified every time this value changes so it can be used to detect
     * changes in this value by invoking its 'wait' method in a suitable loop.
     *
     * @param workflow
     *            the workflow whose counts should be returned.
     *
     * @return the {@link AtomicInteger} containing the number of current active
     *         instances of the specified workflow.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    AtomicInteger getCount(Workflow workflow) throws InitializingException;

    /**
     * Returns the {@link WorkflowDirector} that is directing the specified
     * {@link Workflow}.
     *
     * @param workflow
     *            the {@link Workflow} whose {@link WorkflowDirector} instance
     *            should be returned.
     *
     * @return the {@link WorkflowDirector} that is directing the specified
     *         {@link Workflow}.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    WorkflowDirector getWorkflowDirector(Workflow workflow) throws InitializingException;

    /**
     * Returns the {@link WorkflowsMonitor} that is monitoring all workflows.
     *
     * @return the {@link WorkflowsMonitor} that is monitoring all workflows.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    WorkflowsMonitor getWorkflowsMonitor() throws InitializingException;

    /**
     * Returns <code>true</code> when the specified workflow is suspended, or
     * <code>false</code> if it is running.
     *
     * @param workflow
     *            the workflow whose suspended status should be returned,
     *            <code>null</code> returns <code>true</code> only is all workflows
     *            are running.
     *
     * @return <code>true</code> when the specified workflow is suspended, or
     *         <code>false</code> if it is running.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    boolean isSuspended(final Workflow workflow) throws InitializingException;

    /**
     * Recommences the execution of the deferred activity instance that correlates
     * to the supplied {@link URI}.
     *
     * @param uri
     *            the {@link URI} which refers to the deferred activity instance.
     * @param message
     *            the message to be used by the deferred activity instance when it
     *            recommences.
     */
    void recommence(final URI uri,
                    final Object message);

    /**
     * Recovers and restarted any workflows for the specified {@link Workflow}
     * instance that failed earlier but have been rescued and so can continue.
     *
     * @param workflow
     *            the {@link Workflow} instance for which to rescue workflows.
     *
     * @return the collection of {@link String} instances containing the labels of
     *         the rescued workflows.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    List<String> rescueWorkflows(final Workflow workflow) throws InitializingException;

    /**
     * Set whether the ingestion workflow should be suspended or not.
     *
     * @param workflow
     *            the workflow whose suspended status should be set,
     *            <code>null</code> sets the specified value for all workflows.
     * @param suspended
     *            <code>true</code> if the specified workflow should be suspended,
     *            or <code>false</code> if it should be running.
     *
     * @return true if the state of this Object has changed.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    boolean setSuspended(final Workflow workflow,
                         final boolean suspended) throws InitializingException;

    /**
     * Set the {@link WorkflowTermination} instance to be executed when the
     * specified workflow terminates.
     *
     * @param workflow
     *            the workflow whose {@link WorkflowTermination} instance should be
     *            set, <code>null</code> sets the specified
     *            {@link WorkflowTermination} instance for all workflows.
     * @param termination
     *            the {@link WorkflowTermination} instance to be executed when the
     *            specified workflow terminates.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    void setTermination(final Workflow workflow,
                        final WorkflowTermination termination) throws InitializingException;

    /**
     * Starts the requested workflow with the specified Data Object instance.
     *
     * @param workflow
     *            the workflow that will be start.
     * @param name
     *            the name of assign to this execution of the workflow
     * @param priority
     *            the priority of this workflow with respect to others.
     * @param dataObject
     *            the Data Object instance describing the file to be ingested.
     *
     * @return true is the workflow has successfully started.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    boolean startWorkflow(final Workflow workflow,
                          String name,
                          Integer priority,
                          final Object dataObject) throws InitializingException;
}
