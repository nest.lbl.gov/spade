package gov.lbl.nest.spade.workflow;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendableCollection;

/**
 * This abstract class is used to director the execution of a single workflow.
 *
 * @author patton
 */
public interface WorkflowDirector extends
                                  SuspendableCollection {

    /**
     * Increases the maximum number of threads for the application, if known. If not
     * known <code>null</code> will be returned.
     *
     * @return the new maximum number of threads for the application, if known. If
     *         not known <code>null</code> will be returned.
     */
    Integer addThread();

    /**
     * Allow this workflow to "drain", by suspending the itself, so it will reject
     * any more attempts to start a new instance, but resuming all activities in the
     * workflow.
     *
     * @throws InitializingException
     *             when this Object is still initializing.
     */
    void drain() throws InitializingException;

    /**
     * Returns the {@link ActivityManager} for the named activity, or
     * <code>null</code> if the task does not exist.
     *
     * @param activity
     *            the name of the task whose {@link ActivityManager} should be
     *            returned.
     *
     * @return the {@link ActivityManager} for the named task, or <code>null</code>
     *         if the task does not exist.
     */
    ActivityManager getActivityManager(String activity);

    /**
     * Returns the {@link ActivityMonitor} for the named activity, or
     * <code>null</code> if the task does not exist.
     *
     * @param activity
     *            the name of the task whose {@link ActivityMonitor} should be
     *            returned.
     *
     * @return the {@link ActivityMonitor} for the named task, or <code>null</code>
     *         if the task does not exist.
     */
    ActivityMonitor getActivityMonitor(String activity);

    /**
     * Return the name of all of the BPMN activities in this object's control.
     *
     * @return the name of all of the BPMN activities in this object's control.
     */
    List<? extends String> getActivityNames();

    /**
     * Returns the number of workflow instances that this object is currently
     * directing.
     *
     * @return the number of workflow instances that this object is currently
     *         directing.
     */
    AtomicInteger getCount();

    /**
     * Returns the number of executing tasks within this object.
     *
     * @return the number of executing tasks within this object.
     */
    AtomicInteger getExecutingCount();

    /**
     * Returns the number of task instances waiting for an active thread.
     *
     * @return the number of task instances waiting for an active thread.
     */
    Integer getPendingCount();

    /**
     * Returns the number of active threads for the application, if known. If
     * <code>null</code>, this value will be derived from the sum of individual
     * activities.
     *
     * @return the maximum number of threads for the application, if known. If not
     *         known <code>null</code> will be returned.
     */
    Integer getThreadLimit();

    /**
     * Temporarily stop the consumption of tasks.
     *
     * @return true when the consumption of tasks has been paused.
     */
    boolean pause();

    /**
     * Continue the consumption of tasks after it has been temporarily stopped.
     *
     * @return true when the consumption of tasks is no longer paused.
     */
    boolean proceed();

    /**
     * Decreases the maximum number of threads for the application, if known. If not
     * known <code>null</code> will be returned. That number can not go below zero.
     *
     * @return the new maximum number of threads for the application, if known. If
     *         not known <code>null</code> will be returned.
     */
    Integer removeThread();

    /**
     * Sets the {@link AtomicInteger} containing the number of workflow instances
     * that this object is currently directing.
     *
     * @param count
     *            the {@link AtomicInteger} containing the number of workflow
     *            instances that this object is currently directing. active.
     */
    void setCount(AtomicInteger count);
}
