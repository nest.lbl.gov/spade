package gov.lbl.nest.spade.workflow;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * This interface is used to monitor a single workflow.
 *
 * @author patton
 */
public interface WorkflowsMonitor {

    /**
     * The set of possible states for a the workflow.
     *
     * @author patton
     */
    enum AggregateExecution {
                             /**
                              * In this state the component is running and none of its constituent components
                              * have been suspended.
                              */
                             RUNNING,

                             /**
                              * In this state the component is running and one of more of its constituent
                              * components have been suspended.
                              */
                             PARTIALLY_RUNNING,

                             /**
                              * In this state the component has been suspended but one of more of its
                              * constituent components are running.
                              */
                             PARTIALLY_SUSPENDED,

                             /**
                              * In this state the component has been suspended and none of its constituent
                              * components are running.
                              */
                             SUSPENDED
    }

    /**
     * Returns the {@link AggregateExecution} instance describing the current
     * execution state of all workflows.
     *
     * @return the {@link AggregateExecution} instance describing the current
     *         execution state of all workflows.
     *
     * @throws InitializingException
     *             when the application is still initializing
     */
    AggregateExecution getAggregateExecution() throws InitializingException;

    /**
     * Returns the number of executing tasks within this object.
     *
     * @return the number of executing tasks within this object.
     * 
     * @throws InitializingException
     *             when the application is still initializing
     */
    Integer getExecutingCount() throws InitializingException;

    /**
     * Returns the number of {@link Exception} instances waiting for an active
     * thread.
     *
     * @return the number of {@link Exception} instances waiting for an active
     *         thread.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Integer getPendingCount() throws InitializingException;

    /**
     * Returns the maximum number of threads for the application, if known. If
     * <code>null</code>, this value will be derived from the sum of individual
     * activities.
     *
     * @return the maximum number of threads for the application, if known. If
     *         <code>null</code>, this value will be derived from the sum of
     *         individual activities.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Integer getThreadLimit() throws InitializingException;
}
