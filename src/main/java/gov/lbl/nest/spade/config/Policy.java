package gov.lbl.nest.spade.config;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.configure.InitParam;
import gov.lbl.nest.spade.interfaces.policy.ImplementedPolicy;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class describes an Policy that a SPADE Activity will use.
 *
 * @author patton
 */
@XmlType(propOrder = { "policyClass",
                       "parameters" })
public class Policy {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The canonical name of the class used by this {@link Activity} to define its
     * policy.
     */
    private String policyClass;

    /**
     * The {@link ImplementedPolicy} instance used by this {@link Activity} to
     * define its policy.
     */
    private ImplementedPolicy implementedPolicy;

    /**
     * The initialization parameters, is any, for this policy.
     */
    private List<InitParam> parameters;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Policy() {
    }

    /**
     * Creates an instance of this class.
     */
    Policy(final String className) {
        setPolicyClass(className);
    }

    // instance member method (alphabetic)

    /**
     * Returns the value of the named parameter is specified, otherwise
     * <code>null</code>.
     *
     * @param name
     *            the name of the parameter to return.
     *
     * @return the value of the named parameter is specified, otherwise
     *         <code>null</code>.
     */
    public String getParameter(final String name) {
        final List<InitParam> params = getParameters();
        if (null != params) {
            for (InitParam param : params) {
                if (name.equals(param.getName())) {
                    return param.getValue();
                }
            }
        }
        return null;
    }

    /**
     * Returns the initialization parameters, is any, for this activity.
     *
     * @return the initialization parameters, is any, for this activity.
     */
    @XmlElement(name = "init-param")
    protected List<InitParam> getParameters() {
        return parameters;
    }

    /**
     * Returns the list of values of the named parameter is specified.
     *
     * @param name
     *            the name of the parameter to return.
     *
     * @return the value of the named parameter is specified.
     */
    public List<String> getParameters(final String name) {
        final List<InitParam> params = getParameters();
        final List<String> result = new ArrayList<>();
        if (null != params) {
            for (InitParam param : params) {
                if (name.equals(param.getName())) {
                    result.add(param.getValue());
                }
            }
        }
        return result;
    }

    /**
     * Returns the {@link ImplementedPolicy} instance used by this {@link Activity}
     * to define its policy.
     *
     * @param clazz
     *            the {@link Class} of the object to be returned.
     *
     * @return the {@link ImplementedPolicy} instance used by this {@link Activity}
     *         to define its policy.
     */
    @SuppressWarnings("unchecked")
    <T extends ImplementedPolicy> T getPolicy(Class<T> clazz) {
        try {
            if (null == implementedPolicy) {
                final ClassLoader loader = getClass().getClassLoader();
                try {
                    final Class<ImplementedPolicy> loadedClass = (Class<ImplementedPolicy>) loader.loadClass(policyClass);
                    Constructor<ImplementedPolicy> constructor;
                    try {
                        constructor = loadedClass.getConstructor(Policy.class);
                        try {
                            implementedPolicy = constructor.newInstance(this);
                        } catch (InvocationTargetException e) {
                            throw new IllegalArgumentException("Constructor of class \"" + policyClass
                                                               + "\" can not be invoked with its Activity as an argument");
                        }
                    } catch (SecurityException e) {
                        throw new IllegalArgumentException("Constructor of class \"" + policyClass
                                                           + "\" can not be accessed with its Activity as an argument");
                    } catch (NoSuchMethodException e) {
                        implementedPolicy = loadedClass.getConstructor()
                                                       .newInstance();
                    }
                } catch (InvocationTargetException e) {
                    throw new IllegalArgumentException("Constrcutor of \"" + policyClass
                                                       + "\" failed");
                } catch (NoSuchMethodException e) {
                    throw new IllegalArgumentException("No default constructor for \"" + policyClass
                                                       + "\"");
                } catch (ClassNotFoundException e) {
                    throw new IllegalArgumentException("No such class as \"" + policyClass
                                                       + "\"");
                } catch (InstantiationException e) {
                    throw new IllegalArgumentException("Can not instantiate class \"" + policyClass
                                                       + "\"");
                } catch (IllegalAccessException e) {
                    throw new IllegalArgumentException("Constructor of class \"" + policyClass
                                                       + "\" can not be accessed");
                }
            }
            return (T) implementedPolicy;
        } catch (ClassCastException e) {
            return null;
        }
    }

    /**
     * Returns the canonical name of the class used by this {@link Activity} to
     * define its policy.
     *
     * @return the canonical name of the class used by this {@link Activity} to
     *         define its policy.
     */
    @XmlElement(name = "class",
                required = true)
    protected String getPolicyClass() {
        return policyClass;
    }

    /**
     * Sets the initialization parameters, is any, for this activity.
     *
     * @param params
     *            the initialization parameters, is any, for this activity.
     */
    protected void setParameters(List<InitParam> params) {
        parameters = params;
    }

    /**
     * Sets the canonical name of the class used by this {@link Activity} to define
     * its policy.
     *
     * @param className
     *            the canonical name of the class used by this {@link Activity} to
     *            define its policy.
     */
    protected void setPolicyClass(String className) {
        if (null != policyClass && null != className
            && policyClass.equals(className)) {
            return;
        }
        implementedPolicy = null;
        policyClass = className;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
