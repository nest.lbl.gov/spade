package gov.lbl.nest.spade.config;

import jakarta.xml.bind.annotation.XmlEnum;

/**
 * This enumerates the possible execution states of a component of the
 * application.
 *
 * @author patton
 */
@XmlEnum(String.class)
public enum ExecutionMode {
                           /**
                            * In this state the application both schedules and executes the workflows.
                            */
                           STANDALONE,

                           /**
                            * In this state the application schedules but does not execute the workflows.
                            */
                           SCHEDULER,

                           /**
                            * In this state the application only executes workflows.
                            */
                           EXECUTION,

}
