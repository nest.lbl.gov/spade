package gov.lbl.nest.spade.config;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.PersistentSuspension;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.spade.SpadeSuspension;
import jakarta.ejb.AccessTimeout;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.inject.Inject;

/**
 * This class states whether the Application has been suspended or not.
 *
 * @author patton
 */
@Singleton
@SpadeSuspension
@AccessTimeout(value = 60,
               unit = TimeUnit.MINUTES)
@jakarta.ejb.Lock(value = LockType.READ)
public class ApplicationSuspension implements
                                   Suspendable {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance of this application.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link Lock} instance used to make sure that loading the initial state of
     * the Application is serialized.
     */
    private final Lock lock = new ReentrantLock();

    /**
     * The {@link Suspendable} instance containing the application's state.
     */
    private Suspendable suspendable;

    // constructors

    /**
     * Creates a new instance of this class.
     */
    protected ApplicationSuspension() {
    }

    /**
     * Creates a new instance of this class.
     *
     * @param configuration
     *            the {@link Configuration} instance of this application.
     * @param initialState
     *            the initial state of the Application have been loaded.
     *
     * @throws InitializingException
     *             when the application is still initializing.
     */
    public ApplicationSuspension(Configuration configuration,
                                 boolean initialState) throws InitializingException {
        this.configuration = configuration;
        loadInitialState();
        suspendable.setSuspended(initialState);
    }

    // instance member method (alphabetic)

    @Override
    public String getName() {
        loadInitialState();
        return suspendable.getName();
    }

    @Override
    public boolean isSuspended() throws InitializingException {
        loadInitialState();
        return suspendable.isSuspended();
    }

    /**
     * Loads the initial state of the Application.
     */
    private void loadInitialState() {
        if (null == suspendable) {
            lock.lock();
            try {
                if (null == suspendable) {
                    final String name = (configuration.getAssembly()).getName();
                    suspendable = new PersistentSuspension(name,
                                                           Configuration.getSuspendFile(configuration));
                }
            } finally {
                lock.unlock();
            }
        }
    }

    @Override
    @jakarta.ejb.Lock(value = LockType.WRITE)
    public boolean setSuspended(boolean suspend) throws InitializingException {
        loadInitialState();
        return suspendable.setSuspended(suspend);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
