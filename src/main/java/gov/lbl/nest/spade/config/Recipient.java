package gov.lbl.nest.spade.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import gov.lbl.nest.spade.services.NotificationManager.Scope;
import jakarta.mail.Address;
import jakarta.mail.internet.AddressException;
import jakarta.mail.internet.InternetAddress;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class contains the address of one or more recipients and the scope of
 * notifications to be sent to them.
 *
 * @author patton
 */
@XmlType(propOrder = { "addresses",
                       "scopes" })
public class Recipient {

    /**
     * The collection of {@link Address} instances to which this Object should send
     * notifications
     */
    private Collection<String> addresses;

    /**
     * The collection of {@link Address} instances to which this Object should send
     * notifications
     */
    private List<InternetAddress> addressesAsInternet;

    /**
     * The Collection of {@link Scope} instances for which the notification should
     * be send by this Object.
     */
    private Collection<Scope> scopes;

    /**
     * Returns the collection of {@link Address} instances to which this Object
     * should send notifications
     *
     * @return the collection of {@link Address} instances to which this Object
     *         should send notifications
     */
    @XmlElement(name = "address")
    public Collection<String> getAddresses() {
        return addresses;
    }

    /**
     * Returns the collection of target {@link InternetAddress} instances.
     *
     * @return the collection of target {@link InternetAddress} instances.
     */
    @XmlTransient
    public Collection<InternetAddress> getInternetAddresses() {
        if (null == addressesAsInternet) {
            if (null != addresses) {
                addressesAsInternet = new ArrayList<>();
                for (String address : addresses) {
                    try {
                        addressesAsInternet.add(new InternetAddress(address));
                    } catch (AddressException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        }
        return addressesAsInternet;
    }

    /**
     * Returns the Collection of {@link Scope} instances for which the notification
     * should be send by this Object.
     *
     * @return the Collection of {@link Scope} instances for which the notification
     *         should be send by this Object.
     */
    @XmlElement(name = "scope")
    public Collection<Scope> getScopes() {
        return scopes;
    }

    /**
     * Sets the collection of {@link Address} instances to which this Object should
     * send notifications
     *
     * @param addresses
     *            the collection of {@link Address} instances to which this Object
     *            should send notifications
     */
    protected void setAddresses(Collection<String> addresses) {
        addressesAsInternet = null;
        this.addresses = addresses;
    }

    /**
     * Sets the Collection of {@link Scope} instances for which the notification
     * should be send by this Object.
     *
     * @param scopes
     *            the Collection of {@link Scope} instances for which the
     *            notification should be send by this Object.
     */
    protected void setScopes(Collection<Scope> scopes) {
        this.scopes = scopes;
    }
}
