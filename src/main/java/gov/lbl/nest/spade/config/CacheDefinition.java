package gov.lbl.nest.spade.config;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.spade.registry.ExternalLocation;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class contains the definition of an application's cache.
 *
 * @author patton
 */
@XmlType(propOrder = { "minimum",
                       "cacheDirectories" })
public class CacheDefinition extends
                             RootedPathDefinition {

    // public static final member data

    /**
     * The default directory to use for the Cache.
     */
    public static final File DEFAULT_CACHE_DIR = new File("cache");

    /**
     * The directory to contain postponed tickets.
     */
    public static final String POSTPONED = "postponed";

    /**
     * The directory to contain postponed tickets.
     */
    public static final String PROBLEM = "problems";

    /**
     * The directory to contain files that may need to be reshipped.
     */
    public static final String RESHIP = "reship";

    /**
     * The directory that contains files that need to be re-archived.
     */
    public static final String SPOOL = "spool";

    /**
     * The directory to contain confirmed tickets.
     */
    public static final String UNCONFIRMED = "unconfirmed";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The default directories for the cache.
     */
    @SuppressWarnings("serial")
    static final List<String> DEFAULT_CACHE = new ArrayList<>() {
        {
            add(Activity.ARCHIVER);
            add(Activity.DUPLICATOR);
            add(Activity.COMPRESSOR);
            add(Activity.EXPANDER);
            add(Activity.FETCHER);
            add(Activity.PACKER);
            add(PROBLEM);
            add(POSTPONED);
            add(Activity.RECEIVER);
            add(RESHIP);
            add(Activity.SHIPPER);
            add(SPOOL);
            add(UNCONFIRMED);
            add(Activity.UNPACKER);
        }
    };

    // private static member data

    // private instance member data

    /**
     * The list of directories that make up the cache for this application.
     */
    private List<CacheDirectory> cacheDirectories;

    /**
     * The map of directories that make up the cache for this application.
     */
    private Map<String, File> cacheMap;

    /**
     * The minimum free disk space that should be available for the cache.
     */
    private DiskSpace diskSpace;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected CacheDefinition() {
    }

    /**
     * Create an instance of this class.
     *
     * @param minimum
     *            the minimum free disk space that should be available for the
     *            cache.
     */
    public CacheDefinition(final DiskSpace minimum) {
        this("cache",
             minimum);
    }

    /**
     * Create an instance of this class.
     *
     * @param root
     *            the path to the root directory of the cache, either an absolute
     *            path or one relative to the working directory of the application.
     * @param minimum
     *            the minimum free disk space that should be available for the
     *            cache.
     */
    public CacheDefinition(final String root,
                           final DiskSpace minimum) {
        super(DEFAULT_CACHE_DIR.getPath());
        setMinimum(minimum);
    }

    // instance member method (alphabetic)

    /**
     * Returns the list of directories that make up the cache for this application.
     *
     * @return the list of directories that make up the cache for this application.
     */
    @XmlElement(name = "directory")
    protected List<CacheDirectory> getCacheDirectories() {
        if (null == cacheDirectories) {
            cacheDirectories = new ArrayList<>();
        }
        return cacheDirectories;
    }

    /**
     * Returns the map of directories that make up the cache for this application.
     *
     * @return the map of directories that make up the cache for this application.
     */
    public Map<String, File> getCacheMap() {
        if (null == cacheMap) {
            loadCacheMap();
        }
        return cacheMap;
    }

    /**
     * Returns the minimum free disk space that should be available for the cache.
     *
     * @return the minimum free disk space that should be available for the cache.
     */
    @XmlElement
    public DiskSpace getMinimum() {
        return diskSpace;
    }

    /**
     * Returns the path to the root directory of the cache resolved for any leading
     * "~".
     *
     * @return the path to the root directory of the cache resolved for any leading
     *         "~".
     */
    @XmlTransient
    public String getResolvedRoot() {
        return ExternalLocation.resolvePath(getRootPath());
    }

    /**
     * Loads the cache map when any of it dependencies change.
     */
    private void loadCacheMap() {
        if (null == cacheMap) {
            cacheMap = new HashMap<>();
        } else {
            cacheMap.clear();
        }
        final String resolvedRoot = getResolvedRoot();

        // Fill with default values
        for (String key : DEFAULT_CACHE) {
            cacheMap.put(key,
                         new File(resolvedRoot,
                                  key));
        }
        // Overrider with custom ones.
        for (CacheDirectory directory : getCacheDirectories()) {
            final File path = new File(directory.getPath());
            final File fileToUse;
            if (path.isAbsolute()) {
                fileToUse = path;
            } else {
                fileToUse = new File(resolvedRoot,
                                     directory.getPath());
            }
            cacheMap.put(directory.getName(),
                         fileToUse);
        }
        for (String directory : cacheMap.keySet()) {
            final File file = cacheMap.get(directory);
            file.mkdirs();
        }
    }

    /**
     * Sets the list of directories that make up the cache for this application.
     *
     * @param directories
     *            the list of directories that make up the cache for this
     *            application.
     */
    protected void setCacheDirectories(List<CacheDirectory> directories) {
        cacheDirectories = directories;
        loadCacheMap();
    }

    /**
     * Sets the minimum free disk space that should be available for the cache.
     *
     * @param space
     *            the minimum free disk space that should be available for the
     *            cache.
     */
    protected void setMinimum(final DiskSpace space) {
        diskSpace = space;
    }

    @Override
    protected void setRootPath(String path) {
        super.setRootPath(path);
        loadCacheMap();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
