package gov.lbl.nest.spade.config;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.XmlValue;

/**
 * This class contains the definition of an application's cache.
 *
 * @author patton
 */
@XmlType()
public class ThreadLimit {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The maximum number of threads to be associated with the workflow.
     */
    private Integer limit;

    /**
     * The name of the workflow, if any, to which this Object relates.
     */
    private String workflow;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the maximum number of threads to be associated with the workflow.
     *
     * @return the maximum number of threads to be associated with the workflow.
     */
    @XmlValue
    public Integer getLimit() {
        return limit;
    }

    /**
     * Returns the name of the workflow, if any, to which this Object relates.
     *
     * @return the name of the workflow, if any, to which this Object relates.
     */
    @XmlAttribute
    public String getWorkflow() {
        return workflow;
    }

    /**
     * Sets the maximum number of threads to be associated with the workflow.
     *
     * @param limit
     *            the maximum number of threads to be associated with the workflow.
     */
    protected void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * Sets the name of the workflow, if any, to which this Object relates.
     *
     * @param workflow
     *            the name of the workflow, if any, to which this Object relates.
     */
    protected void setWorkflow(String workflow) {
        this.workflow = workflow;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
