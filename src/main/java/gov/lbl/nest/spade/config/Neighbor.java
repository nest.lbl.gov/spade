package gov.lbl.nest.spade.config;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;

/**
 * Information about a SPADE deployment that makes up part of the neighborhood.
 *
 * @author patton
 */
public class Neighbor {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name by which this instance is know to the neighboring SPADE deployment.
     * If this is <code>null</code> then the name of the assembly will by used,
     */
    private String knownAs;

    /**
     * The name of the neighboring SPADE deployment.
     */
    private String name;

    /**
     * The set of URLs that have been declared for this instance.
     */
    private Urls urls;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Neighbor() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of the neighboring SPADE deployment.
     */
    public Neighbor(final String name) {
        setName(name);
    }

    // instance member method (alphabetic)

    /**
     * Returns the String for the RESTful API URL.
     *
     * @return the String for the RESTful API URL.
     */
    @XmlTransient
    public String getApiUrl() {
        if (null == urls) {
            return null;
        }
        return urls.getApiUrl();
    }

    /**
     * Returns the String containing, if any, the Watcher Role to be used in
     * conjunction with the confirmableUrl.
     *
     * @return the String containing, if any, the Watcher Role to be used in
     *         conjunction with the confirmableUrl.
     */
    @XmlTransient
    public String getConfirmableRole() {
        if (null == urls) {
            return null;
        }
        return urls.getConfirmableRole();
    }

    /**
     * Returns the String from which to build a request for a neighbors
     * confirmables.
     *
     * @return the String from which to build a request for a neighbors
     *         confirmables.
     */
    @XmlTransient
    public String getConfirmablesUrl() {
        if (null == urls) {
            return null;
        }
        return urls.getConfirmablesUrl();
    }

    /**
     * Returns the name by which this instance is know to the neighboring SPADE
     * deployment.
     *
     * @return the name by which this instance is know to the neighboring SPADE
     *         deployment.
     */
    @XmlElement(name = "known_as")
    public String getKnownAs() {
        return knownAs;
    }

    /**
     * Returns the name of the neighboring SPADE deployment.
     *
     * @return the name of the neighboring SPADE deployment.
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * Returns the set of URLs that have been declared for this instance.
     *
     * @return the set of URLs that have been declared for this instance.
     */
    @XmlElement
    protected Urls getUrls() {
        return urls;
    }

    /**
     * Sets the name by which this instance is know to the neighboring SPADE
     * deployment.
     *
     * @param knownAs
     *            the name by which this instance is know to the neighboring SPADE
     *            deployment.
     */
    protected void setKnownAs(String knownAs) {
        this.knownAs = knownAs;
    }

    /**
     * Sets the email used by the SPADE deployment.
     *
     * @param name
     *            the name of the neighboring SPADE deployment.
     */
    protected void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the set of URLs that have been declared for this instance.
     *
     * @param urls
     *            the set of URLs that have been declared for this instance.
     */
    protected void setUrls(Urls urls) {
        this.urls = urls;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
