/**
 * This package contains the classes used to configure a SPADE application.
 *
 * @author patton
 */
package gov.lbl.nest.spade.config;