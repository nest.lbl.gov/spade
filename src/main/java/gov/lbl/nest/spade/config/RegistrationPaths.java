package gov.lbl.nest.spade.config;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class contains the locations of the local and inbound registration
 * trees.
 *
 * @author patton
 */
@XmlType(propOrder = { "local",
                       "inbound" })
public class RegistrationPaths {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The location of the directory, below which can be found all local
     * registrations.
     */
    private String inbound;

    /**
     * The location of the directory, below which can be found all inbound
     * registrations.
     */
    private String local;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected RegistrationPaths() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param local
     *            the location of the directory, below which can be found all local
     *            registrations.
     * @param inbound
     *            the location of the directory, below which can be found all
     *            inbound registrations.
     */
    public RegistrationPaths(String local,
                             String inbound) {
        setInbound(inbound);
        setLocal(local);
    }
    // instance member method (alphabetic)

    /**
     * Returns the location of the directory, below which can be found all inbound
     * registrations.
     *
     * @return the location of the directory, below which can be found all inbound
     *         registrations.
     */
    @XmlElement
    public String getInbound() {
        return inbound;
    }

    /**
     * Returns the location of the directory, below which can be found all local
     * registrations.
     *
     * @return the location of the directory, below which can be found all local
     *         registrations.
     */
    @XmlElement
    public String getLocal() {
        return local;
    }

    /**
     * Sets the location of the directory, below which can be found all inbound
     * registrations.
     *
     * @param location
     *            the location of the directory, below which can be found all
     *            inbound registrations.
     */
    protected void setInbound(String location) {
        inbound = location;
    }

    /**
     * Sets the location of the directory, below which can be found all local
     * registrations.
     *
     * @param location
     *            the location of the directory, below which can be found all local
     *            registrations.
     */
    protected void setLocal(String location) {
        local = location;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
