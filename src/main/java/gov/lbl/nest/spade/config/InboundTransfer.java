package gov.lbl.nest.spade.config;

import java.util.Collection;

import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.registry.InboundTransferRef;
import gov.lbl.nest.spade.registry.internal.InboundLocator;
import gov.lbl.nest.spade.registry.internal.InboundRegistration;
import gov.lbl.nest.spade.registry.internal.InboundSemaphore;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class describes how a bundle will be delivered from a specified origin.
 *
 * @author patton
 */
@XmlType(propOrder = { "location" })
public class InboundTransfer extends
                             ManagedTransfer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * Binds this object to the specified InboundTransfer
     * 
     * @param registration
     *            the registration to be bound.
     * @param transfers
     *            the Collection of inbound transfers to search.
     *
     * @return the inbound transfer to which this object is bound, of
     *         <code>null</code>.
     */
    public static InboundTransfer bind(final InboundRegistration registration,
                                       final Collection<InboundTransfer> transfers) {
        final InboundTransferRef referenceToUse = registration.getInboundReference();
        final InboundTransfer transfer = findInboundTransfer(referenceToUse.getName(),
                                                             transfers);
        if (null != transfer) {
            final String remoteId = referenceToUse.getRemoteId();
            final String remoteIdToUsed;
            if (null == remoteId) {
                remoteIdToUsed = "[^" + InboundLocator.SEPARATOR
                                 + "]*";
            } else {
                remoteIdToUsed = remoteId;
            }
            registration.bind(transfer.getNeighbor(),
                              referenceToUse,
                              transfer.getReceivedLocation(),
                              InboundSemaphore.getRegistrationPattern(remoteIdToUsed));
        }
        return transfer;
    }

    /**
     * Finds the inbound transfer that matches the supplied name.
     *
     * @param name
     *            the name of the inbound transfer to find.
     * @param transfers
     *            the Collection of inbound transfers to search.
     *
     * @return
     */
    private static InboundTransfer findInboundTransfer(final String name,
                                                       final Collection<InboundTransfer> transfers) {
        if (null != transfers) {
            for (InboundTransfer transfer : transfers) {
                if (name.equals(transfer.getName())) {
                    return transfer;
                }
            }
        }
        return null;
    }

    // constructors

    /**
     * The default {@link InboundRegistration} instance for this transfer.
     */
    private InboundRegistration defaultRegistration;

    /**
     * The {@link ExternalLocation} specifying the directory in which received files
     * will be found.
     */
    private ExternalLocation receivedLocation;

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     */
    protected InboundTransfer() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param name
     *            the name with which to refer to this object.
     * @param description
     *            the short description of this transfer.
     * @param neighbor
     *            the identity of the application at the other end of the transfer.
     * @param location
     *            the string specifying the {@link ExternalLocation} to where files
     *            will be received.
     */
    public InboundTransfer(final String name,
                           final String description,
                           final String neighbor,
                           final String location) {
        super(name,
              description,
              neighbor);
        setLocation(location);
        defaultRegistration = new InboundRegistration("." + getName()
                                                      + ".",
                                                      getNeighbor(),
                                                      new InboundTransferRef(getName()),
                                                      getReceivedLocation());
    }

    /**
     * Returns the default {@link InboundRegistration} instance for this transfer.
     *
     * @return the default {@link InboundRegistration} instance for this transfer.
     */
    @XmlTransient
    public InboundRegistration getDefaultInboundRegistration() {
        if (null == defaultRegistration) {
            defaultRegistration = new InboundRegistration("." + getName()
                                                          + ".",
                                                          getNeighbor(),
                                                          new InboundTransferRef(getName()),
                                                          getReceivedLocation());
        }
        return defaultRegistration;
    }

    /**
     * Returns the string specifying the {@link ExternalLocation} to where files
     * will be received.
     *
     * @return the string specifying the {@link ExternalLocation} to where files
     *         will be received.
     */
    @XmlElement
    public String getLocation() {
        return receivedLocation.toString();
    }

    // static member methods (alphabetic)

    /**
     * Returns the {@link ExternalLocation} specifying the directory in which
     * received files will be found.
     *
     * @return the {@link ExternalLocation} specifying the directory in which
     *         received files will be found.
     */
    @XmlTransient
    public ExternalLocation getReceivedLocation() {
        return receivedLocation;
    }

    /**
     * Sets the string specifying the {@link ExternalLocation} to where files will
     * be received.
     *
     * @param location
     *            the string specifying the {@link ExternalLocation} to where files
     *            will be received.
     */
    protected void setLocation(final String location) {
        receivedLocation = new ExternalLocation(location);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
