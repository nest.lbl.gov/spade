package gov.lbl.nest.spade.config;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class contains the name and path to a directory that makes up the
 * applications cache.
 *
 * @author patton
 */
@XmlType(propOrder = { "name",
                       "path" })
public class CacheDirectory {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name of the directory, usual one of the constants in this class.
     */
    private String name;

    /**
     * The path to the directory, relative to the working directory of the
     * application.
     */
    private String path;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the name of the directory, usual one of the constants in this class.
     *
     * @return the name of the directory, usual one of the constants in this class.
     */
    @XmlElement
    protected String getName() {
        return name;
    }

    /**
     * Returns the path to the directory, relative to the working directory of the
     * application.
     *
     * @return the path to the directory, relative to the working directory of the
     *         application.
     */
    @XmlElement
    protected String getPath() {
        return path;
    }

    /**
     * Sets the name of the directory, usual one of the constants in this class.
     *
     * @param name
     *            the name of the directory, usual one of the constants in this
     *            class.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets t
     *
     * @param path
     *            the path to the directory, relative to the working directory of
     *            the application.
     */
    protected void setPath(String path) {
        this.path = path;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
