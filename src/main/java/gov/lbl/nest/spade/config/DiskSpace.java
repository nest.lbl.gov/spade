package gov.lbl.nest.spade.config;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlValue;

/**
 * This class contains information about disk space.
 *
 * @author patton
 */
public class DiskSpace {

    // public static final member data

    /**
     * The String used to signify Gigabytes as units.
     */
    public static final String GB = "GB";

    /**
     * The String used to signify Kilobytes as units.
     */
    public static final String KB = "KB";

    /**
     * The String used to signify Megabytes as units.
     */
    public static final String MB = "MB";

    /**
     * The String used to signify Terabytes as units.
     */
    public static final String TB = "TB";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The number of bytes in a kilobyte;
     */
    private static final long BYTES_IN_KB = 1024;

    // private static member data

    // private instance member data

    /**
     * The units in which the disk space is specified.
     */
    private String units;

    /**
     * The disk space.
     */
    private long value;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected DiskSpace() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param units
     *            the units in which the disk space is specified.
     * @param value
     *            the disk space.
     */
    public DiskSpace(final String units,
                     final long value) {
        this.units = units;
        this.value = value;
    }

    // instance member method (alphabetic)

    /**
     * Returns the disk space, in bytes.
     *
     * @return the disk space, in bytes.
     */
    public long getBytes() {
        if (-1 == value) {
            return value;
        }
        long result = value;
        final String unitsToUse = getUnits();
        if ("bytes".equalsIgnoreCase(unitsToUse)) {
            return result;
        }
        result *= BYTES_IN_KB;
        if ("kilobytes".equalsIgnoreCase(unitsToUse) || KB.equals(unitsToUse)) {
            return result;
        }
        result *= BYTES_IN_KB;
        if ("megabytes".equalsIgnoreCase(unitsToUse) || MB.equals(unitsToUse)) {
            return result;
        }
        result *= BYTES_IN_KB;
        if ("gigabytes".equalsIgnoreCase(unitsToUse) || GB.equals(unitsToUse)) {
            return result;
        }
        result *= BYTES_IN_KB;
        if ("terabytes".equalsIgnoreCase(unitsToUse) || TB.equals(unitsToUse)) {
            return result;
        }
        return -1;
    }

    /**
     * Returns the units in which the disk space is specified.
     *
     * @return the units in which the disk space is specified.
     */
    @XmlAttribute
    protected String getUnits() {
        if (null == units) {
            return "bytes";
        }
        return units;
    }

    /**
     * Returns the disk space.
     *
     * @return the disk space.
     */
    @XmlValue
    protected long getValue() {
        return value;
    }

    /**
     * Set the number of bytes of this object's disk space.
     *
     * @param bytes
     *            the number of bytes of this object's disk space.
     */
    protected void setBytes(final long bytes) {
        final String unitsToUse = getUnits();
        long result = bytes;
        if ("bytes".equals(unitsToUse)) {
            value = result;
            return;
        }
        result /= BYTES_IN_KB;
        if ("kilobytes".equalsIgnoreCase(unitsToUse) || KB.equals(unitsToUse)) {
            value = result;
            return;
        }
        result /= BYTES_IN_KB;
        if ("megabytes".equalsIgnoreCase(unitsToUse) || MB.equals(unitsToUse)) {
            value = result;
            return;
        }
        result /= BYTES_IN_KB;
        if ("gigabytes".equalsIgnoreCase(unitsToUse) || GB.equals(unitsToUse)) {
            value = result;
            return;
        }
        result /= BYTES_IN_KB;
        if ("terabytes".equalsIgnoreCase(unitsToUse) || TB.equals(unitsToUse)) {
            value = result;
            return;
        }
        value = -1;
    }

    /**
     * Sets the units in which the disk space is specified.
     *
     * @param units
     *            the units in which the disk space is specified.
     */
    public void setUnits(String units) {
        this.units = units;
    }

    /**
     * Sets the disk space.
     *
     * @param value
     *            the disk space.
     */
    public void setValue(long value) {
        this.value = value;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
