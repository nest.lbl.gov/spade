package gov.lbl.nest.spade.config;

import java.util.List;

import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class describes the assembly of components that make up the application.
 *
 * @author patton
 */
@XmlType(propOrder = { "name",
                       "ticketLimit",
                       "threadLimit",
                       "timeLimit",
                       "preserve",
                       "executionMode",
                       "activities" })
public class Assembly {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * Calculates the default thread limit work a {@link Workflow} instance.
     *
     * @param workflow
     *            the {@link Workflow} whose limit should be returned.
     * @param threadCount
     *            the requested thread count, if any.
     *
     * @return the default thread limit work a {@link Workflow} instance.
     */
    public static Integer getDefaultThreadLimit(Workflow workflow,
                                                Integer threadCount) {
        /** Default allocations of threads */
        if (Workflow.INGEST == workflow) {
            /** Set INGEST to max - "number of other workflows" */
            final int target = (Runtime.getRuntime()).availableProcessors() - (Workflow.values()).length;
            final int targetToUse;
            if (0 >= target) {
                targetToUse = 1;
            } else {
                targetToUse = target;
            }
            threadCount = Integer.valueOf(targetToUse);
        }
        return threadCount;
    }

    /**
     * The components that make up this assembly.
     */
    private List<Activity> activities;

    /**
     * The {@link ExecutionMode} instance stating how in this instance should run.
     */
    private ExecutionMode mode;

    /**
     * The name of this assembly.
     */
    private String name;

    /**
     * <code>true</code> if details of a workflow failure should be preserved.
     */
    private Boolean preserve;

    /**
     * The collection of {@link ThreadLimit} instance defining the execution
     * context.
     */
    private List<ThreadLimit> threadLimits;

    /**
     * The maximum number of tickets that can be in used at any point in time.
     */
    private Integer ticketLimit;

    // constructors

    /**
     * The time limit, in seconds, after which an executing Task to be considers
     * stalled.
     */
    private Integer timeLimit;

    /**
     * Creates an instance of this class.
     */
    protected Assembly() {
    }

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param name
     *            the name of this assembly.
     * @param activities
     *            the components that make up this assembly.
     */
    public Assembly(final String name,
                    List<Activity> activities) {
        setActivities(activities);
        setName(name);
    }

    /**
     * Returns the components that make up this assembly.
     *
     * @return the components that make up this assembly.
     */
    @XmlElement(name = "activity")
    public List<Activity> getActivities() {
        return activities;
    }

    /**
     * Returns the component, if any, with the specified name, <code>null</code>
     * otherwise.
     *
     * @param activityName
     *            the name of the component that should be returned.
     *
     * @return the component, if any, with the specified name, <code>null</code>
     *         otherwise.
     */
    public Activity getActivity(final String activityName) {
        if (null != activities) {
            for (Activity activity : activities) {
                if (activityName.equals(activity.getName())) {
                    return activity;
                }
            }
        }
        return null;
    }

    /**
     * Returns the {@link ExecutionMode} instance stating how in this instance
     * should run.
     *
     * @return the {@link ExecutionMode} instance stating how in this instance
     *         should run.
     */
    @XmlElement(name = "execution_mode")
    public ExecutionMode getExecutionMode() {
        return mode;
    }

    /**
     * Returns the name of this assembly.
     *
     * @return the name of this assembly.
     */
    @XmlElement(required = true)
    public String getName() {
        return name;
    }

    /**
     * Returns <code>true</code> if details of a workflow failure should be
     * preserved.
     * 
     * @return <code>true</code> if details of a workflow failure should be
     *         preserved.
     */
    @XmlElement
    public Boolean getPreserve() {
        return preserve;
    }

    /**
     * Returns the collection of {@link ThreadLimit} instance defining the execution
     * context.
     *
     * @return the collection of {@link ThreadLimit} instance defining the execution
     *         context.
     */
    @XmlElement(name = "thread_limit")
    public List<ThreadLimit> getThreadLimit() {
        return threadLimits;
    }

    /**
     * Returns the maximum number of tickets that can be in used at any point in
     * time.
     *
     * @return the maximum number of tickets that can be in used at any point in
     *         time.
     */
    @XmlElement(name = "ticket_limit")
    public Integer getTicketLimit() {
        return ticketLimit;
    }

    /**
     * Returns the time limit, in seconds, after which an executing Task to be
     * considers stalled.
     *
     * @return the time limit, in seconds, after which an executing Task to be
     *         considers stalled.
     */
    @XmlElement(name = "time_limit")
    public Integer getTimeLimit() {
        return timeLimit;
    }

    /**
     * Sets the components that make up this assembly.
     *
     * @param activities
     *            the components that make up this assembly.
     */
    protected void setActivities(List<Activity> activities) {
        this.activities = activities;
    }

    /**
     * Sets the {@link ExecutionMode} instance stating how in this instance should
     * run.
     *
     * @param mode
     *            the {@link ExecutionMode} instance stating how in this instance
     *            should run.
     */
    protected void setExecutionMode(ExecutionMode mode) {
        this.mode = mode;
    }

    /**
     * Sets the name of this assembly.
     *
     * @param name
     *            the name of this assembly.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets whether details of a workflow failure should be preserved or not.
     * 
     * @param preserve
     *            <code>true</code> if details of a workflow failure should be
     *            preserved.
     */
    public void setPreserve(Boolean preserve) {
        this.preserve = preserve;
    }

    /**
     * Sets the collection of {@link ThreadLimit} instance defining the execution
     * context.
     *
     * @param limits
     *            the collection of {@link ThreadLimit} instance defining the
     *            execution context.
     */
    protected void setThreadLimit(List<ThreadLimit> limits) {
        threadLimits = limits;
    }

    /**
     * Sets the maximum number of tickets that can be in used at any point in time.
     *
     * @param limit
     *            the maximum number of tickets that can be in used at any point in
     *            time.
     */
    public void setTicketLimit(Integer limit) {
        ticketLimit = limit;
    }

    // static member methods (alphabetic)

    /**
     * Sets the time limit, in seconds, after which an executing Task to be
     * considers stalled.
     *
     * @param limit
     *            the time limit, in seconds, after which an executing Task to be
     *            considers stalled.
     */
    public void setTimeLimit(Integer limit) {
        timeLimit = limit;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
