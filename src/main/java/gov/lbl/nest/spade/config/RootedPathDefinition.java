package gov.lbl.nest.spade.config;

import java.io.File;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class contains the definition of a root path for a system.
 *
 * @author patton
 */
@XmlType(propOrder = { "rootPath" })
public class RootedPathDefinition {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The root directory, if there is one, of the system.
     */
    private File rootDirectory;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected RootedPathDefinition() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param directory
     *            the root directory, if there is one, of the system.
     */
    public RootedPathDefinition(final File directory) {
        setRootPath(directory.getAbsolutePath());
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param directory
     *            the root directory, if there is one, of the system.
     */
    public RootedPathDefinition(final String directory) {
        setRootPath(directory);
    }

    // instance member method (alphabetic)

    /**
     * Returns the root directory, if there is one, of the system.
     *
     * @return the root directory, if there is one, of the system.
     */
    @XmlTransient
    public File getRootDirectory() {
        return rootDirectory;
    }

    /**
     * Returns the path to the root directory, if there is one, of the system.
     *
     * @return the path to the root directory, if there is one, of the system.
     */
    @XmlElement(name = "root_path",
                required = true)
    protected String getRootPath() {
        if (null == rootDirectory) {
            return null;
        }
        return rootDirectory.getAbsolutePath();
    }

    /**
     * Sets the path to the root directory, if there is one, of the system.
     *
     * @param path
     *            the path to the root directory, if there is one, of the system.
     */
    protected void setRootPath(String path) {
        if (null == path) {
            rootDirectory = null;
            return;
        }
        rootDirectory = new File(path);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
