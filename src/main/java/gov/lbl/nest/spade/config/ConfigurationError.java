package gov.lbl.nest.spade.config;

/**
 * This class throw when there is a fatal error in the configuration of this
 * application.
 *
 * @author patton
 */
public class ConfigurationError extends
                                Error {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by serializable.
     */
    private static final long serialVersionUID = 1L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message explaining why this Object was thrown.
     */
    public ConfigurationError(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the {@link Throwable} instance from the operation.
     */
    public ConfigurationError(Throwable cause) {
        super(cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
