package gov.lbl.nest.spade.config;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import jakarta.persistence.Transient;
import jakarta.xml.bind.annotation.XmlAnyElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class contains the set of URLs that have been declared for a
 * {@link Neighbor} instance.
 *
 * @author patton
 *
 */
@XmlType
public class Urls {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The name of the attribute of the "confirmable" element that will contain, if
     * any, the Watcher Role to be used in conjunction with the confirmableUrl.
     */
    private static final String CONFIRMABLES_ROLE_ATTRIBUTE = "role";

    /**
     * The name of the element that will contain the confirmables URL.
     */
    private static final String CONFIRMABLES_TAG = "confirmables";

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Urls.class);

    /**
     * The name of the element that will contain the RESTful API URL.
     */
    private static final String RESTFUL_API_TAG = "api";

    // private static member data

    // private instance member data

    /**
     * The String containing, if any, the Watcher Role to be used in conjunction
     * with the confirmableUrl.
     */
    @Transient
    private String confirmableRole;

    /**
     * The collection of {@link Element} instances making up this object.
     */
    @XmlAnyElement
    private List<Element> elements;

    /**
     * The mapping of name to their URLs.
     */
    @Transient
    private Map<String, String> mapping;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the String for the RESTful API URL.
     *
     * @return the String from which to build a request for a neighbors
     *         confirmables.
     */
    String getApiUrl() {
        final Map<String, String> currentMapping = getMapping();
        if (null == currentMapping) {
            return null;
        }
        return currentMapping.get(RESTFUL_API_TAG);
    }

    /**
     * Returns the String containing, if any, the Watcher Role to be used in
     * conjunction with the confirmableUrl.
     *
     * @return the String containing, if any, the Watcher Role to be used in
     *         conjunction with the confirmableUrl.
     */
    public String getConfirmableRole() {
        // Make sure the elements have been parsed.
        getMapping();
        return confirmableRole;
    }

    /**
     * Returns the String from which to build a request for a neighbors
     * confirmables.
     *
     * @return the String from which to build a request for a neighbors
     *         confirmables.
     */
    String getConfirmablesUrl() {
        final Map<String, String> currentMapping = getMapping();
        if (null == currentMapping) {
            return null;
        }
        return currentMapping.get(CONFIRMABLES_TAG);
    }

    Map<String, String> getMapping() {
        if (null == mapping && null != elements) {
            mapping = new HashMap<>();
            URI apiUri = null;
            for (Element element : elements) {
                final String tag = element.getTagName();
                String text = element.getTextContent();
                if (RESTFUL_API_TAG.equals(tag)) {
                    try {
                        apiUri = new URI(text);
                        text = apiUri.toString();
                    } catch (URISyntaxException e) {
                        LOG.error("\"api\" URL is not syntactically correct");
                    }
                }
                if (CONFIRMABLES_TAG.contentEquals(tag)) {
                    final String role = element.getAttribute(CONFIRMABLES_ROLE_ATTRIBUTE);
                    if (null == role || "".equals(role)) {
                        confirmableRole = null;
                    } else {
                        confirmableRole = role;
                    }
                }
                mapping.put(tag,
                            text);
            }
            if (null != apiUri) {
                for (String tag : mapping.keySet()) {
                    URI uri;
                    try {
                        uri = apiUri.resolve(new URI(mapping.get(tag)));
                        mapping.put(tag,
                                    uri.toString());
                    } catch (URISyntaxException e) {
                        // Do nothing to item.
                    }
                }
            }
        }
        return mapping;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
