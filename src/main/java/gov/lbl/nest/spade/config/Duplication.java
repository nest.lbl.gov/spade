package gov.lbl.nest.spade.config;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.registry.FileCategory;
import gov.lbl.nest.spade.services.FileTransfer;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class describes how a bundle should be transferred to a specified
 * destination.
 *
 * @author patton
 */
@XmlType(propOrder = { "location",
                       "category",
                       "transferClass" })
public class Duplication extends
                         UnmanagedTransfer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link FileCategory} of the files to be duplicated.
     */
    private FileCategory category;

    /**
     * The {@link ExternalLocation} specifying the directory to where files should
     * be delivered.
     */
    private ExternalLocation deliveryLocation;

    /**
     * The {@link FileTransfer} implementation to use for the transfer.
     */
    private FileTransfer transferImplementation;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Duplication() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param name
     *            the name with which to refer to this object.
     * @param description
     *            the short description of this transfer.
     * @param location
     *            the string specifying the {@link ExternalLocation} to where files
     *            should be delivered.
     * @param transferImplementation
     *            the {@link FileTransfer} implementation to use for the transfer.
     */
    public Duplication(final String name,
                       final String description,
                       final String location,
                       final FileTransfer transferImplementation) {
        super(name,
              description);
        setLocation(location);
        this.transferImplementation = transferImplementation;
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link FileCategory} of the files to be duplicated.
     *
     * @return the {@link FileCategory} of the files to be duplicated.
     */
    @XmlElement
    public FileCategory getCategory() {
        return category;
    }

    /**
     * Returns the {@link ExternalLocation} specifying the directory to where files
     * should be duplicated.
     *
     * @return the {@link ExternalLocation} specifying the directory to where files
     *         should be duplicated.
     */
    @XmlTransient
    public ExternalLocation getDuplicationLocation() {
        return deliveryLocation;
    }

    /**
     * Returns the string specifying the {@link ExternalLocation} to where files
     * should be delivered.
     *
     * @return the string specifying the {@link ExternalLocation} to where files
     *         should be delivered.
     */
    @XmlElement
    public String getLocation() {
        return deliveryLocation.toString();
    }

    /**
     * Returns the canonical name for the class to use for the transfer.
     *
     * @return the canonical name for the class to use for the transfer.
     */
    @XmlElement(name = "class")
    protected String getTransferClass() {
        if (null == transferImplementation) {
            return null;
        }
        final Class<?> clazz = transferImplementation.getClass();
        return clazz.getCanonicalName();
    }

    /**
     * Returns the {@link FileTransfer} implementation to use for the transfer.
     *
     * @return the {@link FileTransfer} implementation to use for the transfer.
     */
    @XmlTransient
    public FileTransfer getTransferImplementation() {
        return transferImplementation;
    }

    /**
     * Sets the {@link FileCategory} of the files to be duplicated.
     *
     * @param category
     *            the {@link FileCategory} of the files to be duplicated.
     */
    protected void setCategory(FileCategory category) {
        this.category = category;
    }

    /**
     * Sets the string specifying the {@link ExternalLocation} to where files should
     * be delivered.
     *
     * @param location
     *            the string specifying the {@link ExternalLocation} to where files
     *            should be delivered.
     */
    protected void setLocation(final String location) {
        deliveryLocation = new ExternalLocation(location);
    }

    /**
     * Sets the canonical name of the class to use for the transfer.
     *
     * @param className
     *            the canonical name of the class to use for the transfer.
     */
    protected void setTransferClass(final String className) {
        final ClassLoader loader = getClass().getClassLoader();
        try {
            FileTransfer instance = null;
            final Class<?> clazz = loader.loadClass(className);
            final Constructor<?> constructor = clazz.getConstructor();
            instance = (FileTransfer) constructor.newInstance();
            if (null != instance) {
                transferImplementation = instance;
            }
        } catch (ClassNotFoundException
                 | IllegalAccessException
                 | NoSuchMethodException
                 | InstantiationException
                 | IllegalArgumentException
                 | InvocationTargetException e) {
            throw new IllegalArgumentException("Can not instantiate class \"" + className
                                               + "\"");
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
