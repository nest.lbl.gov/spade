package gov.lbl.nest.spade.config;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is the base class for the definition of a transfer of a bundle.
 *
 * @author patton
 */
@XmlType(propOrder = { "name",
                       "description" })
public abstract class UnmanagedTransfer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The short description of this transfer.
     */
    private String description;

    /**
     * The name with which to refer to this object.
     */
    private String name;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected UnmanagedTransfer() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param name
     *            the name with which to refer to this object.
     * @param description
     *            the short description of this transfer.
     */
    public UnmanagedTransfer(final String name,
                             final String description) {
        setDescription(description);
        setName(name);
    }

    // instance member method (alphabetic)

    /**
     * Returns the short description of this transfer.
     *
     * @return the short description of this transfer.
     */
    @XmlElement
    protected String getDescription() {
        return description;
    }

    /**
     * Returns the name with which to refer to this object.
     *
     * @return the name with which to refer to this object.
     */
    @XmlElement(required = true)
    public String getName() {
        return name;
    }

    /**
     * Sets the short description of this transfer.
     *
     * @param description
     *            the short description of this transfer.
     */
    protected void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Sets the name with which to refer to this object.
     *
     * @param name
     *            the name with which to refer to this object.
     */
    protected void setName(final String name) {
        this.name = name;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
