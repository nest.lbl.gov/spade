package gov.lbl.nest.spade.config;

import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.configure.InitParam;
import gov.lbl.nest.spade.interfaces.policy.ImplementedPolicy;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class describes how to configure a task in SPADE's workflow.
 *
 * @author patton
 */
@XmlType(propOrder = { "name",
                       "threads",
                       "storageFactor",
                       "parameters",
                       "policy" })
public class Activity {

    // public static final member data

    /**
     * The name of the analyzer activity.
     */
    public static final String ANALYZER = "analyzer";

    /**
     * The name of the archiver activity.
     */
    public static final String ARCHIVER = "archiver";

    /**
     * The name of the compressor activity.
     */
    public static final String COMPRESSOR = "compressor";

    /**
     * The name of the confirmer activity.
     */
    public static final String CONFIRMER = "confirmer";

    /**
     * The name of the duplicator activity.
     */
    public static final String DUPLICATOR = "duplicator";

    /**
     * The name of the examiner activity.
     */
    public static final String EXAMINER = "examiner";

    /**
     * The name of the expander activity.
     */
    public static final String EXPANDER = "expander";

    /**
     * The name of the fetcher activity.
     */
    public static final String FETCHER = "fetcher";

    /**
     * The name of the finisher activity.
     */
    public static final String FINISHER = "finisher";

    /**
     * The name of the placer activity.
     */
    public static final String RECEIVER = "receiver";

    /**
     * The name of the packer activity.
     */
    public static final String PACKER = "packer";

    /**
     * The name of the placer activity.
     */
    public static final String PLACER = "placer";

    /**
     * The name of the post-shipper activity.
     */
    public static final String POST_RESHIPPER = "post_reshipper";

    /**
     * The name of the post-shipper activity.
     */
    public static final String POST_SHIPPER = "post_shipper";

    /**
     * The name of the pre-shipper activity.
     */
    public static final String PRE_RESHIPPER = "pre_reshipper";

    /**
     * The name of the pre-shipper activity.
     */
    public static final String PRE_SHIPPER = "pre_shipper";

    /**
     * The name of the shipper activity.
     */
    public static final String REARCHIVER = "rearchiver";

    /**
     * The name of the shipper activity.
     */
    public static final String RESHIPPER = "reshipper";

    /**
     * The name of the shipper activity.
     */
    public static final String SHIPPER = "shipper";

    /**
     * The name of the shipper activity.
     */
    public static final String UNPACKER = "unpacker";

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name of this instance of the activity.
     */
    private String name;

    /**
     * The initialization parameters, is any, for this activity.
     */
    private List<InitParam> parameters;

    /**
     * The {@link Policy}, if any, for this activity.
     */
    private Policy policy;

    /**
     * The factor used to define the minimum space needed for this activity.
     */
    private Integer storageFactor;

    /**
     * The number of thread to assign to this activity.
     */
    private Integer threads;

    // constructors

    /**
     * Creates an instance of this class
     */
    protected Activity() {
    }

    /**
     * Creates an instance of this class
     *
     * @param name
     *            the name of this instance of the activity.
     */
    public Activity(final String name) {
        this(name,
             null,
             null);
    }

    /**
     * Creates an instance of this class
     *
     * @param name
     *            the name of this instance of the activity.
     * @param params
     *            the initialization parameters, is any, for this activity.
     */
    public Activity(final String name,
                    final List<InitParam> params) {
        this(name,
             params,
             null);
    }

    /**
     * Creates an instance of this class
     *
     * @param name
     *            the name of this instance of the activity.
     * @param params
     *            the initialization parameters, is any, for this activity.
     * @param clazz
     *            the canonical name of the class used by this activity to define
     *            its policy.
     */
    Activity(final String name,
             final List<InitParam> params,
             final Class<?> clazz) {
        setName(name);
        if (null != params) {
            setParameters(new ArrayList<>(params));
        }
        if (null != clazz) {
            setPolicy(new Policy(clazz.getCanonicalName()));
        }
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link ImplementedPolicy} instance used by this {@link Activity}
     * to define its policy.
     *
     * @param clazz
     *            the {@link Class} of the object to be returned.
     * @param <T>
     *            the type if class to be returned.
     *
     * @return the {@link ImplementedPolicy} instance used by this {@link Activity}
     *         to define its policy.
     */
    public <T extends ImplementedPolicy> T getImplementedPolicy(Class<T> clazz) {
        if (null == policy) {
            return null;
        }
        return policy.getPolicy(clazz);
    }

    /**
     * Returns the name of this instance of the activity.
     *
     * @return the name of this instance of the activity.
     */
    @XmlElement(required = true)
    public String getName() {
        return name;
    }

    /**
     * Returns the value of the named parameter is specified, otherwise
     * <code>null</code>.
     *
     * @param parameter
     *            the name of the parameter to return.
     *
     * @return the value of the named parameter is specified, otherwise
     *         <code>null</code>.
     */
    public String getParameter(final String parameter) {
        final List<InitParam> params = getParameters();
        if (null != params) {
            for (InitParam param : params) {
                if (parameter.equals(param.getName())) {
                    return param.getValue();
                }
            }
        }
        return null;
    }

    /**
     * Returns the initialization parameters, is any, for this activity.
     *
     * @return the initialization parameters, is any, for this activity.
     */
    @XmlElement(name = "init-param")
    protected List<InitParam> getParameters() {
        return parameters;
    }

    /**
     * Returns the {@link Policy}, if any, for this activity.
     *
     * @return the {@link Policy}, if any, for this activity.
     */
    @XmlElement
    protected Policy getPolicy() {
        return policy;
    }

    /**
     * Returns the factor used to define the minimum space needed for this activity.
     *
     * @return the factor used to define the minimum space needed for this activity.
     */
    @XmlElement(name = "storage_factor")
    public Integer getStorageFactor() {
        return storageFactor;
    }

    /**
     * Returns the total factor used to define the minimum space needed for this
     * activity by multiplying by the number of threads.
     *
     * @return the total factor used to define the minimum space needed for this
     *         activity by multiplying by the number of threads.
     */
    @XmlTransient
    public int getStorageLoad() {
        final Integer factor = getStorageFactor();
        if (null == factor) {
            return 1 * getThreadCount();
        }
        return factor.intValue() * getThreadCount();
    }

    /**
     * Returns the thread count for this activity.
     *
     * @return the thread count for this activity.
     */
    @XmlTransient
    public int getThreadCount() {
        final Integer count = getThreads();
        if (null == count) {
            return 1;
        }
        return count.intValue();
    }

    /**
     * Returns the number of thread to assign to this activity.
     *
     * @return the number of thread to assign to this activity.
     */
    @XmlElement
    protected Integer getThreads() {
        return threads;
    }

    /**
     * Sets the name of this instance of the activity.
     *
     * @param name
     *            the name of this instance of the activity.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the initialization parameters, is any, for this activity.
     *
     * @param params
     *            the initialization parameters, is any, for this activity.
     */
    public void setParameters(List<InitParam> params) {
        parameters = params;
    }

    /**
     * Sets the {@link Policy}, if any, for this activity.
     *
     * @param policy
     *            the {@link Policy}, if any, for this activity.
     */
    protected void setPolicy(Policy policy) {
        this.policy = policy;
    }

    /**
     * Sets the factor used to define the minimum space needed for this activity.
     *
     * @param factor
     *            the factor used to define the minimum space needed for this
     *            activity.
     */
    public void setStorageFactor(Integer factor) {
        storageFactor = factor;
    }

    /**
     * Sets the number of thread to assign to this activity.
     *
     * @param threads
     *            the number of thread to assign to this activity.
     */
    protected void setThreads(Integer threads) {
        this.threads = threads;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
