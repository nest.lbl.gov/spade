package gov.lbl.nest.spade.config;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is the base class for the definition of a transfer of a bundle.
 *
 * @author patton
 */
@XmlType(propOrder = { "neighbor" })
public abstract class ManagedTransfer extends
                                      UnmanagedTransfer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The identity of the application at the other end of the transfer.
     */
    private String neighbor;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ManagedTransfer() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param name
     *            the name with which to refer to this object.
     * @param description
     *            the short description of this transfer.
     * @param neighbor
     *            the identity of the application at the other end of the transfer.
     */
    public ManagedTransfer(final String name,
                           final String description,
                           final String neighbor) {
        super(name,
              description);
        setNeighbor(neighbor);
    }

    // instance member method (alphabetic)

    /**
     * Returns the identity of the application at the other end of the transfer.
     *
     * @return the identity of the application at the other end of the transfer.
     */
    @XmlElement(required = true)
    public String getNeighbor() {
        return neighbor;
    }

    /**
     * Sets the identity of the application at the other end of the transfer.
     *
     * @param identity
     *            the identity of the application at the other end of the transfer.
     */
    protected void setNeighbor(final String identity) {
        neighbor = identity;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
