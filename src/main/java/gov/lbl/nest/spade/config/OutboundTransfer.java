package gov.lbl.nest.spade.config;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import gov.lbl.nest.common.configure.InitParam;
import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.services.FileTransfer;
import gov.lbl.nest.spade.services.impl.LocalhostTransfer;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class describes how a bundle should be transferred to a specified
 * destination.
 *
 * @author patton
 */
@XmlType(propOrder = { "locations",
                       "transferClass",
                       "parameters" })
public class OutboundTransfer extends
                              ManagedTransfer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    private static final FileTransfer UNIMPLEMENTED_TRANSFER = new FileTransfer() {

        @Override
        public boolean send(ExternalLocation targetLocation,
                            File metadataFile,
                            String targetMetadataName,
                            File transferFile,
                            String targetTransferName,
                            File semaphoreFile,
                            String targetSemaphoreName,
                            URI callback) throws IOException,
                                          InterruptedException {
            return false;
        }

        @Override
        public void setEnvironment(File configurationDir) {
            // Do nothing
        }
    };

    // private static member data

    // private instance member data

    /**
     * The collection of {@link String} instance specifying the
     * {@link ExternalLocation} to where files should be delivered.
     */
    private List<String> locations;

    /**
     * True if the selection of the delivery location is random. (The default is
     * round robin)
     */
    private final boolean random = false;

    /**
     * The {@link Random} instance used to select a delivery location when multiple
     * where specified and {@link #random} is <code>true</code>.
     */
    private final Random randomizer = new Random();

    /**
     * The index of the next delivery location to be selected when multiple where
     * specified and the default in not overridden.
     */
    private int robin = 0;

    /**
     * The initialization parameters, is any, for the transfer class.
     */
    private List<InitParam> parameters;

    /**
     * The canonical name for the class to use for the transfer.
     */
    private String transferClass;

    /**
     * The {@link FileTransfer} implementation to use for the transfer.
     */
    private FileTransfer transferImplementation;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected OutboundTransfer() {
    }

    /**
     * Creates an instance of this class for testing purposes.
     *
     * @param name
     *            the name of this Object
     * @param location
     *            the directory on the localhost to which to dispatch transfers.
     * @param neighbor
     *            the identity of the application at the other end of the transfer.
     */
    public OutboundTransfer(String name,
                            String location,
                            String neighbor) {
        super(name,
              null,
              neighbor);
        @SuppressWarnings("serial")
        final List<String> single = new ArrayList<>() {
            {
                add(location);
            }
        };
        setLocations(single);
        this.transferImplementation = new LocalhostTransfer();
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param name
     *            the name with which to refer to this object.
     * @param description
     *            the short description of this transfer.
     * @param neighbor
     *            the identity of the application at the other end of the transfer.
     * @param locations
     *            the string specifying the {@link ExternalLocation} to where files
     *            should be delivered.
     * @param transferImplementation
     *            the {@link FileTransfer} implementation to use for the transfer.
     */
    public OutboundTransfer(final String name,
                            final String description,
                            final String neighbor,
                            final List<String> locations,
                            final FileTransfer transferImplementation) {
        super(name,
              description,
              neighbor);
        setLocations(locations);
        this.transferImplementation = transferImplementation;
    }

    // instance member method (alphabetic)

    /**
     * Returns the one of the {@link ExternalLocation} instances specifying the
     * directory to where files should be delivered. The choice of value to return,
     * if there is more than one, is random.
     *
     * @return the one of the {@link ExternalLocation} instances specifying the
     *         directory to where files should be delivered.
     */
    @XmlTransient
    public ExternalLocation getDeliveryLocation() {
        return new ExternalLocation(getLocation());
    }

    /**
     * Returns the {@link String} specifying one of the {@link ExternalLocation}
     * instance to where files should be delivered.
     *
     * @return the {@link String} specifying one of the {@link ExternalLocation}
     *         instance to where files should be delivered.
     */
    @XmlTransient
    public synchronized String getLocation() {
        if (null == locations || locations.isEmpty()) {
            return null;
        }
        if (1 == locations.size()) {
            return locations.get(0);
        }

        if (random) {
            return locations.get(randomizer.nextInt(locations.size()));
        }
        synchronized (this) {
            if (robin >= locations.size()) {
                robin = 0;
            }
            return locations.get(robin++);
        }
    }

    /**
     * Returns the collection of {@link String} instance specifying the
     * {@link ExternalLocation} to where files should be delivered.
     *
     * @return the collection of {@link String} instance specifying the
     *         {@link ExternalLocation} to where files should be delivered.
     */
    @XmlElement(name = "location")
    public List<String> getLocations() {
        return locations;
    }

    /**
     * Returns the initialization parameters, is any, for the transfer class.
     *
     * @return the initialization parameters, is any, for the transfer class.
     */
    @XmlElement(name = "init-param")
    protected List<InitParam> getParameters() {
        return parameters;
    }

    /**
     * Returns the canonical name for the class to use for the transfer.
     *
     * @return the canonical name for the class to use for the transfer.
     */
    @XmlElement(name = "class")
    protected String getTransferClass() {
        return transferClass;
    }

    /**
     * Returns the {@link FileTransfer} implementation to use for the transfer.
     *
     * @return the {@link FileTransfer} implementation to use for the transfer.
     */
    @XmlTransient
    public FileTransfer getTransferImplementation() {
        if (null != transferClass && null == transferImplementation) {
            synchronized (transferClass) {
                if (null == transferImplementation) {
                    final ClassLoader loader = getClass().getClassLoader();
                    try {
                        FileTransfer instance = null;
                        final Class<?> clazz = loader.loadClass(transferClass);
                        if (null == parameters) {
                            final Constructor<?> constructor = clazz.getConstructor();
                            instance = (FileTransfer) constructor.newInstance();
                        } else {
                            final Constructor<?> constructor = clazz.getConstructor(Collection.class);
                            instance = (FileTransfer) constructor.newInstance(parameters);
                        }
                        if (null == instance) {
                            transferImplementation = UNIMPLEMENTED_TRANSFER;
                            throw new IllegalArgumentException("Can not instantiate class \"" + transferClass
                                                               + "\"");
                        }
                        transferImplementation = instance;
                    } catch (ClassNotFoundException
                             | IllegalAccessException
                             | NoSuchMethodException
                             | InstantiationException
                             | IllegalArgumentException
                             | InvocationTargetException
                             | SecurityException e) {
                        transferImplementation = UNIMPLEMENTED_TRANSFER;
                        throw new IllegalArgumentException("Can not instantiate class \"" + transferClass
                                                           + "\"",
                                                           e);
                    }
                }
            }
        }
        return transferImplementation;
    }

    /**
     * Sets the collection of {@link String} instance specifying the
     * {@link ExternalLocation} to where files should be delivered.
     *
     * @param locations
     *            the collection of {@link String} instance specifying the
     *            {@link ExternalLocation} to where files should be delivered.
     */
    protected void setLocations(final List<String> locations) {
        this.locations = locations;
    }

    /**
     * Sets the initialization parameters, is any, for the transfer class.
     *
     * @param params
     *            the initialization parameters, is any, for the transfer class.
     */
    public void setParameters(List<InitParam> params) {
        parameters = params;
    }

    /**
     * Sets the canonical name of the class to use for the transfer.
     *
     * @param className
     *            the canonical name of the class to use for the transfer.
     */
    protected void setTransferClass(final String className) {
        transferClass = className;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
