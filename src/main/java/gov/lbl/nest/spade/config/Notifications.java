package gov.lbl.nest.spade.config;

import java.util.Collection;

import gov.lbl.nest.spade.services.NotificationManager;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to configure the {@link NotificationManager}.
 *
 * @author patton
 */
@XmlType(propOrder = { "recipients" })
public class Notifications {

    /**
     * The collection of {@link Recipient} instances that make up this Object.
     */
    private Collection<Recipient> recipients;

    /**
     * Returns the collection of {@link Recipient} instances that make up this
     * Object.
     *
     * @return the collection of {@link Recipient} instances that make up this
     *         Object.
     */
    @XmlElement(name = "recipient")
    public Collection<Recipient> getRecipients() {
        return recipients;
    }

    /**
     * Sets the collection of {@link Recipient} instances that make up this Object.
     *
     * @param recipients
     *            the collection of {@link Recipient} instances that make up this
     *            Object.
     */
    protected void setRecipients(Collection<Recipient> recipients) {
        this.recipients = recipients;
    }
}
