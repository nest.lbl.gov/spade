package gov.lbl.nest.spade.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.common.configure.ConfigurationDirectory;
import gov.lbl.nest.common.configure.CustomConfigPath;
import gov.lbl.nest.spade.registry.ExternalLocation;
import jakarta.enterprise.inject.Vetoed;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class contains the configuration for the SPADE application.
 *
 * The {@link #load()} method of this class loads the configuration for a file
 * that is found by the following rules.
 * <ol>
 * <li>If the system property <code>gov.lbl.nest.spade.Configuration</code> is
 * defined then it is taken to be the path, either absolute or from the
 * applications working directory, to the XML file contains the setting.</li>
 * <li>If a provider class has been installed in a file that is visible to the
 * system class loader, and that file contains a provider-configuration file
 * named <code>gov.lbl.nest.spade.Configuration</code> in the resource directory
 * <code>META-INF/config</code>, then the first non-empty line specified in that
 * file is taken to be the file name.</li>
 * <li>Finally, if no provider has been specified by any of the above means then
 * the system-default name, <code>spade/spade.xml</code> is used.</li>
 * </ol>
 *
 * @author patton
 */
@XmlRootElement(name = "spade_config")
@XmlType(propOrder = { "assembly",
                       "registrationPaths",
                       "outboundTransfers",
                       "inboundTransfers",
                       "neighborhood",
                       "duplications",
                       "warehouseDefinition",
                       "archiveDefinition",
                       "cacheDefinition",
                       "notifications" })
@Vetoed
public class Configuration implements
                           ConfigurationDirectory {

    // public static final member data

    /**
     * The default for the directory that contains the main configuration files for
     * the application.
     */
    public static final File DEFAULT_APPLICATION_DIR = new File("spade");

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The default path to the warehouse for this application.
     */
    private static final String DEFAULT_ARCHIVE = "archive";

    /**
     * The value to use as the default minimum spade required, in GBs.
     */
    private static final int DEFAULT_MINIMUM_GB = 1;

    /**
     * The default path to the warehouse for this application.
     */
    private static final String DEFAULT_WAREHOUSE = "warehouse";

    /**
     * The default path to the XML file containing the settings for this object.
     */
    private static final String SPADE_CONFIG_DEFAULT_PATH = "spade/spade.xml";

    /**
     * The name of the resource that may contain the path to an alternate
     * configuration file for SPADE.
     */
    private static final String SPADE_CONFIG_RESOURCE = "spade_config";

    /**
     * The {@link CustomConfigPath} instance that may contain an alternate path to
     * the configuration file.
     */
    private static final CustomConfigPath SPADE_CONFIG_CUSTOM_PATH = new CustomConfigPath(Configuration.class,
                                                                                          SPADE_CONFIG_RESOURCE);

    /**
     * The name of the resource that may contain the path to an alternate
     * <q>suspend<\q> file for SPADE.
     */
    private static final String SUSPEND_FILE_RESOURCE = "suspend_file";

    /**
     * The default path to the file whose existence means the Application is
     * initially suspended. (relative to the configuration directory).
     */
    private static final String SUSPEND_FILE_DEFAULT_PATH = "spade.suspended";

    /**
     * True if the {@link #configFileName} value has been loaded.
     */
    private static CustomConfigPath SUSPEND_FILE_CONFIG_PATH = new CustomConfigPath(ApplicationSuspension.class,
                                                                                    SUSPEND_FILE_RESOURCE);

    /**
     * The {@link Marshaller} property to set formatting.
     */
    private static final String XML_FORMAT_PROPERTY = "jaxb.formatted.output";

    // private static member data

    // private instance member data

    /**
     * Generate a new {@link Configuration} instance and save it in the specified
     * file.
     *
     * @param file
     *            the {@link File} into which to store the generated configuration.
     *
     * @return the generated {@link Configuration} instance.
     */
    private static Configuration generate(final File file) {
        String hostName;
        try {
            hostName = (InetAddress.getLocalHost()).getHostName();
        } catch (UnknownHostException e) {
            hostName = "localhost";
        }
        final Assembly assembly = new Assembly("SPADE@" + hostName,
                                               null);

        final Configuration result = new Configuration(file,
                                                       assembly);

        final RootedPathDefinition warehouseDefinition = result.getWarehouseDefinition();
        final File warehouseRoot = warehouseDefinition.getRootDirectory();
        if (!warehouseRoot.exists()) {
            warehouseRoot.mkdirs();
        }

        final File defaultCache = new File(file.getParentFile(),
                                           "cache");
        if (!defaultCache.exists()) {
            defaultCache.mkdirs();
        }
        final DiskSpace diskSpace = new DiskSpace(DiskSpace.GB,
                                                  DEFAULT_MINIMUM_GB);
        final CacheDefinition cacheDefinition = new CacheDefinition(defaultCache.getPath(),
                                                                    diskSpace);
        result.setCacheDefinition(cacheDefinition);

        return result;
    }

    /**
     * Returns the {@link File} that is used to state whether the Application has
     * been suspended or not.
     *
     * @param directory
     *            the {@link ConfigurationDirectory} for this application.
     *
     * @return the {@link File} that is used to state whether the Application has
     *         been suspended or not.
     */
    public static File getSuspendFile(ConfigurationDirectory directory) {
        final Path filePath;
        final Path configPath = SUSPEND_FILE_CONFIG_PATH.getConfigPath();
        if (null == configPath) {
            final File configDir = directory.getDirectory();
            filePath = (new File(configDir,
                                 SUSPEND_FILE_DEFAULT_PATH).getAbsoluteFile()).toPath();
        } else {
            filePath = configPath;
        }
        return filePath.toFile();
    }

    /**
     * Returns a new instance of a {@link Configuration}.
     *
     * @return a new instance of a {@link Configuration}.
     *
     * @throws JAXBException
     *             when the setting file can not be read.
     */
    public static Configuration load() throws JAXBException {

        final Path filePath;
        final Path configPath = SPADE_CONFIG_CUSTOM_PATH.getConfigPath();
        if (null == configPath) {
            filePath = (new File(ExternalLocation.getUserHome(),
                                 SPADE_CONFIG_DEFAULT_PATH).getAbsoluteFile()).toPath();
        } else {
            filePath = configPath;
        }
        final File configFile = filePath.toFile();
        if (configFile.exists()) {
            try {
                final Configuration result = load(new FileInputStream(configFile));
                result.file = configFile;
                return result;
            } catch (FileNotFoundException e) {
                // Do nothing and proceed as if file did not exist!
            }
        }
        Configuration generated;
        try {
            generated = generate(configFile.getCanonicalFile());
            generated.save();
            return generated;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Attempts to read in a {@link Configuration} instance from an XML file.
     *
     * @param stream
     *            the {@link File} containing the XML.
     *
     * @return the {@link Configuration} instance read from the file.
     *
     * @throws JAXBException
     *             when the setting file can not be read.
     */
    public static Configuration load(final InputStream stream) throws JAXBException {
        JAXBContext content = JAXBContext.newInstance(Configuration.class);
        Unmarshaller unmarshaller = content.createUnmarshaller();
        final Configuration result = (Configuration) unmarshaller.unmarshal(stream);
        return result;
    }

    /**
     * The definition of the application's archive.
     */
    private RootedPathDefinition archive;

    /**
     * The {@link Assembly} instance describing the configurations of the tasks.
     */
    private Assembly assembly;

    /**
     * The current definition of this applications cache.
     */
    private CacheDefinition cacheDefinition;

    /**
     * The definitions of duplications supported by this application.
     */
    private List<Duplication> duplications;

    /**
     * The file that contains the settings for this object.
     */
    private File file;

    /**
     * The definitions of inbound transfers known to this application.
     */
    private List<InboundTransfer> inboundTransfers;

    /**
     * The list of {@link Neighbor} instances that make up the neighborhood of this
     * application.
     */
    private List<Neighbor> neighborhood;

    // constructors

    /**
     * The {@link Notifications} instance for this application.
     */
    private Notifications notifications;

    /**
     * The definitions of outbound transfer paths supported by this application.
     */
    private List<OutboundTransfer> outboundTransfers;

    /**
     * The paths to the top of the local and inbound registration trees.
     */
    private RegistrationPaths registrationPaths;

    // instance member method (alphabetic)

    /**
     * The definition of the application's warehouse.
     */
    private RootedPathDefinition warehouse;

    /**
     * Create an instance of this class.
     */
    protected Configuration() {
    }

    /**
     * Create an instance of this class.
     *
     * @param file
     *            the file that will contain the settings for this object.
     * @param assembly
     *            the {@link Assembly} instance describing the configurations of the
     *            tasks.
     */
    private Configuration(final File file,
                          final Assembly assembly) {
        setAssembly(assembly);
        this.file = file;
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param file
     *            the {@link File} that would have held this object's data.
     * @param assembly
     *            the {@link Assembly} instance describing the configurations of the
     *            tasks.
     * @param definition
     *            the current definition of this applications cache.
     * @param inbounds
     *            the definitions of inbound transfer paths supported by this
     *            application.
     * @param outbounds
     *            the definitions of outbound transfer paths supported by this
     *            application.
     * @param registrationPaths
     *            the paths to the top of the local and inbound registration trees
     * @param warehouse
     *            the description of the warehouse in use.
     */
    public Configuration(final File file,
                         final Assembly assembly,
                         final CacheDefinition definition,
                         final List<InboundTransfer> inbounds,
                         final List<OutboundTransfer> outbounds,
                         final RegistrationPaths registrationPaths,
                         final RootedPathDefinition warehouse) {
        this.file = file;
        setAssembly(assembly);
        setCacheDefinition(definition);
        setInboundTransfers(inbounds);
        setOutboundTransfers(outbounds);
        setRegistrationPaths(registrationPaths);
        setWarehouseDefinition(warehouse);
    }

    /**
     * Returns the definition of the application's archive.
     *
     * @return the definition of the application's archive.
     */
    @XmlElement(name = "archive")
    public RootedPathDefinition getArchiveDefinition() {
        if (null == archive) {
            archive = new RootedPathDefinition();
        }
        if (null == archive.getRootPath()) {
            archive.setRootPath(new File((file.getParentFile()).getParent(),
                                         DEFAULT_ARCHIVE).toString());
        }
        return archive;
    }

    /**
     * Returns the {@link Assembly} instance describing the configurations of the
     * tasks.
     *
     * @return the {@link Assembly} instance describing the configurations of the
     *         tasks.
     */
    @XmlElement
    public Assembly getAssembly() {
        return assembly;
    }

    /**
     * Returns the current definition of this applications cache.
     *
     * @return the current definition of this applications cache.
     */
    @XmlElement(name = "cache",
                required = true)
    public CacheDefinition getCacheDefinition() {
        return cacheDefinition;
    }

    /**
     * Returns the map of directories that make up the cache for this application.
     *
     * @return the map of directories that make up the cache for this application.
     */
    @XmlTransient
    public Map<String, File> getCacheMap() {
        return cacheDefinition.getCacheMap();
    }

    @XmlTransient
    @Override
    public File getDirectory() {
        File result = file.getParentFile();
        if (null == result) {
            return DEFAULT_APPLICATION_DIR;
        }
        return result;
    }

    /**
     * Returns the definitions of duplications supported by this application.
     *
     * @return the definitions of duplications supported by this application.
     */
    @XmlElement(name = "duplication")
    public List<Duplication> getDuplications() {
        return duplications;
    }

    /**
     * Returns the file in which the configuration file is kept.
     *
     * @return the file in which the configuration file is kept.
     */
    @XmlTransient
    public File getFile() {
        return file;
    }

    /**
     * Returns the definitions of inbound transfers known to this application.
     *
     * @return the definitions of inbound transfers known to this application.
     */
    @XmlElement(name = "inbound_transfer")
    public List<InboundTransfer> getInboundTransfers() {
        return inboundTransfers;
    }

    /**
     * Returns the list of {@link Neighbor} instances that make up the neighborhood
     * of this application.
     *
     * @return the list of {@link Neighbor} instances that make up the neighborhood
     *         of this application.
     */
    @XmlElement(name = "neighbor")
    @XmlElementWrapper(name = "neighborhood")
    public List<Neighbor> getNeighborhood() {
        return neighborhood;
    }

    /**
     * Returns the {@link Notifications} instance for this application.
     *
     * @return the {@link Notifications} instance for this application.
     */
    @XmlElement
    public Notifications getNotifications() {
        return notifications;
    }

    /**
     * Returns the definitions of outbound transfer paths supported by this
     * application.
     *
     * @return the definitions of outbound transfer paths supported by this
     *         application.
     */
    @XmlElement(name = "outbound_transfer")
    public List<OutboundTransfer> getOutboundTransfers() {
        return outboundTransfers;
    }

    /**
     * Returns the paths to the top of the local and inbound registration trees.
     *
     * @return the paths to the top of the local and inbound registration trees.
     */
    @XmlElement(name = "registration_paths")
    public RegistrationPaths getRegistrationPaths() {
        return registrationPaths;
    }

    /**
     * Returns the definition of the application's warehouse.
     *
     * @return the definition of the application's warehouse.
     */
    @XmlElement(name = "warehouse")
    public RootedPathDefinition getWarehouseDefinition() {
        if (null == warehouse) {
            warehouse = new RootedPathDefinition();
        }
        if (null == warehouse.getRootPath()) {
            warehouse.setRootPath(new File((file.getParentFile()).getParent(),
                                           DEFAULT_WAREHOUSE).toString());
        }
        return warehouse;
    }

    /**
     * Saves the current content to its file.
     *
     * @return the current version of this object.
     *
     * @throws JAXBException
     *             when the saving can not be saved.
     */
    private Configuration save() throws JAXBException {
        JAXBContext content = JAXBContext.newInstance(this.getClass());
        Marshaller marshaller = content.createMarshaller();
        marshaller.setProperty(XML_FORMAT_PROPERTY,
                               Boolean.TRUE);
        marshaller.marshal(this,
                           file);
        return this;
    }

    /**
     * Sets the definition of the application's archive.
     *
     * @param definition
     *            the definition of the application's archive.
     */
    protected void setArchiveDefinition(final RootedPathDefinition definition) {
        archive = definition;
    }

    /**
     * Sets the {@link Assembly} instance describing the configurations of the
     * tasks.
     *
     * @param assembly
     *            the {@link Assembly} instance describing the configurations of the
     *            tasks.
     */
    protected void setAssembly(final Assembly assembly) {
        this.assembly = assembly;
    }

    /**
     * Sets the current definition of this applications cache.
     *
     * @param definition
     *            the current definition of this applications cache.
     */
    protected void setCacheDefinition(final CacheDefinition definition) {
        cacheDefinition = definition;
    }

    /**
     * Sets the definitions of duplications supported by this application.
     *
     * @param duplications
     *            the definitions of outbound transfer paths supported by this
     *            application.
     */
    protected void setDuplications(final List<Duplication> duplications) {
        this.duplications = duplications;
    }

    /**
     * Sets the definitions of inbound transfers known to this application.
     *
     * @param transfers
     *            the definitions of inbound transfers known to this application.
     */
    protected void setInboundTransfers(final List<InboundTransfer> transfers) {
        inboundTransfers = transfers;
    }

    /**
     * Sets the list of {@link Neighbor} instances that make up the neighborhood of
     * this application.
     *
     * @param neighborhood
     *            the list of {@link Neighbor} instances that make up the
     *            neighborhood of this application.
     */
    protected void setNeighborhood(final List<Neighbor> neighborhood) {
        this.neighborhood = neighborhood;
    }

    // static member methods (alphabetic)

    /**
     * Sets the {@link Notifications} instance for this application.
     *
     * @param notifications
     *            the {@link Notifications} instance for this application.
     */
    protected void setNotifications(Notifications notifications) {
        this.notifications = notifications;
    }

    /**
     * Sets the definitions of outbound transfer paths supported by this
     * application.
     *
     * @param transfers
     *            the definitions of outbound transfer paths supported by this
     *            application.
     */
    protected void setOutboundTransfers(final List<OutboundTransfer> transfers) {
        outboundTransfers = transfers;
    }

    /**
     * Sets the paths to the top of the local and inbound registration trees.
     *
     * @param paths
     *            the paths to the top of the local and inbound registration trees.
     */
    protected void setRegistrationPaths(final RegistrationPaths paths) {
        registrationPaths = paths;
    }

    /**
     * Sets the definition of the application's warehouse.
     *
     * @param definition
     *            the definition of the application's warehouse.
     */
    protected void setWarehouseDefinition(final RootedPathDefinition definition) {
        warehouse = definition;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
