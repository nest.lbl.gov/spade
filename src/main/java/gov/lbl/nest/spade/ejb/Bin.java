package gov.lbl.nest.spade.ejb;

/**
 * This class is used to return flow data from the Database.
 *
 * @author patton
 *
 */
public class Bin {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The number of items in this bin.
     */
    private int count;

    /**
     * The sum of all the items in this bin.
     */
    private long sum;

    /**
     * The upper edge of this bin in seconds from the epoch.
     */
    private long upperEdge;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param upperEdge
     *            the upper edge of this bin in seconds from the epoch.
     * @param sum
     *            the sum of all the items in this bin.
     * @param count
     *            the number of items in this bin.
     */
    public Bin(long upperEdge,
               long sum,
               long count) {
        this.count = (int) count;
        this.sum = sum;
        this.upperEdge = upperEdge;
    }

    // instance member method (alphabetic)

    /**
     * Returns the number of items in this bin.
     *
     * @return the number of items in this bin.
     */
    public int getCount() {
        return count;
    }

    /**
     * Returns the sum of all the items in this bin.
     *
     * @return the sum of all the items in this bin.
     */
    public long getSum() {
        return sum;
    }

    /**
     * Returns the upper edge of this bin in seconds from the epoch.
     *
     * @return the upper edge of this bin in seconds from the epoch.
     */
    public long getUpperEdge() {
        return upperEdge;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
