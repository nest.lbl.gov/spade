package gov.lbl.nest.spade.ejb;

import gov.lbl.nest.spade.registry.LocalRegistration;
import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * This class contains the information about a registration with the context of
 * this application's data movement network.
 *
 * @author patton
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({ @NamedQuery(name = "getLocalRegistrations",
                            query = "SELECT r" + " FROM KnownLocalRegistration r"
                                    + " WHERE r.whenSuperseded IS NULL"),
                @NamedQuery(name = "getLocalRegistrationByLocalId",
                            query = "SELECT r" + " FROM KnownLocalRegistration r"
                                    + " WHERE r.localId = :localId"
                                    + " AND r.whenSuperseded IS NULL"),
                @NamedQuery(name = "getLocalRegistrationsByLocalId",
                            query = "SELECT r" + " FROM KnownLocalRegistration r"
                                    + " WHERE r.localId IN (:localIds)"
                                    + " AND r.whenSuperseded IS NULL") })
@Table(name = "localregistration")
public class KnownLocalRegistration extends
                                    KnownRegistration {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * Create an instance of this class.
     */
    protected KnownLocalRegistration() {
    }

    /**
     * Create an instance of this class.
     *
     * @param registration
     *            the {@link LocalRegistration} instance used to create this object.
     */
    public KnownLocalRegistration(LocalRegistration registration) {
        setLocalId(registration.getLocalId());
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
