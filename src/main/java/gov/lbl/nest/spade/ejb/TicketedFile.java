package gov.lbl.nest.spade.ejb;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.SecondaryTable;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Transient;

/**
 * This class contains the information about a ticketed file.
 *
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "activeTicket",
                            query = "SELECT 0 != count(t)" + " FROM TicketedFile t"
                                    + " WHERE t.ticketedFileKey = :identity"
                                    + " AND t.whenAbandoned IS NULL"),
                @NamedQuery(name = "archivedAfter",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenArchived IS NOT NULL"
                                    + " AND t.whenArchived >= :after"
                                    + " ORDER BY t.whenArchived ASC"),
                @NamedQuery(name = "archivedBefore",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenArchived IS NOT NULL"
                                    + " AND t.whenArchived <= :before"
                                    + " ORDER BY t.whenArchived DESC"),
                @NamedQuery(name = "archivedBetweenAsc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenArchived IS NOT NULL"
                                    + " AND t.whenArchived >= :after"
                                    + " AND t.whenArchived < :before"
                                    + " ORDER BY t.whenArchived ASC"),
                @NamedQuery(name = "archivedBetweenDesc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenArchived IS NOT NULL"
                                    + " AND t.whenArchived > :after"
                                    + " AND t.whenArchived <= :before"
                                    + " ORDER BY t.whenArchived DESC"),
                @NamedQuery(name = "archivedByRegistrationAfter",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenArchived IS NOT NULL"
                                    + " AND t.whenArchived >= :after"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenArchived ASC"),
                @NamedQuery(name = "archivedByRegistrationBefore",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenArchived IS NOT NULL"
                                    + " AND t.whenArchived <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenArchived DESC"),
                @NamedQuery(name = "archivedByRegistrationBetweenAsc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenArchived IS NOT NULL"
                                    + " AND t.whenArchived >= :after"
                                    + " AND t.whenArchived < :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenArchived ASC"),
                @NamedQuery(name = "archivedByRegistrationBetweenDesc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenArchived IS NOT NULL"
                                    + " AND t.whenArchived = :after"
                                    + " AND t.whenArchived <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenArchived DESC"),
                @NamedQuery(name = "firstPlacedDate",
                            query = "SELECT min(t.courseEpochPlaced)" + " FROM TicketedFile t"),
                @NamedQuery(name = "getCourseBinnedPlaced",
                            query = "SELECT NEW gov.lbl.nest.spade.ejb.Bin(t.courseEpochPlaced, sum(t.binarySize), count(t))" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced >= :after"
                                    + " AND t.whenPlaced < :before"
                                    + " GROUP BY t.courseEpochPlaced"
                                    + " ORDER BY t.courseEpochPlaced"),
                @NamedQuery(name = "getFineBinnedPlaced",
                            query = "SELECT NEW gov.lbl.nest.spade.ejb.Bin(t.fineEpochPlaced, sum(t.binarySize), count(t))" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced >= :after"
                                    + " AND t.whenPlaced < :before"
                                    + " GROUP BY t.fineEpochPlaced"
                                    + " ORDER BY t.fineEpochPlaced"),
                @NamedQuery(name = "getPlacementIdByBundle",
                            query = "SELECT t.placementId" + " FROM TicketedFile t"
                                    + " WHERE t.bundle = :bundle"
                                    + " AND t.whenPlaced IS NOT NULL"
                                    + " ORDER BY t.whenPlaced DESC"),
                @NamedQuery(name = "placedAfter",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced IS NOT NULL"
                                    + " AND t.whenPlaced >= :after"
                                    + " ORDER BY t.whenPlaced ASC"),
                @NamedQuery(name = "placedBefore",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced IS NOT NULL"
                                    + " AND t.whenPlaced <= :before"
                                    + " ORDER BY t.whenPlaced DESC"),
                @NamedQuery(name = "placedBetweenAsc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced IS NOT NULL"
                                    + " AND t.whenPlaced >= :after"
                                    + " AND t.whenPlaced < :before"
                                    + " ORDER BY t.whenPlaced ASC"),
                @NamedQuery(name = "placedBetweenDesc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced IS NOT NULL"
                                    + " AND t.whenPlaced > :after"
                                    + " AND t.whenPlaced <= :before"
                                    + " ORDER BY t.whenPlaced DESC"),
                @NamedQuery(name = "placedByRegistrationAfter",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced IS NOT NULL"
                                    + " AND t.whenPlaced >= :after"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenPlaced ASC"),
                @NamedQuery(name = "placedByRegistrationBefore",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced IS NOT NULL"
                                    + " AND t.whenPlaced <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenPlaced DESC"),
                @NamedQuery(name = "placedByRegistrationBetweenAsc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced IS NOT NULL"
                                    + " AND t.whenPlaced >= :after"
                                    + " AND t.whenPlaced < :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenPlaced ASC"),
                @NamedQuery(name = "placedByRegistrationBetweenDesc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenPlaced IS NOT NULL"
                                    + " AND t.whenPlaced > :after"
                                    + " AND t.whenPlaced <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenPlaced DESC"),
                @NamedQuery(name = "ticketedAfter",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenTicketed IS NOT NULL"
                                    + " AND t.whenTicketed >= :after"
                                    + " ORDER BY t.whenTicketed ASC"),
                @NamedQuery(name = "ticketedBefore",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenTicketed IS NOT NULL"
                                    + " AND t.whenTicketed <= :before"
                                    + " ORDER BY t.whenTicketed DESC"),
                @NamedQuery(name = "ticketedBetweenAsc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenTicketed IS NOT NULL"
                                    + " AND t.whenTicketed >= :after"
                                    + " AND t.whenTicketed < :before"
                                    + " ORDER BY t.whenTicketed ASC"),
                @NamedQuery(name = "ticketedBetweenDesc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenTicketed IS NOT NULL"
                                    + " AND t.whenTicketed > :after"
                                    + " AND t.whenTicketed <= :before"
                                    + " ORDER BY t.whenTicketed DESC"),
                @NamedQuery(name = "ticketedByRegistrationAfter",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenTicketed IS NOT NULL"
                                    + " AND t.whenTicketed >= :after"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenTicketed ASC"),
                @NamedQuery(name = "ticketedByRegistrationBefore",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenTicketed IS NOT NULL"
                                    + " AND t.whenTicketed <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenTicketed DESC"),
                @NamedQuery(name = "ticketedByRegistrationBetweenAsc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenTicketed IS NOT NULL"
                                    + " AND t.whenTicketed >= :after"
                                    + " AND t.whenTicketed < :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenTicketed ASC"),
                @NamedQuery(name = "ticketedByRegistrationBetweenDesc",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.whenTicketed IS NOT NULL"
                                    + " AND t.whenTicketed > :after"
                                    + " AND t.whenTicketed <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY t.whenTicketed DESC"),
                @NamedQuery(name = "ticketedFilesByNames",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.bundle IN (:bundles)"
                                    + " AND t.placementId IS NOT NULL"
                                    + " ORDER BY t.whenTicketed DESC"),
                @NamedQuery(name = "ticketedFilesByBundle",
                            query = "SELECT t" + " FROM TicketedFile t"
                                    + " WHERE t.bundle = :bundle"
                                    + " ORDER BY t.whenTicketed DESC") })
@SecondaryTable(name = "PlacedFile")
public class TicketedFile {

    // public static final member data

    /**
     * The size, in seconds, of the the course bins for epoch timestamps.
     */
    final public static long COURSE_BINNING = 86400; // one day.

    /**
     * The size, in seconds, of the the course bins for epoch timestamps.
     */
    final public static long FINE_BINNING = 600; // ten minutes.

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The suffix used to create the archived file name.
     */
    private String archiveSuffix;

    /**
     * The size of the binary file.
     */
    private Long binarySize;

    /**
     * The name used to denote the bundle of files ticketed by this onject.
     */
    private String bundle;

    /**
     * The size of the compressed file.
     */
    private Long compressedSize;

    /**
     * The {@link #whenPlaced} field in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next {@link #COURSE_BINNING}
     * boundary.
     *
     * Note: This is an optimization as JQL has neither a <code>EXTRACT(EPOCH
     * FROM whenPlaced)</code> function, nor a <code>GROUP BY item_identifier</code>
     * construct.
     */
    private Long courseEpochPlaced;

    /**
     * The {@link #whenPlaced} field in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next {@link #FINE_BINNING} boundary.
     *
     * Note: This is an optimization as JQL has neither a <code>EXTRACT(EPOCH
     * FROM whenPlaced)</code> function, nor a <code>GROUP BY item_identifier</code>
     * construct.
     */
    private Long fineEpochPlaced;

    /**
     * The {@link KnownRegistration} that was originally used to locate this file.
     */
    private KnownRegistration localRegistration;

    /**
     * The size of the metadata file.
     */
    private Long metadataSize;

    /**
     * The size of the packed file.
     */
    private Long packedSize;

    /**
     * The total number of bytes of data placed in the warehouse.
     */
    private Long placedSize;

    /**
     * The identity to use in order to retrieve placed files from the warehouse.
     */
    private String placementId;

    /**
     * This object's unique key within the set of {@link TicketedFile} instances.
     */
    private int ticketedFileKey;

    /**
     * The {@link UUID} string, if any, for this ticket.
     */
    private UUID uuid;

    /**
     * The date and time this ticketed file's transfer was abandoned.
     */
    private Date whenAbandoned;

    /**
     * The date and time this file was archived.
     */
    private Date whenArchived;

    /**
     * The date and time the binary size was set.
     */
    private Date whenBinarySet;

    /**
     * The date and time the compressed size was set.
     */
    private Date whenCompressedSet;

    /**
     * The date and time this object was flushed from the cache.
     */
    private Date whenFlushed;

    /**
     * The date and time the metadata size was set.
     */
    private Date whenMetadataSet;

    /**
     * The date and time the packed size was set.
     */
    private Date whenPackedSet;

    /**
     * The date and time the placement information was set.
     */
    private Date whenPlaced;

    /**
     * The date and time this file was restored.
     */
    private Date whenRestored;

    /**
     * The date and time this object was originally created.
     */
    private Date whenTicketed;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected TicketedFile() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param bundle
     *            the name of the bundle that is being ticketed by this object.
     * @param uuid
     *            the {@link UUID} to assign to the new identity.
     * @param registration
     *            the {@link KnownLocalRegistration} that was originally used to
     *            locate this file.
     */
    public TicketedFile(final String bundle,
                        final UUID uuid,
                        final KnownRegistration registration) {
        setBundle(bundle);
        setLocalRegistration(registration);
        setUuid(uuid);
        setWhenTicketed(new Date());
    }

    // instance member method (alphabetic)

    /**
     * Returns the suffix used to create the archived file name.
     *
     * @return the suffix used to create the archived file name.
     */
    protected String getArchiveSuffix() {
        return archiveSuffix;
    }

    /**
     * Returns the size of the binary file.
     *
     * @return the size of the binary file.
     */
    public Long getBinarySize() {
        return binarySize;
    }

    /**
     * Returns the name used to denote the bundle of files ticketed by this file.
     *
     * @return the name used to denote the bundle of files ticketed by this file.
     */
    @Column(nullable = false)
    public String getBundle() {
        return bundle;
    }

    /**
     * Returns the size of the compressed file.
     *
     * @return the size of the compressed file.
     */
    public Long getCompressedSize() {
        return compressedSize;
    }

    /**
     * Returns the whenPlaced property in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next {@link #COURSE_BINNING}
     * boundary.
     *
     * @return the whenPlaced property in terms of the number of seconds since the
     *         beginning of the epoch rounded up to the next {@link #COURSE_BINNING}
     *         boundary.
     */
    @Column(table = "PlacedFile")
    protected Long getCourseEpochPlaced() {
        return courseEpochPlaced;
    }

    /**
     * Returns the whenPlaced property in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next {@link #FINE_BINNING} boundary.
     *
     * @return the whenPlaced property in terms of the number of seconds since the
     *         beginning of the epoch rounded up to the next {@link #FINE_BINNING}
     *         boundary.
     */
    @Column(table = "PlacedFile")
    protected Long getFineEpochPlaced() {
        return fineEpochPlaced;
    }

    /**
     * Returns the {@link KnownRegistration} that was originally used to locate this
     * file.
     *
     * @return the {@link KnownRegistration} that was originally used to locate this
     *         file.
     */
    @ManyToOne(optional = false)
    protected KnownRegistration getLocalRegistration() {
        return localRegistration;
    }

    /**
     * Returns the size of the metadata file.
     *
     * @return the size of the metadata file.
     */
    protected Long getMetadataSize() {
        return metadataSize;
    }

    /**
     * Returns the size of the packed file.
     *
     * @return the size of the packed file.
     */
    public Long getPackedSize() {
        return packedSize;
    }

    /**
     * Returns the total number of bytes of data placed in the warehouse.
     *
     * @return the total number of bytes of data placed in the warehouse.
     */
    @Column(table = "PlacedFile")
    protected Long getPlacedSize() {
        return placedSize;
    }

    /**
     * Returns the identity to use in order to retrieve placed files from the
     * warehouse.
     *
     * @return the identity to use in order to retrieve placed files from the
     *         warehouse.
     */
    @Column(table = "PlacedFile")
    public String getPlacementId() {
        return placementId;
    }

    /**
     * Returns this object's unique key within the set of {@link TicketedFile}
     * instances.
     *
     * @return this object's unique key within the set of {@link TicketedFile}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getTicketedFileKey() {
        return ticketedFileKey;
    }

    /**
     * Returns the identity of the ticket issued for this object.
     *
     * @return the identity of the ticket issued for this object.
     */
    @Transient
    public String getTicketIdentity() {
        return Integer.toString(getTicketedFileKey());
    }

    /**
     * Returns he {@link UUID} for this ticket as a string.
     *
     * @return he {@link UUID} for this ticket as a string.
     */
    @Transient
    protected UUID getUuid() {
        return uuid;
    }

    /**
     * Returns the date and time this ticketed file's transfer was abandoned.
     *
     * @return the date and time this ticketed file's transfer was abandoned.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenAbandoned() {
        return whenAbandoned;
    }

    /**
     * Returns the date and time this file was archived.
     *
     * @return the date and time this file was archived.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenArchived() {
        return whenArchived;
    }

    /**
     * Returns the date and time the binary size was set.
     *
     * @return the date and time the binary size was set.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenBinarySet() {
        return whenBinarySet;
    }

    /**
     * Returns the date and time the compressed size was set.
     *
     * @return the date and time the compressed size was set.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenCompressedSet() {
        return whenCompressedSet;
    }

    /**
     * Returns the date and time this object was flushed from the cache.
     *
     * @return the date and time this object was flushed from the cache.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenFlushed() {
        return whenFlushed;
    }

    /**
     * Returns the date and time the metadata size was set.
     *
     * @return the date and time the metadata size was set.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenMetadataSet() {
        return whenMetadataSet;
    }

    /**
     * Returns the date and time the packed size was set.
     *
     * @return the date and time the packed size was set.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenPackedSet() {
        return whenPackedSet;
    }

    /**
     * Returns the date and time the placement information was set.
     *
     * @return the date and time the placement information was set.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(table = "PlacedFile")
    public Date getWhenPlaced() {
        return whenPlaced;
    }

    /**
     * Returns the date and time this file was restored.
     *
     * @return the date and time this file was restored.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenRestored() {
        return whenRestored;
    }

    /**
     * Returns the date and time this object was originally created.
     *
     * @return the date and time this object was originally created.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenTicketed() {
        return whenTicketed;
    }

    /**
     * Sets the suffix used to create the archived file name.
     *
     * @param suffix
     *            the suffix used to create the archived file name.
     */
    public void setArchiveSuffix(final String suffix) {
        archiveSuffix = suffix;
    }

    /**
     * Sets the size of the binary file.
     *
     * @param size
     *            the size of the binary file.
     */
    public void setBinarySize(final Long size) {
        binarySize = size;
    }

    /**
     * Sets the name used to denote the bundle of files ticketed by this file.
     *
     * @param name
     *            the name used to denote the bundle of files ticketed by this file.
     */
    protected void setBundle(final String name) {
        bundle = name;
    }

    /**
     * Sets the size of the compressed file.
     *
     * @param size
     *            the size of the compressed file.
     */
    public void setCompressedSize(final Long size) {
        compressedSize = size;
    }

    /**
     * Sets the whenPlaced property field in terms of the number of seconds since
     * the beginning of the epoch rounded up to the next {@link #COURSE_BINNING}
     * boundary.
     *
     * @param seconds
     *            the whenPlaced property in terms of the number of seconds since
     *            the beginning of the epoch rounded up to the next
     *            {@link #COURSE_BINNING} boundary.
     */
    protected void setCourseEpochPlaced(final Long seconds) {
        this.courseEpochPlaced = seconds;
    }

    /**
     * Sets the whenPlaced property in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next {@link #COURSE_BINNING}
     * boundary.
     *
     * @param seconds
     *            the whenPlaced property in terms of the number of seconds since
     *            the beginning of the epoch rounded up to the next
     *            {@link #COURSE_BINNING} boundary.
     */
    protected void setFineEpochPlaced(final Long seconds) {
        fineEpochPlaced = seconds;
    }

    /**
     * Sets the {@link KnownRegistration} that was originally used to locate this
     * file.
     *
     * @param registration
     *            the {@link KnownRegistration} that was originally used to locate
     *            this file.
     */
    protected void setLocalRegistration(final KnownRegistration registration) {
        localRegistration = registration;
    }

    /**
     * Sets the size of the metadata file.
     *
     * @param size
     *            the size of the metadata file.
     */
    public void setMetadataSize(final Long size) {
        metadataSize = size;
    }

    /**
     * Sets the size of the packed file.
     *
     * @param size
     *            the size of the packed file.
     */
    public void setPackedSize(final Long size) {
        packedSize = size;
    }

    /**
     * Sets the total number of bytes of data placed in the warehouse.
     *
     * @param size
     *            the total number of bytes of data placed in the warehouse.
     */
    public void setPlacedSize(final Long size) {
        placedSize = size;
    }

    /**
     * Sets the identity to use in order to retrieve placed files from the
     * warehouse.
     *
     * @param identity
     *            the identity to use in order to retrieve placed files from the
     *            warehouse.
     */
    public void setPlacementId(final String identity) {
        placementId = identity;
    }

    /**
     * Sets this object's unique key within the set of {@link TicketedFile}
     * instances.
     *
     * @param key
     *            this object's unique key within the set of {@link TicketedFile}
     *            instances.
     */
    protected void setTicketedFileKey(final int key) {
        ticketedFileKey = key;
    }

    /**
     * Sets the {@link UUID} for this ticket from a string.
     *
     * @param uuid
     *            the {@link UUID} for this ticket from a string.
     */
    protected void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    /**
     * Sets the date and time this ticketed file's transfer was abandoned.
     *
     * @param dateTime
     *            the date and time this ticketed file's transfer was abandoned.
     */
    public void setWhenAbandoned(final Date dateTime) {
        whenAbandoned = dateTime;
    }

    /**
     * Sets the date and time this file was archived.
     *
     * @param dateTime
     *            the date and time this file was archived.
     */
    public void setWhenArchived(final Date dateTime) {
        whenArchived = dateTime;
    }

    /**
     * Sets the date and time the binary size was set.
     *
     * @param dateTime
     *            the date and time the binary size was set.
     */
    public void setWhenBinarySet(final Date dateTime) {
        whenBinarySet = dateTime;
    }

    /**
     * Sets the date and time the compressed size was set.
     *
     * @param dateTime
     *            the date and time the compressed size was set.
     */
    public void setWhenCompressedSet(final Date dateTime) {
        whenCompressedSet = dateTime;
    }

    /**
     * Sets the date and time this object was flushed from the cache.
     *
     * @param dateTime
     *            the date and time this object was flushed from the cache.
     */
    protected void setWhenFlushed(final Date dateTime) {
        whenFlushed = dateTime;
    }

    /**
     * Sets the date and time the metadata size was set.
     *
     * @param dateTime
     *            the date and time the metadata size was set.
     */
    public void setWhenMetadataSet(final Date dateTime) {
        whenMetadataSet = dateTime;
    }

    /**
     * Sets the date and time the packed size was set.
     *
     * @param dateTime
     *            the date and time the packed size was set.
     */
    public void setWhenPackedSet(final Date dateTime) {
        whenPackedSet = dateTime;
    }

    /**
     * Sets the date and time the placement information was set.
     *
     * @param dateTime
     *            the date and time the placement information was set.
     */
    public void setWhenPlaced(final Date dateTime) {
        this.whenPlaced = dateTime;
        if (null == whenPlaced) {
            setCourseEpochPlaced(null);
            setFineEpochPlaced(null);
            return;
        }
        final long timeInSeconds = TimeUnit.SECONDS.convert(dateTime.getTime(),
                                                            TimeUnit.MILLISECONDS);
        setCourseEpochPlaced((1 + (timeInSeconds / COURSE_BINNING)) * COURSE_BINNING);
        setFineEpochPlaced((1 + (timeInSeconds / FINE_BINNING)) * FINE_BINNING);
    }

    /**
     * Sets the date and time this file was restored.
     *
     * @param dateTime
     *            the date and time this file was restored.
     */
    protected void setWhenRestored(final Date dateTime) {
        whenRestored = dateTime;
    }

    /**
     * Sets the date and time this object was originally created.
     *
     * @param dateTime
     *            the date and time this object was originally created.
     */
    protected void setWhenTicketed(final Date dateTime) {
        whenTicketed = dateTime;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
