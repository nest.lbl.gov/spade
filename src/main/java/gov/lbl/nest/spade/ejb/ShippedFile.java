package gov.lbl.nest.spade.ejb;

import java.util.Date;
import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OrderBy;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.persistence.Transient;

/**
 * This class contains the information about a ticketed file.
 *
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "confirmableAfter",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenConfirmable IS NOT NULL"
                                    + " AND s.whenConfirmable >= :after"
                                    + " ORDER BY s.whenConfirmable ASC"),
                @NamedQuery(name = "confirmableBefore",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenConfirmable IS NOT NULL"
                                    + " AND s.whenConfirmable <= :before"
                                    + " ORDER BY s.whenConfirmable DESC"),
                @NamedQuery(name = "confirmableBetweenAsc",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenConfirmable IS NOT NULL"
                                    + " AND s.whenConfirmable >= :after"
                                    + " AND s.whenConfirmable < :before"
                                    + " ORDER BY s.whenConfirmable ASC"),
                @NamedQuery(name = "confirmableBetweenDesc",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenConfirmable IS NOT NULL"
                                    + " AND s.whenConfirmable > :after"
                                    + " AND s.whenConfirmable <= :before"
                                    + " ORDER BY s.whenConfirmable DESC"),
                @NamedQuery(name = "confirmableByBundles",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND t.bundle IN (:bundles)"
                                    + " AND s.whenConfirmable IS NOT NULL"
                                    + " ORDER BY s.whenConfirmable DESC"),
                @NamedQuery(name = "confirmableByRegistrationAfter",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenConfirmable IS NOT NULL"
                                    + " AND s.whenConfirmable >= :after"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY s.whenConfirmable ASC"),
                @NamedQuery(name = "confirmableByRegistrationBefore",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenConfirmable IS NOT NULL"
                                    + " AND s.whenConfirmable <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY s.whenConfirmable DESC"),
                @NamedQuery(name = "confirmableByRegistrationBetweenAsc",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenConfirmable IS NOT NULL"
                                    + " AND s.whenConfirmable >= :after"
                                    + " AND s.whenConfirmable < :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY s.whenConfirmable ASC"),
                @NamedQuery(name = "confirmableByRegistrationBetweenDesc",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenConfirmable IS NOT NULL"
                                    + " AND s.whenConfirmable > :after"
                                    + " AND s.whenConfirmable <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY s.whenConfirmable DESC"),
                @NamedQuery(name = "getConfirmablesByBundle",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND t.bundle = :bundle"
                                    + " AND s.whenConfirmable IS NOT NULL"
                                    + " ORDER BY s.whenConfirmable DESC"),
                @NamedQuery(name = "unverifiedAfter",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenVerificationStarted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND s.whenVerificationStarted >= :after"
                                    + " ORDER BY s.whenVerificationStarted ASC"),
                @NamedQuery(name = "unverifiedBefore",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenVerificationStarted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND s.whenVerificationStarted <= :before"
                                    + " ORDER BY s.whenVerificationStarted DESC"),
                @NamedQuery(name = "unverifiedBetweenAsc",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenVerificationStarted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND s.whenVerificationStarted >= :after"
                                    + " AND s.whenVerificationStarted < :before"
                                    + " ORDER BY s.whenVerificationStarted ASC"),
                @NamedQuery(name = "unverifiedBetweenDesc",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenVerificationStarted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND s.whenVerificationStarted > :after"
                                    + " AND s.whenVerificationStarted <= :before"
                                    + " ORDER BY s.whenVerificationStarted DESC"),
                @NamedQuery(name = "unverifiedByDestinationAfter",
                            query = "SELECT s" + " FROM ShippedFile s, Confirmation c"
                                    + " WHERE s = c.shippedFile"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND c.whenConfirmed IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND c.destination = :neighbor"
                                    + " AND s.whenVerificationStarted >= :after"
                                    + " ORDER BY s.whenVerificationStarted ASC"),
                @NamedQuery(name = "unverifiedByDestinationBefore",
                            query = "SELECT s" + " FROM ShippedFile s, Confirmation c"
                                    + " WHERE s = c.shippedFile"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND c.whenConfirmed IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND c.destination = :neighbor"
                                    + " AND s.whenVerificationStarted <= :before"
                                    + " ORDER BY s.whenVerificationStarted DESC"),
                @NamedQuery(name = "unverifiedByDestinationBetweenAsc",
                            query = "SELECT s" + " FROM ShippedFile s, Confirmation c"
                                    + " WHERE s = c.shippedFile"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND c.whenConfirmed IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND c.destination = :neighbor"
                                    + " AND s.whenVerificationStarted >= :after"
                                    + " AND s.whenVerificationStarted < :before"
                                    + " ORDER BY s.whenVerificationStarted ASC"),
                @NamedQuery(name = "unverifiedByDestinationBetweenDesc",
                            query = "SELECT s" + " FROM ShippedFile s, Confirmation c"
                                    + " WHERE s = c.shippedFile"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND c.whenConfirmed IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND c.destination = :neighbor"
                                    + " AND s.whenVerificationStarted > :after"
                                    + " AND s.whenVerificationStarted <= :before"
                                    + " ORDER BY s.whenVerificationStarted DESC"),
                @NamedQuery(name = "unverifiedByRegistrationAfter",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " AND s.whenVerificationStarted >= :after"
                                    + " ORDER BY s.whenVerificationStarted ASC"),
                @NamedQuery(name = "unverifiedByRegistrationBefore",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " AND s.whenVerificationStarted <= :before"
                                    + " ORDER BY s.whenVerificationStarted DESC"),
                @NamedQuery(name = "unverifiedByRegistrationBetweenAsc",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " AND s.whenVerificationStarted >= :after"
                                    + " AND s.whenVerificationStarted < :before"
                                    + " ORDER BY s.whenVerificationStarted ASC"),
                @NamedQuery(name = "unverifiedByRegistrationBetweenDesc",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " AND s.whenVerificationStarted > :after"
                                    + " AND s.whenVerificationStarted <= :before"
                                    + " ORDER BY s.whenVerificationStarted DESC"),
                @NamedQuery(name = "unverifiedByRegistrationAndDestinationAfter",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t, Confirmation c"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s = c.shippedFile"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND c.whenConfirmed IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND c.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " AND s.whenVerificationStarted >= :after"
                                    + " ORDER BY s.whenVerificationStarted ASC"),
                @NamedQuery(name = "unverifiedByRegistrationAndDestinationBefore",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t, Confirmation c"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s = c.shippedFile"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND c.whenConfirmed IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND c.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " AND s.whenVerificationStarted <= :before"
                                    + " ORDER BY s.whenVerificationStarted DESC"),
                @NamedQuery(name = "unverifiedByRegistrationAndDestinationBetweenAsc",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t, Confirmation c"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s = c.shippedFile"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND c.whenConfirmed IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND c.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " AND s.whenVerificationStarted >= :after"
                                    + " AND s.whenVerificationStarted < :before"
                                    + " ORDER BY s.whenVerificationStarted ASC"),
                @NamedQuery(name = "unverifiedByRegistrationAndDestinationBetweenDesc",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t, Confirmation c"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s = c.shippedFile"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND c.whenConfirmed IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " AND c.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " AND s.whenVerificationStarted > :after"
                                    + " AND s.whenVerificationStarted <= :before"
                                    + " ORDER BY s.whenVerificationStarted DESC"),
                @NamedQuery(name = "getUnverifiedsByBundle",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE  s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND t.bundle = :bundle"
                                    + " AND s.whenVerificationStarted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted IS NULL"
                                    + " AND s.whenAbandoned IS NULL"
                                    + " ORDER BY s.whenVerificationStarted DESC"),
                @NamedQuery(name = "verifiedAfter",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenVerificationCompleted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted >= :after"
                                    + " ORDER BY s.whenVerificationCompleted ASC"),
                @NamedQuery(name = "verifiedBefore",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenVerificationCompleted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted <= :before"
                                    + " ORDER BY s.whenVerificationCompleted DESC"),
                @NamedQuery(name = "verifiedBetweenAsc",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenVerificationCompleted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted >= :after"
                                    + " AND s.whenVerificationCompleted < :before"
                                    + " ORDER BY s.whenVerificationCompleted ASC"),
                @NamedQuery(name = "verifiedBetweenDesc",
                            query = "SELECT s" + " FROM ShippedFile s"
                                    + " WHERE s.whenVerificationCompleted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted > :after"
                                    + " AND s.whenVerificationCompleted <= :before"
                                    + " ORDER BY s.whenVerificationCompleted DESC"),
                @NamedQuery(name = "verifiedByDestinationAfter",
                            query = "SELECT c.shippedFile" + " FROM Confirmation c"
                                    + " WHERE c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed >= :after"
                                    + " AND c.destination = :neighbor"
                                    + " ORDER BY c.whenConfirmed ASC"),
                @NamedQuery(name = "verifiedByDestinationBefore",
                            query = "SELECT c.shippedFile" + " FROM Confirmation c"
                                    + " WHERE c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed <= :before"
                                    + " AND c.destination = :neighbor"
                                    + " ORDER BY c.whenConfirmed DESC"),
                @NamedQuery(name = "verifiedByDestinationBetweenAsc",
                            query = "SELECT c.shippedFile" + " FROM Confirmation c"
                                    + " WHERE c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed >= :after"
                                    + " AND c.whenConfirmed < :before"
                                    + " AND c.destination = :neighbor"
                                    + " ORDER BY c.whenConfirmed ASC"),
                @NamedQuery(name = "verifiedByDestinationBetweenDesc",
                            query = "SELECT c.shippedFile" + " FROM Confirmation c"
                                    + " WHERE c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed > :after"
                                    + " AND c.whenConfirmed <= :before"
                                    + " AND c.destination = :neighbor"
                                    + " ORDER BY c.whenConfirmed DESC"),
                @NamedQuery(name = "verifiedByRegistrationAfter",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenVerificationCompleted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted >= :after"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY s.whenVerificationCompleted ASC"),
                @NamedQuery(name = "verifiedByRegistrationBefore",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenVerificationCompleted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY s.whenVerificationCompleted DESC"),
                @NamedQuery(name = "verifiedByRegistrationBetweenAsc",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenVerificationCompleted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted >= :after"
                                    + " AND s.whenVerificationCompleted < :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY s.whenVerificationCompleted ASC"),
                @NamedQuery(name = "verifiedByRegistrationBetweenDesc",
                            query = "SELECT s" + " FROM ShippedFile s, TicketedFile t"
                                    + " WHERE s.ticketedFileKey = t.ticketedFileKey"
                                    + " AND s.whenVerificationCompleted IS NOT NULL"
                                    + " AND s.whenVerificationCompleted > :after"
                                    + " AND s.whenVerificationCompleted <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY s.whenVerificationCompleted DESC"),
                @NamedQuery(name = "verifiedByRegistrationAndDestinationAfter",
                            query = "SELECT c.shippedFile" + " FROM Confirmation c, TicketedFile t"
                                    + " WHERE c.shippedFile.ticketedFileKey = t.ticketedFileKey"
                                    + " AND c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed >= :after"
                                    + " AND c.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY c.whenConfirmed ASC"),
                @NamedQuery(name = "verifiedByRegistrationAndDestinationBefore",
                            query = "SELECT c.shippedFile" + " FROM Confirmation c, TicketedFile t"
                                    + " WHERE c.shippedFile.ticketedFileKey = t.ticketedFileKey"
                                    + " AND c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed <= :before"
                                    + " AND c.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY c.whenConfirmed DESC"),
                @NamedQuery(name = "verifiedByRegistrationAndDestinationBetweenAsc",
                            query = "SELECT c.shippedFile" + " FROM Confirmation c, TicketedFile t"
                                    + " WHERE c.shippedFile.ticketedFileKey = t.ticketedFileKey"
                                    + " AND c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed >= :after"
                                    + " AND c.whenConfirmed < :before"
                                    + " AND c.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY c.whenConfirmed ASC"),
                @NamedQuery(name = "verifiedByRegistrationAndDestinationBetweenDesc",
                            query = "SELECT c.shippedFile" + " FROM Confirmation c, TicketedFile t"
                                    + " WHERE c.shippedFile.ticketedFileKey = t.ticketedFileKey"
                                    + " AND c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed > :after"
                                    + " AND c.whenConfirmed <= :before"
                                    + " AND c.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY c.whenConfirmed DESC") })
public class ShippedFile {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name of the algorithm used to calculate the checksum.
     */
    private String algorithm;

    /**
     * The value of the checksum of the shipped version of the file.
     */
    private Long checksum;

    /**
     * The collection of {@link Confirmation} instances associated with this object.
     */
    private List<Confirmation> confirmations;

    /**
     * The remote registration within the application that delivered this file.
     */
    private String remoteRegistration;

    /**
     * The remote registration within the application that delivered this file.
     */
    private String remoteTicket;

    /**
     * The unique key within the set of {@link TicketedFile} instances that matches
     * a {@link TicketedFile} instance to this object.
     */
    private int ticketedFileKey;

    /**
     * The date and time this ticketed file's transfer was abandoned.
     */
    private Date whenAbandoned;

    /**
     * The date and time after which the remote SPADE that delivered this file can
     * confirm its delivery.
     */
    private Date whenConfirmable;

    /**
     * The date and time that verification completed.
     */
    private Date whenVerificationCompleted;

    /**
     * The date and time that verification started.
     */
    private Date whenVerificationStarted;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ShippedFile() {
        // Needed by JPA
    }

    /**
     * Creates an instance of this class.
     *
     * @param key
     *            this object's unique key within the set of {@link ShippedFile}
     *            instances.
     */
    public ShippedFile(final int key) {
        ticketedFileKey = key;
    }

    /**
     * Creates an instance of this class.
     *
     * @param ticketedFile
     *            the {@link TicketedFile} instance associated with the created
     *            instance.
     */
    public ShippedFile(final TicketedFile ticketedFile) {
        this(ticketedFile.getTicketedFileKey());
    }

    /**
     * Creates an instance of this class.
     *
     * @param ticketedFile
     *            the {@link TicketedFile} instance associated with the created
     *            instance.
     * @param remoteTicket
     *            the remote registration within the application that delivered this
     *            file.
     * @param remoteRegistration
     *            the remote registration within the application that delivered this
     *            file.
     */
    public ShippedFile(final TicketedFile ticketedFile,
                       final String remoteTicket,
                       final String remoteRegistration) {
        this(ticketedFile);
        this.remoteTicket = remoteTicket;
        this.remoteRegistration = remoteRegistration;
    }

    // instance member method (alphabetic)

    /**
     * Returns the name of the algorithm used to calculate the checksum.
     *
     * @return the name of the algorithm used to calculate the checksum.
     */
    protected String getAlgorithm() {
        return algorithm;
    }

    /**
     * Returns the value of the checksum of the shipped version of the file.
     *
     * @return the value of the checksum of the shipped version of the file.
     */
    public Long getChecksum() {
        return checksum;
    }

    /**
     * Returns the collection of {@link Confirmation} instances associated with this
     * object.
     *
     * @return the collection of {@link Confirmation} instances associated with this
     *         object.
     */
    @OneToMany(mappedBy = "shippedFile")
    @OrderBy("whenConfirmed DESC")
    public List<Confirmation> getConfirmations() {
        return confirmations;
    }

    /**
     * Returns the last successful {@link Confirmation} instance for the specified
     * {@link KnownNeighbor}.
     *
     * @param neighbor
     *            the {@link KnownNeighbor} whose last successful
     *            {@link Confirmation} instance should be returned.
     * @param after
     *            the time on or after which the last successful
     *            {@link Confirmation} instance was confirmed.
     * @param before
     *            the time before which the last successful {@link Confirmation}
     *            instance was confirmed.
     * 
     * @return the last successful {@link Confirmation} instance for the specified
     *         {@link KnownNeighbor}.
     */
    public Confirmation getLastConfirmed(final KnownNeighbor neighbor,
                                         final Date after,
                                         final Date before) {
        final Date afterToUse;
        if (null == after) {
            afterToUse = new Date(0);
        } else {
            afterToUse = after;
        }
        Confirmation result = null;
        for (Confirmation confirmation : confirmations) {
            Date whenConfirmed = confirmation.getWhenConfirmed();
            if (null != whenConfirmed && !(whenConfirmed.before(afterToUse))
                && ((null == before) || before.after(whenConfirmed))) {
                if (null == result || whenConfirmed.after(result.getWhenConfirmed())) {
                    result = confirmation;
                }
            }
        }
        return result;
    }

    /**
     * Returns the remote registration within the application that delivered this
     * file.
     *
     * @return the remote registration within the application that delivered this
     *         file.
     */
    protected String getRemoteRegistration() {
        return remoteRegistration;
    }

    /**
     * Returns the remote ticket within the delivering application of this transfer.
     *
     * @return the remote ticket within the delivering application of this transfer.
     */
    public String getRemoteTicket() {
        return remoteTicket;
    }

    /**
     * Returns the unique key within the set of {@link TicketedFile} instances that
     * matches a {@link TicketedFile} instance to this object.
     *
     * @return the unique key within the set of {@link TicketedFile} instances that
     *         matches a {@link TicketedFile} instance to this object.
     */
    @Id
    public int getTicketedFileKey() {
        return ticketedFileKey;
    }

    /**
     * Returns the identity of the ticket issued for this object.
     *
     * @return the identity of the ticket issued for this object.
     */
    @Transient
    public String getTicketIdentity() {
        return Integer.toString(getTicketedFileKey());
    }

    /**
     * Returns the date and time this ticketed file's transfer was abandoned.
     *
     * @return the date and time this ticketed file's transfer was abandoned.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenAbandoned() {
        return whenAbandoned;
    }

    /**
     * Returns the date and time after which the remote SPADE that delivered this
     * file can confirm its delivery.
     *
     * @return the date and time after which the remote SPADE that delivered this
     *         file can confirm its delivery.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenConfirmable() {
        return whenConfirmable;
    }

    /**
     * Returns the date and time that verification completed.
     *
     * @return the date and time that verification completed.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenVerificationCompleted() {
        return whenVerificationCompleted;
    }

    /**
     * Returns the date and time that verification started.
     *
     * @return the date and time that verification started.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenVerificationStarted() {
        return whenVerificationStarted;
    }

    /**
     * Sets the name of the algorithm used to calculate the checksum.
     *
     * @param algorithm
     *            the name of the algorithm used to calculate the checksum.
     */
    public void setAlgorithm(String algorithm) {
        this.algorithm = algorithm;
    }

    /**
     * Sets the value of the checksum of the shipped version of the file.
     *
     * @param value
     *            the value of the checksum of the shipped version of the file.
     */
    public void setChecksum(final Long value) {
        checksum = value;
    }

    /**
     * Sets the collection of {@link Confirmation} instances associated with this
     * object.
     *
     * @param confirmations
     *            the collection of {@link Confirmation} instances associated with
     *            this object.
     */
    public void setConfirmations(List<Confirmation> confirmations) {
        this.confirmations = confirmations;
    }

    /**
     * Sets the remote registration within the application that delivered this file.
     *
     * @param registration
     *            the the remote registration within the application that delivered
     *            this file.
     */
    protected void setRemoteRegistration(final String registration) {
        this.remoteRegistration = registration;
    }

    /**
     * Sets the remote ticket identity with the delivering application of this
     * transfer.
     *
     * @param identity
     *            the remote ticket with the delivering application of this
     *            transfer.
     */
    protected void setRemoteTicket(final String identity) {
        this.remoteTicket = identity;
    }

    /**
     * Sets the unique key within the set of {@link TicketedFile} instances that
     * matches a {@link TicketedFile} instance to this object.
     *
     * @param key
     *            the unique key within the set of {@link TicketedFile} instances
     *            that matches a {@link TicketedFile} instance to this object.
     */
    protected void setTicketedFileKey(final int key) {
        ticketedFileKey = key;
    }

    /**
     * Sets the date and time this ticketed file's transfer was abandoned.
     *
     * @param dateTime
     *            the date and time this ticketed file's transfer was abandoned.
     */
    public void setWhenAbandoned(Date dateTime) {
        whenAbandoned = dateTime;
        for (Confirmation confirmation : confirmations) {
            if (null == confirmation.getWhenConfirmed()) {
                confirmation.setWhenAbandoned(dateTime);
            }
        }
    }

    /**
     * Sets the date and time after which the remote SPADE that delivered this file
     * can confirm its delivery.
     *
     * @param dateTime
     *            the date and time after which the remote SPADE that delivered this
     *            file can confirm its delivery.
     */
    public void setWhenConfirmable(Date dateTime) {
        this.whenConfirmable = dateTime;

    }

    /**
     * Sets the date and time that verification completed.
     *
     * @param dateTime
     *            the date and time that verification completed.
     */
    public void setWhenVerificationCompleted(Date dateTime) {
        this.whenVerificationCompleted = dateTime;
    }

    /**
     * Sets the date and time that verification started.
     *
     * @param dateTime
     *            the date and time that verification started.
     */
    public void setWhenVerificationStarted(Date dateTime) {
        this.whenVerificationStarted = dateTime;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
