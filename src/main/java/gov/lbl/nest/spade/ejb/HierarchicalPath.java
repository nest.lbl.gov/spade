package gov.lbl.nest.spade.ejb;

import java.io.File;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

/**
 * This class contains the information about a hierarchical path element.
 *
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "getPathsByName",
                            query = "SELECT p" + " FROM HierarchicalPath p"
                                    + " WHERE p.name = :name") })
public class HierarchicalPath {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The maximum length of the {@link #cachedPath}. This need to be equal or less
     * than the column length in the DB.
     */
    private static final int CACHED_PATH_LENGTH_MAX = 511;

    // private static member data

    // private instance member data

    /**
     * The cached full path name of this file or directory's parent, if
     * <code>null</code> then needs to be filled.
     */
    private String cachedPath;

    /**
     * The name of this file or directory.
     */
    private String name;

    /**
     * The parent, if any, of this file or directory.
     */
    private HierarchicalPath parent;

    /**
     * This object's unique key within the set of {@link HierarchicalPath}
     * instances.
     */
    private int pathKey;

    // constructors

    /**
     * Creates an instance of this class.
     */
    HierarchicalPath() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this file or directory.
     */
    protected HierarchicalPath(final String name) {
        setName(name);
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of this file or directory.
     * @param parent
     *            the parent, if any, of this file or directory.
     */
    protected HierarchicalPath(final String name,
                               final HierarchicalPath parent) {
        this(name);
        setParent(parent);
    }

    // instance member method (alphabetic)

    /**
     * Returns the cached full path name of this file or directory's parent, if
     * <code>null</code> then needs to be filled.
     *
     * @return the cached full path name of this file or directory's parent, if
     *         <code>null</code> then needs to be filled.
     */
    protected String getCachedPath() {
        if (null == cachedPath) {
            if (null == getParent() || getPathKey() == getParent().getPathKey()) {
                cachedPath = getName();
            } else {
                final String parentPath = getParent().getCachedPath();
                if ("".equals(parentPath)) {
                    cachedPath = getName();
                } else if ("/".equals(parentPath)) {
                    // Special case for root in Unix
                    cachedPath = parentPath + getName();
                } else {
                    cachedPath = parentPath + File.separator
                                 + getName();
                }
            }
            if (CACHED_PATH_LENGTH_MAX < cachedPath.length()) {
                throw new RuntimeException("Cacheed Path is too long for the DB. Extend this column in the DB");
            }
        }
        return cachedPath;
    }

    /**
     * Returns the name of this file or directory.
     *
     * @return the name of this file or directory.
     */
    protected String getName() {
        return name;
    }

    /**
     * Returns the parent, if any, of this file or directory.
     *
     * @return the parent, if any, of this file or directory.
     */
    @ManyToOne(optional = false)
    protected HierarchicalPath getParent() {
        return parent;
    }

    /**
     * Returns this object's unique key within the set of {@link HierarchicalPath}
     * instances.
     *
     * @return this object's unique key within the set of {@link HierarchicalPath}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getPathKey() {
        return pathKey;
    }

    /**
     * Sets the cached full path name of this file or directory's parent, if
     * <code>null</code> then needs to be filled.
     *
     * @param path
     *            the cached full path name of this file or directory's parent, if
     *            <code>null</code> then needs to be filled.
     */
    protected void setCachedPath(String path) {
        cachedPath = path;
    }

    /**
     * Sets the name of this file or directory.
     *
     * @param name
     *            the name of this file or directory.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the parent, if any, of this file or directory.
     *
     * @param parent
     *            the parent, if any, of this file or directory.
     */
    protected void setParent(HierarchicalPath parent) {
        this.parent = parent;
    }

    /**
     * Sets this object's unique key within the set of {@link HierarchicalPath}
     * instances.
     *
     * @param key
     *            the value of this object's key.
     */
    protected void setPathKey(int key) {
        pathKey = key;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
