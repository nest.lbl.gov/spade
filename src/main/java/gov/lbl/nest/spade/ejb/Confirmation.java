package gov.lbl.nest.spade.ejb;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 * This class contains the information the confirmation of a transfer to a
 * remote SPADE instance.
 *
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "confirmedByDestinationAfter",
                            query = "SELECT c" + " FROM Confirmation c"
                                    + " WHERE c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed >= :after"
                                    + " AND c.destination = :neighbor"
                                    + " ORDER BY c.whenConfirmed ASC"),
                @NamedQuery(name = "confirmedByDestinationBefore",
                            query = "SELECT c" + " FROM Confirmation c"
                                    + " WHERE c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed <= :before"
                                    + " AND c.destination = :neighbor"
                                    + " ORDER BY c.whenConfirmed DESC"),
                @NamedQuery(name = "confirmedByDestinationBetweenAsc",
                            query = "SELECT c" + " FROM Confirmation c"
                                    + " WHERE c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed >= :after"
                                    + " AND c.whenConfirmed < :before"
                                    + " AND c.destination = :neighbor"
                                    + " ORDER BY c.whenConfirmed ASC"),
                @NamedQuery(name = "confirmedByDestinationBetweenDesc",
                            query = "SELECT c" + " FROM Confirmation c"
                                    + " WHERE c.whenConfirmed IS NOT NULL"
                                    + " AND c.whenConfirmed > :after"
                                    + " AND c.whenConfirmed <= :before"
                                    + " AND c.destination = :neighbor"
                                    + " ORDER BY c.whenConfirmed DESC"),
                @NamedQuery(name = "getConfirmationsByShippedFile",
                            query = "SELECT c" + " FROM Confirmation c"
                                    + " WHERE c.shippedFile = :file"),
                @NamedQuery(name = "getConfirmationByTicketAndDestination",
                            query = "SELECT c" + " FROM Confirmation c"
                                    + " WHERE c.shippedFile.ticketedFileKey = :ticket"
                                    + " AND c.destination.name = :destination"),
                @NamedQuery(name = "getUnconfirmedCountByShippedFile",
                            query = "SELECT count(c)" + " FROM Confirmation c"
                                    + " WHERE c.shippedFile = :shippedFile"
                                    + " AND c.whenConfirmed IS NULL"),
                @NamedQuery(name = "getUndeliveredNeighborsByTicket",
                            query = "SELECT c.destination.name" + " FROM Confirmation c"
                                    + " WHERE c.shippedFile.ticketedFileKey = :ticket"
                                    + " AND c.whenDelivered IS NULL") })
public class Confirmation {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * This object's unique key within the set of {@link Confirmation} instances.
     */
    private int confirmationKey;

    /**
     * The remote SPADE instance to which the transfer was delivered.
     */
    private KnownNeighbor destination;

    /**
     * The {@link ShippedFile} whose verification this object is recording.
     */
    private ShippedFile shippedFile;

    /**
     * The date and time that this object was abandoned.
     */
    private Date whenAbandoned;

    /**
     * The date and time when the transfer was confirmed.
     */
    private Date whenConfirmed;

    /**
     * The date and time when the transfer was delivered.
     */
    private Date whenDelivered;

    // constructors

    /**
     * Creates an instance of this class
     */
    protected Confirmation() {
        // TODO Auto-generated constructor stub
    }

    /**
     * Creates an instance of this class
     *
     * @param shippedFile
     *            the {@link ShippedFile} whose verification this object is
     *            recording.
     * @param destination
     *            the remote SPADE instance to which the transfer was delivered.
     */
    public Confirmation(final ShippedFile shippedFile,
                        final KnownNeighbor destination) {
        setDestination(destination);
        setShippedFile(shippedFile);
    }

    // instance member method (alphabetic)

    /**
     * Returns this object's unique key within the set of {@link Confirmation}
     * instances.
     *
     * @return this object's unique key within the set of {@link Confirmation}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getConfirmationKey() {
        return confirmationKey;
    }

    /**
     * Returns the remote SPADE instance to which the transfer was delivered.
     *
     * @return the remote SPADE instance to which the transfer was delivered.
     */
    @ManyToOne(optional = false)
    public KnownNeighbor getDestination() {
        return destination;
    }

    /**
     * Returns the {@link ShippedFile} whose verification this object is recording.
     *
     * @return the {@link ShippedFile} whose verification this object is recording.
     */
    @ManyToOne(optional = false)
    public ShippedFile getShippedFile() {
        return shippedFile;
    }

    /**
     * Returns the date and time that this object was abandoned.
     *
     * @return the date and time that this object was abandoned.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenAbandoned() {
        return whenAbandoned;
    }

    /**
     * Returns the date and time when the transfer was confirmed.
     *
     * @return the date and time when the transfer was confirmed.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenConfirmed() {
        return whenConfirmed;
    }

    /**
     * Returns the date and time when the transfer was delivered.
     *
     * @return the date and time when the transfer was delivered.
     */
    @Temporal(TemporalType.TIMESTAMP)
    public Date getWhenDelivered() {
        return whenDelivered;
    }

    /**
     * Resets this object as if it was newly created.
     */
    public void reset() {
        whenAbandoned = null;
        whenConfirmed = null;
        whenDelivered = null;
    }

    /**
     * Sets this object's unique key within the set of {@link Confirmation}
     * instances.
     *
     * @param key
     *            this object's unique key within the set of {@link Confirmation}
     *            instances.
     */
    protected void setConfirmationKey(int key) {
        confirmationKey = key;
    }

    /**
     * Set the remote SPADE instance to which the transfer was delivered.
     *
     * @param destination
     *            the remote SPADE instance to which the transfer was delivered.
     */
    protected void setDestination(KnownNeighbor destination) {
        this.destination = destination;
    }

    /**
     * Sets the {@link ShippedFile} whose verification this object is recording.
     *
     * @param shippedFile
     *            the {@link ShippedFile} whose verification this object is
     *            recording.
     */
    protected void setShippedFile(ShippedFile shippedFile) {
        this.shippedFile = shippedFile;
    }

    /**
     * Sets the date and time that this object was abandoned.
     *
     * @param dateTime
     *            the date and time that this object was abandoned.
     */
    protected void setWhenAbandoned(Date dateTime) {
        this.whenAbandoned = dateTime;
    }

    /**
     * Sets the date and time when the transfer was confirmed.
     *
     * @param dateTime
     *            the date and time when the transfer was confirmed.
     */
    public void setWhenConfirmed(Date dateTime) {
        this.whenConfirmed = dateTime;
    }

    /**
     * Sets the date and time when the transfer was delivered.
     *
     * @param dateTime
     *            the date and time when the transfer was delivered.
     */
    public void setWhenDelivered(Date dateTime) {
        this.whenDelivered = dateTime;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
