/**
 * This package contains the classes that managed and implement the EJB
 * implementations for SPADE.
 *
 * @author patton
 */
package gov.lbl.nest.spade.ejb;