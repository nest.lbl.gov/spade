package gov.lbl.nest.spade.ejb;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

/**
 * This class contains the information about an attempt to dispatch a file to a
 * particular destination.
 *
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "getCourseBinnedCompleted",
                            query = "SELECT NEW gov.lbl.nest.spade.ejb.Bin(d.courseEpochCompleted, sum(d.ticketedFile.binarySize), count(d))"
                                    + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted >= :after"
                                    + " AND d.whenCompleted < :before"
                                    + " GROUP BY d.courseEpochCompleted"
                                    + " ORDER BY d.courseEpochCompleted"),
                @NamedQuery(name = "getDispatchesByTicketedFile",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.ticketedFile = :file"
                                    + " ORDER BY d.whenStarted"),
                @NamedQuery(name = "getOpenDispatchByTicket",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.ticketedFile.ticketedFileKey = :ticket"
                                    + " AND d.whenCompleted IS NULL"
                                    + " AND d.whenAbandoned IS NULL"),
                @NamedQuery(name = "getOpenDispatchByTicketAndDestination",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.ticketedFile.ticketedFileKey = :ticket"
                                    + " AND d.destination.name = :destination"
                                    + " AND d.whenCompleted IS NULL"
                                    + " AND d.whenAbandoned IS NULL"),
                @NamedQuery(name = "getFineBinnedCompleted",
                            query = "SELECT NEW gov.lbl.nest.spade.ejb.Bin(d.fineEpochCompleted, sum(d.ticketedFile.binarySize), count(d))" + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted >= :after"
                                    + " AND d.whenCompleted < :before"
                                    + " GROUP BY d.fineEpochCompleted"
                                    + " ORDER BY d.fineEpochCompleted"),
                @NamedQuery(name = "completedDispatchAfter",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted >= :after"
                                    + " ORDER BY d.whenStarted ASC"),
                @NamedQuery(name = "completedDispatchBefore",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted <= :before"
                                    + " ORDER BY d.whenStarted DESC"),
                @NamedQuery(name = "completedDispatchBetweenAsc",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted >= :after"
                                    + " AND d.whenStarted < :before"
                                    + " ORDER BY d.whenStarted ASC"),
                @NamedQuery(name = "completedDispatchBetweenDesc",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted > :after"
                                    + " AND d.whenStarted <= :before"
                                    + " ORDER BY d.whenStarted DESC"),
                @NamedQuery(name = "completedDispatchByDestinationAfter",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted >= :after"
                                    + " AND d.destination = :neighbor"
                                    + " ORDER BY d.whenStarted ASC"),
                @NamedQuery(name = "completedDispatchByDestinationBefore",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted <= :before"
                                    + " AND d.destination = :neighbor"
                                    + " ORDER BY d.whenStarted DESC"),
                @NamedQuery(name = "completedDispatchByDestinationBetweenAsc",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted >= :after"
                                    + " AND d.whenStarted < :before"
                                    + " AND d.destination = :neighbor"
                                    + " ORDER BY d.whenStarted ASC"),
                @NamedQuery(name = "completedDispatchByDestinationBetweenDesc",
                            query = "SELECT d" + " FROM Dispatch d"
                                    + " WHERE d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted > :after"
                                    + " AND d.whenStarted <= :before"
                                    + " AND d.destination = :neighbor"
                                    + " ORDER BY d.whenStarted DESC"),
                @NamedQuery(name = "completedDispatchByRegistrationAfter",
                            query = "SELECT d" + " FROM Dispatch d, TicketedFile t"
                                    + " WHERE d.ticketedFile = t"
                                    + " AND d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted >= :after"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY d.whenStarted ASC"),
                @NamedQuery(name = "completedDispatchByRegistrationBefore",
                            query = "SELECT d" + " FROM Dispatch d, TicketedFile t"
                                    + " WHERE d.ticketedFile = t"
                                    + " AND d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY d.whenStarted DESC"),
                @NamedQuery(name = "completedDispatchByRegistrationBetweenAsc",
                            query = "SELECT d" + " FROM Dispatch d, TicketedFile t"
                                    + " WHERE d.ticketedFile = t"
                                    + " AND d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted >= :after"
                                    + " AND d.whenStarted < :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY d.whenStarted ASC"),
                @NamedQuery(name = "completedDispatchByRegistrationBetweenDesc",
                            query = "SELECT d" + " FROM Dispatch d, TicketedFile t"
                                    + " WHERE d.ticketedFile = t"
                                    + " AND d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted > :after"
                                    + " AND d.whenStarted <= :before"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY d.whenStarted DESC"),
                @NamedQuery(name = "completedDispatchByRegistrationAndDestinationAfter",
                            query = "SELECT d" + " FROM Dispatch d, TicketedFile t"
                                    + " WHERE d.ticketedFile = t"
                                    + " AND d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted >= :after"
                                    + " AND d.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY d.whenStarted ASC"),
                @NamedQuery(name = "completedDispatchByRegistrationAndDestinationBefore",
                            query = "SELECT d" + " FROM Dispatch d, TicketedFile t"
                                    + " WHERE d.ticketedFile = t"
                                    + " AND d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted <= :before"
                                    + " AND d.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY d.whenStarted DESC"),
                @NamedQuery(name = "completedDispatchByRegistrationAndDestinationBetweenAsc",
                            query = "SELECT d" + " FROM Dispatch d, TicketedFile t"
                                    + " WHERE d.ticketedFile = t"
                                    + " AND d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted >= :after"
                                    + " AND d.whenStarted < :before"
                                    + " AND d.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY d.whenStarted ASC"),
                @NamedQuery(name = "completedDispatchByRegistrationAndDestinationBetweenDesc",
                            query = "SELECT d" + " FROM Dispatch d, TicketedFile t"
                                    + " WHERE d.ticketedFile = t"
                                    + " AND d.whenCompleted IS NOT NULL"
                                    + " AND d.whenStarted IS NOT NULL"
                                    + " AND d.whenStarted > :after"
                                    + " AND d.whenStarted <= :before"
                                    + " AND d.destination = :neighbor"
                                    + " AND t.localRegistration IN (:registrations)"
                                    + " ORDER BY d.whenStarted DESC"),
                @NamedQuery(name = "firstCompletedDate",
                            query = "SELECT min(d.courseEpochCompleted)" + " FROM  Dispatch d") })
public class Dispatch {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link #whenCompleted} field in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next
     * {@link TicketedFile#COURSE_BINNING} boundary.
     *
     * Note: This is an optimization as JQL has neither a <code>EXTRACT(EPOCH
     * FROM whencompleted)</code> function, nor a
     * <code>GROUP BY item_identifier</code> construct.
     */
    private Long courseEpochCompleted;

    /**
     * The member of this application's data movement network to which this attempt
     * was directed.
     */
    private KnownNeighbor destination;

    /**
     * This object's unique key within the set of {@link Dispatch} instances.
     */
    private int dispatchKey;

    /**
     * The {@link #whenCompleted} field in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next
     * {@link TicketedFile#FINE_BINNING} boundary.
     *
     * Note: This is an optimization as JQL has neither a <code>EXTRACT(EPOCH
     * FROM whencompleted)</code> function, nor a
     * <code>GROUP BY item_identifier</code> construct.
     */
    private Long fineEpochCompleted;

    /**
     * The {@link TicketedFile} whose dispatch this object is describing.
     */
    private TicketedFile ticketedFile;

    /**
     * The date and time that this object was abandoned.
     */
    private Date whenAbandoned;

    /**
     * The date and time that the dispatch completed.
     */
    private Date whenCompleted;

    /**
     * The date and time when the dispatch started.
     */
    private Date whenStarted;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Dispatch() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param file
     *            the {@link TicketedFile} whose dispatch this object is describing.
     * @param member
     *            the member of this application's data movement network to which
     *            this attempt was directed.
     */
    public Dispatch(final TicketedFile file,
                    final KnownNeighbor member) {
        setDestination(member);
        setTicketedFile(file);
        setWhenStarted(new Date());
    }

    // instance member method (alphabetic)

    /**
     * The whenCompleted property in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next
     * {@link TicketedFile#COURSE_BINNING} boundary.
     *
     * @return the whenCompleted property in terms of the number of seconds since
     *         the beginning of the epoch rounded up to the next
     *         {@link TicketedFile#COURSE_BINNING} boundary.
     */
    protected Long getCourseEpochCompleted() {
        return courseEpochCompleted;
    }

    /**
     * Returns the member of this application's data movement network to which this
     * attempt was directed.
     *
     * @return the member of this application's data movement network to which this
     *         attempt was directed.
     */
    @ManyToOne(optional = false)
    public KnownNeighbor getDestination() {
        return destination;
    }

    /**
     * Returns this object's unique key within the set of {@link Dispatch}
     * instances.
     *
     * @return this object's unique key within the set of {@link Dispatch}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getDispatchKey() {
        return dispatchKey;
    }

    /**
     * The whenCompleted property in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next
     * {@link TicketedFile#FINE_BINNING} boundary.
     *
     * @return the whenCompleted property in terms of the number of seconds since
     *         the beginning of the epoch rounded up to the next
     *         {@link TicketedFile#FINE_BINNING} boundary.
     */
    protected Long getFineEpochCompleted() {
        return fineEpochCompleted;
    }

    /**
     * Returns the {@link TicketedFile} whose dispatch this object is describing.
     *
     * @return the {@link TicketedFile} whose dispatch this object is describing.
     */
    @ManyToOne(optional = false)
    public TicketedFile getTicketedFile() {
        return ticketedFile;
    }

    /**
     * Returns the date and time that this object was abandoned.
     *
     * @return the date and time that this object was abandoned.
     */
    public Date getWhenAbandoned() {
        return whenAbandoned;
    }

    /**
     * Returns the date and time that the dispatch completed.
     *
     * @return the date and time that the dispatch completed.
     */
    public Date getWhenCompleted() {
        return whenCompleted;
    }

    /**
     * Returns the date and time when the dispatch started.
     *
     * @return the date and time when the dispatch started.
     */
    public Date getWhenStarted() {
        return whenStarted;
    }

    /**
     * Sets the whenCompleted property in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next
     * {@link TicketedFile#COURSE_BINNING} boundary.
     *
     * @param seconds
     *            the whenCompleted property in terms of the number of seconds since
     *            the beginning of the epoch rounded up to the next
     *            {@link TicketedFile#COURSE_BINNING} boundary.
     */
    protected void setCourseEpochCompleted(Long seconds) {
        courseEpochCompleted = seconds;
    }

    /**
     * Sets the member of this application's data movement network to which this
     * attempt was directed.
     *
     * @param destination
     *            the member of this application's data movement network to which
     *            this attempt was directed.
     */
    protected void setDestination(final KnownNeighbor destination) {
        this.destination = destination;
    }

    /**
     * Sets this object's unique key within the set of {@link Dispatch} instances.
     *
     * @param key
     *            this object's unique key within the set of {@link Dispatch}
     *            instances.
     */
    protected void setDispatchKey(final int key) {
        dispatchKey = key;
    }

    /**
     * Sets the whenCompleted property in terms of the number of seconds since the
     * beginning of the epoch rounded up to the next
     * {@link TicketedFile#FINE_BINNING} boundary.
     *
     * @param seconds
     *            the whenCompleted property in terms of the number of seconds since
     *            the beginning of the epoch rounded up to the next
     *            {@link TicketedFile#FINE_BINNING} boundary.
     */
    protected void setFineEpochCompleted(Long seconds) {
        fineEpochCompleted = seconds;
    }

    /**
     * Sets the {@link TicketedFile} whose dispatch this object is describing.
     *
     * @param file
     *            the {@link TicketedFile} whose dispatch this object is describing.
     */
    protected void setTicketedFile(TicketedFile file) {
        ticketedFile = file;
    }

    /**
     * Sets the date and time that this object was abandoned.
     *
     * @param dateTime
     *            the date and time that this object was abandoned.
     */
    public void setWhenAbandoned(final Date dateTime) {
        whenAbandoned = dateTime;
    }

    /**
     * Sets the date and time that the dispatch completed.
     *
     * @param dateTime
     *            the date and time that the dispatch completed.
     */
    public void setWhenCompleted(final Date dateTime) {
        whenCompleted = dateTime;
        if (null == dateTime) {
            setCourseEpochCompleted(null);
            setFineEpochCompleted(null);
            return;
        }
        final long timeInSeconds = TimeUnit.SECONDS.convert(dateTime.getTime(),
                                                            TimeUnit.MILLISECONDS);
        setCourseEpochCompleted((1 + (timeInSeconds / TicketedFile.COURSE_BINNING)) * TicketedFile.COURSE_BINNING);
        setFineEpochCompleted((1 + (timeInSeconds / TicketedFile.FINE_BINNING)) * TicketedFile.FINE_BINNING);
    }

    /**
     * Sets the date and time when the dispatch started.
     *
     * @param dateTime
     *            the date and time when the dispatch started.
     */
    protected void setWhenStarted(final Date dateTime) {
        whenStarted = dateTime;
    }
    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
