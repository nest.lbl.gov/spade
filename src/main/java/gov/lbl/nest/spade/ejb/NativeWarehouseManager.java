package gov.lbl.nest.spade.ejb;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.spade.interfaces.storage.Location;
import gov.lbl.nest.spade.interfaces.storage.WarehouseManager;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.TypedQuery;

/**
 * This class does the persistency for an implementation of the
 * {@link WarehouseManager} interface. It need to be sub-classes to provide the
 * necessary hooks to configurable details.
 *
 * @author patton
 */
public abstract class NativeWarehouseManager implements
                                             WarehouseManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Creates a {@link Location} instance from the supplied {@link Entry} instance.
     *
     * @param entry
     *            the {@link Entry} instances from which to create the
     *            {@link Location} instance.
     * @param filters
     *            TODO
     *
     * @return the created {@link Location} instance.
     */
    private static Location createLocation(final Entry entry,
                                           List<String> filters) {
        final String meta;
        final String data;
        final String wrapped;
        final String compressed;
        final HierarchicalPath metaPath = entry.getMetaPath();
        if (null != metaPath && (null == filters || filters.isEmpty()
                                 || filters.contains(METADATA_FILTER))) {
            meta = metaPath.getCachedPath();
        } else {
            meta = null;
        }
        final HierarchicalPath dataPath = entry.getDataPath();
        if (null != dataPath && (null == filters || filters.isEmpty()
                                 || filters.contains(DATA_FILTER))) {
            data = dataPath.getCachedPath();
        } else {
            data = null;
        }
        final HierarchicalPath wrappedPath = entry.getWrappedPath();
        if (null != wrappedPath && (null == filters || filters.isEmpty()
                                    || filters.contains(WRAPPED_FILTER))) {
            wrapped = wrappedPath.getCachedPath();
        } else {
            wrapped = null;
        }
        final HierarchicalPath compressedPath = entry.getCompressedPath();
        if (null != compressedPath && (null == filters || filters.isEmpty()
                                       || filters.contains(COMPRESSED_FILTER))) {
            compressed = compressedPath.getCachedPath();
        } else {
            compressed = null;
        }
        return new Location(entry.getIdentity(),
                            meta,
                            data,
                            wrapped,
                            compressed,
                            entry.getWhenLastModified());
    }

    /**
     * Returns the {@link HierarchicalPath}, if any, that is an exact match for the
     * supplied path.
     *
     * @param paths
     *            the collection of {@link HierarchicalPath} instances to search.
     * @param path
     *            the path to match.
     *
     * @return the {@link HierarchicalPath}, if any, that is an exact match for the
     *         supplied path.
     */
    private static HierarchicalPath findPath(final Collection<HierarchicalPath> paths,
                                             final String path) {
        for (HierarchicalPath warehousePath : paths) {
            if (path.equals(warehousePath.getCachedPath())) {
                return warehousePath;
            }
        }
        return null;
    }

    /**
     * Copies a file from the cache into the warehouse.
     *
     * @param cached
     *            the location of the file in the cache.
     * @param warehoused
     *            the location of the file in the warehouse.
     * @param root
     *            the path to the root of the warehouse.
     *
     * @return the public id that may be used retrieve this file's path in the
     *         warehouse.
     *
     * @throws IOException
     *             when the file can not be added.
     * @throws InterruptedException
     *             when the addition to the warehouse is uninterrupted.
     */
    private HierarchicalPath add(final File cached,
                                 final File warehoused,
                                 final File root) throws IOException,
                                                  InterruptedException {
        if (null == warehoused || null == root) {
            return null;
        }
        final File resolvedWarehoused;
        if (warehoused.isAbsolute()) {
            resolvedWarehoused = warehoused;
        } else {
            resolvedWarehoused = new File(root,
                                          warehoused.getPath());
        }

        final HierarchicalPath result = ensureExistance(resolvedWarehoused);
        final File directory = resolvedWarehoused.getParentFile();
        if (!directory.exists()) {
            directory.mkdirs();
        }
        if (directory.exists()) {
            if (resolvedWarehoused.exists()) {
                resolvedWarehoused.delete();
            }
            copy(cached,
                 resolvedWarehoused);
            return result;
        }
        throw new FileNotFoundException("The path for \"" + warehoused.getPath()
                                        + "\" could not be found or created in the warehouse");
    }

    @Override
    public Location add(final String bundle,
                        final File cachedMeta,
                        final File cachedData,
                        final File cachedWrapped,
                        final File cachedCompressed,
                        final File warehousedMeta,
                        final File warehousedData,
                        final File warehousedWrapped,
                        final File warehousedCompressed) throws InterruptedException,
                                                         IOException {
        HierarchicalPath dataPath = null;
        HierarchicalPath metaPath = null;
        HierarchicalPath wrappedPath = null;
        HierarchicalPath compressedPath = null;
        try {
            dataPath = add(cachedData,
                           warehousedData,
                           getDataRoot(bundle,
                                       cachedMeta));
            metaPath = add(cachedMeta,
                           warehousedMeta,
                           getMetaRoot(bundle,
                                       cachedMeta));
            wrappedPath = add(cachedWrapped,
                              warehousedWrapped,
                              getWrappedRoot(bundle,
                                             cachedMeta));
            compressedPath = add(cachedCompressed,
                                 warehousedCompressed,
                                 getCompressedRoot(bundle,
                                                   cachedMeta));
        } catch (IOException e) {
            if (null != wrappedPath) {
                final File file = new File(wrappedPath.getCachedPath());
                file.delete();
            }
            if (null != metaPath) {
                final File file = new File(metaPath.getCachedPath());
                file.delete();
            }
            if (null != dataPath) {
                final File file = new File(dataPath.getCachedPath());
                file.delete();
            }
            throw e;
        }
        final EntityManager entityManager = getEntityManager();
        final TypedQuery<Entry> query = entityManager.createNamedQuery("getEntryByIdentity",
                                                                       Entry.class);
        query.setParameter("identity",
                           bundle);
        try {
            final Entry entry = query.getSingleResult();
            if (null != dataPath) {
                entry.setDataPath(dataPath);
            }
            if (null != metaPath) {
                entry.setMetaPath(metaPath);
            }
            if (null != wrappedPath) {
                entry.setWrappedPath(wrappedPath);
            }
            if (null != compressedPath) {
                entry.setCompressedPath(compressedPath);
            }
            return createLocation(entry,
                                  null);
        } catch (NoResultException e) {
            final Entry result = new Entry(bundle,
                                           dataPath,
                                           metaPath,
                                           wrappedPath,
                                           compressedPath);
            entityManager.persist(result);
            return createLocation(result,
                                  null);
        } catch (NonUniqueResultException e) {
            // TODO : Return or throw a better result!
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Copies the file from an outside location into the warehouse.
     *
     * @param outside
     *            the outside location of the file
     * @param warehouse
     *            the location of the file in the warehouse.
     *
     * @throws InterruptedException
     *             when the copy is interrupted
     * @throws IOException
     *             when the copy can not be completed.
     */
    protected abstract void copy(final File outside,
                                 final File warehouse) throws IOException,
                                                       InterruptedException;

    @Override
    public void delete(final String warehouseId) {
        // TODO Auto-generated method stub

    }

    /**
     * Ensures the supplied path exists, creating it if necessary.
     *
     * @param path
     *            the the path whose existence is required.
     *
     * @return the {@link HierarchicalPath} supplied path.
     */
    private HierarchicalPath ensureExistance(final File path) {
        final HierarchicalPath result = findByFile(path);
        if (null != result) {
            return result;
        }
        final File parent = path.getParentFile();
        if (null == parent) {
            final HierarchicalPath additionPath;
            if ("/".equals(path.getPath())) {
                // Special case for root in Unix
                additionPath = new HierarchicalPath(path.getPath());
            } else {
                additionPath = new HierarchicalPath(path.getName());
            }
            additionPath.setParent(additionPath);
            getEntityManager().persist(additionPath);
            return additionPath;
        }
        final HierarchicalPath additionPath = new HierarchicalPath(path.getName(),
                                                                   ensureExistance(parent));
        getEntityManager().persist(additionPath);
        return additionPath;
    }

    /**
     * Returns the {@link HierarchicalPath}, if any, that is an exact match for the
     * supplied path.
     *
     * @param path
     *            the path to match.
     *
     * @return the {@link HierarchicalPath}, if any, that is an exact match for the
     *         supplied path.
     */
    private final HierarchicalPath findByFile(final File path) {
        final String pathToFind = path.getPath();
        final TypedQuery<HierarchicalPath> query = getEntityManager().createNamedQuery("getPathsByName",
                                                                                       HierarchicalPath.class);
        query.setParameter("name",
                           path.getName());
        final List<HierarchicalPath> paths = query.getResultList();
        return findPath(paths,
                        pathToFind);
    }

    @Override
    public Location getCompressed(final String warehouseId) {
        // TODO: Does not current return compressed placement.
        return null;
    }

    /**
     * Returns the warehouse root for the compressed file of the specified entry.
     *
     * @param bundle
     *            the name used to denote the bundle of files being added.
     * @param metaFile
     *            the location of the metadata file in the cache.
     *
     * @return the warehouse root for the compressed file of the specified entry.
     */
    protected abstract File getCompressedRoot(final String bundle,
                                              final File metaFile);

    @Override
    public List<Location> getData(List<String> warehouseIds) {
        final List<String> identities = new ArrayList<>(warehouseIds.size());
        for (Object warehouseId : warehouseIds) {
            if (!(warehouseId instanceof String)) {
                throw new IllegalArgumentException("Warehouse Ids must all be String objects");
            }
            identities.add((String) warehouseId);
        }
        final List<Location> result = new ArrayList<>(warehouseIds.size());
        if (null == warehouseIds || warehouseIds.isEmpty()) {
            return result;
        }
        final TypedQuery<Entry> query = getEntityManager().createNamedQuery("getDataByIdentities",
                                                                            Entry.class);
        query.setParameter("identities",
                           warehouseIds);
        Map<String, Entry> entries = new HashMap<>(warehouseIds.size());
        final Iterator<Entry> iterator = (query.getResultList()).iterator();
        while (iterator.hasNext()) {
            final Entry entry = iterator.next();
            entries.put(entry.getIdentity(),
                        entry);
        }
        for (String warehouseId : warehouseIds) {
            final Entry entry = entries.get(warehouseId);
            if (null != entry) {
                result.add(new Location(warehouseId,
                                        null,
                                        (entry.getDataPath()).getCachedPath(),
                                        null,
                                        null,
                                        entry.getWhenLastModified()));
            }
        }
        return result;
    }

    @Override
    public Location getData(final String warehouseId) {
        final List<String> list = new ArrayList<>(1);
        list.add(warehouseId);
        return getData(list).get(0);
    }

    /**
     * Returns the warehouse root for the data file of the specified entry.
     *
     * @param bundle
     *            the name used to denote the bundle of files being added.
     * @param metaFile
     *            the location of the metadata file in the cache.
     *
     * @return the warehouse root for the data file of the specified entry.
     */
    protected abstract File getDataRoot(final String bundle,
                                        final File metaFile);

    /**
     * Returns the {@link EntityManager} instance used by this object.
     *
     * @return the {@link EntityManager} instance used by this object.
     */
    protected abstract EntityManager getEntityManager();

    @Override
    public Location getLocation(final String warehouseId,
                                final List<String> filters) {
        final TypedQuery<Entry> query = getEntityManager().createNamedQuery("getEntryByIdentity",
                                                                            Entry.class);
        query.setParameter("identity",
                           warehouseId);
        try {
            final Entry entry = query.getSingleResult();
            return createLocation(entry,
                                  filters);
        } catch (NoResultException e) {
            return null;
        } catch (NonUniqueResultException e) {
            // TODO : Return or throw a better result!
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Location getMetadata(final String warehouseId) {
        final TypedQuery<HierarchicalPath> query = getEntityManager().createNamedQuery("getMetaPaths",
                                                                                       HierarchicalPath.class);
        query.setParameter("identity",
                           warehouseId);
        try {
            final HierarchicalPath path = query.getSingleResult();
            return new Location(warehouseId,
                                path.getCachedPath(),
                                null,
                                null,
                                null,
                                null);
        } catch (NoResultException e) {
            return null;
        } catch (NonUniqueResultException e) {
            // TODO : Return or throw a better result!
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Returns the warehouse root for the metadata file of the specified entry.
     *
     * @param bundle
     *            the name used to denote the bundle of files being added.
     * @param metaFile
     *            the location of the metadata file in the cache.
     *
     * @return the warehouse root for the metadata file of the specified entry.
     */
    protected abstract File getMetaRoot(final String bundle,
                                        final File metaFile);

    @Override
    public List<Location> getModifiedSince(final Date after,
                                           final Date before,
                                           final int maxCount) {
        final TypedQuery<Entry> query;
        if (null == before) {
            query = getEntityManager().createNamedQuery("getModifiedSince",
                                                        Entry.class);
        } else {
            query = getEntityManager().createNamedQuery("getModifiedBetween",
                                                        Entry.class);
            query.setParameter("before",
                               before);
        }
        if (null == after) {
            query.setParameter("after",
                               new Date(0));
        } else {
            query.setParameter("after",
                               after);
        }
        if (maxCount > 0) {
            query.setMaxResults(maxCount);
        }
        final List<Entry> entries = query.getResultList();
        final List<Location> result = new ArrayList<>(entries.size());
        if (entries.isEmpty()) {
            return result;
        }
        for (Entry entry : entries) {
            result.add(createLocation(entry,
                                      null));
        }
        return result;
    }

    @Override
    public List<Location> getModifiedSince(final Date dateTime,
                                           final int maxCount) {
        final TypedQuery<Entry> query = getEntityManager().createNamedQuery("getModifiedSince",
                                                                            Entry.class);
        query.setParameter("lastDateTime",
                           dateTime);
        if (maxCount > 0) {
            query.setMaxResults(maxCount);
        }
        final List<Entry> entries = query.getResultList();
        final List<Location> result = new ArrayList<>(entries.size());
        if (entries.isEmpty()) {
            return result;
        }
        for (Entry entry : entries) {
            result.add(createLocation(entry,
                                      null));
        }
        return result;
    }

    @Override
    public Location getWrapped(final String warehouseId) {
        // TODO: Does not current return compressed placement.
        return null;
    }

    // static member methods (alphabetic)

    /**
     * Returns the warehouse root for the wrapped file of the specified entry.
     *
     * @param bundle
     *            the name used to denote the bundle of files being added.
     * @param metaFile
     *            the location of the metadata file in the cache.
     *
     * @return the warehouse root for the wrapped file of the specified entry.
     */
    protected abstract File getWrappedRoot(final String bundle,
                                           final File metaFile);

    @Override
    public Object move(final String bundle,
                       final File movedData,
                       final File movedMeta,
                       final File movedWrapped,
                       final File movedCompressed) {
        throw new UnsupportedOperationException();
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
