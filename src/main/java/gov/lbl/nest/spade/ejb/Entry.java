package gov.lbl.nest.spade.ejb;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 * This class contains the information about a entry in the warehouse.
 *
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "getEntryByIdentity",
                            query = "SELECT e " + " FROM Entry e"
                                    + " WHERE e.identity = :identity"),
                @NamedQuery(name = "getMetaPathByIdentity",
                            query = "SELECT e.metaPath " + " FROM Entry e"
                                    + " WHERE e.identity = :identity"
                                    + " AND e.metaPath IS NOT NULL"),
                @NamedQuery(name = "getDataByIdentities",
                            query = "SELECT e " + " FROM Entry e"
                                    + " WHERE e.identity IN (:identities)"
                                    + " AND e.dataPath IS NOT NULL"),
                @NamedQuery(name = "getModifiedSince",
                            query = "SELECT e" + " FROM Entry e"
                                    + " WHERE e.whenLastModified IS NOT NULL"
                                    + " AND e.whenLastModified >= :after"
                                    + " ORDER BY e.whenLastModified ASC"),
                @NamedQuery(name = "getModifiedBetween",
                            query = "SELECT e" + " FROM Entry e"
                                    + " WHERE e.whenLastModified IS NOT NULL"
                                    + " AND e.whenLastModified >= :after"
                                    + " AND e.whenLastModified < :before"
                                    + " ORDER BY e.whenLastModified ASC") })
public class Entry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link HierarchicalPath} of the compressed file in the warehouse.
     */
    private HierarchicalPath compressedPath;

    /**
     * The {@link HierarchicalPath} of the data file in the warehouse.
     */
    private HierarchicalPath dataPath;

    /**
     * This object's unique key within the set of {@link Entry} instances.
     */
    private int entryKey;

    /**
     * The external identity of this entry.
     */
    private String identity;

    /**
     * The {@link HierarchicalPath} of the metadata file in the warehouse.
     */
    private HierarchicalPath metaPath;

    /**
     * The date and time this entry was last modified.
     */
    private Date whenLastModified;

    /**
     * The {@link HierarchicalPath} of the wrapped file in the warehouse.
     */
    private HierarchicalPath wrappedPath;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Entry() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the external identity of this entry.
     * @param dataPath
     *            the {@link HierarchicalPath} of the data file in the warehouse.
     * @param metaPath
     *            the {@link HierarchicalPath} of the metadata file in the
     *            warehouse.
     * @param wrappedPath
     *            the {@link HierarchicalPath} of the wrapped file in the warehouse.
     * @param compressedPath
     *            the {@link HierarchicalPath} of the compressed file in the
     *            warehouse.
     */
    protected Entry(final String identity,
                    final HierarchicalPath dataPath,
                    final HierarchicalPath metaPath,
                    final HierarchicalPath wrappedPath,
                    final HierarchicalPath compressedPath) {
        this.compressedPath = compressedPath;
        this.dataPath = dataPath;
        this.identity = identity;
        this.metaPath = metaPath;
        whenLastModified = new Date();
        this.wrappedPath = wrappedPath;
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link HierarchicalPath} of the compressed file in the warehouse.
     *
     * @return the {@link HierarchicalPath} of the compressed file in the warehouse.
     */
    @OneToOne
    protected HierarchicalPath getCompressedPath() {
        return compressedPath;
    }

    /**
     * Returns the {@link HierarchicalPath} of the data file in the warehouse.
     *
     * @return the {@link HierarchicalPath} of the data file in the warehouse.
     */
    @OneToOne
    protected HierarchicalPath getDataPath() {
        return dataPath;
    }

    /**
     * Returns this object's unique key within the set of {@link Entry} instances.
     *
     * @return this object's unique key within the set of {@link Entry} instances.
     */
    @Id
    @GeneratedValue
    protected int getEntryKey() {
        return entryKey;
    }

    /**
     * Returns the external identity of this entry.
     *
     * @return the external identity of this entry.
     */
    protected String getIdentity() {
        return identity;
    }

    /**
     * Returns the {@link HierarchicalPath} of the metadata file in the warehouse.
     *
     * @return the {@link HierarchicalPath} of the metadata file in the warehouse.
     */
    @OneToOne
    protected HierarchicalPath getMetaPath() {
        return metaPath;
    }

    /**
     * Returns the date and time this entry was last modified.
     *
     * @return the date and time this entry was last modified.
     */
    @Temporal(TemporalType.TIMESTAMP)
    protected Date getWhenLastModified() {
        return whenLastModified;
    }

    /**
     * Returns the {@link HierarchicalPath} of the wrapped file in the warehouse.
     *
     * @return the {@link HierarchicalPath} of the wrapped file in the warehouse.
     */
    @OneToOne
    protected HierarchicalPath getWrappedPath() {
        return wrappedPath;
    }

    /**
     * Sets the {@link HierarchicalPath} of the compressed file in the warehouse.
     *
     * @param path
     *            the {@link HierarchicalPath} of the compressed file in the
     *            warehouse.
     */
    protected void setCompressedPath(HierarchicalPath path) {
        if ((null == compressedPath && null == path) || (null != compressedPath && compressedPath.getPathKey() == path.getPathKey())) {
            return;
        }
        setWhenLastModified(new Date());
        compressedPath = path;
    }

    /**
     * Sets the {@link HierarchicalPath} of the data file in the warehouse.
     *
     * @param path
     *            the {@link HierarchicalPath} of the data file in the warehouse.
     */
    protected void setDataPath(HierarchicalPath path) {
        if ((null == dataPath && null == path) || (null != dataPath && dataPath.getPathKey() == path.getPathKey())) {
            return;
        }
        setWhenLastModified(new Date());
        dataPath = path;
    }

    /**
     * Sets this object's unique key within the set of {@link Entry} instances.
     *
     * @param key
     *            the value of this object's key.
     */
    protected void setEntryKey(int key) {
        entryKey = key;
    }

    /**
     * Sets the external identity of this entry.
     *
     * @param identity
     *            the external identity of this entry.
     */
    protected void setIdentity(String identity) {
        this.identity = identity;
    }

    /**
     * Sets the {@link HierarchicalPath} of the metadata file in the warehouse.
     *
     * @param path
     *            the {@link HierarchicalPath} of the metadata file in the
     *            warehouse.
     */
    protected void setMetaPath(HierarchicalPath path) {
        if ((null == metaPath && null == path) || (null != metaPath && metaPath.getPathKey() == path.getPathKey())) {
            return;
        }
        setWhenLastModified(new Date());
        metaPath = path;
    }

    /**
     * Sets the date and time this entry was last modified.
     *
     * @param dateTime
     *            the date and time this entry was last modified.
     */
    protected void setWhenLastModified(Date dateTime) {
        whenLastModified = dateTime;
    }

    /**
     * Sets the {@link HierarchicalPath} of the wrapped file in the warehouse.
     *
     * @param path
     *            the {@link HierarchicalPath} of the wrapped file in the warehouse.
     */
    protected void setWrappedPath(HierarchicalPath path) {
        if ((null == wrappedPath && null == path) || (null != wrappedPath && wrappedPath.getPathKey() == path.getPathKey())) {
            return;
        }
        setWhenLastModified(new Date());
        wrappedPath = path;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
