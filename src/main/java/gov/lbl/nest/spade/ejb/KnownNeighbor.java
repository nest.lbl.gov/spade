package gov.lbl.nest.spade.ejb;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * This class contains the information about a remote SPADE instance that can
 * delivery files to or receive file from this SPADE instance. the file.
 *
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "getNeighborByName",
                            query = "SELECT n" + " FROM KnownNeighbor n"
                                    + " WHERE n.name = :name") })
// The following is for compatibility with v3.0 DB
@Table(name = "neighbor")
public class KnownNeighbor {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The name of the remote SPADE deployment.
     */
    private String name;

    /**
     * This object's unique key within the set of {@link KnownNeighbor} instances.
     */
    private int neighborKey;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected KnownNeighbor() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            the name of the remote SPADE deployment.
     */
    public KnownNeighbor(final String name) {
        setName(name);
    }

    // instance member method (alphabetic)

    /**
     * Returns the name of the remote SPADE deployment.
     *
     * @return the name of the remote SPADE deployment.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns this object's unique key within the set of {@link KnownNeighbor}
     * instances.
     *
     * @return this object's unique key within the set of {@link KnownNeighbor}
     *         instances.
     */
    @Id
    @GeneratedValue
    protected int getNeighborKey() {
        return neighborKey;
    }

    /**
     * Sets the name of the remote SPADE deployment.
     *
     * @param name
     *            the name of the remote SPADE deployment.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets this object's unique key within the set of {@link KnownNeighbor}
     * instances.
     *
     * @param key
     *            this object's unique key within the set of {@link KnownNeighbor}
     *            instances.
     */
    protected void setNeighborKey(int key) {
        neighborKey = key;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
