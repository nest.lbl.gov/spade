package gov.lbl.nest.spade.ejb;

import java.util.Date;

import gov.lbl.nest.spade.registry.internal.InboundRegistration;
import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;

/**
 * This class contains the information about a registration with the context of
 * this application's data movement network.
 *
 * @author patton
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({ @NamedQuery(name = "getInboundRegistrations",
                            query = "SELECT r" + " FROM KnownInboundRegistration r"
                                    + " WHERE r.whenSuperseded IS NULL"),
                @NamedQuery(name = "getInboundRegistrationsByLocalId",
                            query = "SELECT r" + " FROM KnownInboundRegistration r"
                                    + " WHERE r.localId = :localId"
                                    + " AND r.whenSuperseded IS NULL"),
                @NamedQuery(name = "getInboundRegistrationsByLocalIds",
                            query = "SELECT r" + " FROM KnownInboundRegistration r"
                                    + " WHERE r.localId IN (:localIds)"
                                    + " AND r.whenSuperseded IS NULL"),
                @NamedQuery(name = "getInboundRegistrationsByLocalIdsAndNeighbor",
                            query = "SELECT r" + " FROM KnownInboundRegistration r"
                                    + " WHERE r.localId IN (:localIds)"
                                    + " AND r.knownNeighbor = :neighbor"
                                    + " AND r.whenSuperseded IS NULL"),
                @NamedQuery(name = "getInboundRegistrationsByNeighbor",
                            query = "SELECT r" + " FROM KnownInboundRegistration r"
                                    + " WHERE r.knownNeighbor = :neighbor"
                                    + " AND r.whenSuperseded IS NULL") })
@Table(name = "inboundregistration")
public class KnownInboundRegistration extends
                                      KnownRegistration {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link KnownNeighbor} from which file matching this registration are
     * delivered.
     */
    private KnownNeighbor knownNeighbor;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected KnownInboundRegistration() {
    }

    /**
     * Create an instance of this class.
     *
     * @param registration
     *            the {@link InboundRegistration} instance used to create this
     *            object.
     * @param neighbor
     *            the {@link KnownNeighbor} who is delivering to this registration.
     */
    public KnownInboundRegistration(InboundRegistration registration,
                                    KnownNeighbor neighbor) {
        setKnownNeighbor(neighbor);
        setLocalId(registration.getLocalId());
    }

    /**
     * Create an instance of this class that supersedes an existing one.
     *
     * @param registration
     *            the {@link InboundRegistration} instance used to create this
     *            object.
     * @param neighbor
     *            the {@link KnownNeighbor} who is delivering to this registration.
     * @param predecessor
     *            the {@link KnownInboundRegistration} instance that the created one
     *            will supersede.
     */
    public KnownInboundRegistration(InboundRegistration registration,
                                    KnownNeighbor neighbor,
                                    KnownInboundRegistration predecessor) {
        predecessor.setWhenSuperseded(new Date());
        setKnownNeighbor(neighbor);
        setLocalId(registration.getLocalId());
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link KnownNeighbor} from which file matching this registration
     * are delivered.
     *
     * @return the {@link KnownNeighbor} from which file matching this registration
     *         are delivered.
     */
    @ManyToOne(optional = false)
    public KnownNeighbor getKnownNeighbor() {
        return knownNeighbor;
    }

    /**
     * Sets the {@link KnownNeighbor} from which file matching this registration are
     * delivered.
     *
     * @param neighbor
     *            the {@link KnownNeighbor} from which file matching this
     *            registration are delivered.
     */
    protected void setKnownNeighbor(KnownNeighbor neighbor) {
        knownNeighbor = neighbor;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
