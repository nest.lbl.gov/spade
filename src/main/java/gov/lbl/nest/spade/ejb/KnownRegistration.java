package gov.lbl.nest.spade.ejb;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

/**
 * This class contains the information about a registration within the context
 * of this application.
 *
 * @author patton
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NamedQueries({ @NamedQuery(name = "getKnownRegistrations",
                            query = "SELECT r" + " FROM KnownRegistration r"
                                    + " WHERE r.whenSuperseded IS NULL"),
                @NamedQuery(name = "getKnownRegistrationsByLocalId",
                            query = "SELECT r" + " FROM KnownRegistration r"
                                    + " WHERE r.localId = :localId"
                                    + " AND r.whenSuperseded IS NULL"),
                @NamedQuery(name = "getKnownRegistrationsByLocalIds",
                            query = "SELECT r" + " FROM KnownRegistration r"
                                    + " WHERE r.localId IN (:localIds)"
                                    + " AND r.whenSuperseded IS NULL") })
public abstract class KnownRegistration {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * This object's publicly visible identity.
     */
    protected String localId;

    /**
     * This object's unique key within the set of {@link KnownLocalRegistration}
     * instances.
     */
    private int registrationKey;

    /**
     * The date and time this object was superseded by one with the same name.
     */
    private Date whenSuperseded;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected KnownRegistration() {
    }

    /**
     * Returns this object's publicly visible identity.
     *
     * @return this object's publicly visible identity.
     */
    protected String getLocalId() {
        return localId;
    }

    /**
     * Returns this object's unique key within the set of
     * {@link KnownLocalRegistration} instances.
     *
     * @return this object's unique key within the set of
     *         {@link KnownLocalRegistration} instances.
     */
    @Id
    @GeneratedValue
    protected int getRegistrationKey() {
        return registrationKey;
    }

    /**
     * Returns the date and time this object was superseded by one with the same
     * name.
     *
     * @return the date and time this object was superseded by one with the same
     *         name.
     */
    @Temporal(TemporalType.TIMESTAMP)
    protected Date getWhenSuperseded() {
        return whenSuperseded;
    }

    /**
     * Sets this object's publicly visible identity.
     *
     * @param publicId
     *            this object's publicly visible identity.
     */
    protected void setLocalId(final String publicId) {
        localId = publicId;
    }

    /**
     * Sets this object's unique key within the set of
     * {@link KnownLocalRegistration} instances.
     *
     * @param key
     *            this object's unique key within the set of
     *            {@link KnownLocalRegistration} instances.
     */
    protected void setRegistrationKey(final int key) {
        registrationKey = key;
    }

    /**
     * Sets the date and time this object was superseded by one with the same name.
     *
     * @param dataTime
     *            the date and time this object was superseded by one with the same
     *            name.
     */
    public void setWhenSuperseded(final Date dataTime) {
        whenSuperseded = dataTime;
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
