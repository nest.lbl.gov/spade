package gov.lbl.nest.spade.services.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.config.InboundTransfer;
import gov.lbl.nest.spade.config.Neighbor;
import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.ejb.KnownNeighbor;
import gov.lbl.nest.spade.interfaces.SpadeDB;
import jakarta.annotation.Resource;
import jakarta.ejb.LockType;
import jakarta.ejb.SessionContext;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.TypedQuery;

/**
 * This class contains all of the neighbors loaded into the application.
 *
 * @author patton
 */
@Singleton
/** The class does it own concurrency management, so can use READ lock. */
@jakarta.ejb.Lock(LockType.READ)
public class LoadedNeighborhood {

    // public static final member data

    // protected static final member data

    // static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(LoadedNeighborhood.class);

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance used by this class.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @SpadeDB
    private EntityManager entityManager;

    /**
     * The mapping of {@link InboundTransfer} instances to their names.
     */
    private final Map<String, InboundTransfer> inboundTransfers = new HashMap<>();

    /**
     * True if the registrations have been loaded.
     */
    private boolean loaded = false;

    /**
     * The {@link Lock} instance used to make sure that the registration loading is
     * serialized.
     */
    private Lock lock = new ReentrantLock();

    /**
     * The {@link Neighbor} instance, if any, representing this deployment of the
     * application.
     */
    private Neighbor myself;

    /**
     * The mapping of {@link Neighbor} instances to their names.
     */
    private Map<String, Neighbor> neighborByName = new HashMap<>();

    /**
     * The mapping of {@link OutboundTransfer} instances to their names.
     */
    private final Map<String, OutboundTransfer> outboundTransfers = new HashMap<>();

    /**
     * This object's {@link SessionContext} instance used to trigger new
     * transactions.
     */
    @Resource
    SessionContext sessionContext;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected LoadedNeighborhood() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param configuration
     *            the {@link Configuration} instance used by the created object.
     * @param entityManager
     *            the {@link EntityManager} instance used by this object.
     */
    public LoadedNeighborhood(final Configuration configuration,
                              final EntityManager entityManager) {
        this.configuration = configuration;
        this.entityManager = entityManager;
    }

    // instance member method (alphabetic)

    private KnownNeighbor findKnownNeighbor(String identity) {
        final TypedQuery<KnownNeighbor> query = entityManager.createNamedQuery("getNeighborByName",
                                                                               KnownNeighbor.class);
        query.setParameter("name",
                           identity);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            return null;
        } catch (NonUniqueResultException e) {
            LOG.error("More than one KnownNeighbor named \"" + identity
                      + "\". This need to be fixed!");
            return null;
        }
    }

    /**
     * Returns the {@link KnownNeighbor} with the specified identity or
     * <code>null</code>.
     *
     * @param identity
     *            the identity to match.
     *
     * @return the {@link Neighbor} with the specified identity or
     *         <code>null</code>.
     */
    public KnownNeighbor getKnownNeighbor(String identity) {
        loadNeighbors();
        return findKnownNeighbor(identity);
    }

    /**
     * Returns the {@link KnownNeighbor} with the specified identity or
     * <code>null</code>.
     *
     * @param identities
     *            the list of identities to match.
     *
     * @return the {@link Neighbor} with the specified identity or
     *         <code>null</code>.
     */
    public List<KnownNeighbor> getKnownNeighbors(List<String> identities) {
        loadNeighbors();
        final TypedQuery<KnownNeighbor> query = entityManager.createNamedQuery("getNeighborsByName",
                                                                               KnownNeighbor.class);
        query.setParameter("names",
                           identities);
        return query.getResultList();
    }

    /**
     * Returns the {@link Neighbor} with the specified identity or
     * <code>null</code>.
     *
     * @param identity
     *            the identity to match.
     *
     * @return the {@link Neighbor} with the specified identity or
     *         <code>null</code>.
     */
    public Neighbor getNeighbor(String identity) {
        loadNeighbors();
        return neighborByName.get(identity);
    }

    /**
     * Returns the collection of {@link Neighbor} instances that make up the
     * neighborhood.
     *
     * @return the collection of {@link Neighbor} instances that make up the
     *         neighborhood.
     */
    public Collection<Neighbor> getNeighborhood() {
        loadNeighbors();
        return neighborByName.values();
    }

    /**
     * Returns the {@link OutboundTransfer} instance that matches the name, or
     * <code>null</code> otherwise.
     *
     * @param name
     *            the name of the {@link OutboundTransfer} instance to return.
     *
     * @return the {@link OutboundTransfer} instance that matches the name, or
     *         <code>null</code> otherwise.
     */
    public OutboundTransfer getOutboundTransfer(String name) {
        loadNeighbors();
        return outboundTransfers.get(name);
    }

    /**
     * Returns the {@link Neighbor} instance, if any, representing this deployment
     * of the application.
     *
     * @return the {@link Neighbor} instance, if any, representing this deployment
     *         of the application.
     */
    public Neighbor getSelf() {
        loadNeighbors();
        return myself;
    }

    /**
     * Loads the known neighbors into this object.
     */
    private void loadNeighbors() {
        if (!loaded) {
            lock.lock();
            try {
                if (!loaded) {
                    if (null == sessionContext) {
                        prepareNeighbors();
                    } else {
                        final LoadedNeighborhood self = sessionContext.getBusinessObject(LoadedNeighborhood.class);
                        self.prepareNeighbors();
                    }
                    loaded = true;
                }
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Prepares the known inbound transfers for used by this object.
     */
    private void prepareInboundTransfers() {
        final List<InboundTransfer> transfers = configuration.getInboundTransfers();
        if (null != transfers) {
            for (InboundTransfer transfer : transfers) {
                inboundTransfers.put(transfer.getName(),
                                     transfer);
                final String neighbor = transfer.getNeighbor();
                if (!neighborByName.containsKey(neighbor)) {
                    neighborByName.put(neighbor,
                                       new Neighbor(neighbor));
                }
            }
        }
    }

    /**
     * Prepares the known neighbors for used by this object, within its own
     * transaction.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void prepareNeighbors() {
        final String selfName = (configuration.getAssembly()).getName();
        LOG.info("Assembly name is " + selfName);
        final List<Neighbor> neighbors = configuration.getNeighborhood();
        if (null != neighbors) {
            for (Neighbor neighbor : neighbors) {
                final String name = neighbor.getName();
                if (null == name) {
                    LOG.warn("A neighbor has been found with no name, so will be ignored");
                } else {
                    if (null != selfName && selfName.equals(name)) {
                        LOG.info("Found myself within the neighborhood");
                        myself = neighbor;
                    } else {
                        LOG.info("Found neighbor named " + name);
                    }
                    neighborByName.put(name,
                                       neighbor);
                }
            }
        }
        prepareInboundTransfers();
        prepareOutboundTransfers();
        for (Neighbor neighbor : neighborByName.values()) {
            final String name = neighbor.getName();
            final KnownNeighbor knownNeighbor = findKnownNeighbor(name);
            if (null == knownNeighbor) {
                entityManager.persist(new KnownNeighbor(name));
            }
        }
    }

    private void prepareOutboundTransfers() {
        final List<OutboundTransfer> transfers = configuration.getOutboundTransfers();
        if (null != transfers) {
            for (OutboundTransfer transfer : transfers) {
                outboundTransfers.put(transfer.getName(),
                                      transfer);
                final String neighbor = transfer.getNeighbor();
                if (!neighborByName.containsKey(neighbor)) {
                    neighborByName.put(neighbor,
                                       new Neighbor(neighbor));
                }
            }
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
