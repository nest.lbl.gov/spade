package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.grid.ProxyManager;
import gov.lbl.nest.spade.registry.ExternalLocation;

/**
 * This class manages transfers that use X509 proxies for authentication.
 *
 * @author patton
 */
public abstract class X509ProxyTransfer extends
                                        ThirdPartyFileTransfer {

    /**
     * The protocol that specified the local file system should be used.
     */
    protected static final String LOCAL_FILESYSTEM_PROTOCOL = "file";

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(X509ProxyTransfer.class);

    /**
     * The string used to separate path elements.
     */
    protected static final String PATH_SEPARATOR = "/";

    /**
     * The "protocol://host" separator string.
     */
    protected static final String PROTOCOL_SEPARATOR = "://";

    /**
     * The {@link ProxyManager} used by this class.
     */
    private static AtomicBoolean initialProxyCreated = new AtomicBoolean();

    /**
     * The {@link ProxyManager} used by this class.
     */
    private static ProxyManager proxyManager;

    /**
     * Crates an instance of this class.
     */
    protected X509ProxyTransfer() {
        if (!initialProxyCreated.get()) {
            synchronized (initialProxyCreated) {
                if (!initialProxyCreated.get()) {
                    proxyManager = new ProxyManager();
                    try {
                        if (proxyManager.issueProxy()) {
                            LOG.info("Create VOMS proxy for " + getClass().getSimpleName());
                        } else {
                            LOG.info("No VOMS proxy was created for " + getClass().getSimpleName());
                        }
                    } catch (CertificateException
                             | IOException
                             | InterruptedException e) {
                        LOG.error("Failed in attempt to for " + getClass().getSimpleName());
                        e.printStackTrace();
                    }
                    initialProxyCreated.set(true);
                }
            }
        }
    }

    /**
     * Returns the port to use to deliver the "far" file.
     *
     * @return the port to use to deliver the "far" file.
     */
    protected abstract String getFarPort();

    /**
     * Returns the protocol to use to deliver the "far" file.
     *
     * @return the protocol to use to deliver the "far" file.
     */
    protected abstract String getFarProtocol();

    /**
     * Returns the {@link String} instances containing the flags, if any, to use
     * with the transfer command.
     *
     * @return the {@link String} instancess containing the flags, if any, to use
     *         with the transfer command.
     */
    protected abstract String[] getFlags();

    /**
     * Returns the command to execute to perform the transfer.
     *
     * @return the command to execute to perform the transfer.
     */
    protected abstract String getTransferCommand();

    @Override
    protected void outboundTransfer(final File nearFile,
                                    final ExternalLocation location,
                                    final String targetName,
                                    final String nearProtocol) throws IOException,
                                                               InterruptedException {
        final String host = location.getHost();
        if (null == host || LocalhostTransfer.LOCALHOST.equals(host)) {
            LocalhostTransfer.copy(nearFile,
                                   new File(LocalhostTransfer.getResolvedPath(location.getDirectory()),
                                            targetName));
        } else {
            final String local;
            if (!LOCAL_FILESYSTEM_PROTOCOL.equals(nearProtocol)) {

                // Trying to do third party transfer
                Path path = nearFile.toPath();
                final Path real;
                if (Files.isSymbolicLink(path)) {
                    real = Files.readSymbolicLink(path);
                    LOG.debug("Resolved symbolic link \"" + path
                              + "\" to path \""
                              + real
                              + "\"");
                } else {
                    real = path;
                }
                final File mappedFile = getMountPointsMapping().peekMappedFile(real.toString());
                if (null == mappedFile) {
                    LOG.warn("Could not do Third Party transfer, falling back to local transfer");
                    local = nearFile.getCanonicalPath();
                } else {
                    local = nearProtocol + PROTOCOL_SEPARATOR
                            + mappedFile.toString();
                }
            } else {
                local = nearFile.getCanonicalPath();
            }
            final String remote = host + getFarPort()
                                  + PATH_SEPARATOR
                                  + location.getDirectory()
                                  + PATH_SEPARATOR
                                  + targetName;
            final String farProtocol = getFarProtocol();
            final String transferCommand = getTransferCommand();
            final String[] vanillaCmd = new String[] { transferCommand,
                                                       local,
                                                       farProtocol + PROTOCOL_SEPARATOR
                                                              + remote };
            final String[] cmdArray;
            final String[] flags = getFlags();
            if (null == flags) {
                cmdArray = vanillaCmd;
            } else {
                cmdArray = new String[vanillaCmd.length + flags.length];
                cmdArray[0] = vanillaCmd[0];
                int offset = 1;
                for (String flag : flags) {
                    cmdArray[offset] = flag;
                    ++offset;
                }
                for (int i = 1;
                     i != vanillaCmd.length;
                     ++i) {
                    cmdArray[offset] = vanillaCmd[i];
                    ++offset;
                }
            }

            final Map<String, String> env;
            if ("gsiftp".equals(farProtocol) || "gsiftp".equals(nearProtocol)) {
                String proxyFile = proxyManager.getProxyFile();
                if (null == proxyFile) {
                    env = null;
                } else {
                    env = new HashMap<>();
                    env.put(ProxyManager.X509_USER_PROXY_ENV,
                            proxyFile);
                }
            }

            final Command command = new Command(cmdArray);
            LOG.debug("Executing the following command " + command.toString());
            command.execute();
            final ExecutionFailedException cmdException = command.getCmdException();
            if (null != cmdException) {
                LOG.error("The following command failed: " + command.toString());
                final String[] stderr = cmdException.getStdErr();
                for (String line : stderr) {
                    LOG.error(line);
                }
                throw new IOException(transferCommand + " execution failed",
                                      cmdException);
            }
        }
    }
}
