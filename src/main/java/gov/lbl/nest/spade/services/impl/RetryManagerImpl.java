package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.registry.FileCategory;
import gov.lbl.nest.spade.registry.RetryOptions;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.RetryManager;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.xml.bind.JAXBException;

/**
 * This class is the default implementation of the {@link RetryManager}
 * interface.
 *
 * @author patton
 */
@Stateless
public class RetryManagerImpl implements
                              RetryManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The name to use as the reship ticket file name.
     */
    private static final String RESHIP_TICKET_FILENAME = "ticket.xml";

    // private static member data

    // private instance member data

    /**
     * The directory to contain tickets for successful dispatches that have not yet
     * been confirmed.
     */
    private File awaitingConfirmationDir;

    /**
     * The directory to contain files that have been shipped but delivery has not
     * been verified for all destinations.
     */
    private File awaitingVerificationDir;

    /**
     * The {@link CacheManager} instance used by this object.
     */
    @Inject
    private CacheManager cacheManager;

    /**
     * The Cache Mapping instance used by this object.
     */
    private Map<String, File> cacheMap;

    /**
     * The {@link Configuration} instance used by this object.
     */
    @Inject
    private Configuration configuration;

    /**
     * The directory to contain tickets for dispatches that failed.
     */
    private File dispatchFailedDir;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected RetryManagerImpl() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     * @param cacheMap
     *            the Cache Mapping instance used by this object.
     */
    public RetryManagerImpl(final CacheManager cacheManager,
                            final Map<String, File> cacheMap) {
        this.cacheManager = cacheManager;
        this.cacheMap = cacheMap;
    }

    // instance member method (alphabetic)

    @Override
    public boolean cancel(final String ticket) {
        boolean result = false;
        final File postponedTicket = new File(getDispatchFailedDir(),
                                              ticket);
        result |= cacheManager.delete(postponedTicket);

        final File directory = new File(getAwaitingVerificationDir(),
                                        ticket);
        result |= cacheManager.delete(directory);
        return result;
    }

    /**
     * Returns the directory to contain files that have been shipped but delivery
     * has not been verified for all destinations.
     *
     * @return the directory to contain files that have been shipped but delivery
     *         has not been verified for all destinations.
     */
    private File getAwaitingConfirmationDir() {
        if (null == awaitingConfirmationDir) {
            awaitingConfirmationDir = getCacheMap().get(CacheDefinition.UNCONFIRMED);
        }
        return awaitingConfirmationDir;
    }

    @Override
    public IngestTicket getAwaitingConfirmationTicket(String ticket) {
        /**
         * This is a place holder until the awaitingConfirmationDir and its contents are
         * properly managed.
         */
        if (Boolean.TRUE.booleanValue()) {
            return IngestTicket.read(new File(new File(getAwaitingVerificationDir(),
                                                       ticket),
                                              RESHIP_TICKET_FILENAME));
        }
        /**
         * This is what will eventually be used.
         */
        return IngestTicket.read(new File(getAwaitingConfirmationDir(),
                                          ticket));
    }

    /**
     * Returns the directory to contain files that have been shipped but delivery
     * has not been verified for all destinations.
     *
     * @return the directory to contain files that have been shipped but delivery
     *         has not been verified for all destinations.
     */
    private File getAwaitingVerificationDir() {
        if (null == awaitingVerificationDir) {
            awaitingVerificationDir = getCacheMap().get(CacheDefinition.RESHIP);
        }
        return awaitingVerificationDir;
    }

    @Override
    public IngestTicket getAwaitingVerificationTicket(final String ticket) {
        return IngestTicket.read(new File(new File(getAwaitingVerificationDir(),
                                                   ticket),
                                          RESHIP_TICKET_FILENAME));
    }

    private Map<String, File> getCacheMap() {
        if (null == cacheMap) {
            cacheMap = (configuration.getCacheDefinition()).getCacheMap();
        }
        return cacheMap;
    }

    /**
     * Returns the directory to contain tickets for dispatches that failed.
     *
     * @return the directory to contain tickets for dispatches that failed.
     */
    private File getDispatchFailedDir() {
        if (null == dispatchFailedDir) {
            dispatchFailedDir = getCacheMap().get(CacheDefinition.POSTPONED);
        }
        return dispatchFailedDir;
    }

    @Override
    public IngestTicket getDispatchFailedTicket(final String ticket) {
        return IngestTicket.read(new File(getDispatchFailedDir(),
                                          ticket));
    }

    @Override
    public List<String> getPostponed(final Long cushion,
                                     final TimeUnit unit) {
        final long now = new Date().getTime();
        final TimeUnit unitToUse;
        if (null == unit) {
            unitToUse = TimeUnit.MINUTES;
        } else {
            unitToUse = unit;
        }
        final long beforeToUse;
        if (null == cushion) {
            beforeToUse = now;
        } else {
            long before = now - TimeUnit.MILLISECONDS.convert(cushion,
                                                              unitToUse);
            if (before < 0) {
                beforeToUse = 0;
            } else {
                beforeToUse = before;
            }
        }
        final File[] postponed = getDispatchFailedDir().listFiles(new FileFilter() {

            @Override
            public boolean accept(File pathname) {
                return pathname.lastModified() < beforeToUse;
            }
        });
        final List<File> postponedAsList = Arrays.asList(postponed);
        Collections.sort(postponedAsList,
                         new Comparator<File>() {

                             @Override
                             public int compare(File o1,
                                                File o2) {
                                 final long difference = o1.lastModified() - o2.lastModified();
                                 if (0 > difference) {
                                     return -1;
                                 }
                                 if (0 < difference) {
                                     return 1;
                                 }
                                 return 0;
                             }
                         });
        final List<String> result = new ArrayList<>(postponedAsList.size());
        for (File postponedFile : postponedAsList) {
            result.add(postponedFile.getName());
        }
        return result;
    }

    @Override
    public void postponed(final IngestTicket ticket,
                          final Collection<String> outboundTransfers) throws JAXBException {
        ticket.setOutboundTransfers(outboundTransfers);
        ticket.write(new File(getDispatchFailedDir(),
                              ticket.getIdentity()));
    }

    @Override
    public IngestTicket prepare(final IngestTicket ticket) throws IOException,
                                                           JAXBException {
        final String identity = ticket.getIdentity();
        final File directory = new File(getAwaitingVerificationDir(),
                                        ticket.getIdentity());
        directory.mkdir();
        final File metadataFile = ticket.getMetadataFile();
        final File transferFile = ticket.getTransferFile();
        final FileCategory shippingCategory = ticket.getShippingCategory();
        final boolean singleData = ticket.isSingle() && FileCategory.DATA == shippingCategory;
        final File transferSource;
        if (singleData) {
            transferSource = transferFile.getParentFile();
        } else {
            transferSource = transferFile;
        }
        final File retryMetadata;
        final File retryTransfer;
        if ((directory.getAbsolutePath()).equals((transferSource.getParentFile()).getAbsolutePath())) {
            retryMetadata = metadataFile;
            retryTransfer = transferFile;
        } else {

            final File retryDestination;
            /**
             * Need to grab the transfer name before the file is moved.
             */
            if (singleData) {
                retryDestination = new File(directory,
                                            transferSource.getName());
                retryTransfer = new File(retryDestination,
                                         transferFile.getName());
            } else {
                retryDestination = new File(directory,
                                            transferSource.getName());
                retryTransfer = retryDestination;
            }

            cacheManager.copy(transferSource,
                              retryDestination);

            if (!metadataFile.equals(transferSource)) {
                retryMetadata = new File(directory,
                                         metadataFile.getName());
                cacheManager.copy(metadataFile,
                                  retryMetadata);
            } else {
                retryMetadata = retryDestination;
            }

            /**
             * Note, this ticket is NOT used as a Data Object in the workflow, but rather it
             * is used in the RESHIP workflow to reship to ALL outbound_transfers.
             */
            final IngestTicket retryTicket = new IngestTicket(identity,
                                                              ticket.getBundle(),
                                                              retryMetadata,
                                                              ticket.getShippingCategory(),
                                                              retryTransfer,
                                                              new RetryOptions(ticket.getOptions()));
            retryTicket.setOutboundTransfers(ticket.getOutboundTransfers());
            retryTicket.write(new File(directory,
                                       RESHIP_TICKET_FILENAME));
        }

        /**
         * Note, this ticket is saved by the `postponed` method in the POSTPONED
         * directory and is NOT used as a Data Object in the workflow, but rather it is
         * used in the RESHIP workflow to reship to those outbound transfers that were
         * not successfully dispatched.
         */
        return new IngestTicket(identity,
                                ticket.getBundle(),
                                retryMetadata,
                                ticket.getShippingCategory(),
                                retryTransfer,
                                new RetryOptions(ticket.getOptions()));
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
