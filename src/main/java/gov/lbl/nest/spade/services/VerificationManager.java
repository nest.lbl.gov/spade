package gov.lbl.nest.spade.services;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.rs.Bundle;

/**
 * This interface is used to manage the verification of transfers to and from
 * remote SPADE instances.
 *
 * @author patton
 */
public interface VerificationManager {

    /**
     * The categories of time stamps that can be queried.
     *
     * @author patton
     */
    enum Stamped {

                  /**
                   * The confirmable category.
                   */
                  CONFIRMABLE,

                  /**
                   * The confired category.
                   */
                  CONFIRMED,

                  /**
                   * The unverified category.
                   */
                  UNVERIFIED,

                  /**
                   * The verified category.
                   */
                  VERIFIED
    }

    /**
     * Verifies or rejects the confirmation of the specified ticket depending on
     * whether the confirmation information matches or not.
     *
     * @param neighbor
     *            the neighbor from which the confirmation has been received.
     * @param confirmables
     *            the collection of {@link DigestChange} instances containing the
     *            confirmation information from the neighbor.
     *
     * @return a List of {@link DigestChange} instances listing the successful
     *         confirmations correlated with the supplied information. If there was
     *         a confirmation before this method was called that will be the value
     *         in the {@link DigestChange} return for that bundle.
     */
    List<? extends DigestChange> confirm(String neighbor,
                                         List<? extends DigestChange> confirmables);

    /**
     * Tells this object the specified ticket may now be verified by the remote
     * SPADE that delivered it. This method assumes the confirmation information has
     * already been created for the transfer file.
     *
     * @param ticket
     *            the identity of the ticket that may now be verified.
     */
    void confirmable(String ticket);

    /**
     * Tells this object to create the confirmation information for the transfer
     * file after which it may be verified by the remote SPADE that delivered it.
     *
     * @param ticket
     *            the identity of the ticket whose confirmation information should
     *            be created.
     * @param metadataFile
     *            the {@link File} containing the metadata that was transferred to
     *            this SPADE instance.
     * @param transferFile
     *            the {@link File} that was transferred to this SPADE instance.
     *
     * @throws IOException
     *             if the transfer file can not be found or read.
     * @throws MetadataParseException
     *             if the metadata file can not be found or read.
     */
    void confirmable(String ticket,
                     File metadataFile,
                     File transferFile) throws IOException,
                                        MetadataParseException;

    /**
     * Tells this object the specified ticket has been successfully delivered to the
     * supplied neighbor.
     *
     * @param ticket
     *            the identity of the ticket to be verified.
     * @param neighbor
     *            the identity of the remote SPADE instance to which the ticket was
     *            successfully delivered.
     */
    void delivered(String ticket,
                   String neighbor);

    /**
     * Returns a list of {@link DigestChange} instances that have been time-stamped
     * with the specified {@link Stamped}.
     *
     * @param stamped
     *            the {@link Stamped} type to be returned.
     * @param bundles
     *            the collection of bundle names to be considered.
     *
     * @return the list of {@link DigestChange} instances that have been been
     *         time-stamped with the specified {@link Stamped}.
     */
    List<DigestChange> getByBundle(Stamped stamped,
                                   List<String> bundles);

    /**
     * Returns a list of {@link DigestChange} instances that have been time-stamped
     * with the specified {@link Stamped}.
     *
     * @param stamped
     *            the {@link Stamped} type to be returned.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     *
     * @return the list of {@link DigestChange} instances that have been been
     *         time-stamped with the specified {@link Stamped}.
     */
    List<DigestChange> getByTime(Stamped stamped,
                                 Date after,
                                 Date before,
                                 boolean reversed,
                                 int max);

    /**
     * Returns a list of {@link DigestChange} instances that have been time-stamped
     * with the specified {@link Stamped}.
     *
     * @param stamped
     *            the {@link Stamped} type to be returned.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     * @param neighbor
     *            the neighbor for whom the confirmables should be returned,
     *            <code>null</code> implies all neighbors.
     * @param registrations
     *            the collection the names of {@link Registration} instance to
     *            included in the result.
     *
     * @return the list of {@link DigestChange} instances that have been
     *         time-stamped with the specified {@link Stamped}.
     */
    List<DigestChange> getByTime(Stamped stamped,
                                 Date after,
                                 Date before,
                                 boolean reversed,
                                 int max,
                                 String neighbor,
                                 List<String> registrations);

    /**
     * Returns the list of neighbor names to which the specified ticket has not been
     * successfully delivered.
     *
     * @param ticket
     *            the identity of the ticket whose undelivered transfers should be
     *            returned.
     *
     * @return the list of neighbor names to which the specified ticket has not been
     *         successfully delivered.
     */
    Collection<String> getUndeliveredTransfers(String ticket);

    /**
     * Prepares this object to manager the verification for the supplied ticket.
     *
     * @param ticket
     *            the identity of the ticket to be verified.
     * @param bundle
     *            the name of the {@link Bundle} instance to be verified.
     * @param metadataFile
     *            the {@link File} instance containing the bundle's metadata.
     * @param transferFile
     *            the {@link File} that will be transferred from this SPADE
     *            instance.
     * @param transfers
     *            the collection of outbound transfers whose delivery will need to
     *            be confirmed.
     *
     * @throws IOException
     *             if the transfer file can not be found or read.
     * @throws MetadataParseException
     *             if the metadata file can not be found or read.
     */
    void prepare(String ticket,
                 String bundle,
                 File metadataFile,
                 File transferFile,
                 Collection<String> transfers) throws IOException,
                                               MetadataParseException;

    /**
     * Starts the verification that the supplied ticket's transfers have all been
     * successfully confirmed.
     *
     * @param ticket
     *            the identity of the ticket whose verification should be started.
     */
    void start(String ticket);

}
