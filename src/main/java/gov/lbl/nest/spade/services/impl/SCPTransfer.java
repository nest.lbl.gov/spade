package gov.lbl.nest.spade.services.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitParam;
import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.interfaces.policy.ImplementedPolicy;
import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.services.FileTransfer;

/**
 * This class implements the {@link FileTransfer} interface in order to transfer
 * files using 'scp'.
 *
 * @author patton
 */
public class SCPTransfer extends
                         ThirdPartyFileTransfer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(SCPTransfer.class);

    /**
     * The name of the resource that may contain the path to an alternate
     * configuration file for ssh.
     */
    private static final String SSH_CONFIG_RESOURCE = "scptransfer_ssh_config";

    // private static member data

    /**
     * True if the {@link #configFileName} value has been loaded.
     */
    private static boolean configNameLoaded = false;

    /**
     * The path to an alternate configuration file for ssh.
     */
    private static String configFileName;

    // private instance member data

    // constructors

    /**
     * Returns a path to a configuration file that will be used as default rather
     * than in the in-build default.
     *
     * @return a path to a configuration file that will be used as default rather
     *         than in the in-build default.
     */
    private static synchronized String getConfigFileName() {
        return getConfigFileName(SCPTransfer.class,
                                 SSH_CONFIG_RESOURCE);
    }

    private static synchronized String getConfigFileName(final Class<?> clazz,
                                                         final String resource) {
        if (configNameLoaded) {
            return configFileName;
        }
        try {
            final InputStream is = clazz.getResourceAsStream(resource);
            final BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            do {
                try {
                    line = br.readLine();
                    if (null != line && !"".equals(line)) {
                        final String lineToUse = line.trim();
                        if (!lineToUse.startsWith("#")) {
                            configFileName = lineToUse;
                            configNameLoaded = true;
                            return configFileName;
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } while (null != line);
        } catch (NullPointerException e) {
            configNameLoaded = true;
        }
        return configFileName;
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Create an instance of this class.
     *
     * The first instance created will attempt to create the necessary VOMs proxy.
     */
    public SCPTransfer() {
        this(null);
    }

    /**
     * Create an instance of this class.
     *
     * The first instance created will attempt to create the necessary VOMs proxy.
     *
     * @param parameters
     *            the {@link InitParam} instances to use when creation the new
     *            instance.
     */
    public SCPTransfer(final Collection<InitParam> parameters) {
        final String configToUse = ImplementedPolicy.getParameter(parameters,
                                                                  "config");
        if (null != configToUse && 0 != (configToUse.trim()).length()) {
            configFileName = configToUse;
            configNameLoaded = true;
        }
    }

    @Override
    protected String getMetadataNearProtocol() {
        return null;
    }

    @Override
    protected String getMountPointProperties() {
        return null;
    }

    @Override
    protected String getMountPointResource() {
        return null;
    }

    @Override
    protected String getSemaphoreNearProtocol() {
        return null;
    }

    @Override
    protected String getTransferNearProtocol() {
        return null;
    }

    @Override
    protected void outboundTransfer(File localFile,
                                    ExternalLocation location,
                                    String targetName,
                                    String nearProtocol) throws IOException,
                                                         InterruptedException {
        final String host = location.getHost();
        if (null == host || LocalhostTransfer.LOCALHOST.equals(host)) {
            LocalhostTransfer.copy(localFile,
                                   new File(LocalhostTransfer.getResolvedPath(location.getDirectory()),
                                            targetName));
        } else {
            final String[] cmdArray;
            final String configFile = getConfigFileName();
            if (null == configFile) {
                cmdArray = new String[] { "scp",
                                          localFile.getCanonicalPath(),
                                          location.toString() + '/'
                                                                        + targetName };
            } else {
                cmdArray = new String[] { "scp",
                                          "-F",
                                          configFile,
                                          localFile.getCanonicalPath(),
                                          location.toString() + '/'
                                                                        + targetName };
            }
            final Command command = new Command(cmdArray);
            command.execute();
            final ExecutionFailedException cmdException = command.getCmdException();
            if (null != cmdException) {
                LOG.error("The following command failed: " + command.toString());
                final String[] stderr = cmdException.getStdErr();
                for (String line : stderr) {
                    LOG.error(line);
                }
                throw new IOException("'scp' execution failed",
                                      cmdException);
            }
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
