package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.rs.QueryDate;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.jee.watching.LastWatched;
import gov.lbl.nest.jee.watching.Watcher;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.config.Neighbor;
import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.UnmarshalException;
import jakarta.xml.bind.Unmarshaller;

/**
 * The class is the default implementation of the {@link NeighborhoodManager}
 * interface.
 *
 * @author patton
 */
@Stateless
public class NeighborhoodManagerImpl implements
                                     NeighborhoodManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The prefix used to define the "role" when watching a confirmables URL.
     */
    private static final String CONFIRMABLE_PREFIX = "Confimables_";

    /**
     * The default number of records to request when make a RESTful call to a
     * neighbor.
     */
    private static final int DEFAULT_SELECTION_DEPTH = 33;

    /**
     * Value to use when the file list is empty.
     */
    private static final List<DigestChange> EMPTY_CHANGE_LIST = new ArrayList<>();

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(NeighborhoodManagerImpl.class);

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance used by this object.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link LoadedNeighborhood} instance used by this object.
     */
    @Inject
    private LoadedNeighborhood loadedNeighbors;

    /**
     * The {@link Watcher} instance used by this object.
     */
    @Inject
    private Watcher watcher;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected NeighborhoodManagerImpl() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param configuration
     *            The {@link Configuration} instance used by this object.
     * @param loadedNeighbors
     *            the {@link LoadedNeighborhood} instance used by the created
     *            object.
     * @param watcher
     *            The {@link Watcher} instance used by this object.
     */
    public NeighborhoodManagerImpl(Configuration configuration,
                                   final LoadedNeighborhood loadedNeighbors,
                                   final Watcher watcher) {
        this.configuration = configuration;
        this.loadedNeighbors = loadedNeighbors;
        this.watcher = watcher;
    }

    // instance member method (alphabetic)

    @Override
    public List<? extends DigestChange> getConfirmables(final Neighbor neighbor) {
        if (null == watcher) {
            return null;
        }
        final String name = neighbor.getName();
        final String confirmableRole = neighbor.getConfirmableRole();
        final String roleToUse;
        if (null == confirmableRole) {
            roleToUse = CONFIRMABLE_PREFIX + name;
        } else {
            roleToUse = confirmableRole;
        }
        final LastWatched last = watcher.getLastWatched(roleToUse);
        if (null == last) {
            throw new IllegalStateException("\"" + name
                                            + "\" is not watching for confirmables, so will not do anything");
        }
        final Date lastDateTime = last.getLastDateTime();
        LOG.debug("Looking for new confirmables for \"" + name
                  + "\" on or after "
                  + lastDateTime);
        final List<? extends DigestChange> issued = getIssuedItemsOnOrAfter(lastDateTime,
                                                                            null,
                                                                            DEFAULT_SELECTION_DEPTH,
                                                                            neighbor);
        if (null == issued) {
            return null;
        }
        final List<? extends DigestChange> confirmables = watcher.forwardLastWatched(last,
                                                                                     issued);
        return confirmables;
    }

    @Override
    public String getDirectory(String identity) {
        final Neighbor neighbor = loadedNeighbors.getNeighbor(identity);
        if (null == neighbor) {
            throw new IllegalArgumentException("No neighbour found for \"" + identity
                                               + "\"");
        }
        final String[] parts = identity.split("@",
                                              2);
        if (1 == parts.length) {
            return DEFAULT_HOST + File.separator
                   + parts[0];
        }
        return parts[1] + File.separator
               + parts[0];
    }

    /**
     * Returns the list of items issued at the specified URL, on or after the
     * supplied date and time.
     *
     * @param after
     *            the time on or after which items should be include in the returned
     *            list.
     * @param before
     *            the time before which items should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param max
     *            the maximum number of items to return, 0 is unlimited.
     * @param neighbor
     *            the {@link Neighbor} from which to get the collection of items.
     * @param log
     *            the {@link Logger} instance to used within this method.
     *
     * @return the list of items issued at the specified URL, on or after the
     *         supplied date and time.
     */
    private List<? extends DigestChange> getIssuedItemsOnOrAfter(final Date after,
                                                                 final Date before,
                                                                 final int max,
                                                                 final Neighbor neighbor) {
        final String url = neighbor.getConfirmablesUrl();
        if (null == url) {
            return null;
        }
        final String knownAs = neighbor.getKnownAs();
        final String knownAsToUse;
        if (null == knownAs) {
            knownAsToUse = (configuration.getAssembly()).getName();
        } else {
            knownAsToUse = knownAs;
        }
        final String modifiedUrl;
        final String conjunction;
        final int beginingOfQuery = url.indexOf("?");
        if (-1 == beginingOfQuery) {
            modifiedUrl = url + "/"
                          + knownAsToUse;
            conjunction = "?";
        } else {
            modifiedUrl = url.substring(0,
                                        beginingOfQuery)
                          + "/"
                          + knownAsToUse
                          + url.substring(beginingOfQuery);
            conjunction = "&";
        }
        final String afterDate = QueryDate.LONG_ZONED_FORMATTER.format(after);
        final String afterQuery = "after=" + afterDate.replaceFirst("\\+",
                                                                    "%2B");
        final String beforeQuery;
        if (null == before) {
            beforeQuery = "";
        } else {
            final String beforeDate = QueryDate.LONG_ZONED_FORMATTER.format(before);
            beforeQuery = "&before=" + beforeDate.replaceFirst("\\+",
                                                               "%2B");
        }

        final String maxToUse;
        if (url.contains("?max=") || url.contains("&max=")) {
            maxToUse = "";
        } else {
            maxToUse = "&max=" + Integer.toString(max);
        }

        final String finalUrl = modifiedUrl + conjunction
                                + afterQuery
                                + beforeQuery
                                + maxToUse;
        LOG.info("URL for watched items is \"" + finalUrl
                 + "\"");
        try {
            final JAXBContext jc = JAXBContext.newInstance(Digest.class);
            final Unmarshaller u = jc.createUnmarshaller();
            URL watchedUrl = new URL(finalUrl);
            Digest digest;
            digest = (Digest) u.unmarshal(watchedUrl);
            final List<? extends DigestChange> lines = digest.getIssued();
            if (null == lines) {
                return EMPTY_CHANGE_LIST;
            }
            return lines;
        } catch (MalformedURLException e1) {
            // Should never get here
            e1.printStackTrace();
            return EMPTY_CHANGE_LIST;
        } catch (UnmarshalException e) {
            final Throwable t = e.getLinkedException();
            if (t instanceof java.net.ConnectException) {
                LOG.warn("Could not connect to URL \"" + finalUrl
                         + "\"");
            } else if (t instanceof FileNotFoundException) {
                LOG.warn("URL \"" + finalUrl
                         + "\" could not be found");
            } else if (t instanceof IOException) {
                LOG.warn("URL \"" + finalUrl
                         + "\" could not be read, see following message:");
                LOG.warn("    " + t.getMessage());
            } else {
                e.printStackTrace();
            }
            return EMPTY_CHANGE_LIST;
        } catch (JAXBException e) {
            e.printStackTrace();
            return EMPTY_CHANGE_LIST;
        }
    }

    @Override
    public Collection<Neighbor> getNeighbourhood() {
        return loadedNeighbors.getNeighborhood();
    }

    @Override
    public OutboundTransfer getOutboundTransfer(String name) {
        return loadedNeighbors.getOutboundTransfer(name);
    }

    @Override
    public Neighbor getSelf() {
        return loadedNeighbors.getSelf();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
