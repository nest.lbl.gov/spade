package gov.lbl.nest.spade.services;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.TimeUnit;

import gov.lbl.nest.spade.activities.IngestTicket;
import jakarta.xml.bind.JAXBException;

/**
 * This interface is used to manage the any attempt to retry a transfer once it
 * has already been attempted either successfully or unsuccessfully.
 *
 * @author patton
 *
 */
public interface RetryManager {

    /**
     * Signals that retries of the specified ticket are no longer required.
     *
     * @param ticket
     *            the identity of the ticket whose retries are no longer required
     * 
     * @return <code>true</code> if the cancellation is successful.
     */
    boolean cancel(final String ticket);

    /**
     * Returns the {@link IngestTicket} instance, if any, that can be used to
     * execute the re-ship unconfirmed outbound transfers.
     *
     * @param ticket
     *            the identity of the ticket whose ticket should be returned.
     *
     * @return the {@link IngestTicket} instance, if any, that can be used to
     *         execute the re-ship unconfirmed outbound transfers.
     */
    IngestTicket getAwaitingConfirmationTicket(String ticket);

    /**
     * Returns the {@link IngestTicket} instance, if any, that can be used to
     * execute the re-ship of the ticket to all outbound transfers.
     *
     * @param ticket
     *            the identity of the ticket whose ticket should be returned.
     *
     * @return the {@link IngestTicket} instance, if any, that can be used to
     *         execute the re-ship of the ticket to all outbound transfers.
     */
    IngestTicket getAwaitingVerificationTicket(String ticket);

    /**
     * Returns the {@link IngestTicket} instance, if any, that can be used to
     * execute the re-dispatch of unsuccessful deliveries.
     *
     * @param ticket
     *            the identity of the ticket whose ticket should be returned.
     *
     * @return the {@link IngestTicket} instance, if any, that can be used to
     *         execute the re-dispatch of unsuccessful deliveries.
     */
    IngestTicket getDispatchFailedTicket(String ticket);

    /**
     * Returns the {@link Collection} of ticket identities that are currently queued
     * for retry.
     *
     * @param cushion
     *            the time in seconds that the identity must have been in the queue
     *            in order to be returned.
     * @param unit
     *            the time unit of the cushion argument
     *
     * @return the {@link Collection} of ticket identities that are currently queued
     *         for retry.
     */
    List<String> getPostponed(Long cushion,
                              TimeUnit unit);

    /**
     * Signals that the transfer of the specified ticket to the provided outbound
     * transfers is postponed as it has not been successfully dispatched.
     *
     * @param ticket
     *            the ticket to be used for the retry.
     * @param outboundTransfers
     *            the collection of outbound transfers that should be include in the
     *            queued retries.
     *
     * @throws JAXBException
     *             when the ticket to be used for the retry can not be saved.
     *
     */
    void postponed(final IngestTicket ticket,
                   final Collection<String> outboundTransfers) throws JAXBException;

    /**
     * Prepares for the case where part or all of the supplied ingest ticket will
     * need to be retried.
     *
     * @param ticket
     *            the {@link IngestTicket} that may need to be retried.
     *
     * @return the {@link IngestTicket} ticket to be used to
     *         {@link #postponed(IngestTicket, Collection)} the retry if it is
     *         required.
     *
     * @throws IOException
     *             when the files can no be stored for a retry.
     * @throws JAXBException
     *             when the ticket to be used for the retry can not be saved.
     */
    IngestTicket prepare(final IngestTicket ticket) throws IOException,
                                                    JAXBException;

}
