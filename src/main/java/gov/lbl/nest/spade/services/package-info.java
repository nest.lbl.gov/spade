/**
 * The package contains the Interfaces used by SPADE that are internal to its
 * operation, but whose implementation can be selected at deployment.
 *
 * @author patton
 */
package gov.lbl.nest.spade.services;