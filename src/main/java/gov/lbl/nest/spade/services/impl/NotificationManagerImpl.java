package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.jee.configure.Mailer;
import gov.lbl.nest.spade.config.Notifications;
import gov.lbl.nest.spade.config.Recipient;
import gov.lbl.nest.spade.services.NotificationManager;
import jakarta.mail.Address;
import jakarta.mail.Message;
import jakarta.mail.MessagingException;
import jakarta.mail.Transport;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeMessage;

/**
 * This class implements the {@link NotificationManager} interface to both
 * record notifications and email them to registered parties.
 *
 * @author patton
 *
 */
public class NotificationManagerImpl implements
                                     NotificationManager {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(NotificationManagerImpl.class);

    /**
     * The name of the application using this Object.
     */
    private String applicationName;

    /**
     * True is the mailer has failed to send a notification.
     */
    private AtomicBoolean disabled = new AtomicBoolean();

    /**
     * The collection of recipients for each {@link Scope} instance.
     */
    private Map<Scope, Collection<InternetAddress>> knownRecipients = new HashMap<>();

    /**
     * The {@link Mailer} instance used by this Object.
     */
    private final Mailer mailer;

    /**
     * Create an instance of this class.
     */
    protected NotificationManagerImpl() {
        mailer = null;
        applicationName = null;
    }

    /**
     * Creates an instance of this class.
     *
     * @param applicationName
     *            the name of the application.
     * @param directory
     *            the directory that hold this application's configuration files.
     * @param notifications
     *            the {@link Notifications} instance for this application.
     */
    public NotificationManagerImpl(String applicationName,
                                   File directory,
                                   Notifications notifications) {
        this.applicationName = applicationName;
        mailer = new Mailer(directory);
        if (null != notifications) {
            final Collection<Recipient> recipients = notifications.getRecipients();
            if (null != recipients && !recipients.isEmpty()) {
                for (Recipient recipient : recipients) {
                    final Collection<InternetAddress> addresses = recipient.getInternetAddresses();
                    if (null != addresses && !addresses.isEmpty()) {
                        final Collection<Scope> scopes = recipient.getScopes();
                        if (null != scopes && !scopes.isEmpty()) {
                            for (Scope scope : scopes) {
                                final Collection<InternetAddress> addressesForScope = knownRecipients.get(scope);
                                final Collection<InternetAddress> addressesToUse;
                                if (null == addressesForScope) {
                                    addressesToUse = new ArrayList<>();
                                    knownRecipients.put(scope,
                                                        addressesToUse);
                                } else {
                                    addressesToUse = addressesForScope;
                                }
                                addressesToUse.addAll(addresses);
                            }
                        }
                    }
                }
            }
        }
    }

    @Override
    public void postNotice(Scope scope,
                           String notification) {
        final Collection<InternetAddress> addressesForScope = knownRecipients.get(scope);
        final Collection<InternetAddress> addressesForAll = knownRecipients.get(Scope.ALL);
        final Collection<InternetAddress> addressesToUse;
        if (null == addressesForScope) {
            addressesToUse = addressesForAll;
        } else {
            if (null != addressesForAll && !addressesForAll.isEmpty()) {
                addressesForScope.addAll(addressesForAll);
            }
            addressesToUse = addressesForScope;
        }

        if (mailer.isValid() && null != addressesToUse
            && !addressesToUse.isEmpty()) {
            final Address[] addressArray = addressesToUse.toArray(new Address[] {});
            if (0 != addressArray.length) {
                try {
                    final String subject = scope.toString() + " notification from "
                                           + applicationName;
                    final Message message = new MimeMessage(mailer.getSession());
                    message.setSubject(subject);
                    message.setText(notification);
                    message.addRecipients(Message.RecipientType.TO,
                                          addressArray);
                    Transport.send(message,
                                   addressArray);
                    if (disabled.get()) {
                        LOG.warn("Successfully sent Notification so re-enabling sending of Notifications");
                        disabled.set(true);
                    }
                } catch (MessagingException e) {
                    if (!disabled.get()) {
                        LOG.error("Failed to send Notification because of \"" + e.getMessage()
                                  + "\", disabling sending of Notifications");
                        disabled.set(true);
                    }
                }
            }
        }
    }
}
