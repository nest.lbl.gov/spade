package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.interfaces.storage.ArchiveFailureException;
import gov.lbl.nest.spade.interfaces.storage.ArchiveManager;
import jakarta.annotation.Priority;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

/**
 * This class is an implementation of the {@link ArchiveManager} interface that
 * uses HPSS as its back end.
 *
 * @author patton
 */
@Priority(value = 0)
@Stateless
public class HpssManager implements
                         ArchiveManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance used by this object.
     */
    @Inject
    private Configuration configuration;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected HpssManager() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param configuration
     *            the {@link Configuration} instance used by the created object.
     */
    public HpssManager(final Configuration configuration) {
        this.configuration = configuration;
    }

    // instance member method (alphabetic)

    @Override
    public void add(final File archiveFile,
                    final File placement) throws ArchiveFailureException {
        final StringBuffer f = new StringBuffer();
        final File archiveRoot = (configuration.getArchiveDefinition()).getRootDirectory();
        f.append("lcd " + archiveFile.getParent()
                 + " ; cd "
                 + archiveRoot.getAbsolutePath());
        final List<String> levels = new ArrayList<>();
        File parent = placement.getParentFile();
        while (null != parent) {
            levels.add(0,
                       parent.getName());
            parent = parent.getParentFile();
        }
        for (String level : levels) {
            f.append(" ; mkdir " + level
                     + " ; cd "
                     + level);
        }
        f.append(" ; put " + archiveFile.getName()
                 + " : "
                 + placement.getName());
        final String[] cmdArray = new String[] { "hsi",
                                                 f.toString() };
        final Command command = new Command(cmdArray);
        try {
            command.execute();
            final ExecutionFailedException cmdException = command.getCmdException();
            if (null == cmdException) {
                return;
            }
            throw new ArchiveFailureException(null,
                                              cmdException);
        } catch (IOException
                 | InterruptedException e) {
            throw new ArchiveFailureException(null,
                                              e);
        }
    }

    @Override
    public void add(final File archiveFile,
                    final File metadataFile,
                    final File placement) {
        throw new UnsupportedOperationException();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
