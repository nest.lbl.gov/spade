package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Properties;

import gov.lbl.nest.common.configure.MountPointsMapping;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.services.FileTransfer;

/**
 * This class provides a framework based on the {@link FileTransfer} interface
 * to enable the creation of third party transfers.
 *
 * @author patton
 */
public abstract class ThirdPartyFileTransfer implements
                                             FileTransfer {

    /**
     * The {@link MountPointsMapping} instance that maps the mount points of "near"
     * files to "third-party" transfer points.
     */
    private MountPointsMapping mountPointsMapping;

    /**
     * Returns the protocol to use for the "near" part of the metadata files.
     * 
     * @return the protocol to use for the "near" part of the metadata files.
     */
    protected abstract String getMetadataNearProtocol();

    /**
     * Returns the name of the default properties file containing the mount points
     * mapping. The contents of this file is only used if the resource indicated by
     * {@link #getMountPointResource()} does not exist or is empty.
     *
     * @return the name of the resource, if any, that holds the path to the
     *         {@link Properties} file.
     */
    protected abstract String getMountPointProperties();

    /**
     * Returns the name of the resource contains the path to the properties file
     * that contains the mount points mapping. If there is no resource, or it is
     * empty then the contains of the file returned by
     * {@link #getMountPointProperties()} is used.
     *
     * @return he name of the resource contains the path to the properties file that
     *         contains the mount points mapping.
     */
    protected abstract String getMountPointResource();

    /**
     * Returns the {@link MountPointsMapping} instance used by this class.
     *
     * @return the {@link MountPointsMapping} instance used by this class.
     */
    protected MountPointsMapping getMountPointsMapping() {
        return mountPointsMapping;
    }

    /**
     * Returns the protocol to use for the "near" part of the semaphore files.
     * 
     * @return the protocol to use for the "near" part of the semaphore files.
     */
    protected abstract String getSemaphoreNearProtocol();

    /**
     * Returns the protocol to use for the "near" part of the transfer files.
     * 
     * @return the protocol to use for the "near" part of the transfer files.
     */
    protected abstract String getTransferNearProtocol();

    /**
     * Executes the "near" to "remote" transfer of the specified file.
     *
     * @param nearFile
     *            the local {@link File} to be transferred
     * @param location
     *            the {@link ExternalFile} instance specifying the target directory
     *            into which the file should be placed.
     * @param targetName
     *            the name of target file.
     * @param nearProtocol
     *            the protocol to use for "near" end of third party transfers.
     *
     * @throws IOException
     *             when the transfer can not be completed.
     * @throws InterruptedException
     *             when the transfer is interrupted.
     */
    protected abstract void outboundTransfer(final File nearFile,
                                             final ExternalLocation location,
                                             final String targetName,
                                             final String nearProtocol) throws IOException,
                                                                        InterruptedException;

    @Override
    public boolean send(final ExternalLocation targetLocation,
                        final File metadataFile,
                        final String targetMetadataName,
                        final File transferFile,
                        final String targetTransferName,
                        final File semaphoreFile,
                        final String targetSemaphoreName,
                        final URI callback) throws IOException,
                                            InterruptedException {
        final String targetMetadataNameToUse;
        if (null != metadataFile && null == targetMetadataName) {
            targetMetadataNameToUse = metadataFile.getName();
        } else {
            targetMetadataNameToUse = targetMetadataName;
        }
        final String targetTransferNameToUse;
        if (null == targetTransferName) {
            targetTransferNameToUse = transferFile.getName();
        } else {
            targetTransferNameToUse = targetTransferName;
        }
        final String targetSemaphoreNameToUse;
        if (null == targetSemaphoreName) {
            targetSemaphoreNameToUse = semaphoreFile.getName();
        } else {
            targetSemaphoreNameToUse = targetSemaphoreName;
        }
        try {
            if (null != metadataFile) {
                outboundTransfer(metadataFile,
                                 targetLocation,
                                 targetMetadataNameToUse,
                                 getMetadataNearProtocol());
            }
            try {
                outboundTransfer(transferFile,
                                 targetLocation,
                                 targetTransferNameToUse,
                                 getTransferNearProtocol());
                try {
                    outboundTransfer(semaphoreFile,
                                     targetLocation,
                                     targetSemaphoreNameToUse,
                                     getSemaphoreNearProtocol());
                } catch (IOException
                         | InterruptedException e) {
                    throw e;
                }
            } catch (IOException
                     | InterruptedException e) {
                throw e;
            }
        } catch (IOException
                 | InterruptedException e) {
            throw e;
        }
        return true;
    }

    @Override
    public void setEnvironment(File configurationDir) {
        if (null == mountPointsMapping) {
            mountPointsMapping = new MountPointsMapping(Configuration.class,
                                                        getMountPointResource(),
                                                        new File(configurationDir,
                                                                 getMountPointProperties()));
        }
    }

}
