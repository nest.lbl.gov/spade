package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.services.FileTransfer;

/**
 * This class implements the {@link FileTransfer} for files are directly
 * accessible on the local file system.
 *
 * @author patton
 */
public class LocalhostTransfer implements
                               FileTransfer {

    // public static final member data

    /**
     * The string indicating the files are directly accessible on the local file
     * system.
     */
    public static final String LOCALHOST = "localhost";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The amount of time, in seconds, to wait before re-trying a disk operation.
     */
    private static final int FAILURE_LIMIT = Integer.parseInt(System.getProperty("gov.lbl.nest.spade.disk.failure.limit",
                                                                                 "3"));

    /**
     * The amount of time, in seconds, to wait before re-trying a disk operation.
     */
    private static final int FAILURE_WAIT = Integer.parseInt(System.getProperty("gov.lbl.nest.spade.disk.failure.wait",
                                                                                "60"));
    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(LocalhostTransfer.class);

    /**
     * The field for the mount point of a directory.
     */
    private static final int MOUNT_POINT = 2;

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Copies one file to another location.
     *
     * @param from
     *            the location of the source file
     * @param to
     *            the location of the destination file.
     *
     * @throws IOException
     *             when the copy can not be competed.
     * @throws InterruptedException
     *             when the copy was interrupted.
     */
    public static void copy(final File from,
                            final File to) throws IOException,
                                           InterruptedException {
        copy(from,
             to,
             true);
    }

    /**
     * Copies one file to another location.
     *
     * @param from
     *            the location of the source file
     * @param to
     *            the location of the destination file.
     * @param hardlink
     *            true if a hard-link is allowed instead of a real copy.
     *
     * @throws IOException
     *             when the copy can not be competed.
     * @throws InterruptedException
     *             when the copy was interrupted.
     */
    public static void copy(final File from,
                            final File to,
                            final boolean hardlink) throws IOException,
                                                    InterruptedException {
        if (!((from.getCanonicalFile()).exists())) {
            throw new FileNotFoundException("\"" + from.getPath()
                                            + "\" does not exist");
        }
        String canonicalFrom = from.getCanonicalPath();
        String canonicalTo = to.getCanonicalPath();
        try {
            if (hardlink && isLinkable(from,
                                       to)) {
                final Date begin = new Date();
                Files.createLink(to.toPath(),
                                 (from.getCanonicalFile()).toPath());
                LOG.debug("Link created, in " + getDuration(begin.getTime())
                          + " secs, for \""
                          + from.toString()
                          + "\"");
                return;
            }
        } catch (IOException e) {
            LOG.info("Hard link creation failed for copy from \"" + canonicalFrom
                     + "\" to \""
                     + canonicalTo);
            LOG.info("retrying using \"cp\"");
        }
        final String[] cmdArray = new String[] { "cp",
                                                 "-rp",
                                                 canonicalFrom,
                                                 canonicalTo };
        final Command command = new Command(cmdArray);
        boolean succeeded = false;
        int failures = 0;
        while (!succeeded) {
            final Date begin = new Date();
            command.execute();
            final ExecutionFailedException cmdException = command.getCmdException();
            if (null != cmdException) {
                failures += 1;
                if (FAILURE_LIMIT == failures) {
                    throw new IOException("Local \"" + cmdArray[0]
                                          + "\" execution failed",
                                          cmdException);
                }
                Thread.sleep(TimeUnit.SECONDS.toMillis(FAILURE_WAIT));
            } else {
                succeeded = true;
                LOG.debug("Copy done, in " + getDuration(begin.getTime())
                          + " secs, for \""
                          + from.toString()
                          + "\"");
            }
        }
    }

    // static member methods (alphabetic)

    /**
     * Deletes a file, and if it is a directory deletes all file below it.
     *
     * @param file
     *            the location of the file to be deleted.
     *
     * @return true when the deletion has been successful.
     */
    public static boolean delete(final File file) {
        if (null == file || !file.exists()) {
            return true;
        }
        if (file.isDirectory()) {
            final File[] children = file.listFiles();
            if (null != children) {
                file.setWritable(true);
                for (File child : children) {
                    delete(child);
                }
            }
        }
        final Date begin = new Date();
        boolean result = file.delete();
        LOG.debug("Deletion, in " + getDuration(begin.getTime())
                  + " secs, of \""
                  + file.toString()
                  + "\"");
        return result;
    }

    /**
     * Returns a String containing the number of seconds since the specified start
     * time until the present time.
     *
     * @param start
     *            the time from which this duration should be calculated.
     *
     * @return a String containing the number of seconds since the specified start
     *         time until the present time.
     */
    private static String getDuration(Long start) {
        if (null == start) {
            return null;
        }
        final long now = new Date().getTime();
        return Long.toString(TimeUnit.SECONDS.convert(now - start.longValue(),
                                                      TimeUnit.MILLISECONDS));
    }

    /**
     * Returns the requested field for the disk containing the supplied directory.
     *
     * @param directory
     *            the directory whose disk information should be returned.
     *
     * @return The requested field for the disk containing the supplied directory.
     *
     * @throws IOException
     */
    private static Object getMountPoint(final File directory) throws IOException {
        final String pathToUse;
        if (null == directory) {
            pathToUse = ".";
        } else {
            pathToUse = directory.getCanonicalPath();
        }
        final String[] cmdArray = new String[] { "df",
                                                 "-Pk",
                                                 pathToUse };
        final Command command = new Command(cmdArray);
        int retCode;
        try {
            retCode = command.execute();
        } catch (IOException e) {
            return null;
        } catch (InterruptedException e) {
            return null;
        }
        if (0 != retCode) {
            return null;
        }
        List<String> output = command.getStdOut();
        if (2 != output.size()) {
            return null;
        }
        Pattern pattern = Pattern.compile("^[^\\s]*\\s*[^\\s]*\\s*[^\\s]*\\s*([^\\s]*)\\s*[^\\s]*\\s*([^\\s]*)");
        Matcher matcher = pattern.matcher(output.get(1));
        if (matcher.find() && (2 == matcher.groupCount())) {
            final String result = matcher.group(MOUNT_POINT);
            return result;
        }
        return null;
    }

    /**
     * Returns the value of the path resolved for any leading "~".
     *
     * @param path
     *            the path to be resolved.
     *
     * @return the value of the path resolved for any leading "~".
     */
    protected static String getResolvedPath(final String path) {
        return ExternalLocation.resolvePath(path);
    }

    /**
     * Returns true if the two files can be hard linked.
     *
     * @param from
     *            the original file.
     * @param to
     *            the file to be linked to the original file.
     *
     * @return true if the two files can be hard linked.
     */
    private static boolean isLinkable(final File from,
                                      final File to) {
        // Directories can not be hard linked, so forget it.
        if (from.isDirectory()) {
            return false;
        }
        String mountFrom;
        String mountTo;
        try {
            mountFrom = (String) getMountPoint(from);
            if (null == mountFrom) {
                return false;
            }
            final File fileToTest;
            if (to.isDirectory()) {
                fileToTest = to;
            } else {
                fileToTest = to.getParentFile();
            }
            mountTo = (String) getMountPoint(fileToTest);
            if (null == mountTo) {
                return false;
            }
        } catch (IOException e1) {
            return false;
        }
        if (mountFrom.equals(mountTo)) {
            return true;
        }
        return false;
    }

    /**
     * Moves a file from one location to another.
     *
     * @param from
     *            the location of the source file
     * @param to
     *            the location of the destination file.
     *
     * @return true when the move has been successful.
     *
     * @throws IOException
     *             when there is a problem with the move.
     * @throws InterruptedException
     *             when the move was interrupted.
     */
    public static boolean move(final File from,
                               final File to) throws IOException,
                                              InterruptedException {
        if (from.isDirectory()) {
            final Date begin = new Date();
            // TODO: Review this for safety
            boolean result = from.renameTo(to);
            LOG.debug("Renamed, in " + getDuration(begin.getTime())
                      + " secs from \""
                      + from.toString()
                      + "\"");
            return result;
        }
        copy(from,
             to);
        return delete(from);
    }

    /**
     * Creates a softlink from a file.
     *
     * @param from
     *            the location of the source file
     * @param to
     *            the location of the destination softlink.
     *
     * @throws IOException
     *             when the softlink can not be competed.
     * @throws InterruptedException
     *             when the softlink was interrupted.
     */
    public static void softlink(final File from,
                                final File to) throws IOException,
                                               InterruptedException {
        if (!(from.exists())) {
            throw new FileNotFoundException("\"" + from.getPath()
                                            + "\" does not exist");
        }
        /**
         * Uses the canonical path for both arguments as this means the soft-link is
         * created to the absolute path and thus can be resolved correctly. (As of Java
         * 1.8 a relative soft link can fail to be detected as a file.)
         */
        final Date begin = new Date();
        Files.createSymbolicLink((to.getCanonicalFile()).toPath(),
                                 (from.getCanonicalFile()).toPath());
        LOG.debug("Softlink created, in " + getDuration(begin.getTime())
                  + " secs, for \""
                  + from.toString()
                  + "\"");
    }

    @Override
    public boolean send(final ExternalLocation targetLocation,
                        final File metadataFile,
                        final String targetMetadataName,
                        final File transferFile,
                        final String targetTransferName,
                        final File semaphoreFile,
                        final String targetSemaphoreName,
                        final URI callback) throws IOException,
                                            InterruptedException {
        final String host = targetLocation.getHost();
        if (null != host && !LOCALHOST.equals(host)) {
            throw new UnsupportedOperationException();
        }
        final String targetMetadataNameToUse;
        if (null != metadataFile && null == targetMetadataName) {
            targetMetadataNameToUse = metadataFile.getName();
        } else {
            targetMetadataNameToUse = targetMetadataName;
        }
        final String targetTransferNameToUse;
        if (null == targetTransferName) {
            targetTransferNameToUse = transferFile.getName();
        } else {
            targetTransferNameToUse = targetTransferName;
        }
        final String targetSemaphoreNameToUse;
        if (null == targetSemaphoreName) {
            targetSemaphoreNameToUse = semaphoreFile.getName();
        } else {
            targetSemaphoreNameToUse = targetSemaphoreName;
        }
        final File directory = new File(targetLocation.getResolvedDirectory());
        final File metaTarget;

        if (null != metadataFile) {
            metaTarget = new File(directory,
                                  targetMetadataNameToUse);
        } else {
            metaTarget = null;
        }
        final File transferTarget = new File(directory,
                                             targetTransferNameToUse);
        final File semaphoreTarget = new File(directory,
                                              targetSemaphoreNameToUse);
        try {
            if (null != metaTarget) {
                copy(metadataFile,
                     metaTarget);
            }
            try {
                copy(transferFile,
                     transferTarget);
                try {
                    copy(semaphoreFile,
                         semaphoreTarget);
                } catch (IOException
                         | InterruptedException e) {
                    delete(semaphoreTarget);
                    throw e;
                }
            } catch (IOException
                     | InterruptedException e) {
                delete(transferTarget);
                throw e;
            }
        } catch (IOException
                 | InterruptedException e) {
            if (null != metaTarget) {
                delete(metaTarget);
            }
            throw e;
        }
        return true;
    }

    @Override
    public void setEnvironment(File configurationDir) {
        // Do nothing
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
