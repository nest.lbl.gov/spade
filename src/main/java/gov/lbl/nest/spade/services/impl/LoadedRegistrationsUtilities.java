package gov.lbl.nest.spade.services.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.registry.Registration;

/**
 * This class is a container for static methods used by the
 * {@link LoadedRegistrations} class.
 *
 * @author patton
 *
 */
public class LoadedRegistrationsUtilities {

    /**
     * @param <T>
     *            the type of {@link Registration} instances being used to extends
     *            the mapping.
     * @param <U>
     *            the type of {@link Registration} instances being used to in the
     *            mapping.
     * @param registrations
     *            the {@link Registration} instance with which to extend the
     *            mapping.
     * @param mapping
     *            the current mapping that is to be extended.
     */
    @SuppressWarnings("unchecked")
    public static <T extends Registration, U extends Registration> void extendRegistrationMappings(final List<T> registrations,
                                                                                                   final Map<String, Map<String, Collection<U>>> mapping) {
        for (Registration registration : registrations) {
            final ExternalLocation dropLocation = registration.getDropLocation();
            final String host = dropLocation.getHost();
            final String hostToUse;
            if (null == host) {
                hostToUse = LoadedRegistrations.DEFAULT_HOST;
            } else {
                hostToUse = host;
            }
            final Map<String, Collection<U>> registrationsOnHost = mapping.get(hostToUse);
            final Map<String, Collection<U>> registrationsOnHostToUse;
            if (null == registrationsOnHost) {
                registrationsOnHostToUse = new HashMap<>();
                mapping.put(hostToUse,
                            registrationsOnHostToUse);
            } else {
                registrationsOnHostToUse = registrationsOnHost;
            }
            final String directory = dropLocation.getDirectory();
            final Collection<U> registrationsInPath = registrationsOnHostToUse.get(directory);
            final List<U> registrationsInPathToUse;
            if (null == registrationsInPath) {
                registrationsInPathToUse = new ArrayList<>();
                registrationsOnHostToUse.put(directory,
                                             registrationsInPathToUse);
            } else {
                registrationsInPathToUse = (List<U>) registrationsInPath;
            }
            registrationsInPathToUse.add((U) registration);
        }
    }

}
