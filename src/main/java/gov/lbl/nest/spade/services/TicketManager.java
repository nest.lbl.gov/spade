package gov.lbl.nest.spade.services;

import java.util.Date;
import java.util.List;

import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.ejb.TicketedFile;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.rs.Bundles;

/**
 * This interface is used to manage the issuing and disposal of ticket that are
 * handed to the workflows to process.
 *
 * When a ticket of a given type is issued for a bundle then it is not possible
 * to issue another ticket of the same type until the original ticket has been
 * disposed of, or in the case it has been retained, released.
 *
 * The purpose of retaining a ticket is to stop any other ticket of that type
 * being issued for the same bundle. The main purpose of this pattern is to stop
 * the application doing infinite re-tried of a bundle that is continuously
 * failing.
 *
 * @author patton
 */
public interface TicketManager {

    /**
     * Blocks the specified ticket signaling that it is no longer in use but should
     * not be issued again. Blocked tickets may explicitly unblocked later or are
     * flushed when the application is deleted.
     *
     * @param ticket
     *            the ticket to be blocked.
     * @param cause
     *            the cause of the blocking of this ticket.
     * @param <T>
     *            The type of ticket to be blocked.
     *
     * @return true when the blocking of the ticket is a success.
     */
    <T> boolean block(final T ticket,
                      String cause);

    /**
     * Dispose of the specified issued ticket making any associated resources
     * available to newly issued tickets, and allowing a ticket for that bundle to
     * be issued again.
     *
     * @param ticket
     *            the ticket to be disposed of.
     * @param <T>
     *            The type of ticket to be disposed.
     *
     * @return true when the disposal of the ticket is a success.
     */
    <T> boolean dispose(final T ticket);

    /**
     * Returns the collection of names of all the bundles that currently have a
     * issued {@link IngestTicket} instance.
     *
     * @return the collection of names of all the bundles that currently have a
     *         issued {@link IngestTicket} instance.
     */
    Bundles getIssuedBundles();

    /**
     * Returns the number of bundles that currently have a issued
     * {@link IngestTicket} instance.
     *
     * It is equvalent to: <code>
     *     getIssuedBundles().size()
     * </code>
     *
     * @return the number of bundles that currently have a issued
     *         {@link IngestTicket} instance.
     */
    int getIssuedCount();

    /**
     * Returns most recent warehouse id for the specified bundle. If there are no
     * placement Id then <code>null</code> is returned.
     *
     * @param bundle
     *            the {@link Bundle} instance for which return the placement ids.
     *
     * @return the sequence of warehouse ids for the specified bundle.
     */
    String getPlacementId(final String bundle);

    /**
     * Returns the sequence of warehouse ids for the specified bundle, ordered from
     * most recent to oldest. If there are no placement Id for the specified bundle
     * either <code>null</code> or an empty list may be returned.
     *
     * @param bundle
     *            the {@link Bundle} instance for which return the placement ids.
     *
     * @return the sequence of warehouse ids for the specified bundle.
     */
    List<String> getPlacementIds(final List<String> bundle);

    /**
     * Returns the collection of names of all the bundles that are currently being
     * retained.
     *
     * @return the collection of names of all the bundles that are currently being
     *         retained.
     */
    Bundles getRetainedBundles();

    /**
     * Returns the sequence of {@link TicketedFile} identities for the specified
     * bundle, ordered from most recent to oldest. If there are no placement Id for
     * the specified bundle either <code>null</code> or an empty list may be
     * returned.
     *
     * @param bundle
     *            the {@link Bundle} instance for which to return the tickets
     *
     * @return the sequence of {@link TicketedFile} identities for the specified
     *         bundle.
     */
    List<String> getTickets(final String bundle);

    /**
     * Issues a new {@link IngestTicket} for the specified bundle. If there is an
     * outstanding {@link IngestTicket} for that bundle then a new ticket will not
     * be issued and <code>null</code> will be returned.
     *
     * @param bundle
     *            the {@link Bundle} instance for which to issue the ticket.
     *
     * @return the issued {@link IngestTicket} of <code>null</code> if one is not
     *         issued.
     *
     * @throws TooManyTicketsException
     *             when there is an attempt to issue more tickets than the specified
     *             limit.
     */
    IngestTicket issue(Bundle bundle) throws TooManyTicketsException;

    /**
     * Reissues the specified ticket which has been recovered during a restart.
     *
     * @param ticket
     *            the ticket to be reissued.
     * @param <T>
     *            The type of ticket to be reissued.
     *
     * @return true when the reissue of the ticket is a success.
     */
    <T> boolean reissue(final T ticket);

    /**
     * Tells this Object the time of the latest scan. Any bundle that have been
     * dispose can now be removed.
     *
     * @param dateTime
     *            the time of the latest scan.
     */
    void setLatestScan(Date dateTime);

    /**
     * Releases the retained ticket making any associated resources available to
     * newly issued tickets, and allowing a ticket for that bundle to be issued
     * again.
     *
     * @param bundle
     *            the {@link Bundle} instance for which to unblock its
     *            {@link IngestTicket}.
     *
     * @return true when the unblocking of the bundle is a success.
     */
    boolean unblock(Bundle bundle);

    /**
     * Unblocks the blocked ticket making any associated resources available to
     * newly issued tickets, and allowing a ticket for that bundle to be issued
     * again.
     *
     * @param ticket
     *            the ticket to be unblocked.
     * @param <T>
     *            The type of ticket to be unblocked.
     *
     * @return true when the unblocking of the ticket is a success.
     */
    <T> boolean unblock(T ticket);

}
