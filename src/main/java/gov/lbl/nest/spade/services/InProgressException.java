package gov.lbl.nest.spade.services;

/**
 * This exception is thrown when there is an attempt to issue more tickets than
 * the specified limit.
 *
 * @author patton
 */
public class InProgressException extends
                                 Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    /**
     * Use by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     */
    public InProgressException() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the message describing why this object was thrown.
     */
    public InProgressException(final String message) {
        super(message);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
