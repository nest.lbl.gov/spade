package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

import gov.lbl.nest.spade.services.CacheManager;
import jakarta.ejb.Stateless;

/**
 * The class is the default implementation of the {@link CacheManager}
 * interface.
 *
 * @author patton
 */
@Stateless
public class CacheManagerImpl implements
                              CacheManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    private static final SimpleFileVisitor<Path> DELETE_TREE = new SimpleFileVisitor<>() {
        @Override
        public FileVisitResult postVisitDirectory(Path dir,
                                                  IOException exc) throws IOException {
            Files.delete(dir);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file,
                                         BasicFileAttributes attrs) throws IOException {
            Files.delete(file);
            return FileVisitResult.CONTINUE;
        }
    };

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Deletes this specified file and, if it is a directory, all files it contains.
     *
     * @param file
     *            the file to delete.
     *
     * @throws IOException
     *             when there is a problem deleting the tree.
     */
    private static void deleteTree(final File file) throws IOException {
        if (!file.exists()) {
            return;
        }
        Files.walkFileTree(file.toPath(),
                           DELETE_TREE);
    }

    @Override
    public boolean copy(File from,
                        File to) {
        try {
            LocalhostTransfer.copy(from,
                                   to);
            return true;
        } catch (IOException
                 | InterruptedException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean delete(File file) {
        try {
            deleteTree(file);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean move(File from,
                        File to) {
        return from.renameTo(to);
    }

    // static member methods (alphabetic)

    @Override
    public void waitForSpace(File file,
                             int storageFactor,
                             String name) {
        // TODO: Needs to be implemented
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
