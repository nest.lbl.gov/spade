package gov.lbl.nest.spade.services.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.services.RegistrationManager;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

/**
 * This is the standard implementation of the {@link RegistrationManager}
 * interface.
 *
 * @author patton
 */
@Stateless
public class RegistrationManagerImpl implements
                                     RegistrationManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Map} of {@link Registration} instances index by host and
     * directory.
     */
    private Map<String, Map<String, Collection<Registration>>> knownRegistrations;

    /**
     * The {@link LoadedRegistrations} instance used by this object.
     */
    @Inject
    private LoadedRegistrations loadedRegistrations;

    /**
     * The {@link Map} of {@link Registration} instances index by local identity
     */
    private Map<String, Registration> registrationsByLocalId;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected RegistrationManagerImpl() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param loadedRegistrations
     *            the {@link LoadedRegistrations} instance used by this object.
     */
    public RegistrationManagerImpl(final LoadedRegistrations loadedRegistrations) {
        this.loadedRegistrations = loadedRegistrations;
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param knownRegistrations
     *            the {@link Map} of {@link Registration} instances index by host
     *            and directory.
     */
    public RegistrationManagerImpl(Map<String, Map<String, Collection<Registration>>> knownRegistrations) {
        this.knownRegistrations = knownRegistrations;
    }

    // instance member method (alphabetic)

    @Override
    public Bundle assignRegistration(final Bundle bundle) {
        final String localId = bundle.getLocalId();
        if (null != localId) {
            final Registration registration = getRegistration(localId);
            if (null == registration) {
                return null;
            }
            return bundle;
        }
        final ExternalFile semaphore = bundle.getSemaphore();
        final String host = semaphore.getHost();
        final String hostToUse;
        if (null == host) {
            hostToUse = LoadedRegistrations.DEFAULT_HOST;
        } else {
            hostToUse = host;
        }
        final Map<String, Map<String, Collection<Registration>>> knownRegistrations = getKnownRegistrations();
        final Map<String, Collection<Registration>> registrationsOnHost = knownRegistrations.get(hostToUse);
        if (null != registrationsOnHost) {
            final Collection<? extends Registration> registrationsInPath = registrationsOnHost.get(semaphore.getDirectory());
            if (null != registrationsInPath) {
                for (Registration registration : registrationsInPath) {
                    final Predicate<String> predicate = registration.getPredicate();
                    final String name = semaphore.getName();
                    if (predicate.test(name)) {
                        bundle.setLocalId(registration.getLocalId());
                        if (null == bundle.getName()) {
                            bundle.setName((registration.getDataLocator()).getBundleName(name));
                        }
                        final String neighbor = registration.getNeighbor();
                        bundle.setNeighbor(neighbor);
                        return bundle;
                    }
                }
            }
        }
        return null;
    }

    private Map<String, Map<String, Collection<Registration>>> getKnownRegistrations() {
        if (null == knownRegistrations) {
            knownRegistrations = loadedRegistrations.getRegistrations();
        }
        return knownRegistrations;
    }

    @Override
    public Registration getRegistration(final String localId) {
        if (null == registrationsByLocalId) {
            registrationsByLocalId = new HashMap<>();
            final Map<String, Map<String, Collection<Registration>>> x = getKnownRegistrations();
            for (String host : x.keySet()) {
                final Map<String, Collection<Registration>> directories = x.get(host);
                for (String directory : directories.keySet()) {
                    for (Registration registration : directories.get(directory)) {
                        registrationsByLocalId.put(registration.getLocalId(),
                                                   registration);
                    }
                }
            }
        }
        return registrationsByLocalId.get(localId);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
