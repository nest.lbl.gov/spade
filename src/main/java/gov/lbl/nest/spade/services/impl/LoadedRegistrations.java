package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Assembly;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.config.InboundTransfer;
import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.config.RegistrationPaths;
import gov.lbl.nest.spade.config.RootedPathDefinition;
import gov.lbl.nest.spade.ejb.KnownInboundRegistration;
import gov.lbl.nest.spade.ejb.KnownLocalRegistration;
import gov.lbl.nest.spade.ejb.KnownNeighbor;
import gov.lbl.nest.spade.ejb.KnownRegistration;
import gov.lbl.nest.spade.interfaces.SpadeDB;
import gov.lbl.nest.spade.registry.LocalRegistration;
import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.registry.internal.InboundLocator;
import gov.lbl.nest.spade.registry.internal.InboundRegistration;
import jakarta.annotation.Resource;
import jakarta.ejb.LockType;
import jakarta.ejb.SessionContext;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;

/**
 * This class contains all of the registrations loaded into the application.
 *
 * @author patton
 */
@Singleton
/** The class does it own concurrency management, so can use READ lock. */
@jakarta.ejb.Lock(LockType.READ)
public class LoadedRegistrations {

    // public static final member data

    /**
     * The host name to use if none is specified.
     */
    public static final String DEFAULT_HOST = "localhost";

    /**
     * The default path, with respect to the configuration directory, to the
     * directory containing the inbound registration instances.
     */
    public static final String DEFAULT_INBOUND_REGISTRY_PATH = "registrations/inbound";

    /**
     * The path, with respect to the configuration file, to the default directory
     * containing the local registration instances.
     */
    public static final String DEFAULT_LOCAL_REGISTRY_PATH = "registrations/local";

    // protected static final member data

    // static final member data

    // private static final member data

    private static final Comparator<Registration> REGISTRATION_COMPARATOR = new Comparator<>() {

        @Override
        public int compare(Registration o1,
                           Registration o2) {
            return ((o1.getPattern()).pattern()).length() - ((o2.getPattern()).pattern()).length();
        }
    };

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(LoadedRegistrations.class);

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance of this application.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @SpadeDB
    private EntityManager entityManager;

    /**
     * True if the registrations have been loaded.
     */
    private boolean loaded = false;

    /**
     * The {@link LoadedNeighborhood} instance used by this object.
     */
    @Inject
    private LoadedNeighborhood loadedNeighborhood;

    /**
     * The {@link Lock} instance used to make sure that the registration loading is
     * serialized.
     */
    private final Lock lock = new ReentrantLock();

    /**
     * The {@link Map} of {@link LocalRegistration} instances index by host and
     * directory. for local registrations
     */
    private final Map<String, Map<String, Collection<LocalRegistration>>> localByHost = new HashMap<>();

    /**
     * The {@link Map} of {@link InboundRegistration} instances index by host and
     * directory.
     */
    private final Map<String, Map<String, Collection<InboundRegistration>>> inboundByHost = new HashMap<>();

    /**
     * The {@link Collection} of localDis for {@link Registration} instances that
     * have one or more {@link OutboundTransfer} instances.
     */
    private final Collection<String> outboundRegistrations = new ArrayList<>();

    /**
     * The {@link Map} of {@link Registration} instances index by host and
     * directory.
     */
    private final Map<String, Map<String, Collection<Registration>>> registrationsByHost = new HashMap<>();

    /**
     * The {@link Map} of {@link Registration} instances index by
     * {@link OutboundTransfer} names.
     */
    private final Map<String, Collection<String>> registrationsByOutboundNeighbors = new HashMap<>();

    /**
     * The {@link Map} of {@link Registration} instances index by local identity
     */
    private final Map<String, Registration> registrationsByLocalId = new HashMap<>();

    /**
     * This object's {@link SessionContext} instance used to trigger new
     * transactions.
     */
    @Resource
    SessionContext sessionContext;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected LoadedRegistrations() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param configuration
     *            the {@link Configuration} instance of this application.
     * @param entityManager
     *            the {@link EntityManager} instance used by this object.
     * @param loadedNeighborhood
     *            the {@link LoadedNeighborhood} instance used by this object.
     */
    public LoadedRegistrations(final Configuration configuration,
                               final EntityManager entityManager,
                               LoadedNeighborhood loadedNeighborhood) {
        this.configuration = configuration;
        this.entityManager = entityManager;
        this.loadedNeighborhood = loadedNeighborhood;
    }

    // instance member method (alphabetic)

    /**
     * Adds the supplied collection of {@link Registration} instance to this object.
     *
     * @param registrations
     *            the collection of {@link Registration} instance to add to this
     *            object.
     */
    private <T extends Registration> void extendRegistrations(final List<T> registrations) {
        LoadedRegistrationsUtilities.extendRegistrationMappings(registrations,
                                                                registrationsByHost);
        for (Registration registration : registrations) {
            registrationsByLocalId.put(registration.getLocalId(),
                                       registration);
            final Collection<String> outboundTransfers = registration.getOutboundTransfers();
            if (!(null == outboundTransfers || outboundTransfers.isEmpty())) {
                outboundRegistrations.add(registration.getLocalId());
                for (String outboundTransfer : outboundTransfers) {
                    final OutboundTransfer transfer = loadedNeighborhood.getOutboundTransfer(outboundTransfer);
                    final String neighbor = transfer.getNeighbor();
                    final Collection<String> registrationsByOutboundNeighbor = registrationsByOutboundNeighbors.get(neighbor);
                    final Collection<String> registrationsByOutboundToUse;
                    if (null == registrationsByOutboundNeighbor) {
                        registrationsByOutboundToUse = new ArrayList<>();
                        registrationsByOutboundNeighbors.put(neighbor,
                                                             registrationsByOutboundToUse);
                    } else {
                        registrationsByOutboundToUse = registrationsByOutboundNeighbor;
                    }
                    registrationsByOutboundToUse.add(registration.getLocalId());
                }
            }
        }
    }

    private boolean findOrCreateRegistration(final InboundRegistration registration) {
        final TypedQuery<KnownInboundRegistration> query = entityManager.createNamedQuery("getInboundRegistrationsByLocalId",
                                                                                          KnownInboundRegistration.class);
        query.setParameter("localId",
                           registration.getLocalId());
        final String neighbor = registration.getNeighbor();
        try {
            final KnownInboundRegistration knownRegistration = query.getSingleResult();
            final KnownNeighbor knownNeighbor = knownRegistration.getKnownNeighbor();
            if (null != knownNeighbor && (knownNeighbor.getName()).equals(neighbor)) {
                return true;
            }
            if (null == knownNeighbor) {
                LOG.error("The registration \"" + registration.getLocalId()
                          + "\" refers to an unknown neighbor, \""
                          + neighbor
                          + "\", so it will be ignored");
                return false;
            }
            final KnownRegistration knownLocalRegistration = new KnownInboundRegistration(registration,
                                                                                          knownNeighbor,
                                                                                          knownRegistration);
            entityManager.persist(knownLocalRegistration);
            LOG.warn("The registration \"" + registration.getLocalId()
                     + "\" refers to different neighbor, \""
                     + neighbor
                     + "\", than before so it will supersede that previous one");
            return true;
        } catch (NoResultException e) {
            final KnownNeighbor knownNeighbor = loadedNeighborhood.getKnownNeighbor(neighbor);
            if (null == knownNeighbor) {
                LOG.error("The registration \"" + registration.getLocalId()
                          + "\" refers to an unknown neighbor, \""
                          + neighbor
                          + "\", so it will be ignored");
                return false;
            }
            final KnownRegistration knownLocalRegistration = new KnownInboundRegistration(registration,
                                                                                          knownNeighbor);
            entityManager.persist(knownLocalRegistration);
            return true;
        }
    }

    /**
     * Returns a {@link Map} of {@link Registration} instances for inbound
     * transfers, indexed by host and directory.
     *
     * @return a {@link Map} of {@link Registration} instances for inbound
     *         transfers, indexed by host and directory.
     */
    public Map<String, Map<String, Collection<InboundRegistration>>> getInboundRegistrations() {
        // Make sure registrations are loaded.
        loadRegistrations();

        return inboundByHost;
    }

    /**
     * Returns a collection of {@link InboundRegistration} instances containing the
     * set of known inbound registrations.
     *
     * @param registrations
     *            the collections of registrations to include in the returned
     *            collection. A <code>null</code> or empty list means all
     *            registrations should be included.
     *
     * @return the collection of {@link InboundRegistration} instances containing
     *         the set of known inbound registrations.
     */
    public List<InboundRegistration> getInboundRegistrations(final List<String> registrations) {
        // Make sure registrations are loaded.
        loadRegistrations();

        final List<InboundRegistration> result = new ArrayList<>();
        for (Registration registration : registrationsByLocalId.values()) {
            if (registration instanceof InboundRegistration && (null == registrations || registrations.isEmpty()
                                                                || registrations.contains(registration.getLocalId()))) {
                result.add((InboundRegistration) registration);
            }
        }
        return result;
    }

    /**
     * Returns a collection of {@link KnownRegistration} instances associated with
     * the specified neighbor. If no neighbor is specified then the collection will
     * container all local registrations.
     *
     * @param knownNeighbor
     *            the {@link KnownNeighbor} instance specifying which
     *            {@link KnownRegistration} instances should be included in the
     *            result.
     * @param registrations
     *            the collection of registrations names that may be included in the
     *            result.
     *
     * @return the collection of {@link KnownRegistration} instances associated with
     *         the specified neighbor.
     */
    public List<? extends KnownRegistration> getKnownDispatchingRegistrations(final KnownNeighbor knownNeighbor,
                                                                              final List<String> registrations) {
        // Make sure registrations are loaded.
        loadRegistrations();

        final TypedQuery<KnownRegistration> query;
        if (null == registrations || registrations.isEmpty()) {
            query = entityManager.createNamedQuery("getKnownRegistrationsByLocalIds",
                                                   KnownRegistration.class);
            if (null == knownNeighbor) {
                query.setParameter("localIds",
                                   outboundRegistrations);
            } else {
                final Collection<String> registrationsByOutboundNeighbor = registrationsByOutboundNeighbors.get(knownNeighbor.getName());
                if (null == registrationsByOutboundNeighbor) {
                    return null;
                }
                query.setParameter("localIds",
                                   registrationsByOutboundNeighbor);
            }
        } else {
            query = entityManager.createNamedQuery("getKnownRegistrationsByLocalIds",
                                                   KnownRegistration.class);
            if (null == knownNeighbor) {
                query.setParameter("localIds",
                                   registrations);
            } else {
                final Collection<String> registrationsByOutboundNeighbor = registrationsByOutboundNeighbors.get(knownNeighbor.getName());
                if (null == registrationsByOutboundNeighbor || registrationsByOutboundNeighbor.isEmpty()) {
                    return null;
                }
                final List<String> registrationsToUse = new ArrayList<>();
                for (String registration : registrations) {
                    if (registrationsByOutboundNeighbor.contains(registration)) {
                        registrationsToUse.add(registration);
                    }
                }
                if (registrationsToUse.isEmpty()) {
                    return null;
                }
                query.setParameter("localIds",
                                   registrationsToUse);
            }
        }

        return query.getResultList();
    }

    /**
     * Returns a collection of {@link KnownRegistration} instances associated with
     * the specified neighbor. If no neighbor is specified then the collection will
     * container all local registrations.
     *
     * @param knownNeighbor
     *            the {@link KnownNeighbor} instance specifying which
     *            {@link KnownRegistration} instances should be included in the
     *            result.
     * @param registrations
     *            the collection of registrations names that may be included in the
     *            result.
     *
     * @return the collection of {@link KnownRegistration} instances associated with
     *         the specified neighbor.
     */
    public List<KnownInboundRegistration> getKnownReceivingRegistrations(final KnownNeighbor knownNeighbor,
                                                                         final List<String> registrations) {
        // Make sure registrations are loaded.
        loadRegistrations();

        final TypedQuery<KnownInboundRegistration> query;
        if (null == registrations || registrations.isEmpty()) {
            if (null == knownNeighbor) {
                query = entityManager.createNamedQuery("getInboundRegistrations",
                                                       KnownInboundRegistration.class);
            } else {
                query = entityManager.createNamedQuery("getInboundRegistrationsByNeighbor",
                                                       KnownInboundRegistration.class);
                query.setParameter("neighbor",
                                   knownNeighbor);
            }
        } else {
            if (null == knownNeighbor) {
                query = entityManager.createNamedQuery("getInboundRegistrationsByLocalIds",
                                                       KnownInboundRegistration.class);
            } else {
                query = entityManager.createNamedQuery("getInboundRegistrationsByLocalIdsAndNeighbor",
                                                       KnownInboundRegistration.class);
                query.setParameter("neighbor",
                                   knownNeighbor);
            }
            query.setParameter("localIds",
                               registrations);
        }

        if (null == query) {
            return null;
        }
        return query.getResultList();
    }

    /**
     * Returns a collection of {@link KnownRegistration} instances associated with
     * the specified neighbor. If no neighbor is specified then the collection will
     * container all local registrations.
     *
     * @param knownNeighbor
     *            the {@link KnownNeighbor} instance specifying which
     *            {@link KnownRegistration} instances should be included in the
     *            result.
     * @param registrations
     *            the collection of registrations names that may be included in the
     *            result.
     *
     * @return the collection of {@link KnownRegistration} instances associated with
     *         the specified neighbor.
     */
    public List<KnownRegistration> getKnownRegistrations(final KnownNeighbor knownNeighbor,
                                                         final List<String> registrations) {
        // Make sure registrations are loaded.
        loadRegistrations();

        final TypedQuery<KnownRegistration> query;
        if (null == registrations || registrations.isEmpty()) {
            if (null == knownNeighbor) {
                query = entityManager.createNamedQuery("getKnownRegistrations",
                                                       KnownRegistration.class);
            } else {
                return getKnownRegistrationsByNeighbor(knownNeighbor,
                                                       registrations);
            }
        } else {
            if (null == knownNeighbor) {
                query = entityManager.createNamedQuery("getKnownRegistrationsByLocalIds",
                                                       KnownRegistration.class);
            } else {
                return getKnownRegistrationsByNeighbor(knownNeighbor,
                                                       registrations);
            }
            query.setParameter("localIds",
                               registrations);
        }

        if (null == query) {
            return null;
        }
        return query.getResultList();
    }

    private List<KnownRegistration> getKnownRegistrationsByNeighbor(final KnownNeighbor knownNeighbor,
                                                                    final List<String> registrations) {
        final List<KnownRegistration> result = new ArrayList<>();
        List<? extends KnownRegistration> dispatchRegistrations = getKnownDispatchingRegistrations(knownNeighbor,
                                                                                                   registrations);
        if (!(null == dispatchRegistrations || dispatchRegistrations.isEmpty())) {
            for (KnownRegistration known : dispatchRegistrations) {
                result.add(known);
            }

        }
        List<KnownInboundRegistration> receivingRegistrations = getKnownReceivingRegistrations(knownNeighbor,
                                                                                               registrations);
        if (!(null == receivingRegistrations || receivingRegistrations.isEmpty())) {
            for (KnownRegistration known : receivingRegistrations) {
                if (!result.contains(known)) {
                    result.add(known);
                }
            }
        }
        return result;
    }

    /**
     * Returns a {@link Map} of {@link LocalRegistration} instances for local
     * bundles, indexed by host and directory.
     *
     * @return the {@link Map} of {@link LocalRegistration} instances for local
     *         bundles, indexed by host and directory.
     */
    public Map<String, Map<String, Collection<LocalRegistration>>> getLocalRegistrations() {
        // Make sure registrations are loaded.
        loadRegistrations();

        return localByHost;
    }

    /**
     * Returns a collection of {@link LocalRegistration} instances containing the
     * set of known local registrations.
     *
     * @param registrations
     *            the collections of registrations to include in the returned
     *            collection. A <code>null</code> or empty list means all
     *            registrations should be included.
     *
     * @return the collection of {@link LocalRegistration} instances containing the
     *         set of known local registrations.
     */
    public List<LocalRegistration> getLocalRegistrations(final List<String> registrations) {
        // Make sure registrations are loaded.
        loadRegistrations();

        final List<LocalRegistration> result = new ArrayList<>();
        for (Registration registration : registrationsByLocalId.values()) {
            if (registration instanceof LocalRegistration && (null == registrations || registrations.isEmpty()
                                                              || registrations.contains(registration.getLocalId()))) {
                result.add((LocalRegistration) registration);
            }
        }
        return result;
    }

    /**
     * Returns a {@link Map} of {@link Registration} instances index by host and
     * directory.
     *
     * @return a {@link Map} of {@link Registration} instances index by host and
     *         directory.
     */
    public Map<String, Map<String, Collection<Registration>>> getRegistrations() {
        // Make sure registrations are loaded.
        loadRegistrations();

        return registrationsByHost;
    }

    /**
     * Loads the known registrations into this object.
     */
    private void loadRegistrations() {
        if (!loaded) {
            lock.lock();
            try {
                if (!loaded) {
                    // Simple forces Neighborhood to be loaded.
                    loadedNeighborhood.getNeighborhood();

                    if (null == sessionContext) {
                        prepareRegistrations();
                    } else {
                        final LoadedRegistrations self = sessionContext.getBusinessObject(LoadedRegistrations.class);
                        self.prepareRegistrations();
                    }
                    loaded = true;
                }
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Pre-loads the {@link Registration} instances in their own transaction.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void preload() {
        loadRegistrations();
    }

    /**
     * Returns the collection of default {@link InboundRegistration} instances for
     * all the {@link InboundTransfer} instance in the application.
     *
     * @return the collection of default {@link InboundRegistration} instances for
     *         all the {@link InboundTransfer} instance in the application.
     */
    private List<InboundRegistration> prepareDefaultRegistrations() {
        final List<InboundRegistration> defaultRegistrations = new ArrayList<>();

        final List<InboundTransfer> inboundTransfers = configuration.getInboundTransfers();
        if (null != inboundTransfers) {
            for (InboundTransfer transfer : inboundTransfers) {
                final InboundRegistration registration = transfer.getDefaultInboundRegistration();
                if (findOrCreateRegistration(registration)) {
                    defaultRegistrations.add(registration);
                }
            }
            /*
             * This first extension of default registrations extends the `inboundByHost`
             * field.
             */
            LoadedRegistrationsUtilities.extendRegistrationMappings(defaultRegistrations,
                                                                    inboundByHost);
        }
        return defaultRegistrations;
    }

    /**
     * Returns the collection of {@link InboundRegistration} instances found in the
     * supplied tree.
     *
     * @param directory
     *            the directory below which the registration tree will be found.
     * @param locations
     *            the {@link RegistrationPaths} instance, if any, that specifies
     *            where, below the directory, the registration tree is rooted.
     *
     * @return the collection of {@link InboundRegistration} instances found in the
     *         supplied tree.
     */
    private List<InboundRegistration> prepareInboundRegistrations(final File directory,
                                                                  final RegistrationPaths locations) {
        final File directoryToUse;
        if (null == locations || null == locations.getInbound()) {
            directoryToUse = new File(directory,
                                      DEFAULT_INBOUND_REGISTRY_PATH);
        } else {
            final File location = new File(locations.getInbound());
            if (location.isAbsolute()) {
                directoryToUse = location;
            } else {
                directoryToUse = new File(directory,
                                          location.getPath());
            }
        }

        final List<InboundTransfer> transfers = configuration.getInboundTransfers();
        final List<InboundRegistration> inboundRegistrations = InboundRegistration.readInboundFromTree(directoryToUse);
        final Iterator<InboundRegistration> iterator = inboundRegistrations.iterator();
        while (iterator.hasNext()) {
            final InboundRegistration registration = iterator.next();
            if ((registration.getLocalId()).contains(InboundLocator.SEPARATOR)) {
                LOG.error("The registration \"" + registration.getLocalId()
                          + "\" contains \""
                          + InboundLocator.SEPARATOR
                          + "\" is its localId which is no allowed so it will be ignored");
                iterator.remove();
            } else {
                final InboundTransfer transfer = InboundTransfer.bind(registration,
                                                                      transfers);
                if (null == transfer) {
                    LOG.error("The registration \"" + registration.getLocalId()
                              + "\" references an unknown inboundTransfer, \""
                              + (registration.getInboundReference()).getName()
                              + "\", so it will be ignored");
                    iterator.remove();
                } else {
                    // findOrCreateRegistration logs reason for failure
                    if (!findOrCreateRegistration(registration)) {
                        iterator.remove();
                    }
                }
            }
        }

        Collections.sort(inboundRegistrations,
                         REGISTRATION_COMPARATOR);
        /*
         * This first extension of inbound registrations extends the `inboundByHost`
         * field.
         */
        LoadedRegistrationsUtilities.extendRegistrationMappings(inboundRegistrations,
                                                                inboundByHost);

        final String plural;
        if (1 == inboundRegistrations.size()) {
            plural = "";
        } else {
            plural = "s";
        }
        final Assembly assembly = configuration.getAssembly();
        LOG.info("Found " + inboundRegistrations.size()
                 + " inbound registration"
                 + plural
                 + " for \""
                 + assembly.getName()
                 + "\" in or below \""
                 + directoryToUse
                 + "\"");
        return inboundRegistrations;
    }

    /**
     * Returns the collection of {@link LocalRegistration} instances found in the
     * supplied tree.
     *
     * @param directory
     *            the directory below which the registration tree will be found.
     * @param locations
     *            the {@link RegistrationPaths} instance, if any, that specifies
     *            where, below the directory, the registration tree is rooted.
     *
     * @return the collection of {@link LocalRegistration} instances found in the
     *         supplied tree.
     */
    private List<LocalRegistration> prepareLocalRegistrations(final File directory,
                                                              final RegistrationPaths locations) {
        final File directoryToUse;
        if (null == locations || null == locations.getLocal()) {
            directoryToUse = new File(directory,
                                      DEFAULT_LOCAL_REGISTRY_PATH);
        } else {
            final File location = new File(locations.getLocal());
            if (location.isAbsolute()) {
                directoryToUse = location;
            } else {
                directoryToUse = new File(directory,
                                          location.getPath());
            }
        }

        final RootedPathDefinition warehouseDefinition = configuration.getWarehouseDefinition();
        final File warehouseRoot;
        if (null == warehouseDefinition) {
            warehouseRoot = null;
        } else {
            warehouseRoot = warehouseDefinition.getRootDirectory();
        }
        final List<LocalRegistration> localRegistrations = LocalRegistration.readLocalFromTree(directoryToUse,
                                                                                               warehouseRoot);
        final Iterator<LocalRegistration> iterator = localRegistrations.iterator();
        while (iterator.hasNext()) {
            final LocalRegistration registration = iterator.next();
            if ((registration.getLocalId()).contains(InboundLocator.SEPARATOR)) {
                LOG.error("The registration \"" + registration.getLocalId()
                          + "\" contains \""
                          + InboundLocator.SEPARATOR
                          + "\" is its localId which is no allowed so it will be ignored");
                iterator.remove();
            } else {

                final TypedQuery<KnownLocalRegistration> query = entityManager.createNamedQuery("getLocalRegistrationByLocalId",
                                                                                                KnownLocalRegistration.class);
                query.setParameter("localId",
                                   registration.getLocalId());
                KnownLocalRegistration knownLocalRegistration;
                try {
                    knownLocalRegistration = query.getSingleResult();
                } catch (NoResultException e) {
                    knownLocalRegistration = new KnownLocalRegistration(registration);
                    entityManager.persist(knownLocalRegistration);
                }
            }
        }

        /*
         * This first extension of local registrations extends the `localByHost` field.
         */
        LoadedRegistrationsUtilities.extendRegistrationMappings(localRegistrations,
                                                                localByHost);

        final String plural;
        if (1 == localRegistrations.size()) {
            plural = "";
        } else {
            plural = "s";
        }
        final Assembly assembly = configuration.getAssembly();
        LOG.info("Found " + localRegistrations.size()
                 + " local registration"
                 + plural
                 + " for \""
                 + assembly.getName()
                 + "\" in or below \""
                 + directoryToUse
                 + "\"");
        return localRegistrations;
    }

    /**
     * Prepares the known registration for use by this object, within its own
     * transaction.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void prepareRegistrations() {
        final File configurationDirectory = configuration.getDirectory();
        final RegistrationPaths locations = configuration.getRegistrationPaths();
        final List<LocalRegistration> localRegistrations = prepareLocalRegistrations(configurationDirectory,
                                                                                     locations);
        // This second extension of local registrations extends the
        // `registrationsByHost` field.
        extendRegistrations(localRegistrations);
        final List<InboundRegistration> inboundRegistrations = prepareInboundRegistrations(configurationDirectory,
                                                                                           locations);
        // This second extension of inbound registrations extends the
        // `registrationsByHost` field.
        extendRegistrations(inboundRegistrations);
        final List<InboundRegistration> defaultRegistrations = prepareDefaultRegistrations();
        // This second extension of default registrations extends the
        // `registrationsByHost` field.
        extendRegistrations(defaultRegistrations);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
