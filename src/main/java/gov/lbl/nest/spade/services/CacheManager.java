package gov.lbl.nest.spade.services;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * This interface is used the manage the cache area used by the application.
 *
 * @author patton
 *
 */
public interface CacheManager {

    /**
     * Copies a file from one location to another within the cache..
     *
     * @param from
     *            the location of the source file
     * @param to
     *            the location of the destination file.
     *
     * @return true when the move has been successful.
     *
     * @throws IOException
     *             when there is a problem with the move.
     */
    boolean copy(final File from,
                 final File to) throws IOException;

    /**
     * Deletes a file.
     *
     * @param file
     *            the location of the file to be deleted.
     *
     * @return true when the deletion has been successful.
     */
    boolean delete(final File file);

    /**
     * Moves a file from one location to another within the cache..
     *
     * @param from
     *            the location of the source file
     * @param to
     *            the location of the destination file.
     *
     * @return true when the move has been successful.
     *
     * @throws IOException
     *             when there is a problem with the move.
     */
    boolean move(final File from,
                 final File to) throws IOException;

    /**
     * This method blocks until there is sufficient free space on the specified
     * local partition.
     *
     * @param file
     *            a file that is on the partition where the free space is required.
     * @param storageFactor
     *            the multiple for the minimum number of bytes that must be free for
     *            this method.
     * @param name
     *            the name of the component waiting for space.
     *
     * @throws TimeoutException
     *             when this method times out rather then enough space opening up.
     * @throws InterruptedException
     *             when this wait has been interrupted.
     */
    void waitForSpace(final File file,
                      final int storageFactor,
                      final String name) throws TimeoutException,
                                         InterruptedException;
}
