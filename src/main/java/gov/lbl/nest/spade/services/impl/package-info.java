/**
 * The package contains the built-in implementations of the SPADE services.
 *
 * @author patton
 *
 * @see gov.lbl.nest.spade.services
 */
package gov.lbl.nest.spade.services.impl;