package gov.lbl.nest.spade.services;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import gov.lbl.nest.spade.interfaces.policy.ImplementedPolicy;
import gov.lbl.nest.spade.registry.ExternalLocation;

/**
 * This interface define how files are transferred to destination locations.
 *
 * @author patton
 *
 */
public interface FileTransfer extends
                              ImplementedPolicy {

    /**
     * Attempts to send a file and its semaphore file and place them in the
     * specified destination location
     *
     * @param targetLocation
     *            the {@link ExternalLocation} that is the destination for both
     *            files.
     * @param metadataFile
     *            the local metadata file to be sent.
     * @param targetMetadataName
     *            the name of the metadata file in the remote location if different
     *            from the local name. <code>null</code> means that the files both
     *            have the same name.
     * @param transferFile
     *            the local file or directory to be sent.
     * @param targetTransferName
     *            the name of the file or directory in the remote location if
     *            different from the local name. <code>null</code> means that the
     *            files both have the same name.
     * @param semaphoreFile
     *            the local file to be used as the semaphore file.
     * @param targetSemaphoreName
     *            the name of the semaphore file in the remote location if different
     *            from the local name, <code>null</code> means that the semaphore
     *            files both have the same name.
     * @param callback
     *            the {@link URI} used to identify the calling activity so that is
     *            can be used, if necessary, to indicate when an asynchronously has
     *            completed.
     *
     * @return true if the send has completed, that's to say synchronous, false if
     *         it has started but not completed, that is to say asynchronously.
     *
     * @throws IOException
     *             when one of the files can not be sent.
     * @throws InterruptedException
     *             when this method is interrupted.
     */
    boolean send(final ExternalLocation targetLocation,
                 final File metadataFile,
                 final String targetMetadataName,
                 final File transferFile,
                 final String targetTransferName,
                 final File semaphoreFile,
                 final String targetSemaphoreName,
                 final URI callback) throws IOException,
                                     InterruptedException;

    /**
     * Sets the environment in which this object is executing.
     *
     * @param configurationDir
     *            the directory where configuration information for this object may
     *            be found.
     */
    void setEnvironment(final File configurationDir);
}
