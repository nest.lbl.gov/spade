package gov.lbl.nest.spade.services;

/**
 * This interface is used to suppress a stacktrace being output for a particular
 * {@link Exception} subclass, rather the alternate message will be displayed in
 * the log file.
 *
 * @author patton
 *
 */
public interface NonStrackTraceException {

    /**
     * Returns the message to output as an alternate to executing a
     * <code>printStackTrace</code> method.
     *
     * @return the message to output as an alternate to executing a
     *         <code>printStackTrace</code> method.
     */
    String getAlternateMessage();
}
