package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendedException;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.activities.Fetcher;
import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.Assembly;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.config.ConfigurationError;
import gov.lbl.nest.spade.config.Neighbor;
import gov.lbl.nest.spade.interfaces.policy.FetchingPolicy;
import gov.lbl.nest.spade.registry.HandlingPriority;
import gov.lbl.nest.spade.registry.LocalRegistration;
import gov.lbl.nest.spade.registry.Options;
import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.registry.Registry;
import gov.lbl.nest.spade.registry.internal.InboundRegistration;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.rs.Bundles;
import gov.lbl.nest.spade.rs.Slice;
import gov.lbl.nest.spade.rs.Tickets;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.InProgressException;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import gov.lbl.nest.spade.services.RegistrationManager;
import gov.lbl.nest.spade.services.RetryManager;
import gov.lbl.nest.spade.services.Spade;
import gov.lbl.nest.spade.services.SpoolManager;
import gov.lbl.nest.spade.services.TicketManager;
import gov.lbl.nest.spade.services.TooManyTicketsException;
import gov.lbl.nest.spade.services.VerificationManager;
import gov.lbl.nest.spade.utilities.BundleOrdering;
import gov.lbl.nest.spade.utilities.OldestAndNewestOrdering;
import gov.lbl.nest.spade.utilities.Scanner;
import gov.lbl.nest.spade.workflow.ActivityManager;
import gov.lbl.nest.spade.workflow.ActivityMonitor;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowDirector;
import gov.lbl.nest.spade.workflow.WorkflowsManager;
import gov.lbl.nest.spade.workflow.WorkflowsMonitor;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

/**
 * This class is the implementation of the {@link Spade} interface.
 *
 * @author patton
 */
@Stateless
public class SpadeImpl implements
                       Spade {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link BundleOrdering} instance to use when scanning for semaphore files.
     */
    private static final BundleOrdering BUNDLE_ORDERING = new OldestAndNewestOrdering();

    /**
     * The number of minutes to wait to get the ingest lock before abandoning the
     * attempt.
     */
    private static final long DEFAULT_INGEST_TIMOUT = 2;
    /**
     * The default limit of bundle for any single registration for each invocation
     * of the {@link #ingest(Bundles, URI)} method.
     */
    private final static int DEFAULT_SCAN_LIMIT = 4;

    /**
     * The {@link Lock} instance to avoid multiple delivery scans at the same time.
     *
     * This is static as there can be multiple instances of this class in a single
     * application.
     */
    private static final Lock DELIVERY_LOCK = new ReentrantLock();

    /**
     * The {@link Lock} instance to avoid multiple inbound scans at the same time.
     *
     * This is static as there can be multiple instances of this class in a single
     * application.
     */
    private static final Lock INBOUND_LOCK = new ReentrantLock();

    /**
     * The {@link Lock} instance to avoid multiple ingestions at the same time.
     *
     * This is static as there can be multiple instances of this class in a single
     * application.
     */
    private static final Lock INGEST_LOCK = new ReentrantLock();

    /**
     * The {@link Lock} instance to avoid multiple local scans at the same time.
     *
     * This is static as there can be multiple instances of this class in a single
     * application.
     */
    private static final Lock LOCAL_LOCK = new ReentrantLock();

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(SpadeImpl.class);

    /**
     * The {@link Lock} instance to avoid multiple redispatch scans at the same
     * time.
     *
     * This is static as there can be multiple instances of this class in a single
     * application.
     */
    private static final Lock REDISPATCH_LOCK = new ReentrantLock();

    /**
     * The fraction of the the ticket limit above which no redispatch tickets can be
     * issued.
     */
    private static final double REDISPATCH_LIMIT_FRACTION = 0.5;

    // private static member data

    private final static AtomicBoolean urlWarning = new AtomicBoolean();

    // private instance member data

    /**
     * Returns the appropriate string to uae as a plural suffix
     *
     * @param count
     *            the number of items whose plural should be returned.
     *
     * @return
     */
    private static String createPlural(final Integer count) {
        final String plural;
        if (1 == count) {
            plural = "";
        } else {
            plural = "s";
        }
        return plural;
    }

    /**
     * The directory that contains the main configuration files for the application.
     */
    private File applicationDirectory;

    /**
     * The {@link Assembly} used by this object.
     */
    private Assembly assembly;

    /**
     * The {@link Bookkeeping} used by this object.
     */
    @Inject
    private Bookkeeping bookkeeping;

    /**
     * The {@link Configuration} used by this object.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link FetchingPolicy} used by this object for inbound bundles.
     */
    private FetchingPolicy inboundPolicy;

    /**
     * The {@link LoadedRegistrations} used by this object.
     */
    @Inject
    private LoadedRegistrations loadedRegistrations;

    /**
     * The {@link FetchingPolicy} used by this object for local bundles.
     */
    private FetchingPolicy localPolicy;

    /**
     * The {@link NeighborhoodManager} used by this object.
     */
    @Inject
    private NeighborhoodManager neighborhoodManager;

    /**
     * The {@link RegistrationManager} used by this object.
     */
    @Inject
    private RegistrationManager registrationManager;

    /**
     * The {@link RetryManager} used by this object.
     */
    @Inject
    private RetryManager retryManager;

    /**
     * The {@link SpoolManager} used by this object.
     */
    @Inject
    private SpoolManager spoolManager;

    /**
     * The {@link TicketManager} used by this object.
     */
    @Inject
    private TicketManager ticketManager;

    /**
     * The {@link VerificationManager} used by this object.
     */
    @Inject
    private VerificationManager verificationManager;

    // constructors

    /**
     * The {@link WorkflowsManager} used by this object.
     */
    @Inject
    private WorkflowsManager workflowsManager;

    /**
     * Creates an instance of this class.
     */
    protected SpadeImpl() {
    }

    /**
     * Creates an instance of this class for test purposes only.
     *
     * @param assembly
     *            the {@link Assembly} used by this object.
     * @param registrationManager
     *            the {@link RegistrationManager} used by the created object.
     * @param ticketManager
     *            the {@link TicketManager} used by the created object.
     * @param workflowManager
     *            the {@link WorkflowsManager} used by the created object.
     */
    public SpadeImpl(Assembly assembly,
                     RegistrationManager registrationManager,
                     TicketManager ticketManager,
                     WorkflowsManager workflowManager) {
        this.assembly = assembly;
        this.registrationManager = registrationManager;
        this.ticketManager = ticketManager;
        this.workflowsManager = workflowManager;
    }

    /**
     * Creates an instance of this class for test purposes only.
     *
     * @param assembly
     *            the {@link Assembly} used by this object.
     * @param retryManager
     *            the {@link RetryManager} used by the created object.
     * @param ticketManager
     *            the {@link TicketManager} used by the created object.
     * @param workflowManager
     *            the {@link WorkflowsManager} used by the created object.
     */
    public SpadeImpl(Assembly assembly,
                     RetryManager retryManager,
                     TicketManager ticketManager,
                     WorkflowsManager workflowManager) {
        this.assembly = assembly;
        this.retryManager = retryManager;
        this.ticketManager = ticketManager;
        this.workflowsManager = workflowManager;
    }

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param configuration
     *            the {@link Configuration} used by this object.
     * @param bookkeeping
     *            the {@link LoadedRegistrations} used by this object.
     * @param loadedRegisrations
     *            the {@link Bookkeeping} used by this object.
     * @param neighborhoodManager
     *            TODO
     * @param registrationManager
     *            the {@link RegistrationManager} used by the created object.
     * @param retryManager
     *            the {@link RetryManager} used by the created object.
     * @param ticketManager
     *            the {@link TicketManager} used by the created object.
     * @param verificationManager
     *            the {@link VerificationManager} used by this object.
     * @param workflowManager
     *            the {@link WorkflowsManager} used by the created object.
     */
    public SpadeImpl(final Configuration configuration,
                     final Bookkeeping bookkeeping,
                     final LoadedRegistrations loadedRegisrations,
                     final NeighborhoodManager neighborhoodManager,
                     final RegistrationManager registrationManager,
                     final RetryManager retryManager,
                     final TicketManager ticketManager,
                     final VerificationManager verificationManager,
                     final WorkflowsManager workflowManager) {
        this.bookkeeping = bookkeeping;
        this.configuration = configuration;
        this.neighborhoodManager = neighborhoodManager;
        this.registrationManager = registrationManager;
        this.retryManager = retryManager;
        this.ticketManager = ticketManager;
        this.verificationManager = verificationManager;
        this.workflowsManager = workflowManager;
    }

    @Override
    public Tickets abandon(final Tickets tickets) throws SuspendedException,
                                                  InitializingException {
        if (isSuspended()) {
            throw new SuspendedException("The Application is suspended");
        }
        final List<String> result = new ArrayList<>();
        if (null != tickets) {
            Collection<String> content = tickets.getTickets();
            if (null != content) {
                for (String ticket : content) {
                    boolean success = false;
                    success |= retryManager.cancel(ticket);
                    success |= bookkeeping.abandon(ticket);
                    if (success) {
                        LOG.info("Abandoned ticket \"" + ticket
                                 + "\"");
                        result.add(ticket);
                    }
                }
            }
        }
        return new Tickets(null,
                           result);
    }

    @Override
    public List<? extends DigestChange> delivery(final String neighbor,
                                                 final List<? extends DigestChange> confirmables) {
        return verificationManager.confirm(neighbor,
                                           confirmables);
    }

    @Override
    public List<DigestChange> deliveryScan() throws SuspendedException,
                                             InitializingException,
                                             InProgressException {
        if (DELIVERY_LOCK.tryLock()) {
            try {
                if (isSuspended()) {
                    throw new SuspendedException("The Application is suspended");
                }
                final List<DigestChange> result = new ArrayList<>();
                final Collection<Neighbor> neighbors = neighborhoodManager.getNeighbourhood();
                if (null != neighbors) {
                    for (Neighbor neighbor : neighbors) {
                        final List<? extends DigestChange> confirmables = neighborhoodManager.getConfirmables(neighbor);
                        if (null != confirmables) {
                            result.addAll(delivery(neighbor.getName(),
                                                   confirmables));
                        }
                    }
                }
                return result;
            } finally {
                DELIVERY_LOCK.unlock();
            }
        }
        throw new InProgressException("Delivery Scan request in progress, so new request will be ignored");
    }

    @Override
    public void drain() throws InitializingException {
        workflowsManager.drain(null);
    }

    @Override
    public Bundles getActiveBundles() throws SuspendedException,
                                      InitializingException {
        if (isSuspended()) {
            throw new SuspendedException("The Application is suspended");
        }
        return ticketManager.getIssuedBundles();
    }

    /**
     * Returns an {@link Activity} instance for the specified task name.
     *
     * @param name
     *            the name of the class whose {@link Activity} instance should be
     *            returned.
     *
     * @return the {@link Activity} instance for the specified task name.
     */
    private Activity getActivity(final String name) {
        final Activity activity = getAssembly().getActivity(name);
        final Activity activityToUse;
        if (null == activity) {
            activityToUse = new Activity(name);
        } else {
            activityToUse = activity;
        }
        return activityToUse;
    }

    @Override
    public ActivityManager getActivityManager(final Workflow workflow,
                                              final String name) throws InitializingException {
        return workflowsManager.getActivityManager(workflow,
                                                   name);
    }

    @Override
    public ActivityMonitor getActivityMonitor(final Workflow workflow,
                                              final String name) throws InitializingException {
        return workflowsManager.getActivityMonitor(workflow,
                                                   name);
    }

    @Override
    public List<? extends String> getActivityNames(Workflow workflow) throws InitializingException {
        return workflowsManager.getActivityNames(workflow);
    }

    private File getApplicationDirectory() {
        if (null == applicationDirectory) {
            if (null == configuration) {
                return Configuration.DEFAULT_APPLICATION_DIR;
            } else {
                applicationDirectory = configuration.getDirectory();
            }
        }
        return applicationDirectory;
    }

    @Override
    public List<DigestChange> getArchivedByTime(final Date after,
                                                final Date before,
                                                boolean reversed,
                                                final int max,
                                                final String neighbor,
                                                final List<String> registrations) {
        return bookkeeping.getByTime(Bookkeeping.Stamped.ARCHIVED,
                                     after,
                                     before,
                                     reversed,
                                     max,
                                     neighbor,
                                     registrations);
    }

    private Assembly getAssembly() {
        if (null == assembly) {
            assembly = configuration.getAssembly();
        }
        return assembly;
    }

    @Override
    public Bundles getBlockedBundles() throws SuspendedException,
                                       InitializingException {
        if (isSuspended()) {
            throw new SuspendedException("The Application is suspended");
        }
        return ticketManager.getRetainedBundles();
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }

    @Override
    public List<DigestChange> getConfirmableByBundle(List<String> bundles) {
        return verificationManager.getByBundle(VerificationManager.Stamped.CONFIRMABLE,
                                               bundles);
    }

    @Override
    public List<DigestChange> getConfirmableByTime(Date after,
                                                   Date before,
                                                   boolean reversed,
                                                   int max,
                                                   String neighbor,
                                                   List<String> registrations) {
        return verificationManager.getByTime(VerificationManager.Stamped.CONFIRMABLE,
                                             after,
                                             before,
                                             reversed,
                                             max,
                                             neighbor,
                                             registrations);
    }

    @Override
    public List<DigestChange> getConfirmedByTime(Date after,
                                                 Date before,
                                                 boolean reversed,
                                                 int max,
                                                 String neighbor,
                                                 List<String> registrations) {
        return verificationManager.getByTime(VerificationManager.Stamped.CONFIRMED,
                                             after,
                                             before,
                                             reversed,
                                             max,
                                             neighbor,
                                             registrations);
    }

    @Override
    public AtomicInteger getCount(Workflow workflow) throws InitializingException {
        return workflowsManager.getCount(workflow);
    }

    @Override
    public URI getDefaultBaseUri() {
        final Neighbor selfToUse;
        if (null == neighborhoodManager) {
            selfToUse = null;
        } else {
            selfToUse = neighborhoodManager.getSelf();
        }
        try {
            if (null == selfToUse) {
                if (urlWarning.get()) {
                    LOG.warn("This instance is not defined in the neighborhood, so a default URL assigned to be its Base URL");
                    urlWarning.set(true);
                }
                return new URI("file:///" + getApplicationDirectory().getAbsolutePath());
            }
            final String myApi = selfToUse.getApiUrl();
            if (null == myApi) {
                if (urlWarning.get()) {
                    LOG.warn("This instance is not defined an API URL, so a default URL assigned to be its Base URL");
                    urlWarning.set(true);
                }
                return new URI("file:///" + getApplicationDirectory().getAbsolutePath());
            }
            return new URI(myApi).resolve("../");
        } catch (URISyntaxException e) {
            throw new ConfigurationError(e);
        }
    }

    @Override
    public List<DigestChange> getDispatchedByTime(Date after,
                                                  Date before,
                                                  boolean reversed,
                                                  int max,
                                                  String neighbor,
                                                  List<String> registrations) {
        return bookkeeping.getByTime(Bookkeeping.Stamped.DISPATCHED,
                                     after,
                                     before,
                                     reversed,
                                     max,
                                     neighbor,
                                     registrations);
    }

    @Override
    public Digest getHistoryByTicket(final String ticket) {
        return bookkeeping.getHistoryByTicket(ticket);
    }

    /**
     * Returns this object's inbound {@link FetchingPolicy} instance.
     *
     * @return this object's inbound {@link FetchingPolicy} instance.
     */
    private FetchingPolicy getInboundPolicy() {
        if (null == inboundPolicy) {
            inboundPolicy = Fetcher.getFetchingPolicy(getActivity(Activity.RECEIVER),
                                                      null);
        }
        return inboundPolicy;
    }

    /**
     * Returns this object's local {@link FetchingPolicy} instance.
     *
     * @return this object's local {@link FetchingPolicy} instance.
     */
    private FetchingPolicy getLocalPolicy() {
        if (null == localPolicy) {
            localPolicy = Fetcher.getFetchingPolicy(getActivity(Activity.FETCHER),
                                                    null);
        }
        return localPolicy;
    }

    @Override
    public String getName() {
        return getAssembly().getName();
    }

    @Override
    public List<DigestChange> getPlacedByTime(Date after,
                                              Date before,
                                              boolean reversed,
                                              int max,
                                              String neighbor,
                                              List<String> registrations) {
        return bookkeeping.getByTime(Bookkeeping.Stamped.PLACED,
                                     after,
                                     before,
                                     reversed,
                                     max,
                                     neighbor,
                                     registrations);
    }

    @Override
    public String getPlacementId(final String bundle) {
        return ticketManager.getPlacementId(bundle);
    }

    @Override
    public List<String> getPlacementIds(List<String> bundles) {
        return ticketManager.getPlacementIds(bundles);
    }

    @Override
    public List<Slice> getPlacementSlices(boolean fineBins,
                                          Integer span,
                                          Date after) {
        return bookkeeping.getPlacementSlices(fineBins,
                                              span,
                                              after);
    }

    @Override
    public List<String> getPostponed(long cushion,
                                     TimeUnit unit) {
        final List<String> tickets = retryManager.getPostponed(cushion,
                                                               unit);
        return tickets;
    }

    @Override
    public Registry getRegistrations(List<String> registrations) {
        final Registry result = new Registry(loadedRegistrations.getLocalRegistrations(registrations),
                                             loadedRegistrations.getInboundRegistrations(registrations));
        return result;
    }

    @Override
    public List<Slice> getShippingSlices(boolean fineBins,
                                         Integer span,
                                         Date after) {
        return bookkeeping.getShippingSlices(fineBins,
                                             span,
                                             after);
    }

    @Override
    public List<Slice> getShippingSlices(String neighbor,
                                         boolean fineBins,
                                         Integer span,
                                         Date after) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<DigestChange> getTicketedByTime(Date after,
                                                Date before,
                                                boolean reversed,
                                                int max,
                                                String neighbor,
                                                List<String> registrations,
                                                Boolean inbound) {
        if (null == inbound) {
            return bookkeeping.getByTime(Bookkeeping.Stamped.TICKETED,
                                         after,
                                         before,
                                         reversed,
                                         max,
                                         neighbor,
                                         registrations);
        }
        if (Boolean.TRUE == inbound) {
            return bookkeeping.getByTime(Bookkeeping.Stamped.TICKETED_INBOUND,
                                         after,
                                         before,
                                         reversed,
                                         max,
                                         neighbor,
                                         registrations);
        }
        return bookkeeping.getByTime(Bookkeeping.Stamped.TICKETED_OUTBOUND,
                                     after,
                                     before,
                                     reversed,
                                     max,
                                     neighbor,
                                     registrations);
    }

    @Override
    public List<String> getTicketsByBundle(final String bundle) {
        return ticketManager.getTickets(bundle);
    }

    @Override
    public List<DigestChange> getUnverifiedByTime(Date after,
                                                  Date before,
                                                  boolean reversed,
                                                  int max) {
        return verificationManager.getByTime(VerificationManager.Stamped.UNVERIFIED,
                                             after,
                                             before,
                                             reversed,
                                             max);
    }

    @Override
    public List<DigestChange> getVerifiedByTime(Date after,
                                                Date before,
                                                boolean reversed,
                                                int max,
                                                List<String> registrations) {
        return verificationManager.getByTime(VerificationManager.Stamped.VERIFIED,
                                             after,
                                             before,
                                             reversed,
                                             max,
                                             null,
                                             registrations);
    }

    @Override
    public WorkflowDirector getWorkflowDirector(Workflow workflow) throws InitializingException {
        return workflowsManager.getWorkflowDirector(workflow);
    }

    @Override
    public WorkflowsMonitor getWorkflowsMonitor() throws InitializingException {
        return workflowsManager.getWorkflowsMonitor();
    }

    @Override
    public Bundles inboundScan(final URI baseUri) throws SuspendedException,
                                                  IOException,
                                                  InProgressException,
                                                  InterruptedException,
                                                  TooManyTicketsException,
                                                  InitializingException {
        if (INBOUND_LOCK.tryLock()) {
            try {
                if (isSuspended(Workflow.INGEST)) {
                    throw new SuspendedException("The Workflow \"" + Workflow.INGEST.name()
                                                 + "\" is suspended");
                }
                LOG.info("Starting an inbound Scan");
                final Scanner<InboundRegistration> scanner = new Scanner<>();
                final Bundles foundBundles = scanner.scan(loadedRegistrations.getInboundRegistrations(),
                                                          getInboundPolicy(),
                                                          BUNDLE_ORDERING);
                final Bundles results = ingest(foundBundles,
                                               baseUri);
                LOG.info("Completed an inbound Scan");
                return results;
            } finally {
                INBOUND_LOCK.unlock();
            }
        }
        throw new InProgressException("Inbound Scan request in progress, so new request will be ignored");
    }

    @Override
    public Bundles ingest(final Bundles bundles) throws SuspendedException,
                                                 TooManyTicketsException,
                                                 InitializingException,
                                                 InProgressException,
                                                 InterruptedException {
        return ingest(bundles,
                      null);
    }

    @Override
    public Bundles ingest(final Bundles bundles,
                          final URI baseUri) throws SuspendedException,
                                             TooManyTicketsException,
                                             InitializingException,
                                             InProgressException,
                                             InterruptedException {
        if (INGEST_LOCK.tryLock(DEFAULT_INGEST_TIMOUT,
                                TimeUnit.MINUTES)) {
            try {
                final Date scanStart = bundles.getDateTime();
                if (null != scanStart) {
                    ticketManager.setLatestScan(scanStart);
                }

                if (isSuspended(Workflow.INGEST)) {
                    throw new SuspendedException("The Workflow \"" + Workflow.INGEST.name()
                                                 + "\" is suspended");
                }

                final URI baseUriToUse;
                if (null == baseUri) {
                    baseUriToUse = getDefaultBaseUri();
                } else {
                    baseUriToUse = baseUri;
                }

                final List<Bundle> registered = new ArrayList<>();
                for (Bundle bundle : bundles.getBundles()) {
                    final Bundle registeredBundle = registrationManager.assignRegistration(bundle);
                    if (null != registeredBundle) {
                        registered.add(bundle);
                    }
                }
                final Map<String, Integer> scanLimits = new HashMap<>();
                final List<Bundle> result = new ArrayList<>();
                final Map<String, Integer> used = new HashMap<>();
                final Map<String, Integer> skipped = new HashMap<>();
                final Map<String, Integer> waiting = new HashMap<>();
                final Iterator<Bundle> iterator = registered.iterator();
                final Integer ticketLimit = getAssembly().getTicketLimit();
                int countIssued = ticketManager.getIssuedCount();
                boolean belowLimit = (null == ticketLimit || countIssued < ticketLimit.intValue());
                while (iterator.hasNext() && belowLimit) {
                    final Bundle bundle = iterator.next();
                    final String localId = bundle.getLocalId();
                    final Integer usedCount = used.get(localId);
                    final int usedCountToUse;
                    if (null == usedCount) {
                        usedCountToUse = 0;
                    } else {
                        usedCountToUse = usedCount.intValue();
                    }

                    /*
                     * Find scan limit for this registration, filling cache if not already in it.
                     */
                    final Integer scanLimit = scanLimits.get(localId);
                    final int scanLimitToUse;
                    if (null == scanLimit) {
                        final Registration registration = registrationManager.getRegistration(localId);
                        final Integer limit = registration.getScanLimit();
                        if (null == limit) {
                            scanLimitToUse = DEFAULT_SCAN_LIMIT;
                        } else {
                            scanLimitToUse = limit;
                        }
                        scanLimits.put(localId,
                                       scanLimitToUse);
                    } else {
                        scanLimitToUse = scanLimit;
                    }

                    /*
                     * Issue ticket if scan limit for registration has not been reached.
                     */
                    if (scanLimitToUse > usedCountToUse) {
                        try {
                            IngestTicket ticket = ticketManager.issue(bundle);
                            if (null == ticket) {
                                final Integer skipCount = skipped.get(localId);
                                if (null == skipCount) {
                                    skipped.put(localId,
                                                1);
                                } else {
                                    skipped.put(localId,
                                                skipCount.intValue() + 1);
                                }
                            } else {
                                ++countIssued;
                                ticket.setBaseUri(baseUriToUse);
                                final HandlingPriority priorityToUse;
                                final Options options = ticket.getOptions();
                                if (null == options) {
                                    priorityToUse = HandlingPriority.NORMAL;
                                } else {
                                    final HandlingPriority priority = options.getPriority();
                                    if (null == priority) {
                                        priorityToUse = HandlingPriority.NORMAL;
                                    } else {
                                        priorityToUse = priority;
                                    }
                                }
                                if (workflowsManager.startWorkflow(Workflow.INGEST,
                                                                   (ticket.getBundle()).getName(),
                                                                   priorityToUse.ordinal(),
                                                                   ticket)) {
                                    result.add(bundle);
                                    used.put(localId,
                                             usedCountToUse + 1);
                                }
                            }
                            belowLimit = (null == ticketLimit || countIssued < ticketLimit.intValue());
                        } catch (TooManyTicketsException e) {
                            belowLimit = false;
                        }
                    } else {
                        final Integer waitCount = waiting.get(localId);
                        if (null == waitCount) {
                            waiting.put(localId,
                                        1);
                        } else {
                            waiting.put(localId,
                                        waitCount.intValue() + 1);
                        }
                    }
                }

                /**
                 * Can not use Set returned from collection as is does not support the addAll
                 * method, so need to create a copy.
                 */
                final Set<String> localIds = new TreeSet<>(used.keySet());
                localIds.addAll(skipped.keySet());
                for (String localId : localIds) {
                    final Integer usedCount = used.get(localId);
                    if (null != usedCount) {
                        final String usedPlural = createPlural(usedCount);
                        LOG.info("Ingested " + usedCount
                                 + " semaphore file"
                                 + usedPlural
                                 + " for registration \""
                                 + localId
                                 + "\"");
                    }

                    final Integer waitCount = waiting.get(localId);
                    if (null != waitCount) {
                        String waitPlural = createPlural(waitCount);
                        LOG.info("Registration \"" + localId
                                 + "\" as surpassed its limit leaving "
                                 + waitCount
                                 + " semaphore file"
                                 + waitPlural
                                 + " to wait for the next time");
                    }

                    final Integer skipCount = skipped.get(localId);
                    if (null != skipCount) {
                        final String skipPlural = createPlural(skipCount);
                        LOG.info("Skipped " + skipCount
                                 + " semaphore file"
                                 + skipPlural
                                 + " for registration \""
                                 + localId
                                 + "\" that are already being fetched");
                    }
                }
                if (!belowLimit) {
                    final String plural;
                    if (1 == countIssued) {
                        plural = "";
                    } else {
                        plural = "s";
                    }
                    final String limit;
                    if (null == ticketLimit) {
                        limit = "undefined";
                    } else {
                        limit = Integer.toString(ticketLimit);
                    }
                    LOG.warn(Integer.toString(countIssued) + " ticket"
                             + plural
                             + " are current issued and the limit is "
                             + limit
                             + ", so nothing more will be ingested at the moment");
                }
                return new Bundles(null,
                                   result);
            } finally {
                INGEST_LOCK.unlock();
            }
        }
        throw new InProgressException("Could not get the ingestion lock so will ignore this request");
    }

    @Override
    public boolean isSuspended() throws InitializingException {
        return workflowsManager.isSuspended(null);
    }

    @Override
    public boolean isSuspended(final Workflow workflow) throws InitializingException {
        return workflowsManager.isSuspended(workflow);
    }

    @Override
    public Bundles localScan(final URI baseUri) throws SuspendedException,
                                                IOException,
                                                InProgressException,
                                                InterruptedException,
                                                TooManyTicketsException,
                                                InitializingException {
        if (LOCAL_LOCK.tryLock()) {
            try {
                if (isSuspended(Workflow.INGEST)) {
                    throw new SuspendedException("The Workflow \"" + Workflow.INGEST.name()
                                                 + "\" is suspended");
                }
                LOG.info("Starting a local scan");
                final Scanner<LocalRegistration> scanner = new Scanner<>();
                final Bundles foundBundles = scanner.scan(loadedRegistrations.getLocalRegistrations(),
                                                          getLocalPolicy(),
                                                          BUNDLE_ORDERING);
                final Bundles results = ingest(foundBundles,
                                               baseUri);
                LOG.info("Completed a local scan");
                return results;
            } finally {
                LOCAL_LOCK.unlock();
            }
        }
        throw new InProgressException("Local Scan request in progress, so new request will be ignored");
    }

    @Override
    public Collection<String> rearchive(final Collection<String> tickets,
                                        final URI baseUri) throws SuspendedException,
                                                           InitializingException {
        if (isSuspended(Workflow.REARCHIVE)) {
            throw new SuspendedException("The Workflow \"" + Workflow.REARCHIVE.name()
                                         + "\" is suspended");
        }

        final List<IngestTicket> validTickets = new ArrayList<>();
        for (String ticket : tickets) {
            final IngestTicket validTicket = spoolManager.getSpooledTicket(ticket);
            if (null != validTicket) {
                validTickets.add(validTicket);
            }
        }

        return redo(validTickets,
                    Workflow.REARCHIVE,
                    baseUri);
    }

    @Override
    public void recommence(final URI uri,
                           final Object message) {
        workflowsManager.recommence(uri,
                                    message);
    }

    @Override
    public Collection<String> redispatch(final Collection<String> tickets,
                                         final URI baseUri) throws SuspendedException,
                                                            InitializingException {
        if (isSuspended(Workflow.RESHIP)) {
            throw new SuspendedException("The Workflow \"" + Workflow.RESHIP.name()
                                         + "\" is suspended");
        }

        final List<IngestTicket> validTickets = new ArrayList<>();
        for (String ticket : tickets) {
            final IngestTicket validTicket = retryManager.getDispatchFailedTicket(ticket);
            if (null != validTicket) {
                validTickets.add(validTicket);
            }
        }

        return redo(validTickets,
                    Workflow.RESHIP,
                    baseUri);
    }

    @Override
    public Collection<String> redispatchScan(final long cushion,
                                             final TimeUnit unit,
                                             final URI baseUri) throws SuspendedException,
                                                                InitializingException,
                                                                InProgressException {
        if (REDISPATCH_LOCK.tryLock()) {
            try {
                if (isSuspended(Workflow.RESHIP)) {
                    throw new SuspendedException("The Workflow \"" + Workflow.RESHIP.name()
                                                 + "\" is suspended");
                }
                LOG.info("Starting a re-dispatch scan");
                final List<String> tickets = getPostponed(cushion,
                                                          unit);

                /* Calculate limit of number of tickets to use */
                final Integer ticketLimit = getAssembly().getTicketLimit();
                final int upperLimit;
                if (null == ticketLimit || 0 >= ticketLimit.intValue()) {
                    upperLimit = tickets.size();
                } else {
                    final int issuedLimit = ticketLimit.intValue();
                    final int issuedCount = ticketManager.getIssuedCount();

                    final double fractionalLimit = issuedLimit * REDISPATCH_LIMIT_FRACTION;
                    if (issuedCount > fractionalLimit) {
                        upperLimit = 0;
                    } else {
                        final int maxUpperLimit = (int) (fractionalLimit - issuedCount);
                        if (tickets.size() > maxUpperLimit) {
                            upperLimit = maxUpperLimit;
                        } else {
                            upperLimit = tickets.size();
                        }
                    }
                }
                final List<String> ticketsToUse = tickets.subList(0,
                                                                  upperLimit);
                final Collection<String> result;
                if (null == ticketsToUse || ticketsToUse.isEmpty()) {
                    result = ticketsToUse;
                } else {
                    result = redispatch(ticketsToUse,
                                        baseUri);
                }
                LOG.info("Completed a re-dispatch scan");
                return result;
            } finally {
                REDISPATCH_LOCK.unlock();
            }
        }
        throw new InProgressException("Redispatch Scan request in progress, so new request will be ignored");
    }

    /**
     * Attempts to redo a give task or set of tasks as defined by the supplied BPMN
     * file.
     *
     * @param validTickets
     *            the {@link Collection} of {@link IngestTicket} instances whose
     *            task or set of tasks should be redone.
     * @param workflow
     *            TODO
     * @param baseUri
     *            The base {@link URI} used to request to request this redo, if
     *            <code>null</code> then the default base {@link URI} is used.
     *
     * @return the {@link Collection} of tickets whose task or set of tasks have
     *         been attempted.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws InitializingException
     *             TODO
     */
    private Collection<String> redo(final List<IngestTicket> validTickets,
                                    final Workflow workflow,
                                    final URI baseUri) throws SuspendedException,
                                                       InitializingException {
        final URI baseUriToUse;
        if (null == baseUri) {
            baseUriToUse = getDefaultBaseUri();
        } else {
            baseUriToUse = baseUri;
        }

        final List<String> result = new ArrayList<>();
        final Map<String, Integer> used = new HashMap<>();
        final Map<String, Integer> skipped = new HashMap<>();
        for (IngestTicket validTicket : validTickets) {
            final Bundle bundle = validTicket.getBundle();
            final String localId = bundle.getLocalId();
            if (!ticketManager.reissue(validTicket)) {
                final Integer skipCount = skipped.get(localId);
                if (null == skipCount) {
                    skipped.put(localId,
                                1);
                } else {
                    skipped.put(localId,
                                skipCount.intValue() + 1);
                }
            } else {
                validTicket.setBaseUri(baseUriToUse);
                final HandlingPriority priority = (validTicket.getOptions()).getPriority();
                final HandlingPriority priorityToUse;
                if (null == priority) {
                    priorityToUse = HandlingPriority.NORMAL;
                } else {
                    priorityToUse = priority;
                }
                if (workflowsManager.startWorkflow(workflow,
                                                   bundle.getName(),
                                                   priorityToUse.ordinal(),
                                                   validTicket)) {
                    final Integer usedCount = used.get(localId);
                    final int usedCountToUse;
                    if (null == usedCount) {
                        usedCountToUse = 0;
                    } else {
                        usedCountToUse = usedCount.intValue();
                    }
                    result.add(validTicket.getIdentity());
                    used.put(localId,
                             usedCountToUse + 1);
                }
            }
        }

        /**
         * Can not used Set returned from collection and is does not support the addAll
         * method.
         */
        final Set<String> localIds = new TreeSet<>(used.keySet());
        localIds.addAll(skipped.keySet());
        for (String localId : localIds) {
            final Integer usedCount = used.get(localId);
            if (null != usedCount) {
                final String usedPlural = createPlural(usedCount);
                LOG.info("Reissued " + usedCount
                         + " ticket"
                         + usedPlural
                         + " with registration \""
                         + localId
                         + "\"");
            }

            final Integer skipCount = skipped.get(localId);
            if (null != skipCount) {
                final String skipPlural = createPlural(skipCount);
                LOG.info("Skipped reissue of " + skipCount
                         + " ticket"
                         + skipPlural
                         + " with registration \""
                         + localId
                         + "\" as they are already issued");
            }
        }
        return result;
    }

    @Override
    public Collection<Bundle> rescueScan(Workflow workflow) throws InitializingException,
                                                            SuspendedException,
                                                            InProgressException {
        if (LOCAL_LOCK.tryLock()) {
            try {
                if (isSuspended()) {
                    throw new SuspendedException("The Application is suspended");
                }
                LOG.info("Starting a rescue scan");
                final Collection<String> labels = workflowsManager.rescueWorkflows(workflow);
                final Collection<Bundle> results = new ArrayList<>();
                for (String label : labels) {
                    results.add(new Bundle(label));
                }
                LOG.info("Completed a rescue scan");
                return results;
            } finally {
                LOCAL_LOCK.unlock();
            }
        }
        throw new InProgressException("Rescue Scan request in progress, so new request will be ignored");
    }

    @Override
    public Collection<String> reship(final Collection<String> tickets,
                                     final Boolean all,
                                     final URI baseUri) throws SuspendedException,
                                                        InitializingException {
        if (isSuspended(Workflow.RESHIP)) {
            throw new SuspendedException("The Workflow \"" + Workflow.RESHIP.name()
                                         + "\" is suspended");
        }

        final List<IngestTicket> validTickets = new ArrayList<>();
        for (String ticket : tickets) {
            if (null != all && all.booleanValue()) {
                final IngestTicket validTicket = retryManager.getAwaitingVerificationTicket(ticket);
                if (null != validTicket) {
                    validTickets.add(validTicket);
                }
            } else {
                final IngestTicket validTicket = retryManager.getAwaitingVerificationTicket(ticket);
                if (null != validTicket) {
                    validTickets.add(validTicket);
                }
            }
        }

        return redo(validTickets,
                    Workflow.RESHIP,
                    baseUri);
    }

    @Override
    public boolean setSuspended(final boolean suspended) throws InitializingException {
        final boolean initial = isSuspended();
        boolean result = workflowsManager.setSuspended(null,
                                                       suspended);

        if (suspended != isSuspended()) {
            throw new RuntimeException("Suspension state did not successful change");
        }
        if (suspended != initial) {
            if (suspended) {
                LOG.info("Application has been suspended");
            } else {
                LOG.info("Application has been resumed");
            }
        }
        return result;
    }

    // static member methods (alphabetic)

    @Override
    public Bundles unblock(final Bundles bundles) throws SuspendedException,
                                                  InitializingException {
        if (isSuspended()) {
            throw new SuspendedException("The Application is suspended");
        }
        final List<Bundle> result = new ArrayList<>();
        if (null != bundles) {
            for (Bundle bundle : bundles.getBundles()) {
                if (ticketManager.unblock(bundle)) {
                    result.add(bundle);
                }
            }
        }
        return new Bundles(null,
                           result);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
