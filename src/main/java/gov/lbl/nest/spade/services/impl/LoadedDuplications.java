package gov.lbl.nest.spade.services.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.config.Duplication;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.inject.Inject;

/**
 * This class contains all of the duplications loaded into the application.
 *
 * @author patton
 */
@Singleton
/** The class does it own concurrency management, so can use READ lock. */
@jakarta.ejb.Lock(LockType.READ)
public class LoadedDuplications {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance used by this class.
     */
    @Inject
    private Configuration configuration;

    /**
     * The mapping of {@link Duplication} instances to their names.
     */
    private final Map<String, Duplication> duplicationsByName = new HashMap<>();

    /**
     * True if the registrations have been loaded.
     */
    private boolean loaded = false;

    /**
     * The {@link Lock} instance used to make sure that the registration loading is
     * serialized.
     */
    private Lock lock = new ReentrantLock();

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the {@link Duplication} with the specified name or <code>null</code>.
     *
     * @param name
     *            the name to match.
     *
     * @return the {@link Duplication} instance with the specified name or
     *         <code>null</code>.
     */
    public Duplication getDuplication(String name) {
        loadDuplications();
        if (".default.".equals(name) && 1 == duplicationsByName.size()) {
            return ((duplicationsByName.values()).iterator()).next();
        }
        return duplicationsByName.get(name);
    }

    /**
     * Loads the known neighbors into this object.
     */
    private void loadDuplications() {
        if (!loaded) {
            lock.lock();
            try {
                if (!loaded) {
                    final List<Duplication> duplications = configuration.getDuplications();
                    if (null != duplications) {
                        for (Duplication duplication : duplications) {
                            duplicationsByName.put(duplication.getName(),
                                                   duplication);
                        }
                    }
                    loaded = true;
                }
            } finally {
                lock.unlock();
            }
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
