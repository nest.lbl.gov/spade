package gov.lbl.nest.spade.services;

import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.rs.Bundle;

/**
 * This interface is used to manage and match the Registration instances known
 * by this application.
 *
 * @author patton
 */
public interface RegistrationManager {

    /**
     * Returns the supplied {@link Bundle} instance with its registration property
     * set. If that property is already set then this method does nothing but return
     * the supplied Bundle. If no {@link Registration} matches the supplied Bundle
     * then <code>null</code> is returned.
     *
     * @param bundle
     *            the {@link Bundle} instance whose registration should be found.
     *
     * @return the supplied {@link Bundle} instance with its registration property
     *         set, or <code>null</code> if no {@link Registration} matches it.
     */
    Bundle assignRegistration(final Bundle bundle);

    /**
     * Returns the {@link Registration} instance, if any, matching the supplied
     * localId, otherwise returns <code>null</code>.
     *
     * @param localId
     *            the local registration id to match.
     *
     * @return the {@link Registration} instance, if any, matching the supplied
     *         localId, otherwise returns <code>null</code>.
     */
    Registration getRegistration(final String localId);
}
