package gov.lbl.nest.spade.services;

import java.lang.reflect.Constructor;
import java.net.URI;
import java.util.ServiceLoader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.workflow.ActivityDefermentKey;

/**
 * This class is used by {@link Spade} activities to generate callback URIs.
 *
 * @author patton
 */
public abstract class UriHandler {

    // public static final member data

    /**
     * The {@link UriHandler} implementation that does nothing.
     */
    public final static UriHandler DEFAULT_URI_HANDLER = new UriHandler() {

        @Override
        public URI getCallback(final URI baseUri,
                               final ActivityDefermentKey key) {
            if (null == baseUri) {
                return null;
            }
            return baseUri.resolve("command/callback/" + key.forURI());
        }
    };

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(UriHandler.class);

    /**
     * The {@link ServiceLoader} instance that will load the {@link UriHandler}
     * implementation.
     */
    private final static ServiceLoader<UriHandler> URI_HANDLER_LOADER = ServiceLoader.load(UriHandler.class,
                                                                                           UriHandler.class.getClassLoader());

    // private static member data

    /**
     * true if the implementation class of Instrumentation has been logged.
     */
    private static boolean logged = false;

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Creates an instance of the {@link UriHandler} class.
     *
     * @return the created instance of the {@link UriHandler} class.
     */
    public static UriHandler getUriHandler() {
        for (UriHandler instrumentation : URI_HANDLER_LOADER) {
            final Class<?> clazz = instrumentation.getClass();
            try {
                final Constructor<?> constructor = clazz.getConstructor();
                UriHandler result = (UriHandler) constructor.newInstance();
                if (!logged) {
                    LOG.info("Using \"" + clazz.getCanonicalName()
                             + "\" as UriHandler implementation");
                    logged = true;
                }
                return result;
            } catch (Exception e) {
                return null;
            }
        }
        if (!logged) {
            LOG.info("Using default, " + UriHandler.class.getName()
                     + " as UriHandler implementation");
            logged = true;
        }
        return DEFAULT_URI_HANDLER;
    }

    // static member methods (alphabetic)

    /**
     * Creates a suitable callback {@link URI} based on the supplied arguments.
     *
     * @param baseUri
     *            the base URI of the application.
     * @param key
     *            the {@link ActivityDefermentKey} from which to construction the
     *            callback {@link URI}.
     *
     * @return the created callback {@link URI} based on the supplied arguments.
     */
    public abstract URI getCallback(final URI baseUri,
                                    final ActivityDefermentKey key);

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
