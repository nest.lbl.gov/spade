package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.registry.ArchiveOptions;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.SpoolManager;
import jakarta.annotation.Priority;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.xml.bind.JAXBException;

/**
 * This class is the default implementation of the {@link SpoolManager}
 * interface.
 *
 * @author patton
 */
@Priority(value = 0)
@Stateless
public class SpoolManagerImpl implements
                              SpoolManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The name to use for the filename of any archive ticket.
     */
    private static final String ARCHIVE_TICKET_FILENAME = "ticket.xml";

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance used by this object.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link CacheManager} instance used by this object.
     */
    @Inject
    private CacheManager cacheManager;

    /**
     * The directory that contains files that need to be re-archived.
     */
    private File spoolDir;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the directory that contains files that need to be re-archived.
     *
     * @return the directory that contains files that need to be re-archived.
     */
    private File getSpoolDir() {
        if (null == spoolDir) {
            spoolDir = ((configuration.getCacheDefinition()).getCacheMap()).get(CacheDefinition.SPOOL);
        }
        return spoolDir;
    }

    @Override
    public IngestTicket getSpooledTicket(String ticket) {
        return IngestTicket.read(new File(new File(getSpoolDir(),
                                                   ticket),
                                          ARCHIVE_TICKET_FILENAME));
    }

    @Override
    public boolean isSpoolDirectory(final File directory) {
        return (null != directory && getSpoolDir().equals(directory.getParentFile()));
    }

    @Override
    public void placeInSpool(final IngestTicket ticket,
                             final String name,
                             final int storageLoad,
                             final File archiveFile,
                             final File archiveMetadata) throws TimeoutException,
                                                         InterruptedException,
                                                         IOException {
        final String identity = ticket.getIdentity();
        final File directory = new File(getSpoolDir(),
                                        identity);
        directory.mkdirs();
        cacheManager.waitForSpace(directory,
                                  storageLoad,
                                  name);
        final File spoolMetadata;
        if (null != archiveMetadata) {
            spoolMetadata = new File(directory,
                                     archiveMetadata.getName());
            if (!cacheManager.copy(archiveMetadata,
                                   spoolMetadata)) {
                throw new IOException("Failured to copy " + archiveMetadata.getPath()
                                      + " into spool directory \""
                                      + directory
                                      + "\"",
                                      null);
            }
        } else {
            throw new IllegalArgumentException("No metafile was specified to be spooled.");
        }
        final File spoolFile = new File(directory,
                                        archiveFile.getName());
        if (cacheManager.copy(archiveFile,
                              spoolFile)) {
            final IngestTicket archiveTicket = new IngestTicket(identity,
                                                                ticket.getBundle(),
                                                                spoolMetadata,
                                                                ticket.getArchiveCategory(),
                                                                spoolFile,
                                                                new ArchiveOptions(ticket.getOptions()));
            try {
                archiveTicket.write(new File(directory,
                                             ARCHIVE_TICKET_FILENAME));
            } catch (JAXBException e) {
                throw new IOException("Failed to write archive ticket",
                                      e);
            }
        } else {
            throw new IOException("Failured to copy " + archiveFile.getPath()
                                  + " into spool directory \""
                                  + directory
                                  + "\"");
        }
    }

    @Override
    public void removeFromSpool(final IngestTicket ticket) {
        LocalhostTransfer.delete(new File(getSpoolDir(),
                                          ticket.getIdentity()));
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
