package gov.lbl.nest.spade.services;

import jakarta.xml.bind.annotation.XmlEnum;

/**
 * This interface is used to send out notifications from this application.
 *
 * @author patton
 */
public interface NotificationManager {

    /**
     * This enumerates the possible priorities for handling bundles.
     *
     * @author patton
     */
    @XmlEnum(String.class)
    static public enum Scope {
                              /**
                               * Receive all notifications
                               */
                              ALL,

                              /**
                               * Receive notifications not dealt with any other scope.
                               */
                              MISCELLANEOUS,

                              /**
                               * Receive notifications related to starting an instance.
                               */
                              STARTING,

                              /**
                               * Receive notifications related to credentials an instance.
                               */
                              CREDENTIALS,
    }

    /**
     * Posts the supplied Notification.
     *
     * @param scope
     *            the {@link Scope} instance defining how the notification should be
     *            handled.
     *
     * @param notification
     *            the {@link String} instance containing the message to be posted as
     *            part of the notification.
     */
    void postNotice(Scope scope,
                    String notification);

}
