package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.watching.Detail;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.ejb.Confirmation;
import gov.lbl.nest.spade.ejb.KnownNeighbor;
import gov.lbl.nest.spade.ejb.KnownRegistration;
import gov.lbl.nest.spade.ejb.ShippedFile;
import gov.lbl.nest.spade.ejb.TicketedFile;
import gov.lbl.nest.spade.interfaces.SpadeDB;
import gov.lbl.nest.spade.interfaces.metadata.ChecksumElement;
import gov.lbl.nest.spade.interfaces.metadata.ChecksumFactory;
import gov.lbl.nest.spade.interfaces.metadata.ChecksumMetadata;
import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import gov.lbl.nest.spade.registry.internal.InternalFileName;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import gov.lbl.nest.spade.services.RetryManager;
import gov.lbl.nest.spade.services.VerificationFailure;
import gov.lbl.nest.spade.services.VerificationManager;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.TypedQuery;
import jakarta.xml.bind.DatatypeConverter;

/**
 * This class is the default implementation of the {@link VerificationManager}
 * interface.
 *
 * <b>Note:</b> This class uses the {@link ConfirmationModifier} class to handle
 * modification to the {@link Confirmation} instances to avoid a deadlock that
 * can happen when both {@link #prepare(String, String, File, File, Collection)}
 * and {@link #confirm(String, List)} are called in parallel.
 *
 * @author patton
 */
@Stateless
public class VerificationManagerImpl implements
                                     VerificationManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(VerificationManagerImpl.class);

    /**
     * {@link List} to return if <code>null</code> is returned as the list of
     * {@link ShippedFile} or {@link Confirmation}instances.
     */
    private final static List<DigestChange> NO_SHIPPED_FILES = new ArrayList<>(0);

    /**
     * The default checksum algorithm to use if none is provided.
     */
    private final static String DEFAULT_ALGORITHM = ChecksumFactory.CRC32_TYPE;

    /**
     * The name used in the digest to identify the remote ticket to be verified.
     */
    final public static String TICKET_NAME = "ticket";

    /**
     * The name used in the digest to identify the local checksum of the ticket to
     * be verified.
     */
    final public static String CHECKSUM_NAME = "checksum";

    /**
     * The name used in the digest to identify the dispatch time of the ticket that
     * was verified.
     */
    final public static String WHEN_DISPATCHED = "when_dispatched";

    // private static member data

    /**
     * The format for date queries.
     */
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * The formatter for date supplied to this class as Strings.
     */
    private static final DateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    /**
     * The maximum allowed year for {@link DatatypeConverter} result (to avoid DB
     * lookup issues).
     */
    private static final Date MAXIMUM_DATE_TIME;

    static {
        Date max = null;
        try {
            max = DATE_FORMATTER.parse("10000-01-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MAXIMUM_DATE_TIME = max;
    }

    // private instance member data

    /**
     * The {@link CacheManager} instance used by this object.
     */
    @Inject
    private CacheManager cacheManager;

    /**
     * The {@link ConfirmationModifier} instance used by this object.
     */
    @Inject
    private ConfirmationModifier confirmationModifier;

    // private static member data

    // private instance member data

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @SpadeDB
    private EntityManager entityManager;

    /**
     * The {@link MetadataManager} instance used by this object.
     */
    @Inject
    private MetadataManager metadataManager;

    /**
     * The {@link LoadedRegistrations} instance used by this object.
     */
    @Inject
    private LoadedRegistrations loadedRegistrations;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected VerificationManagerImpl() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     * @param loadedRegistrations
     *            the {@link LoadedRegistrations} instance used by this object.
     * @param metadataManager
     *            the {@link MetadataManager} instance used by this object.
     * @param neighborhoodManager
     *            the {@link NeighborhoodManager} instance used by this object.
     * @param retryManager
     *            the {@link RetryManager} instance used by this object.
     * @param entityManager
     *            the {@link EntityManager} instance used by the created object.
     */
    public VerificationManagerImpl(final CacheManager cacheManager,
                                   final LoadedRegistrations loadedRegistrations,
                                   final MetadataManager metadataManager,
                                   final NeighborhoodManager neighborhoodManager,
                                   final RetryManager retryManager,
                                   final EntityManager entityManager) {
        this.cacheManager = cacheManager;
        this.confirmationModifier = new ConfirmationModifier(neighborhoodManager,
                                                             retryManager,
                                                             entityManager);
        this.entityManager = entityManager;
        this.loadedRegistrations = loadedRegistrations;
        this.metadataManager = metadataManager;
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param entityManager
     *            the {@link EntityManager} instance used by the created object.
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     * @param metadataManager
     *            the {@link MetadataManager} instance used by this object.
     * @param neighborhoodManager
     *            the {@link NeighborhoodManager} instance used by this object.
     * @param retryManager
     *            the {@link RetryManager} instance used by this object.
     */
    public VerificationManagerImpl(EntityManager entityManager,
                                   CacheManager cacheManager,
                                   MetadataManager metadataManager,
                                   NeighborhoodManager neighborhoodManager,
                                   RetryManager retryManager) {
        this.cacheManager = cacheManager;
        this.confirmationModifier = new ConfirmationModifier(entityManager,
                                                             neighborhoodManager,
                                                             retryManager);
        this.entityManager = entityManager;
        this.metadataManager = metadataManager;
    }

    // instance member method (alphabetic)

    @Override
    public List<? extends DigestChange> confirm(final String neighbor,
                                                final List<? extends DigestChange> confirmables) {
        final List<DigestChange> confirmed = new ArrayList<>();
        for (DigestChange confirmable : confirmables) {
            final List<Detail> details = confirmable.getDetails();
            String ticket = null;
            Long checksum = null;
            final Iterator<Detail> iterator = details.iterator();
            while (iterator.hasNext() && (null == ticket || null == checksum)) {
                final Detail detail = iterator.next();
                if (TICKET_NAME.equals(detail.getName())) {
                    ticket = (String) detail.getObject();
                } else if (CHECKSUM_NAME.equals(detail.getName())) {
                    checksum = (Long) detail.getObject();
                }
            }
            if (null == ticket) {
                LOG.warn("No ticket supplied by the other SPADE instance for \"" + confirmable.getItem()
                         + "\" so can not be confirmed");
            } else if (null == checksum) {
                LOG.warn("No checksum supplied by the other SPADE instance for ticket \"" + ticket
                         + "\" so can not be confirmed");
            } else {
                final ShippedFile shippedFile = entityManager.find(ShippedFile.class,
                                                                   Integer.parseInt(ticket));
                if (null == shippedFile) {
                    LOG.warn("No such ticket as \"" + ticket
                             + "\" for \""
                             + confirmable.getItem()
                             + "\" so can not be confirmed");
                } else if (null == shippedFile.getWhenAbandoned()) {
                    final Long ticketedChecksum = shippedFile.getChecksum();
                    if (null == ticketedChecksum) {
                        LOG.warn("Checksum for this other SPADE instance for ticket \"" + ticket
                                 + "\" was never saved");
                    } else if (checksum.equals(ticketedChecksum)) {
                        if (null == shippedFile.getWhenVerificationCompleted()) {
                            try {
                                final Date time = confirmationModifier.confirmNeighborDelivery(neighbor,
                                                                                               ticket);
                                confirmed.add(new DigestChange(ticket,
                                                               time));
                            } catch (IllegalArgumentException e) {
                                throw new VerificationFailure(null,
                                                              e,
                                                              null);
                            }
                        } else {
                            final Date time = confirmationModifier.reconfirmNeighborDelivery(neighbor,
                                                                                             ticket);
                            confirmed.add(new DigestChange(ticket,
                                                           time));
                        }
                    } else {
                        LOG.warn("Checksum mismatch for ticket \"" + ticket
                                 + "\", expected "
                                 + ticketedChecksum
                                 + " but received "
                                 + checksum);
                    }
                }
            }
        }
        return confirmed;
    }

    @Override
    public void confirmable(final String ticket) {
        confirmationModifier.confirmable(ticket);
    }

    @Override
    public void confirmable(final String ticket,
                            final File metadataFile,
                            final File transferFile) throws IOException,
                                                     MetadataParseException {
        final Metadata metadata = metadataManager.createMetadata(metadataFile);
        String algorithm;
        if (metadata instanceof ChecksumMetadata) {
            final ChecksumElement checksumElement = ((ChecksumMetadata) metadata).getChecksum();
            if (null == checksumElement) {
                algorithm = DEFAULT_ALGORITHM;
            } else {
                algorithm = checksumElement.getAlgorithm();
            }
        } else {
            algorithm = DEFAULT_ALGORITHM;
        }
        confirmationModifier.confirmable(ticket,
                                         new ChecksumElement(ChecksumFactory.calculateValue(transferFile,
                                                                                            algorithm),
                                                             algorithm));
    }

    // instance member method (alphabetic)

    private List<DigestChange> createDigest(Stamped stamped,
                                            final List<ShippedFile> shippedFiles,
                                            Date after,
                                            Date before,
                                            KnownNeighbor knownNeighbor) {
        final List<DigestChange> result = new ArrayList<>(shippedFiles.size());
        for (ShippedFile shippedFile : shippedFiles) {
            // TODO: Clean up call so Key is not exposed!
            final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                                 shippedFile.getTicketedFileKey());
            final Date when;
            final List<Detail> details;
            if (Stamped.CONFIRMABLE == stamped) {
                when = shippedFile.getWhenConfirmable();
                details = new ArrayList<>();
                details.add(new Detail(TICKET_NAME,
                                       shippedFile.getRemoteTicket()));
                details.add(new Detail(CHECKSUM_NAME,
                                       shippedFile.getChecksum()));
            } else {
                final Date whenVerificationStarted = shippedFile.getWhenVerificationStarted();
                if (Stamped.UNVERIFIED == stamped) {
                    when = whenVerificationStarted;
                    details = null;
                } else if (Stamped.VERIFIED == stamped) {
                    details = new ArrayList<>();
                    if (null == knownNeighbor) {
                        when = shippedFile.getWhenVerificationCompleted();
                        if (null == whenVerificationStarted) {
                            LOG.warn("Ticket #" + shippedFile.getTicketIdentity()
                                     + " does not have and date/time set for when verification started");
                        } else {
                            /**
                             * A new Date object is created as the sql.Timestamp object is not handled by
                             * default by JAXB.
                             */
                            details.add(new Detail(WHEN_DISPATCHED,
                                                   new Date(whenVerificationStarted.getTime())));
                        }
                    } else {
                        Confirmation last = shippedFile.getLastConfirmed(knownNeighbor,
                                                                         after,
                                                                         before);
                        when = last.getWhenConfirmed();
                        /**
                         * A new Date object is created as the sql.Timestamp object is not handled by
                         * default by JAXB.
                         */
                        details.add(new Detail(WHEN_DISPATCHED,
                                               new Date((last.getWhenDelivered()).getTime())));
                    }
                } else {
                    // Should never get here is all enumerations as dealt with.o
                    throw new IllegalArgumentException();
                }
            }
            result.add(new DigestChange(ticketedFile.getBundle(),
                                        when,
                                        details));
        }
        return result;
    }

    @Override
    public void delivered(final String ticket,
                          final String neighbor) {
        confirmationModifier.delivered(ticket,
                                       neighbor);
    }

    @Override
    public List<DigestChange> getByBundle(Stamped stamped,
                                          List<String> bundles) {
        if (Stamped.CONFIRMABLE != stamped) {
            throw new UnsupportedOperationException("Only CONFIRMABLE is currently supported");
        }
        if (null == bundles || bundles.isEmpty()) {
            return NO_SHIPPED_FILES;
        }
        final String category = "confirmable";
        final TypedQuery<ShippedFile> query = entityManager.createNamedQuery(category + "ByBundles",
                                                                             ShippedFile.class);
        query.setParameter("bundles",
                           bundles);
        final List<ShippedFile> shippedFiles = query.getResultList();
        if (null == shippedFiles || shippedFiles.isEmpty()) {
            return NO_SHIPPED_FILES;
        }
        final List<DigestChange> result = new ArrayList<>(bundles.size());
        for (ShippedFile shippedFile : shippedFiles) {
            final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                                 shippedFile.getTicketedFileKey());
            result.add(new DigestChange(ticketedFile.getBundle(),
                                        shippedFile.getWhenConfirmable()));
        }
        return result;
    }

    @Override
    public List<DigestChange> getByTime(final Stamped stamped,
                                        final Date after,
                                        final Date before,
                                        boolean reversed,
                                        final int max) {
        return getByTime(stamped,
                         after,
                         before,
                         reversed,
                         max,
                         null,
                         null);
    }

    @Override
    public List<DigestChange> getByTime(final Stamped stamped,
                                        final Date after,
                                        final Date before,
                                        boolean reversed,
                                        final int max,
                                        final String neighbor,
                                        final List<String> registrations) {
        final KnownNeighbor knownNeighbor;
        final Stamped stampedToUse;
        if (null == neighbor || Stamped.CONFIRMED == stamped) {
            knownNeighbor = null;
            if (Stamped.CONFIRMED == stamped) {
                stampedToUse = Stamped.VERIFIED;
            } else {
                stampedToUse = stamped;
            }
        } else {
            stampedToUse = stamped;
            final TypedQuery<KnownNeighbor> query2 = entityManager.createNamedQuery("getNeighborByName",
                                                                                    KnownNeighbor.class);
            query2.setParameter("name",
                                neighbor);
            try {
                knownNeighbor = query2.getSingleResult();
            } catch (NoResultException e) {
                LOG.warn("Request for an unknown neighbor \"" + neighbor
                         + "\", so will be skipped");
                return null;
            } catch (NonUniqueResultException e) {
                LOG.error("More than one KnownNeighbor named \"" + neighbor
                          + "\". This need to be fixed!");
                return null;
            }
        }

        final String timing;
        final Date afterToUse;
        final Date beforeToUse;
        if (reversed) {
            if (null == after) {
                timing = "Before";
            } else {
                timing = "BetweenDesc";
            }
            afterToUse = after;
            if (null == before) {
                beforeToUse = MAXIMUM_DATE_TIME;
            } else {
                beforeToUse = before;
            }
        } else {
            if (null == before) {
                timing = "After";
            } else {
                timing = "BetweenAsc";
            }
            if (null == after) {
                afterToUse = new Date(0);
            } else {
                afterToUse = after;
            }
            beforeToUse = before;
        }

        if (Stamped.CONFIRMED == stampedToUse) {
            return getDigestFromConfirmations(afterToUse,
                                              beforeToUse,
                                              timing,
                                              max,
                                              knownNeighbor);
        }
        return getDigestFromShippedFiles(stampedToUse,
                                         afterToUse,
                                         beforeToUse,
                                         timing,
                                         max,
                                         knownNeighbor,
                                         registrations);
    }

    /**
     * Returns the sequence to {@link Confirmation} instance that match the
     * specified criteria.
     *
     * @param namedQuery
     *            the name of the query to used.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     *
     * @return the sequence to {@link TicketedFile} instance that match the
     *         specified criteria.
     */
    private List<Confirmation> getConfirmationsByTime(final String namedQuery,
                                                      final Date after,
                                                      final Date before,
                                                      final int max,
                                                      final KnownNeighbor neighbor) {
        final TypedQuery<Confirmation> query = entityManager.createNamedQuery(namedQuery,
                                                                              Confirmation.class);
        if (null != after) {
            query.setParameter("after",
                               after);
        }
        if (null != before) {
            query.setParameter("before",
                               before);
        }
        query.setParameter("neighbor",
                           neighbor);
        if (max > 0) {
            query.setMaxResults(max);
        }
        final List<Confirmation> confirmations = query.getResultList();
        return confirmations;
    }

    private List<DigestChange> getDigestFromConfirmations(Date after,
                                                          Date before,
                                                          String timing,
                                                          int max,
                                                          KnownNeighbor knownNeighbor) {

        final List<Confirmation> confirmations = getConfirmationsByTime("confirmedByDestination" + timing,
                                                                        after,
                                                                        before,
                                                                        max,
                                                                        knownNeighbor);
        if (null == confirmations) {
            return NO_SHIPPED_FILES;
        }

        final List<DigestChange> result = new ArrayList<>(confirmations.size());
        for (Confirmation confirmation : confirmations) {
            // TODO: Clean up call so Key is not exposed!
            final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                                 (confirmation.getShippedFile()).getTicketedFileKey());
            final Date when = confirmation.getWhenConfirmed();
            final List<Detail> details = new ArrayList<>();
            details.add(new Detail(WHEN_DISPATCHED,
                                   new Date((confirmation.getWhenDelivered()).getTime())));
            result.add(new DigestChange(ticketedFile.getBundle(),
                                        when,
                                        details));
        }
        return result;
    }

    private List<DigestChange> getDigestFromShippedFiles(Stamped stamped,
                                                         Date after,
                                                         Date before,
                                                         String timing,
                                                         int max,
                                                         KnownNeighbor knownNeighbor,
                                                         List<String> registrations) {
        List<? extends KnownRegistration> knownRegistrations;
        if (Stamped.CONFIRMABLE == stamped) {
            knownRegistrations = loadedRegistrations.getKnownReceivingRegistrations(knownNeighbor,
                                                                                    registrations);
        } else {
            knownRegistrations = loadedRegistrations.getKnownDispatchingRegistrations(knownNeighbor,
                                                                                      registrations);
            // select shipped
        }
        /*
         * If there has been a request to filter on registration but none of them are
         * valid then return nothing, otherwise the result would be to return everything
         * which is clearly not the intention.
         */
        if (null != registrations && !registrations.isEmpty()
            && knownRegistrations.isEmpty()) {
            return NO_SHIPPED_FILES;
        }

        final String qualifiers;
        if (null == knownNeighbor && (null == knownRegistrations || knownRegistrations.isEmpty())) {
            qualifiers = "";
        } else {
            if (null == knownNeighbor || Stamped.CONFIRMABLE == stamped) {
                qualifiers = "ByRegistration";
            } else if (null == knownRegistrations || knownRegistrations.isEmpty()) {
                qualifiers = "ByDestination";
            } else {
                qualifiers = "ByRegistrationAndDestination";
            }
        }

        final String category;
        if (Stamped.CONFIRMABLE == stamped) {
            if (null == knownRegistrations || knownRegistrations.isEmpty()) {
                return NO_SHIPPED_FILES;
            }
            category = "confirmable";
        } else if (Stamped.UNVERIFIED == stamped) {
            category = "unverified";
        } else if (Stamped.VERIFIED == stamped) {
            category = "verified";
        } else {
            // Should never get here is all enumerations as dealt with.
            throw new IllegalArgumentException();
        }
        final KnownNeighbor knownNeighborToUse;
        if (Stamped.CONFIRMABLE == stamped) {
            knownNeighborToUse = null;
        } else {
            knownNeighborToUse = knownNeighbor;
        }
        final List<ShippedFile> shippedFiles = getShippedFilesByTime(category + qualifiers
                                                                     + timing,
                                                                     after,
                                                                     before,
                                                                     max,
                                                                     knownNeighborToUse,
                                                                     knownRegistrations);
        if (null == shippedFiles) {
            return NO_SHIPPED_FILES;
        }

        final List<DigestChange> result = createDigest(stamped,
                                                       shippedFiles,
                                                       after,
                                                       before,
                                                       knownNeighbor);
        return result;
    }

    /**
     * Returns the sequence to {@link TicketedFile} instance that match the
     * specified criteria.
     *
     * @param namedQuery
     *            the name of the query to use when no "before" is specified.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     *
     * @return the sequence to {@link TicketedFile} instance that match the
     *         specified criteria.
     */
    private List<ShippedFile> getShippedFilesByTime(final String namedQuery,
                                                    final Date after,
                                                    final Date before,
                                                    final int max,
                                                    final KnownNeighbor neighbor,
                                                    final List<? extends KnownRegistration> registrations) {
        final TypedQuery<ShippedFile> query = entityManager.createNamedQuery(namedQuery,
                                                                             ShippedFile.class);
        if (null != after) {
            query.setParameter("after",
                               after);
        }
        if (null != before) {
            query.setParameter("before",
                               before);
        }
        if (null != neighbor) {
            query.setParameter("neighbor",
                               neighbor);
        }
        if (null != registrations && !registrations.isEmpty()) {
            query.setParameter("registrations",
                               registrations);
        }
        if (max > 0) {
            query.setMaxResults(max);
        }
        final List<ShippedFile> shippedFiles = query.getResultList();
        return shippedFiles;
    }

    @Override
    public Collection<String> getUndeliveredTransfers(final String ticket) {
        final TypedQuery<String> query = entityManager.createNamedQuery("getUndeliveredNeighborsByTicket",
                                                                        String.class);
        query.setParameter("ticket",
                           Integer.parseInt(ticket));
        return query.getResultList();
    }

    @Override
    public void prepare(final String ticket,
                        final String bundle,
                        final File metadataFile,
                        final File transferFile,
                        final Collection<String> transfers) throws IOException,
                                                            MetadataParseException {
        final Metadata metadata = metadataManager.createMetadata(metadataFile);
        String algorithm;
        if (metadata instanceof ChecksumMetadata) {
            final ChecksumElement checksumElement = ((ChecksumMetadata) metadata).getChecksum();
            if (null == checksumElement) {
                algorithm = DEFAULT_ALGORITHM;
            } else if (null == checksumElement.getValue()) {
                algorithm = checksumElement.getAlgorithm();
            } else {
                confirmationModifier.prepare(ticket,
                                             checksumElement,
                                             transfers);
                return;
            }
        } else {
            algorithm = DEFAULT_ALGORITHM;
        }
        final ChecksumElement checksumElement = new ChecksumElement(ChecksumFactory.calculateValue(transferFile,
                                                                                                   algorithm),
                                                                    algorithm);
        if (metadata instanceof ChecksumMetadata) {
            ((ChecksumMetadata) metadata).setChecksum(checksumElement);
            writeMetadataFile(bundle,
                              metadataFile.getParentFile(),
                              metadata);
        }
        confirmationModifier.prepare(ticket,
                                     checksumElement,
                                     transfers);
    }

    @Override
    public void start(final String ticket) {
        confirmationModifier.start(ticket);
    }

    /**
     * Safely write out the new metadata file.
     *
     * @param bundle
     *            the name of the {@link Bundle} instance whose metadata is to be
     *            written.
     * @param directory
     *            the directory in which to write the {@link Metadata} instance.
     * @param metadata
     *            the {@link Metadata} instance to save.
     *
     * @return The {@link File} instance into which the {@link Metadata} instance
     *         was written.
     *
     * @throws IOException
     */
    private File writeMetadataFile(final String bundleName,
                                   final File directory,
                                   final Metadata metadata) throws IOException {
        final File metadataFile = new File(directory,
                                           InternalFileName.getMetadataFilename(bundleName));
        final File tmpFile = new File(metadataFile.getParentFile(),
                                      metadataFile.getName() + InternalFileName.TMP_SUFFIX);
        metadataManager.save(metadata,
                             tmpFile);
        cacheManager.move(tmpFile,
                          metadataFile);
        return metadataFile;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
