package gov.lbl.nest.spade.services.impl;

import gov.lbl.nest.spade.activities.IngestTermination;
import gov.lbl.nest.spade.activities.IngestTerminationImpl;
import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.TerminationManager;
import gov.lbl.nest.spade.services.TicketManager;
import jakarta.annotation.Resource;
import jakarta.ejb.Stateless;
import jakarta.ejb.Timeout;
import jakarta.ejb.Timer;
import jakarta.ejb.TimerService;
import jakarta.inject.Inject;

/**
 * This is the standard implementation of the {@link TerminationManager}
 * interface.
 *
 * @author patton
 */
@Stateless
public class TerminationManagerImpl implements
                                    TerminationManager {

    /**
     * The {@link CacheManager} instance used by this object.
     */
    @Inject
    private CacheManager cacheManager;

    /**
     * The {@link Configuration} instance of this application.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link TicketManager} used by this object.
     */
    @Inject
    private TicketManager ticketManager;

    /**
     * The {@link TimerService} instance used to retry fetching.
     */
    @Resource
    private TimerService timerService;

    @Override
    public IngestTermination createIngestTermination() {
        final Boolean preserve = (configuration.getAssembly()).getPreserve();
        final boolean preserveToUse = null != preserve && preserve == Boolean.TRUE;
        return new IngestTerminationImpl(((configuration.getCacheDefinition()).getCacheMap()).get(CacheDefinition.PROBLEM),
                                         preserveToUse,
                                         cacheManager,
                                         ticketManager,
                                         timerService);
    }

    /**
     * Executed in order for failed Fetcher
     *
     * @param timer
     *            the {@link Timer} instance that expired.
     */
    @Timeout
    public void timeout(Timer timer) {
        final IngestTicket ticket = (IngestTicket) timer.getInfo();
        ticketManager.unblock(ticket);
    }

}
