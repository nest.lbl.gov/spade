package gov.lbl.nest.spade.services;

/**
 * This class is an extension of the {@link RuntimeException} class to add on
 * the {@link NonStrackTraceException} interface in order to suppress stacktrace
 * outputs.
 *
 * @author patton
 *
 */
public class VerificationFailure extends
                                 RuntimeException implements
                                 NonStrackTraceException {
    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    /**
     * The message to output as an alternate to executing a
     * <code>printStackTrace</code> method.
     */
    private final String alternate;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the detail message (which is saved for later retrieval by the
     *            Throwable.getMessage() method).
     * @param cause
     *            the Exception that caused this object was thrown.
     * @param alternate
     *            the message to output as an alternate to executing a
     *            <code>printStackTrace</code> method.
     */
    public VerificationFailure(final String message,
                               final Throwable cause,
                               final String alternate) {
        super(message,
              cause);
        this.alternate = alternate;
    }

    // instance member method (alphabetic)

    @Override
    public String getAlternateMessage() {
        return alternate;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
