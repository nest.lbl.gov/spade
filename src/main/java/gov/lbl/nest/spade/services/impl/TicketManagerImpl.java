package gov.lbl.nest.spade.services.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.monitor.Instrumentation;
import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.ejb.KnownRegistration;
import gov.lbl.nest.spade.ejb.ShippedFile;
import gov.lbl.nest.spade.ejb.TicketedFile;
import gov.lbl.nest.spade.interfaces.SpadeDB;
import gov.lbl.nest.spade.registry.internal.InboundSemaphore;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.rs.Bundles;
import gov.lbl.nest.spade.services.TicketManager;
import gov.lbl.nest.spade.services.TooManyTicketsException;
import jakarta.ejb.AccessTimeout;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.TypedQuery;
import jakarta.resource.ResourceException;

/**
 * This is the standard implementation of the {@link TicketManager} interface.
 *
 * @author patton
 */
@Singleton
@AccessTimeout(value = 60,
               unit = TimeUnit.MINUTES)
@Lock(LockType.WRITE)
public class TicketManagerImpl implements
                               TicketManager {

    private static class AwaitingRemoval implements
                                         Comparable<AwaitingRemoval> {

        Date date;

        String bundle;

        /**
         * Creates an instance of this class.
         *
         * @param date
         *            the date and time when the bundle was disposed.
         * @param bundle
         *            the name of the bunlde disposed.
         */
        AwaitingRemoval(final Date date,
                        final String bundle) {
            this.date = date;
            this.bundle = bundle;
        }

        @Override
        public int compareTo(AwaitingRemoval o) {
            return date.compareTo(o.date);
        }

        String getBundle() {
            return bundle;
        }

        Date getDate() {
            return date;
        }

    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(TicketManagerImpl.class);

    /**
     * The {@link Instrumentation}, if any, used to instrument {@link IngestTicket}
     * operations.
     */
    private static final Instrumentation TRANSFER_INSTRUMENTATION = Instrumentation.getInstrumentation(TicketManager.class.getSimpleName() + "."
                                                                                                       + "transfer");

    private static final Comparator<TicketedFile> WHEN_TICKETED_COMPARATOR_DESC = new Comparator<>() {

        @Override
        public int compare(TicketedFile o1,
                           TicketedFile o2) {
            final Date whenPlaced1 = o1.getWhenPlaced();
            final Date whenPlaced2 = o2.getWhenPlaced();
            if (null == whenPlaced1) {
                if (null == whenPlaced2) {
                    return 0;
                }
                return 1;
            }
            if (null == whenPlaced2) {
                return -1;
            }
            if (whenPlaced2.after(whenPlaced1)) {
                return 1;
            }
            if (whenPlaced1.after(whenPlaced2)) {
                return -1;
            }
            return 0;
        }
    };

    // private static member data

    // private instance member data

    /**
     * Saves instrumented information for the disposal of a ticket.
     *
     * @param ticket
     *            the ticket being disposal of.
     */
    private static void instrumentDisposal(final IngestTicket ticket) {
        final Map<String, String> attributes;
        attributes = new HashMap<>(1);
        final String name = (ticket.getBundle()).getName();
        attributes.put("bundle.id",
                       name);
        attributes.put("ticket.id",
                       name);
        TRANSFER_INSTRUMENTATION.pointOfInterst("dispose",
                                                ticket.getUuid(),
                                                attributes);
        LOG.info("Disposal of ticket for \"" + name
                 + "\"");
    }

    /**
     * Saves instrumented information for the issuance of a ticket.
     *
     * @param ticket
     *            the ticket that was issued.
     */
    private static void instrumentIssuance(final IngestTicket ticket) {
        final Bundle bundle = ticket.getBundle();
        final String name = bundle.getName();
        LOG.info("Issuing ingest ticket #" + ticket.getIdentity()
                 + " for bundle \""
                 + name
                 + "\" for registration \""
                 + bundle.getLocalId()
                 + "\"");
        final Map<String, String> attributes;
        attributes = new HashMap<>(1);
        attributes.put("bundle.id",
                       name);
        attributes.put("ticket.id",
                       ticket.getIdentity());
        TRANSFER_INSTRUMENTATION.pointOfInterst("issued",
                                                ticket.getUuid(),
                                                attributes);
    }

    // constructors

    /**
     * Saves instrumented information for the issuance of a ticket.
     *
     * @param ticket
     *            the ticket that was issued.
     */
    private static void instrumentReissuance(final IngestTicket ticket) {
        final String name = (ticket.getBundle()).getName();
        LOG.info("Reissuing ingest ticket #" + ticket.getIdentity()
                 + " for bundle \""
                 + name
                 + "\"");
        final Map<String, String> attributes;
        attributes = new HashMap<>(1);
        attributes.put("bundle.id",
                       name);
        attributes.put("ticket.id",
                       ticket.getIdentity());
        TRANSFER_INSTRUMENTATION.pointOfInterst("reissued",
                                                ticket.getUuid(),
                                                attributes);
    }

    /**
     * Saves instrumented information for the release of a ticket.
     *
     * @param ticket
     *            the ticket that was issued.
     */
    private static void instrumentRelease(final Bundle bundle) {
        final String name = bundle.getName();
        LOG.info("Releasing ingest ticket for bundle \"" + name
                 + "\"");
        final Map<String, String> attributes;
        attributes = new HashMap<>(1);
        attributes.put("bundle.id",
                       name);
        TRANSFER_INSTRUMENTATION.pointOfInterst("released",
                                                null,
                                                attributes);
    }

    /**
     * Saves instrumented information for the release of a ticket.
     *
     * @param ticket
     *            the ticket that was issued.
     */
    private static void instrumentRelease(final IngestTicket ticket) {
        final String name = (ticket.getBundle()).getName();
        LOG.info("Releasing ingest ticket #" + ticket.getIdentity()
                 + " for bundle \""
                 + name
                 + "\"");
        final Map<String, String> attributes;
        attributes = new HashMap<>(1);
        attributes.put("bundle.id",
                       name);
        attributes.put("ticket.id",
                       ticket.getIdentity());
        TRANSFER_INSTRUMENTATION.pointOfInterst("released",
                                                ticket.getUuid(),
                                                attributes);
    }

    /**
     * Saves instrumented information for the retention of a ticket.
     *
     * @param ticket
     *            the ticket that was issued.
     */
    private static void instrumentRetention(final IngestTicket ticket) {
        final String name = (ticket.getBundle()).getName();
        LOG.info("Retaining ingest ticket #" + ticket.getIdentity()
                 + " for bundle \""
                 + name
                 + "\"");
        final Map<String, String> attributes;
        attributes = new HashMap<>(1);
        attributes.put("bundle.id",
                       name);
        attributes.put("ticket.id",
                       ticket.getIdentity());
        TRANSFER_INSTRUMENTATION.pointOfInterst("retained",
                                                ticket.getUuid(),
                                                attributes);
    }

    /**
     * Collection of bundles awating removal.
     */
    private PriorityQueue<AwaitingRemoval> awaitingRemoval = new PriorityQueue<>();

    // instance member method (alphabetic)

    /**
     * The {@link Map} of bundles that are currently blocked and the associated
     * cause, if any.
     */
    private Map<String, String> blockedBundles = new HashMap<>();

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @SpadeDB
    private EntityManager entityManager;

    /**
     * The {@link Set} of bundles for whom an {@link IngestTicket} instance have
     * been issued and that are awaiting either disposal or release.
     */
    private Set<String> issuedIngest = new HashSet<>();

    /**
     * True if this Object is using the {@link #setLatestScan(Date)} method to
     * manage bundle removal.
     */
    private boolean usingLatestScan = false;

    /**
     * Creates an instance of this class.
     */
    protected TicketManagerImpl() {
    }

    /**
     * Creates an instance of this class.
     *
     * @param entityManager
     *            the {@link EntityManager} instance used by this object.
     */
    public TicketManagerImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public <T> boolean block(T ticket,
                             String cause) {
        if (ticket instanceof IngestTicket) {
            final IngestTicket ingestTicket = (IngestTicket) ticket;
            final String name = (ingestTicket.getBundle()).getName();
            if (issuedIngest.contains(name) && !blockedBundles.containsKey(name)) {
                if (issuedIngest.remove(name)) {
                    instrumentRetention(ingestTicket);
                    blockedBundles.put(name,
                                       cause);
                    return blockedBundles.containsKey(name);
                }
            }
            return false;
        }
        throw new UnsupportedOperationException("The \"retain\" method does not support tickets of class \"" + (ticket.getClass()).getSimpleName()
                                                + "\"");
    }

    @Override
    public <T> boolean dispose(T ticket) {
        if (ticket instanceof IngestTicket) {
            final IngestTicket ingestTicket = (IngestTicket) ticket;
            final String bundle = (ingestTicket.getBundle()).getName();
            if (issuedIngest.contains(bundle)) {
                instrumentDisposal(ingestTicket);
                synchronized (awaitingRemoval) {
                    awaitingRemoval.add(new AwaitingRemoval(new Date(),
                                                            bundle));
                }
                if (!usingLatestScan) {
                    /** Removes all bundles awaiting removal. */
                    setLatestScan(null);
                }
            }
            return false;
        }
        throw new UnsupportedOperationException("The \"dispose\" method does not support tickets of class \"" + (ticket.getClass()).getSimpleName()
                                                + "\"");
    }

    @Override
    @Lock(LockType.READ)
    public Bundles getIssuedBundles() {
        final List<Bundle> result = new ArrayList<>();
        for (String name : issuedIngest) {
            result.add(new Bundle(name));
        }
        return new Bundles(null,
                           result);
    }

    @Override
    @Lock(LockType.READ)
    public int getIssuedCount() {
        return issuedIngest.size();
    }

    @Override
    @Lock(LockType.READ)
    public String getPlacementId(final String bundle) {
        final TypedQuery<String> query = entityManager.createNamedQuery("getPlacementIdByBundle",
                                                                        String.class);
        query.setParameter("bundle",
                           bundle);
        final List<String> placementIds = query.getResultList();
        if (null == placementIds || placementIds.isEmpty()) {
            return null;
        }
        return placementIds.get(0);
    }

    @Override
    public List<String> getPlacementIds(List<String> bundles) {
        if (null == bundles || bundles.isEmpty()) {
            return new ArrayList<>(0);
        }

        final List<String> result = new ArrayList<>(bundles.size());

        final TypedQuery<TicketedFile> query = entityManager.createNamedQuery("ticketedFilesByNames",
                                                                              TicketedFile.class);
        query.setParameter("bundles",
                           bundles);
        final List<TicketedFile> resultList = query.getResultList();
        if (null == resultList || resultList.isEmpty()) {
            return result;
        }

        final Map<String, TicketedFile> ticketedFiles = new HashMap<>();
        for (TicketedFile ticketedFile : resultList) {
            final String bundle = ticketedFile.getBundle();
            final TicketedFile knownFile = ticketedFiles.get(bundle);
            if (null == knownFile) {
                ticketedFiles.put(bundle,
                                  ticketedFile);
            } else if (0 > WHEN_TICKETED_COMPARATOR_DESC.compare(ticketedFile,
                                                                 knownFile)) {
                ticketedFiles.put(bundle,
                                  ticketedFile);
            }
        }

        for (String bundle : bundles) {
            final TicketedFile knownFile = ticketedFiles.get(bundle);
            if (null != knownFile) {
                result.add(knownFile.getPlacementId());
            }
        }

        return result;
    }

    @Override
    @Lock(LockType.READ)
    public Bundles getRetainedBundles() {
        final List<Bundle> result = new ArrayList<>();
        for (String name : blockedBundles.keySet()) {
            result.add(new Bundle(name,
                                  blockedBundles.get(name)));
        }
        return new Bundles(null,
                           result);
    }

    @Override
    public List<String> getTickets(String bundle) {
        final TypedQuery<TicketedFile> query = entityManager.createNamedQuery("ticketedFilesByBundle",
                                                                              TicketedFile.class);
        query.setParameter("bundle",
                           bundle);
        final List<TicketedFile> tickets = query.getResultList();

        final List<String> result = new ArrayList<>();
        if (null != tickets && !tickets.isEmpty()) {
            for (TicketedFile ticket : tickets) {
                result.add(ticket.getTicketIdentity());
            }
        }
        return result;
    }

    // static member methods (alphabetic)

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public IngestTicket issue(Bundle bundle) throws TooManyTicketsException {
        final String name = bundle.getName();
        if (issuedIngest.contains(name) || blockedBundles.containsKey(name)) {
            return null;
        }
        issuedIngest.add(name);
        try {
            final UUID uuid = UUID.randomUUID();
            final String identity = issueNewIdentity(bundle,
                                                     uuid);
            final IngestTicket result = new IngestTicket(identity,
                                                         bundle,
                                                         uuid);
            instrumentIssuance(result);
            return result;
        } catch (IllegalArgumentException e) {
            if (issuedIngest.remove(name)) {
                blockedBundles.put(name,
                                   e.getMessage());
            }
            LOG.error(e.getMessage());
            return null;
        } catch (Exception e) {
            if (e instanceof ResourceException) {
                issuedIngest.remove(name);
                LOG.error(e.getMessage());
            } else {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * Issues a new {@link IngestTicket} identity for the specified {@link Bundle}
     * instance.
     *
     * @param bundle
     *            the {@link Bundle} instance requesting a new identity.
     * @param uuid
     *            the {@link UUID} to assign to the new identity.
     *
     * @return the newly issued identity.
     */
    private String issueNewIdentity(Bundle bundle,
                                    UUID uuid) {
        final KnownRegistration registration;
        final TypedQuery<KnownRegistration> query = entityManager.createNamedQuery("getKnownRegistrationsByLocalId",
                                                                                   KnownRegistration.class);
        query.setParameter("localId",
                           bundle.getLocalId());
        try {
            registration = query.getSingleResult();
        } catch (NoResultException e) {
            throw new IllegalArgumentException("No such registration as \"" + bundle.getLocalId()
                                               + "\"");
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("Two or more registrations match \"" + bundle.getLocalId()
                                            + "\"");
        }
        final TicketedFile ticketedFile = new TicketedFile(bundle.getName(),
                                                           uuid,
                                                           registration);
        entityManager.persist(ticketedFile);
        if (bundle.isInbound()) {
            final InboundSemaphore inboundSemaphore = new InboundSemaphore((bundle.getSemaphore()).getName());

            ShippedFile shippedFile = new ShippedFile(ticketedFile,
                                                      inboundSemaphore.getTicket(),
                                                      inboundSemaphore.getRegistration());
            entityManager.persist(shippedFile);
        }
        return ticketedFile.getTicketIdentity();
    }

    @Override
    @Lock(LockType.READ)
    public <T> boolean reissue(T ticket) {
        if (ticket instanceof IngestTicket) {
            final IngestTicket ingestTicket = (IngestTicket) ticket;
            final TypedQuery<Boolean> query = entityManager.createNamedQuery("activeTicket",
                                                                             Boolean.class);
            query.setParameter("identity",
                               Integer.parseInt(((IngestTicket) ticket).getIdentity()));
            final Boolean active = query.getSingleResult();
            if (!active) {
                LOG.warn("Attempt to re-issue an abandoned or non-existent ticket, " + ingestTicket.getIdentity());
                return false;
            }
            final String name = (ingestTicket.getBundle()).getName();
            if (issuedIngest.contains(name)) {
                return false;
            }
            issuedIngest.add(name);
            try {
                instrumentReissuance(ingestTicket);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void setLatestScan(Date dateTime) {
        if (null != dateTime && !usingLatestScan) {
            usingLatestScan = true;
        }
        synchronized (awaitingRemoval) {
            while (!awaitingRemoval.isEmpty()) {
                AwaitingRemoval bundle = awaitingRemoval.peek();
                if (null != bundle && (null == dateTime || dateTime.after(bundle.getDate()))) {
                    issuedIngest.remove((awaitingRemoval.poll()).getBundle());
                } else if (null != bundle) {
                    final String count = Integer.toString(awaitingRemoval.size());
                    final String plural;
                    if ("1" == count) {
                        plural = "";
                    } else {
                        plural = "s";
                    }

                    LOG.debug("There are " + count
                              + " disposed ticket"
                              + plural
                              + " awaiting removal");
                    return;
                }
            }
        }
        LOG.debug("There are no more disposed tickets awaiting removal");
    }

    @Override
    public boolean unblock(Bundle bundle) {
        final String name = bundle.getName();
        if (blockedBundles.containsKey(name)) {
            instrumentRelease(bundle);
            blockedBundles.remove(name);
            return !blockedBundles.containsKey(name);
        }
        return false;
    }

    @Override
    public <T> boolean unblock(T ticket) {
        if (ticket instanceof IngestTicket) {
            final IngestTicket ingestTicket = (IngestTicket) ticket;
            final String bundle = (ingestTicket.getBundle()).getName();
            if (blockedBundles.containsKey(bundle)) {
                instrumentRelease(ingestTicket);
                blockedBundles.remove(bundle);
                return blockedBundles.containsKey(bundle);
            }
            return false;
        }
        throw new UnsupportedOperationException("The \"release\" method does not support tickets of class \"" + (ticket.getClass()).getSimpleName()
                                                + "\"");
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
