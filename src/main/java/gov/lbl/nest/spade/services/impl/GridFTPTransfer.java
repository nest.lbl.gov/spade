package gov.lbl.nest.spade.services.impl;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.Collection;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitParam;
import gov.lbl.nest.spade.grid.ProxyManager;
import gov.lbl.nest.spade.interfaces.policy.ImplementedPolicy;
import gov.lbl.nest.spade.services.FileTransfer;

/**
 * This class implements the {@link FileTransfer} interface in order to transfer
 * files using GridFTP.
 *
 * @author patton
 */
public class GridFTPTransfer extends
                             X509ProxyTransfer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The default protocol for the gfal-copy.
     */
    private static final String DEFAULT_PORT = "";

    /**
     * The default protocol for the gfal-copy.
     */
    private static final String DEFAULT_LOCAL_PROTOCOL = "file";

    /**
     * The default protocol for the gfal-copy.
     */
    private static final String DEFAULT_PROTOCOL = "sshftp";

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(GridFTPTransfer.class);

    /**
     * The name of the default properties file contains the mount points mapping.
     */
    private static final String MOUNT_POINT_PROPERTIES = "gfal-mounts.properties";

    /**
     * The name of the resource contains the path to the properties file contains
     * the mount points mapping.
     */
    private static final String MOUNT_POINT_RESOURCE = "gfal_third_party.mounts";

    /**
     * The "host:port" separator string.
     */
    private static final String PORT_SEPARATOR = ":";

    // private static member data

    /**
     * The {@link ProxyManager} used by this class.
     */
    private static AtomicBoolean initialProxyCreated = new AtomicBoolean();

    /**
     * The {@link ProxyManager} used by this class.
     */
    private static ProxyManager proxyManager;

    // private instance member data

    /**
     * The protocol to use for the remote file transfer.
     */
    private final String farProtocol;

    /**
     * The flags, if any, to use with the transfer command.
     */
    private final String[] flags;

    /**
     * The protocol to use for the local file in third party transfers.
     */
    private final String nearProtocol;

    /**
     * The string specifying the port, if any, to used for the transfer.
     */
    private final String port;

    // constructors

    /**
     * Create an instance of this class.
     *
     * The first instance created will attempt to create the necessary VOMs proxy.
     */
    public GridFTPTransfer() {
        this(null);
    }

    /**
     * Create an instance of this class.
     *
     * The first instance created will attempt to create the necessary VOMs proxy.
     *
     * @param parameters
     *            the {@link InitParam} instances to use when creation the new
     *            instance.
     */
    public GridFTPTransfer(final Collection<InitParam> parameters) {
        final String flagsToUse = ImplementedPolicy.getParameter(parameters,
                                                                 "flags");
        if (null == flagsToUse || 0 == (flagsToUse.trim()).length()) {
            flags = null;
        } else {
            final String trimmedFlags = flagsToUse.trim();
            flags = trimmedFlags.split(" ");
            LOG.info("Using flags \"" + trimmedFlags
                     + "\"");
        }

        final String portToUse = ImplementedPolicy.getParameter(parameters,
                                                                "port");
        if (null == portToUse || 0 == (portToUse.trim()).length()) {
            port = DEFAULT_PORT;
        } else {
            final String trimmedPort = portToUse.trim();
            if (trimmedPort.startsWith(PORT_SEPARATOR)) {
                port = portToUse;
            } else {
                port = PORT_SEPARATOR + trimmedPort;
            }
            LOG.info("Using port \"" + port.substring(1)
                     + "\"");
        }

        final String protocolToUse = ImplementedPolicy.getParameter(parameters,
                                                                    "protocol");
        if (null == protocolToUse) {
            farProtocol = DEFAULT_PROTOCOL;
        } else {
            final String trimmedProtocol = protocolToUse.trim();
            if (trimmedProtocol.endsWith(PROTOCOL_SEPARATOR)) {
                farProtocol = portToUse.substring(0,
                                                  trimmedProtocol.length() - PROTOCOL_SEPARATOR.length());
            } else {
                farProtocol = trimmedProtocol;
            }
            LOG.info("Using protocol \"" + farProtocol
                     + "\"");
        }

        final String localProtocolToUse = ImplementedPolicy.getParameter(parameters,
                                                                         "local_protocol");
        if (null == localProtocolToUse) {
            nearProtocol = DEFAULT_LOCAL_PROTOCOL;
        } else {
            final String trimmedProtocol = localProtocolToUse.trim();
            if (trimmedProtocol.endsWith(PROTOCOL_SEPARATOR)) {
                nearProtocol = portToUse.substring(0,
                                                   trimmedProtocol.length() - PROTOCOL_SEPARATOR.length());
            } else {
                nearProtocol = trimmedProtocol;
            }
            LOG.info("Doing third party transfers, using protocol \"" + nearProtocol
                     + "\" for local files");
        }

        if (!initialProxyCreated.get()) {
            synchronized (initialProxyCreated) {
                if (!initialProxyCreated.get()) {
                    proxyManager = new ProxyManager();
                    try {
                        if (proxyManager.issueProxy()) {
                            LOG.info("Create VOMS proxy for " + getClass().getSimpleName());
                        } else {
                            LOG.info("No VOMS proxy was created for " + getClass().getSimpleName());
                        }
                    } catch (CertificateException
                             | IOException
                             | InterruptedException e) {
                        LOG.error("Failed in attempt to for " + getClass().getSimpleName());
                        e.printStackTrace();
                    }
                    initialProxyCreated.set(true);
                }
            }
        }
    }

    @Override
    protected String getFarPort() {
        return port;
    }

    @Override
    protected String getFarProtocol() {
        return farProtocol;
    }

    @Override
    protected String[] getFlags() {
        return flags;
    }

    @Override
    protected String getMetadataNearProtocol() {
        return LOCAL_FILESYSTEM_PROTOCOL;
    }

    @Override
    protected String getMountPointProperties() {
        return MOUNT_POINT_PROPERTIES;
    }

    @Override
    protected String getMountPointResource() {
        return MOUNT_POINT_RESOURCE;
    }

    @Override
    protected String getSemaphoreNearProtocol() {
        return LOCAL_FILESYSTEM_PROTOCOL;
    }

    @Override
    protected String getTransferCommand() {
        return "globus-url-copy";
    }

    @Override
    protected String getTransferNearProtocol() {
        return nearProtocol;
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
