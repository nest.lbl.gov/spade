package gov.lbl.nest.spade.services;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import gov.lbl.nest.spade.activities.IngestTicket;

/**
 * This interface is used to manage the spooling area where unsuccessfully
 * aechived files are held.
 *
 * @author patton
 *
 */
public interface SpoolManager {

    /**
     * Returns the {@link IngestTicket} instance, if any, that can be used to
     * execute the re-ship unconfirmed transfers.
     *
     * @param ticket
     *            the identity of the ticket whose ticket should be returned.
     *
     * @return the {@link IngestTicket} instance, if any, that can be used to
     *         execute the re-ship unconfirmed transfers.
     */
    IngestTicket getSpooledTicket(String ticket);

    /**
     * Returns true is the directory matches the one used as the spool directory.
     *
     * @param directory
     *            the directory to be tested.
     *
     * @return true is the directory matches the one used as the spool directory.
     */
    boolean isSpoolDirectory(File directory);

    /**
     * Places the specified files into the spool area for archiving later.
     *
     * @param ticket
     *            the {@link IngestTicket} instance associated with the files.
     * @param name
     *            the name of the task requesting this placement.
     * @param storageLoad
     *            the storage load for task requesting this placement.
     * @param archiveFile
     *            the file to be archived.
     * @param archiveMetadata
     *            the metadata file, if any to be archived with the main file.
     *
     * @throws TimeoutException
     *             when the placement times out
     * @throws InterruptedException
     *             when the placement is interrupted
     * @throws IOException
     *             when there is an IO problem.
     */
    void placeInSpool(final IngestTicket ticket,
                      final String name,
                      final int storageLoad,
                      final File archiveFile,
                      final File archiveMetadata) throws TimeoutException,
                                                  InterruptedException,
                                                  IOException;

    /**
     * Removed the files associated with the specified ticket from the spool.
     *
     * @param ticket
     *            the ticket whose files should be removed.
     */
    void removeFromSpool(final IngestTicket ticket);
}
