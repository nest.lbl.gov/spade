package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;

import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.ejb.NativeWarehouseManager;
import gov.lbl.nest.spade.interfaces.SpadeDB;
import gov.lbl.nest.spade.interfaces.storage.WarehouseManager;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

/**
 * This class implements the {@link WarehouseManager} interface using the
 * {@link NativeWarehouseManager} implementation.
 *
 * @author patton
 */
@Stateless
public class WarehouseManagerImpl extends
                                  NativeWarehouseManager implements
                                  WarehouseManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Configuration} instance used by this object.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @SpadeDB
    private EntityManager entityManager;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected WarehouseManagerImpl() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param configuration
     *            the {@link Configuration} instance used by the created object.
     * @param entityManager
     *            the {@link EntityManager} instance used by the created object.
     */
    public WarehouseManagerImpl(final Configuration configuration,
                                final EntityManager entityManager) {
        this.configuration = configuration;
        this.entityManager = entityManager;
    }

    // instance member method (alphabetic)

    @Override
    protected void copy(File outside,
                        File warehouse) throws IOException,
                                        InterruptedException {
        LocalhostTransfer.copy(outside,
                               warehouse);
    }

    @Override
    protected File getCompressedRoot(String bundle,
                                     File metaFile) {
        return (configuration.getWarehouseDefinition()).getRootDirectory();
    }

    @Override
    protected File getDataRoot(String bundle,
                               File metaFile) {
        return (configuration.getWarehouseDefinition()).getRootDirectory();
    }

    @Override
    protected EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    protected File getMetaRoot(String bundle,
                               File metaFile) {
        return (configuration.getWarehouseDefinition()).getRootDirectory();
    }

    @Override
    protected File getWrappedRoot(String bundle,
                                  File metaFile) {
        return (configuration.getWarehouseDefinition()).getRootDirectory();
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
