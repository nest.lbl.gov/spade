package gov.lbl.nest.spade.services.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.ejb.Confirmation;
import gov.lbl.nest.spade.ejb.KnownNeighbor;
import gov.lbl.nest.spade.ejb.ShippedFile;
import gov.lbl.nest.spade.ejb.TicketedFile;
import gov.lbl.nest.spade.interfaces.SpadeDB;
import gov.lbl.nest.spade.interfaces.metadata.ChecksumElement;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import gov.lbl.nest.spade.services.RetryManager;
import jakarta.ejb.AccessTimeout;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.LockModeType;
import jakarta.persistence.NoResultException;
import jakarta.persistence.NonUniqueResultException;
import jakarta.persistence.TypedQuery;

/**
 * This class is used by the {@link VerificationManagerImpl} to modify the
 * {@link Confirmation} instances in their own transaction.
 *
 * <b>Note:</b> This class uis used to handle modification to the
 * {@link Confirmation} instances to avoid a deadlock that can happen when both
 * {@link VerificationManagerImpl#prepare(String, String, File, File, Collection)}
 * and {@link VerificationManagerImpl#confirm(String, List)} are called in
 * parallel.
 *
 * @author patton
 *
 */
@Singleton
@AccessTimeout(value = 60,
               unit = TimeUnit.MINUTES)
@Lock(value = LockType.WRITE)
public class ConfirmationModifier {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(ConfirmationModifier.class);

    // private static member data

    // private instance member data

    /**
     * Find the matching {@link Confirmation} in the supplied collection.
     *
     * @param name
     *            the name of the {@link Confirmation} instance to find.
     * @param confirmations
     *            the collection of {@link Confirmation} instances to search.
     *
     * @return the match in {@link Confirmation} instance or <code>null</code>.
     */
    private static Confirmation findConfirmation(final String name,
                                                 final List<Confirmation> confirmations) {
        for (Confirmation confirmation : confirmations) {
            if (name.equals((confirmation.getDestination()).getName())) {
                return confirmation;
            }
        }
        return null;
    }
    // Description of this object.
    // @Override
    // public String toString() {}

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @SpadeDB
    private EntityManager entityManager;

    /**
     * The {@link NeighborhoodManager} instance used by this object.
     */
    @Inject
    private NeighborhoodManager neighborhoodManager;

    // constructors

    /**
     * The {@link RetryManager} instance used by this object.
     */
    @Inject
    private RetryManager retryManager;

    /**
     * Create an instance of this class.
     */
    protected ConfirmationModifier() {
    }

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param entityManager
     *            the {@link EntityManager} instance used by the created object.
     * @param neighborhoodManager
     *            the {@link NeighborhoodManager} instance used by this object.
     * @param retryManager
     *            the {@link RetryManager} instance used by this object.
     */
    public ConfirmationModifier(EntityManager entityManager,
                                NeighborhoodManager neighborhoodManager,
                                RetryManager retryManager) {
        this.entityManager = entityManager;
        this.neighborhoodManager = neighborhoodManager;
        this.retryManager = retryManager;
    }

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class to test purposes.
     *
     * @param neighborhoodManager
     *            the {@link NeighborhoodManager} instance used by this object.
     * @param retryManager
     *            the {@link RetryManager} instance used by this object.
     * @param entityManager
     *            the {@link EntityManager} instance used by the created object.
     */
    public ConfirmationModifier(final NeighborhoodManager neighborhoodManager,
                                final RetryManager retryManager,
                                final EntityManager entityManager) {
        this.entityManager = entityManager;
        this.neighborhoodManager = neighborhoodManager;
        this.retryManager = retryManager;
    }

    /**
     * Sets all {@link ShippedFile} instance for the specified bundle as
     * unconfirmed, i.e. <code>null</code>.
     *
     * @param ticketKey
     *            the identity of the ticket whose previous {@link ShippedFile}
     *            instances should be unconfirmed.
     */
    private void abandonPreviousConfirmables(final int ticketKey) {
        final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                             ticketKey);
        final TypedQuery<ShippedFile> query = entityManager.createNamedQuery("getConfirmablesByBundle",
                                                                             ShippedFile.class);
        query.setParameter("bundle",
                           ticketedFile.getBundle());
        final List<ShippedFile> previousTicketeds = query.getResultList();
        if (null != previousTicketeds) {
            for (ShippedFile previousTicketed : previousTicketeds) {
                previousTicketed.setWhenConfirmable(null);
            }
        }
    }

    /**
     * Sets all {@link ShippedFile} instances for the specified bundle as abandoned.
     *
     * @param ticketKey
     *            the identity of the ticket whose previous {@link ShippedFile}
     *            instances should be abandoned.
     */
    private void abandonPreviousVerifications(final int ticketKey) {
        final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                             ticketKey);
        final TypedQuery<ShippedFile> query = entityManager.createNamedQuery("getUnverifiedsByBundle",
                                                                             ShippedFile.class);
        query.setParameter("bundle",
                           ticketedFile.getBundle());
        final List<ShippedFile> previousTicketeds = query.getResultList();
        if (null != previousTicketeds) {
            final Date now = new Date();
            for (ShippedFile previousTicketed : previousTicketeds) {
                previousTicketed.setWhenAbandoned(now);
                retryManager.cancel(previousTicketed.getTicketIdentity());
            }
        }
    }

    /**
     * Tells this object the specified ticket may now be verified by the remote
     * SPADE that delivered it. This method assumes the confirmation information has
     * already been created for the transfer file.
     *
     * @param ticket
     *            the identity of the ticket that may now be verified.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void confirmable(final String ticket) {
        int shippedKey = Integer.parseInt(ticket);
        final ShippedFile shippedFile = entityManager.find(ShippedFile.class,
                                                           shippedKey,
                                                           LockModeType.PESSIMISTIC_WRITE);
        abandonPreviousConfirmables(shippedKey);
        shippedFile.setWhenConfirmable(new Date());
    }

    /**
     * Tells this object to create the confirmation information for the transfer
     * file after which it may be verified by the remote SPADE that delivered it.
     *
     * @param ticket
     *            the identity of the ticket whose confirmation information should
     *            be created.
     * @param checksum
     *            the {@link ChecksumElement} of the {@link File} that was
     *            transferred to this SPADE instance.
     *
     * @throws IOException
     *             if the transfer file can not be found or read.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void confirmable(final String ticket,
                            final ChecksumElement checksum) throws IOException {
        int shippedKey = Integer.parseInt(ticket);
        final ShippedFile shippedFileToUse = getShippedFile(shippedKey,
                                                            checksum);
        abandonPreviousConfirmables(Integer.parseInt(ticket));
        shippedFileToUse.setWhenConfirmable(new Date());
    }

    /**
     * Confirms that the specified ticket has been successfully delivered to the
     * supplied neighbor.
     *
     * @param ticket
     *            the identity of the ticket that is to be confirmed.
     * @param neighbor
     *            the neighbor from which the confirmation has been received.
     *
     * @return the time that delivery was first confirmed for this neighbor.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Date confirmNeighborDelivery(final String neighbor,
                                        final String ticket) {
        final TypedQuery<Confirmation> query = entityManager.createNamedQuery("getConfirmationByTicketAndDestination",
                                                                              Confirmation.class);
        query.setParameter("ticket",
                           Integer.parseInt(ticket));
        query.setParameter("destination",
                           neighbor);
        try {
            final Confirmation knownConfirmation = query.getSingleResult();
            final Date whenConfirmed = knownConfirmation.getWhenConfirmed();
            final Date time;
            if (null == whenConfirmed) {
                time = new Date();
                knownConfirmation.setWhenConfirmed(time);
                LOG.info("Confirmed transfer of \"" + ticket
                         + "\" to \""
                         + neighbor
                         + "\"");
                final ShippedFile shippedFile = knownConfirmation.getShippedFile();
                if (null == shippedFile.getWhenVerificationCompleted() && null != isVerified(shippedFile)) {
                    LOG.info("Verified delivery of \"" + ticket
                             + "\" to all destinations");
                }
            } else {
                time = whenConfirmed;
            }
            return time;
        } catch (NoResultException e) {
            throw new IllegalArgumentException("No Confirmation exists for ticket \"" + ticket
                                               + "\" to \""
                                               + neighbor
                                               + "\"");
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("Two or more Confirmations exists for ticket \"" + ticket
                                            + "\" to \""
                                            + neighbor
                                            + "\"");
        }
    }

    /**
     * Tells this object the specified ticket has been successfully delivered to the
     * supplied neighbor.
     *
     * @param ticket
     *            the identity of the ticket to be verified.
     * @param neighbor
     *            the identity of the remote SPADE instance to which the ticket was
     *            successfully delivered.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void delivered(final String ticket,
                          final String neighbor) {
        final TypedQuery<Confirmation> query = entityManager.createNamedQuery("getConfirmationByTicketAndDestination",
                                                                              Confirmation.class);
        query.setParameter("ticket",
                           Integer.parseInt(ticket));
        query.setParameter("destination",
                           neighbor);
        try {
            final Confirmation confirmation = query.getSingleResult();
            confirmation.setWhenDelivered(new Date());
        } catch (NoResultException e) {
            throw new IllegalArgumentException("No Confirmation exists for ticket \"" + ticket
                                               + "\" to \""
                                               + neighbor
                                               + "\"");
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("Two or more Confirmations exists for ticket \"" + ticket
                                            + "\" to \""
                                            + neighbor
                                            + "\"");
        }
    }

    /**
     * Returns a {@link ShippedFile} instance for the specified key, creating it if
     * necessary, and setting its checksum.
     *
     * @param key
     *            the unique key within the set of {@link TicketedFile} instances
     *            that matches a {@link TicketedFile} instance to this object.
     * @param checksum
     *            the checksum calculated for the file.
     *
     * @return the {@link ShippedFile} instance for the specified key.
     */
    private ShippedFile getShippedFile(int key,
                                       final ChecksumElement checksum) {
        final ShippedFile shippedFile = entityManager.find(ShippedFile.class,
                                                           key,
                                                           LockModeType.PESSIMISTIC_WRITE);
        final ShippedFile shippedFileToUse;
        if (null == shippedFile) {
            shippedFileToUse = new ShippedFile(key);
            entityManager.persist(shippedFileToUse);
        } else {
            shippedFileToUse = shippedFile;
        }

        shippedFileToUse.setAlgorithm(checksum.getAlgorithm());
        shippedFileToUse.setChecksum(checksum.getValue());
        return shippedFileToUse;
    }

    /**
     * Returns true if all of the {@link Confirmation} instances for the specified
     * ticket have been confirmed.
     *
     * @param shippedFile
     *            the {@link ShippedFile} to check.
     *
     * @return true if all of the {@link Confirmation} instances for the specified
     *         ticket have been confirmed.
     */
    private Date isVerified(final ShippedFile shippedFile) {
        if (null == shippedFile.getWhenVerificationStarted()) {
            return null;
        }
        if (null != shippedFile.getWhenVerificationCompleted()) {
            return shippedFile.getWhenVerificationCompleted();
        }
        final TypedQuery<Long> query = entityManager.createNamedQuery("getUnconfirmedCountByShippedFile",
                                                                      Long.class);
        query.setParameter("shippedFile",
                           shippedFile);
        final Long unconfirmedCount = query.getSingleResult();
        if (0 != unconfirmedCount) {
            return null;
        }
        final Date now = new Date();
        shippedFile.setWhenVerificationCompleted(now);
        retryManager.cancel(shippedFile.getTicketIdentity());
        return now;
    }

    /**
     * Prepares this object to manager the verification for the supplied ticket.
     *
     * @param ticket
     *            the identity of the ticket to be verified.
     * @param checksum
     *            the {@link ChecksumElement} associated with the specified ticket.
     * @param transfers
     *            the collection of outbound transfers whose delivery will need to
     *            be confirmed.
     *
     * @throws IOException
     *             if the transfer file can not be found or read.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void prepare(final String ticket,
                        final ChecksumElement checksum,
                        final Collection<String> transfers) throws IOException {
        int shippedKey = Integer.parseInt(ticket);
        final ShippedFile shippedFileToUse = getShippedFile(shippedKey,
                                                            checksum);
        final List<Confirmation> confirmations = shippedFileToUse.getConfirmations();
        final List<Confirmation> confirmationsToUse;
        if (null == confirmations) {
            confirmationsToUse = new ArrayList<>();
            shippedFileToUse.setConfirmations(confirmationsToUse);
        } else {
            confirmationsToUse = confirmations;
        }
        final List<Confirmation> confirmationsToBeRemoved = new ArrayList<>(confirmationsToUse);
        for (String transfer : transfers) {
            final OutboundTransfer outboundTransfer = neighborhoodManager.getOutboundTransfer(transfer);
            if (null == outboundTransfer) {
                LOG.error("No such outbound transfer as \"" + transfer
                          + "\", no confirm will be attempted");
            } else {
                final String neighbor = outboundTransfer.getNeighbor();
                final Confirmation confirmation = findConfirmation(neighbor,
                                                                   confirmationsToUse);
                if (null == confirmation) {
                    try {
                        final TypedQuery<KnownNeighbor> query = entityManager.createNamedQuery("getNeighborByName",
                                                                                               KnownNeighbor.class);
                        query.setParameter("name",
                                           neighbor);
                        final KnownNeighbor knownNeighbor = query.getSingleResult();
                        final Confirmation created = new Confirmation(shippedFileToUse,
                                                                      knownNeighbor);
                        entityManager.persist(created);
                        confirmationsToUse.add(created);
                    } catch (NoResultException e) {
                        throw new IllegalArgumentException("No such neighbor as \"" + neighbor
                                                           + "\"");
                    } catch (NonUniqueResultException e) {
                        throw new IllegalStateException("Two or more neighbors match \"" + neighbor
                                                        + "\"");
                    }
                } else {
                    confirmationsToBeRemoved.remove(confirmation);
                    confirmation.reset();
                }
            }
        }
        for (Confirmation confirmation : confirmationsToBeRemoved) {
            entityManager.remove(confirmation);
        }
        shippedFileToUse.setWhenVerificationStarted(null);
        abandonPreviousVerifications(shippedKey);
    }

    /**
     * Confirms that the specified ticket has been successfully delivered to the
     * supplied neighbor.
     *
     * @param ticket
     *            the identity of the ticket that is to be confirmed.
     * @param neighbor
     *            the neighbor from which the confirmation has been received.
     *
     * @return the time that delivery was first confirmed for this neighbor.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Date reconfirmNeighborDelivery(final String neighbor,
                                          final String ticket) {
        final TypedQuery<Confirmation> query = entityManager.createNamedQuery("getConfirmationByTicketAndDestination",
                                                                              Confirmation.class);
        query.setParameter("ticket",
                           Integer.parseInt(ticket));
        query.setParameter("destination",
                           neighbor);
        try {
            final Confirmation knownConfirmation = query.getSingleResult();
            final Date whenConfirmed = knownConfirmation.getWhenConfirmed();
            if (null == whenConfirmed) {
                return null;
            }
            final ShippedFile shippedFile = knownConfirmation.getShippedFile();
            if (null == shippedFile.getWhenVerificationCompleted()) {
                return null;
            }
            retryManager.cancel(shippedFile.getTicketIdentity());
            return whenConfirmed;
        } catch (NoResultException e) {
            throw new IllegalArgumentException("No Confirmation exists for ticket \"" + ticket
                                               + "\" to \""
                                               + neighbor
                                               + "\"");
        } catch (NonUniqueResultException e) {
            throw new IllegalStateException("Two or more Confirmations exists for ticket \"" + ticket
                                            + "\" to \""
                                            + neighbor
                                            + "\"");
        }
    }

    // static member methods (alphabetic)

    /**
     * Starts the verification that the supplied ticket's transfers have all been
     * successfully confirmed.
     *
     * @param ticket
     *            the identity of the ticket whose verification should be started.
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void start(String ticket) {
        final ShippedFile shippedFile = entityManager.find(ShippedFile.class,
                                                           Integer.parseInt(ticket));
        if (null != shippedFile.getWhenVerificationStarted()) {
            return;
        }
        shippedFile.setWhenVerificationStarted(new Date());
        if (null == shippedFile.getWhenVerificationCompleted() && null != isVerified(shippedFile)) {
            LOG.info("Verified delivery of \"" + ticket
                     + "\" to all destinations");
        }
    }

    // public static void main(String args[]) {}
}
