package gov.lbl.nest.spade.services;

import java.util.Collection;
import java.util.List;

import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.config.Neighbor;
import gov.lbl.nest.spade.config.OutboundTransfer;

/**
 * This interface is used to access information about the neighbouring
 * applications of this application.
 *
 * @author patton
 */
public interface NeighborhoodManager {

    /**
     * The host to use in an email when no host is specified.
     */
    static String DEFAULT_HOST = "localhost";

    /**
     * Attempts to read the Confirmable information from this neighbor.
     *
     * @param neighbor
     *            the neighbor whose confirmables will attempt to be read.
     *
     * @return the list of {@link DigestChange} instance that container all the new
     *         confirmables since that last call to this method.
     */
    List<? extends DigestChange> getConfirmables(final Neighbor neighbor);

    /**
     * Returns the directory associated with deliveries from a neighboring SPADE
     * deployment.
     *
     * @param neighbor
     *            the name of the neighboring SPADE deployment.
     *
     * @return the directory associate with the supplied email.
     */
    String getDirectory(String neighbor);

    /**
     * Returns the collection of {@link Neighbor} instances that make up the
     * neighborhood.
     *
     * @return the collection of {@link Neighbor} instances that make up the
     *         neighborhood.
     */
    Collection<Neighbor> getNeighbourhood();

    /**
     * Returns the {@link OutboundTransfer} instance that matches the name, or
     * <code>null</code> otherwise.
     *
     * @param name
     *            the name of the {@link OutboundTransfer} instance to return.
     *
     * @return the {@link OutboundTransfer} instance that matches the name, or
     *         <code>null</code> otherwise.
     */
    OutboundTransfer getOutboundTransfer(String name);

    /**
     * Returns the {@link Neighbor} instance, if any, representing this deployment
     * of the application.
     *
     * @return the {@link Neighbor} instance, if any, representing this deployment
     *         of the application.
     */
    Neighbor getSelf();
}
