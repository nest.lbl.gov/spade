package gov.lbl.nest.spade.services.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.watching.Detail;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.ejb.Bin;
import gov.lbl.nest.spade.ejb.Confirmation;
import gov.lbl.nest.spade.ejb.Dispatch;
import gov.lbl.nest.spade.ejb.KnownNeighbor;
import gov.lbl.nest.spade.ejb.KnownRegistration;
import gov.lbl.nest.spade.ejb.ShippedFile;
import gov.lbl.nest.spade.ejb.TicketedFile;
import gov.lbl.nest.spade.interfaces.SpadeDB;
import gov.lbl.nest.spade.rs.Slice;
import gov.lbl.nest.spade.services.Bookkeeping;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.xml.bind.DatatypeConverter;

/**
 * This is the default implementation of the {@link Bookkeeping} interface.
 *
 * @author patton
 */
@Stateless
public class BookkeepingImpl implements
                             Bookkeeping {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(TicketManagerImpl.class);

    /**
     * {@link List} to return if <code>null</code> is returned as the list of
     * {@link ShippedFile} instances.
     */
    private final static List<DigestChange> NO_TICKETED_FILES = new ArrayList<>(0);

    /**
     * The name used in the digest to identify the bytes transferred by a completed
     * dispatch.
     */
    final public static String PAYLOAD_BYTES = "payload_bytes";

    /**
     * The time between attempts to access the DB to see if an entry has appear in
     * the DB.
     */
    private static final long RETRY_INTERVAL = 100L;

    /**
     * The total time out, in milliseconds, that this object will wait for entries
     * to appear in the database.
     */
    private static final long TIMEOUT_WAITING_FOR_DB = 30000L;

    /**
     * The name used in the digest to identify the time by a dispatch completed.
     */
    final public static String WHEN_COMPLETED = "when_completed";

    /**
     * The format for date queries.
     */
    private static final String DATE_FORMAT = "yyyy-MM-dd";

    // private static member data

    /**
     * The formatter for date supplied to this class as Strings.
     */
    private static final DateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT);

    /**
     * The maximum allowed year for {@link DatatypeConverter} result (to avoid DB
     * lookup issues).
     */
    private static final Date MAXIMUM_DATE_TIME;

    static {
        Date max = null;
        try {
            max = DATE_FORMATTER.parse("10000-01-01");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        MAXIMUM_DATE_TIME = max;
    }

    /**
     * The dataLimit named query to use if none is provided.
     */
    private final String DEFAULT_DATA_LIMIT = "firstPlacedDate";

    // private instance member data

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @SpadeDB
    private EntityManager entityManager;

    /**
     * The {@link LoadedNeighborhood} used by this object.
     */
    @Inject
    private LoadedNeighborhood loadedNeighborhood;

    /**
     * The {@link LoadedRegistrations} used by this object.
     */
    @Inject
    private LoadedRegistrations loadedRegistrations;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected BookkeepingImpl() {
    }

    /**
     * Creates an instance for this class for test purposes only.
     *
     * @param entityManager
     *            the {@link EntityManager} instance used by this object.
     */
    public BookkeepingImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    // instance member method (alphabetic)

    @Override
    public boolean abandon(String ticket) {
        final TypedQuery<Dispatch> query = entityManager.createNamedQuery("getOpenDispatchByTicket",
                                                                          Dispatch.class);
        return abandonDispatches(ticket,
                                 query);
    }

    @Override
    public boolean abandon(String ticket,
                           String destination) {
        final TypedQuery<Dispatch> query = entityManager.createNamedQuery("getOpenDispatchByTicketAndDestination",
                                                                          Dispatch.class);
        query.setParameter("destination",
                           destination);
        /**
         * It is possible that not all Dispatched for this ticket/destination have been
         * closed, so abandon ALL open Dispatches.
         */
        return abandonDispatches(ticket,
                                 query);
    }

    private boolean abandonDispatches(String ticket,
                                      TypedQuery<Dispatch> query) {
        query.setParameter("ticket",
                           Integer.parseInt(ticket));
        final List<Dispatch> dispatches = query.getResultList();
        if (null != dispatches) {
            for (Dispatch dispatch : dispatches) {
                dispatch.setWhenAbandoned(new Date());
            }
            return true;
        }
        return false;
    }

    /**
     * Creates a {@link Digest} instance from the supplied {@link TicketedFile}.
     *
     *
     * @param ticketedFile
     *            the {@link TicketedFile} from which to create the {@link Digest}
     *            instance.
     *
     * @return the newly created {@link Digest} instance from the supplied
     *         {@link TicketedFile}
     */
    private Digest createHistory(final TicketedFile ticketedFile) {
        if (null == ticketedFile) {
            return null;
        }
        final List<DigestChange> timedItems = new ArrayList<>();
        final Date whenTicketed = ticketedFile.getWhenTicketed();
        if (null != whenTicketed) {
            timedItems.add(new DigestChange("Ticketed",
                                            whenTicketed));
        }
        final Date whenBinarySet = ticketedFile.getWhenBinarySet();
        if (null != whenBinarySet) {
            timedItems.add(new DigestChange("Binary File",
                                            whenBinarySet));
        }
        final Date whenMetadataSet = ticketedFile.getWhenMetadataSet();
        if (null != whenMetadataSet) {
            timedItems.add(new DigestChange("Metadata File",
                                            whenMetadataSet));
        }
        final Date whenCompressedSet = ticketedFile.getWhenCompressedSet();
        if (null != whenCompressedSet) {
            timedItems.add(new DigestChange("Compressed File",
                                            whenCompressedSet));
        }
        if (null != ticketedFile.getWhenPackedSet()) {
            timedItems.add(new DigestChange("Packed File",
                                            ticketedFile.getWhenPackedSet()));
        }
        final ShippedFile shippedFile = entityManager.find(ShippedFile.class,
                                                           Integer.parseInt(ticketedFile.getTicketIdentity()));
        if (null != shippedFile) {
            final Date whenVerificationStarted = shippedFile.getWhenVerificationStarted();
            if (null != whenVerificationStarted) {
                timedItems.add(new DigestChange("Verification Started",
                                                whenVerificationStarted));
            }
        }
        final Date whenPlaced = ticketedFile.getWhenPlaced();
        if (null != whenPlaced) {
            timedItems.add(new DigestChange("Placed in Warehouse",
                                            whenPlaced));
        }
        final Date whenArchived = ticketedFile.getWhenArchived();
        if (null != whenArchived) {
            timedItems.add(new DigestChange("Archived File",
                                            whenArchived));
        }
        if (null != shippedFile) {
            final Date whenVerificationCompleted = shippedFile.getWhenVerificationCompleted();
            if (null != whenVerificationCompleted) {
                timedItems.add(new DigestChange("Verification Completed",
                                                whenVerificationCompleted));
            }
            final Date whenConfirmable = shippedFile.getWhenConfirmable();
            if (null != whenConfirmable) {
                timedItems.add(new DigestChange("Confirmable",
                                                whenConfirmable));
            }
        }
        if (null != ticketedFile.getWhenFlushed()) {
            timedItems.add(new DigestChange("Flushed From Cache",
                                            ticketedFile.getWhenFlushed()));
        }

        final TypedQuery<Dispatch> query = entityManager.createNamedQuery("getDispatchesByTicketedFile",
                                                                          Dispatch.class);
        query.setParameter("file",
                           ticketedFile);
        final List<Dispatch> dispatches = query.getResultList();
        if (null != dispatches) {
            for (Dispatch dispatch : dispatches) {
                final String neighbor = (dispatch.getDestination()).getName();
                final Date whenStarted = dispatch.getWhenStarted();
                if (null != whenStarted) {
                    timedItems.add(new DigestChange("*   Dispatch started to " + neighbor,
                                                    whenStarted));
                }
                final Date whenAbandoned = dispatch.getWhenAbandoned();
                if (null != whenAbandoned) {
                    timedItems.add(new DigestChange("*   Dispatch abandoned to " + neighbor,
                                                    whenAbandoned));
                }
                /**
                 * Note: the whenCompleted is not reported as is should be nearly identical to
                 * the confirmation.whenDelivered value. Note: The above is not true when there
                 * are multiple destinations, so put it back in.
                 */
                final Date whenCompleted = dispatch.getWhenCompleted();
                if (null != whenCompleted) {
                    timedItems.add(new DigestChange("*   Dispatch confirmed to " + neighbor,
                                                    whenCompleted));
                }
            }
        }
        if (null != shippedFile) {
            final List<Confirmation> confirmations = shippedFile.getConfirmations();
            if (null != confirmations) {
                for (Confirmation confirmation : confirmations) {
                    final String neighbor = (confirmation.getDestination()).getName();
                    final Date whenDelivered = confirmation.getWhenDelivered();
                    if (null != whenDelivered) {
                        timedItems.add(new DigestChange("*   Delivered to " + neighbor,
                                                        whenDelivered));
                    }
                    final Date whenConfirmed = confirmation.getWhenConfirmed();
                    if (null != whenConfirmed) {
                        timedItems.add(new DigestChange("*   Confirmed delivery to " + neighbor,
                                                        whenConfirmed));
                    }
                    final Date whenAbandoned = confirmation.getWhenAbandoned();
                    if (null != whenAbandoned) {
                        timedItems.add(new DigestChange("*   Abandoned delivered to " + neighbor,
                                                        whenAbandoned));
                    }
                }
            }
        }
        Collections.sort(timedItems,
                         new Comparator<DigestChange>() {

                             @Override
                             public int compare(DigestChange arg0,
                                                DigestChange arg1) {
                                 final Date d0 = arg0.getTime();
                                 final Date d1 = arg1.getTime();
                                 final long t0 = d0.getTime();
                                 final long t1 = d1.getTime();
                                 if (t0 == t1) {
                                     return 0;
                                 }
                                 if (t0 > t1) {
                                     return -1;
                                 }
                                 return 1;
                             }
                         });
        final Digest result = new Digest("Ticket #" + ticketedFile.getTicketIdentity()
                                         + " ("
                                         + ticketedFile.getBundle()
                                         + ")",
                                         timedItems);
        return result;
    }

    @Override
    public List<DigestChange> getByTime(Stamped stamped,
                                        Date after,
                                        Date before,
                                        boolean reversed,
                                        int max,
                                        String neighbor,
                                        List<String> registrations) {
        KnownNeighbor knownNeighbor;
        if (null == neighbor) {
            knownNeighbor = null;
        } else {
            knownNeighbor = loadedNeighborhood.getKnownNeighbor(neighbor);
            if (null == knownNeighbor) {
                LOG.warn("Request for an neighbor \"" + neighbor
                         + "\" failed, and so will be skipped");
                return null;
            }
        }
        final List<? extends KnownRegistration> knownRegistrations;
        if (Stamped.TICKETED == stamped) {
            knownRegistrations = loadedRegistrations.getKnownRegistrations(knownNeighbor,
                                                                           registrations);
        } else if (Stamped.DISPATCHED == stamped || Stamped.TICKETED_OUTBOUND == stamped) {
            knownRegistrations = loadedRegistrations.getKnownDispatchingRegistrations(knownNeighbor,
                                                                                      registrations);
        } else {
            knownRegistrations = loadedRegistrations.getKnownReceivingRegistrations(knownNeighbor,
                                                                                    registrations);
        }

        /*
         * If there has been a request to filter on registration but none of them are
         * valid then return nothing, otherwise the result would be to return everything
         * which is clearly not the intention.
         */
        if (null != registrations && !registrations.isEmpty()
            && knownRegistrations.isEmpty()) {
            return NO_TICKETED_FILES;
        }

        final String qualifiers;
        boolean noKnownRegistrations = null == knownRegistrations || knownRegistrations.isEmpty();
        if (null == knownNeighbor && noKnownRegistrations) {
            qualifiers = "";
        } else {
            if (null == knownNeighbor || Stamped.DISPATCHED != stamped) {
                qualifiers = "ByRegistration";
            } else if (noKnownRegistrations) {
                qualifiers = "ByDestination";
            } else {
                qualifiers = "ByRegistrationAndDestination";
            }
        }

        final String timing;
        final Date afterToUse;
        final Date beforeToUse;
        if (reversed) {
            if (null == after) {
                timing = "Before";
            } else {
                timing = "BetweenDesc";
            }
            afterToUse = after;
            if (null == before) {
                beforeToUse = MAXIMUM_DATE_TIME;
            } else {
                beforeToUse = before;
            }
        } else {
            if (null == before) {
                timing = "After";
            } else {
                timing = "BetweenAsc";
            }
            if (null == after) {
                afterToUse = new Date(0);
            } else {
                afterToUse = after;
            }
            beforeToUse = before;
        }

        final String category;
        if (Stamped.ARCHIVED == stamped) {
            category = "archived";
        } else if (Stamped.PLACED == stamped) {
            category = "placed";
        } else if (Stamped.TICKETED == stamped || Stamped.TICKETED_INBOUND == stamped
                   || Stamped.TICKETED_OUTBOUND == stamped) {
            category = "ticketed";
        } else if (Stamped.DISPATCHED == stamped) {
            category = "completedDispatch";
            final List<Dispatch> completedDispatches = getCompletedDispatchByTime(category + qualifiers
                                                                                  + timing,
                                                                                  afterToUse,
                                                                                  beforeToUse,
                                                                                  max,
                                                                                  knownNeighbor,
                                                                                  knownRegistrations);
            if (null == completedDispatches) {
                return NO_TICKETED_FILES;
            }

            final List<DigestChange> result = new ArrayList<>(completedDispatches.size());
            for (Dispatch completedDispatch : completedDispatches) {
                final TicketedFile ticketedFile = completedDispatch.getTicketedFile();
                final Date when = completedDispatch.getWhenStarted();
                final List<Detail> details = new ArrayList<>();
                details.add(new Detail(WHEN_COMPLETED,
                                       new Date((completedDispatch.getWhenCompleted()).getTime())));
                details.add(new Detail(PAYLOAD_BYTES,
                                       ticketedFile.getBinarySize()));
                result.add(new DigestChange(ticketedFile.getBundle(),
                                            when,
                                            details));
            }
            return result;
        } else {
            // Should never get here is all enumerations as dealt with.
            throw new IllegalArgumentException();
        }

        final List<TicketedFile> ticketedFiles = getTicketedFilesByTime(category + qualifiers
                                                                        + timing,
                                                                        afterToUse,
                                                                        beforeToUse,
                                                                        max,
                                                                        knownRegistrations);
        if (null == ticketedFiles) {
            return NO_TICKETED_FILES;
        }

        final List<DigestChange> result = new ArrayList<>(ticketedFiles.size());
        for (TicketedFile ticketedFile : ticketedFiles) {
            final Date when;
            if (Bookkeeping.Stamped.ARCHIVED == stamped) {
                when = ticketedFile.getWhenArchived();
            } else if (Bookkeeping.Stamped.PLACED == stamped) {
                when = ticketedFile.getWhenPlaced();
            } else if (Bookkeeping.Stamped.TICKETED == stamped || Stamped.TICKETED_INBOUND == stamped
                       || Stamped.TICKETED_OUTBOUND == stamped) {
                when = ticketedFile.getWhenTicketed();
            } else {
                // Should never get here is all enumerations as dealt with.o
                throw new IllegalArgumentException();
            }
            result.add(new DigestChange(ticketedFile.getBundle(),
                                        when));
        }
        return result;
    }

    /**
     * Returns the sequence to {@link TicketedFile} instance that match the
     * specified criteria.
     *
     * @param namedQuery
     *            the name of the query to use when no "before" is specified.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     *
     * @return the sequence to {@link TicketedFile} instance that match the
     *         specified criteria.
     */
    private List<Dispatch> getCompletedDispatchByTime(final String namedQuery,
                                                      final Date after,
                                                      final Date before,
                                                      final int max,
                                                      final KnownNeighbor neighbor,
                                                      final List<? extends KnownRegistration> registrations) {
        final TypedQuery<Dispatch> query = entityManager.createNamedQuery(namedQuery,
                                                                          Dispatch.class);
        if (null != after) {
            query.setParameter("after",
                               after);
        }
        if (null != before) {
            query.setParameter("before",
                               before);
        }
        if (null != neighbor) {
            query.setParameter("neighbor",
                               neighbor);
        }
        if (null != registrations && !registrations.isEmpty()) {
            query.setParameter("registrations",
                               registrations);
        }
        if (max > 0) {
            query.setMaxResults(max);
        }
        final List<Dispatch> completedDispatches = query.getResultList();
        return completedDispatches;
    }

    @Override
    @Lock(LockType.READ)
    public Digest getHistoryByTicket(final String ticket) {
        final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                             Integer.parseInt(ticket));
        return createHistory(ticketedFile);
    }

    @Override
    public List<Slice> getPlacementSlices(boolean fineBins,
                                          Integer span,
                                          Date after) {
        if (fineBins) {
            return getSlices("getFineBinnedPlaced",
                             "firstPlacedDate",
                             TicketedFile.FINE_BINNING,
                             span,
                             after);
        }
        return getSlices("getCourseBinnedPlaced",
                         "firstPlacedDate",
                         TicketedFile.COURSE_BINNING,
                         span,
                         after);
    }

    @Override
    public List<Slice> getShippingSlices(boolean fineBins,
                                         Integer span,
                                         Date after) {
        if (fineBins) {
            return getSlices("getFineBinnedCompleted",
                             "firstCompletedDate",
                             TicketedFile.FINE_BINNING,
                             span,
                             after);
        }
        return getSlices("getCourseBinnedCompleted",
                         "firstCompletedDate",
                         TicketedFile.COURSE_BINNING,
                         span,
                         after);
    }

    private List<Slice> getSlices(String namedQuery,
                                  String dateLimit,
                                  long binSize,
                                  Integer span,
                                  Date after) {
        final TypedQuery<Bin> query = entityManager.createNamedQuery(namedQuery,
                                                                     Bin.class);
        final long nowInSeconds = TimeUnit.SECONDS.convert(new Date().getTime(),
                                                           TimeUnit.MILLISECONDS);
        // Upper edge of Slice that includes "now"
        final long nowBin = (1 + (nowInSeconds / binSize)) * binSize;
        // Lower edge of Slice that includes "after"
        final long afterBin;
        // Upper edge of Slice that includes "before"
        final long beforeBin;
        if (null == after) {
            if (null == span) {
                final TypedQuery<Long> query2;
                final String dateLimitToUse;
                if (null == dateLimit) {
                    dateLimitToUse = DEFAULT_DATA_LIMIT;
                } else {
                    dateLimitToUse = dateLimit;
                }
                query2 = entityManager.createNamedQuery(dateLimitToUse,
                                                        Long.class);
                afterBin = (query2.getSingleResult()).longValue();
            } else {
                afterBin = nowBin - ((1 + span) * binSize);
            }
            beforeBin = nowBin;
        } else {
            final long afterInSeconds = TimeUnit.SECONDS.convert(after.getTime(),
                                                                 TimeUnit.MILLISECONDS);
            afterBin = (afterInSeconds / binSize) * binSize;
            if (null == span) {
                beforeBin = nowInSeconds;
            } else {
                beforeBin = afterBin + ((1 + span) * binSize);
            }
        }
        final Date afterDate = new Date(TimeUnit.MILLISECONDS.convert(afterBin,
                                                                      TimeUnit.SECONDS));
        query.setParameter("after",
                           afterDate);
        final Date beforeDate = new Date(TimeUnit.MILLISECONDS.convert(beforeBin,
                                                                       TimeUnit.SECONDS));
        query.setParameter("before",
                           beforeDate);
        final List<Bin> bins = query.getResultList();

        final List<Slice> result = new ArrayList<>();

        if (null == bins || bins.isEmpty()) {
            result.add(new Slice(afterDate,
                                 0,
                                 0));
            result.add(new Slice(beforeDate,
                                 0,
                                 0));
            return result;
        }

        if (null == span) {
            if (null == bins || bins.isEmpty()) {
                return result;
            }
        }

        final Iterator<Bin> iterator = bins.iterator();
        Bin bin;
        if (iterator.hasNext()) {
            bin = iterator.next();
        } else {
            bin = null;
        }

        for (long edge = afterBin;
             edge != beforeBin;
             edge += binSize) {
            final Slice slice;
            final Date dateTime = new Date(TimeUnit.MILLISECONDS.convert(edge,
                                                                         TimeUnit.SECONDS));
            if (null != bin && edge == bin.getUpperEdge()) {
                slice = new Slice(dateTime,
                                  bin.getSum(),
                                  bin.getCount());
                if (iterator.hasNext()) {
                    bin = iterator.next();
                } else {
                    bin = null;
                }
            } else {
                slice = new Slice(dateTime,
                                  0,
                                  0);
            }
            result.add(slice);
        }
        return result;
    }

    private TicketedFile getTicketedFile(final String ticket) {
        /*
         * There is a timing issue when more than one small file is found in the drop
         * boxes. This loop allows for the ticket to appear in the database.
         */
        final long end = new Date().getTime() + TIMEOUT_WAITING_FOR_DB;
        TicketedFile ticketedFile;
        do {
            ticketedFile = entityManager.find(TicketedFile.class,
                                              Integer.parseInt(ticket));
            try {
                Thread.sleep(RETRY_INTERVAL);
            } catch (InterruptedException e) {
                // Ignore an try again.
            }
        } while (null == ticketedFile && end > new Date().getTime());
        return ticketedFile;
    }

    private List<TicketedFile> getTicketedFilesByTime(String namedQuery,
                                                      Date after,
                                                      Date before,
                                                      int max,
                                                      List<? extends KnownRegistration> registrations) {
        final TypedQuery<TicketedFile> query = entityManager.createNamedQuery(namedQuery,
                                                                              TicketedFile.class);
        if (null != after) {
            query.setParameter("after",
                               after);
        }
        if (null != before) {
            query.setParameter("before",
                               before);
        }
        if (null != registrations && !registrations.isEmpty()) {
            query.setParameter("registrations",
                               registrations);
        }
        if (max > 0) {
            query.setMaxResults(max);
        }
        final List<TicketedFile> ticketedFile = query.getResultList();
        return ticketedFile;
    }

    @Override
    public void sending(String ticket,
                        String destination) {
        /**
         * It is possible that not all Dispatched for this ticket/destination have been
         * closed, so close any open ones before creating a new one.
         */
        abandon(ticket,
                destination);
        final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                             Integer.parseInt(ticket));
        if (null != ticketedFile) {
            final KnownNeighbor knownNeighbor = loadedNeighborhood.getKnownNeighbor(destination);
            if (null == knownNeighbor) {
                LOG.warn("Request for an neighbor \"" + destination
                         + "\" failed, and so will be skipped");
                return;
            }
            final Dispatch dispatch = new Dispatch(ticketedFile,
                                                   knownNeighbor);
            entityManager.persist(dispatch);
        }

    }

    @Override
    public void sent(String ticket,
                     String destination) {
        final TypedQuery<Dispatch> query = entityManager.createNamedQuery("getOpenDispatchByTicketAndDestination",
                                                                          Dispatch.class);
        query.setParameter("ticket",
                           Integer.parseInt(ticket));
        query.setParameter("destination",
                           destination);
        final List<Dispatch> dispatches = query.getResultList();
        if (null != dispatches) {
            /**
             * It is possible that not all Dispatched for this ticket/destination have been
             * closed. The list of dispatches is time order so the first one is most recent,
             * and therefore the Dispatch that has succeeded. Thus the first is completed
             * and the rest abandoned.
             */
            final int size = dispatches.size();
            if (size > 0) {
                (dispatches.get(0)).setWhenCompleted(new Date());
            }
            if (size > 1) {
                for (Dispatch dispatch : dispatches.subList(1,
                                                            size)) {
                    dispatch.setWhenAbandoned(new Date());
                }
            }
        }
    }

    @Override
    public void setArchived(String ticket,
                            String suffix) {
        TicketedFile ticketedFile = getTicketedFile(ticket);

        if (null == ticketedFile) {
            LOG.warn("Could not set archive dtails for \"" + ticket
                     + "\"");
            return;
        }
        ticketedFile.setArchiveSuffix(suffix);
        ticketedFile.setWhenArchived(new Date());
    }

    @Override
    public void setBinarySize(final String ticket,
                              final long size) {
        TicketedFile ticketedFile = getTicketedFile(ticket);

        if (null == ticketedFile) {
            LOG.warn("Could not set binary size for \"" + ticket
                     + "\" to be "
                     + size);
            return;
        }
        Long recordedSize = ticketedFile.getBinarySize();
        if (null != recordedSize && size == recordedSize.longValue()) {
            return;
        }
        ticketedFile.setBinarySize(size);
        ticketedFile.setWhenBinarySet(new Date());
    }

    @Override
    public void setCompressedSize(String ticket,
                                  long size) {
        TicketedFile ticketedFile = getTicketedFile(ticket);

        if (null == ticketedFile) {
            LOG.warn("Could not set compressed size for \"" + ticket
                     + "\" to be "
                     + size);
            return;
        }
        Long recordedSize = ticketedFile.getCompressedSize();
        if (null != recordedSize && size == recordedSize.longValue()) {
            return;
        }
        ticketedFile.setCompressedSize(size);
        ticketedFile.setWhenCompressedSet(new Date());
    }

    @Override
    public void setFlushed(String ticket) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setMetadataSize(final String ticket,
                                final long size) {
        final TicketedFile ticketedFile = getTicketedFile(ticket);
        if (null == ticketedFile) {
            LOG.warn("Could not set metadata size for \"" + ticket
                     + "\" to be "
                     + size);
            return;
        }
        ticketedFile.setMetadataSize(size);
        ticketedFile.setWhenMetadataSet(new Date());
    }

    @Override
    public void setPackedSize(String ticket,
                              long size) {
        TicketedFile ticketedFile = getTicketedFile(ticket);

        if (null == ticketedFile) {
            LOG.warn("Could not set packed size for \"" + ticket
                     + "\" to be "
                     + size);
            return;
        }
        Long recordedSize = ticketedFile.getPackedSize();
        if (null != recordedSize && size == recordedSize.longValue()) {
            return;
        }
        ticketedFile.setPackedSize(size);
        ticketedFile.setWhenPackedSet(new Date());
    }

    @Override
    public void setPlacement(final String ticket,
                             final Object warehouseId,
                             final long size) {
        final TicketedFile ticketedFile = entityManager.find(TicketedFile.class,
                                                             Integer.parseInt(ticket));
        ticketedFile.setPlacedSize(size);
        ticketedFile.setPlacementId((String) warehouseId);
        ticketedFile.setWhenPlaced(new Date());
    }

    @Override
    public void setRestored(final String ticket) {
        // TODO Auto-generated method stub

    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
