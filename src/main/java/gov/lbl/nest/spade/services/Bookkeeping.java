package gov.lbl.nest.spade.services;

import java.util.Date;
import java.util.List;

import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.rs.Slice;

/**
 * This interface defines the interaction with the bookkeeping DB.
 *
 * @author patton
 */
public interface Bookkeeping {

    /**
     * The categories of time stamps that can be queried.
     *
     * @author patton
     */
    enum Stamped {

                  /**
                   * The archive category.
                   */
                  ARCHIVED,

                  /**
                   * The dispatched category.
                   */
                  DISPATCHED,

                  /**
                   * The placed category.
                   */
                  PLACED,

                  /**
                   * The ticketed category.
                   */
                  TICKETED,

                  /**
                   * The inbound category for ticketed files.
                   */
                  TICKETED_INBOUND,

                  /**
                   * The outbound category for ticketed files.
                   */
                  TICKETED_OUTBOUND
    }

    /**
     * Records that all current dispatches of the specified ticket have been
     * abandoned.
     *
     * @param ticket
     *            the identity of the ticket being abandoned.
     *
     * @return true is one or more dispatches has been abandoned.
     *
     */
    boolean abandon(String ticket);

    /**
     * Rcords that all current dispatches of the specified ticket to a destination
     * have been abandoned.
     *
     * @param ticket
     *            the identity of the ticket being abandoned.
     * @param destination
     *            the identity of the destination application.
     *
     * @return true is one or more dispatches has been abandoned.
     */
    boolean abandon(final String ticket,
                    final String destination);

    /**
     * Returns a list of {@link DigestChange} instances that have been time-stamped
     * with the specified {@link Stamped}.
     *
     * @param stamped
     *            the {@link Stamped} type to be returned.
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list. If <code>reversed</code> is <code>true</code> the
     *            only item after will be included.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>reversed</code> is <code>true</code> the items on
     *            or before will be included.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     * @param neighbor
     *            the neighbor for whom the confirmables should be returned,
     *            <code>null</code> implies all neighbors.
     * @param registrations
     *            the collection the names of {@link Registration} instance to
     *            included in the result.
     *
     * @return the list of {@link DigestChange} instances that have been placed into
     *         the warehouse between the specified dates.
     */
    List<DigestChange> getByTime(final Stamped stamped,
                                 final Date after,
                                 final Date before,
                                 boolean reversed,
                                 final int max,
                                 String neighbor,
                                 List<String> registrations);

    /**
     * Returns the {@link Digest} instance, if there is one, for the specified
     * ticket.
     *
     * @param ticket
     *            the identity of the ticket whose {@link Digest} should be
     *            returned.
     *
     * @return the {@link Digest} instance, if there is one, for the specified
     *         ticket.
     */
    Digest getHistoryByTicket(final String ticket);

    /**
     * Returns the sequence of placement {@link Slice} instances as specified by the
     * conditions.
     *
     * @param fineBins
     *            true if fine "epoch" bins should be used, otherwise course will be
     *            used.
     * @param span
     *            the limit, if any, of number of slices to include.
     * @param after
     *            the date and time from which to start the sequence. If
     *            <code>null</code> then this is set to "now" minus the time covered
     *            by the "span".
     *
     * @return the sequence of placement {@link Slice} instances as specified by the
     *         conditions.
     */
    List<Slice> getPlacementSlices(boolean fineBins,
                                   Integer span,
                                   Date after);

    /**
     * Returns the sequence of shipping {@link Slice} instances to all neighbors as
     * specified by the conditions.
     *
     * @param fineBins
     *            true if fine "epoch" bins should be used, otherwise course will be
     *            used.
     * @param span
     *            the limit, if any, of number of slices to include.
     * @param after
     *            the date and time from which to start the sequence. If
     *            <code>null</code> then this is set to "now" minus the time covered
     *            by the "span".
     *
     * @return the sequence of shipping {@link Slice} instances to all neighbors as
     *         specified by the conditions.
     */
    List<Slice> getShippingSlices(boolean fineBins,
                                  Integer span,
                                  Date after);

    /**
     * Records that the specified ticket is being sent to the supplied application.
     *
     * @param ticket
     *            the identity of the ticket that is being sent.
     * @param destination
     *            the email of the destination application.
     */
    void sending(final String ticket,
                 final String destination);

    /**
     * Records that the specified ticket has been sent to the supplied application.
     *
     * @param ticket
     *            the identity of the ticket that has been sent.
     * @param destination
     *            the email of the destination application.
     */
    void sent(final String ticket,
              final String destination);

    /**
     * Records the ticket was successfully archived.
     *
     * @param ticket
     *            the identity of the ticket that was successfully archived.
     * @param suffix
     *            the suffix used to create the archived file name.
     */
    void setArchived(final String ticket,
                     final String suffix);

    /**
     * Sets the binary file size for the specified ticket.
     *
     * @param ticket
     *            the identity of the ticket whose file size should be set.
     * @param size
     *            the size of the binary file for the specified ticket.
     */
    void setBinarySize(final String ticket,
                       final long size);

    /**
     * Sets the compressed file size for the specified ticket.
     *
     * @param ticket
     *            the identity of the ticket whose file size should be set.
     * @param size
     *            the size of the compressed for the specified ticket.
     */
    void setCompressedSize(final String ticket,
                           final long size);

    /**
     * Records that the specified ticket has been flushed from the cache.
     *
     * @param ticket
     *            the identity of the ticket that has been flushed.
     */
    void setFlushed(final String ticket);

    /**
     * Sets the metadata file size for the specified ticket.
     *
     * @param ticket
     *            the identity of the ticket whose file size should be set.
     * @param size
     *            the size of the metadata file for the specified ticket.
     */
    void setMetadataSize(final String ticket,
                         final long size);

    /**
     * Sets the wrapped file size for the specified ticket.
     *
     * @param ticket
     *            the identity of the ticket whose file size should be set.
     * @param size
     *            the size of the wrapped file for the specified ticket.
     */
    void setPackedSize(final String ticket,
                       final long size);

    /**
     * Sets the placement information for the specified ticket.
     *
     * @param ticket
     *            the identity of the ticket whose placement information should be
     *            set.
     * @param warehouseId
     *            the warehouse id that may be used retrieve the placed files from
     *            the warehouse.
     * @param size
     *            the total number of bytes of data placed in the warehouse for the
     *            specified ticket.
     */
    void setPlacement(final String ticket,
                      final Object warehouseId,
                      final long size);

    /**
     * Records the ticket was successfully restored.
     *
     * @param ticket
     *            the identity of the ticket that was successfully restored.
     */
    void setRestored(final String ticket);

}
