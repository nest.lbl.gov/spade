package gov.lbl.nest.spade.services;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.suspension.SuspendedException;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.registry.Registry;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.rs.Bundles;
import gov.lbl.nest.spade.rs.Slice;
import gov.lbl.nest.spade.rs.Tickets;
import gov.lbl.nest.spade.workflow.ActivityManager;
import gov.lbl.nest.spade.workflow.ActivityMonitor;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowDirector;
import gov.lbl.nest.spade.workflow.WorkflowsMonitor;

/**
 * This interface defines what the SPADE application can do.
 *
 * @author patton
 */
public interface Spade extends
                       Suspendable {

    /**
     * The version of the specification that this library implements.
     */
    static String SPECIFICATION = "5.0";

    /**
     * Marks the supplied colleciton of tickets as abandoned.
     * 
     * @param tickets
     *            the {@link Tickets} instance containing the collection of tickets
     *            to abandon.
     * 
     * @return the collection if ticket's that have been successfully abandoned.
     * 
     * @throws SuspendedException
     *             when the application is currently suspended.
     * @throws InitializingException
     *             when the application is still initializing.
     */
    Tickets abandon(Tickets tickets) throws SuspendedException,
                                     InitializingException;

    /**
     * Attempts to confirm the delivery of the bundles from the specified neighbor.
     *
     * @param neighbor
     *            the neighbor from which the confirmation has been received.
     * @param confirmations
     *            the confirmation information from the neighbor.
     *
     * @return a {@link Digest} instance listing the confirmations that now exist as
     *         a result of this method.
     */
    List<? extends DigestChange> delivery(String neighbor,
                                          List<? extends DigestChange> confirmations);

    /**
     * Scans the neighborhood for deliveries that can now be confirmed.
     *
     * @return a {@link Digest} instance listing the confirmations that now exist as
     *         a result of this method.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws InitializingException
     *             when the workflows are still initializing.
     * @throws InProgressException
     *             if the local scan is already in progress.
     */
    List<DigestChange> deliveryScan() throws SuspendedException,
                                      InitializingException,
                                      InProgressException;

    /**
     * Allow all workflow to "drain", by suspending the all workflows, so they will
     * reject any more attempts to start a new instance, but resuming all activities
     * in those workflow.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    void drain() throws InitializingException;

    /**
     * Returns the collection of names of all the bundles that currently active in
     * the application (waiting for an action is considered active).
     *
     * @return the collection of names of all the bundles that currently active in
     *         the application.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Bundles getActiveBundles() throws SuspendedException,
                               InitializingException;

    /**
     * Returns the {@link ActivityManager} for the specified task, or
     * <code>null</code> if there is not such task.
     *
     * @param workflow
     *            the name of the workflow that contains the named activity.
     * @param activity
     *            the name of the task whose {@link ActivityManager} should be
     *            returned.
     *
     * @return the {@link ActivityManager} for the named task, or <code>null</code>
     *         if there is not such task.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    ActivityManager getActivityManager(Workflow workflow,
                                       String activity) throws InitializingException;

    /**
     * Returns the {@link ActivityMonitor} for the specified task, or
     * <code>null</code> if there is not such task.
     *
     * @param workflow
     *            the name of the workflow that contains the named activity.
     * @param activity
     *            the name of the task whose {@link ActivityMonitor} should be
     *            returned.
     *
     * @return the {@link ActivityMonitor} for the named task, or <code>null</code>
     *         if there is not such task.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    ActivityMonitor getActivityMonitor(Workflow workflow,
                                       String activity) throws InitializingException;

    /**
     * Returns the names of all activities in the specified workflow.
     *
     * @param workflow
     *            the workflow whose activity names should be returned.
     *
     * @return the names of all activities in the specified workflow.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    List<? extends String> getActivityNames(Workflow workflow) throws InitializingException;

    /**
     * Returns a list of {@link DigestChange} instances for bundles that have been
     * archived.
     *
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list. If <code>reversed</code> is <code>true</code> the
     *            only item after will be included.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>reversed</code> is <code>true</code> the items on
     *            or before will be included.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     * @param neighbor
     *            the neighbor for whom the archive records should be returned,
     *            <code>null</code> implies all neighbors.
     * @param registrations
     *            the collection of names of registrations to included in the
     *            result.
     *
     * @return the list of {@link DigestChange} instances for bundles that have been
     *         archived.
     */
    List<DigestChange> getArchivedByTime(Date after,
                                         Date before,
                                         boolean reversed,
                                         int max,
                                         String neighbor,
                                         List<String> registrations);

    /**
     * Returns the collection of names of all the bundles that currently being
     * "blocked" by the application, that is to say they they can not be acted on.
     *
     * @return Returns the collection of names of all the bundles that currently
     *         being "blocked" by the application.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Bundles getBlockedBundles() throws SuspendedException,
                                InitializingException;

    /**
     * Returns the {@link Configuration} instance used by this application.
     *
     * @return the {@link Configuration} instance used by this application.
     */
    Configuration getConfiguration();

    /**
     * Returns a list of {@link DigestChange} instances for specified collation of
     * bundles that are ready be verified by the remote SPADE that delivered it.
     * 
     * @param bundles
     *            the collection of {@link Bundle} instances whose confirmable data
     *            should be included in the result,
     * 
     * @return the list of {@link DigestChange} instances for specified collation of
     *         bundles that are ready be verified by the remote SPADE that delivered
     *         it.
     */
    List<DigestChange> getConfirmableByBundle(List<String> bundles);

    /**
     * Returns a list of {@link DigestChange} instances for bundles that are ready
     * be verified by the remote SPADE that delivered it and which match the
     * supplied criteria.
     *
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list. If <code>reversed</code> is <code>true</code> the
     *            only item after will be included.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>reversed</code> is <code>true</code> the items on
     *            or before will be included.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     * @param neighbor
     *            the neighbor, if any, to which the confirmables were delivered.
     * @param registrations
     *            the collection of names of registrations to included in the
     *            result.
     *
     * @return the list of {@link DigestChange} instances for bundles that are ready
     *         be verified
     */
    List<DigestChange> getConfirmableByTime(Date after,
                                            Date before,
                                            boolean reversed,
                                            int max,
                                            String neighbor,
                                            List<String> registrations);

    /**
     * Returns a list of {@link DigestChange} instances for bundles whose delivery
     * to a single destinations has been verified (or all is not neighbor is
     * specified).
     *
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list. If <code>reversed</code> is <code>true</code> the
     *            only item after will be included.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>reversed</code> is <code>true</code> the items on
     *            or before will be included.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     * @param neighbor
     *            the neighbor, if any, to which the delivery was confirmed.
     * @param registrations
     *            the collection of names of registrations to included in the
     *            result.
     *
     * @return the list of {@link DigestChange} instances for bundles that are ready
     *         be verified
     */
    List<DigestChange> getConfirmedByTime(Date after,
                                          Date before,
                                          boolean reversed,
                                          int max,
                                          String neighbor,
                                          List<String> registrations);

    /**
     * Returns an {@link AtomicInteger} containing the number of current active
     * instances of the specified workflow.
     *
     * The returned {@link AtomicInteger} instance can be queried at any time. Also
     * it is notified every time this value changes so it can be used to detect
     * changes in this value by invoking its 'wait' method in a suitable loop.
     *
     * @param workflow
     *            the workflow whose counts should be returned.
     *
     * @return the {@link AtomicInteger} containing the number of current active
     *         instances of the specified workflow.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    AtomicInteger getCount(Workflow workflow) throws InitializingException;

    /**
     * Returns the {@link URI} to used as the default for internal scan requests.
     *
     * @return the {@link URI} to used as the default for internal scan requests.
     *
     * @throws URISyntaxException
     *             if the supplied URI is not syntactically correct.
     */
    URI getDefaultBaseUri() throws URISyntaxException;

    /**
     * Returns a list of {@link DigestChange} instances for successful dispatches of
     * Bundles to one or more their destinations
     *
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list. If <code>reversed</code> is <code>true</code> the
     *            only item after will be included.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>reversed</code> is <code>true</code> the items on
     *            or before will be included.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     * @param neighbor
     *            the neighbor for whom the archive records should be returned,
     *            <code>null</code> implies all neighbors.
     * @param registrations
     *            the collection of names of registrations to included in the
     *            result.
     *
     * @return the list of {@link DigestChange} instances for bundles whose delivery
     *         to all destinations has been verified.
     */
    List<DigestChange> getDispatchedByTime(Date after,
                                           Date before,
                                           boolean reversed,
                                           int max,
                                           String neighbor,
                                           List<String> registrations);

    /**
     * Returns a {@link Digest} instance containing all of the timestamps associated
     * with the specified ticket.
     *
     * @param ticket
     *            the identity of the ticket that has been sent.
     *
     * @return The {@link Digest} instance containing all of the timestamps
     *         associated with the specified ticket.
     */
    Digest getHistoryByTicket(String ticket);

    /**
     * Returns a list of {@link DigestChange} instances for bundles that have been
     * placed in the local warehouse.
     *
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list. If <code>reversed</code> is <code>true</code> the
     *            only item after will be included.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>reversed</code> is <code>true</code> the items on
     *            or before will be included.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     * @param neighbor
     *            the neighbor for whom the confirmables should be returned,
     *            <code>null</code> implies all neighbors.
     * @param registrations
     *            the collection of names of registrations to included in the
     *            result.
     *
     * @return the list of {@link DigestChange} instances for bundles that have been
     *         placed in the local warehouse.
     */
    List<DigestChange> getPlacedByTime(Date after,
                                       Date before,
                                       boolean reversed,
                                       int max,
                                       String neighbor,
                                       List<String> registrations);

    /**
     * Returns the identity of the specified bundle within the context of the
     * warehouse manager.
     *
     * @param bundle
     *            the bundle whose warehouse id should be returned.
     *
     * @return the identity of the specified bundle within the context of the
     *         warehouse manager.
     */
    String getPlacementId(String bundle);

    /**
     * Returns a collection of identities of the specified bundles within the
     * context of the warehouse manager.
     *
     * @param bundles
     *            the bundles whose warehouse ids should be returned.
     *
     * @return the collection of identities of the specified bundles within the
     *         context of the warehouse manager.
     */
    List<String> getPlacementIds(List<String> bundles);

    /**
     * Returns the sequence of placement {@link Slice} instances as specified by the
     * conditions.
     *
     * @param fineBins
     *            true if fine "epoch" bins should be used, otherwise course will be
     *            used.
     * @param span
     *            the limit, if any, of number of slices to include.
     * @param after
     *            the date and time from which to start the sequence. If
     *            <code>null</code> then this is set to "now" minus the time covered
     *            by the "span".
     *
     * @return the sequence of placement {@link Slice} instances as specified by the
     *         conditions.
     */
    List<Slice> getPlacementSlices(boolean fineBins,
                                   Integer span,
                                   Date after);

    /**
     * Returns the {@link Collection} of ticket identities that are currently queued
     * for retry.
     *
     * @param cushion
     *            the time in seconds that the identity must have been in the queue
     *            in order to be returned.
     * @param unit
     *            the time unit of the cushion argument
     *
     * @return the {@link Collection} of ticket identities that are currently queued
     *         for retry.
     */
    List<String> getPostponed(long cushion,
                              TimeUnit unit);

    /**
     * Returns a {@link Registry} instance containing the set of known
     * registrations.
     *
     * @param registrations
     *            the collections of registrations to include in the returned
     *            {@link Registry} instance. A <code>null</code> or empty list means
     *            all registrations should be included.
     *
     * @return the {@link Registry} instance containing the set of known
     *         registrations.
     */
    Registry getRegistrations(List<String> registrations);

    /**
     * Returns the sequence of shipping {@link Slice} instances to all neighbors as
     * specified by the conditions.
     *
     * @param fineBins
     *            true if fine "epoch" bins should be used, otherwise course will be
     *            used.
     * @param span
     *            the limit, if any, of number of slices to include.
     * @param after
     *            the date and time from which to start the sequence. If
     *            <code>null</code> then this is set to "now" minus the time covered
     *            by the "span".
     *
     * @return the sequence of shipping {@link Slice} instances to all neighbors as
     *         specified by the conditions.
     */
    List<Slice> getShippingSlices(boolean fineBins,
                                  Integer span,
                                  Date after);

    /**
     * Returns the sequence of shipping {@link Slice} instances to the supplied
     * neighbor as specified by the conditions.
     *
     * @param neighbor
     *            the neighboring SPADE instance, in any, for which to restrict the
     *            result.
     * @param fineBins
     *            true if fine "epoch" bins should be used, otherwise course will be
     *            used.
     * @param span
     *            the limit, if any, of number of slices to include.
     * @param after
     *            the date and time from which to start the sequence. If
     *            <code>null</code> then this is set to "now" minus the time covered
     *            by the "span".
     *
     * @return the sequence of shipping {@link Slice} instances to the supplied
     *         neighbor as specified by the conditions.
     */
    List<Slice> getShippingSlices(String neighbor,
                                  boolean fineBins,
                                  Integer span,
                                  Date after);

    /**
     * Returns a list of {@link DigestChange} instances for bundles whose delivery
     * to all destinations has been verified.
     *
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list. If <code>reversed</code> is <code>true</code> the
     *            only item after will be included.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>reversed</code> is <code>true</code> the items on
     *            or before will be included.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     * @param neighbor
     *            the neighboring SPADE instance, in any, for which to restrict the
     *            result.
     * @param registrations
     *            the collection of names of registrations to included in the
     *            result.
     * @param inbound
     *            True if only inbound transfers should be included in the result,
     *            False if only outbound transfers and <code>null</code> if both
     *            directions should be included.
     *
     * @return the list of {@link DigestChange} instances for bundles whose delivery
     *         to all destinations has been verified.
     */
    List<DigestChange> getTicketedByTime(Date after,
                                         Date before,
                                         boolean reversed,
                                         int max,
                                         String neighbor,
                                         List<String> registrations,
                                         Boolean inbound);

    /**
     * Returns the sequence of ticketed file identities associated with the
     * specified bundle.
     *
     * @param bundle
     *            the name of the bundle whose tickets should be returned.
     *
     * @return the sequence of ticketed file identities associated with the
     *         specified bundle.
     */
    List<String> getTicketsByBundle(String bundle);

    /**
     * Returns a list of {@link DigestChange} instances for bundles whose delivery
     * to all destinations has not yet been verified.
     *
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list. If <code>reversed</code> is <code>true</code> the
     *            only item after will be included.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>reversed</code> is <code>true</code> the items on
     *            or before will be included.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     *
     * @return the list of {@link DigestChange} instances for bundles whose delivery
     *         to all destinations has not yet been verified.
     */
    List<DigestChange> getUnverifiedByTime(Date after,
                                           Date before,
                                           boolean reversed,
                                           int max);

    /**
     * Returns a list of {@link DigestChange} instances for bundles whose delivery
     * to all destinations has been verified.
     *
     * @param after
     *            the time on or after which bundles should be include in the
     *            returned list. If <code>reversed</code> is <code>true</code> the
     *            only item after will be included.
     * @param before
     *            the time before which bundles should be include in the returned
     *            list. If <code>reversed</code> is <code>true</code> the items on
     *            or before will be included.
     * @param reversed
     *            <code>true</code> if the resulting list should be in reverse
     *            order.
     * @param max
     *            the maximum number of {@link DigestChange} instances to return, 0
     *            is unlimited.
     * @param registrations
     *            the collection of names of registrations to included in the
     *            result.
     *
     * @return the list of {@link DigestChange} instances for bundles whose delivery
     *         to all destinations has been verified.
     */
    List<DigestChange> getVerifiedByTime(Date after,
                                         Date before,
                                         boolean reversed,
                                         int max,
                                         List<String> registrations);

    /**
     * Returns the {@link WorkflowDirector} that is directing the specified
     * {@link Workflow}.
     *
     * @param workflow
     *            the {@link Workflow} whose {@link WorkflowDirector} instance
     *            should be returned.
     *
     * @return the {@link WorkflowDirector} that is directing the specified
     *         {@link Workflow}.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    WorkflowDirector getWorkflowDirector(Workflow workflow) throws InitializingException;

    /**
     * Returns the {@link WorkflowsMonitor} that is monitoring all workflows.
     *
     * @return the {@link WorkflowsMonitor} that is monitoring all workflows.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    WorkflowsMonitor getWorkflowsMonitor() throws InitializingException;

    /**
     * Scans for new bundles that match any inbound registration and call
     * {@link #ingest(Bundles, URI)} with the resulting collection.
     *
     * @param baseUri
     *            the base {@link URI}, if any, used to request this scan.
     *
     * @return the collection of {@link Bundle} instances whose ingestion has
     *         succeeded.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws IOException
     *             if there is an IO problem during the scan.
     * @throws InProgressException
     *             if the local scan is already in progress.
     * @throws InterruptedException
     *             if the scan is interrupted.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Bundles inboundScan(URI baseUri) throws SuspendedException,
                                     IOException,
                                     InProgressException,
                                     InterruptedException,
                                     TooManyTicketsException,
                                     InitializingException;

    /**
     * Convenience method to all {@link #ingest(Bundles, URI)}, using the default
     * base {@link URI}.
     *
     * @param bundles
     *            The set of {@link Bundle} instances to be processed.
     *
     * @return the collection of {@link Bundle} instances whose ingestion has
     *         succeeded.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InitializingException
     *             when the application is still initializing
     * @throws InProgressException
     *             when an ingestion is already in progess.
     * @throws InterruptedException
     *             when this ingestion is interrupted.
     */
    Bundles ingest(Bundles bundles) throws SuspendedException,
                                    TooManyTicketsException,
                                    InitializingException,
                                    InProgressException,
                                    InterruptedException;

    /**
     * Requests that the supplied {@link Bundle} instances be ingested by this
     * instance of the application. The application will determine the appropriate
     * registration for each Bundle. If no registration is found for a Bundle it
     * will not be included in the returned list of Bundles. Similarly if the Bundle
     * is already in the process of ingestion it will not be included in the
     * returned list of Bundles
     *
     * The returned collection may, or may not be, the supplied collection that has
     * been modified.
     *
     * @param bundles
     *            The set of {@link Bundle} instances to be processed.
     * @param baseUri
     *            The base {@link URI} used to request to request this ingestion, if
     *            <code>null</code> then the default base {@link URI} is used.
     *
     * @return the collection of {@link Bundle} instances whose ingestion has
     *         succeeded.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InitializingException
     *             when the workflows are still initializing.
     * @throws InProgressException
     *             when an ingestion is already in progess.
     * @throws InterruptedException
     *             when this ingestion is interrupted.
     */
    Bundles ingest(Bundles bundles,
                   URI baseUri) throws SuspendedException,
                                TooManyTicketsException,
                                InitializingException,
                                InProgressException,
                                InterruptedException;

    /**
     * Returns true when the specified workflow is suspended.
     *
     * @param workflow
     *            the workflow whose state should be returned.
     *
     * @return true when the specified workflow is suspended.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    boolean isSuspended(Workflow workflow) throws InitializingException;

    /**
     * Scans for new bundles that match any local registrations and calls
     * {@link #ingest(Bundles, URI)} with the resulting collection.
     *
     * @param baseUri
     *            the base {@link URI}, if any, used to request this scan.
     *
     * @return the collection of {@link Bundle} instances whose ingestion has
     *         succeeded.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws IOException
     *             if there is an IO problem during the scan.
     * @throws InProgressException
     *             if the local scan is already in progress.
     * @throws InterruptedException
     *             if the scan is interrupted.
     * @throws TooManyTicketsException
     *             when this instance is already processing its limit of tickets.
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Bundles localScan(URI baseUri) throws SuspendedException,
                                   IOException,
                                   InProgressException,
                                   InterruptedException,
                                   TooManyTicketsException,
                                   InitializingException;

    /**
     * Attempts to re-archive the transfers of the supplied set of tickets. Only
     * tickets whose archiving has failed will be re-archived, tickets whose
     * archiving has succeeded or that have no archiving will be ignored.
     *
     * @param tickets
     *            the {@link Collection} of tickets whose archiving should be
     *            re-archived.
     * @param baseUri
     *            The base {@link URI} used to request to request this re-archive,
     *            if <code>null</code> then the default base {@link URI} is used.
     *
     * @return the {@link Collection} of tickets whose re-archiving will be
     *         attempted.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Collection<String> rearchive(Collection<String> tickets,
                                 URI baseUri) throws SuspendedException,
                                              InitializingException;

    /**
     * Recommences the execution of the deferred activity instance that correlates
     * to the supplied {@link URI}.
     *
     * @param uri
     *            the {@link URI} which refers to the deferred activity instance.
     * @param message
     *            the message to be used by the deferred activity instance when it
     *            recommences.
     */
    void recommence(URI uri,
                    Object message);

    /**
     * Attempts to re-dispatch the transfers of the supplied set of tickets. Only
     * tickets whose transfers have failed will be re-dispatched, tickets whose
     * transfers have succeeded or that have no transfers will be ignored.
     *
     * @param tickets
     *            the {@link Collection} of tickets whose transfers should be
     *            re-dispatched.
     * @param baseUri
     *            The base {@link URI} used to request to request this re-dispatch,
     *            if <code>null</code> then the default base {@link URI} is used.
     *
     * @return the {@link Collection} of tickets whose transfers will be
     *         re-dispatched.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Collection<String> redispatch(Collection<String> tickets,
                                  URI baseUri) throws SuspendedException,
                                               InitializingException;

    /**
     * Scans for bundles whose transfers should be re-dispatched and calls
     * {@link #redispatchScan(long, TimeUnit, URI)}.
     *
     * @param cushion
     *            the time in seconds that the identity must have been in the queue
     *            in order to be returned.
     * @param unit
     *            the time unit of the cushion argument
     * @param baseUri
     *            The base {@link URI}, if any, used to request to request this
     *            re-dispatch.
     *
     * @return the {@link Collection} of tickets whose transfers will be
     *         re-dispatched.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws IOException
     *             if there is an IO problem during the scan.
     * @throws InterruptedException
     *             if the scan is interrupted.
     * @throws InitializingException
     *             when the workflows are still initializing.
     * @throws InProgressException
     *             if the redispatch scan is already in progress.
     */
    Collection<String> redispatchScan(long cushion,
                                      TimeUnit unit,
                                      URI baseUri) throws SuspendedException,
                                                   IOException,
                                                   InterruptedException,
                                                   InitializingException,
                                                   InProgressException;

    /**
     * Scans for new directories in the rescue area from which to restore Bundle
     * executions that have previously failed.
     *
     * @param workflow
     *            the {@link Workflow} instance for which to rescue workflows.
     *
     * @return the collection of {@link Bundle} instances that have been rescued.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws InProgressException
     *             if the local scan is already in progress.
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Collection<Bundle> rescueScan(Workflow workflow) throws InitializingException,
                                                     SuspendedException,
                                                     InProgressException;

    /**
     * Attempts to re-dispatch the transfers of the supplied set of tickets. Only
     * tickets whose transfers have failed will be re-dispatched, tickets whose
     * transfers have succeeded or that have no transfers will be ignored.
     *
     * @param tickets
     *            the {@link Collection} of tickets whose transfers should be
     *            re-shipped.
     * @param all
     *            true if all outbound transfers should be re-dispatched, otherwise
     *            only the unconfirmed ones will be.
     * @param baseUri
     *            The base {@link URI} used to request to request this re-ship, if
     *            <code>null</code> then the default base {@link URI} is used.
     *
     * @return the {@link Collection} of tickets whose transfers will be re-shipped.
     *
     * @throws SuspendedException
     *             when the application is suspended.
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Collection<String> reship(Collection<String> tickets,
                              Boolean all,
                              URI baseUri) throws SuspendedException,
                                           InitializingException;

    /**
     * Requests that the tickets for the supplied {@link Bundle} instances be
     * unblocked by this instance of the application.
     *
     * The returned collection may, or may not be, the supplied collection that has
     * been modified.
     *
     * @param bundles
     *            The set of {@link Bundle} instances to be unblocked.
     *
     * @return the collection of {@link Bundle} instances whose unblocking has
     *         succeeded.
     *
     * @throws SuspendedException
     *             when the application is suspended. when the application is
     *             suspended.
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    Bundles unblock(Bundles bundles) throws SuspendedException,
                                     InitializingException;

}
