package gov.lbl.nest.spade.services;

import gov.lbl.nest.spade.activities.IngestTermination;

/**
 * This interface is used to create {@link IngestTermination} instances.
 *
 * @author patton
 */
public interface TerminationManager {

    /**
     * Creates an {@link IngestTermination} instance.
     *
     * @return the created {@link IngestTermination} instance.
     */
    IngestTermination createIngestTermination();
}
