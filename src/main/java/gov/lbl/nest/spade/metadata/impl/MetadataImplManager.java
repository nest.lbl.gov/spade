package gov.lbl.nest.spade.metadata.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import gov.lbl.nest.spade.activities.Examiner;
import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import jakarta.annotation.Priority;
import jakarta.ejb.Stateless;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.PropertyException;
import jakarta.xml.bind.Unmarshaller;

/**
 * This is the default implementation of the {@link MetadataManager} interface.
 *
 * @author patton
 */
@Priority(value = 0)
@Stateless
public class MetadataImplManager implements
                                 MetadataManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Marshaller} property to set formatting.
     */
    private static final String XML_FORMAT_PROPERTY = "jaxb.formatted.output";

    // private static member data

    // private instance member data

    /**
     * The class implementing the {@link Metadata} interface that this object is
     * managing.
     */
    private final Class<? extends Metadata> clazz;

    // constructors

    /**
     * Creates an instance of this class.
     */
    public MetadataImplManager() {
        this(MetadataImpl.class);
    }

    /**
     * Create an instance of this class.
     *
     * @param clazz
     *            the class implementing the {@link Metadata} interface that this
     *            object is managing.
     */
    protected MetadataImplManager(final Class<? extends Metadata> clazz) {
        this.clazz = clazz;
    }

    // instance member method (alphabetic)

    @Override
    public Metadata createMetadata(final File file) throws MetadataParseException,
                                                    FileNotFoundException,
                                                    IOException {
        if (0L == file.length()) {
            return new MetadataImpl();
        }
        try (final InputStream is = new FileInputStream(file)) {
            final JAXBContext jc = JAXBContext.newInstance(clazz);
            final Unmarshaller u = jc.createUnmarshaller();
            final Metadata metadata = (Metadata) u.unmarshal(is);
            return metadata;
        } catch (JAXBException e) {
            throw new MetadataParseException(e);
        }
    }

    @Override
    public Metadata createMetadata(final Metadata metadata,
                                   final Metadata defaults) {
        if ((null == defaults) || ((metadata instanceof MetadataImpl) && (defaults instanceof MetadataImpl))) {
            return metadata;
        }
        throw new IllegalArgumentException("Metadata instance of classes of \"" + (metadata.getClass()).getName()
                                           + " and "
                                           + (defaults.getClass()).getName()
                                           + "\" can not be handled by the \""
                                           + (this.getClass()).getName()
                                           + "\"");
    }

    /**
     * Returns a {@link Object} instance to be saved, based on the supplied
     * {@link Metadata} instance.
     * 
     * @param metadata
     *            the {@link Metadata} instance on which to base the {@link Object}
     *            instance to be saved.
     * 
     * @return the {@link Object} instance to be saved, based on the supplied
     *         {@link Metadata} instance.
     */
    protected Object getObjectToSave(final Metadata metadata) {
        return metadata;
    }

    /**
     * Save the supplied {@link Metadata} instance to the specified {@link File}.
     *
     * @param metadata
     *            the {@link Metadata} instance to save
     * @param file
     *            the {@link File} in which to save it.
     *
     * @see Examiner
     */
    @Override
    public void save(Metadata metadata,
                     File file) {
        try {
            final Object objectToSave = getObjectToSave(metadata);
            JAXBContext content = JAXBContext.newInstance(objectToSave.getClass());
            Marshaller marshaller = content.createMarshaller();
            marshaller.setProperty(XML_FORMAT_PROPERTY,
                                   Boolean.TRUE);
            marshaller.marshal(objectToSave,
                               file);
        } catch (PropertyException e) {
            throw new Error("Should not be able to get here under normal circumstances!");
        } catch (JAXBException e) {
            throw new RuntimeException(e);
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
