package gov.lbl.nest.spade.metadata.impl;

import java.util.List;

import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import gov.lbl.nest.spade.registry.ExternalFile;

/**
 * This interface is used by {@link Metadata} instances to provide the original
 * location of the data files.
 *
 * @author patton
 */
public interface PathMetadata extends
                              Metadata {

    /**
     * Returns the {@link ExternalFile} instances describing the location of the
     * data file.
     *
     * @return the {@link ExternalFile} instances describing the location of the
     *         data file.
     */
    List<ExternalFile> getPaths();

    /**
     * Sets all <code>null</code> elements to their value in the supplied defaults.
     *
     * @param defaults
     *            the value of the elements to use when they are <code>null</code>.
     *
     * @return the resulting merge {@link PathMetadata} instance.
     */
    PathMetadata merge(PathMetadata defaults);
}
