package gov.lbl.nest.spade.metadata.impl;

import java.util.List;

import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import gov.lbl.nest.spade.registry.ExternalFile;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;

/**
 * This is an implementation of the {@link Metadata} interface that contains the
 * original path to the data file.
 *
 * @author patton
 */
@XmlSeeAlso(value = { MetadataImpl.class })
@XmlRootElement(name = "path_metadata")
public class PathMetadataImpl implements
                              PathMetadata {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link ExternalFile} instances describing the location of the data file.
     */
    private List<ExternalFile> paths;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the {@link ExternalFile} instances describing the location of the
     * data file.
     *
     * @return the {@link ExternalFile} instances describing the location of the
     *         data file.
     */
    @Override
    @XmlElement(name = "path")
    public List<ExternalFile> getPaths() {
        return paths;
    }

    /**
     * Sets all <code>null</code> elements to their value in the supplied defaults.
     *
     * @param defaults
     *            the value of the elements to use when they are <code>null</code>.
     *
     * @return the resulting merge {@link PathMetadata} instance.
     */
    @Override
    public PathMetadata merge(PathMetadata defaults) {
        if (null == getPaths()) {
            setPaths(defaults.getPaths());
        }
        return this;
    }

    /**
     * Sets the {@link ExternalFile} instances describing the location of the data
     * file.
     *
     * @param paths
     *            the {@link ExternalFile} instances describing the location of the
     *            data file.
     */
    public void setPaths(final List<ExternalFile> paths) {
        this.paths = paths;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
