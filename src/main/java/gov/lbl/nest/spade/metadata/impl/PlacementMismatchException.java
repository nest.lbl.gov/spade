package gov.lbl.nest.spade.metadata.impl;

import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import gov.lbl.nest.spade.interfaces.policy.PlacingPolicy;
import gov.lbl.nest.spade.policy.impl.Sha1Placement;

/**
 * This exception is thrown when the {@link Metadata} instance implies that the
 * {@link Sha1Placement} class should be used as the {@link PlacingPolicy}.
 *
 * @author patton
 *
 */
public class PlacementMismatchException extends
                                        Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
