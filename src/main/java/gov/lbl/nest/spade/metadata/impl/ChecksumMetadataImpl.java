package gov.lbl.nest.spade.metadata.impl;

import gov.lbl.nest.spade.interfaces.metadata.ChecksumElement;
import gov.lbl.nest.spade.interfaces.metadata.ChecksumMetadata;
import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;

/**
 * This is an implementation of the {@link Metadata} interface that contains the
 * data or pre-packed file's checksum.
 *
 * @author patton
 */
@XmlSeeAlso(value = { MetadataImpl.class })
@XmlRootElement(name = "checksum_metadata")
public class ChecksumMetadataImpl implements
                                  ChecksumMetadata {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link ChecksumElement} instance for this object.
     */
    private ChecksumElement checksum;

    // constructors

    // instance member method (alphabetic)

    @Override
    @XmlElement(name = "checksum")
    public ChecksumElement getChecksum() {
        return checksum;
    }

    /**
     * Sets the {@link ChecksumElement} instance for this object.
     *
     * @param element
     *            the {@link ChecksumElement} instance for this object.
     */
    @Override
    public void setChecksum(ChecksumElement element) {
        this.checksum = element;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
