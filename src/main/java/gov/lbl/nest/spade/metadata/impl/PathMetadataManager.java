package gov.lbl.nest.spade.metadata.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import jakarta.ejb.Stateless;
import jakarta.enterprise.inject.Alternative;

/**
 * This is the implementation of the {@link MetadataManager} interface for the
 * {@link PathMetadataImpl} class.
 *
 * Note: the <code>implements</code> declaration appears to be necessary for the
 * CDI service to work correctly.
 *
 * @author patton
 */
@Alternative
@Stateless
public class PathMetadataManager extends
                                 MetadataImplManager implements
                                 MetadataManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link MetadataImplManager} instance to use when the {@link Metadata}
     * instance is not an instance of {@link PathMetadata} class.
     */
    private final MetadataImplManager metadataImplManager = new MetadataImplManager(MetadataImpl.class);

    // constructors

    /**
     * Creates an instance of this class.
     */
    public PathMetadataManager() {
        super(PathMetadataImpl.class);
    }

    // instance member method (alphabetic)

    @Override
    public Metadata createMetadata(final File file) throws MetadataParseException,
                                                    FileNotFoundException,
                                                    IOException {
        if (0L == file.length()) {
            return new PathMetadataImpl();
        }
        return super.createMetadata(file);
    }

    @Override
    public Metadata createMetadata(final Metadata metadata,
                                   final Metadata defaults) {
        if ((metadata instanceof PathMetadata) && (defaults instanceof PathMetadata)) {
            return ((PathMetadata) metadata).merge((PathMetadata) defaults);
        }
        try {
            return metadataImplManager.createMetadata(metadata,
                                                      defaults);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Metadata instance of classes of \"" + (metadata.getClass()).getName()
                                               + " and "
                                               + (defaults.getClass()).getName()
                                               + "\" can not be handled by the \""
                                               + (this.getClass()).getName()
                                               + "\"");
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
