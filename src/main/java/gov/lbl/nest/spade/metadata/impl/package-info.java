/**
 * The package contains the classes that implement the pre-packaged Metadata
 * choices.
 *
 * @author patton
 */
package gov.lbl.nest.spade.metadata.impl;