package gov.lbl.nest.spade.metadata.impl;

import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlSeeAlso;

/**
 * The is the default implementation of the {@link Metadata} tag, as managed by
 * the {@link MetadataImplManager} class.O
 *
 * @author patton
 *
 */
@XmlSeeAlso(value = { ChecksumMetadataImpl.class })
@XmlRootElement(name = "metadata")
public class MetadataImpl implements
                          Metadata {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
