package gov.lbl.nest.spade.metadata.impl;

import java.util.Date;

import gov.lbl.nest.spade.interfaces.metadata.Metadata;

/**
 * This interface is used by {@link Metadata} instances to provide the date and
 * time used by the UTC placement policy.
 *
 * @author patton
 */
public interface UtcMetadata extends
                             Metadata {

    /**
     * Returns the date and time to be used by the UTC placement policy.
     *
     * @return the date and time to be used by the UTC placement policy.
     */
    Date getUtcDateTime();
}
