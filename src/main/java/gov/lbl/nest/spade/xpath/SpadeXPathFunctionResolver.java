package gov.lbl.nest.spade.xpath;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathFunction;
import javax.xml.xpath.XPathFunctionResolver;

/**
 * This class implements the XPath functions used by SPADE.
 *
 * @author patton
 *
 */
public class SpadeXPathFunctionResolver implements
                                        XPathFunctionResolver {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    private static final QName FN_FALSE = new QName(FnNamespaceContextImpl.FN_NS,
                                                    "false");

    private static final QName FN_TRUE = new QName(FnNamespaceContextImpl.FN_NS,
                                                   "true");

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    @Override
    public XPathFunction resolveFunction(QName arg0,
                                         int arg1) {
        final String localPart = arg0.getLocalPart();
        if (FnNamespaceContextImpl.FN_NS.equals(arg0.getNamespaceURI())) {
            if ((FN_FALSE.getLocalPart()).equals(localPart)) {
                return new XPathFunction() {

                    @Override
                    public Object evaluate(List<?> arg2) {
                        return Boolean.FALSE;
                    }
                };
            }
            if ((FN_TRUE.getLocalPart()).equals(localPart)) {
                return new XPathFunction() {

                    @Override
                    public Object evaluate(List<?> arg2) {
                        return Boolean.TRUE;
                    }
                };
            }
            return null;
        }
        return null;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
