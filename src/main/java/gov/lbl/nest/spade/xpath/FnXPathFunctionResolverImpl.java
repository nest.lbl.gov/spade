package gov.lbl.nest.spade.xpath;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.xpath.XPathFunction;
import javax.xml.xpath.XPathFunctionResolver;

import org.w3c.dom.NodeList;

/**
 * This class implements the fn: namespace functions.
 *
 * @author patton
 *
 */
public class FnXPathFunctionResolverImpl implements
                                         XPathFunctionResolver {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    private static final QName FN_COUNT = new QName(FnNamespaceContextImpl.FN_NS,
                                                    "count");

    private static final QName FN_ENDS_WITH = new QName(FnNamespaceContextImpl.FN_NS,
                                                        "ends-with");

    private static final QName FN_FALSE = new QName(FnNamespaceContextImpl.FN_NS,
                                                    "false");

    private static final QName FN_STRING = new QName(FnNamespaceContextImpl.FN_NS,
                                                     "string");

    private static final QName FN_TRUE = new QName(FnNamespaceContextImpl.FN_NS,
                                                   "true");

    // private static member data

    // private instance member data

    /**
     * The {@link XPathFunctionResolver} to use if this one does not resolve a
     * function.
     */
    private final XPathFunctionResolver fallback;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param resolver
     *            the {@link XPathFunctionResolver} to use if this one does not
     *            resolve a function.
     */
    public FnXPathFunctionResolverImpl(XPathFunctionResolver resolver) {
        fallback = resolver;
    }

    // instance member method (alphabetic)

    @Override
    public XPathFunction resolveFunction(QName functionName,
                                         int arity) {
        final String localPart = functionName.getLocalPart();
        if (FnNamespaceContextImpl.FN_NS.equals(functionName.getNamespaceURI())) {
            if ((FN_COUNT.getLocalPart()).equals(localPart) && 1 == arity) {
                return new XPathFunction() {

                    @Override
                    public Object evaluate(List<?> arg0) {
                        final NodeList list = (NodeList) arg0.get(0);
                        return list.getLength();
                    }
                };
            }
            if (FN_ENDS_WITH.equals(functionName) && 2 == arity) {
                return new XPathFunction() {

                    @Override
                    public Object evaluate(List<?> args) {
                        final String string = (String) args.get(0);
                        final String s = (String) args.get(1);
                        return string.endsWith(s);
                    }
                };
            }
            if ((FN_FALSE.getLocalPart()).equals(localPart) && 0 == arity) {
                return new XPathFunction() {

                    @Override
                    public Object evaluate(List<?> arg0) {
                        return Boolean.FALSE;
                    }
                };
            }
            if (FN_STRING.equals(functionName) && 1 == arity) {
                return new XPathFunction() {

                    @Override
                    public Object evaluate(List<?> arg0) {
                        final NodeList list = (NodeList) arg0.get(0);
                        int finished = list.getLength();
                        final StringBuilder sb = new StringBuilder();
                        for (int i = 0;
                             finished != i;
                             ++i) {
                            sb.append((list.item(i)).getTextContent());
                        }
                        return sb.toString();
                    }
                };

            }
            if ((FN_TRUE.getLocalPart()).equals(localPart) && 0 == arity) {
                return new XPathFunction() {

                    @Override
                    public Object evaluate(List<?> arg0) {
                        return Boolean.TRUE;
                    }
                };
            }
            return null;
        }
        if (null == fallback) {
            return null;
        }
        return fallback.resolveFunction(functionName,
                                        arity);
    }
    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
