package gov.lbl.nest.spade.xpath;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.NamespaceContext;

/**
 * This class implements the {@link NamespaceContext} interface for the "fn"
 * namespace.
 *
 * @author patton
 */
public class FnNamespaceContextImpl implements
                                    NamespaceContext {

    // public static final member data

    /**
     * The namespace for the BPMN extensions to XPath.
     */
    public static final String FN_NS = "http://www.w3.org/2005/xpath-functions";

    /**
     * The prefix associated with the BPMN extensions to XPath.
     */
    public static final String FN_PREFIX = "fn";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The List of BPMN prefixes.
     */
    private static final List<String> FN_PREFIXES = new ArrayList<>();
    private static final Collection<String> UNMODIFIABLE_FN_PREFIXES = Collections.unmodifiableCollection(FN_PREFIXES);
    {
        FN_PREFIXES.add(FN_PREFIX);
    }

    // private static member data

    // private instance member data

    /**
     * The {@link NamespaceContext} to use if this one does not have a response for
     * an invoked method.
     */
    private final NamespaceContext fallback;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param context
     *            the {@link NamespaceContext} to use if this one does not resolve a
     *            function.
     */
    public FnNamespaceContextImpl(NamespaceContext context) {
        fallback = context;
    }

    // instance member method (alphabetic)

    @Override
    public String getNamespaceURI(String prefix) {
        if (prefix.equals("fn")) {
            return FN_NS;
        }
        if (null == fallback) {
            return null;
        }
        return fallback.getNamespaceURI(prefix);
    }

    @Override
    public String getPrefix(String namespaceURI) {
        if (null == namespaceURI) {
            throw new IllegalArgumentException("Namespace must not be null");
        }
        if (namespaceURI.equals(FN_NS)) {
            return FN_PREFIX;
        }
        if (null == fallback) {
            return null;
        }
        return fallback.getPrefix(namespaceURI);
    }

    @Override
    public Iterator<String> getPrefixes(String namespaceURI) {
        if (null == namespaceURI) {
            throw new IllegalArgumentException("Namespace must not be null");
        }
        if (namespaceURI.equals(FN_NS)) {
            return UNMODIFIABLE_FN_PREFIXES.iterator();
        }
        if (null == fallback) {
            return null;
        }
        return fallback.getPrefixes(namespaceURI);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
