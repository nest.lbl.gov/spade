/**
 * The package contains the classes that implement the
 * {@link javax.xml.xpath.XPath} interfaces that are use by this application.
 *
 * @author patton
 */
package gov.lbl.nest.spade.xpath;