/**
 * The package contains the classes for the main SPADE application.
 *
 * @author patton
 */
package gov.lbl.nest.spade;