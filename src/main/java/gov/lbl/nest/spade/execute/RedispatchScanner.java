package gov.lbl.nest.spade.execute;

import java.util.Collection;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendedException;
import gov.lbl.nest.spade.services.InProgressException;
import gov.lbl.nest.spade.services.Spade;

/**
 * This class in a {@link Runnable} implementation that scans a SPADE's transfer
 * destinations for delivery confirmations.
 *
 * @author patton
 */
public class RedispatchScanner implements
                               Runnable {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(RedispatchScanner.class);

    // private static member data

    // private instance member data

    /**
     * The SPADE instance that which be scanned.
     */
    private final Spade spade;

    // constructors

    /**
     * Create an instance of this class.
     *
     * @param spade
     *            the SPADE instance that which be scanned.
     */
    public RedispatchScanner(final Spade spade) {
        this.spade = spade;
    }

    // instance member method (alphabetic)

    @Override
    public void run() {
        try {
            final Collection<String> redispatched = spade.redispatchScan(13,
                                                                         TimeUnit.MINUTES,
                                                                         null);
            final String plural;
            if (1 == redispatched.size()) {
                plural = "";
            } else {
                plural = "s";
            }
            LOG.info("Successfully re-queued " + redispatched.size()
                     + " postponed ticket"
                     + plural
                     + " for redispatch"
                     + plural);
        } catch (InProgressException e) {
            LOG.warn(e.getMessage());
        } catch (InitializingException
                 | SuspendedException e) {
            LOG.warn("Redispatch Scan request was ignored because:" + e.getMessage());
        } catch (Exception e) {
            LOG.error("Failed when trying to redispatch postponeds tickets");
            e.printStackTrace();
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
