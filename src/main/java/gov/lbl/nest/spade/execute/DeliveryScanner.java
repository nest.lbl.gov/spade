package gov.lbl.nest.spade.execute;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendedException;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.spade.services.InProgressException;
import gov.lbl.nest.spade.services.Spade;

/**
 * This class in a {@link Runnable} implementation that scans a SPADE's transfer
 * destinations for delivery confirmations.
 *
 * @author patton
 */
public class DeliveryScanner implements
                             Runnable {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DeliveryScanner.class);

    // private static member data

    // private instance member data

    /**
     * The SPADE instance that which be scanned.
     */
    private final Spade spade;

    // constructors

    /**
     * Create an instance of this class.
     *
     * @param spade
     *            the SPADE instance that which be scanned.
     */
    public DeliveryScanner(final Spade spade) {
        this.spade = spade;
    }

    // instance member method (alphabetic)

    @Override
    public void run() {
        try {
            final Collection<DigestChange> confirmed = spade.deliveryScan();
            final String plural;
            if (1 == confirmed.size()) {
                plural = "y";
            } else {
                plural = "ies";
            }
            LOG.info("Successfully confirmed " + confirmed.size()
                     + " deliver"
                     + plural);
        } catch (InProgressException e) {
            LOG.warn(e.getMessage());
        } catch (InitializingException
                 | SuspendedException e) {
            LOG.warn("Delivery Scan request was ignored because:" + e.getMessage());
        } catch (Exception e) {
            LOG.error("Failed when trying to check delivery of bundles");
            e.printStackTrace();
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
