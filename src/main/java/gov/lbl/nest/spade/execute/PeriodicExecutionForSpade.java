package gov.lbl.nest.spade.execute;

import gov.lbl.nest.spade.services.Spade;
import jakarta.annotation.Resource;
import jakarta.ejb.Schedule;
import jakarta.ejb.Stateless;
import jakarta.enterprise.concurrent.ManagedExecutorService;
import jakarta.inject.Inject;

/**
 * This case executes activities periodically.
 *
 * @author patton
 */
@Stateless
public class PeriodicExecutionForSpade {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link DeliveryScanner} implementation that check for new confirmations.
     */
    private Runnable delivery;

    /**
     * The {@link ManagedExecutorService} instance used for running scans.
     */
    @Resource
    ManagedExecutorService executor;

    /**
     * The {@link InboundScanner} implementation that check for new inbound bundles.
     */
    private Runnable inbound;

    /**
     * The {@link LocalScanner} implementation that check for new local bundles.
     */
    private Runnable local;

    /**
     * The {@link RedispatchScanner} implementation that check for postponed
     * transfers.
     */
    private Runnable redispatch;

    /**
     * The {@link Spade} instance used by this object.
     */
    @Inject
    private Spade spade;

    // constructors

    // instance member method (alphabetic)

    private Runnable getDeliveryScanner() {
        if (null == delivery) {
            delivery = new DeliveryScanner(spade);
        }
        return delivery;
    }

    private Runnable getInboundScanner() {
        if (null == inbound) {
            inbound = new InboundScanner(spade);
        }
        return inbound;
    }

    private Runnable getLocalScanner() {
        if (null == local) {
            local = new LocalScanner(spade);
        }
        return local;
    }

    private Runnable getRedispatchScanner() {
        if (null == redispatch) {
            redispatch = new RedispatchScanner(spade);
        }
        return redispatch;
    }

    /**
     * Scans for new file in the local dropboxes.
     */
    @Schedule(hour = "*",
              minute = "*/5",
              persistent = false)
    public void scanning() {
        executor.execute(getLocalScanner());
        executor.execute(getInboundScanner());
        executor.execute(getDeliveryScanner());
        executor.execute(getRedispatchScanner());
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
