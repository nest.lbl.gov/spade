package gov.lbl.nest.spade.execute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendedException;
import gov.lbl.nest.spade.rs.Bundles;
import gov.lbl.nest.spade.services.InProgressException;
import gov.lbl.nest.spade.services.Spade;
import gov.lbl.nest.spade.services.TooManyTicketsException;

/**
 * This class in a {@link Runnable} implementation that scans a SPADE's local
 * registration dropboxes.
 *
 * @author patton
 */
public class LocalScanner implements
                          Runnable {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(LocalScanner.class);

    // private static member data

    // private instance member data

    /**
     * The SPADE instance that which be scanned.
     */
    private final Spade spade;

    // constructors

    /**
     * Create an instance of this class.
     *
     * @param spade
     *            the SPADE instance that which be scanned.
     */
    public LocalScanner(final Spade spade) {
        this.spade = spade;
    }

    // instance member method (alphabetic)

    @Override
    public void run() {
        try {
            final Bundles scanned = spade.localScan(spade.getDefaultBaseUri());
            final String plural;
            if (1 == scanned.size()) {
                plural = "";
            } else {
                plural = "s";
            }
            LOG.info("Successfully ingested " + scanned.size()
                     + " new local bundle"
                     + plural);
        } catch (InProgressException e) {
            LOG.warn(e.getMessage());
        } catch (InitializingException
                 | SuspendedException
                 | TooManyTicketsException e) {
            LOG.warn("Local Scan request was ignored because:" + e.getMessage());
        } catch (Exception e) {
            LOG.error("Failed when trying to ingest new local bundles");
            e.printStackTrace();
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
