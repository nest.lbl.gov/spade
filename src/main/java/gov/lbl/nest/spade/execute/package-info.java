/**
 * The package contains the classes driver the execution of SPADE workflows.
 *
 * @author patton
 */
package gov.lbl.nest.spade.execute;