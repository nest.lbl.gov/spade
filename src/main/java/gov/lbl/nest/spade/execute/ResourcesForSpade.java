package gov.lbl.nest.spade.execute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.execution.DeployedFiles;
import gov.lbl.nest.common.execution.DeployedFiles.DeployedFile;
import gov.lbl.nest.common.tasks.TaskTracker;
import gov.lbl.nest.jee.monitor.InstrumentationDB;
import gov.lbl.nest.jee.watching.WatcherDB;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;
import gov.lbl.nest.spade.ImplementationString;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.interfaces.SpadeDB;
import gov.lbl.nest.spade.services.NotificationManager;
import gov.lbl.nest.spade.services.NotificationManager.Scope;
import gov.lbl.nest.spade.services.impl.NotificationManagerImpl;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.enterprise.inject.Produces;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.xml.bind.JAXBException;

/**
 * This class provides resources to be injected into the SPADE application.
 *
 * @author patton
 */
@Singleton
@Lock(value = LockType.READ)
public class ResourcesForSpade {

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ResourcesForSpade.class);

    /**
     * The version String containing this project's release.
     */
    private static String version;

    // private static member data

    /**
     * Returns the version String containing this project's release.
     * 
     * @return the version String containing this project's release.
     */
    private static synchronized String getDeployedVersion() {
        if (null == version) {
            DeployedFile deployedFile = DeployedFiles.getDeployedWarFile();
            LOG.info("Using version \"" + deployedFile.version
                     + "\" of artifact \""
                     + deployedFile.project
                     + "\"");
            version = deployedFile.version;
        }
        return version;
    }

    /**
     * Expose an entity manager using the resource producer pattern
     */
    @PersistenceContext(unitName = "spade")
    private EntityManager entityManager;

    /**
     * The {@link Configuration} instance used by this application.
     */
    private Configuration configuration;

    /**
     * The {@link NotificationManager} instance used by this application.
     */
    private NotificationManager notificationManager;

    /**
     * The {@link TaskTracker} instance used by this application.
     */
    private TaskTracker<FlowNodeTask> taskTracker;

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     */
    public ResourcesForSpade() {
        getDeployedVersion();
    }

    /**
     * Returns the {@link Configuration} instance used by this application.
     *
     * @return the {@link Configuration} instance used by this application.
     */
    @Produces
    @Lock(value = LockType.WRITE)
    public Configuration getConfiguration() {
        if (null == configuration) {
            try {
                configuration = Configuration.load();
                final String name = (configuration.getAssembly()).getName();
                LOG.info("Reading configuration for \"" + name
                         + "\" from \""
                         + configuration.getFile()
                         + "\"");
                notificationManager = new NotificationManagerImpl(name,
                                                                  configuration.getDirectory(),
                                                                  configuration.getNotifications());
                notificationManager.postNotice(Scope.STARTING,
                                               name + " has started execution, running version "
                                                               + getDeployedVersion());
            } catch (JAXBException e1) {
                LOG.error("Could not parse configuration file");
                e1.printStackTrace();
            }
        }
        return configuration;
    }

    /**
     * Returns the {@link EntityManager} instance used by this application.
     *
     * @return the {@link EntityManager} instance used by this application.
     */
    @Produces
    @InstrumentationDB
    @SpadeDB
    @WatcherDB
    public EntityManager getEntityManager() {
        return entityManager;
    }

    /**
     * Returns the implementation version.
     * 
     * @return the implementation version.
     */
    @Produces
    @ImplementationString
    public String getImplementationString() {
        getDeployedVersion();
        return version;
    }

    /**
     * Returns the {@link NotificationManager} instance used by this application.
     *
     * @return the {@link NotificationManager} instance used by this application.
     */
    @Produces
    public NotificationManager getNotificationManager() {
        // Make sure NotificationManager has been created.
        getConfiguration();
        return notificationManager;
    }

    /**
     * Returns the {@link TaskTracker} instance used by this application.
     *
     * @return the {@link TaskTracker} instance used by this application.
     */
    @Produces
    public TaskTracker<FlowNodeTask> getTaskTracker() {
        // Make sure TaskTracker has been created.
        if (null == taskTracker) {
            taskTracker = new TaskTracker<>();
        }
        return taskTracker;
    }
}
