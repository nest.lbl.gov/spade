package gov.lbl.nest.spade.execute;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendedException;
import gov.lbl.nest.spade.rs.Bundles;
import gov.lbl.nest.spade.services.InProgressException;
import gov.lbl.nest.spade.services.Spade;
import gov.lbl.nest.spade.services.TooManyTicketsException;

/**
 * This class in a {@link Runnable} implementation that scans a SPADE's inbound
 * registration dropboxes.
 *
 * @author patton
 */
public class InboundScanner implements
                            Runnable {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(InboundScanner.class);

    // private static member data

    // private instance member data

    /**
     * The SPADE instance that which be scanned.
     */
    private final Spade spade;

    // constructors

    /**
     * Create an instance of this class.
     *
     * @param spade
     *            the SPADE instance that which be scanned.
     */
    public InboundScanner(final Spade spade) {
        this.spade = spade;
    }

    // instance member method (alphabetic)

    @Override
    public void run() {
        try {
            final Bundles scanned = spade.inboundScan(spade.getDefaultBaseUri());
            final String plural;
            if (1 == scanned.size()) {
                plural = "";
            } else {
                plural = "s";
            }
            LOG.info("Successfully ingested " + scanned.size()
                     + " new inbound bundle"
                     + plural);
        } catch (InProgressException e) {
            LOG.warn(e.getMessage());
        } catch (InitializingException
                 | SuspendedException
                 | TooManyTicketsException e) {
            LOG.warn("Inbound Scan request was ignored because:" + e.getMessage());
        } catch (Exception e) {
            LOG.error("Failed when trying to ingest new inbound bundles");
            e.printStackTrace();
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
