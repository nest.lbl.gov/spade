/**
 * The package contains the classes that implement the pre-packaged Locators.
 *
 * @author patton
 */
package gov.lbl.nest.spade.locators;