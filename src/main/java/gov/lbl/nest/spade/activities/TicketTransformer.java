package gov.lbl.nest.spade.activities;

import gov.lbl.nest.spade.workflow.ActivityDefermentKey;

/**
 * This class is used to mark sure a task that can transform a ticket is loaded.
 *
 * @author patton
 *
 * @param <T>
 *            the class of ticket that is transformed.
 */
public interface TicketTransformer<T> {

    /**
     * Returns the name of the task that is transforming the ticket.
     *
     * @return the name of the task that is transforming the ticket.
     */
    String getName();

    /**
     * Executes the task.
     *
     * @param ticket
     *            the ticket to be transformed.
     * @param key
     *            the {@link ActivityDefermentKey} instance to use if this method
     *            defers its completion.
     *
     * @return the transformed ticket
     *
     * @throws Exception
     *             when there is a problem transforming the ticket.
     */
    T processTicket(final T ticket,
                    ActivityDefermentKey key) throws Exception;
}
