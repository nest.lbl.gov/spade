package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.interfaces.metadata.Metadata;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import gov.lbl.nest.spade.interfaces.policy.FetchingPolicy;
import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.LocalOptions;
import gov.lbl.nest.spade.registry.Options;
import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.registry.internal.InternalFileName;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import gov.lbl.nest.spade.services.RegistrationManager;

/**
 * This class examines the registration and any associated semaphore file for a
 * bundle in order to create the metadata and the information needed to ingest
 * that bundle.
 *
 * @author patton
 */
public class Examiner {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Bookkeeping} instance used by this object.
     */
    private final Bookkeeping bookkeeping;

    /**
     * The {@link CacheDefinition} instance used by this object.
     */
    private final CacheDefinition cacheDefinition;

    /**
     * The {@link CacheManager} instance used by this object.
     */
    private final CacheManager cacheManager;

    /**
     * The directory, below which the default metadata file are stored.
     */
    private final File defaultsTree;

    /**
     * The {@link FetchingPolicy} instance used by the receiver task.
     */
    private final FetchingPolicy inboundPolicy;

    /**
     * The {@link FetchingPolicy} instance used by the fetcher task.
     */
    private final FetchingPolicy localPolicy;

    /**
     * The {@link MetadataManager} instance used by this object.
     */
    private final MetadataManager metadataManager;

    /**
     * The name of this activity.
     */
    private final String name;

    /**
     * The {@link NeighborhoodManager} instance used by this object.
     */
    private final NeighborhoodManager neighborhoodManager;

    /**
     * The {@link RegistrationManager} instance used by this object.
     */
    private final RegistrationManager registrationManager;

    /**
     * The interval, if any, in seconds that a fetch can be retried after it has
     * failed.
     */
    private final Long retryInterval;

    /**
     * The total factor used to define the minimum space needed for this activity by
     * multiplying by the number of threads.
     */
    private final int storageLoad;

    // constructors

    /**
     * Creates an instance of this class for testing.
     * 
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param fetcherActivity
     *            the {@link Activity} instance declaring the fetcher task.
     * @param receiverActivity
     *            the {@link Activity} instance declaring the receiver task.
     * @param configurationDir
     *            the {@link File} instance of the directory holding the
     *            configuration file of the application.
     * @param bookkeeping
     *            the {@link Bookkeeping} instance used by this object.
     * @param cacheDefinition
     *            the {@link CacheDefinition} instance used by this object.
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     * @param metadataManager
     *            the {@link MetadataManager} instance used by this object.
     * @param neighborhoodManager
     *            the {@link NeighborhoodManager} instance used by this object.
     * @param registrationManager
     *            the {@link RegistrationManager} instance used by this object.
     */
    public Examiner(final Activity activity,
                    final Activity fetcherActivity,
                    final Activity receiverActivity,
                    final File configurationDir,
                    final Bookkeeping bookkeeping,
                    final CacheDefinition cacheDefinition,
                    final CacheManager cacheManager,
                    final MetadataManager metadataManager,
                    final NeighborhoodManager neighborhoodManager,
                    final RegistrationManager registrationManager) {
        this.bookkeeping = bookkeeping;
        this.cacheDefinition = cacheDefinition;
        this.cacheManager = cacheManager;
        this.defaultsTree = configurationDir;
        inboundPolicy = Fetcher.getFetchingPolicy(receiverActivity,
                                                  null);
        localPolicy = Fetcher.getFetchingPolicy(fetcherActivity,
                                                null);
        this.metadataManager = metadataManager;
        name = activity.getName();
        this.neighborhoodManager = neighborhoodManager;
        this.registrationManager = registrationManager;
        final String retryIntervalParameter = activity.getParameter("retry_interval");
        if (null == retryIntervalParameter) {
            retryInterval = null;
        } else {
            retryInterval = Long.parseLong(retryIntervalParameter);
        }
        storageLoad = activity.getStorageLoad();
    }

    // instance member method (alphabetic)

    /**
     * Creates a new {@link Metadata} instance storing it in the files that is
     * returned.
     *
     * @param bundle
     *            the {@link Bundle} whose metadata should be created
     * @param internalSemaphore
     *            the semaphore file that may contain metadata.
     * @param options
     *            the {@link LocalOptions} instance use to retrieve any default
     *            metadata.
     *
     * @throws MetadataParseException
     * @throws FileNotFoundException
     * @throws IOException
     */
    private File createMetadata(final Bundle bundle,
                                final File internalSemaphore,
                                final File defaults) throws MetadataParseException,
                                                     FileNotFoundException,
                                                     IOException {
        final Metadata defaultsMetadata;
        if (null == defaults) {
            defaultsMetadata = null;
        } else {
            defaultsMetadata = metadataManager.createMetadata(new File(defaultsTree,
                                                                       defaults.getPath()));
        }
        final Metadata created = metadataManager.createMetadata(metadataManager.createMetadata(internalSemaphore),
                                                                defaultsMetadata);
        final File metadataFile = writeMetadataFile(bundle.getName(),
                                                    internalSemaphore.getParentFile(),
                                                    created);
        return metadataFile;
    }

    /**
     * Examines a bundle and any associated semaphore file in order to create, along
     * with it matching registration, the metadata and processing information for
     * that bundle.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's file
     *            locations.
     *
     * @return the {@link IngestTicket} instance that results from successfully
     *         examining the bundle.
     *
     * @throws ExaminerException
     *             when a {@link Throwable} instance is generated in this method.
     */
    public IngestTicket examine(IngestTicket ticket) throws ExaminerException {

        /**
         * Fetch the external semaphore file into the cache.
         */
        final Bundle bundle = ticket.getBundle();
        final String activity;
        final FetchingPolicy policy;
        if (bundle.isInbound()) {
            activity = Activity.RECEIVER;
            policy = inboundPolicy;
        } else {
            activity = Activity.FETCHER;
            policy = localPolicy;
        }
        final ExternalFile externalSemaphore = bundle.getSemaphore();
        final String identity = ticket.getIdentity();
        File internalSemaphore = null;
        File metadataFile = null;
        File dataDeliveryDirectory = null;
        try {
            final File deliveryDirectory = new File(getDeliveryDirectory(bundle,
                                                                         (cacheDefinition.getCacheMap()).get(activity)),
                                                    identity);
            deliveryDirectory.mkdir();
            internalSemaphore = new File(deliveryDirectory,
                                         externalSemaphore.getName());
            externalSemaphore.setDeliveryLocation(internalSemaphore);

            // Clean up any previous attempt
            final File tmpFile = new File(internalSemaphore.getPath() + InternalFileName.TMP_SUFFIX);

            cacheManager.delete(tmpFile);

            // Test semaphore file still exists.
            // (It could have been processed while this request was
            // received.)
            final long currentStamp = policy.getLastModified(externalSemaphore);
            if (0 == currentStamp) {
                throw new InvalidatedIngestException("Fetching of file(s) associated with semaphore file \"" + externalSemaphore
                                                     + "\" abandoned as that semaphore file was removed before examination");
            }

            /**
             * If the semaphore file has not already been fetched do so now.
             */
            if (!internalSemaphore.exists()) {

                // Then get the current one.
                cacheManager.waitForSpace(tmpFile,
                                          storageLoad,
                                          name);
                policy.receive(externalSemaphore,
                               tmpFile,
                               false,
                               false);
                cacheManager.move(tmpFile,
                                  internalSemaphore);
            }

            /**
             * Work out from where the data-files should be fetched.
             */
            final Registration registration = registrationManager.getRegistration(bundle.getLocalId());
            final DataLocator dataLocator = registration.getDataLocator();
            /**
             * Get the set of ExternalFiles that make up the bundle's data. Their delivery
             * is with respect to the delivery directory.
             */
            final List<ExternalFile> externalFiles = dataLocator.locateData(externalSemaphore,
                                                                            internalSemaphore,
                                                                            identity);
            if (null == externalFiles || externalFiles.isEmpty()) {
                cacheManager.delete(internalSemaphore);
                throw new IllegalArgumentException("The semphore file \"" + externalSemaphore
                                                   + "\" has no data files associated with it.");
            }

            /**
             * If required create the metadata.
             */
            dataDeliveryDirectory = new File(deliveryDirectory,
                                             InternalFileName.PAYLOAD_DIRECTORY);
            final Options options = registration.getOptions();
            if (options.createMetadata()) {
                metadataFile = createMetadata(bundle,
                                              internalSemaphore,
                                              ((LocalOptions) options).getDefaultMetadataFile());
                bookkeeping.setMetadataSize(identity,
                                            metadataFile.length());
            } else {
                final int index = externalFiles.size() - 1;
                metadataFile = new File(dataDeliveryDirectory,
                                        ((externalFiles.get(index)).getDeliveryLocation()).getPath());
            }
            return new IngestTicket(ticket,
                                    currentStamp,
                                    metadataFile,
                                    dataDeliveryDirectory,
                                    externalFiles,
                                    options);

        } catch (Throwable t) {
            throw new ExaminerException(null,
                                        t,
                                        internalSemaphore,
                                        metadataFile,
                                        dataDeliveryDirectory,
                                        retryInterval);
        }
    }

    /**
     * Extends the supplied directory taking into account the neighbor, if any, who
     * delivered the file.
     *
     * @param the
     *            {@link Bundle} instance that is being delivered.
     * @param directory
     *            the base directory which may be extended.
     *
     * @return the extended directory appropriate for the member of the data
     *         movement network that delivered the file.
     *
     * @throws FileNotFoundException
     *             when the far end's directory is not defined.
     */
    private File getDeliveryDirectory(final Bundle bundle,
                                      final File directory) throws FileNotFoundException {
        if ((null == neighborhoodManager) || !bundle.isInbound()) {
            return directory;
        }
        final String neighbor = bundle.getNeighbor();
        final File result = new File(directory,
                                     neighborhoodManager.getDirectory(neighbor));
        if (!result.exists()) {
            if (!result.mkdirs()) {
                throw new FileNotFoundException("The directory, \"" + directory.getPath()
                                                + "\" could not be created.");
            }
        }
        return result;
    }

    /**
     * Safely write out the new metadata file.
     *
     * @param bundleName
     *            the name of the {@link Bundle} instance whose metadata is to be
     *            written.
     * @param directory
     *            the directory in which to write the {@link Metadata} instance.
     * @param metadata
     *            the {@link Metadata} instance to save.
     *
     * @return The {@link File} instance into which the {@link Metadata} instance
     *         was written.
     *
     * @throws IOException
     */
    private File writeMetadataFile(final String bundleName,
                                   final File directory,
                                   final Metadata metadata) throws IOException {
        final File metadataFile = new File(directory,
                                           InternalFileName.getMetadataFilename(bundleName));
        final File tmpFile = new File(metadataFile.getParentFile(),
                                      metadataFile.getName() + InternalFileName.TMP_SUFFIX);
        metadataManager.save(metadata,
                             tmpFile);
        cacheManager.move(tmpFile,
                          metadataFile);
        return metadataFile;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
