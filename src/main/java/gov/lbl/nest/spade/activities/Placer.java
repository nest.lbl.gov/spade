package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.policy.PlacingPolicy;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedException;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedFailure;
import gov.lbl.nest.spade.interfaces.storage.Location;
import gov.lbl.nest.spade.interfaces.storage.WarehouseManager;
import gov.lbl.nest.spade.policy.impl.Sha1Placement;
import gov.lbl.nest.spade.services.Bookkeeping;

/**
 * The class places selected files related to the Bundle into the Warehouse.
 *
 * The files to be placed and the location of their placement are determined by
 * the PlacementPolicy that is provided to this task at construction.
 *
 * @author patton
 *
 */
public class Placer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Placer.class);

    // private static member data

    // private instance member data

    /**
     * Returns the {@link PlacingPolicy} instance for the supplied {@link Activity}
     * instance.
     *
     * @param activity
     *            the {@link Activity} instance whose {@link PlacingPolicy} instance
     *            should be returned.
     * @param logger
     *            the {@link Logger} instance though which the selected policy will
     *            be logged.
     *
     * @return the {@link PlacingPolicy} instance for the supplied {@link Activity}
     *         instance.
     */
    public static PlacingPolicy getPlacingPolicy(Activity activity,
                                                 Logger logger) {
        final PlacingPolicy policyToUse;
        if (null == activity) {
            policyToUse = new Sha1Placement();
        } else {
            final PlacingPolicy policy = activity.getImplementedPolicy(PlacingPolicy.class);
            if (null == policy) {
                policyToUse = new Sha1Placement();
            } else {
                policyToUse = policy;
            }
        }
        final Class<?> clazz = policyToUse.getClass();
        if (null != activity && null != logger) {
            logger.info("\"" + activity.getName()
                        + "\" is using "
                        + clazz.getCanonicalName()
                        + " as its policy");
        }
        return policyToUse;
    }

    /**
     * Returns the size of a file or directory tree, or 0 if the file is
     * <code>null</code>.
     *
     * @param path
     *            the path to file whose size should be returned.
     *
     * @return the size of the file or directory tree, or 0 if the file is
     *         <code>null</code>.
     */
    private static long getSize(final String path) {
        if (null == path) {
            return 0;
        }
        final File file = new File(path);
        if (file.exists()) {
            // TODO: fix size of for directory trees.
            return path.length();
        }
        return 0;
    }

    /**
     * The {@link Bookkeeping} instance used by this object.
     */
    private final Bookkeeping bookkeeping;

    // constructors

    /**
     * The {@link PlacingPolicy} instance used by this object.
     */
    private final PlacingPolicy policy;

    // instance member method (alphabetic)

    /**
     * The {@link WarehouseManager} instance used by this object.
     */
    private final WarehouseManager warehouseManager;

    // static member methods (alphabetic)

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the Activity instance used to configure the new instance.
     * @param configurationDir
     *            the directory where configuration information of a
     *            {@link PlacingPolicy} may be found.
     * @param bookkeeping
     *            the Bookkeeping instance used by the new instance.
     * @param metadataManager
     *            the {@link MetadataManager} instance that will be passed to the
     *            PlacementPolicy used by this class.
     * @param warehouseManager
     *            the {@link WarehouseManager} instance used by the new instance.
     */
    public Placer(final Activity activity,
                  final File configurationDir,
                  final Bookkeeping bookkeeping,
                  final MetadataManager metadataManager,
                  final WarehouseManager warehouseManager) {
        this.bookkeeping = bookkeeping;
        policy = getPlacingPolicy(activity,
                                  LOG);
        policy.setEnvironment(configurationDir,
                              metadataManager);
        this.warehouseManager = warehouseManager;
    }

    /**
     * Places the relevant files for a bundle into the warehouse.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's file
     *            locations.
     *
     * @return the {@link IngestTicket} instance that results from successfully
     *         placing the bundle in the warehouse.
     *
     * @throws InterruptedException
     *             when this method is interrupted.
     * @throws IOException
     *             when there is an IO error.
     * @throws PolicyFailedException
     *             when there is failure in the {@link PlacingPolicy} instance.
     */
    public IngestTicket place(IngestTicket ticket) throws PolicyFailedException,
                                                   IOException,
                                                   InterruptedException {
        try {
            final File metadataFile = ticket.getMetadataFile();
            if (null == metadataFile) {
                throw new PolicyFailedException("No Metadata file was available.");
            }
            final File deliveredData = ticket.getDataDeliveryDirectory();
            final File wrappedData = ticket.getPackedFile();
            final File compressedData = ticket.getCompressedFile();
            final File placedMetadata = policy.getMetadataPlacement(metadataFile.getName(),
                                                                    metadataFile);
            final File plainData;
            final File placedData;
            if (null != deliveredData) {
                if (ticket.isSingle()) {
                    final File[] files = deliveredData.listFiles();
                    plainData = files[0];
                    placedData = policy.getDataPlacement(plainData.getName(),
                                                         metadataFile);
                } else {
                    plainData = deliveredData;
                    placedData = policy.getDataPlacement(deliveredData,
                                                         (ticket.getBundle()).getName(),
                                                         metadataFile);
                }
            } else {
                plainData = null;
                placedData = null;
            }
            final File placedWrapped;
            if (null != wrappedData) {
                placedWrapped = policy.getWrappedPlacement(wrappedData.getName(),
                                                           metadataFile);
            } else {
                placedWrapped = null;
            }
            final File placedCompressed;
            if (null != compressedData) {
                placedCompressed = policy.getCompressedPlacement(compressedData.getName(),
                                                                 metadataFile);
            } else {
                placedCompressed = null;
            }
            Location location = warehouseManager.add((ticket.getBundle()).getName(),
                                                     metadataFile,
                                                     plainData,
                                                     wrappedData,
                                                     compressedData,
                                                     placedMetadata,
                                                     placedData,
                                                     placedWrapped,
                                                     placedCompressed);
            long size = getSize(location.getMetadata()) + getSize(location.getData())
                        + getSize(location.getPacked())
                        + getSize(location.getCompressed());
            bookkeeping.setPlacement(ticket.getIdentity(),
                                     location.getIdentity(),
                                     size);
            return new IngestTicket(ticket,
                                    location.getMetadata(),
                                    location.getData(),
                                    location.getPacked(),
                                    location.getCompressed());
        } catch (PolicyFailedFailure e) {
            throw new PolicyFailure(null,
                                    e,
                                    e.getAlternateMessage());
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
