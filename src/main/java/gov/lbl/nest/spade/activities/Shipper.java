package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.registry.FileCategory;
import gov.lbl.nest.spade.registry.internal.InboundLocator;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.FileTransfer;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import gov.lbl.nest.spade.services.UriHandler;
import gov.lbl.nest.spade.services.VerificationManager;
import gov.lbl.nest.spade.workflow.ActivityDefermentException;
import gov.lbl.nest.spade.workflow.ActivityDefermentKey;

/**
 * The class ships the appropriate bundle file or files to a destination using
 * the ticket's specified outbound transfer.
 */
public class Shipper {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Shipper.class);

    /**
     * The {@link UriHandler}, if any, used to instrument all SPADE activities.
     */
    private static final UriHandler URI_HANDLER = UriHandler.getUriHandler();

    // private static member data

    // private instance member data

    /**
     * The {@link CacheDefinition} instance used by this object.
     */
    private final Bookkeeping bookkeeping;

    /**
     * The directory where configuration information of a {@link FileTransfer} may
     * be found.
     */
    private final File configurationDir;

    /**
     * The directory under which in which done files are created.
     */
    private final File doneDir;

    /**
     * The {@link NeighborhoodManager} instance used by this object.
     */
    private final NeighborhoodManager neighborhoodManager;

    /**
     * The {@link Verification} instance used by this object.
     */
    private final VerificationManager verificationManager;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param bookkeeping
     *            the {@link Bookkeeping} instance used by this object.
     * @param cacheDefinition
     *            the {@link CacheDefinition} instance used by this object.
     * @param configurationDir
     *            the directory where configuration information of a
     *            {@link FileTransfer} may be found.
     * @param neighborhoodManager
     *            the {@link NeighborhoodManager} instance used by this object.
     * @param verificationManager
     *            the {@link VerificationManager} instance used by this object.
     */
    public Shipper(final Activity activity,
                   final Bookkeeping bookkeeping,
                   final CacheDefinition cacheDefinition,
                   final File configurationDir,
                   final NeighborhoodManager neighborhoodManager,
                   final VerificationManager verificationManager) {
        this.bookkeeping = bookkeeping;
        this.configurationDir = configurationDir;
        this.doneDir = (cacheDefinition.getCacheMap()).get(Activity.SHIPPER);
        this.neighborhoodManager = neighborhoodManager;
        this.verificationManager = verificationManager;
    }

    // instance member method (alphabetic)

    /**
     * Shipping a Bundle one destination.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's file
     *            locations.
     * @param key
     *            the {@link ActivityDefermentKey} instance to use if this method
     *            defers its completion.
     *
     * @throws ActivityDefermentException
     *             when the shipping is offloaded.
     * @throws IOException
     *             when there is an IO error.
     * @throws InterruptedException
     *             when this method is interrupted.
     */
    public void ship(final IngestTicket ticket,
                     final ActivityDefermentKey key) throws ActivityDefermentException,
                                                     IOException,
                                                     InterruptedException {
        final Collection<String> outboundTransfers = ticket.getOutboundTransfers();
        if (1 != outboundTransfers.size()) {
            if (outboundTransfers.isEmpty()) {
                throw new IllegalStateException("No outbound transfers are specified where one must be utilized");
            }
            throw new IllegalStateException("Multiple outbound transfers are specified where only one can be utilized");
        }
        final String name = (outboundTransfers.iterator()).next();
        final OutboundTransfer outboundTransfer = neighborhoodManager.getOutboundTransfer(name);
        if (null == outboundTransfer) {
            throw new IllegalStateException("Unknown TransferDefinition, \"" + name
                                            + "\"");
        }
        final Bundle bundle = ticket.getBundle();
        LOG.info("Shipping \"" + bundle.getName()
                 + "\" via the outbound transfer \""
                 + outboundTransfer.getName()
                 + "\"");

        final String identity = ticket.getIdentity();
        final String deliveryPrefix = InboundLocator.V4_PREFIX + InboundLocator.SEPARATOR
                                      + identity
                                      + InboundLocator.SEPARATOR;
        final File metadataFile = ticket.getMetadataFile();
        final File transferFile = ticket.getTransferFile();
        final File shippersDir = new File(doneDir,
                                          outboundTransfer.getName());
        shippersDir.mkdirs();
        final File semaphoreFile = new File(shippersDir,
                                            transferFile.getName());
        final String localId = bundle.getLocalId();
        final File metadataFileToUse;
        final String targetMetadataNameToUse;
        final String targetPayloadName;
        if (metadataFile.equals(transferFile)) {
            targetPayloadName = deliveryPrefix + FileCategory.EMBEDDED
                                + InboundLocator.SEPARATOR
                                + transferFile.getName();
            metadataFileToUse = null;
            targetMetadataNameToUse = null;
        } else {
            metadataFileToUse = metadataFile;
            targetMetadataNameToUse = deliveryPrefix + FileCategory.METADATA
                                      + InboundLocator.SEPARATOR
                                      + metadataFile.getName();
            final FileCategory shippingCategory = ticket.getShippingCategory();
            targetPayloadName = deliveryPrefix + shippingCategory
                                + InboundLocator.SEPARATOR
                                + transferFile.getName();
        }

        final FileTransfer fileTransfer = outboundTransfer.getTransferImplementation();
        fileTransfer.setEnvironment(configurationDir);
        final String neighbor = outboundTransfer.getNeighbor();
        try {
            /**
             * Ensure the semaphore file is created fresh so its creation date is "now"
             */
            semaphoreFile.delete();
            semaphoreFile.createNewFile();

            bookkeeping.sending(identity,
                                neighbor);
            final URI uri;
            if (null == key) {
                uri = null;
            } else {
                uri = URI_HANDLER.getCallback(ticket.getBaseUri(),
                                              key);
            }
            final boolean synchronous = fileTransfer.send(outboundTransfer.getDeliveryLocation(),
                                                          metadataFileToUse,
                                                          targetMetadataNameToUse,
                                                          transferFile,
                                                          targetPayloadName,
                                                          semaphoreFile,
                                                          deliveryPrefix + FileCategory.DONE
                                                                         + InboundLocator.SEPARATOR
                                                                         + localId
                                                                         + InboundLocator.SEPARATOR
                                                                         + bundle.getName(),
                                                          uri);
            if (!synchronous) {
                throw new ActivityDefermentException(uri);
            }
            shipCompleted(identity,
                          neighbor);
        } catch (ActivityDefermentException e) {
            // Re-throw here to stop the next catch statement swallowing it.
            throw e;
        } catch (Throwable t) {
            /**
             * Simple error log to keep down verbosity.
             */
            LOG.error(t.getMessage());
            bookkeeping.abandon(identity,
                                neighbor);
            /**
             * Failure of a shipping should not stop the rest of the process, rather it
             * should be postponed. Re-shipping is now handled by PostShipper task.
             */
        } finally {
            semaphoreFile.delete();
        }
        return;
    }

    /**
     * Completes the shipping a Bundle one destination.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's file
     *            locations.
     * @param message
     *            the {@link Object} instance returned in order for the shipping to
     *            complete.
     */
    public void ship(final IngestTicket ticket,
                     final Object message) {
        final Collection<String> outboundTransfers = ticket.getOutboundTransfers();
        if (1 != outboundTransfers.size()) {
            if (outboundTransfers.isEmpty()) {
                throw new IllegalStateException("No outbound transfers are specified where one must be utilized");
            }
            throw new IllegalStateException("Multiple outbound transfers are specified where only one can be utilized");
        }
        final String name = (outboundTransfers.iterator()).next();
        final OutboundTransfer outboundTransfer = neighborhoodManager.getOutboundTransfer(name);
        final String identity = ticket.getIdentity();
        final String neighbor = outboundTransfer.getNeighbor();
        try {
            shipCompleted(identity,
                          neighbor);
        } catch (Throwable t) {
            t.printStackTrace();
            bookkeeping.abandon(identity,
                                neighbor);
            /**
             * Failure of a shipping should not stop the rest of the process, rather it
             * should be postponed. Re-shipping is now handled by PostShipper task.
             */
        } finally {
            final File transferFile = ticket.getTransferFile();
            final File shippersDir = new File(doneDir,
                                              outboundTransfer.getName());
            final File semaphoreFile = new File(shippersDir,
                                                transferFile.getName());
            semaphoreFile.delete();
        }
        return;
    }

    private void shipCompleted(final String identity,
                               final String neighbor) {
        verificationManager.delivered(identity,
                                      neighbor);
        bookkeeping.sent(identity,
                         neighbor);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
