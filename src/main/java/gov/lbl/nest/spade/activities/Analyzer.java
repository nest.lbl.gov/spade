package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.interfaces.storage.Location;

/**
 * This class submits a bundle for analysis.
 *
 * @author patton
 */
public class Analyzer {

    // public static final member data

    /**
     * The directory, in the destination, into which error files will be written.
     */
    public static final String ERR_DIR = "err";

    /**
     * The suffix used to create the default error file name.
     */
    public static final String ERR_SUFFIX = ".err";

    /**
     * The directory, in the destination, into which log files will be written.
     */
    public static final String LOG_DIR = "log";

    /**
     * The suffix used to create the default log file name.
     */
    public static final String LOG_SUFFIX = ".log";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The path to use when a file does not exist.
     */
    private static final String NO_PATH = "/dev/null";

    /**
     * The UTC {@link TimeZone}, used to build log file hierarchy.
     */
    private static final TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("Etc/UTC");

    /**
     * The formatter for date and time supplied to this class as Strings.
     */
    private static final DateFormat LONG_DATE_AND_TIME_FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    static {
        LONG_DATE_AND_TIME_FORMATTER.setTimeZone(UTC_TIMEZONE);
    }

    // private static member data

    // private instance member data

    /**
     * Extends the supplied directory by adding on calendar related sub-directories.
     *
     * @param directory
     *            the directory to extend.
     * @param dateTime
     *            the date and time to use for the extension
     *
     * @return the extended directory.
     */
    private static File addCalendarDirectories(final File directory,
                                               final Date dateTime) {
        final Calendar calendar = new GregorianCalendar(UTC_TIMEZONE);
        calendar.setTime(dateTime);
        final File year = new File(directory,
                                   String.format("%04d",
                                                 Integer.valueOf(calendar.get(Calendar.YEAR))));
        if (!year.exists()) {
            year.mkdir();
        }
        final File month = new File(year,
                                    String.format("%02d",
                                                  Integer.valueOf(calendar.get(Calendar.MONTH) + 1)));
        if (!month.exists()) {
            month.mkdir();
        }
        final File day = new File(month,
                                  String.format("%02d",
                                                Integer.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
        if (!day.exists()) {
            day.mkdir();
        }
        return day;
    }

    /**
     * the {@link MessageFormat} pattern that will be used to construct the analysis
     * command line.
     */
    private final String analysisCmd;

    /**
     * True if the failure of the analysis command should throw an exception.
     */
    private final boolean fatal;

    /**
     * The root relative to which log and error files will be written.
     */
    private final File logRoot;

    // constructors

    /**
     * The interval, if any, in seconds after which the {@link Command} instance is
     * considered to have timed out.
     */
    private Long retryInterval;

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     * 
     * @param activity
     *            the {@link Activity} instance declaring this task.
     */
    public Analyzer(Activity activity) {
        analysisCmd = activity.getParameter("command");
        final String outputDir = activity.getParameter("output");
        if (null == analysisCmd || null == outputDir) {
            fatal = Boolean.TRUE;
        } else {
            final String fatalParameter = activity.getParameter("fatal");
            if (null != fatalParameter) {
                fatal = Boolean.parseBoolean(fatalParameter);
            } else {
                fatal = false;
            }
        }
        if (null == outputDir) {
            this.logRoot = null;
            return;
        }
        this.logRoot = new File(outputDir);
        final String retryIntervalParameter = activity.getParameter("timeout");
        if (null == retryIntervalParameter) {
            retryInterval = null;
        } else {
            retryInterval = Long.parseLong(retryIntervalParameter);
        }
    }

    // static member methods (alphabetic)

    /**
     * Submits a bundle for analysis.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's file
     *            locations.
     *
     * @throws InterruptedException
     *             when the analysis is interrupted
     * @throws IOException
     *             when the analysis can not do I/O.
     * @throws ExecutionFailedException
     *             when the execution of the analysis fails.
     * @throws TimeoutException
     *             when the execution of the analysis take too long.
     */
    public void analyze(final IngestTicket ticket) throws IOException,
                                                   InterruptedException,
                                                   ExecutionFailedException,
                                                   TimeoutException {
        if (null == analysisCmd && null == logRoot) {
            throw new IOException("Neither the \"command\" or \"output\" parameter has not been defined");
        } else if (null == analysisCmd) {
            throw new IOException("The \"command\" parameter has not been defined");
        } else if (null == logRoot) {
            throw new IOException("The \"output\" parameter has not been defined");
        }

        final Location location = ticket.getPlacedFiles();
        final File compressedFile = location.getCompressedFile();
        final String compressedPath;
        if (null != compressedFile && compressedFile.exists()) {
            compressedPath = compressedFile.getAbsolutePath();
        } else {
            compressedPath = NO_PATH;
        }
        final File dataFile = location.getDataFile();
        final String dataPath;
        if (null != dataFile && dataFile.exists()) {
            dataPath = dataFile.getAbsolutePath();
        } else {
            dataPath = NO_PATH;
        }
        final File metaFile = location.getMetadataFile();
        final String metaPath;
        if (null != metaFile && metaFile.exists()) {
            metaPath = metaFile.getAbsolutePath();
        } else {
            metaPath = NO_PATH;
        }
        final File packedFile = location.getPackedFile();
        final String packedPath;
        if (null != packedFile && packedFile.exists()) {
            packedPath = packedFile.getAbsolutePath();
        } else {
            packedPath = NO_PATH;
        }

        final Date now = new Date();
        final String bundle = (ticket.getBundle()).getName();
        final String[] files = new String[] { bundle,
                                              metaPath,
                                              dataPath,
                                              packedPath,
                                              compressedPath,
                                              LONG_DATE_AND_TIME_FORMATTER.format(now) };
        final MessageFormat request = new MessageFormat(analysisCmd);
        final String cmdString = request.format(files);
        final String[] cmdArray = new String[] { "bash",
                                                 "-c",
                                                 cmdString };
        final Command command = new Command(cmdArray);

        final File outputDir = new File(addCalendarDirectories(logRoot,
                                                               now),
                                        LOG_DIR);
        if (!outputDir.exists()) {
            if (!outputDir.mkdirs() && !outputDir.exists()) {
                throw new FileNotFoundException("The analysis output directory, \"" + outputDir.getPath()
                                                + "\" could not be created.");
            }
        }
        File errorDir = new File(addCalendarDirectories(logRoot,
                                                        now),
                                 ERR_DIR);
        if (!errorDir.exists()) {
            if (!errorDir.mkdirs() && !errorDir.exists()) {
                throw new FileNotFoundException("The analysis error directory, \"" + errorDir.getPath()
                                                + "\" could not be created.");
            }
        }
        final File output = new File(outputDir,
                                     bundle + LOG_SUFFIX);
        final File error = new File(errorDir,
                                    bundle + ERR_SUFFIX);
        final TimeUnit units;
        if (null == retryInterval) {
            units = null;
        } else {
            units = TimeUnit.SECONDS;
        }
        command.execute(output,
                        error,
                        null,
                        retryInterval,
                        units);
        final ExecutionFailedException cmdException = command.getCmdException();
        if (null != cmdException) {
            if (fatal) {
                throw cmdException;
            }
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
