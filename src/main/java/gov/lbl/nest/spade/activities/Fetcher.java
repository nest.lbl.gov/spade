package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.interfaces.policy.FetchingPolicy;
import gov.lbl.nest.spade.policy.impl.LocalhostFetching;
import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.FileCategory;
import gov.lbl.nest.spade.registry.internal.InboundLocator;
import gov.lbl.nest.spade.registry.internal.InboundSemaphore;
import gov.lbl.nest.spade.registry.internal.InternalFileName;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.CacheManager;

/**
 * This class fetches the data files from their external locations and places
 * them inside the SPADE cache.
 *
 * @author patton
 *
 */
public class Fetcher {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Fetcher.class);

    // private static member data

    // private instance member data

    /**
     * Returns the {@link FetchingPolicy} instance for the supplied {@link Activity}
     * instance.
     *
     * @param activity
     *            the {@link Activity} instance whose {@link FetchingPolicy}
     *            instance should be returned.
     * @param logger
     *            the {@link Logger} instance though which the selected policy will
     *            be logged.
     *
     * @return the {@link FetchingPolicy} instance for the supplied {@link Activity}
     *         instance.
     */
    public static FetchingPolicy getFetchingPolicy(Activity activity,
                                                   Logger logger) {
        final FetchingPolicy policyToUse;
        if (null == activity) {
            policyToUse = new LocalhostFetching();
        } else {
            final FetchingPolicy policy = activity.getImplementedPolicy(FetchingPolicy.class);
            if (null == policy) {
                policyToUse = new LocalhostFetching();
            } else {
                policyToUse = policy;
            }
        }
        final Class<?> clazz = policyToUse.getClass();
        if (null != activity && null != logger) {
            logger.info("\"" + activity.getName()
                        + "\" is using "
                        + clazz.getCanonicalName()
                        + " as its policy");
        }
        return policyToUse;
    }

    /**
     * The {@link Bookkeeping} instance used by this object.
     */
    private final Bookkeeping bookkeeping;

    /**
     * The {@link CacheManager} instance used by this object.
     */
    private final CacheManager cacheManager;

    /**
     * The name of this activity.
     */
    private final String name;

    /**
     * The {@link FetchingPolicy} instance used by this object.
     */
    private final FetchingPolicy policy;

    /**
     * The interval, if any, in seconds that a fetch can be retried after it has
     * failed.
     */
    private final Long retryInterval;

    // constructors

    /**
     * The total factor used to define the minimum space needed for this activity by
     * multiplying by the number of threads.
     */
    private final int storageLoad;

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     * 
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param bookkeeping
     *            the {@link Bookkeeping} instance used by this object.
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     */
    public Fetcher(Activity activity,
                   Bookkeeping bookkeeping,
                   CacheManager cacheManager) {
        this.bookkeeping = bookkeeping;
        this.cacheManager = cacheManager;
        name = activity.getName();
        policy = getFetchingPolicy(activity,
                                   LOG);
        final String retryIntervalParameter = activity.getParameter("retry_interval");
        if (null == retryIntervalParameter) {
            retryInterval = null;
        } else {
            retryInterval = Long.parseLong(retryIntervalParameter);
        }
        storageLoad = activity.getStorageLoad();
    }

    // static member methods (alphabetic)

    /**
     * Places the relevant files for a bundle into the warehouse.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's file
     *            locations.
     * 
     * @return the updated {@link IngestTicket} instance.
     * 
     * @throws RetriableException
     *             when the fetch has failed,
     */
    public IngestTicket fetch(IngestTicket ticket) throws RetriableException {
        final Pattern inboundPattern;
        if ((ticket.getBundle()).isInbound()) {
            final InboundSemaphore inboundSemaphore = new InboundSemaphore(((ticket.getBundle()).getSemaphore()).getName());
            inboundPattern = InboundLocator.payloadPattern(InboundLocator.V4_PREFIX,
                                                           inboundSemaphore.getTicket());
        } else {
            inboundPattern = null;
        }
        FileCategory shippedCategory = null;
        try {
            final File dataDeliveryDirectory = ticket.getDataDeliveryDirectory();
            dataDeliveryDirectory.mkdir();

            final ExternalFile externalSemaphore = ticket.getExternalSemaphore();

            File firstFile = null;
            final boolean cacheData = (ticket.getOptions()).cacheData();
            long dataSize = 0;
            final Collection<ExternalFile> externalFiles = ticket.getExternalFiles();
            final Iterator<ExternalFile> iterator = externalFiles.iterator();
            while (iterator.hasNext()) {
                ExternalFile externalFile = iterator.next();
                if (null != shippedCategory && shippedCategory == FileCategory.EMBEDDED
                    && !iterator.hasNext()) {
                    /**
                     * Note, the externalFiles collection is "live" so this step removed the
                     * metadata file from the collection of external files.
                     */
                    iterator.remove();
                } else {
                    final File deliveryLocation = externalFile.getDeliveryLocation();
                    final File internalFile;
                    if (null == deliveryLocation || null == deliveryLocation.getPath()) {
                        externalFile.setDeliveryLocation(new File(externalFile.getName()));
                    }
                    internalFile = new File(dataDeliveryDirectory,
                                            (externalFile.getDeliveryLocation()).getPath());

                    /**
                     * If the internal file has not already been fetched do so now.
                     */
                    if (!internalFile.exists()) {
                        // Clean up any previous attempt
                        final File tmpFile = new File(internalFile.getParent(),
                                                      internalFile.getName() + InternalFileName.TMP_SUFFIX);
                        cacheManager.delete(tmpFile);

                        // Then get the current one.
                        cacheManager.waitForSpace(tmpFile,
                                                  storageLoad,
                                                  name);
                        /*
                         * The 'receivedFile' resolves any wild-card that is part of the original
                         * 'externalFile'
                         */
                        final ExternalFile receivedFile = policy.receive(externalFile,
                                                                         tmpFile,
                                                                         !cacheData);
                        externalFile.replace(receivedFile);
                        if (null != inboundPattern && null == shippedCategory) {
                            final Matcher matcher = inboundPattern.matcher(receivedFile.getName());
                            if (matcher.matches()) {
                                shippedCategory = Enum.valueOf(FileCategory.class,
                                                               matcher.group(InboundLocator.FILECATEGORY_INDEX));
                            }
                        }

                        final File destinationFile = receivedFile.getDeliveryLocation();
                        cacheManager.move(tmpFile,
                                          destinationFile);

                        // Test nothing changed since fetching commenced
                        final long currentStamp = policy.getLastModified(externalSemaphore);
                        if (0 == currentStamp) {
                            final File internalSemaphore = externalSemaphore.getDeliveryLocation();
                            cacheManager.delete(internalSemaphore);
                            cacheManager.delete(destinationFile);
                            throw new InvalidatedIngestException("Fetching of file(s) associated with semaphore file \"" + internalSemaphore
                                                                 + "\" abandoned as that semaphore file was removed before fetching completed");
                        }
                        if (ticket.getPrefetchStamp() != currentStamp) {
                            final File internalSemaphore = externalSemaphore.getDeliveryLocation();
                            cacheManager.delete(internalSemaphore);
                            cacheManager.delete(destinationFile);
                            throw new InvalidatedIngestException("Fetching of file(s) associated with semaphore file \"" + internalSemaphore
                                                                 + "\" abandoned as that semaphore file was changed before fetching completed");
                        }

                        dataSize += destinationFile.length();
                        if ((ticket.getBundle()).isInbound()) {
                            if (null == firstFile) {
                                bookkeeping.setBinarySize(ticket.getIdentity(),
                                                          destinationFile.length());
                                firstFile = destinationFile;
                            } else {
                                bookkeeping.setMetadataSize(ticket.getIdentity(),
                                                            destinationFile.length());
                            }
                        }
                    }
                }
            }
            if (!(ticket.getBundle()).isInbound()) {
                bookkeeping.setBinarySize(ticket.getIdentity(),
                                          dataSize);
            }
            if (cacheData) {
                policy.delete(externalFiles);
            }
            policy.delete(externalSemaphore);
            if (null == shippedCategory) {
                return ticket;
            }
            return new IngestTicket(ticket,
                                    shippedCategory,
                                    firstFile);
        } catch (IOException t) {
            if (t instanceof FileNotFoundException) {
                throw new FetcherFailure(null,
                                         t,
                                         retryInterval,
                                         null);
            } else {
                final Throwable cause = t.getCause();
                if (null != cause && cause instanceof IllegalStateException) {
                    throw new FetcherFailure(null,
                                             cause,
                                             retryInterval,
                                             null);
                } else {
                    throw new FetcherException(null,
                                               t,
                                               retryInterval);
                }
            }
        } catch (Throwable t) {
            throw new FetcherException(null,
                                       t,
                                       retryInterval);
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
