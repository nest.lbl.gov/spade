package gov.lbl.nest.spade.activities;

/**
 * This class is used to manipulate {@link IngestTicket} instances within the
 * BPMN workflow.
 *
 * @author patton
 */
public class Tickets {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Copies the supplied {@link IngestTicket} instance and returns the copy.
     *
     * @param ticket
     *            the {@link IngestTicket} in be copied.
     *
     * @return the copy of the supplied {@link IngestTicket} instance
     */
    public static IngestTicket copy(final IngestTicket ticket) {
        return new IngestTicket(ticket);
    }

    /**
     * Merges the supplied collection of {@link IngestTicket} instances into a
     * single new instance.
     *
     * @param lhs
     *            the first {@link IngestTicket} instance to be merged.
     * @param rhs
     *            the {@link IngestTicket} instance to be merged with the first one.
     *
     * @return the resulting merged {@link IngestTicket} instance
     */
    public static IngestTicket merge(final IngestTicket lhs,
                                     final IngestTicket rhs) {
        final IngestTicket result = new IngestTicket(lhs);
        return result.merge(rhs);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
