package gov.lbl.nest.spade.activities;

/**
 * The exception is thrown when the cause of an exception may be fixed
 * externally and so should be retried after the specified interval.
 *
 * @author patton
 */
public class RetriableException extends
                                Exception {

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = -1L;

    /**
     * The interval, if any, in seconds that a fetch can be retried after it has
     * failed.
     */
    protected final Long retryInterval;

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the detail message (which is saved for later retrieval by the
     *            Throwable.getMessage() method).
     * @param cause
     *            the Exception that caused this object was thrown.
     * @param retryInterval
     *            the interval, if any, in seconds that a fetch can be retried after
     *            it has failed.
     */
    protected RetriableException(String message,
                                 Throwable cause,
                                 final Long retryInterval) {
        super(message,
              cause);
        this.retryInterval = retryInterval;
    }

    /**
     * Returns the interval, if any, in seconds that a fetch can be retried after it
     * has failed.
     *
     * @return the interval, if any, in seconds that a fetch can be retried after it
     *         has failed.
     */
    public Long getRetryInterval() {
        return retryInterval;
    }

}