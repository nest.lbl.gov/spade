package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.TicketManager;
import jakarta.ejb.TimerConfig;
import jakarta.ejb.TimerService;
import jakarta.xml.bind.JAXBException;

/**
 * This class is executed when the ingestion workflow terminates.
 *
 * @author patton
 */
public class IngestTerminationImpl implements
                                   IngestTermination {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(IngestTerminationImpl.class);

    /**
     * True when process instance should be preserved after a non-fetch failure.
     */
    private static final boolean PRESERVE_FAILED_INSTANCE = true;

    /**
     * True when process instance should be preserved after a fetch failure. Note:
     * each attempted fetch of a file will create a new process instance, so
     * normally this should be set to 'false'.
     */
    private static final boolean PRESERVE_FAILED_FETCH_INSTANCE = false;

    /**
     * dataFile + packagedFile + compressedFile = 3+, so use 4.
     */
    private static final int PROBLEM_FILE_STORAGE_FACTOR = 4;

    // private static member data

    /**
     * The current highest value for the unique problem directory.
     */
    private static AtomicInteger uniqueFileCycle = new AtomicInteger(-1);

    // private instance member data

    /**
     * Conditionally added the appropriate file to the files parameter
     *
     * @param files
     *            the {@link Collection} of file being moved.
     * @param internalSemaphore
     *            the file that existed, if any, when the problem, occurred.
     * @param ticket
     *            the fallback file taken from the {@link IngestTicket}.
     */
    private static void conditionallyAddFile(final Collection<File> files,
                                             final File explicitFile,
                                             final File fallbackFile) {
        final File fileToUse;
        if (null == explicitFile) {
            fileToUse = fallbackFile;
        } else {
            fileToUse = explicitFile;
        }
        if (null != fileToUse && fileToUse.exists()) {
            files.add(fileToUse);
        }
    }

    /**
     * Returns a unique sub-directory of the supplied parent directory.
     *
     * @param parent
     *            the parent directory of the required sub-directory.
     * @param cycleForAction
     *            the current highest value for the unique directory. <b>Note:</b>
     *            this object is updated by this method.
     *
     * @return a unique sub-directory of the supplied parent directory.
     *
     * @throws FileNotFoundException
     *             when the unique directory can not be found nor created.
     */
    static File getUniqueDir(final File parent,
                             AtomicInteger cycleForAction) throws FileNotFoundException {
        synchronized (cycleForAction) {
            if (-1 == cycleForAction.get()) {
                if (-1 == cycleForAction.get()) {
                    final String[] directories = parent.list();
                    int greatestExisting = -1;
                    if (null != directories) {
                        for (String directory : directories) {
                            try {
                                final int cycle = Integer.parseInt(directory);
                                if (cycle > greatestExisting) {
                                    greatestExisting = cycle;
                                }
                            } catch (NumberFormatException e) {
                                // Ignore this file name.
                            }
                        }
                    }
                    cycleForAction.set(greatestExisting + 1);
                }
            }

            File uniqueDir = new File(parent,
                                      cycleForAction.toString());
            int cycle = cycleForAction.get() + 1;
            while (uniqueDir.exists()) {
                uniqueDir = new File(parent,
                                     Integer.toString(cycle));
            }
            if (!uniqueDir.mkdir()) {
                throw new FileNotFoundException("Unable to create unique problem directory");
            }
            cycleForAction.set(cycle);
            return uniqueDir;
        }
    }

    /**
     * It the specified {@link Throwable} is a {@link ExecutionFailedException} of a
     * failed {@link Command} instance then writes the 'stderr' to the log file,
     * otherwise does nothing.
     *
     * @param c
     *            the {@link Throwable} that may have come for a failed
     *            {@link Command} instance.
     */
    private static void logStdErr(final Throwable c) {
        if (c instanceof ExecutionFailedException) {
            final String[] stdErr = ((ExecutionFailedException) c).getStdErr();
            if (null != stdErr) {
                for (String s : stdErr) {
                    LOG.error(s);
                }
            }
        }
    }

    /**
     * The {@link CacheManager} instance used by this class.
     */
    private CacheManager cacheManager;

    // constructors

    /**
     * <code>true</code> if details of a workflow failure should be preserved.
     */
    private final boolean preserve;

    /**
     * The directory below which problem files are saved.
     */
    private final File problemDir;

    // instance member method (alphabetic)

    /**
     * The {@link TicketManager} instance used by this object.
     */
    private TicketManager ticketManager;

    /**
     * The {@link TimerService} instance used to retry fetching.
     */
    private final TimerService timerService;

    /**
     * Creates an instance of this class.
     *
     * @param problemDir
     *            the directory below which problem files are saved.
     * @param preserve
     *            <code>true</code> if details of a workflow failure should be
     *            preserved.
     * @param cacheManager
     *            the {@link CacheManager} instance used by this class.
     * @param ticketManager
     *            the {@link TicketManager} instance used by this object.
     * @param timerService
     *            the {@link TimerService} instance used to retry fetching.
     */
    public IngestTerminationImpl(File problemDir,
                                 boolean preserve,
                                 CacheManager cacheManager,
                                 TicketManager ticketManager,
                                 TimerService timerService) {
        this.cacheManager = cacheManager;
        this.preserve = preserve;
        this.problemDir = problemDir;
        this.ticketManager = ticketManager;
        this.timerService = timerService;
    }

    @Override
    public boolean failed(final IngestTicket ticket,
                          final String name,
                          final Object rescue,
                          final Throwable thrown) {
        if (!preserve) {
            return false;
        }
        if (thrown instanceof InvalidatedIngestException) {
            return false;
        }
        if (thrown instanceof ExaminerException) {
            final ExaminerException e = (ExaminerException) thrown;
            return retriableFailure(ticket,
                                    name,
                                    rescue,
                                    e);
        } else if (thrown instanceof FetcherException) {
            final RetriableException e = (RetriableException) thrown;
            return retriableFailure(ticket,
                                    name,
                                    rescue,
                                    e);
        } else if (thrown instanceof PackerException) {
            saveProblemFiles(name,
                             rescue,
                             thrown.getCause(),
                             ticket,
                             null,
                             null,
                             null,
                             ((PackerException) thrown).getPackedFile(),
                             null);
        } else if (thrown instanceof CompressorException) {
            saveProblemFiles(name,
                             rescue,
                             thrown.getCause(),
                             ticket,
                             null,
                             null,
                             null,
                             null,
                             ((CompressorException) thrown).getCompressedFile());
        } else {
            saveProblemFiles(name,
                             rescue,
                             thrown,
                             ticket);
        }
        ticketManager.dispose(ticket);
        return PRESERVE_FAILED_INSTANCE;
    }

    @Override
    public boolean failed(Object ticket,
                          String name,
                          Object rescue,
                          Throwable t) {
        return failed((IngestTicket) ticket,
                      name,
                      rescue,
                      t);
    }

    /**
     * Moves all of the problem files into the problem's unique directory.
     *
     * @param uniqueDir
     *            the Directory into which to move the files
     * @param files
     *            the collection of File instances to be moved
     *
     * @throws IOException
     *             if one or more of the files could not be moved.
     */
    private void moveFiles(final File uniqueDir,
                           final Collection<File> files) throws IOException {
        final File target = new File(uniqueDir,
                                     "files");
        target.mkdirs();
        int failedCount = 0;
        for (File file : files) {
            if (!cacheManager.move(file,
                                   new File(target,
                                            file.getName()))) {
                ++failedCount;
            }
        }

        if (0 != failedCount) {
            final String plural;
            if (1 == failedCount) {
                plural = "";
            } else {
                plural = "s";
            }
            throw new IOException(failedCount + " failure"
                                  + plural
                                  + " moving files into problem directory \""
                                  + uniqueDir
                                  + "\"");
        }
    }

    // static member methods (alphabetic)

    /**
     * This method handle a failure within the fetcher task.
     *
     * @param ticket
     *            the resulting ticket of the workflow.
     * @param name
     *            the name of the task that failed.
     * @param e
     *            the {@link FetcherException} instance that cause the workflow to
     *            fail.
     *
     * @return
     */
    private boolean retriableFailure(final IngestTicket ticket,
                                     final String name,
                                     final Object rescue,
                                     final RetriableException e) {
        ticketManager.block(ticket,
                            e.getMessage());
        saveProblemFiles(name,
                         rescue,
                         e,
                         ticket);
        final Long interval = e.getRetryInterval();
        if (null == interval || 0 > interval.longValue()) {
            // Do not preserve process instance as new will will be created
            // next time the file is fetched.
            return PRESERVE_FAILED_FETCH_INSTANCE;
        }

        e.printStackTrace();
        logStdErr(e.getCause());
        final TimerConfig timerConfig = new TimerConfig();
        timerConfig.setInfo(ticket);
        timerConfig.setPersistent(false);
        timerService.createSingleActionTimer(TimeUnit.MILLISECONDS.convert(interval.longValue(),
                                                                           TimeUnit.SECONDS),
                                             timerConfig);
        // Do not preserve process instance as new will will be created
        // next time the file is fetched.
        return PRESERVE_FAILED_FETCH_INSTANCE;
    }

    /**
     * Saves as much information as possible about a problem.
     *
     * @param name
     *            the name of the task that failed.
     * @param rescue
     *            the {@link Object} instance that can be used to rescue a failed
     *            workflow once its issue has been resolved.
     * @param t
     *            the {@link Throwable} containing the reason for the problem.
     * @param ticket
     *            the {@link TransferTicket} instance that failed.
     *
     * @return the File into which the files were placed.
     */
    private final File saveProblemFiles(String name,
                                        Object rescue,
                                        Throwable t,
                                        IngestTicket ticket) {
        return saveProblemFiles(name,
                                rescue,
                                t,
                                ticket,
                                null,
                                null,
                                null,
                                null,
                                null);
    }

    /**
     * Saves as much information as possible about a failed SPADE operation.
     *
     * @param name
     *            the name of the task that failed.
     * @param rescue
     *            the {@link Object} instance that can be used to rescue a failed
     *            workflow once its issue has been resolved.
     * @param t
     *            the {@link Throwable} containing the reason for the problem.
     * @param ticket
     *            the {@link TransferTicket} instance that failed.
     * @param internalSemaphore
     *            the file, if any, containing the internal semaphore file when the
     *            problem, occurred.
     * @param metadataFile
     *            the file, if any, containing the metadata file when the problem,
     *            occurred.
     * @param dataDeliveryDirectory
     *            the directory, if any, containing the data files when the problem,
     *            occurred.
     * @param packedFile
     *            the file, if any, that is the packed file when the problem,
     *            occurred.
     * @param compressedFile
     *            the file, if any, that is the compressed file when the problem,
     *            occurred.
     *
     * @return the File into which the files were placed.
     */
    private final File saveProblemFiles(final String name,
                                        final Object rescue,
                                        final Throwable t,
                                        final IngestTicket ticket,
                                        final File internalSemaphore,
                                        final File metadataFile,
                                        final File dataDeliveryDirectory,
                                        final File packedFile,
                                        final File compressedFile) {
        try {
            LOG.error("Attempting to save problem files for \"" + ticket.getIdentity()
                      + "\"");
            final File uniqueDir = getUniqueDir(problemDir,
                                                uniqueFileCycle);
            boolean waitingForSpace = true;
            while (waitingForSpace) {
                try {
                    cacheManager.waitForSpace(uniqueDir,
                                              PROBLEM_FILE_STORAGE_FACTOR,
                                              name);
                    waitingForSpace = false;
                } catch (TimeoutException e1) {
                    // Ignore timeout an keep trying.
                } catch (InterruptedException e1) {
                    // Try and finish
                    waitingForSpace = false;
                }
            }
            try {
                ticket.write(new File(uniqueDir,
                                      name + "."
                                                 + rescue.toString()));
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            if (null != t) {
                try (final PrintWriter pw = new PrintWriter(new File(uniqueDir,
                                                                     "throwable.stacktrace"))) {
                    pw.println("Class: " + t.getClass());
                    Throwable thrown = t;
                    while (null != thrown) {
                        if (thrown instanceof ExecutionFailedException) {
                            final ExecutionFailedException e = ((ExecutionFailedException) thrown);
                            pw.println("External Command: " + e.getCmdString());
                            pw.println("Begin standard error dump:");
                            final String[] stdErr = e.getStdErr();
                            if (null != stdErr) {
                                for (String line : stdErr) {
                                    pw.println("  " + line);
                                }
                            }
                            pw.println("End standard error");
                            thrown = null;
                        } else {
                            final Throwable cause = thrown.getCause();
                            if (null != cause && cause != thrown) {
                                thrown = cause;
                            } else {
                                thrown = null;
                            }
                        }
                    }
                    t.printStackTrace(pw);
                    pw.close();
                }
            }

            final Collection<File> files = new ArrayList<>();
            conditionallyAddFile(files,
                                 internalSemaphore,
                                 ticket.getInternalSemaphore());
            conditionallyAddFile(files,
                                 metadataFile,
                                 ticket.getMetadataFile());
            conditionallyAddFile(files,
                                 dataDeliveryDirectory,
                                 ticket.getDataDeliveryDirectory());
            conditionallyAddFile(files,
                                 packedFile,
                                 ticket.getPackedFile());
            conditionallyAddFile(files,
                                 compressedFile,
                                 ticket.getCompressedFile());

            if (!files.isEmpty()) {
                moveFiles(uniqueDir,
                          files);
            }

        } catch (IOException e1) {
            LOG.error("Failed to save all problem files for \"" + ticket.getIdentity());
            e1.printStackTrace();
        }
        return null;
    }

    @Override
    public void succeeded(final IngestTicket ticket) {
        ticketManager.dispose(ticket);
    }

    @Override
    public void succeeded(Object ticket) {
        succeeded((IngestTicket) ticket);

    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
