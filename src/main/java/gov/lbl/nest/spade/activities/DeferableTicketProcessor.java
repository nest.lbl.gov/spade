package gov.lbl.nest.spade.activities;

/**
 * This class is used to mark sure a task that only processes the ticket but
 * does not transform it.
 *
 * @author patton
 *
 * @param <T>
 *            the class of ticket that is transformed.
 */
public interface DeferableTicketProcessor<T> extends
                                         TicketProcessor<T> {

    /**
     * Completes the task.
     *
     * @param ticket
     *            the ticket to be preserved.
     * @param message
     *            the {@link Object} instance returned in order for the shipping to
     *            complete.
     *
     * @throws Exception
     *             when there is a problem preserving the ticket.
     */
    void completeTicket(final T ticket,
                        final Object message) throws Exception;
}
