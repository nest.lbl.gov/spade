package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.policy.PlacingPolicy;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedException;
import gov.lbl.nest.spade.interfaces.storage.ArchiveFailureException;
import gov.lbl.nest.spade.interfaces.storage.ArchiveManager;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.SpoolManager;

/**
 * This class wraps a archive the transfer file and any associated metadata for
 * a bundle.
 *
 * @author patton
 */
public class Archiver {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Archiver.class);

    // private static member data

    // private instance member data

    /**
     * The {@link ArchiveManager} instance used by this object.
     */
    private final ArchiveManager archiveManager;

    /**
     * The {@link Bookkeeping} instance used by this object.
     */
    private final Bookkeeping bookkeeping;

    /**
     * True if the failure of the archive command should throw an exception.
     */
    private final boolean fatal;

    /**
     * The {@link PlacingPolicy} instance used by this object.
     */
    private final PlacingPolicy policy;

    /**
     * The name of this activity.
     */
    private final String name;

    /**
     * The total factor used to define the minimum space needed for this activity by
     * multiplying by the number of threads.
     */
    private final int storageLoad;

    /**
     * The {@link SpoolManager} instance used by this object.
     */
    private final SpoolManager spoolManager;

    // constructors

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param configurationDir
     *            the directory where configuration information of a
     *            {@link PlacingPolicy} may be found.
     * @param archiveManager
     *            the {@link ArchiveManager} instance used by the new instance.
     * @param bookkeeping
     *            the {@link Bookkeeping} instance used by the new instance.
     * @param metadataManager
     *            the {@link MetadataManager} instance that will be passed to the
     *            PlacementPolicy used by this class.
     * @param spoolManager
     *            the {@link SpoolManager} instance used by the new instance.
     */
    public Archiver(final Activity activity,
                    final File configurationDir,
                    final ArchiveManager archiveManager,
                    final Bookkeeping bookkeeping,
                    final MetadataManager metadataManager,
                    final SpoolManager spoolManager) {
        this.archiveManager = archiveManager;
        this.bookkeeping = bookkeeping;
        fatal = false;
        name = activity.getName();
        this.policy = Placer.getPlacingPolicy(activity,
                                              LOG);
        policy.setEnvironment(configurationDir,
                              metadataManager);
        this.spoolManager = spoolManager;
        storageLoad = activity.getStorageLoad();
    }

    /**
     * Archives the transfer file referred to by the {@link IngestTicket} instance.
     *
     * @param ticket
     *            the {@link IngestTicket} instance whose transfer file should be
     *            archived.
     *
     * @return the {@link IngestTicket} instance that results from this attempt to
     *         archive a bundle.
     *
     * @throws TimeoutException
     *             when the wrapping takes too long.
     * @throws InterruptedException
     *             when the waiting for space method has been interrupted.
     * @throws IOException
     *             when there is a problem with the I/O.
     * @throws ArchiveFailureException
     *             when the file can not be successfully archived or spooled.
     * @throws PolicyFailedException
     *             when the policy can not complete its responsibilities.
     */
    public IngestTicket archive(final IngestTicket ticket) throws TimeoutException,
                                                           InterruptedException,
                                                           IOException,
                                                           ArchiveFailureException,
                                                           PolicyFailedException {
        final File archiveMetadata = ticket.getArchiveMetadata();
        if (null != archiveMetadata && !archiveMetadata.exists()) {
            throw new FileNotFoundException(archiveMetadata.getPath() + " does not exist");
        }
        final File archiveFile = ticket.getArchiveFile();
        if (!archiveFile.exists()) {
            throw new FileNotFoundException(archiveFile.getPath() + " does not exist");
        }
        final File archivePlacement = policy.getArchivePlacement(archiveFile.getName(),
                                                                 ticket.getMetadataFile());

        final boolean isSpooled = spoolManager.isSpoolDirectory(archiveFile.getParentFile());

        boolean preserveTransferFile = false;
        try {
            if (null == archiveMetadata) {
                archiveManager.add(archiveFile,
                                   archivePlacement);
            } else {
                archiveManager.add(archiveFile,
                                   archiveMetadata,
                                   archivePlacement);
            }

            final String bundle = (ticket.getBundle()).getName();
            final String archivedName = archiveFile.getName();
            bookkeeping.setArchived(ticket.getIdentity(),
                                    archivedName.substring(bundle.length()));
            if (isSpooled) {
                spoolManager.removeFromSpool(ticket);
                LOG.info("Deleted archived spool files for \"" + bundle
                         + "\" along with its outbound spool directory");
            }
        } catch (ArchiveFailureException e) {
            if (fatal) {
                throw e;
            }
            /*
             * Note: This is not considered a terminal failure so the file is saved to the
             * spool for later archiving.
             */
            final String bundle = (ticket.getBundle()).getName();
            LOG.warn("Archiving of bundle \"" + bundle
                     + "\" failed so it is postponed until later");
            if (!isSpooled) {
                spoolManager.placeInSpool(ticket,
                                          name,
                                          storageLoad,
                                          archiveFile,
                                          ticket.getMetadataFile());
                preserveTransferFile = true;
            }
        }

        /**
         * In the case where this is a re-archive the transfer file will be the spool
         * copy and therefore should not be deleted.
         */
        return new IngestTicket(ticket,
                                preserveTransferFile);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
