package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.interfaces.policy.CompressingPolicy;
import gov.lbl.nest.spade.policy.impl.GzipCompression;
import gov.lbl.nest.spade.registry.internal.InternalFileName;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.CacheManager;

/**
 * This class compresses a wrapped bundle and any associated semaphore file in
 * order to create a packaged file ready for shipping.
 *
 * @author patton
 */
public class Compressor {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Compressor.class);

    // private static member data

    // private instance member data

    /**
     * Returns the {@link CompressingPolicy} instance for the supplied
     * {@link Activity} instance.
     *
     * @param activity
     *            the {@link Activity} instance whose {@link CompressingPolicy}
     *            instance should be returned.
     * @param logger
     *            the {@link Logger} instance though which the selected policy will
     *            be logged.
     *
     * @return the {@link CompressingPolicy} instance for the supplied
     *         {@link Activity} instance.
     */
    public static CompressingPolicy getCompressionPolicy(Activity activity,
                                                         Logger logger) {
        final CompressingPolicy policyToUse;
        if (null == activity) {
            policyToUse = new GzipCompression();
        } else {
            final CompressingPolicy policy = activity.getImplementedPolicy(CompressingPolicy.class);
            if (null == policy) {
                policyToUse = new GzipCompression();
            } else {
                policyToUse = policy;
            }
        }
        final Class<?> clazz = policyToUse.getClass();
        if (null != activity && null != logger) {
            logger.info("\"" + activity.getName()
                        + "\" is using "
                        + clazz.getCanonicalName()
                        + " as its policy");
        }
        return policyToUse;
    }

    /**
     * The {@link Bookkeeping} instance used by this object.
     */
    private final Bookkeeping bookkeeping;

    /**
     * The {@link CacheManager} instance used by this object.
     */
    private final CacheManager cacheManager;

    /**
     * The directory into which to write the compressed file.
     */
    private final File compressorDir;

    /**
     * The name of this activity.
     */
    private final String name;

    /**
     * The {@link CompressingPolicy} instance used by this object.
     */
    private final CompressingPolicy policy;

    // constructors

    /**
     * The total factor used to define the minimum space needed for this activity by
     * multiplying by the number of threads.
     */
    private final int storageLoad;

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     * 
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param bookkeeping
     *            the {@link Bookkeeping} instance used by this object.
     * @param cacheDefinition
     *            the {@link CacheDefinition} instance used by this object.
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     */
    public Compressor(Activity activity,
                      Bookkeeping bookkeeping,
                      CacheDefinition cacheDefinition,
                      CacheManager cacheManager) {
        this.bookkeeping = bookkeeping;
        this.cacheManager = cacheManager;
        this.compressorDir = (cacheDefinition.getCacheMap()).get(Activity.COMPRESSOR);
        name = activity.getName();
        policy = getCompressionPolicy(activity,
                                      LOG);
        storageLoad = activity.getStorageLoad();
    }

    // static member methods (alphabetic)

    /**
     * Compresses a wrapped bundle and any associated semaphore file in order to
     * create a packaged file ready for shipping.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's
     *            wrapped file.
     *
     * @return the {@link IngestTicket} instance that results from successfully
     *         compressing the bundle.
     *
     * @throws IOException
     *             when the compressed file can not the written.
     * @throws FileNotFoundException
     *             if the file(s) to be compressed can not be found.
     * @throws ExecutionFailedException
     *             when the there is a failure in the compression application.
     * @throws InterruptedException
     *             when the the compression is interrupted
     * @throws TimeoutException
     *             when the compression take too long.
     */
    public IngestTicket compress(final IngestTicket ticket) throws ExecutionFailedException,
                                                            FileNotFoundException,
                                                            InterruptedException,
                                                            IOException,
                                                            TimeoutException {

        final File packedFile = ticket.getPackedFile();
        if (null == packedFile) {
            throw new FileNotFoundException("No Packed file was supplied");
        } else if (!packedFile.exists()) {
            throw new FileNotFoundException(packedFile.getPath() + " does not exist");
        }
        final String identity = ticket.getIdentity();
        final File directory = new File(compressorDir,
                                        identity);

        final String suffix = policy.getCompressedSuffixFromPackedFile(packedFile.getName());
        final File compressedFile = new File(directory,
                                             (ticket.getBundle()).getName() + suffix);
        final File tmpFile = new File(compressedFile.getParentFile(),
                                      compressedFile.getName() + InternalFileName.TMP_SUFFIX);
        cacheManager.delete(tmpFile);

        if (!compressedFile.exists()) {
            cacheManager.waitForSpace(tmpFile,
                                      storageLoad,
                                      name);
            if (!directory.mkdirs()) {
                throw new FileNotFoundException("The compression working directory, \"" + directory.getPath()
                                                + "\" could not be created.");
            }
            try {
                policy.compress(packedFile,
                                tmpFile);
            } catch (ExecutionFailedException e) {
                cacheManager.delete(tmpFile);
                throw e;
            }
            if (ticket.isUnplaced()) {
                bookkeeping.setCompressedSize(identity,
                                              tmpFile.length());
            }
            cacheManager.move(tmpFile,
                              compressedFile);
        }
        return new IngestTicket(ticket,
                                (File) null,
                                null,
                                compressedFile);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
