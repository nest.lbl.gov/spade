package gov.lbl.nest.spade.activities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.OutboundTransfer;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import gov.lbl.nest.spade.services.RetryManager;
import gov.lbl.nest.spade.services.VerificationManager;
import jakarta.xml.bind.JAXBException;

/**
 * The class deals with the result of shipping a Bundle one or more
 * destinations.
 */
public class PostShipper {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * The {@link NeighborhoodManager} instance used by this object.
     */
    private final NeighborhoodManager neighborhoodManager;

    /**
     * The {@link RetryManager} instance used by this object.
     */
    private final RetryManager retryManager;

    /**
     * The {@link Verification} instance used by this object.
     */
    private final VerificationManager verificationManager;

    /**
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param neighborhoodManager
     *            the {@link NeighborhoodManager} instance used by this object.
     * @param retryManager
     *            the {@link RetryManager} instance used by this object.
     * @param verificationManager
     *            the {@link VerificationManager} instance used by this object.
     */
    public PostShipper(final Activity activity,
                       final NeighborhoodManager neighborhoodManager,
                       final RetryManager retryManager,
                       final VerificationManager verificationManager) {
        this.neighborhoodManager = neighborhoodManager;
        this.retryManager = retryManager;
        this.verificationManager = verificationManager;
    }

    // instance member method (alphabetic)

    /**
     * Deals with the result of shipping a Bundle one or more destinations.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's file
     *            locations.
     *
     * @throws IOException
     *             when the files can no be stored for a retry.
     * @throws JAXBException
     *             when the ticket to be used for the retry can not be saved.
     */
    public void postShip(final IngestTicket ticket) throws IOException,
                                                    JAXBException {
        /**
         * The move to the retry directory is done here as so that is does not impact
         * the latency of delivery. Moreover it then takes place in parallel with the
         * reception of the file at the remote SPADE instances.
         */
        IngestTicket retryTicket = retryManager.prepare(ticket);

        final String identity = ticket.getIdentity();
        final Collection<String> undelivered = verificationManager.getUndeliveredTransfers(identity);
        if (null == undelivered || undelivered.isEmpty()) {
            verificationManager.start(identity);
            return;
        }

        final List<String> reships = new ArrayList<>();
        final Collection<String> outboundTransfers = ticket.getOutboundTransfers();
        for (String transferName : outboundTransfers) {
            final OutboundTransfer outboundTransfer = neighborhoodManager.getOutboundTransfer(transferName);
            if (undelivered.contains(outboundTransfer.getNeighbor())) {
                reships.add(transferName);
            }
        }
        retryManager.postponed(retryTicket,
                               reships);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
