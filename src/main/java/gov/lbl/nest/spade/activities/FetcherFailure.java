package gov.lbl.nest.spade.activities;

import gov.lbl.nest.spade.services.NonStrackTraceException;

/**
 * This class is an extension of the {@link FetcherException} class to add on
 * the {@link NonStrackTraceException} interface in order to suppress stacktrace
 * outputs.
 *
 * @author patton
 *
 */
public class FetcherFailure extends
                            FetcherException implements
                            NonStrackTraceException {
    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    /**
     * The message to output as an alternate to executing a
     * <code>printStackTrace</code> method.
     */
    private final String alternate;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the detail message (which is saved for later retrieval by the
     *            Throwable.getMessage() method).
     * @param cause
     *            the Exception that caused this object was thrown.
     * @param retryInterval
     *            the interval, if any, in seconds that a fetch can be retried after
     *            it has failed.
     * @param alternate
     *            the message to output as an alternate to executing a
     *            <code>printStackTrace</code> method.
     */
    public FetcherFailure(final String message,
                          final Throwable cause,
                          final Long retryInterval,
                          final String alternate) {
        super(message,
              cause,
              retryInterval);
        this.alternate = alternate;
    }

    // instance member method (alphabetic)

    @Override
    public String getAlternateMessage() {
        return alternate;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
