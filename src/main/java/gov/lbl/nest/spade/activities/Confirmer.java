package gov.lbl.nest.spade.activities;

import java.io.IOException;
import java.util.Collection;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import gov.lbl.nest.spade.services.VerificationManager;

/**
 * This class creates the confirmation information for a bundle.
 *
 * @author patton
 */
public class Confirmer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Verification} instance used by this object.
     */
    private final VerificationManager verificationManager;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the {@link Activity} instance declaring this activity.
     * @param verificationManager
     *            the {@link VerificationManager} instance to be used by the created
     *            object.
     */
    public Confirmer(final Activity activity,
                     final VerificationManager verificationManager) {
        this.verificationManager = verificationManager;
    }

    // instance member method (alphabetic)

    /**
     * Creates the checksum for the transfer file of a bundle.
     * 
     * @param ticket
     *            the {@link IngestTicket} instance of the bundle.
     *
     * @throws IOException
     *             if the transfer file can not be found or read.
     * @throws MetadataParseException
     *             if the metadata file can not be found or read.
     */
    public void confirm(final IngestTicket ticket) throws IOException,
                                                   MetadataParseException {
        final Collection<String> transfers = ticket.getOutboundTransfers();
        if (transfers.isEmpty()) {
            verificationManager.confirmable(ticket.getIdentity(),
                                            ticket.getMetadataFile(),
                                            ticket.getTransferFile());
        } else {
            verificationManager.confirmable(ticket.getIdentity());
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
