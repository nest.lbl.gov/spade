package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.interfaces.policy.PackingPolicy;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedException;
import gov.lbl.nest.spade.registry.internal.InternalFileName;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.CacheManager;

/**
 * This class unwraps a bundle and any associated metadata from a single file.
 *
 * @author patton
 */
public class Unpacker {

    private static class DataSize implements
                                  FileVisitor<Path> {

        /**
         * The current size of data files.
         */
        private long size = 0;

        long getSize() {
            return size;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir,
                                                  IOException exc) {
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir,
                                                 BasicFileAttributes attrs) {
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file,
                                         BasicFileAttributes attrs) {
            size += attrs.size();
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file,
                                               IOException exc) {
            return FileVisitResult.CONTINUE;
        }

    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Unpacker.class);

    // private static member data

    // private instance member data

    static long getDataSize(final File directory) throws IOException {
        final DataSize visitor = new DataSize();
        Files.walkFileTree(directory.toPath(),
                           visitor);
        return visitor.getSize();
    }

    /**
     * The {@link Bookkeeping} instance used by this object.
     */
    private final Bookkeeping bookkeeping;

    /**
     * The {@link CacheManager} instance used by this object.
     */
    private final CacheManager cacheManager;

    /**
     * The directory into which to write the wrapped file.
     */
    private final File unpackerDir;

    /**
     * The name of this activity.
     */
    private final String name;

    /**
     * The {@link PackingPolicy} instance used by this object.
     */
    private final PackingPolicy policy;

    // constructors

    /**
     * The total factor used to define the minimum space needed for this activity by
     * multiplying by the number of threads.
     */
    private final int storageLoad;

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param bookkeeping
     *            the {@link Bookkeeping} instance used by this object.
     * @param cacheDefinition
     *            the {@link CacheDefinition} instance used by this object.
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     */
    public Unpacker(Activity activity,
                    Bookkeeping bookkeeping,
                    CacheDefinition cacheDefinition,
                    CacheManager cacheManager) {
        this.bookkeeping = bookkeeping;
        this.cacheManager = cacheManager;
        this.unpackerDir = (cacheDefinition.getCacheMap()).get(Activity.UNPACKER);
        name = activity.getName();
        policy = Packer.getPackingPolicy(activity,
                                         LOG);
        storageLoad = activity.getStorageLoad();
    }

    // static member methods (alphabetic)

    /**
     * Unwraps a bundle and any associated metadata from a single file.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's
     *            wrapped file location.
     *
     * @return the {@link IngestTicket} instance that results from successfully
     *         unwrapping the bundle.
     *
     * @throws InterruptedException
     *             when this method is interrupted.
     * @throws IOException
     *             when there is an IO error.
     * @throws PolicyFailedException
     *             when there is an error in managing the unpacking directory.
     * @throws TimeoutException
     *             if the packing takes too long.
     */
    public IngestTicket unpack(IngestTicket ticket) throws PolicyFailedException,
                                                    IOException,
                                                    InterruptedException,
                                                    TimeoutException {
        final File packedFile = ticket.getPackedFile();
        if (null == packedFile) {
            throw new FileNotFoundException("No Packed file was supplied");
        } else if (!packedFile.exists()) {
            throw new FileNotFoundException(packedFile.getPath() + " does not exist");
        }

        final String identity = ticket.getIdentity();
        final File directory = new File(unpackerDir,
                                        identity);
        final File tmpDir = new File(directory.getParentFile(),
                                     directory.getName() + InternalFileName.TMP_SUFFIX);
        cacheManager.delete(tmpDir);
        final boolean isSingle;
        final File dataDeliveryDirectory;
        if (!directory.exists()) {
            cacheManager.waitForSpace(tmpDir,
                                      storageLoad,
                                      name);
            if (!tmpDir.mkdirs()) {
                throw new FileNotFoundException("The temporary unpacking directory, \"" + tmpDir.getPath()
                                                + "\" could not be created.");
            }
            try {
                policy.unpack(packedFile,
                              tmpDir);
            } catch (PolicyFailedException e) {
                cacheManager.delete(tmpDir);
                throw e;
            }

            /**
             * Remove metadata file from unpacked data. That file is only included for
             * archiving purposes and the authoritative metadata is the one delivered with
             * the original file.
             */
            final Bundle bundle = ticket.getBundle();
            final File metadataFile = new File(tmpDir,
                                               InternalFileName.getMetadataFilename(bundle.getName()));
            if (!metadataFile.delete()) {
                LOG.warn("Metadata file was missing from packed file for bundle \"" + bundle.getName()
                         + "\"");
            }

            final File[] payloads = tmpDir.listFiles();
            if (null == payloads || 1 != payloads.length) {
                throw new IllegalStateException("The is more than one payload file in the for packed file for bundle \"" + bundle.getName()
                                                + "\"");
            }
            final File payload = payloads[0];
            if (ticket.isUnplaced()) {
                bookkeeping.setBinarySize(identity,
                                          getDataSize(payload));
            }
            if (payload.isDirectory()) {
                dataDeliveryDirectory = new File(directory,
                                                 payload.getName());
                dataDeliveryDirectory.mkdirs();
                cacheManager.move(payload,
                                  dataDeliveryDirectory);
                isSingle = false;
            } else {
                dataDeliveryDirectory = new File(directory,
                                                 InternalFileName.PAYLOAD_DIRECTORY);
                dataDeliveryDirectory.mkdirs();
                final File target = new File(dataDeliveryDirectory,
                                             payload.getName());
                cacheManager.move(payload,
                                  target);
                isSingle = true;
            }
            cacheManager.delete(tmpDir);
        } else {
            throw new UnsupportedOperationException("Can not determine whether unpacked file is single or not.");
        }
        final IngestTicket result = new IngestTicket(ticket,
                                                     dataDeliveryDirectory,
                                                     isSingle);
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
