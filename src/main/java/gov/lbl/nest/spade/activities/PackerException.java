package gov.lbl.nest.spade.activities;

import java.io.File;

/**
 * This Exception is thrown when a Spade operation has failed and so its
 * 'problem' files have been saved.
 *
 * @author patton
 */
public class PackerException extends
                             Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    /**
     * The file, if any, that is the packed file when this object was thrown.
     *
     */
    private final File packedFile;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param cause
     *            the Exception that caused this object was thrown.
     * @param packedFile
     *            the directory in which the files are being wrapped.
     */
    public PackerException(final Throwable cause,
                           final File packedFile) {
        super(cause);
        this.packedFile = packedFile;
    }

    // instance member method (alphabetic)

    /**
     * Returns the file, if any, that is the packed file when this object was
     * thrown.
     *
     * @return the file, if any, that is the packed file when this object was
     *         thrown.
     */
    public File getPackedFile() {
        return packedFile;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
