package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.interfaces.metadata.MetadataParseException;
import gov.lbl.nest.spade.services.VerificationManager;

/**
 * The class prepares for shipping a Bundle one or more destinations.
 */
public class PreShipper {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Verification} instance used by this object.
     */
    private final VerificationManager verificationManager;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param verificationManager
     *            the {@link VerificationManager} instance used by this object.
     */
    public PreShipper(final Activity activity,
                      final VerificationManager verificationManager) {
        this.verificationManager = verificationManager;
    }

    // instance member method (alphabetic)

    /**
     * Prepares for shipping a Bundle one or more destinations.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's file
     *            locations.
     *
     * @return the collection {@link IngestTicket} instances that will be used for
     *         shipping. Each returned ticket will have one and only one outbound
     *         transfer.
     *
     * @throws IOException
     *             if the transfer file can not be found or read.
     * @throws MetadataParseException
     *             if the metadata file can not be found or read.
     */
    public Collection<IngestTicket> preShip(IngestTicket ticket) throws IOException,
                                                                 MetadataParseException {
        /**
         * Create collection of Ingest tickets for subsequent multi-instance task.
         */
        final File transferFile = ticket.getTransferFile();
        final Collection<String> outboundTransfers = ticket.getOutboundTransfers();
        final List<IngestTicket> result = new ArrayList<>(outboundTransfers.size());
        for (String outboundTransfer : outboundTransfers) {
            result.add(new IngestTicket(ticket,
                                        outboundTransfer));
        }
        verificationManager.prepare(ticket.getIdentity(),
                                    (ticket.getBundle()).getName(),
                                    ticket.getMetadataFile(),
                                    transferFile,
                                    outboundTransfers);
        return result;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
