package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.interfaces.policy.CompressingPolicy;
import gov.lbl.nest.spade.interfaces.policy.PolicyFailedException;
import gov.lbl.nest.spade.registry.internal.InternalFileName;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.CacheManager;

/**
 * This class expands a compressed bundle into its packed file.
 *
 * @author patton
 */
public class Expander {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Expander.class);

    // private static member data

    // private instance member data

    /**
     * The {@link Bookkeeping} instance used by this object.
     */
    private final Bookkeeping bookkeeping;

    /**
     * The {@link CacheManager} instance used by this object.
     */
    private final CacheManager cacheManager;

    /**
     * The directory into which to write the compressed file.
     */
    private final File expanderDir;

    /**
     * The name of this activity.
     */
    private final String name;

    /**
     * The {@link CompressingPolicy} instance used by this object.
     */
    private final CompressingPolicy policy;

    /**
     * The total factor used to define the minimum space needed for this activity by
     * multiplying by the number of threads.
     */
    private final int storageLoad;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the name of this activity.
     * @param bookkeeping
     *            the {@link Bookkeeping} instance used by this object.
     * @param cacheDefinition
     *            the {@link CacheDefinition} instance used by this object.
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     */
    public Expander(final Activity activity,
                    final Bookkeeping bookkeeping,
                    final CacheDefinition cacheDefinition,
                    final CacheManager cacheManager) {
        this.bookkeeping = bookkeeping;
        this.cacheManager = cacheManager;
        this.expanderDir = (cacheDefinition.getCacheMap()).get(Activity.EXPANDER);
        name = activity.getName();
        policy = Compressor.getCompressionPolicy(activity,
                                                 LOG);
        storageLoad = activity.getStorageLoad();
    }

    // instance member method (alphabetic)

    /**
     * Expands a compressed bundle into its packed file.
     * 
     * @param ticket
     *            the {@link IngestTicket} instance describing the bundle.
     * 
     * @return the updated {@link IngestTicket} instance.
     *
     * @throws IOException
     *             when the expanded file can not the written.
     * @throws PolicyFailedException
     *             when the there is a failure in the expansion application.
     * @throws InterruptedException
     *             when the the expansion is interrupted
     * @throws TimeoutException
     *             when the expansion take too long.
     */
    public IngestTicket expand(final IngestTicket ticket) throws PolicyFailedException,
                                                          IOException,
                                                          InterruptedException,
                                                          TimeoutException {
        final File compressedFile = ticket.getCompressedFile();
        if (null == compressedFile) {
            throw new FileNotFoundException("No Compressed file was supplied");
        } else if (!compressedFile.exists()) {
            throw new FileNotFoundException(compressedFile.getPath() + " does not exist");
        }
        final String identity = ticket.getIdentity();
        if (ticket.isUnplaced()) {
            bookkeeping.setCompressedSize(identity,
                                          compressedFile.length());
        }

        final File directory = new File(expanderDir,
                                        identity);
        final String suffix = policy.getPackedSuffixFromCompressedFile(compressedFile.getName());
        final File expandedFile = new File(directory,
                                           (ticket.getBundle()).getName() + suffix);
        final File tmpFile = new File(expandedFile.getParentFile(),
                                      expandedFile.getName() + InternalFileName.TMP_SUFFIX);
        cacheManager.delete(tmpFile);

        if (!expandedFile.exists()) {
            if (!directory.mkdirs()) {
                throw new FileNotFoundException("The expansion working directory, \"" + directory.getPath()
                                                + "\" could not be created.");
            }
            cacheManager.waitForSpace(tmpFile,
                                      storageLoad,
                                      name);
            try {
                policy.expand(compressedFile,
                              tmpFile);
            } catch (PolicyFailedException e) {
                cacheManager.delete(tmpFile);
                throw e;
            }
            if (ticket.isUnplaced()) {
                bookkeeping.setPackedSize(identity,
                                          tmpFile.length());
            }
            cacheManager.move(tmpFile,
                              expandedFile);
        }
        return new IngestTicket(ticket,
                                expandedFile);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
