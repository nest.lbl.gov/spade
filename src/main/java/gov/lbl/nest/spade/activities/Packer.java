package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.interfaces.policy.PackingPolicy;
import gov.lbl.nest.spade.interfaces.storage.Location;
import gov.lbl.nest.spade.policy.impl.TarPacking;
import gov.lbl.nest.spade.registry.internal.InternalFileName;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.impl.LocalhostTransfer;

/**
 * This class wraps a bundle and any associated metadata into a single file.
 *
 * @author patton
 */
public class Packer {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Packer.class);

    // private static member data

    // private instance member data

    /**
     * Returns the {@link PackingPolicy} instance for the supplied {@link Activity}
     * instance.
     *
     * @param activity
     *            the {@link Activity} instance whose {@link PackingPolicy} instance
     *            should be returned.
     * @param logger
     *            the {@link Logger} instance though which the selected policy will
     *            be logged.
     *
     * @return the {@link PackingPolicy} instance for the supplied {@link Activity}
     *         instance.
     */
    public static PackingPolicy getPackingPolicy(Activity activity,
                                                 Logger logger) {
        final PackingPolicy policyToUse;
        if (null == activity) {
            policyToUse = new TarPacking();
        } else {
            final PackingPolicy policy = activity.getImplementedPolicy(PackingPolicy.class);
            if (null == policy) {
                policyToUse = new TarPacking();
            } else {
                policyToUse = policy;
            }
        }
        final Class<?> clazz = policyToUse.getClass();
        if (null != activity && null != logger) {
            logger.info("\"" + activity.getName()
                        + "\" is using "
                        + clazz.getCanonicalName()
                        + " as its policy");
        }
        return policyToUse;
    }

    /**
     * The {@link Bookkeeping} instance used by this object.
     */
    private final Bookkeeping bookkeeping;

    /**
     * The {@link CacheManager} instance used by this object.
     */
    private final CacheManager cacheManager;

    /**
     * The directory into which to write the wrapped file.
     */
    private final File packerDir;

    /**
     * The name of this activity.
     */
    private final String name;

    /**
     * The {@link PackingPolicy} instance used by this object.
     */
    private final PackingPolicy policy;

    // constructors

    /**
     * The total factor used to define the minimum space needed for this activity by
     * multiplying by the number of threads.
     */
    private final int storageLoad;

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param bookkeeping
     *            the {@link Bookkeeping} instance used by this object.
     * @param cacheDefinition
     *            the {@link CacheDefinition} instance used by this object.
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     */
    public Packer(Activity activity,
                  Bookkeeping bookkeeping,
                  CacheDefinition cacheDefinition,
                  CacheManager cacheManager) {
        this.bookkeeping = bookkeeping;
        this.cacheManager = cacheManager;
        this.packerDir = (cacheDefinition.getCacheMap()).get(Activity.PACKER);

        name = activity.getName();
        policy = getPackingPolicy(activity,
                                  LOG);
        storageLoad = activity.getStorageLoad();
    }

    // static member methods (alphabetic)

    /**
     * Wraps a bundle and any associated metadata into a single file.
     *
     * @param ticket
     *            the {@link IngestTicket} instance the contains the bundle's files
     *            to be packed.
     *
     * @return the {@link IngestTicket} instance that results from successfully
     *         packing of the bundle.
     *
     * @throws TimeoutException
     *             if the packing takes too long.
     * @throws PackerException
     *             when a {@link Throwable} instance is generated in this method.
     */
    public IngestTicket pack(final IngestTicket ticket) throws TimeoutException,
                                                        PackerException {
        File packedFile = null;
        try {
            final File dataFile;
            final File metadataFile;
            final File dataDeliveryDirectory = ticket.getDataDeliveryDirectory();
            final File metadataDeliveryFile = ticket.getMetadataFile();
            if (null == dataDeliveryDirectory && null == metadataDeliveryFile) {
                final Location placedFiles = ticket.getPlacedFiles();
                dataFile = placedFiles.getDataFile();
                metadataFile = placedFiles.getMetadataFile();
            } else {
                dataFile = dataDeliveryDirectory;
                metadataFile = metadataDeliveryFile;
            }
            if (null == dataFile) {
                throw new FileNotFoundException("No data file was supplied");
            } else if (!dataFile.exists()) {
                throw new FileNotFoundException(dataFile.getPath() + " does not exist");
            }

            final File directory = new File(packerDir,
                                            ticket.getIdentity());
            packedFile = new File(directory,
                                  (ticket.getBundle()).getName() + policy.getSuffix());
            final File tmpFile = new File(packedFile.getParentFile(),
                                          packedFile.getName() + InternalFileName.TMP_SUFFIX);
            cacheManager.delete(tmpFile);

            final File tmpDir = new File(packerDir,
                                         ticket.getIdentity() + InternalFileName.TMP_SUFFIX);
            cacheManager.delete(tmpDir);
            cacheManager.waitForSpace(tmpDir,
                                      storageLoad,
                                      name);
            if (!tmpDir.mkdirs()) {
                throw new FileNotFoundException("The temporary packing directory, \"" + tmpDir.getPath()
                                                + "\" could not be created.");
            }

            final File tmpMetadataFile = new File(tmpDir,
                                                  metadataFile.getName());
            LocalhostTransfer.copy(metadataFile,
                                   tmpMetadataFile);
            final File tmpDataFile = new File(tmpDir,
                                              dataFile.getName());
            LocalhostTransfer.copy(dataFile,
                                   tmpDataFile);

            if (!packedFile.exists()) {
                cacheManager.waitForSpace(tmpFile,
                                          storageLoad,
                                          name);
                cacheManager.waitForSpace(directory,
                                          storageLoad,
                                          name);
                if (!directory.mkdirs()) {
                    throw new FileNotFoundException("Packed results directory, \"" + directory.getPath()
                                                    + "\" could not be created.");
                }
                policy.pack(tmpDataFile,
                            tmpMetadataFile,
                            ticket.isSingle(),
                            tmpFile);
                cacheManager.move(tmpFile,
                                  packedFile);
                cacheManager.delete(tmpDir);
            }
            bookkeeping.setPackedSize(ticket.getIdentity(),
                                      packedFile.length());
            final IngestTicket result = new IngestTicket(ticket,
                                                         packedFile);
            return result;
        } catch (TimeoutException e) {
            throw e;
        } catch (Throwable t) {
            throw new PackerException(t,
                                      packedFile);
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
