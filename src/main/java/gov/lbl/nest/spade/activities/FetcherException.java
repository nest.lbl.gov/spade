package gov.lbl.nest.spade.activities;

/**
 * This Exception is thrown when a Spade operation has failed and so its
 * 'problem' files have been saved.
 *
 * @author patton
 */
public class FetcherException extends
                              RetriableException {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            the detail message (which is saved for later retrieval by the
     *            Throwable.getMessage() method).
     * @param cause
     *            the Exception that caused this object was thrown.
     * @param retryInterval
     *            the interval, if any, in seconds that a fetch can be retried after
     *            it has failed.
     */
    public FetcherException(final String message,
                            final Throwable cause,
                            final Long retryInterval) {
        super(message,
              cause,
              retryInterval);
    }

    // instance member method (alphabetic)

    @Override
    public String getMessage() {
        final String message = super.getMessage();
        if (null == message) {
            if (null != getCause()) {
                return getCause().getMessage();
            }
            return null;
        }
        return message;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
