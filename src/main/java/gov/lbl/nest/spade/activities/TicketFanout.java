package gov.lbl.nest.spade.activities;

import java.util.Collection;

/**
 * This class is used to mark sure a task that does not transform a ticket is
 * loaded.
 *
 * @author patton
 *
 * @param <T>
 *            the class of ticket that is transformed.
 */
public interface TicketFanout<T> {

    /**
     * Executes the task that fans out a collection of tickets.
     *
     * @param ticket
     *            the ticket to be preserved.
     *
     * @return the collection of fanned out tickets.
     *
     * @throws Exception
     *             when there is a problem preserving the ticket.
     */
    Collection<T> fanoutTicket(final T ticket) throws Exception;

    /**
     * Returns the name of the task that is preserving the ticket.
     *
     * @return the name of the task that is preserving the ticket.
     */
    String getName();
}
