package gov.lbl.nest.spade.activities;

import gov.lbl.nest.spade.workflow.ActivityDefermentKey;

/**
 * This class is used to mark sure a task that only processes the ticket but
 * does not transform it.
 *
 * @author patton
 *
 * @param <T>
 *            the class of ticket that is transformed.
 */
public interface TicketProcessor<T> {

    /**
     * Returns the name of the task that is preserving the ticket.
     *
     * @return the name of the task that is preserving the ticket.
     */
    String getName();

    /**
     * Executes the task.
     *
     * @param ticket
     *            the ticket to be preserved.
     * @param key
     *            the {@link ActivityDefermentKey} instance to use if this method
     *            defers its completion.
     *
     * @throws Exception
     *             when there is a problem preserving the ticket.
     */
    void processTicket(final T ticket,
                       final ActivityDefermentKey key) throws Exception;
}
