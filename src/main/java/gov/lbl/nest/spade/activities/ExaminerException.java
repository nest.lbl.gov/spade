package gov.lbl.nest.spade.activities;

import java.io.File;

/**
 * This Exception is thrown when a Spade operation has failed and so its
 * 'problem' files have been saved.
 *
 * @author patton
 */
public class ExaminerException extends
                               RetriableException {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    /**
     * The file containing the internal semaphore file, if any, when this object was
     * thrown.
     */
    private final File internalSemaphore;

    /**
     * The file containing the metadata file, if any, when this object what thrown.
     */
    private final File metadataFile;

    /**
     * The directory, if any, containing the data files when this object what
     * thrown.
     */
    private final File dataDeliveryDirectory;

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param message
     *            TODO
     * @param cause
     *            the Exception that caused this object was thrown.
     * @param internalSemaphore
     *            the file, if any, containing the internal semaphore file when this
     *            object was thrown.
     * @param metadataFile
     *            the file, if any, containing the metadata file when this object
     *            was thrown.
     * @param dataDeliveryDirectory
     *            the directory, if any, containing the data files when this object
     *            was thrown.
     * @param retryInterval
     *            the interval, if any, in seconds that a fetch can be retried after
     *            it has failed.
     */
    public ExaminerException(String message,
                             final Throwable cause,
                             final File internalSemaphore,
                             final File metadataFile,
                             final File dataDeliveryDirectory,
                             Long retryInterval) {
        super(message,
              cause,
              retryInterval);
        this.dataDeliveryDirectory = dataDeliveryDirectory;
        this.internalSemaphore = internalSemaphore;
        this.metadataFile = metadataFile;
    }

    // instance member method (alphabetic)

    /**
     * Returns the directory, if any, containing the data files when this object was
     * thrown.
     *
     * @return the directory, if any, containing the data files when this object was
     *         thrown.
     */
    public File getDataDeliveryDirectory() {
        return dataDeliveryDirectory;
    }

    /**
     * Returns the file, if any, containing the internal semaphore file when this
     * object was thrown.
     *
     * @return the file, if any, containing the internal semaphore file when this
     *         object was thrown.
     */
    public File getInternalSemaphore() {
        return internalSemaphore;
    }

    /**
     * Returns the file, if any, containing the metadata file when this object was
     * thrown.
     *
     * @return the file, if any, containing the metadata file when this object was
     *         thrown.
     */
    public File getMetadataFile() {
        return metadataFile;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
