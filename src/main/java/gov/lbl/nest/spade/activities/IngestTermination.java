package gov.lbl.nest.spade.activities;

import gov.lbl.nest.spade.workflow.WorkflowTermination;

/**
 * This interface is used when a workflow has terminated.
 *
 * @author patton
 */
public interface IngestTermination extends
                                   WorkflowTermination {

    /**
     * This method is invoked when the workflow does not ends successfully.
     *
     * @param ticket
     *            the resulting ticket of the workflow.
     * @param name
     *            the name of the task that failed.
     * @param rescue
     *            the Object that can be used to rescue a failed workflow once its
     *            issue has been resolved.
     * @param thrown
     *            the {@link Throwable} instance that cause the workflow to fail.
     *
     * @return true if the internal structures of the instance should be preserved
     *         so it can be rescued.
     *
     */
    public boolean failed(IngestTicket ticket,
                          String name,
                          Object rescue,
                          Throwable thrown);

    /**
     * This method is invoked when the workflow ends successfully.
     *
     * @param ticket
     *            the resulting ticket of the workflow.
     */
    public void succeeded(IngestTicket ticket);
}
