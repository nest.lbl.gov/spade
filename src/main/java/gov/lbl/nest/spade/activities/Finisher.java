package gov.lbl.nest.spade.activities;

import java.io.File;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.services.CacheManager;

/**
 * This class finishes up the processing of a bundle.
 *
 * @author patton
 */
public class Finisher {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link CacheManager} instance used by this object.
     */
    private final CacheManager cacheManager;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param cacheManager
     *            the {@link CacheManager} instance used by this object.
     */
    public Finisher(Activity activity,
                    CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    // instance member method (alphabetic)

    private void cleanup(final File file,
                         final File nested,
                         final File preserve) {
        if (null == file || (null != preserve && file == preserve)) {
            return;
        }

        cacheManager.delete(file);
        final File parent = file.getParentFile();
        if (null != parent && parent.exists()
            && 0 == (parent.list()).length) {
            cacheManager.delete(parent);
            if (null != nested && file == nested) {
                final File grandparent = parent.getParentFile();
                if (null != grandparent && 0 == (grandparent.list()).length) {
                    cacheManager.delete(grandparent);
                }
            }
        }
    }

    /**
     * Finishes up the processing of a bundle.
     * 
     * @param ticket
     *            the {@link IngestTicket} instance with which this workflow has
     *            finished.
     */
    public void finish(IngestTicket ticket) {
        final File nestedTransfer;
        if ((ticket.getBundle()).isInbound() && ticket.isSingle()) {
            nestedTransfer = ticket.getTransferFile();
        } else {
            nestedTransfer = null;
        }
        final File preservedArchive;
        Boolean archivePreserved = ticket.isArchivePreserved();
        if (null != archivePreserved && archivePreserved.booleanValue()) {
            preservedArchive = ticket.getArchiveFile();
        } else {
            preservedArchive = null;
        }
        cleanup(ticket.getInternalSemaphore(),
                nestedTransfer,
                preservedArchive);
        cleanup(ticket.getMetadataFile(),
                nestedTransfer,
                preservedArchive);
        cleanup(ticket.getDataDeliveryDirectory(),
                nestedTransfer,
                preservedArchive);
        cleanup(ticket.getPackedFile(),
                nestedTransfer,
                preservedArchive);
        cleanup(ticket.getCompressedFile(),
                nestedTransfer,
                preservedArchive);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
