/**
 * The package contains the workflow tasks used to make up the SPADE
 * application.
 *
 * @author patton
 */
package gov.lbl.nest.spade.activities;