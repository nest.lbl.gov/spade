package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.config.Duplication;
import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.registry.FileCategory;
import gov.lbl.nest.spade.services.FileTransfer;
import gov.lbl.nest.spade.services.impl.LoadedDuplications;
import gov.lbl.nest.spade.services.impl.LocalhostTransfer;
import jakarta.resource.spi.IllegalStateException;

/**
 * This class duplicates a bundle to an unmanaged directory.
 *
 * @author patton
 */
public class Duplicator {

    // public static final member data

    /**
     * The suffix used to indicate completed shipping.
     */
    public static final String DONE_SUFFIX = ".done";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The default {@link FileTransfer} instance to use if one is not specified.
     */
    private static final FileTransfer DEFAULT_TRANSFER = new LocalhostTransfer();

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger(Duplicator.class);

    // private static member data

    // private instance member data

    /**
     * The directory into which to write the done files.
     */
    private final File doneDir;

    /**
     * True if the failure of the duplication should throw an exception.
     */
    private final boolean fatal;

    /**
     * The {@link LoadedDuplications} instance used by this object.
     */
    private final LoadedDuplications loadedDuplications;

    // constructors

    /**
     * Creates an instance of this class.
     *
     * @param activity
     *            the {@link Activity} instance declaring this task.
     * @param cacheDefinition
     *            the {@link CacheDefinition} instance used by this object.
     * @param loadedDuplications
     *            the {@link LoadedDuplications} instance used by this object.
     */
    public Duplicator(final Activity activity,
                      final CacheDefinition cacheDefinition,
                      final LoadedDuplications loadedDuplications) {
        this.doneDir = (cacheDefinition.getCacheMap()).get(Activity.DUPLICATOR);
        this.loadedDuplications = loadedDuplications;
        final String fatalParameter = activity.getParameter("fatal");
        if (null == fatalParameter) {
            fatal = false;
        } else {
            fatal = Boolean.parseBoolean(fatalParameter);
        }
    }

    // instance member method (alphabetic)

    /**
     * Duplicates the data file associated with a bundle.
     * 
     * @param ticket
     *            the {@link IngestTicket} instance defining the bundle.
     * 
     * @throws Exception
     *             when the duplication can not complete successfully.
     */
    public void duplicate(final IngestTicket ticket) throws Exception {
        final Collection<String> duplications = (ticket.getOptions()).getDuplications();
        if (null == duplications || duplications.isEmpty()) {
            return;
        }
        if (1 != duplications.size()) {
            throw new IllegalStateException("Multiple duplicators requested, only a single duplicator is supported at present.");
        }

        final String duplicationName = (duplications.iterator()).next();
        final Duplication duplication = loadedDuplications.getDuplication(duplicationName);
        if (null == duplication) {
            throw new IllegalArgumentException("There is no duplication named \"" + duplicationName
                                               + "\"");
        }

        final FileCategory category = duplication.getCategory();
        final FileCategory categoryToUse;
        if (null == category) {
            categoryToUse = FileCategory.DATA;
        } else {
            categoryToUse = category;
        }

        final File sourceFile = ticket.getFileForCategory(categoryToUse);
        if (null == sourceFile) {
            final String message = "There is no file for the \"" + categoryToUse
                                   + "\" catagory, so it can not be duplicated.";
            if (fatal) {
                throw new IllegalArgumentException(message);
            }
            LOG.warn(message);
        }
        if (!sourceFile.exists()) {
            final String message = "The file \"" + sourceFile.getPath()
                                   + "\" does not exist, so it can not be duplicated.";
            if (fatal) {
                throw new FileNotFoundException(message);
            }
            LOG.warn(message);
        }
        final File doneFile = new File(doneDir,
                                       sourceFile.getName() + DONE_SUFFIX);

        final ExternalLocation duplicationLocation = duplication.getDuplicationLocation();
        final FileTransfer transfer = duplication.getTransferImplementation();
        final FileTransfer transferToUse;
        if (null == transfer) {
            transferToUse = DEFAULT_TRANSFER;
        } else {
            transferToUse = transfer;
        }

        try {
            doneFile.delete();
            doneFile.createNewFile();
            transferToUse.send(duplicationLocation,
                               null,
                               null,
                               sourceFile,
                               sourceFile.getName(),
                               doneFile,
                               doneFile.getName(),
                               null);
        } catch (InterruptedException e) {
            throw e;
        } catch (Exception e) {
            if (fatal) {
                throw e;
            }
            // Failure of a duplication should not stop the rest of the
            // process.
            e.printStackTrace();
        } finally {
            doneFile.delete();
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
