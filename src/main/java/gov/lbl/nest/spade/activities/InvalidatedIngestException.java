package gov.lbl.nest.spade.activities;

/**
 * This Exception is thrown when an ingest request has already been completed or
 * has been superseded by a new one.
 *
 * @author patton
 */
public class InvalidatedIngestException extends
                                        Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private instance member data

    /**
     * Used by Serializable.
     */
    private static final long serialVersionUID = 1L;

    // constructors

    /**
     * Creates an instance of this class. object was thrown.
     *
     * @param message
     *            the detail message. The detail message is saved for later
     *            retrieval by the getMessage() method.
     */
    public InvalidatedIngestException(final String message) {
        super(message);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
