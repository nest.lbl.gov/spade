package gov.lbl.nest.spade.activities;

import java.io.File;
import java.io.Serializable;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.interfaces.storage.Location;
import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.FileCategory;
import gov.lbl.nest.spade.registry.Options;
import gov.lbl.nest.spade.registry.internal.InternalFileName;
import gov.lbl.nest.spade.rs.Bundle;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is use to carry information while executing the ingest workflow.
 *
 * @author patton
 */
@XmlRootElement(name = "ingest_ticket")
@XmlType
public class IngestTicket implements
                          Serializable {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(IngestTicket.class);

    /**
     * The object to return when there is no collection of transfers.
     */
    private static final Collection<String> EMPTY_TRANSFERS = new ArrayList<>();

    /**
     * The amount of time, in seconds, to wait before re-trying a disk operation.
     */
    private static final int FAILURE_LIMIT = Integer.parseInt(System.getProperty("gov.lbl.nest.spade.disk.failure.limit",
                                                                                 "5"));

    /**
     * The amount of time, in seconds, to wait before re-trying a disk operation.
     */
    private static final int FAILURE_WAIT = Integer.parseInt(System.getProperty("gov.lbl.nest.spade.disk.failure.wait",
                                                                                "2"));
    /**
     * Use the by the {@link Serializable} interface.
     */
    private static final long serialVersionUID = 1L;

    // private static member data

    // private instance member data

    /**
     * Returns a String containing the number of seconds since the specified start
     * time until the present time.
     *
     * @param start
     *            the time from which this duration should be calculated.
     *
     * @return a String containing the number of seconds since the specified start
     *         time until the present time.
     */
    private static String getDuration(Long start) {
        if (null == start) {
            return null;
        }
        final long now = new Date().getTime();
        return Long.toString(TimeUnit.SECONDS.convert(now - start.longValue(),
                                                      TimeUnit.MILLISECONDS));
    }

    /**
     * Read the {@link IngestTicket} instance for the supplied identity from the
     * specified file.
     *
     * @param file
     *            the {@link File} in which to save the ticket.
     *
     * @return the {@link IngestTicket} instance for the supplied identity from the
     *         specified file.
     */
    public static IngestTicket read(final File file) {
        JAXBContext content;
        try {
            if (file.exists()) {
                content = JAXBContext.newInstance(IngestTicket.class);
                Unmarshaller unmarshaller = content.createUnmarshaller();
                final IngestTicket result = (IngestTicket) unmarshaller.unmarshal(file);
                return result;
            }
        } catch (JAXBException e) {
            // Should never get here.
            throw new IllegalArgumentException(e);
        }
        return null;
    }

    /**
     * True if the Transfer file should be preserved when this workflow "finishes".
     */
    private Boolean archivePreserved;

    /**
     * The {@link URI}, if any, used to request this ticket be used.
     */
    private URI baseUri;

    /**
     * The {@link Bundle} from which this ticket was issued.
     */
    private Bundle bundle;

    /**
     * The compressed data file, if any, for this ticket.
     */
    private File compressedFile;

    /**
     * The directory into which data files should be delivered.
     */
    private File dataDeliveryDirectory;

    /**
     * The {@link ExternalFile} instances that define the data to be fetched.
     */
    private Collection<ExternalFile> externalFiles;

    /**
     * The identity of this ticket within its application.
     */
    private String identity;

    /**
     * The location of the metadata file for this ticket.
     */
    private File metadataFile;

    /**
     * The settings of available ingestion options.
     */
    private Options options;

    /**
     * The packed data file, if any, for this ticket.
     */
    private File packedFile;

    /**
     * The location of the files in the warehouse.
     */
    private Location placedFiles;

    // constructors

    /**
     * The time-stamp of the semaphore file when examined.
     */
    private Long prefetchStamp;

    /**
     * The {@link UUID} for this ticket.
     */
    private UUID uuid;

    /**
     * Creates an instance of this class.
     */
    protected IngestTicket() {
    }

    /**
     * Copies an instance of this class.
     *
     * @param rhs
     *            the {@link IngestTicket} instance to be copied.
     */
    IngestTicket(final IngestTicket rhs) {
        baseUri = rhs.baseUri;
        setBundle(rhs.bundle);
        compressedFile = rhs.compressedFile;
        dataDeliveryDirectory = rhs.dataDeliveryDirectory;
        setExternalFiles(rhs.externalFiles);
        prefetchStamp = rhs.prefetchStamp;
        setIdentity(rhs.identity);
        metadataFile = rhs.metadataFile;
        if (null != rhs.options) {
            setOptions((rhs.options).copy());
        }
        packedFile = rhs.packedFile;
        setPlacedFiles(rhs.placedFiles);
        setUuid(rhs.uuid);
    }

    /**
     * Creates an instance of this class.
     *
     * @param ticket
     *            the {@link IngestTicket} ticket to be copied.
     * @param preserveArchiveFile
     *            true if the archive file should be preserved when this workflow
     *            "finishes".
     */
    public IngestTicket(final IngestTicket ticket,
                        final boolean preserveArchiveFile) {
        this(ticket);
        this.archivePreserved = preserveArchiveFile;
    }

    /**
     * Creates an instance of this file by copying the supplied ticket and adding on
     * the first received file of the fetcher as the transfer file.
     *
     * @param ticket
     *            the {@link IngestTicket} ticket to be copied.
     * @param packedFile
     *            the packed data file, if any, for this ticket.
     */
    public IngestTicket(final IngestTicket ticket,
                        final File packedFile) {
        this(ticket);
        this.packedFile = packedFile;
    }

    /**
     * Creates an instance of this class.
     *
     * @param ticket
     *            the {@link IngestTicket} ticket to be copied.
     * @param dataDeliveryDirectory
     *            the directory into which data files should be delivered.
     * @param isSingle
     *            true if the payload matching this registration is always only a
     *            single file.
     */
    public IngestTicket(final IngestTicket ticket,
                        final File dataDeliveryDirectory,
                        boolean isSingle) {
        this(ticket);
        this.dataDeliveryDirectory = dataDeliveryDirectory;
        (this.options).setSingle(isSingle);
    }

    /**
     * Creates an instance of this file by copying the supplied ticket and adding on
     * the first received file of the fetcher as the transfer file.
     *
     * @param ticket
     *            the {@link IngestTicket} ticket to be copied.
     * @param ignore
     *            dummy argument to differential from other {@link File} based
     *            constructors.
     * @param ignore2
     *            dummy argument to differential from other {@link File} based
     *            constructors.
     * @param compressedFile
     *            the compressed packed file, if any, for this ticket.
     */
    public IngestTicket(final IngestTicket ticket,
                        File ignore,
                        File ignore2,
                        final File compressedFile) {
        this(ticket);
        this.compressedFile = compressedFile;
    }

    /**
     * Creates an instance of this file by copying the supplied ticket and adding on
     * the first received file of the fetcher as the transfer file.
     *
     * @param ticket
     *            the {@link IngestTicket} ticket to be copied.
     * @param shippedCategory
     *            the category of the shipped file.
     * @param transferFile
     *            the first received file of the fetcher.
     */
    public IngestTicket(final IngestTicket ticket,
                        FileCategory shippedCategory,
                        File transferFile) {
        this(ticket);
        setTransferFile(shippedCategory,
                        transferFile);
    }

    /**
     * Creates an instance of this file by copying the supplied ticket and adding on
     * the results of the examiner.
     *
     * @param ticket
     *            the {@link IngestTicket} ticket to be copied.
     * @param prefetchStamp
     *            the time-stamp of the semaphore file when examined.
     * @param metadataFile
     *            the location of the metadata file for this ticket.
     * @param dataDeliveryDirectory
     *            the directory into which data files should be delivered.
     * @param externalFiles
     *            the {@link ExternalFile} instances that define the data to be
     *            fetched.
     * @param options
     *            the setting of available ingestion options.
     */
    public IngestTicket(final IngestTicket ticket,
                        final Long prefetchStamp,
                        final File metadataFile,
                        final File dataDeliveryDirectory,
                        final Collection<ExternalFile> externalFiles,
                        final Options options) {
        this(ticket);
        this.dataDeliveryDirectory = dataDeliveryDirectory;
        setExternalFiles(externalFiles);
        this.prefetchStamp = prefetchStamp;
        this.metadataFile = metadataFile;
        setOptions(options);
    }

    /**
     * Creates an instance of this file by copying the supplied ticket and adding on
     * the results of the pre-shipper.
     *
     * @param ticket
     *            the {@link IngestTicket} ticket to be copied.
     * @param outboundTransfer
     *            the name of the outbound transfer to be used with this ticket.
     */
    public IngestTicket(final IngestTicket ticket,
                        final String outboundTransfer) {
        this(ticket);
        final List<String> outboundTransfers = new ArrayList<>(1);
        outboundTransfers.add(outboundTransfer);
        setOutboundTransfers(outboundTransfers);
    }

    /**
     * Creates an instance of this file by copying the supplied ticket and adding on
     * the results of the examiner.
     *
     * @param ticket
     *            the {@link IngestTicket} ticket to be copied.
     * @param metadata
     *            the path to the metadata file of this object.
     * @param data
     *            the path to the data file of this object.
     * @param packed
     *            the path to the packed file of this object.
     * @param compressed
     *            the path to the compressed file of this object.
     */
    public IngestTicket(final IngestTicket ticket,
                        final String metadata,
                        final String data,
                        final String packed,
                        final String compressed) {
        this(ticket);
        placedFiles = new Location(metadata,
                                   data,
                                   packed,
                                   compressed);
    }

    // instance member method (alphabetic)

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the identity of this ticket within its application.
     * @param bundle
     *            the {@link Bundle} from which this ticket was issued.
     * @param metadataFile
     *            the location of the metadata file for this ticket.
     * @param category
     *            the {@link FileCategory} of the "transfer/archived" file.
     * @param transferFile
     *            the location of the file, if any, that was or will be transferred
     *            or archived by this ticket.
     * @param options
     *            the settings of available transfer options.
     */
    public IngestTicket(final String identity,
                        final Bundle bundle,
                        final File metadataFile,
                        final FileCategory category,
                        final File transferFile,
                        final Options options) {
        setBundle(bundle);
        setIdentity(identity);
        this.metadataFile = metadataFile;
        setOptions(options);
        setTransferFile(category,
                        transferFile);
    }

    /**
     * Creates an instance of this class.
     *
     * @param identity
     *            the identity of this ticket within its application.
     * @param bundle
     *            the {@link Bundle} from which this ticket was issued.
     * @param uuid
     *            the {@link UUID} to assign to the new identity.
     */
    public IngestTicket(final String identity,
                        final Bundle bundle,
                        UUID uuid) {
        setBundle(new Bundle(bundle));
        setIdentity(identity);
        setUuid(uuid);
    }

    /**
     * Returns the category of files matching this registration should be archived
     * locally.
     *
     * @return the category of files matching this registration should be archived
     *         locally.
     */
    @XmlTransient
    public FileCategory getArchiveCategory() {
        return options.getArchive();
    }

    /**
     * Returns the location of the file, if any, that was or will be archived by
     * this ticket.
     *
     * @return the location of the file, if any, that was or will be archived by
     *         this ticket.
     */
    @XmlTransient
    public File getArchiveFile() {
        return getFileForCategory(getArchiveCategory());
    }

    /**
     * Returns the location of the metadata, if any, that was or will be archived by
     * this ticket.
     *
     * @return the location of the metadata, if any, that was or will be archived by
     *         this ticket.
     */
    @XmlTransient
    public File getArchiveMetadata() {
        return getMetadataForCategory(getArchiveCategory());
    }

    /**
     * Returns the base {@link URI}, if any, used to request this ticket be used.
     *
     * @return the base {@link URI}, if any, used to request this ticket be used.
     */
    @XmlElement(name = "base_uri")
    public URI getBaseUri() {
        return baseUri;
    }

    /**
     * Returns the {@link Bundle} from which this ticket was issued.
     *
     * @return the {@link Bundle} from which this ticket was issued.
     */
    @XmlElement
    public Bundle getBundle() {
        return bundle;
    }

    /**
     * Returns the compressed data file, if any, for this ticket.
     *
     * @return the compressed data file, if any, for this ticket.
     */
    @XmlTransient
    public File getCompressedFile() {
        return compressedFile;
    }

    /**
     * Returns the path the compressed file for this ticket.
     *
     * @return the path the compressed file for this ticket.
     */
    @XmlElement(name = "compressed")
    protected String getCompressedPath() {
        if (null == compressedFile) {
            return null;
        }
        return compressedFile.getAbsolutePath();
    }

    /**
     * Returns the directory into which data files should be delivered.
     *
     * @return the directory into which data files should be delivered.
     */
    @XmlTransient
    public File getDataDeliveryDirectory() {
        return dataDeliveryDirectory;
    }

    /**
     * Returns the path to the directory into which data files should be delivered.
     *
     * @return the path to the directory into which data files should be delivered.
     */
    @XmlElement(name = "delivered")
    protected String getDataDeliveryPath() {
        if (null == dataDeliveryDirectory) {
            return null;
        }
        return dataDeliveryDirectory.getAbsolutePath();
    }

    /**
     * Returns the {@link ExternalFile} instances that define the data to be
     * fetched.
     *
     * @return the {@link ExternalFile} instances that define the data to be
     *         fetched.
     */
    @XmlElement(name = "file")
    @XmlElementWrapper(name = "external")
    public Collection<ExternalFile> getExternalFiles() {
        return externalFiles;
    }

    /**
     * Returns the location of the external semaphore file for this ticket.
     *
     * @return the location of the external semaphore file for this ticket.
     */
    @XmlTransient
    public ExternalFile getExternalSemaphore() {
        if (bundle == null || null == bundle.getSemaphore()) {
            return null;
        }
        return bundle.getSemaphore();
    }

    /**
     * Returns the location of the file, if any, for the specified category.
     *
     * @param category
     *            the {@link FileCategory} for which the file should be returned.
     *
     * @return the location of the file, if any, for the specified category. by this
     *         ticket.
     */
    public File getFileForCategory(final FileCategory category) {
        if (null == category) {
            return null;
        }
        if (category == FileCategory.METADATA) {
            return metadataFile;
        }
        if (category == FileCategory.DATA || category == FileCategory.EMBEDDED) {
            final Boolean single = options.isSingle();
            if (null == single || single == Boolean.TRUE) {
                int failures = 0;
                while (true) {
                    final Date begin = new Date();
                    final File[] files = dataDeliveryDirectory.listFiles();
                    if (null == files || 0 == files.length) {
                        failures += 1;
                        if (FAILURE_LIMIT == failures) {
                            return null;
                        }
                        try {
                            Thread.sleep(TimeUnit.SECONDS.toMillis(FAILURE_WAIT));
                        } catch (InterruptedException e) {
                            // Do nothing, try again
                        }
                    } else {
                        LOG.debug("Single file found, in " + getDuration(begin.getTime())
                                  + " secs, in directory \""
                                  + dataDeliveryDirectory.toString()
                                  + "\"");
                        return files[0];
                    }
                }
            }
            return dataDeliveryDirectory;
        }
        if (category == FileCategory.PACKED) {
            return packedFile;
        }
        if (category == FileCategory.COMPRESSED) {
            return compressedFile;
        }
        return null;
    }

    /**
     * Returns the identity of this ticket within its application.
     *
     * @return the identity of this ticket within its application.
     */
    @XmlElement
    public String getIdentity() {
        return identity;
    }

    /**
     * Returns the location of the internal semaphore file for this ticket.
     *
     * @return the location of the internal semaphore file for this ticket.
     */
    @XmlTransient
    public File getInternalSemaphore() {
        if (bundle == null || null == bundle.getSemaphore()) {
            return null;
        }
        return (bundle.getSemaphore()).getDeliveryLocation();
    }

    /**
     * Returns the location of the metadata file for this ticket.
     *
     * @return the location of the metadata file for this ticket.
     */
    @XmlTransient
    public File getMetadataFile() {
        return metadataFile;
    }

    /**
     * Returns the location of the metadata, if any, for the specified category.
     *
     * @param category
     *            the {@link FileCategory} for which the metadata should be
     *            returned.
     *
     * @return the location of the metadata, if any, for the specified category. by
     *         this ticket.
     */
    private File getMetadataForCategory(final FileCategory category) {
        if (null == category) {
            return null;
        }
        if (category == FileCategory.METADATA || category == FileCategory.DATA) {
            return metadataFile;
        }
        return null;
    }

    /**
     * Returns the path to the metadata file for this ticket.
     *
     * @return the path to the metadata file for this ticket.
     */
    @XmlElement(name = "metadata")
    protected String getMetadataPath() {
        if (null == metadataFile) {
            return null;
        }
        return metadataFile.getAbsolutePath();
    }

    /**
     * Returns the settings of available ingestion options.
     *
     * @return the settings of available ingestion options.
     */
    @XmlElement
    public Options getOptions() {
        return options;
    }

    /**
     * Returns the collection of outbound transfers by which files matching this
     * registration should be transferred outbound from this application.
     *
     * @return the collection of outbound transfers by which files matching this
     *         registration should be transferred outbound from this application.
     */
    @XmlTransient
    public Collection<String> getOutboundTransfers() {
        if (null == options) {
            return EMPTY_TRANSFERS;
        }
        final Collection<String> result = options.getOutboundTransfers();
        if (null == result) {
            return EMPTY_TRANSFERS;
        }
        return result;
    }

    /**
     * Returns the packed data file, if any, for this ticket.
     *
     * @return the packed data file, if any, for this ticket.
     */
    @XmlTransient
    public File getPackedFile() {
        return packedFile;
    }

    /**
     * Returns the path the packed file for this ticket.
     *
     * @return the path the packed file for this ticket.
     */
    @XmlElement(name = "packed")
    protected String getPackedPath() {
        if (null == packedFile) {
            return null;
        }
        return packedFile.getAbsolutePath();
    }

    /**
     * Returns the location of the files in the warehouse.
     *
     * @return the location of the files in the warehouse.
     */
    @XmlElement(name = "placement")
    protected Location getPlacedFiles() {
        return placedFiles;
    }

    /**
     * Returns the time-stamp of the semaphore file when examined.
     *
     * @return the time-stamp of the semaphore file when examined.
     */
    @XmlElement(name = "prefetch_stamp")
    protected Long getPrefetchStamp() {
        return prefetchStamp;
    }

    /**
     * Returns the category of file to ship.
     *
     * @return the category of file to ship.
     */
    @XmlTransient
    public FileCategory getShippingCategory() {
        if ((null != options.getCompress() && options.getCompress()) || (null != options.getExpand() && options.getExpand())) {
            return FileCategory.COMPRESSED;
        } else if ((null != options.getPack() && options.getPack()) || (null != options.getUnpack() && options.getUnpack())) {
            return FileCategory.PACKED;
        } else if (null != options.getEmbedded() && options.getEmbedded()) {
            return FileCategory.EMBEDDED;
        } else {
            return FileCategory.DATA;
        }
    }

    /**
     * Returns the location of the file, if any, that was or will be transferred by
     * this ticket.
     *
     * @return the location of the file, if any, that was or will be transferred by
     *         this ticket.
     */
    @XmlTransient
    public File getTransferFile() {
        return getFileForCategory(getShippingCategory());
    }

    /**
     * Returns the {@link UUID} for this ticket.
     *
     * @return the {@link UUID} for this ticket.
     */
    @XmlTransient
    public UUID getUuid() {
        return uuid;
    }

    /**
     * Returns true if the archive file should be preserved when this workflow
     * "finishes".
     *
     * @return true if the archive file should be preserved when this workflow
     *         "finishes".
     */
    @XmlElement(name = "archive_preserved")
    public Boolean isArchivePreserved() {
        return archivePreserved;
    }

    /**
     * Returns true if the data payload matching this registration is always only a
     * single file.
     *
     * @return true if the data payload matching this registration is always only a
     *         single file.
     */
    @XmlTransient
    public boolean isSingle() {
        final Boolean single = options.isSingle();
        if (null == single || single == Boolean.TRUE) {
            return true;
        }
        return false;
    }

    /**
     * Returns true when the file has not been placed the warehouse, even once.
     *
     * @return true when the file has not been placed in the warehouse, even once.
     */
    @XmlTransient
    public boolean isUnplaced() {
        return null == getPlacedFiles();
    }

    /**
     * Merges the supplied {@link IngestTicket} instance with this one, returning
     * the merged instance.
     *
     * @param rhs
     *            the {@link IngestTicket} instance to be merged with this one.
     *
     * @return the merged instance.
     */
    public IngestTicket merge(final IngestTicket rhs) {
        if (null == baseUri) {
            baseUri = rhs.baseUri;
        } else if (null != rhs.baseUri && !baseUri.equals(rhs.baseUri)) {
            throw new IllegalArgumentException();
        }

        // bundle

        if (null == compressedFile) {
            compressedFile = rhs.compressedFile;
        } else if (null != rhs.compressedFile && !compressedFile.equals(rhs.compressedFile)) {
            throw new IllegalArgumentException();
        }

        if (null == dataDeliveryDirectory) {
            dataDeliveryDirectory = rhs.dataDeliveryDirectory;
        } else if (null != rhs.dataDeliveryDirectory && !dataDeliveryDirectory.equals(rhs.dataDeliveryDirectory)) {
            throw new IllegalArgumentException();
        }

        // externalFiles

        if (null == identity) {
            identity = rhs.identity;
        } else if (null != rhs.identity && !identity.equals(rhs.identity)) {
            throw new IllegalArgumentException();
        }

        if (null == metadataFile) {
            metadataFile = rhs.metadataFile;
        } else if (null != rhs.metadataFile && !metadataFile.equals(rhs.metadataFile)) {
            throw new IllegalArgumentException();
        }

        if (null == options) {
            options = rhs.options;
        } else {
            options.merge(rhs.options);
        }

        if (null == packedFile) {
            packedFile = rhs.packedFile;
        } else if (null != rhs.packedFile && !packedFile.equals(rhs.packedFile)) {
            throw new IllegalArgumentException();
        }

        // placedFiles

        if (null == prefetchStamp) {
            prefetchStamp = rhs.prefetchStamp;
        } else if (null != rhs.prefetchStamp && !prefetchStamp.equals(rhs.prefetchStamp)) {
            throw new IllegalArgumentException();
        }

        if (null == archivePreserved) {
            archivePreserved = rhs.archivePreserved;
        } else if (null != rhs.archivePreserved && !archivePreserved.equals(rhs.archivePreserved)) {
            throw new IllegalArgumentException();
        }

        if (null == uuid) {
            uuid = rhs.uuid;
        } else if (null != rhs.uuid && !uuid.equals(rhs.uuid)) {
            throw new IllegalArgumentException();
        }

        return this;
    }

    /**
     * Sets whether the archive file should be preserved when this workflow
     * "finishes"
     *
     * @param preserveArchiveFile
     *            true if the Transfer file should be preserved when this workflow
     *            "finishes".
     */
    protected void setArchivePreserved(Boolean preserveArchiveFile) {
        this.archivePreserved = preserveArchiveFile;
    }

    /**
     * Sets the base {@link URI}, if any, used to request this ticket be used.
     *
     * @param uri
     *            the base {@link URI}, if any, used to request this ticket be used.
     */
    public void setBaseUri(URI uri) {
        baseUri = uri;
    }

    /**
     * Sets the name used to denote the bundle of files being transferred.
     *
     * @param bundle
     *            the name used to denote the bundle of files being transferred.
     */
    protected void setBundle(final Bundle bundle) {
        this.bundle = bundle;
    }

    /**
     * Sets the path to the compressed file for this ticket.
     *
     * @param path
     *            the path to the compressed file for this ticket.
     */
    protected void setCompressedPath(final String path) {
        if (null == path) {
            compressedFile = null;
            return;
        }
        compressedFile = new File(path);
    }

    /**
     * Sets the path to the directory into which data files should be delivered.
     *
     * @param path
     *            the path to the directory into which data files should be
     *            delivered.
     */
    protected void setDataDeliveryPath(final String path) {
        if (null == path) {
            dataDeliveryDirectory = null;
            return;
        }
        dataDeliveryDirectory = new File(path);
    }

    /**
     * Sets the {@link ExternalFile} instances that define the data to be fetched.
     *
     * @param files
     *            the {@link ExternalFile} instances that define the data to be
     *            fetched.
     */
    protected void setExternalFiles(Collection<ExternalFile> files) {
        this.externalFiles = files;
    }

    /**
     * Sets the identity of this ticket within its application.
     *
     * @param identity
     *            the identity of this ticket within its application.
     */
    protected void setIdentity(final String identity) {
        this.identity = identity;
    }

    /**
     * Sets the path to the metadata file for this ticket.
     *
     * @param path
     *            the path to the metadata file for this ticket.
     */
    protected void setMetadataPath(final String path) {
        if (null == path) {
            metadataFile = null;
            return;
        }
        metadataFile = new File(path);
    }

    /**
     * Sets the settings of available ingestion options.
     *
     * @param options
     *            the settings of available ingestion options.
     */
    protected void setOptions(final Options options) {
        this.options = options;
    }

    /**
     * Sets the collection of outbound transfers by which files matching this ticket
     * should be transferred outbound from this application.
     *
     * @param outboundTransfers
     *            the collection of outbound transfers by which files matching this
     *            registration should be transferred outbound from this application.
     */
    public void setOutboundTransfers(final Collection<String> outboundTransfers) {
        options.setOutboundTransfers(outboundTransfers);
    }

    /**
     * Sets the path to the packed file for this ticket.
     *
     * @param path
     *            the path to the packed file for this ticket.
     */
    protected void setPackedPath(final String path) {
        if (null == path) {
            packedFile = null;
            return;
        }
        packedFile = new File(path);
    }

    /**
     * Sets the location of the files in the warehouse.
     *
     * @param location
     *            the location of the files in the warehouse.
     */
    protected void setPlacedFiles(final Location location) {
        placedFiles = location;
    }

    /**
     * Sets the time-stamp of the semaphore file when examined.
     *
     * @param timestamp
     *            the time-stamp of the semaphore file when examined.
     */
    protected void setPrefetchStamp(Long timestamp) {
        prefetchStamp = timestamp;
    }

    private void setTransferFile(final FileCategory shippedCategory,
                                 final File transferFile) {
        final boolean expand;
        final boolean unpack;
        if (null == transferFile) {
            throw new NullPointerException("No TransferFile was defined");
        } else if (shippedCategory == FileCategory.COMPRESSED) {
            expand = true;
            unpack = true;
            dataDeliveryDirectory = null;
            compressedFile = transferFile;
        } else if (shippedCategory == FileCategory.PACKED) {
            expand = false;
            unpack = true;
            dataDeliveryDirectory = null;
            packedFile = transferFile;
        } else if (shippedCategory == FileCategory.EMBEDDED) {
            options.setEmbedded(true);
            expand = false;
            unpack = false;
            dataDeliveryDirectory = transferFile.getParentFile();
            packedFile = transferFile;
            metadataFile = transferFile;
        } else {
            expand = false;
            unpack = false;
            if (shippedCategory == FileCategory.DATA) {
                dataDeliveryDirectory = transferFile.getParentFile();
            } else if (shippedCategory == FileCategory.METADATA) {
                metadataFile = transferFile;
                dataDeliveryDirectory = null;
            }
        }
        options.setExpand(expand);
        options.setUnpack(unpack);
    }

    /**
     * Returns the {@link UUID} for this ticket.
     *
     * @param uuid
     *            the {@link UUID} for this ticket.
     */
    protected void setUuid(final UUID uuid) {
        this.uuid = uuid;
    }

    // static member methods (alphabetic)

    /**
     * Writes this instance to a file.
     *
     * @param file
     *            the {@link File} in which to save the ticket.
     *
     * @return the saved instance.
     *
     * @throws JAXBException
     *             when there is a problem saving the instance.
     */
    public final IngestTicket write(final File file) throws JAXBException {
        JAXBContext content = JAXBContext.newInstance(this.getClass());
        Marshaller marshaller = content.createMarshaller();
        final File tmpFile = new File(file.getParentFile(),
                                      file.getName() + InternalFileName.TMP_SUFFIX);
        marshaller.marshal(this,
                           tmpFile);
        tmpFile.renameTo(file);
        return this;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
