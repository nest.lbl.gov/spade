/**
 * The package contains the classes use to enable Lazarus to be the worflow
 * ending for this application.
 *
 * @author patton
 */
package gov.lbl.nest.spade.lazarus;