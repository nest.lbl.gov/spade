package gov.lbl.nest.spade.lazarus;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.CollectableSuspendable;
import gov.lbl.nest.common.suspension.SuspendableCollection;
import gov.lbl.nest.lazarus.management.ProcessManager;
import gov.lbl.nest.lazarus.management.ProcessManagerImpl;
import gov.lbl.nest.lazarus.structure.ControlOnlyProcess;
import gov.lbl.nest.lazarus.structure.Process;
import gov.lbl.nest.lazarus.structure.SuspendableActivity;
import gov.lbl.nest.spade.workflow.ActivityManager;
import gov.lbl.nest.spade.workflow.ActivityMonitor;
import gov.lbl.nest.spade.workflow.WorkflowDirector;

/**
 * This class implements the {@link WorkflowDirector} interface using the
 * Lazarus product.
 *
 * @author patton
 */
public class LazarusWorkflowDirector implements
                                     WorkflowDirector {

    /**
     * The suffixes of FlowNodes that should not be returned in the list of activity
     * names.
     */
    private static final String[] INVISIBLE_SUFFICES = new String[] { ".fork",
                                                                      ".join",
                                                                      ".split",
                                                                      ".merge", };

    private static boolean isInvisible(String name) {
        for (String suffix : INVISIBLE_SUFFICES) {
            if (name.endsWith(suffix)) {
                return true;
            }
        }
        return false;
    }

    /**
     * The cached list of Activity names.
     */
    private List<String> activityNames;

    /**
     * The {@link AtomicInteger} instance used to keep count of the number of
     * parallel {@link ControlOnlyProcess} instances that exist.
     */
    private AtomicInteger count;

    /**
     * The {@link ProcessManagerImpl} instance used by this object.
     */
    private final ProcessManager processManager;

    /**
     * The {@link SuspendableCollection} instance to which this Object belongs.
     */
    private SuspendableCollection collection;

    /**
     * Creates an instance of this class.
     *
     * @param processManager
     *            the {@link ProcessManager} instance used by this object.
     */
    public LazarusWorkflowDirector(ProcessManager processManager) {
        this.processManager = processManager;
        this.processManager.setCollection(this);
    }

    @Override
    public Integer addThread() {
        return processManager.addThread();
    }

    /**
     * Binds the supplied {@link Process} instance to this object.
     *
     * @param process
     *            the {@link Process} instance to bind to this object.
     */
    public void bind(Process process) {
        setCount(process.getCount());
    }

    @Override
    public void drain() throws InitializingException {
        final boolean changed = setSuspended(true);
        if (!changed) {
            return;
        }
        List<String> activities = getActivityNames();
        for (String activity : activities) {
            getActivityManager(activity).setSuspended(false);
        }
    }

    @Override
    public ActivityManager getActivityManager(String activity) {
        final SuspendableActivity controller = processManager.getSuspendableActivity(activity);
        if (null == controller) {
            return null;
        }
        return new LazarusActivityManager(controller);
    }

    @Override
    public ActivityMonitor getActivityMonitor(String activity) {
        final SuspendableActivity situation = processManager.getSuspendableActivity(activity);
        if (null == situation) {
            return null;
        }
        return new LazarusActivityMonitor(situation);
    }

    @Override
    public List<String> getActivityNames() {
        if (null == activityNames) {
            activityNames = new ArrayList<>();
            Collection<? extends CollectableSuspendable> collectableSuspendables = processManager.getCollectableSuspendables();
            for (CollectableSuspendable collectable : collectableSuspendables) {
                String name = collectable.getName();
                if (!isInvisible(name)) {
                    activityNames.add(name);
                }
            }
            Collections.sort(activityNames);
        }
        return activityNames;
    }

    @Override
    public Collection<? extends CollectableSuspendable> getCollectableSuspendables() {
        return processManager.getCollectableSuspendables();
    }

    @Override
    public AtomicInteger getCount() {
        return count;
    }

    @Override
    public AtomicInteger getExecutingCount() {
        return processManager.getExecutingCount();
    }

    @Override
    public String getName() {
        return processManager.getName();
    }

    @Override
    public Integer getPendingCount() {
        return processManager.getPendingCount();
    }

    @Override
    public Integer getThreadLimit() {
        return processManager.getExecutingLimit();
    }

    @Override
    public boolean isSuspended() throws InitializingException {
        return processManager.isSuspended();
    }

    @Override
    public boolean isSuspended(String name) throws InitializingException {
        return processManager.isSuspended(name);
    }

    @Override
    public boolean pause() {
        return processManager.pause();
    }

    @Override
    public boolean proceed() {
        return processManager.proceed();
    }

    @Override
    public Integer removeThread() {
        return processManager.removeThread();
    }

    @Override
    public void setCollection(SuspendableCollection collection) {
        this.collection = collection;
    }

    @Override
    public void setCount(AtomicInteger count) {
        this.count = count;
    }

    @Override
    public boolean setSuspended(boolean suspended) throws InitializingException {
        return processManager.setSuspended(suspended);
    }

    @Override
    public void update(CollectableSuspendable suspendable) throws InitializingException {
        collection.update(suspendable);
    }

}
