package gov.lbl.nest.spade.lazarus;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import gov.lbl.nest.lazarus.java.JavaInstanceFactory;
import gov.lbl.nest.lazarus.java.JavaInstantiationException;
import gov.lbl.nest.spade.activities.Analyzer;
import gov.lbl.nest.spade.activities.Archiver;
import gov.lbl.nest.spade.activities.Compressor;
import gov.lbl.nest.spade.activities.Confirmer;
import gov.lbl.nest.spade.activities.Duplicator;
import gov.lbl.nest.spade.activities.Examiner;
import gov.lbl.nest.spade.activities.Expander;
import gov.lbl.nest.spade.activities.Fetcher;
import gov.lbl.nest.spade.activities.Finisher;
import gov.lbl.nest.spade.activities.Packer;
import gov.lbl.nest.spade.activities.Placer;
import gov.lbl.nest.spade.activities.PostShipper;
import gov.lbl.nest.spade.activities.PreShipper;
import gov.lbl.nest.spade.activities.Shipper;
import gov.lbl.nest.spade.activities.Unpacker;
import gov.lbl.nest.spade.config.Activity;
import gov.lbl.nest.spade.config.Assembly;
import gov.lbl.nest.spade.config.CacheDefinition;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.storage.ArchiveManager;
import gov.lbl.nest.spade.interfaces.storage.WarehouseManager;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import gov.lbl.nest.spade.services.RegistrationManager;
import gov.lbl.nest.spade.services.RetryManager;
import gov.lbl.nest.spade.services.SpoolManager;
import gov.lbl.nest.spade.services.VerificationManager;
import gov.lbl.nest.spade.services.impl.LoadedDuplications;

/**
 * This class implemnts the {@link JavaInstanceFactory} for this package.
 *
 * @author patton
 */
public class JavaInstanceFactoryImpl extends
                                     JavaInstanceFactory {

    /**
     * The class is a simple data structure to hold the Stateless session beans used
     * in test.
     *
     * @author patton
     */
    public static class ExecutionEnvironment {

        /**
         * The {@link ArchiveManager} session bean to use.
         */
        public ArchiveManager archiveManager;

        /**
         * The {@link Bookkeeping} session bean to use.
         */
        public Bookkeeping bookkeeping;

        /**
         * The {@link CacheManager} session bean to use.
         */
        public CacheManager cacheManager;

        /**
         * The {@link Configuration} session bean to use.
         */
        public Configuration configuration;

        /**
         * The {@link LoadedDuplications} session bean to use.
         */
        public LoadedDuplications loadedDuplications;

        /**
         * The {@link MetadataManager} session bean to use.
         */
        public MetadataManager metadataManager;

        /**
         * The {@link NeighborhoodManager} session bean to use.
         */
        public NeighborhoodManager neighborhoodManager;

        /**
         * The {@link RegistrationManager} session bean to use.
         */
        public RegistrationManager registrationManager;

        /**
         * The {@link RetryManager} session bean to use.
         */
        public RetryManager retryManager;

        /**
         * The {@link SpoolManager} session bean to use.
         */
        public SpoolManager spoolManager;

        /**
         * The {@link VerificationManager} session bean to use.
         */
        public VerificationManager verificationManager;

        /**
         * The {@link WarehouseManager} session bean to use.
         */
        public WarehouseManager warehouseManager;
    }

    /**
     * The "class" name used to create the {@link PostShipper} instance for
     * reshipping.
     */
    private static final String POST_RESHIPPER_PSEUDO_CLASS = "gov.lbl.nest.spade.activities.PostReshipper";

    /**
     * The "class" name used to create the {@link PreShipper} instance for
     * reshipping.
     */
    private static final String PRE_RESHIPPER_PSEUDO_CLASS = "gov.lbl.nest.spade.activities.PreReshipper";

    /**
     * The "class" name used to create the {@link Archiver} instance for
     * rearchiving.
     */
    private static final String REARCHIVER_PSEUDO_CLASS = "gov.lbl.nest.spade.activities.Rearchiver";

    /**
     * The "class" name used to create the {@link Shipper} instance for reshipping.
     */
    private static final String RESHIPPER_PSEUDO_CLASS = "gov.lbl.nest.spade.activities.Reshipper";

    /**
     * The default storage factor for fetching (because fetched file will take up
     * space).
     */
    private static final Integer DEFAULT_FETCHING_STORAGE_FACTOR = 1;

    /**
     * The {@link ExecutionEnvironment} instance, if any, set manually.
     */
    private static ExecutionEnvironment manualExecutionEnvironment;

    /**
     * Sets the {@link ExecutionEnvironment} instance manually.
     *
     * @param executionEnvironment
     *            the {@link ExecutionEnvironment} instance to be set.
     */
    public static void setManualExecutionEnvironment(ExecutionEnvironment executionEnvironment) {
        manualExecutionEnvironment = executionEnvironment;
    }

    /**
     * The {@link Assembly} used by this object.
     */
    private Assembly assembly;
    /**
     * The {@link CacheDefinition} instance used by this Object.
     */
    private CacheDefinition cacheDefinition;

    /**
     * The directory that holds the configuration information.
     */
    private File configurationDir;

    /**
     * The {@link ExecutionEnvironment} instance used by this class.
     */
    private ExecutionEnvironment executionEnvironment;

    @Override
    protected Object createInstance(String implementation) throws JavaInstantiationException {
        try {
            String interfaceName = implementation;
            if ((Analyzer.class.getName()).equals(interfaceName)) {
                final Analyzer activity = new Analyzer(getActivity(Activity.ANALYZER));
                Object result = new SpadeActivityLazarusAdapter(Activity.ANALYZER,
                                                                activity);
                return result;
            }
            if ((Archiver.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Archiver activity = new Archiver(getActivity(Activity.ARCHIVER),
                                                       getConfigurationDir(),
                                                       environment.archiveManager,
                                                       environment.bookkeeping,
                                                       environment.metadataManager,
                                                       environment.spoolManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.ARCHIVER,
                                                                activity);
                return result;
            }
            if ((Compressor.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Compressor activity = new Compressor(getActivity(Activity.COMPRESSOR),
                                                           environment.bookkeeping,
                                                           getCacheDefinition(),
                                                           environment.cacheManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.COMPRESSOR,
                                                                activity);
                return result;
            }
            if ((Confirmer.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Confirmer activity = new Confirmer(getActivity(Activity.CONFIRMER),
                                                         environment.verificationManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.CONFIRMER,
                                                                activity);
                return result;
            }
            if ((Duplicator.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Duplicator activity = new Duplicator(getActivity(Activity.DUPLICATOR),
                                                           getCacheDefinition(),
                                                           environment.loadedDuplications);
                Object result = new SpadeActivityLazarusAdapter(Activity.DUPLICATOR,
                                                                activity);
                return result;
            }
            if ((Examiner.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Examiner activity = new Examiner(getActivity(Activity.EXAMINER),
                                                       getActivity(Activity.FETCHER),
                                                       getActivity(Activity.RECEIVER),
                                                       getConfigurationDir(),
                                                       environment.bookkeeping,
                                                       getCacheDefinition(),
                                                       environment.cacheManager,
                                                       environment.metadataManager,
                                                       environment.neighborhoodManager,
                                                       environment.registrationManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.EXAMINER,
                                                                activity);
                return result;
            }
            if ((Expander.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Expander activity = new Expander(getActivity(Activity.EXPANDER),
                                                       environment.bookkeeping,
                                                       getCacheDefinition(),
                                                       environment.cacheManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.EXPANDER,
                                                                activity);
                return result;
            }
            if ((Fetcher.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Activity activityToUse = getActivity(Activity.FETCHER);
                if (null == activityToUse.getStorageFactor()) {
                    activityToUse.setStorageFactor(DEFAULT_FETCHING_STORAGE_FACTOR);
                }
                final Fetcher activity = new Fetcher(activityToUse,
                                                     environment.bookkeeping,
                                                     environment.cacheManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.FETCHER,
                                                                activity);
                return result;
            }
            if ((Finisher.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Finisher activity = new Finisher(getActivity(Activity.FINISHER),
                                                       environment.cacheManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.FINISHER,
                                                                activity);
                return result;
            }
            if ((Packer.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Packer activity = new Packer(getActivity(Activity.PACKER),
                                                   environment.bookkeeping,
                                                   getCacheDefinition(),
                                                   environment.cacheManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.PACKER,
                                                                activity);
                return result;
            }
            if ((Placer.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Placer activity = new Placer(getActivity(Activity.PLACER),
                                                   configurationDir,
                                                   environment.bookkeeping,
                                                   environment.metadataManager,
                                                   environment.warehouseManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.PLACER,
                                                                activity);
                return result;
            }
            if (POST_RESHIPPER_PSEUDO_CLASS.equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final PostShipper activity = new PostShipper(getActivity(Activity.POST_RESHIPPER),
                                                             environment.neighborhoodManager,
                                                             environment.retryManager,
                                                             environment.verificationManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.POST_RESHIPPER,
                                                                activity);
                return result;
            }
            if ((PostShipper.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final PostShipper activity = new PostShipper(getActivity(Activity.POST_SHIPPER),
                                                             environment.neighborhoodManager,
                                                             environment.retryManager,
                                                             environment.verificationManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.POST_SHIPPER,
                                                                activity);
                return result;
            }
            if (PRE_RESHIPPER_PSEUDO_CLASS.equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final PreShipper activity = new PreShipper(getActivity(Activity.PRE_RESHIPPER),
                                                           environment.verificationManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.PRE_RESHIPPER,
                                                                activity);
                return result;
            }
            if ((PreShipper.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final PreShipper activity = new PreShipper(getActivity(Activity.PRE_SHIPPER),
                                                           environment.verificationManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.PRE_SHIPPER,
                                                                activity);
                return result;
            }
            if (REARCHIVER_PSEUDO_CLASS.equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Archiver activity = new Archiver(getActivity(Activity.REARCHIVER),
                                                       getConfigurationDir(),
                                                       environment.archiveManager,
                                                       environment.bookkeeping,
                                                       environment.metadataManager,
                                                       environment.spoolManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.REARCHIVER,
                                                                activity);
                return result;
            }
            if (RESHIPPER_PSEUDO_CLASS.equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Shipper activity = new Shipper(getActivity(Activity.RESHIPPER),
                                                     environment.bookkeeping,
                                                     getCacheDefinition(),
                                                     getConfigurationDir(),
                                                     environment.neighborhoodManager,
                                                     environment.verificationManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.RESHIPPER,
                                                                activity);
                return result;
            }
            if ((Shipper.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Shipper activity = new Shipper(getActivity(Activity.SHIPPER),
                                                     environment.bookkeeping,
                                                     getCacheDefinition(),
                                                     getConfigurationDir(),
                                                     environment.neighborhoodManager,
                                                     environment.verificationManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.SHIPPER,
                                                                activity);
                return result;
            }
            if ((Unpacker.class.getName()).equals(interfaceName)) {
                final ExecutionEnvironment environment = getExecutionEnvironment();
                final Unpacker activity = new Unpacker(getActivity(Activity.UNPACKER),
                                                       environment.bookkeeping,
                                                       getCacheDefinition(),
                                                       environment.cacheManager);
                Object result = new SpadeActivityLazarusAdapter(Activity.UNPACKER,
                                                                activity);
                return result;
            }
            Class<?> clazz = (getClass().getClassLoader()).loadClass(interfaceName);
            return (clazz.getDeclaredConstructor()).newInstance();
        } catch (ClassNotFoundException
                 | IllegalAccessException
                 | InstantiationException
                 | InvocationTargetException
                 | NoSuchMethodException e) {
            throw new JavaInstantiationException(e);
        }
    }

    /**
     * Returns an {@link Activity} instance for the specified activities name.
     *
     * @param name
     *            the name of the class whose {@link Activity} instance should be
     *            returned.
     *
     * @return the {@link Activity} instance for the specified activities name.
     */
    private Activity getActivity(final String name) {
        final Assembly assembly = getAssembly();
        final Activity activity = assembly.getActivity(name);
        final Activity activityToUse;
        if (null == activity) {
            activityToUse = new Activity(name);
        } else {
            activityToUse = activity;
        }
        return activityToUse;
    }

    /**
     * Returns the {@link Assembly} used by this object.
     *
     * @return the {@link Assembly} used by this object.
     */
    private Assembly getAssembly() {
        if (null == assembly) {
            assembly = (getExecutionEnvironment().configuration).getAssembly();
        }
        return assembly;
    }

    private CacheDefinition getCacheDefinition() {
        if (null == cacheDefinition) {
            cacheDefinition = (getExecutionEnvironment().configuration).getCacheDefinition();
        }
        return cacheDefinition;
    }

    /**
     * Returns the directory that holds the configuration information.
     *
     * @return the directory that holds the configuration information.
     */
    private File getConfigurationDir() {
        if (null == configurationDir) {
            configurationDir = (getExecutionEnvironment().configuration).getDirectory();
        }
        return configurationDir;
    }

    /**
     * Returns the {@link ExecutionEnvironment} instance used by this class.
     *
     * @return the {@link ExecutionEnvironment} instance used by this class.
     */
    private ExecutionEnvironment getExecutionEnvironment() {
        if (null != manualExecutionEnvironment) {
            return manualExecutionEnvironment;
        }
        if (null == executionEnvironment) {
            try {
                final InitialContext context = new InitialContext();
                final ExecutionEnvironmentSupplier supplier = (ExecutionEnvironmentSupplier) context.lookup("java:global/spade/ExecutionEnvironmentSupplier!gov.lbl.nest.spade.lazarus.ExecutionEnvironmentSupplier");
                executionEnvironment = supplier.getExecutionEnvironment();
            } catch (NamingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }
        }
        return executionEnvironment;
    }

    @Override
    protected Method getMethod(Object instance,
                               String methodName,
                               Class<?> input) throws NoSuchMethodException {
        if (instance instanceof SpadeActivityLazarusAdapter) {
            final Method method = ((SpadeActivityLazarusAdapter) instance).getMethod(methodName,
                                                                                     input);
            return method;
        }
        final Method method = (instance.getClass()).getMethod(methodName,
                                                              new Class<?>[] { input });
        return method;
    }

}
