package gov.lbl.nest.spade.lazarus;

import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.spade.workflow.ActivityTaskStatus;

/**
 * This class implements the {@link ActivityTaskStatus} interface for this
 * package.
 *
 * @author patton
 */
public class LazarusActivityTaskStatus implements
                                       ActivityTaskStatus {

    /**
     * The {@link FlowNodeInspector} instance used by this object.
     */
    private final FlowNodeInspector inspector;

    /**
     * Creates an instance of this class.
     *
     * @param inspector
     *            the {@link FlowNodeInspector} instance used by this object.
     */
    LazarusActivityTaskStatus(FlowNodeInspector inspector) {
        this.inspector = inspector;
    }

    @Override
    public String getBundle() {
        return inspector.getLabel();
    }

    @Override
    public STATE getState() {
        if (null == inspector.getThread()) {
            if (null == inspector.getLabel()) {
                return STATE.COMPLETE;
            }
            return STATE.PENDING;
        }
        return STATE.EXECUTING;
    }

}
