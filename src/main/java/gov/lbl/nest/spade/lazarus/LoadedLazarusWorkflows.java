package gov.lbl.nest.spade.lazarus;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.common.suspension.SuspendableImpl;
import gov.lbl.nest.common.tasks.Consumer;
import gov.lbl.nest.common.tasks.Consumer.Producer;
import gov.lbl.nest.common.tasks.Consumer.Task;
import gov.lbl.nest.common.tasks.OpportunisticProducers;
import gov.lbl.nest.common.tasks.ParallelConsumer;
import gov.lbl.nest.common.tasks.Supplier;
import gov.lbl.nest.common.tasks.TaskTracker;
import gov.lbl.nest.common.tasks.TrackingSupplier;
import gov.lbl.nest.lazarus.bpmn.XMLDefinitions;
import gov.lbl.nest.lazarus.control.FlowNodeTaskProducer;
import gov.lbl.nest.lazarus.execution.Cause;
import gov.lbl.nest.lazarus.execution.ExecutableProcess;
import gov.lbl.nest.lazarus.execution.ProcessDefinition;
import gov.lbl.nest.lazarus.execution.ProcessScheduler;
import gov.lbl.nest.lazarus.execution.TerminationListener;
import gov.lbl.nest.lazarus.execution.ThrownFailure;
import gov.lbl.nest.lazarus.scheduling.ClassicScheduler;
import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.lazarus.structure.FlowNodeTask;
import gov.lbl.nest.lazarus.structure.Process;
import gov.lbl.nest.spade.SpadeSuspension;
import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.config.ApplicationSuspension;
import gov.lbl.nest.spade.config.Assembly;
import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.config.ThreadLimit;
import gov.lbl.nest.spade.services.NotificationManager;
import gov.lbl.nest.spade.services.NotificationManager.Scope;
import gov.lbl.nest.spade.services.TerminationManager;
import gov.lbl.nest.spade.services.TicketManager;
import gov.lbl.nest.spade.workflow.LoadedWorkflows;
import gov.lbl.nest.spade.workflow.WorkflowCollection;
import gov.lbl.nest.spade.workflow.WorkflowCollection.Workflow;
import gov.lbl.nest.spade.workflow.WorkflowDirector;
import gov.lbl.nest.spade.workflow.WorkflowTermination;
import gov.lbl.nest.spade.workflow.impl.WorkflowCollectionImpl;
import gov.lbl.nest.spade.xpath.FnNamespaceContextImpl;
import gov.lbl.nest.spade.xpath.FnXPathFunctionResolverImpl;
import jakarta.annotation.Resource;
import jakarta.ejb.LockType;
import jakarta.ejb.Schedule;
import jakarta.ejb.Singleton;
import jakarta.ejb.Startup;
import jakarta.enterprise.concurrent.ManagedExecutorService;
import jakarta.inject.Inject;
import jakarta.xml.bind.JAXBException;

/**
 * This class contains all of the workflows loaded into the application.
 *
 * *Note:* This class does _not_ implement the Suspendable interface as that
 * dependency messes up the Injection mechanics.
 *
 * @author patton
 */
@Startup
@Singleton
/** The class does it own concurrency management, so can use READ lock. */
@jakarta.ejb.Lock(LockType.READ)
public class LoadedLazarusWorkflows implements
                                    LoadedWorkflows {

    static class TerminationListenerAdapter implements
                                            TerminationListener {

        final WorkflowTermination termination;

        TerminationListenerAdapter(final WorkflowTermination termination) {
            this.termination = termination;
        }

        @Override
        public boolean failure(ExecutableProcess process) {
            Map<String, Object> dataObjects = process.getValues();
            IngestTicket ticket = (IngestTicket) dataObjects.get(INGESTION_TICKET);
            if (null == ticket) {
                throw new Error("Should not be able to get here under normal circumstances!");
            }
            final ThrownFailure thrownFailure = process.getFailure();
            final Cause cause = thrownFailure.getCause();
            return termination.failed(ticket,
                                      thrownFailure.getOwner(),
                                      thrownFailure.getRescue(),
                                      cause.getThrowable());
        }

        @Override
        public void success(ExecutableProcess process) {
            Map<String, Object> dataObjects = process.getValues();
            Object ticket = dataObjects.get(INGESTION_TICKET);
            if (null == ticket) {
                throw new Error("Should not be able to get here under normal circumstances!");
            }
            termination.succeeded(ticket);
        }
    }

    /**
     * The name of the files containing the BPMN diagrams for each {@link Workflow}
     * instance in order.
     */
    private static final String[] BPMN_DIAGRAMS = new String[] { "ingest.bpmn",
                                                                 "reship.bpmn",
                                                                 "rearchive.bpmn" };

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(LoadedLazarusWorkflows.class);

    /**
     * The data object name of the ticket used in the ingestion workflow.
     */
    static final String INGESTION_TICKET = "ingestion ticket";

    /**
     * The time to wait to get the initialization lock. If the lock has not been
     * acquired in this time it is assumed that another thread is initializing the
     * workflows.
     */
    private static final long LOCK_WAIT = 5;

    /**
     * The {@link TimeUnit} in which the LOCK_WAIT is specified.
     */
    private static final TimeUnit LOCK_WAIT_UNIT = TimeUnit.SECONDS;

    /**
     * Selects the limit to use if there are multiple limits for the same workflow
     * (and bad idea!).
     *
     * @param workflow
     *            the {@link Workflow} instance for which to select the limit.
     * @param count
     *            the default limit for the cnfiguration file.
     * @param limit
     *            the {@link ThreadLimit} instance from the configuration file.
     *
     * @return
     */
    private static Integer selectLimit(Workflow workflow,
                                       Integer count,
                                       ThreadLimit limit) {
        if (null == count) {
            if (null == limit.getLimit()) {
                return Integer.valueOf(1);
            }
            return limit.getLimit();
        }
        LOG.warn("Multiple thread_limit declarations for the \"" + workflow.toString()
                 + "\" workflow. Largest will be used.");
        if ((limit.getLimit()).intValue() > count.intValue()) {
            return limit.getLimit();
        }
        return count;
    }

    /**
     * The {@link Configuration} instance of this application.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link DeferredExecutionManager} used by this Object.
     */
    private final Object deferredExecutionManager = null;

    /**
     * The {@link ManagedExecutorService} instance used for initialization.
     */
    @Resource
    ManagedExecutorService executor;

    /**
     * True if the workflows have been loaded.
     */
    private final AtomicBoolean loaded = new AtomicBoolean();

    /**
     * True if the workflows are in the process of being leaded.
     */
    private final AtomicBoolean loading = new AtomicBoolean();

    /**
     * The {@link Lock} instance used to make sure that the workflow loading is
     * serialized.
     */
    private final Lock lock = new ReentrantLock();

    /**
     * The {@link NotificationManager} instance used by this Object.
     */
    @Inject
    private NotificationManager notificationManager;

    /**
     * The is the current stall has already sent of a notification.
     */
    private final AtomicBoolean notified = new AtomicBoolean();

    /**
     * The {@link ProcessScheduler} instance used by this object.
     */
    private final ProcessScheduler scheduler = new ClassicScheduler();

    /**
     * The {@link Suspendable} instance used by this object.
     */
    @Inject
    @SpadeSuspension
    private Suspendable suspended;

    /**
     * The {@link TerminationManager} used by this object.
     */
    @Inject
    private TerminationManager terminationManager;

    /**
     * The {@link TicketManager} used by this object.
     */
    @Inject
    private TicketManager ticketManager;

    /**
     * The {@link Duration} instance for how long a {@link FlowNodeTask} can run
     * before being declared stalled.
     */
    private final Duration timeLimit;

    /**
     * The {@link TaskTracker} instance used by this application.
     */
    @Inject
    private TaskTracker<FlowNodeTask> tracker;

    /**
     * The {@link WorkflowCollection} used by this Object.
     */
    private WorkflowCollection workflowCollection;

    /**
     * The map of {@link Workflow} instances to {@link Process} instances.
     */
    private final Map<Workflow, ProcessDefinition> workflows = new HashMap<>();

    /**
     * The {@link XMLDefinitions} instance used by this object.
     */
    private final XMLDefinitions xmlDefinitions = new XMLDefinitions(new FnNamespaceContextImpl(null),
                                                                     new FnXPathFunctionResolverImpl(null),
                                                                     null,
                                                                     false);

    /**
     * Creates an instance of this class.
     */
    protected LoadedLazarusWorkflows() {
        // Test with one minute seconds
        this.timeLimit = Duration.ofMillis(60000);
    }

    /**
     * Creates an instance of this class for teasting purposes.
     *
     * @param configuration
     *            the {@link Configuration} instance to be used by this object.
     * @param applicationSuspension
     *            the {@link ApplicationSuspension} instance to be used by this
     *            object.
     * @param executor
     *            the {@link ManagedExecutorService} instance to be used by this
     *            object.
     */
    LoadedLazarusWorkflows(Configuration configuration,
                           ApplicationSuspension applicationSuspension,
                           ManagedExecutorService executor) {
        this.configuration = configuration;
        this.executor = executor;
        suspended = applicationSuspension;
        this.timeLimit = Duration.ofMillis(60000);
    }

    /**
     * Adds a new {@link Process} instance to this object.
     *
     * @param workflow
     *            the {@link Workflow} instance to which the {@link Process}
     *            instance should be assigned.
     * @param process
     *            the {@link Process} instance to add.
     */
    protected void addWorkflow(Workflow workflow,
                               ProcessDefinition process) {
        final LazarusWorkflowDirector workflowDirector = (LazarusWorkflowDirector) workflowCollection.getWorkflowDirector(workflow);
        workflowDirector.bind(process);
        workflows.put(workflow,
                      process);
    }

    /**
     * Adds a new {@link WorkflowDirector} instance to this object.
     *
     * @param workflow
     *            the {@link Workflow} instance to which the
     *            {@link WorkflowDirector} instance should be assigned.
     * @param director
     *            the {@link WorkflowDirector} instance to add.
     */
    protected void addWorkflowDirector(Workflow workflow,
                                       WorkflowDirector director) {
        try {
            workflowCollection.addWorkflowDirector(workflow,
                                                   director);
        } catch (InitializingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void addWorkflowTermination(Workflow workflow,
                                       WorkflowTermination termination) throws InitializingException {
        final TerminationListenerAdapter listener = new TerminationListenerAdapter(termination);
        if (null == workflow) {
            for (Workflow flow : Workflow.values()) {
                getWorkflow(flow).setDefaultTerminationListener(listener);
            }
            return;
        }
        getWorkflow(workflow).setDefaultTerminationListener(listener);
    }

    /**
     * Looks at all executing {@link Task} instances to see if any have been
     * executing for too long.
     */
    @Schedule(hour = "*",
              minute = "*")
    void detectStalledTasks() {
        final List<FlowNodeTask> waiting = tracker.getWaiting();
        final List<FlowNodeTask> executing = tracker.getExecuting();
        LOG.info("Scheduled Task Check: " + waiting.size()
                 + " waiting, and "
                 + executing.size()
                 + " executing.");
        if (null == executing || executing.isEmpty()) {
            return;
        }
        final Iterator<FlowNodeTask> iterator = executing.iterator();
        boolean done = false;
        int count = 0;
        while (!done && iterator.hasNext()) {
            final FlowNodeTask task = iterator.next();
            final Duration executionDuration = task.getExecutionDuration();
            if ((timeLimit.minus(executionDuration)).isNegative()) {
                final FlowNodeInspector inspector = task.inspect();
                LOG.debug("The bundle " + inspector.getLabel()
                          + " appears to have stalled in the "
                          + inspector.getFlowNode()
                          + " Activity");
                ++count;
            } else {
                done = true;
            }
        }
        if (0 != count) {
            if (!notified.get()) {
                notificationManager.postNotice(Scope.MISCELLANEOUS,
                                               "One or more Activities appears to have stalled");
                notified.set(true);
            }
            final String plural;
            if (1 == count) {
                plural = " has";
            } else {
                plural = "s have";
            }
            LOG.warn("It appears that " + count
                     + " task"
                     + plural
                     + " stalled");
        } else {
            if (notified.get()) {
                notificationManager.postNotice(Scope.MISCELLANEOUS,
                                               "All stalled Activities have now terminated");
                notified.set(false);
            }
        }
    }

    /**
     * Allow all workflows to "drain", by suspending the work themselves, so they
     * will reject any more attempts to start a new instance, but resuming all
     * activities in the workflows.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    @Override
    public void drain() throws InitializingException {
        suspended.setSuspended(true);
        loadWorkflows();
        for (Workflow workflow : workflows.keySet()) {
            getWorkflowDirector(workflow).drain();
        }
    }

    /**
     * Returns the names of all activities in the specified workflow.
     *
     * @param workflow
     *            the workflow of all activities in the specified workflow.
     *
     * @return the names of all known tasks.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    @Override
    public List<? extends String> getActivityNames(Workflow workflow) throws InitializingException {
        return getWorkflowDirector(workflow).getActivityNames();
    }

    /**
     * Returns the limit to use if the initial count is not supplied or not
     * sensible, otherwise returns the initial count.
     *
     * @param initialCount
     *            the initial count.
     *
     * @return the limit to use if the initial count is not supplied or not
     *         sensible, otherwise returns the initial count.
     */
    private int getLimitToUse(Integer initialCount) {
        final int limitToUse;
        if (null == initialCount || 0 >= initialCount.intValue()) {
            final int target = (Runtime.getRuntime()).availableProcessors() - 1;
            if (0 >= target) {
                limitToUse = 1;
            } else {
                limitToUse = target;
            }
        } else {
            limitToUse = initialCount;
        }
        return limitToUse;
    }

    /**
     * Returns the thread count to used for the specified {@link Workflow} instance.
     *
     * @param assembly
     *            the {@link Assembly} instance from which to extra the number
     * @param workflow
     *            the {@link Workflow} instance for which to return the answer.
     *
     * @return the thread count to used for the specified {@link Workflow} instance.
     */
    protected Integer getThreadCount(final Assembly assembly,
                                     final Workflow workflow) {
        Integer threadCount = null;
        final List<ThreadLimit> limits = assembly.getThreadLimit();
        if (null == limits) {
            threadCount = Assembly.getDefaultThreadLimit(workflow,
                                                         threadCount);
        } else {
            for (ThreadLimit limit : limits) {
                final String limitedWorkflow = limit.getWorkflow();
                if (null == limitedWorkflow) {
                    if (Workflow.INGEST == workflow) {
                        threadCount = selectLimit(workflow,
                                                  threadCount,
                                                  limit);
                    }
                } else {
                    if (limitedWorkflow.equals(workflow.toString())) {
                        threadCount = selectLimit(workflow,
                                                  threadCount,
                                                  limit);
                    }
                }
            }
            if (null == threadCount) {
                threadCount = Integer.valueOf(1);
            }
        }
        return threadCount;
    }

    /**
     * Returns the {@link Process} instance for the specified workflow.
     *
     * @param workflow
     *            the workflow that should be returned.
     *
     * @return the {@link Process} instance for the specified workflow.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    public ProcessDefinition getWorkflow(final Workflow workflow) throws InitializingException {
        loadWorkflows();
        return workflows.get(workflow);
    }

    /**
     * Returns the {@link WorkflowCollection} used by this Object.
     *
     *
     * @return the {@link WorkflowCollection} used by this Object.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    private WorkflowCollection getWorkflowCollection() throws InitializingException {
        loadWorkflows();
        return workflowCollection;
    }

    /**
     * Returns the {@link WorkflowDirector} used by the specified workflow.
     *
     * @param workflow
     *            the {@link Workflow} whose {@link WorkflowDirector} should be
     *            returned.
     *
     * @return the {@link WorkflowDirector} used by the specified workflow.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    @Override
    public WorkflowDirector getWorkflowDirector(Workflow workflow) throws InitializingException {
        return getWorkflowCollection().getWorkflowDirector(workflow);
    }

    /**
     * Returns true when this object is suspended.
     *
     * @return true when this object is suspended.
     *
     * @throws InitializingException
     *             when this Onject is still initializing.
     */
    @Override
    public boolean isSuspended() throws InitializingException {
        return suspended.isSuspended();
    }

    /**
     * Loads the known workflows into this object.
     *
     * @throws InitializingException
     *             when the workflows are still initializing.
     */
    public void loadWorkflows() throws InitializingException {
        if (!loaded.get()) {
            try {
                if (lock.tryLock(LOCK_WAIT,
                                 LOCK_WAIT_UNIT)) {
                    try {

                        // Check it was not done while waiting for the lock.
                        if (!loaded.get() && !loading.get()) {
                            LOG.info("Beginning to load Workflows");
                            loading.set(true);

                            final Assembly assembly = configuration.getAssembly();
                            final File suspensionDir = (Configuration.getSuspendFile(configuration)).getParentFile();
                            workflowCollection = new WorkflowCollectionImpl(suspended,
                                                                            new File(suspensionDir,
                                                                                     assembly.getName() + ".suspended"));
                            final List<Consumer> consumers = new ArrayList<>();
                            final Iterator<Workflow> iterator = (Arrays.asList(Workflow.values())).iterator();
                            Producer<FlowNodeTask> preceeding = new FlowNodeTaskProducer();
                            LOG.info("Creating Workflow consumers");
                            while (iterator.hasNext()) {
                                final Workflow workflow = iterator.next();
                                final Producer<FlowNodeTask> succeeding;
                                if (iterator.hasNext()) {
                                    succeeding = new FlowNodeTaskProducer();
                                } else {
                                    succeeding = null;
                                }
                                final OpportunisticProducers<FlowNodeTask> producers = new OpportunisticProducers<>(succeeding,
                                                                                                                    preceeding);
                                preceeding = producers.getSingleProducer();
                                Integer threadCount = getThreadCount(assembly,
                                                                     workflow);
                                consumers.add(new ParallelConsumer(workflow.toString(),
                                                                   getLimitToUse(threadCount),
                                                                   producers.getDoubleProducer(),
                                                                   executor));
                            }

                            // Launch workload preparation is separate thread to avoid blocking this one.
                            LOG.info("Submitting Workflow Initalization");
                            Future<?> future = executor.submit(new Runnable() {

                                @Override
                                public void run() {
                                    LOG.info("Initializing Workflows");
                                    try {
                                        prepareWorkflows(consumers,
                                                         suspensionDir.toPath());
                                        loaded.set(true);
                                        loading.set(false);
                                        LOG.info("Workflows Initialized");
                                    } catch (NoSuchMethodException e) {
                                        LOG.info("Workflow Initialized");
                                        // TODO: decide what to do here!
                                        e.printStackTrace();
                                    }
                                }

                            });
                            try {
                                future.get(LOCK_WAIT,
                                           LOCK_WAIT_UNIT);
                            } catch (ExecutionException e) {
                                // TODO: decide what to do here!
                                e.printStackTrace();
                            } catch (TimeoutException e) {
                                // Do nothing as completion test is below.
                            }
                        }
                    } finally {
                        lock.unlock();
                    }
                    if (!loaded.get()) {
                        throw new InitializingException("The Workflows are still initializing");
                    }
                } else {
                    throw new InitializingException("The Workflows are still initializing");
                }
            } catch (InterruptedException e) {
                // Do nothing, abandon this attempt.
            }
        }
    }

    /**
     * Prepares workflows for the supplied {@link Workflow} values.
     *
     * @param consumer
     *            the {@link Consumer} instances for the workflow.
     * @param path
     *            the {@link Path} instance, if any, to the file containing the set
     *            of names of suspended {@link FlowNodeTask} instances.
     *
     * @throws NoSuchMethodException
     *             when the BPMN uses a method that is not in the classpath.
     */
    private void prepareWorkflow(Workflow workflow,
                                 Consumer consumer,
                                 Supplier<FlowNodeTask> supplier,
                                 String resource,
                                 Path path) throws NoSuchMethodException {
        LOG.info("Preparing " + workflow.name()
                 + " workflow");
        try (InputStream inputStream = getClass().getResourceAsStream(resource)) {
            LOG.debug("Building Process");
            final Supplier<FlowNodeTask> supplierToUse;
            if (null == tracker) {
                supplierToUse = supplier;
            } else {
                supplierToUse = new TrackingSupplier<>(supplier,
                                                       tracker,
                                                       FlowNodeTask.class);
            }

            final boolean suspended = workflowCollection.isWorkflowSuspended(workflow);
            final String name = workflow.toString();
            final Path pathToUse;
            if (null == path) {
                pathToUse = null;
            } else {
                pathToUse = path.resolve(name + ".suspended");
            }
            final Map<String, ? extends ProcessDefinition> processes = xmlDefinitions.parseDefinitions(inputStream,
                                                                                                       consumer,
                                                                                                       supplierToUse,
                                                                                                       new SuspendableImpl(name,
                                                                                                                           suspended),
                                                                                                       pathToUse);
            if (1 != processes.size()) {
                throw new UnsupportedOperationException("Only a single Process per BPMN file can be handled.");
            }
            final ProcessDefinition process = ((processes.values()).iterator()).next();
            final WorkflowDirector director = new LazarusWorkflowDirector(process.getProcessManager());
            workflowCollection.addWorkflowDirector(workflow,
                                                   director);

            LOG.debug("Setting termination");
            setTermination(workflow,
                           process);
            LOG.debug("Installing workflow");
            addWorkflow(workflow,
                        process);
            LOG.debug("Recovering instances");
            Collection<? extends ExecutableProcess> executableProcesses = process.recoverExecutableProcesses();
            LOG.debug("Reissuing any recovered tickets");
            for (ExecutableProcess executableProcess : executableProcesses) {
                final IngestTicket ticket = (IngestTicket) (executableProcess.getValues()).get(INGESTION_TICKET);
                ticketManager.reissue(ticket);
                scheduler.addExecutableProcess(executableProcess);
            }
            LOG.info(workflow.name() + " workflow is ready");
        } catch (ClassNotFoundException
                 | JAXBException
                 | IOException
                 | NoSuchElementException
                 | InitializingException e) {
            e.printStackTrace();
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    /**
     * Prepares workflows for all of the known {@link Workflow} values.
     *
     * @param consumers
     *            the collection of {@link Consumer} instances or the workflows.
     * @param path
     *            the {@link Path} instance, if any, to the file containing the set
     *            of names of suspended {@link FlowNodeTask} instances.
     *
     * @throws NoSuchMethodException
     *             when the BPMN uses a method that is not in the classpath.
     */
    protected void prepareWorkflows(List<Consumer> consumers,
                                    Path path) throws NoSuchMethodException {
        for (Workflow workflow : Workflow.values()) {
            final int index = workflow.ordinal();
            final Consumer consumer = consumers.remove(0);
            @SuppressWarnings("unchecked")
            final Supplier<FlowNodeTask> supplier = (Supplier<FlowNodeTask>) consumer.getSupplier();
            prepareWorkflow(workflow,
                            consumer,
                            supplier,
                            BPMN_DIAGRAMS[index],
                            path);
        }
    }

    @Override
    public void recommence(final URI uri,
                           final Object message) {
        if (null == deferredExecutionManager) {
            throw new IllegalStateException("No DeferredExecutionManager declared so it is not possible to recommence a Task");
        }
        throw new UnsupportedOperationException();
    }

    @Override
    public List<String> rescueWorkflow(Workflow workflow) throws InitializingException {
        LOG.debug("Recovering instances");
        final ProcessDefinition process = workflows.get(workflow);
        final Collection<? extends ExecutableProcess> executableProcesses = process.rescueExecutableProcesses();
        LOG.debug("Reissuing any rescued tickets");
        final List<String> results = new ArrayList<>();
        for (ExecutableProcess executableProcess : executableProcesses) {
            final IngestTicket ticket = (IngestTicket) (executableProcess.getValues()).get(INGESTION_TICKET);
            ticketManager.reissue(ticket);
            if (scheduler.addExecutableProcess(executableProcess)) {
                results.add(executableProcess.getLabel());
            }
        }
        return results;
    }

    /**
     * Sets whether execution of this object should be suspended or not.
     *
     * @param suspend
     *            True when execution of this object should be suspended.
     *
     * @return true if the state of this object has changed.
     */
    @Override
    public boolean setSuspended(boolean suspend) {
        try {
            return getWorkflowCollection().setSuspended(suspend);
        } catch (InitializingException e) {
            return false;
        }
    }

    private void setTermination(Workflow workflow,
                                ProcessDefinition process) {
        final WorkflowTermination termination = terminationManager.createIngestTermination();
        process.setDefaultTerminationListener(new TerminationListenerAdapter(termination));
    }

    @Override
    public boolean startWorkflow(Workflow workflow,
                                 String name,
                                 Integer priority,
                                 Object dataObject) throws InitializingException {
        final Map<String, Object> values = new HashMap<>();
        values.put(INGESTION_TICKET,
                   dataObject);

        final ProcessDefinition process = workflows.get(workflow);
        final ExecutableProcess executableProcess = scheduler.addProcessDefinition(process,
                                                                                   name,
                                                                                   priority,
                                                                                   values);
        return null != executableProcess;
    }
}
