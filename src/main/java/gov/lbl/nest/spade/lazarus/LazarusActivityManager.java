package gov.lbl.nest.spade.lazarus;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.lazarus.execution.SuspendableFlowNodeExecutions;
import gov.lbl.nest.lazarus.process.SuspendableActivityImpl;
import gov.lbl.nest.spade.workflow.ActivityManager;

/**
 * This class implements the {@link ActivityManager} interface for this package.
 *
 * @author patton
 */
public class LazarusActivityManager extends
                                    LazarusActivityMonitor implements
                                    ActivityManager {

    /**
     * Creates an instance of this class.
     *
     * @param executions
     *            the {@link SuspendableActivityImpl} instance used by this object.
     */
    LazarusActivityManager(SuspendableFlowNodeExecutions executions) {
        super(executions);
    }

    @Override
    public boolean setSuspended(boolean suspended) throws InitializingException {
        return getSuspendableFlowNodeExecutions().setSuspended(suspended);
    }

}
