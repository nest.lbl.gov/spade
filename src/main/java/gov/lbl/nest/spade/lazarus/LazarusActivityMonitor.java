package gov.lbl.nest.spade.lazarus;

import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.lazarus.execution.FlowNodeExecutions;
import gov.lbl.nest.lazarus.execution.SuspendableFlowNodeExecutions;
import gov.lbl.nest.lazarus.process.SuspendableActivityImpl;
import gov.lbl.nest.lazarus.structure.FlowNodeInspector;
import gov.lbl.nest.spade.workflow.ActivityMonitor;
import gov.lbl.nest.spade.workflow.ActivityTaskStatus;

/**
 * This class implements the {@link ActivityMonitor} interface for this package.
 *
 * @author patton
 */
public class LazarusActivityMonitor implements
                                    ActivityMonitor {

    /**
     * The {@link SuspendableFlowNodeExecutions} instance used by this object.
     */
    private final SuspendableFlowNodeExecutions executions;

    /**
     * Creates an instance of this class.
     *
     * @param executions
     *            the {@link FlowNodeExecutions} instance used by this object.
     */
    LazarusActivityMonitor(SuspendableFlowNodeExecutions executions) {
        this.executions = executions;
    }

    @Override
    public List<ActivityTaskStatus> getActivityTaskStatuses() {
        final List<ActivityTaskStatus> result = new ArrayList<>();
        for (FlowNodeInspector inspector : executions.getInspectors()) {
            result.add(new LazarusActivityTaskStatus(inspector));
        }
        return result;
    }

    @Override
    public int getExecutingCount() {
        return executions.getExecutingCount();
    }

    @Override
    public Integer getMaximumThreadCount() {
        return null;
    }

    @Override
    public String getName() {
        return executions.getName();
    }

    @Override
    public int getPendingCount() {
        return executions.getPendingCount();
    }

    /**
     * Returns the {@link SuspendableActivityImpl} instance used by this object.
     *
     * @return the {@link SuspendableActivityImpl} instance used by this object.
     */
    protected SuspendableFlowNodeExecutions getSuspendableFlowNodeExecutions() {
        return executions;
    }

    @Override
    public boolean isSuspended() throws InitializingException {
        return executions.isSuspended();
    }

}
