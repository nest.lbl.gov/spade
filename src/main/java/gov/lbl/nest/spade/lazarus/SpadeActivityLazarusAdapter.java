package gov.lbl.nest.spade.lazarus;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.monitor.Instrumentation;
import gov.lbl.nest.spade.activities.IngestTicket;
import gov.lbl.nest.spade.workflow.ActivityDefermentKey;

/**
 * This class adapters a raw SPADE activity class so that it can to execute
 * within an Lazarus instance.
 *
 * @author patton
 */
public class SpadeActivityLazarusAdapter {

    private enum CallType {
                           DEFERRABLE,
                           DEFERRABLE_CALLBACK,
                           NON_DEFERRABLE,
                           REDEFERRABLE_CALLBACK,
                           VOID_DEFERRABLE,
                           VOID_DEFERRABLE_CALLBACK,
                           VOID_NON_DEFERRABLE,
                           VOID_REDEFERRABLE_CALLBACK,
    }

    /**
     * The {@link Instrumentation}, if any, used to instrument all SPADE activities.
     */
    private static final Instrumentation ACTIVITY_INSTRUMENTATION = Instrumentation.getInstrumentation(SpadeActivityLazarusAdapter.class.getSimpleName());

    /**
     * The String used to specify a "failure" transition transition.
     */
    private static final String FAILED_TRANSITION_TYPE = "Failed";

    /**
     * The {@link Logger} instance to used.
     */
    private static final Logger LOG = LoggerFactory.getLogger("gov.lbl.nest.spade.activity");

    /**
     * Returns a String containing the number of seconds since the specified start
     * time until the present time.
     *
     * @param start
     *            the time from which this duration should be calculated.
     *
     * @return a String containing the number of seconds since the specified start
     *         time until the present time.
     */
    private static String getDuration(Long start) {
        if (null == start) {
            return null;
        }
        final long now = new Date().getTime();
        return Long.toString(TimeUnit.SECONDS.convert(now - start.longValue(),
                                                      TimeUnit.MILLISECONDS));
    }

    /**
     * Saves instrumented information for the beginning of the execution of a
     * activity.
     *
     * @param name
     *            the name of the activity that is starting
     * @param ticket
     *            the ticket whose activity has started.
     */
    private static Map<String, String> instrumentBegin(final String name,
                                                       final IngestTicket ticket) {
        final String bundleName = (ticket.getBundle()).getName();
        logTransition("Starting",
                      name,
                      bundleName,
                      ticket.getIdentity(),
                      null);
        final Map<String, String> attributes;
        attributes = new HashMap<>(1);
        attributes.put("bundle.id",
                       bundleName);
        attributes.put("ticket.id",
                       ticket.getIdentity());
        ACTIVITY_INSTRUMENTATION.start(ticket.getUuid(),
                                       attributes);
        return attributes;
    }

    /**
     * Saves instrumented information for the end of the execution of a activity.
     *
     * @param name
     *            the name of the activity that has stopped.
     * @param ticket
     *            the ticket whose activity has started.
     * @param attributes
     *            the attributes used to instrument the start of this activity.
     * @param duration
     *            the duration of this activity.
     */
    private static void instrumentEnd(final String name,
                                      final IngestTicket ticket,
                                      final Map<String, String> attributes,
                                      final String duration) {
        attributes.put("duration",
                       duration);
        ACTIVITY_INSTRUMENTATION.end(ticket.getUuid(),
                                     attributes);
        final String bundleName = (ticket.getBundle()).getName();
        logTransition("Stopped",
                      name,
                      bundleName,
                      ticket.getIdentity(),
                      duration);
    }

    /**
     * Saves instrumented information for the start of the failure of a activity.
     *
     * @param name
     *            the name of the activity that has stopped.
     * @param ticket
     *            the ticket whose activity has started.
     * @param attributes
     *            the attributes used to instrument the start of this activity.
     * @param duration
     *            the duration of this activity.
     */
    private static void instrumentFailure(final String name,
                                          final IngestTicket ticket,
                                          final Map<String, String> attributes,
                                          final String duration) {
        attributes.put("duration",
                       duration);
        ACTIVITY_INSTRUMENTATION.error(-1,
                                       ticket.getUuid(),
                                       attributes);
        final String bundleName = (ticket.getBundle()).getName();
        logTransition("Failed",
                      name,
                      bundleName,
                      ticket.getIdentity(),
                      duration);
    }

    /**
     * Saves instrumented information for the postponement of a activity.
     *
     * @param name
     *            the name of the activity that has stopped.
     * @param ticket
     *            the ticket whose activity has started.
     * @param attributes
     *            the attributes used to instrument the start of this activity.
     * @param duration
     *            the duration of this activity.
     */
    private static void instrumentPostponement(final String name,
                                               final IngestTicket ticket,
                                               final Map<String, String> attributes,
                                               final String duration) {
        attributes.put("duration",
                       duration);
        ACTIVITY_INSTRUMENTATION.pointOfInterst("postponement",
                                                ticket.getUuid(),
                                                attributes);
        final String bundleName = (ticket.getBundle()).getName();
        logTransition("Failed",
                      name,
                      bundleName,
                      ticket.getIdentity(),
                      duration);
    }

    /**
     * Write the detail of a activity transition to the logging system.
     *
     * @param type
     *            a String specifying the type of transition.
     * @param name
     *            the name of the activity executing the transition.
     * @param bundle
     *            the bundle to which the transition is being applied
     * @param identity
     * @param duration
     */
    private static void logTransition(String type,
                                      String name,
                                      String bundle,
                                      String identity,
                                      String duration) {
        final String timing;
        if (null == duration) {
            timing = "";
        } else {
            final String plural;
            if ("1" == duration) {
                plural = "";
            } else {
                plural = "s";
            }
            timing = " elapsed time " + duration
                     + " sec"
                     + plural
                     + ".";
        }
        if (FAILED_TRANSITION_TYPE.equals(type)) {
            LOG.error(type + " \""
                      + name
                      + "\" activity for \""
                      + bundle
                      + "\" (Ticket #"
                      + identity
                      + ")"
                      + timing);
            return;
        }
        LOG.info(type + " \""
                 + name
                 + "\" activity for \""
                 + bundle
                 + "\" (Ticket #"
                 + identity
                 + ")"
                 + timing);
    }

    /**
     * The {@link Object} instance on which this Object is adapting.
     */
    private final Object instance;

    /**
     * The {@link Method} instance to execute on the adapted object.
     */
    private Method method;

    /**
     * The name of the activity being adapted.
     */
    private final String name;

    /**
     * Creates an instance of this class.
     *
     * @param name
     *            The name of the activity being adapted.
     * @param instance
     *            the instance of the SPADE activity class being adapted by this
     *            class.
     */
    public SpadeActivityLazarusAdapter(String name,
                                       Object instance) {
        this.instance = instance;
        this.name = name;
    }

    /**
     * Invoked for a deferable method that returns an {@link Object} instance.
     *
     * @param ticket
     *            the {@link IngestTicket} to be acted upon.
     *
     * @return an {@link Object} instance that is the result of the action.
     *
     * @throws IllegalAccessException
     *             when the adapted {@link Method} instance can not be accessed.
     * @throws IllegalArgumentException
     *             when the arguments for the adapted {@link Method} instance are
     *             not correct.
     * @throws Exception
     *             when the the adapted {@link Method} instance execution throws an
     *             exception.
     */
    public Object deferable(IngestTicket ticket) throws IllegalAccessException,
                                                 IllegalArgumentException,
                                                 Exception {
        return execute(CallType.DEFERRABLE,
                       ticket);
    }

    /**
     * Executes the appropriate method for the adapted {@link Method} instance.
     *
     * @param callType
     *            the type of call to execute.
     * @param ticket
     *            the {@link IngestTicket} to be acted upon.
     *
     * @return an {@link Object} instance, if any, that is the result of the
     *         execution.
     *
     * @throws IllegalAccessException
     *             when the adapted {@link Method} instance can not be accessed.
     * @throws IllegalArgumentException
     *             when the arguments for the adapted {@link Method} instance are
     *             not correct.
     * @throws Exception
     *             when the the adapted {@link Method} instance execution throws an
     *             exception.
     */
    private Object execute(CallType callType,
                           IngestTicket ticket) throws IllegalAccessException,
                                                IllegalArgumentException,
                                                Exception {
        final long start = new Date().getTime();
        final Map<String, String> attributes = instrumentBegin(name,
                                                               ticket);
        try {
            final Object result;
            switch (callType) {
            case DEFERRABLE: {
                result = method.invoke(instance,
                                       new Object[] { ticket,
                                                      null });
                break;
            }
            case NON_DEFERRABLE: {
                result = method.invoke(instance,
                                       new Object[] { ticket });
                break;
            }
            case VOID_DEFERRABLE: {
                method.invoke(instance,
                              new Object[] { ticket,
                                             null });
                result = null;
                break;
            }
            case VOID_NON_DEFERRABLE:
            default: {
                method.invoke(instance,
                              new Object[] { ticket });
                result = null;
                break;
            }
            }
            final String duration = getDuration(start);
            instrumentEnd(name,
                          ticket,
                          attributes,
                          duration);
            return result;
        } catch (InvocationTargetException ite) {
            try {
                throw (Exception) ite.getCause();
            } catch (TimeoutException e) {
                final String duration = getDuration(start);
                instrumentPostponement(name,
                                       ticket,
                                       attributes,
                                       duration);
                throw e;
            } catch (Exception e) {
                final String duration = getDuration(start);
                instrumentFailure(name,
                                  ticket,
                                  attributes,
                                  duration);
                throw e;
            }
        }
    }

    /**
     * Returns the {@link Method} instance of this object to call.
     *
     * @param methodName
     *            the name of the method for the adapted class.
     * @param input
     *            the input Type for the method for the adapted class.
     *
     * @return the {@link Method} instance of this object to call.
     *
     * @throws NoSuchMethodException
     *             when there is no matching method for the adapted class.
     */
    public Method getMethod(String methodName,
                            Class<?> input) throws NoSuchMethodException {
        try {
            method = (instance.getClass()).getMethod(methodName,
                                                     new Class<?>[] { input,
                                                                      ActivityDefermentKey.class });
            final String adapterMethod;
            if (Void.TYPE == method.getReturnType()) {
                adapterMethod = "voidDeferable";
            } else {
                adapterMethod = "deferable";
            }
            return getClass().getMethod(adapterMethod,
                                        new Class<?>[] { input });
        } catch (NoSuchMethodException e) {
            method = (instance.getClass()).getMethod(methodName,
                                                     new Class<?>[] { input });
        }
        final String adapterMethod;
        if (Void.TYPE == method.getReturnType()) {
            adapterMethod = "voidNonDeferable";
        } else {
            adapterMethod = "nonDeferable";
        }
        return getClass().getMethod(adapterMethod,
                                    new Class<?>[] { input });
    }

    /**
     * Invoked for a non-deferable method that returns an {@link Object} instance.
     *
     * @param ticket
     *            the {@link IngestTicket} to be acted upon.
     *
     * @return an {@link Object} instance that is the result of the action.
     *
     * @throws IllegalAccessException
     *             when the adapted {@link Method} instance can not be accessed.
     * @throws IllegalArgumentException
     *             when the arguments for the adapted {@link Method} instance are
     *             not correct.
     * @throws Exception
     *             when the the adapted {@link Method} instance execution throws an
     *             exception.
     */
    public Object nonDeferable(IngestTicket ticket) throws IllegalAccessException,
                                                    IllegalArgumentException,
                                                    Exception {
        return execute(CallType.NON_DEFERRABLE,
                       ticket);
    }

    /**
     * Invoked for a deferable method that does not return an {@link Object}
     * instance.
     *
     * @param ticket
     *            the {@link IngestTicket} to be acted upon.
     *
     * @throws IllegalAccessException
     *             when the adapted {@link Method} instance can not be accessed.
     * @throws IllegalArgumentException
     *             when the arguments for the adapted {@link Method} instance are
     *             not correct.
     * @throws Exception
     *             when the the adapted {@link Method} instance execution throws an
     *             exception.
     */
    public void voidDeferable(IngestTicket ticket) throws IllegalAccessException,
                                                   IllegalArgumentException,
                                                   Exception {
        execute(CallType.VOID_DEFERRABLE,
                ticket);
    }

    /**
     * Invoked for a non-deferable method that does not return an {@link Object}
     * instance.
     *
     * @param ticket
     *            the {@link IngestTicket} to be acted upon.
     *
     * @throws IllegalAccessException
     *             when the adapted {@link Method} instance can not be accessed.
     * @throws IllegalArgumentException
     *             when the arguments for the adapted {@link Method} instance are
     *             not correct.
     * @throws Exception
     *             when the the adapted {@link Method} instance execution throws an
     *             exception.
     */
    public void voidNonDeferable(IngestTicket ticket) throws IllegalAccessException,
                                                      IllegalArgumentException,
                                                      Exception {
        execute(CallType.VOID_NON_DEFERRABLE,
                ticket);
    }

}
