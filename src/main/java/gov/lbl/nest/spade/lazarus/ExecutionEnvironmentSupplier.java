package gov.lbl.nest.spade.lazarus;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gov.lbl.nest.spade.config.Configuration;
import gov.lbl.nest.spade.interfaces.metadata.MetadataManager;
import gov.lbl.nest.spade.interfaces.storage.ArchiveManager;
import gov.lbl.nest.spade.interfaces.storage.WarehouseManager;
import gov.lbl.nest.spade.lazarus.JavaInstanceFactoryImpl.ExecutionEnvironment;
import gov.lbl.nest.spade.services.Bookkeeping;
import gov.lbl.nest.spade.services.CacheManager;
import gov.lbl.nest.spade.services.NeighborhoodManager;
import gov.lbl.nest.spade.services.RegistrationManager;
import gov.lbl.nest.spade.services.RetryManager;
import gov.lbl.nest.spade.services.SpoolManager;
import gov.lbl.nest.spade.services.VerificationManager;
import gov.lbl.nest.spade.services.impl.LoadedDuplications;
import jakarta.ejb.AccessTimeout;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.inject.Inject;

/**
 * This class is used to provide an {@link ExecutionEnvironment} instance that
 * is filled from an Jakarta EE environment.
 *
 * @author patton
 */
@Singleton
@AccessTimeout(value = 60,
               unit = TimeUnit.MINUTES)
/** The class does it own concurrency management, so can use READ lock. */
@jakarta.ejb.Lock(value = LockType.READ)
public class ExecutionEnvironmentSupplier {

    /**
     * The {@link ArchiveManager} instance used by this object.
     */
    @Inject
    private ArchiveManager archiveManager;

    /**
     * The {@link Bookkeeping} instance used by this object.
     */
    @Inject
    private Bookkeeping bookkeeping;

    /**
     * The {@link CacheManager} instance used by this object.
     */
    @Inject
    private CacheManager cacheManager;

    /**
     * The {@link Configuration} instance used by this object.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link ExecutionEnvironment} instance created by this object.
     */
    private ExecutionEnvironment executionEnvironment;

    /**
     * The {@link LoadedDuplications} instance used by this object.
     */
    @Inject
    private LoadedDuplications loadedDuplications;

    /**
     * The {@link Lock} instance used to make sure that the registration loading is
     * serialized.
     */
    private Lock lock = new ReentrantLock();

    /**
     * The {@link MetadataManager} instance used by this object.
     */
    @Inject
    private MetadataManager metadataManger;

    /**
     * The {@link NeighborhoodManager} instance used by this object.
     */
    @Inject
    private NeighborhoodManager neighborhoodManager;

    /**
     * The {@link RegistrationManager} instance used by this object.
     */
    @Inject
    private RegistrationManager registrationManager;

    /**
     * The {@link RetryManager} instance used by this object.
     */
    @Inject
    private RetryManager retryManager;

    /**
     * The {@link SpoolManager} instance used by this object.
     */
    @Inject
    private SpoolManager spoolManager;

    /**
     * The {@link VerificationManager} instance used by this object.
     */
    @Inject
    private VerificationManager verificationManager;

    /**
     * The {@link WarehouseManager} instance used by this object.
     */
    @Inject
    private WarehouseManager warehouseManager;

    /**
     * Returns the {@link ExecutionEnvironment} instance that was filled from an
     * Jakarta EE environment.
     *
     * @return the {@link ExecutionEnvironment} instance that was filled from an
     *         Jakarta EE environment.
     */
    public ExecutionEnvironment getExecutionEnvironment() {
        if (null == executionEnvironment) {
            lock.lock();
            try {
                if (null == executionEnvironment) {
                    executionEnvironment = new ExecutionEnvironment();
                    executionEnvironment.archiveManager = archiveManager;
                    executionEnvironment.bookkeeping = bookkeeping;
                    executionEnvironment.cacheManager = cacheManager;
                    executionEnvironment.configuration = configuration;
                    executionEnvironment.loadedDuplications = loadedDuplications;
                    executionEnvironment.metadataManager = metadataManger;
                    executionEnvironment.neighborhoodManager = neighborhoodManager;
                    executionEnvironment.registrationManager = registrationManager;
                    executionEnvironment.retryManager = retryManager;
                    executionEnvironment.spoolManager = spoolManager;
                    executionEnvironment.verificationManager = verificationManager;
                    executionEnvironment.warehouseManager = warehouseManager;
                }
            } finally {
                lock.unlock();
            }
        }
        return executionEnvironment;
    }
}
