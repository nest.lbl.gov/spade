package gov.lbl.nest.spade;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import gov.lbl.nest.common.suspension.Suspendable;
import jakarta.inject.Qualifier;

/**
 * The annotation is used to qualify the {@link Suspendable} instance use to
 * manage SPADE execution.
 *
 * @author patton
 */
@Qualifier
@Retention(RUNTIME)
@Target({ TYPE,
          METHOD,
          FIELD,
          PARAMETER })
public @interface SpadeSuspension {
    // Does nothing, just a label.
}
