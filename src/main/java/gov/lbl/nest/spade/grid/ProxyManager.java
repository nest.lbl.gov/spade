package gov.lbl.nest.spade.grid;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;

/**
 * This class manages any X509 proxies needed by the application.
 *
 * @author patton
 */
public class ProxyManager {

    // public static final member data

    /**
     * The name of the environmental variable containing location, if any, of the
     * file containing the user's proxy.
     */
    public final static String X509_USER_PROXY_ENV = "X509_USER_PROXY";

    /**
     * The name of the environmental variable containing the name of the VO from
     * which to request the proxy.
     */
    public final static String VONAME_ENV = "VONAME";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(ProxyManager.class);

    /**
     * The name of the environmental variable containing the user id for the proxy.
     */
    private final static String USER_ID_ENV = "USER_ID";

    // private static member data

    // private instance member data

    /**
     * Runs simple tests on the user's proxy.
     *
     * @param args
     *            1. the unix id of the user who owns the proxy.
     *
     * @throws Exception
     *             when there is a problem.
     */
    public static void main(String[] args) throws Exception {
        final ProxyManager proxyManager;
        try {
            if (0 == args.length) {
                proxyManager = new ProxyManager(Integer.parseInt(args[0]),
                                                null);
            } else {
                proxyManager = new ProxyManager();
            }
        } catch (NumberFormatException e) {
            throw e;
        }
        final Date expiryDate = proxyManager.getExpiryDate();
        System.out.println(expiryDate);
    }

    private static Integer readUserId() {
        final String userId = System.getenv(USER_ID_ENV);
        if (null == userId) {
            return null;
        }
        return Integer.parseInt(userId);
    }

    // constructors

    private static String readUserProxy() {
        final String userProxy = System.getenv(X509_USER_PROXY_ENV);
        if (null == userProxy) {
            return null;
        }
        return new String(userProxy);
    }

    /**
     * True if this object is managing the proxy.
     */
    private final boolean manage;

    // instance member method (alphabetic)

    /**
     * The name, if any, of the file containing the x509 proxy.
     */
    private final String proxyFile;

    /**
     * Creates an instance of this class.
     */
    public ProxyManager() {
        this(readUserId(),
             readUserProxy());
    }

    /**
     * Creates an instance of this class.
     *
     * @param userId
     *            the unix id of the user who owns the proxy.
     * @param userProxy
     *            the name of the file containing the x509 proxy.
     */
    public ProxyManager(final Integer userId,
                        final String userProxy) {
        if (null == userProxy) {
            manage = true;
            if (null == userId) {
                proxyFile = null;
                return;
            }
            proxyFile = "/tmp/x509up_u" + Integer.toString(userId);
        } else {
            manage = false;
            proxyFile = userProxy;
        }
    }

    /**
     * Returns the expiry date of the Proxy.
     *
     * @return the expiry date of the Proxy.
     *
     * @throws FileNotFoundException
     *             if the Proxy's file can not be found.
     * @throws CertificateException
     *             if the proxy can not be parsed.
     */
    private Date getExpiryDate() throws FileNotFoundException,
                                 CertificateException {
        if (null == proxyFile) {
            return null;
        }
        FileInputStream fs = new FileInputStream(proxyFile);
        X509Certificate certificate = ((X509Certificate) (CertificateFactory.getInstance("X.509")).generateCertificate(fs));
        return certificate.getNotAfter();
    }

    /**
     * Returns the name, if any, of the file containing the x509 proxy.
     *
     * @return the name, if any, of the file containing the x509 proxy.
     */
    public String getProxyFile() {
        return proxyFile;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    /**
     * Returns true if the proxy does not exist or has expired.
     *
     * @return true if the certificate has expired.
     *
     * @throws CertificateException
     *             if the proxy can not be parsed.
     */
    public boolean isExpired() throws CertificateException {
        return isExpired(new Date());
    }

    /**
     * Returns true if the proxy does not exist or will be expired by the provided
     * {@link Date}.
     *
     * @param dateTime
     *            the date against which to test the proxy's expiry.
     *
     * @return true if the certificate has expired.
     *
     * @throws CertificateException
     *             if the proxy can not be parsed.
     */
    public boolean isExpired(final Date dateTime) throws CertificateException {

        Date expiryDate;
        try {
            expiryDate = getExpiryDate();
        } catch (FileNotFoundException e) {
            return true;
        }
        if (null == expiryDate) {
            return true;
        }
        return null == expiryDate || dateTime.after(expiryDate);
    }

    /**
     * Issues a Proxy using the Virtual Organization Membership Service
     *
     * @return true if the Proxy has been successfully issued and has not already
     *         expired.
     *
     * @throws IOException
     *             when the proxy file can not be correctly handled.
     * @throws InterruptedException
     *             when this routine is interrupted.
     * @throws CertificateException
     *             if the proxy can not be parsed.
     */
    public boolean issueProxy() throws IOException,
                                InterruptedException,
                                CertificateException {
        if (!manage || null == proxyFile) {
            return false;
        }
        final String voname = System.getenv(VONAME_ENV);
        if (null == voname || "".equals(voname)) {
            return false;
        }
        final String[] cmdArray = new String[] { "voms-proxy-init",
                                                 "-hours",
                                                 "60",
                                                 "-out",
                                                 proxyFile,
                                                 "-dont-verify-ac",
                                                 "-voms",
                                                 voname };
        final Command command = new Command(cmdArray);
        command.execute();
        final ExecutionFailedException cmdException = command.getCmdException();
        if (null != cmdException) {
            LOG.error("The following command failed: " + command.toString());
            final String[] stderr = cmdException.getStdErr();
            for (String line : stderr) {
                LOG.error(line);
            }
            throw new IOException(cmdArray[0] + " execution failed",
                                  cmdException);
        }
        return !isExpired();
    }
}
