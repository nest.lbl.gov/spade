/**
 * The package contains the classes needed to run SPADE on GRID machines.
 *
 * @author patton
 */
package gov.lbl.nest.spade.grid;