package gov.lbl.nest.spade.utilities;

import java.io.File;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.rs.Bundle;

/**
 * This class implements the {@link BundleOrdering} interface so that the oldest
 * and newest bundles are preferred.
 *
 * @author patton
 */
public class OldestAndNewestOrdering implements
                                     BundleOrdering {

    /**
     * The string indicating the files are directly accessible on the local file
     * system.
     */
    private static final String LOCALHOST = "localhost";

    /**
     * The {@link Comparator} instance to order element from oldest to newest.
     */
    private final static Comparator<Bundle> OLDEST_FIRST = new Comparator<>() {

        @Override
        public int compare(Bundle o1,
                           Bundle o2) {
            final Date lhs = getDate(o1.getSemaphore());
            final Date rhs = getDate(o2.getSemaphore());
            if (null == lhs) {
                if (null == rhs) {
                    return 0;
                } else {
                    return -1;
                }
            }
            if (null == rhs) {
                return 1;
            }
            final long t1 = lhs.getTime();
            final long t2 = rhs.getTime();
            if (t1 == t2) {
                return 0;
            }
            if (t1 > t2) {
                return -1;
            }
            return 1;
        }

        private Date getDate(ExternalFile file) {
            final String host = file.getHost();
            if (null == host || host.equals(LOCALHOST)) {
                final File localFile = new File(file.getDirectory(),
                                                file.getName());
                return new Date(localFile.lastModified());
            }
            return null;
        }
    };

    @Override
    public void sort(List<Bundle> bundles) {
        Collections.sort(bundles,
                         OLDEST_FIRST);
        if (3 > bundles.size()) {
            return;
        }
        final int lastIndex = bundles.size() - 1;
        final int finished = lastIndex / 2;
        for (int index = 0;
             finished != index;
             ++index) {
            bundles.add(((index * 2) + 1),
                        bundles.remove(lastIndex));
        }
    }

}
