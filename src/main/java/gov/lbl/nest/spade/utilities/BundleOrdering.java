package gov.lbl.nest.spade.utilities;

import java.util.List;

import gov.lbl.nest.spade.rs.Bundle;

/**
 * The interface is used define the order in which {@link Bundle} instances are
 * return from a scan.
 * 
 * @author patton
 *
 */
public interface BundleOrdering {

    /**
     * Sorts the supplied collection of {@link Bundle} instances into an order.
     *
     * @param bundle
     *            the collection of {@link Bundle} instances to sort. This will be
     *            sorted in place so the contents of this parameter may not be the
     *            same afterward as it was before.
     */
    void sort(List<Bundle> bundle);

}
