/**
 * The package contains the classes that provide general utilities for various
 * part of this application.
 *
 * @author patton
 */
package gov.lbl.nest.spade.utilities;