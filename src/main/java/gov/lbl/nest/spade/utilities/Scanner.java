package gov.lbl.nest.spade.utilities;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.spade.interfaces.policy.FetchingPolicy;
import gov.lbl.nest.spade.registry.DataLocator;
import gov.lbl.nest.spade.registry.ExternalFile;
import gov.lbl.nest.spade.registry.ExternalLocation;
import gov.lbl.nest.spade.registry.HandlingPriority;
import gov.lbl.nest.spade.registry.Registration;
import gov.lbl.nest.spade.rs.Bundle;
import gov.lbl.nest.spade.rs.Bundles;

/**
 * This class implements scanning of dropboxes to find Bundles.
 *
 * @author patton
 *
 * @param <T>
 *            the type of {@link Registration} instance being scanned.
 */
public class Scanner<T extends Registration> {

    class SemaphoreFilter implements
                          FilenameFilter {

        /**
         * The {@link ExternalLocation} instance to use when building {@link Bundle}
         * instances.
         */
        ExternalLocation location;

        /**
         * The found {@link Bundle} instances, indexed by their registration localId.
         */
        final Map<String, List<Bundle>> registeredBundles = new HashMap<>();

        /**
         * The collection of {@link Registration} instances to use while filtering.
         */
        Collection<T> registrations;

        /**
         * Creates an instance of this class.
         */
        public SemaphoreFilter() {
        }

        @Override
        public boolean accept(File dir,
                              String name) {
            for (Registration registration : registrations) {
                final Predicate<String> predicate = registration.getPredicate();
                final DataLocator dataLocator = registration.getDataLocator();
                final String localId = registration.getLocalId();
                final String neighbor = registration.getNeighbor();
                if (predicate.test(name)) {
                    final Bundle bundle = new Bundle(dataLocator.getBundleName(name),
                                                     new ExternalFile(location,
                                                                      name),
                                                     localId,
                                                     neighbor);
                    final List<Bundle> bundles = registeredBundles.get(localId);
                    bundles.add(bundle);
                    return true;
                }
            }
            return false;
        }

        Map<String, List<Bundle>> getRegisteredBundles() {
            return registeredBundles;
        }

        /**
         * Sets the {@link ExternalLocation} instance to use when building
         * {@link Bundle} instances.
         *
         * @param location
         *            the {@link ExternalLocation} instance to use when building
         *            {@link Bundle} instances.
         */
        public void setLocation(ExternalLocation location) {
            this.location = location;
        }

        /**
         * Sets the collection of {@link Registration} instances to use while filtering.
         *
         * @param registrations
         *            the collection of {@link Registration} instances to use while
         *            filtering.
         */
        public void setRegistrations(Collection<T> registrations) {
            this.registrations = registrations;
            for (Registration registration : registrations) {
                final String localId = registration.getLocalId();
                final List<Bundle> bundles = registeredBundles.get(localId);
                if (null == bundles) {
                    registeredBundles.put(localId,
                                          new ArrayList<Bundle>());
                }
            }
        }

    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private final static Logger LOG = LoggerFactory.getLogger(Scanner.class);

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Returns the Map of {@link Registration} instances, index by directory, that
     * are required to visit all of the sub-directories.
     *
     * @param host
     *            the host on which to find the sub-directories.
     * @param registrations
     *            the {@link Map} of registrations to use, keys by directory name.
     * @param policy
     *            the {@link FetchingPolicy} to use to find the semaphore files.
     *
     * @return the Map of {@link Registration} instances, index by directory, that
     *         are required to visit all of the sub-directories.
     */
    private Map<String, Collection<T>> getExtendDirectories(String host,
                                                            Map<String, Collection<T>> registrations,
                                                            FetchingPolicy policy) {
        final Map<String, Collection<T>> result = new HashMap<>();

        final Set<String> directories = registrations.keySet();
        if (null != directories && !directories.isEmpty()) {
            for (String directory : directories) {
                final List<T> registrationsWithSubdirs = new ArrayList<>();
                for (T registration : registrations.get(directory)) {
                    final Boolean subdirs = registration.getSubdirs();
                    if (null == subdirs || subdirs.booleanValue()) {
                        registrationsWithSubdirs.add(registration);
                    }
                }
                if (registrationsWithSubdirs.isEmpty()) {
                    result.put(directory,
                               registrations.get(directory));
                } else {
                    final ExternalLocation root = new ExternalLocation(host,
                                                                       directory);
                    // Note: subdirs include root directory
                    final List<String> subdirs = policy.getSubdirs(root);
                    if (null != subdirs && !subdirs.isEmpty()) {
                        for (String subdir : subdirs) {
                            if (result.containsKey(subdir)) {
                                final Collection<T> known = result.get(subdir);
                                for (T registration : registrationsWithSubdirs) {
                                    if (!known.contains(registration)) {
                                        known.add(registration);
                                    }
                                }
                            } else {
                                result.put(subdir,
                                           registrationsWithSubdirs);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private List<List<T>> getPrioritizedRegistrations(Map<String, Map<String, Collection<T>>> map) {
        final List<T> scannedRegistrations = new ArrayList<>();
        for (Map<String, Collection<T>> directoriesOnHost : map.values()) {
            for (Collection<T> registrations : directoriesOnHost.values()) {
                scannedRegistrations.addAll(registrations);
            }
        }
        final List<List<T>> result = new ArrayList<>();
        for (T registration : scannedRegistrations) {
            final HandlingPriority priority = registration.getPriority();
            final int priorityToUse;
            if (null == priority) {
                priorityToUse = HandlingPriority.NORMAL.ordinal();
            } else {
                priorityToUse = priority.ordinal();
            }
            if (result.size() <= priorityToUse) {
                for (int index = result.size();
                     index <= priorityToUse;
                     ++index) {
                    result.add(new ArrayList<T>());
                }
            }
            (result.get(priorityToUse)).add(registration);
        }
        return result;
    }

    /**
     * Uses the supplied {@link Registration} instances and {@link FetchingPolicy}
     * to scan for any new semaphore files that match to a registration.
     *
     * @param map
     *            the {@link Map} of registrations to use indexed by directory,
     *            keyed by host name.
     * @param policy
     *            the {@link FetchingPolicy} to use to find the semaphore files.
     * @param bundleOrdering
     *            the {@link BundleOrdering} instance that determines the order in
     *            which the bundles are returned.
     *
     * @return the {@link Collection} of {@link Bundle} instances found by the scan.
     *
     */
    public Bundles scan(Map<String, Map<String, Collection<T>>> map,
                        FetchingPolicy policy,
                        BundleOrdering bundleOrdering) {
        final Date begin = new Date();
        final List<Bundle> result = new ArrayList<>();

        final List<List<T>> prioritizedRegistrations = getPrioritizedRegistrations(map);

        Map<String, List<Bundle>> foundBundles = null;
        final Set<String> hosts = map.keySet();
        if (null == hosts || hosts.isEmpty()) {
            return new Bundles(null,
                               result,
                               begin);
        }
        final SemaphoreFilter semaphoreFilter = new SemaphoreFilter();
        for (String host : hosts) {
            final Map<String, Collection<T>> directoriesOnHost = map.get(host);
            final Map<String, Collection<T>> directoriesToScan = getExtendDirectories(host,
                                                                                      directoriesOnHost,
                                                                                      policy);
            if (null != directoriesToScan && !directoriesToScan.isEmpty()) {
                for (String directory : directoriesToScan.keySet()) {
                    final ExternalLocation location = new ExternalLocation(host,
                                                                           directory);
                    Collection<T> registrations = directoriesToScan.get(directory);
                    semaphoreFilter.setRegistrations(registrations);
                    semaphoreFilter.setLocation(location);
                    policy.list(location,
                                semaphoreFilter);
                }
            }

            if (null == foundBundles) {
                foundBundles = semaphoreFilter.getRegisteredBundles();
            } else {
                LOG.warn("Multiple hosts are not yet supported");
            }
        }

        for (int index = prioritizedRegistrations.size() - 1;
             index >= 0;
             --index) {
            final List<Bundle> prioritizedBundles = new ArrayList<>();
            final List<T> registrations = prioritizedRegistrations.get(index);
            if (null != registrations && !registrations.isEmpty()) {
                for (T registration : registrations) {
                    final List<Bundle> registeredBundles = foundBundles.get(registration.getLocalId());
                    if (null != registeredBundles) {
                        prioritizedBundles.addAll(registeredBundles);
                    }
                }
                if (!prioritizedBundles.isEmpty()) {
                    if (null == bundleOrdering) {
                        Collections.shuffle(prioritizedBundles);
                    } else {
                        bundleOrdering.sort(prioritizedBundles);
                    }
                    result.addAll(prioritizedBundles);
                }
            }
        }
        return new Bundles(null,
                           result,
                           begin);
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
