# Rescue files #

The following files in this directory can be used to "rescue" files that have ended up in the `problem` directory of the cache.

*   `rescue_spade` : Attempts to rescue all "SPADE problem" filesets by either returning saved files to their appropriate location and reseting their workflow or, if there are no saved files, cleaning up their workflow.

*   `rescue_problem` : Attempts to rescue a "SPADE problem" fileset by returning saved files to their appropriate location and reseting their workflow.

*   `cleanup_problem` : Attempts to cleanup a "SPADE problem" fileset by cleaning up the fileset's workflow.

Obviously, `rescue_spade` make used of both of te other commands while those commands can be used standalone as well.

The default of these commands are set up for use inside the standard SPADE docker container. If run outside that container (or SPADE is not being run in a container) The `PROBLEM_DIR` and `LAZARUS_DIR` environmental variables should be set to the appropriate directories.
